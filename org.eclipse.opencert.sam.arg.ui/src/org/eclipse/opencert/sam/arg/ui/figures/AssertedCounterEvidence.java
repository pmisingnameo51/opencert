/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.ui.figures;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.draw2d.ui.figures.PolylineConnectionEx;
import org.eclipse.draw2d.ColorConstants;

public class AssertedCounterEvidence extends PolylineConnectionEx {

	public AssertedCounterEvidence(){
		
	}
	@Override
	public void paint(Graphics graphics) {
		Rectangle r =getPoints().getBounds();
		graphics.setBackgroundColor(ColorConstants.white);
		graphics.setForegroundColor(ColorConstants.red);
		int radius=(r.width+r.height)/32;
		super.paint(graphics);
		Point p2 = new Point(r.getCenter().x-radius,r.getCenter().y+radius);
		Point p1 = new Point(r.getCenter().x+radius, r.getCenter().y-radius);
		graphics.drawLine(p1, p2);
		p2 = new Point(r.getCenter().x-radius, r.getCenter().y-radius);
		p1 = new Point(r.getCenter().x+radius, r.getCenter().y+radius);
		graphics.drawLine(p1, p2);
	}
}
