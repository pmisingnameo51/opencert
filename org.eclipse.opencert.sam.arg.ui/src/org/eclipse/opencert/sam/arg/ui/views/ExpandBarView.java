/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.ui.views;



import java.io.File;
import java.net.URL;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.edit.ui.dnd.LocalTransfer;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.opencert.sam.arg.ui.dnd.DragPatternListener;
import org.eclipse.opencert.sam.arg.ui.dnd.TreeDragFileListener;
import org.eclipse.opencert.sam.arg.ui.util.TreeListFiles.TreeObject;
import org.eclipse.opencert.sam.arg.ui.util.UtilConstants;
import org.eclipse.opencert.sam.arg.ui.views.ExplorerComponent.ViewContentProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.ExpandEvent;
import org.eclipse.swt.events.ExpandListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ExpandBar;
import org.eclipse.swt.widgets.ExpandItem;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.DrillDownAdapter;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.ViewPart;




public class ExpandBarView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "org.eclipse.opencert.sam.arg.ui.views.ExpandBarView";

	ExplorerComponent MEView0;
	ExplorerComponent MEView1;
	ExplorerCDOComponent MEView2;
	ExplorerCDOComponent MEView3;
	private ExpandBar viewer;

	private DrillDownAdapter drillDownAdapter0;	
	private Action doubleClickAction0;
	
	private DrillDownAdapter drillDownAdapter1;
	private Action doubleClickAction1;
	
	

	/**
	 * This is a callback that will allow us
	 * to create the viewer and initialize it.
	 */
	public void createPartControl(Composite parent) {
		String modulesDir = Platform.getPreferencesService().
				  getString("org.eclipse.opencert.sam.preferences", UtilConstants.Module_PATH, "Modules Dir wrong", null); 
		String patternsDir = Platform.getPreferencesService().
				  getString("org.eclipse.opencert.sam.preferences", UtilConstants.Pattern_PATH, "Patterns Dir wrong", null); 
		String sDirFiles="\\MyResults";
		if(patternsDir.lastIndexOf("\\")!=-1){
			sDirFiles = patternsDir.substring(0, patternsDir.lastIndexOf("\\")) + "\\MyResults";
		}
		String cdoPatternsDir = Platform.getPreferencesService().
				  getString("org.eclipse.opencert.sam.preferences", "cdoPatternPathPreference", "Patterns Dir wrong", null);
		String cdoModulesDir = Platform.getPreferencesService().
				  getString("org.eclipse.opencert.sam.preferences", "cdoModulePathPreference", "Modules Dir wrong", null);
		
		viewer = new ExpandBar (parent, SWT.V_SCROLL); 

		
		// First item (Pattern Explorer)
		Composite container0 = new Composite(viewer, SWT.NONE);
		FillLayout layout0 = new FillLayout();
		container0.setLayout(layout0);
		MEView0 = new ExplorerComponent(getViewSite(), patternsDir, "Pattern Explorer");
		MEView0.createPartControl(container0);
		URL url = this.getClass().getResource("/icons/database-16.gif");
		Image image0 = ImageDescriptor.createFromURL(url).createImage();
		ExpandItem item0 = new ExpandItem (viewer, SWT.NONE, 0);
		item0.setText("Pattern Explorer");
		item0.setHeight(container0.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
		item0.setControl(container0);
		item0.setImage(image0);
		
		// Second item (Module Explorer)
		Composite container1 = new Composite(viewer, SWT.NONE);
		FillLayout layout1 = new FillLayout();
		container1.setLayout(layout1);
		MEView1 = new ExplorerComponent(getViewSite(), modulesDir, "Module Explorer");
		MEView1.createPartControl(container1);

		Image image1 = ImageDescriptor.createFromURL(url).createImage();
		ExpandItem item1 = new ExpandItem (viewer, SWT.NONE, 1);
		item1.setText("Module Explorer");
		item1.setHeight(container1.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
		item1.setControl(container1);
		item1.setImage(image1);
//		container0.setRedraw(true);
		
		//by default Module Explorer will be expanded
		item1.setExpanded(true);
		// Third item (CDO Pattern Explorer)
		Composite container2 = new Composite(viewer, SWT.NONE);
		FillLayout layout2 = new FillLayout();
		container2.setLayout(layout2);
		MEView2 = new ExplorerCDOComponent(getViewSite(), cdoPatternsDir, "Repository Pattern Explorer");
		MEView2.createPartControl(container2);

		Image image2 = ImageDescriptor.createFromURL(url).createImage();
		ExpandItem item2 = new ExpandItem (viewer, SWT.NONE, 2);
		item2.setText("Repository Pattern Explorer");
		item2.setHeight(container2.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
		item2.setControl(container2);
		item2.setImage(image2);
		item2.setExpanded(true);
		container0.setRedraw(true);	
		
		// Forth item (CDO Module Explorer)
		Composite container3 = new Composite(viewer, SWT.NONE);
		FillLayout layout3 = new FillLayout();
		container3.setLayout(layout3);
		MEView3 = new ExplorerCDOComponent(getViewSite(), cdoModulesDir, "Repository Module Explorer");
		MEView3.createPartControl(container3);

		Image image3 = ImageDescriptor.createFromURL(url).createImage();
		ExpandItem item3 = new ExpandItem (viewer, SWT.NONE, 3);
		item3.setText("Repository Module Explorer");
		item3.setHeight(container3.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
		item3.setControl(container3);
		item3.setImage(image3);
		item3.setExpanded(true);
				
				container0.setRedraw(true);		
		// each time a menu is expanded check if there has been changes on the data
		/*
		viewer.addExpandListener(new ExpandListener() {
            public void itemCollapsed(ExpandEvent arg0) {
                Display.getCurrent().asyncExec(new Runnable() {
                    public void run() {
                    	viewer.getShell().pack();
                    }
                });
            }

            public void itemExpanded(ExpandEvent arg0) {
            	viewer.getShell().pack();

                Display.getCurrent().asyncExec(new Runnable() {
                    public void run() {
                    	viewer.getShell().pack();
                    }
                });
            }
        });
        */
		viewer.addExpandListener(new ExpandListener() {
			@Override
		    public void itemExpanded(ExpandEvent e) {
//				Widget w = e.widget;
				ExpandItem item = (ExpandItem)e.item;
//				Control c = item.getControl();
				//Widget w2 = (Widget)c.getData();
				ExplorerComponent ec= MEView0;
//				ExplorerCDOComponent ec1= null;
				if(item.getText().compareTo("Pattern Explorer") == 0) ec = MEView0;
				if(item.getText().compareTo("Module Explorer") == 0) ec = MEView1;
//				if(item.getText().compareTo("CDO Pattern Explorer") == 0) ec1 = MEView2;
//				if(item.getText().compareTo("CDO Pattern Explorer") == 0) ec1=MEView3;
//				 
//				if (ec!=null){
					ViewContentProvider cp = (ViewContentProvider) ec.viewer.getContentProvider();
					cp.initialize();
					ec.viewer.setInput(getViewSite());
					ec.viewer.expandToLevel(AbstractTreeViewer.ALL_LEVELS);
//				}else{
//					ViewContentProvider cp = (ViewContentProvider) ec1.viewer.getContentProvider();
//					cp.initialize();
//					ec1.viewer.setInput(getViewSite());
//					ec1.viewer.expandToLevel(AbstractTreeViewer.ALL_LEVELS);
//				}
				/*
				item.setHeight(container1.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
				viewer.layout(true);
				viewer.getParent().layout(true);
				*/
				
				
		    }

			@Override
			public void itemCollapsed(ExpandEvent e) {
				// TODO Auto-generated method stub
				
			}
		});


		makeActions();
		contributeToActionBars();
		
		// Create the help context id for the viewer's control
		PlatformUI.getWorkbench().getHelpSystem().setHelp(MEView0.viewer.getControl(), "org.eclipse.opencert.sam.arg.ui.viewer");
		drillDownAdapter0 = new DrillDownAdapter(MEView0.viewer);		
		hookContextMenu(MEView0.viewer);
//		hookDoubleClickAction(MEView0.viewer);
		
		PlatformUI.getWorkbench().getHelpSystem().setHelp(MEView1.viewer.getControl(), "org.eclipse.opencert.sam.arg.ui.viewer");
		drillDownAdapter1 = new DrillDownAdapter(MEView1.viewer);		
		hookContextMenu(MEView1.viewer);
//		hookDoubleClickAction(MEView1.viewer);
		
		int operations = DND.DROP_COPY;
	    Transfer[] transferTypes = new Transfer[]{LocalTransfer.getInstance()};
	    // asocia internamente el Control tipo Tree a este Drag Listener 
	    MEView0.viewer.addDragSupport(operations, transferTypes , new TreeDragFileListener(MEView0.viewer, sDirFiles));
	    MEView1.viewer.addDragSupport(operations, transferTypes , new TreeDragFileListener(MEView1.viewer, sDirFiles));
	    MEView2.viewer.addDragSupport(operations, transferTypes , new DragPatternListener(MEView2.viewer));
	    MEView3.viewer.addDragSupport(operations, transferTypes , new DragPatternListener(MEView3.viewer));

		/*
	    int operations = DND.DROP_COPY | DND.DROP_MOVE;
	    Transfer[] transferTypes = new Transfer[]{TextTransfer.getInstance()};
	    MEView1.viewer.addDropSupport(operations, transferTypes, new TreeDropFile(MEView1.viewer));
	    */

	}

	private void hookContextMenu(final TreeViewer vw) {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				ExpandBarView.this.fillContextMenu(manager, vw);
			}
		});
		Menu menu = menuMgr.createContextMenu(vw.getControl());
		vw.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, vw);
	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}
	
//	private void contributeToActionBars(TreeViewer vw) {
//		IActionBars bars = getViewSite().getActionBars();
//		//fillLocalPullDown(bars.getMenuManager());
//		fillLocalToolBar(bars.getToolBarManager(), vw);
//	}

	private void fillLocalPullDown(IMenuManager manager) {
		manager.add(new Separator());
	}

	// context menu for a view
	private void fillContextMenu(IMenuManager manager, TreeViewer vw) {
		if(vw.equals(MEView0.viewer)) drillDownAdapter0.addNavigationActions(manager);
		else if(vw.equals(MEView1.viewer)) drillDownAdapter1.addNavigationActions(manager);
		// Other plug-ins can contribute there actions here
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(new Separator());
	}

	
	@SuppressWarnings("unused")
	private void fillLocalToolBar(IToolBarManager manager,TreeViewer vw) {
		if(vw.equals(MEView0.viewer)) drillDownAdapter0.addNavigationActions(manager);
		else if(vw.equals(MEView1.viewer)) drillDownAdapter1.addNavigationActions(manager);
	}

	private void makeActions() {
		setDoubleClickAction0(new Action() {
			public void run() {
				ISelection selection = MEView0.viewer.getSelection();
				TreeObject obj = (TreeObject)((IStructuredSelection)selection).getFirstElement();
				File file = new File(obj.getFullName());
				if(!file.isFile() || (!obj.getFullName().contains(UtilConstants.ARGFILE_MODEL_ID) && !obj.getFullName().contains(UtilConstants.GSNFILE_MODEL_ID))) return;
				IWorkspace workspace= ResourcesPlugin.getWorkspace(); 
				IPath location= Path.fromOSString(file.getAbsolutePath()); 
				IFile file2= workspace.getRoot().getFileForLocation(location); 
			    IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			    IWorkbenchPage page = window.getActivePage();
			    try {
						  page.openEditor
						  (new FileEditorInput(file2),
							PlatformUI.getWorkbench().getEditorRegistry().getDefaultEditor(file2.getFullPath().toString()).getId()); 
				  }
				  catch (PartInitException exception) {
						  exception.printStackTrace();
			  }
			}
		});

		
		setDoubleClickAction1(new Action() {
			public void run() {
				ISelection selection = MEView1.viewer.getSelection();
				TreeObject obj = (TreeObject)((IStructuredSelection)selection).getFirstElement();
				File file = new File(obj.getFullName());
				if(!file.isFile() || (!obj.getFullName().contains(UtilConstants.ARGFILE_MODEL_ID) && !obj.getFullName().contains(UtilConstants.GSNFILE_MODEL_ID))) return;
				IWorkspace workspace= ResourcesPlugin.getWorkspace(); 
				IPath location= Path.fromOSString(file.getAbsolutePath()); 
				IFile file2= workspace.getRoot().getFileForLocation(location); 
			    IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			    IWorkbenchPage page = window.getActivePage();
			    try {
						  page.openEditor
						  (new FileEditorInput(file2),
							PlatformUI.getWorkbench().getEditorRegistry().getDefaultEditor(file2.getFullPath().toString()).getId()); 
				  }
				  catch (PartInitException exception) {
						  exception.printStackTrace();
			  }
			}
		});
	}

//	private void hookDoubleClickAction(final TreeViewer vw) {
//		vw.addDoubleClickListener(new IDoubleClickListener() {
//			public void doubleClick(DoubleClickEvent event) {
//				if(vw.equals(MEView0.viewer)) doubleClickAction0.run();				
//				else if(vw.equals(MEView1.viewer)) doubleClickAction1.run();
//			}
//		});
//	}
	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.setFocus();
	}

	public Action getDoubleClickAction0() {
		return doubleClickAction0;
	}

	public void setDoubleClickAction0(Action doubleClickAction0) {
		this.doubleClickAction0 = doubleClickAction0;
	}

	public Action getDoubleClickAction1() {
		return doubleClickAction1;
	}

	public void setDoubleClickAction1(Action doubleClickAction1) {
		this.doubleClickAction1 = doubleClickAction1;
	}

}
