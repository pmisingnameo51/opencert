/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.ui.util;

import static java.nio.file.FileVisitResult.*;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.core.runtime.IAdaptable;


public class TreeListFiles extends SimpleFileVisitor<Path> {
	
	/*
	 * The content provider class is responsible for
	 * providing objects to the view. It can wrap
	 * existing objects in adapters or simply return
	 * objects as-is. These objects may be sensitive
	 * to the current input of the view, or ignore
	 * it and always show the same content 
	 * (like Task List, for example).
	 */
	 
	public class TreeObject implements IAdaptable {
		private String name;
		private String Fullname;
		private TreeParent parent;
		
		public TreeObject(String name, String fullname) {
			this.name = name;
			this.Fullname = fullname;
		}
		public String getName() {
			return name;
		}
		public String getFullName() {
			return Fullname;
		}
		public void setParent(TreeParent parent) {
			this.parent = parent;
		}
		public TreeParent getParent() {
			return parent;
		}
		// lo requiere ExplorerComponent.ViewLabelProvider
		public String toString() {
			return getName();
		}

		public Object getAdapter(@SuppressWarnings("rawtypes") Class key) {
			return null;
		}
	}
	
	public class TreeParent extends TreeObject {
		private ArrayList<TreeObject> children;
		public TreeParent(String name, String fullname) {
			super(name, fullname);
			children = new ArrayList<TreeObject>();
		}
		public void addChild(TreeObject child) {
			children.add(child);
			child.setParent(this);
		}
		public void removeChild(TreeObject child) {
			children.remove(child);
			child.setParent(null);
		}
		public TreeObject [] getChildren() {
			return children.toArray(new TreeObject[children.size()]);
		}
		public boolean hasChildren() {
			return children.size()>0;
		}
		public TreeObject getChild (String sName)
		{
			TreeObject res = null;
			
			if(getFullName().compareTo(sName) == 0) return this;
			
			Iterator<TreeObject> it = children.iterator();
			while(it.hasNext())
			{
				TreeObject child = it.next();
				if(child.getFullName().compareTo(sName) == 0) return child;
				else if(child.getClass().getSimpleName().compareTo("TreeParent") == 0)
				{
					res = ((TreeParent)child).getChild(sName);
				}
				if(res != null) break;
			}
				
			return res;
		}
	}
	

    private String rootDir ="";
	private TreeParent invisibleRoot=new TreeParent("","");
	//private TreeObject[] to1 = new TreeObject[UtilConstants.MAX_SUB_DIR];
	//private int numTO=0;
	
    	// Print information about
    // each type of file.
    public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
//        if (attr.isSymbolicLink()) {
//            System.out.format("Symbolic link: %s ", file);
//        } else if (attr.isRegularFile()) {
//            System.out.format("Regular file: %s ", file);
//        } else {
//            System.out.format("Other: %s ", file);
//        }
//        System.out.println("(" + attr.size() + "bytes)");
        /*
        to1[numTO] = new TreeObject(file.getFileName().toString(),file.getParent().toString()+"\\"+file.getFileName().toString());
        numTO++;*/
        TreeParent p1 = (TreeParent)invisibleRoot.getChild(file.getParent().toString());
        if(file.getFileName().toString().contains(UtilConstants.ARGFILE_DIAGRAM_ID) || file.getFileName().toString().contains(UtilConstants.GSNFILE_DIAGRAM_ID)){
    	p1.addChild(new TreeObject(file.getFileName().toString(),file.getParent().toString()+"\\"+file.getFileName().toString()));
        }
    	return CONTINUE;
    }

    
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {        
        System.out.format("Directory preVisit: %s%n", dir);
        TreeParent p1 = new TreeParent(dir.getFileName().toString(),dir.getParent().toString()+"\\"+dir.getFileName().toString());
        Path parent = dir.getParent();
        String sParent = "";
        if(parent != null ) sParent = parent.toString();
        TreeParent tParent =  invisibleRoot;
        // && sParent.compareTo(rootDir) != 0
        if (dir.toString().compareTo(rootDir) != 0 && 
            sParent.compareTo("") != 0) tParent = (TreeParent)invisibleRoot.getChild(sParent);
        /* MCP: OJO si no me da problemas 
        if(!dir.getFileName().toString().startsWith(".")){
        tParent.addChild(p1);
        }
        */
        if(!dir.getFileName().toString().startsWith(".") && tParent != null) {
        	tParent.addChild(p1);        
        }
        return CONTINUE;    
    	}    

    // Print each directory visited.
    /*
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
        System.out.format("Directory postVisit: %s%n", dir);
        TreeParent p1 =  (TreeParent)invisibleRoot.getChild(dir.getParent().toString()+"\\"+dir.getFileName().toString());
        for(int i=0; i< numTO ; i++)
        {
        	p1.addChild(to1[i]);
            //to1[i].setParent(p1); lo hace addChild
        }
        // reiniciar lista ficheros
        numTO = 0;
        return CONTINUE;
    }*/

    // If there is some error accessing
    public FileVisitResult visitFileFailed(Path file, IOException exc) {
        System.err.println(exc);
        return CONTINUE;
    }
    
    public TreeParent getRoot()
    {
    	return invisibleRoot;
    }
    
    public TreeListFiles (String sRootDir)
    {
    	rootDir = sRootDir;
    }
}
