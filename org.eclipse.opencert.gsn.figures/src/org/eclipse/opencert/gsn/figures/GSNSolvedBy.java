/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation and KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza(Tecnalia) - initial API and implementation
 *   Alejandra Ru�z(Tecnalia) - initial API and implementation
 *   Idoya Del R�o(Tecnalia) - initial API and implementation
 *   Mari Carmen Palacios(Tecnalia) - initial API and implementation
 *   Angel L�pez(Tecnalia) - initial API and implementation
 *   Jan Mauersberger(KPIT)- LayoutUtil implementation
 *   Sascha Baumgart(KPIT)- LayoutUtil implementation
 *******************************************************************************/
package org.eclipse.opencert.gsn.figures;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.draw2d.ui.figures.PolylineConnectionEx;
import org.eclipse.draw2d.ColorConstants;

public class GSNSolvedBy extends PolylineConnectionEx {

	//says if the element is related with any multiplicity extension
	private  int shape;

	public GSNSolvedBy(){
		this.setShape(0);
		
	}
	@Override
	public void paint(Graphics graphics) {
		Rectangle r =getPoints().getBounds();
		int radius=(r.width+r.height)/32;
		super.paint(graphics);
		Rectangle circle=new Rectangle(r.getCenter().x-radius, r.getCenter().y-radius, 2*radius, 2*radius);
		if(shape==1){
			graphics.setBackgroundColor(ColorConstants.white);
			graphics.setForegroundColor(ColorConstants.black);
			graphics.drawOval(circle);
			graphics.fillOval(circle);
		}
		else if(shape==2){
			graphics.setBackgroundColor(ColorConstants.black);
			graphics.setForegroundColor(ColorConstants.black);
			graphics.drawOval(circle);
			graphics.fillOval(circle);
		}

	}
	
	public void setShape(int shape) {
		this.shape = shape;
	}
	public int getShape() {
		return shape;
	}
}
