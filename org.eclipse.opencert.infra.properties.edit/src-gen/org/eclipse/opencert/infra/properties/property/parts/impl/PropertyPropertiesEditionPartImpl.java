/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.infra.properties.property.parts.impl;

// Start of user code for imports
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.eef.runtime.EEFRuntimePlugin;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.EEFFeatureEditorDialog;
import org.eclipse.emf.eef.runtime.ui.widgets.EMFComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.opencert.infra.properties.property.PropertyPackage;
import org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart;
import org.eclipse.opencert.infra.properties.property.parts.PropertyViewsRepository;
import org.eclipse.opencert.infra.properties.property.providers.PropertyMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

// End of user code

/**
 * 
 * 
 */
public class PropertyPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, PropertyPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected EMFComboViewer datatype;
	protected Text enumValues;
	protected Button editEnumValues;
	private EList enumValuesList;
	protected Text unit;



	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public PropertyPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence property_Step = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = property_Step.addStep(PropertyViewsRepository.Property_.Properties.class);
		propertiesStep.addStep(PropertyViewsRepository.Property_.Properties.id);
		propertiesStep.addStep(PropertyViewsRepository.Property_.Properties.name);
		propertiesStep.addStep(PropertyViewsRepository.Property_.Properties.datatype);
		propertiesStep.addStep(PropertyViewsRepository.Property_.Properties.enumValues);
		propertiesStep.addStep(PropertyViewsRepository.Property_.Properties.unit);
		
		
		composer = new PartComposer(property_Step) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == PropertyViewsRepository.Property_.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == PropertyViewsRepository.Property_.Properties.id) {
					return createIdText(parent);
				}
				if (key == PropertyViewsRepository.Property_.Properties.name) {
					return createNameText(parent);
				}
				if (key == PropertyViewsRepository.Property_.Properties.datatype) {
					return createDatatypeEMFComboViewer(parent);
				}
				if (key == PropertyViewsRepository.Property_.Properties.enumValues) {
					return createEnumValuesMultiValuedEditor(parent);
				}
				if (key == PropertyViewsRepository.Property_.Properties.unit) {
					return createUnitText(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(PropertyMessages.PropertyPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	
	protected Composite createIdText(Composite parent) {
		createDescription(parent, PropertyViewsRepository.Property_.Properties.id, PropertyMessages.PropertyPropertiesEditionPart_IdLabel);
		id = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PropertyPropertiesEditionPartImpl.this, PropertyViewsRepository.Property_.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
			}

		});
		id.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PropertyPropertiesEditionPartImpl.this, PropertyViewsRepository.Property_.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}

		});
		EditingUtils.setID(id, PropertyViewsRepository.Property_.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(PropertyViewsRepository.Property_.Properties.id, PropertyViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(Composite parent) {
		createDescription(parent, PropertyViewsRepository.Property_.Properties.name, PropertyMessages.PropertyPropertiesEditionPart_NameLabel);
		name = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PropertyPropertiesEditionPartImpl.this, PropertyViewsRepository.Property_.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
			}

		});
		name.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PropertyPropertiesEditionPartImpl.this, PropertyViewsRepository.Property_.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}

		});
		EditingUtils.setID(name, PropertyViewsRepository.Property_.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(PropertyViewsRepository.Property_.Properties.name, PropertyViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDatatypeEMFComboViewer(Composite parent) {
		createDescription(parent, PropertyViewsRepository.Property_.Properties.datatype, PropertyMessages.PropertyPropertiesEditionPart_DatatypeLabel);
		datatype = new EMFComboViewer(parent);
		datatype.setContentProvider(new ArrayContentProvider());
		datatype.setLabelProvider(new AdapterFactoryLabelProvider(EEFRuntimePlugin.getDefault().getAdapterFactory()));
		GridData datatypeData = new GridData(GridData.FILL_HORIZONTAL);
		datatype.getCombo().setLayoutData(datatypeData);
		datatype.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 * 	
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PropertyPropertiesEditionPartImpl.this, PropertyViewsRepository.Property_.Properties.datatype, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getDatatype()));
			}

		});
		datatype.setID(PropertyViewsRepository.Property_.Properties.datatype);
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(PropertyViewsRepository.Property_.Properties.datatype, PropertyViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createDatatypeEMFComboViewer

		// End of user code
		return parent;
	}

	protected Composite createEnumValuesMultiValuedEditor(Composite parent) {
		enumValues = SWTUtils.createScrollableText(parent, SWT.BORDER | SWT.READ_ONLY);
		GridData enumValuesData = new GridData(GridData.FILL_HORIZONTAL);
		enumValuesData.horizontalSpan = 2;
		enumValues.setLayoutData(enumValuesData);
		EditingUtils.setID(enumValues, PropertyViewsRepository.Property_.Properties.enumValues);
		EditingUtils.setEEFtype(enumValues, "eef::MultiValuedEditor::field"); //$NON-NLS-1$
		editEnumValues = new Button(parent, SWT.NONE);
		editEnumValues.setText(getDescription(PropertyViewsRepository.Property_.Properties.enumValues, PropertyMessages.PropertyPropertiesEditionPart_EnumValuesLabel));
		GridData editEnumValuesData = new GridData();
		editEnumValues.setLayoutData(editEnumValuesData);
		editEnumValues.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			public void widgetSelected(SelectionEvent e) {
				EEFFeatureEditorDialog dialog = new EEFFeatureEditorDialog(
						enumValues.getShell(), "Property", new AdapterFactoryLabelProvider(adapterFactory), //$NON-NLS-1$
						enumValuesList, PropertyPackage.eINSTANCE.getProperty_EnumValues().getEType(), null,
						false, true, 
						null, null);
				if (dialog.open() == Window.OK) {
					enumValuesList = dialog.getResult();
					if (enumValuesList == null) {
						enumValuesList = new BasicEList();
					}
					enumValues.setText(enumValuesList.toString());
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PropertyPropertiesEditionPartImpl.this, PropertyViewsRepository.Property_.Properties.enumValues, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, new BasicEList(enumValuesList)));
					setHasChanged(true);
				}
			}
		});
		EditingUtils.setID(editEnumValues, PropertyViewsRepository.Property_.Properties.enumValues);
		EditingUtils.setEEFtype(editEnumValues, "eef::MultiValuedEditor::browsebutton"); //$NON-NLS-1$
		// Start of user code for createEnumValuesMultiValuedEditor

		// End of user code
		return parent;
	}

	
	protected Composite createUnitText(Composite parent) {
		createDescription(parent, PropertyViewsRepository.Property_.Properties.unit, PropertyMessages.PropertyPropertiesEditionPart_UnitLabel);
		unit = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData unitData = new GridData(GridData.FILL_HORIZONTAL);
		unit.setLayoutData(unitData);
		unit.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PropertyPropertiesEditionPartImpl.this, PropertyViewsRepository.Property_.Properties.unit, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, unit.getText()));
			}

		});
		unit.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PropertyPropertiesEditionPartImpl.this, PropertyViewsRepository.Property_.Properties.unit, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, unit.getText()));
				}
			}

		});
		EditingUtils.setID(unit, PropertyViewsRepository.Property_.Properties.unit);
		EditingUtils.setEEFtype(unit, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(PropertyViewsRepository.Property_.Properties.unit, PropertyViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createUnitText

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(PropertyViewsRepository.Property_.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(PropertyMessages.Property_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(PropertyViewsRepository.Property_.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(PropertyMessages.Property_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart#getDatatype()
	 * 
	 */
	public Enumerator getDatatype() {
		Enumerator selection = (Enumerator) ((StructuredSelection) datatype.getSelection()).getFirstElement();
		return selection;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart#initDatatype(Object input, Enumerator current)
	 */
	public void initDatatype(Object input, Enumerator current) {
		datatype.setInput(input);
		datatype.modelUpdating(new StructuredSelection(current));
		boolean eefElementEditorReadOnlyState = isReadOnly(PropertyViewsRepository.Property_.Properties.datatype);
		if (eefElementEditorReadOnlyState && datatype.isEnabled()) {
			datatype.setEnabled(false);
			datatype.setToolTipText(PropertyMessages.Property_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !datatype.isEnabled()) {
			datatype.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart#setDatatype(Enumerator newValue)
	 * 
	 */
	public void setDatatype(Enumerator newValue) {
		datatype.modelUpdating(new StructuredSelection(newValue));
		boolean eefElementEditorReadOnlyState = isReadOnly(PropertyViewsRepository.Property_.Properties.datatype);
		if (eefElementEditorReadOnlyState && datatype.isEnabled()) {
			datatype.setEnabled(false);
			datatype.setToolTipText(PropertyMessages.Property_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !datatype.isEnabled()) {
			datatype.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart#getEnumValues()
	 * 
	 */
	public EList getEnumValues() {
		return enumValuesList;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart#setEnumValues(EList newValue)
	 * 
	 */
	public void setEnumValues(EList newValue) {
		enumValuesList = newValue;
		if (newValue != null) {
			enumValues.setText(enumValuesList.toString());
		} else {
			enumValues.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(PropertyViewsRepository.Property_.Properties.enumValues);
		if (eefElementEditorReadOnlyState && enumValues.isEnabled()) {
			enumValues.setEnabled(false);
			enumValues.setToolTipText(PropertyMessages.Property_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !enumValues.isEnabled()) {
			enumValues.setEnabled(true);
		}	
		
	}

	public void addToEnumValues(Object newValue) {
		enumValuesList.add(newValue);
		if (newValue != null) {
			enumValues.setText(enumValuesList.toString());
		} else {
			enumValues.setText(""); //$NON-NLS-1$
		}
	}

	public void removeToEnumValues(Object newValue) {
		enumValuesList.remove(newValue);
		if (newValue != null) {
			enumValues.setText(enumValuesList.toString());
		} else {
			enumValues.setText(""); //$NON-NLS-1$
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart#getUnit()
	 * 
	 */
	public String getUnit() {
		return unit.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart#setUnit(String newValue)
	 * 
	 */
	public void setUnit(String newValue) {
		if (newValue != null) {
			unit.setText(newValue);
		} else {
			unit.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(PropertyViewsRepository.Property_.Properties.unit);
		if (eefElementEditorReadOnlyState && unit.isEnabled()) {
			unit.setEnabled(false);
			unit.setToolTipText(PropertyMessages.Property_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !unit.isEnabled()) {
			unit.setEnabled(true);
		}	
		
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return PropertyMessages.Property_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
