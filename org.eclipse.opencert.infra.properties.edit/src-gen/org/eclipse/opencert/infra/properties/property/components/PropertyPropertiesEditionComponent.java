/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.infra.properties.property.components;

// Start of user code for imports
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.impl.utils.EEFUtils;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.infra.properties.property.DataTypeKind;
import org.eclipse.opencert.infra.properties.property.Property;
import org.eclipse.opencert.infra.properties.property.PropertyPackage;
import org.eclipse.opencert.infra.properties.property.parts.PropertyPropertiesEditionPart;
import org.eclipse.opencert.infra.properties.property.parts.PropertyViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class PropertyPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	
	/**
	 * Default constructor
	 * 
	 */
	public PropertyPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject property, String editing_mode) {
		super(editingContext, property, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = PropertyViewsRepository.class;
		partKey = PropertyViewsRepository.Property_.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final Property property = (Property)elt;
			final PropertyPropertiesEditionPart basePart = (PropertyPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(PropertyViewsRepository.Property_.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, property.getId()));
			
			if (isAccessible(PropertyViewsRepository.Property_.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, property.getName()));
			
			if (isAccessible(PropertyViewsRepository.Property_.Properties.datatype)) {
				basePart.initDatatype(EEFUtils.choiceOfValues(property, PropertyPackage.eINSTANCE.getProperty_Datatype()), property.getDatatype());
			}
			if (isAccessible(PropertyViewsRepository.Property_.Properties.enumValues))
				basePart.setEnumValues(property.getEnumValues());
			
			if (isAccessible(PropertyViewsRepository.Property_.Properties.unit))
				basePart.setUnit(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, property.getUnit()));
			
			// init filters
			
			
			
			
			
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}








	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == PropertyViewsRepository.Property_.Properties.id) {
			return GeneralPackage.eINSTANCE.getNamedElement_Id();
		}
		if (editorKey == PropertyViewsRepository.Property_.Properties.name) {
			return GeneralPackage.eINSTANCE.getNamedElement_Name();
		}
		if (editorKey == PropertyViewsRepository.Property_.Properties.datatype) {
			return PropertyPackage.eINSTANCE.getProperty_Datatype();
		}
		if (editorKey == PropertyViewsRepository.Property_.Properties.enumValues) {
			return PropertyPackage.eINSTANCE.getProperty_EnumValues();
		}
		if (editorKey == PropertyViewsRepository.Property_.Properties.unit) {
			return PropertyPackage.eINSTANCE.getProperty_Unit();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		Property property = (Property)semanticObject;
		if (PropertyViewsRepository.Property_.Properties.id == event.getAffectedEditor()) {
			property.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (PropertyViewsRepository.Property_.Properties.name == event.getAffectedEditor()) {
			property.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (PropertyViewsRepository.Property_.Properties.datatype == event.getAffectedEditor()) {
			property.setDatatype((DataTypeKind)event.getNewValue());
		}
		if (PropertyViewsRepository.Property_.Properties.enumValues == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.SET) {
				property.getEnumValues().clear();
				property.getEnumValues().addAll(((EList) event.getNewValue()));
			}
		}
		if (PropertyViewsRepository.Property_.Properties.unit == event.getAffectedEditor()) {
			property.setUnit((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			PropertyPropertiesEditionPart basePart = (PropertyPropertiesEditionPart)editingPart;
			if (GeneralPackage.eINSTANCE.getNamedElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(PropertyViewsRepository.Property_.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (GeneralPackage.eINSTANCE.getNamedElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(PropertyViewsRepository.Property_.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (PropertyPackage.eINSTANCE.getProperty_Datatype().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && isAccessible(PropertyViewsRepository.Property_.Properties.datatype))
				basePart.setDatatype((DataTypeKind)msg.getNewValue());
			
			if (PropertyPackage.eINSTANCE.getProperty_EnumValues().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(PropertyViewsRepository.Property_.Properties.enumValues)) {
				if (msg.getNewValue() instanceof EList<?>) {
					basePart.setEnumValues((EList<?>)msg.getNewValue());
				} else if (msg.getNewValue() == null) {
					basePart.setEnumValues(new BasicEList<Object>());
				} else {
					BasicEList<Object> newValueAsList = new BasicEList<Object>();
					newValueAsList.add(msg.getNewValue());
					basePart.setEnumValues(newValueAsList);
				}
			}
			
			if (PropertyPackage.eINSTANCE.getProperty_Unit().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(PropertyViewsRepository.Property_.Properties.unit)) {
				if (msg.getNewValue() != null) {
					basePart.setUnit(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setUnit("");
				}
			}
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			GeneralPackage.eINSTANCE.getNamedElement_Id(),
			GeneralPackage.eINSTANCE.getNamedElement_Name(),
			PropertyPackage.eINSTANCE.getProperty_Datatype(),
			PropertyPackage.eINSTANCE.getProperty_EnumValues(),
			PropertyPackage.eINSTANCE.getProperty_Unit()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (PropertyViewsRepository.Property_.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), newValue);
				}
				if (PropertyViewsRepository.Property_.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), newValue);
				}
				if (PropertyViewsRepository.Property_.Properties.datatype == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(PropertyPackage.eINSTANCE.getProperty_Datatype().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(PropertyPackage.eINSTANCE.getProperty_Datatype().getEAttributeType(), newValue);
				}
				if (PropertyViewsRepository.Property_.Properties.enumValues == event.getAffectedEditor()) {
					BasicDiagnostic chain = new BasicDiagnostic();
					for (Iterator iterator = ((List)event.getNewValue()).iterator(); iterator.hasNext();) {
						chain.add(Diagnostician.INSTANCE.validate(PropertyPackage.eINSTANCE.getProperty_EnumValues().getEAttributeType(), iterator.next()));
					}
					ret = chain;
				}
				if (PropertyViewsRepository.Property_.Properties.unit == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(PropertyPackage.eINSTANCE.getProperty_Unit().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(PropertyPackage.eINSTANCE.getProperty_Unit().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
