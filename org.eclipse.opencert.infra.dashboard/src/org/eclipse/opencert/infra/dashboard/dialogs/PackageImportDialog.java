package org.eclipse.opencert.infra.dashboard.dialogs;

import java.util.Collection;

import org.eclipse.papyrus.uml.profile.ui.dialogs.PackageImportTreeSelectionDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Package;

/**
 * This class is cloned from org.eclipse.papyrus.uml.importt.ui as it could not be imported directly.
 */
public class PackageImportDialog extends PackageImportTreeSelectionDialog {

	/**
	 * Default constructor.
	 * 
	 * @param pShell The parent shell
	 * @param pPackage The package to import
	 */
	public PackageImportDialog(Shell pShell, Package pPackage) {
		super(pShell, pPackage);
	}

	/**
	 * Constructor with a collection of packages to import.
	 *
	 * @param pShell The parent shell
	 * @param pPackages The packages to import
	 */
	public PackageImportDialog(Shell pShell, Collection<? extends Package> pPackages) {
		super(pShell, pPackages);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Composite createDialogArea(Composite pParent) {
		Composite vResult = (Composite) super.createDialogArea(pParent);

		Composite vButtons = new Composite(vResult, SWT.NONE);
		vButtons.setLayout(new RowLayout());

		Button vBtnLoadAll = new Button(vButtons, SWT.PUSH);
		vBtnLoadAll.setText("Load All"); //$NON-NLS-1$
		vBtnLoadAll.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void widgetSelected(SelectionEvent pEvent) {
				selectAll(ImportAction.LOAD);
			}
		});

		Button vBtnImportAll = new Button(vButtons, SWT.PUSH);
		vBtnImportAll.setText("Import All");//$NON-NLS-1$
		vBtnImportAll.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void widgetSelected(SelectionEvent pEvent) {
				selectAll(ImportAction.IMPORT);
			}
		});

		Button vBtnCopyAll = new Button(vButtons, SWT.PUSH);
		vBtnCopyAll.setText("Copy All"); //$NON-NLS-1$
		vBtnCopyAll.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void widgetSelected(SelectionEvent pEvent) {
				selectAll(ImportAction.COPY);
			}
		});

		Button vBtnDeselectAll = new Button(vButtons, SWT.PUSH);
		vBtnDeselectAll.setText("Deselect All"); //$NON-NLS-1$
		vBtnDeselectAll.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void widgetSelected(SelectionEvent pEvent) {
				selectAll(ImportAction.NONE);
			}
		});

		return vResult;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean isResizable() {
		return true;
	}
}
