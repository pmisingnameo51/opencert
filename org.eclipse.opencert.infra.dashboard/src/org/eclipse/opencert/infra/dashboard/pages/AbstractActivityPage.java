/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.pages;

import org.eclipse.amalgam.explorer.activity.ui.api.editor.pages.BasicSessionActivityExplorerPage;
import org.eclipse.amalgam.explorer.activity.ui.api.editor.pages.viewers.AbstractActivityExplorerViewer;
import org.eclipse.opencert.infra.dashboard.viewers.OpenCertDiagramViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.widgets.Section;

/**
 * Abstract implementation for all the activity pages. This implementation
 * allows:
 * <ul>
 * <li>to filter the content by viewpoint,</li>
 * <li>to remove the unused toolbar (contains a unique unused action) of the
 * viewer located in the right side.</li>
 * <li>to remove the unused context menus of the viewer located in the right
 * side.</li>
 * </ul>
 */
public abstract class AbstractActivityPage extends BasicSessionActivityExplorerPage {

	/** The container of the viewer located in the right side of the page. */
	private Composite mViewerContainer = null;

	/**
	 * {@inheritDoc}
	 *
	 * Overridden to remember the viewer container, which will be used later to
	 * hide the unused toolbar.
	 */
	@Override
	protected Composite createViewerContainer(final Composite pParent, final IManagedForm pManagedForm) {
		mViewerContainer = super.createViewerContainer(pParent, pManagedForm);
		return mViewerContainer;
	}

	/**
	 * {@inheritDoc}
	 *
	 * Overridden to hide the unused toolbar which contains the unique Remove
	 * Section Filter action.
	 */
	@Override
	protected void createFormContent(final IManagedForm pManagedForm) {
		super.createFormContent(pManagedForm);

		// The section which contains the toolbar is the only child of the viewer container
		if (mViewerContainer != null && mViewerContainer.getChildren().length == 1
				&& mViewerContainer.getChildren()[0] instanceof Section) {

			Section vSection = (Section) mViewerContainer.getChildren()[0];

			// The toolbar which must be hidden is the third control of the found section
			if (vSection.getChildren().length == 3 && vSection.getChildren()[2] instanceof ToolBar) {
				ToolBar vToolbar = (ToolBar) vSection.getChildren()[2];
				vToolbar.setVisible(false);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * Overridden to use {@link OpenCertDiagramViewer} as the viewer of this
	 * page.
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected Class<? extends AbstractActivityExplorerViewer>[] addViewersTypeInPage() {
		return new Class[] { OpenCertDiagramViewer.class };
	}
}
