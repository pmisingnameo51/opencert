/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.pages.adapters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.window.Window;
import org.eclipse.opencert.infra.dashboard.dialogs.PackageImportDialog;
import org.eclipse.opencert.infra.dashboard.helpers.ChessPapyrusHelper;
import org.eclipse.opencert.infra.dashboard.helpers.EclipseHelper;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.infra.ui.editor.IMultiDiagramEditor;
import org.eclipse.papyrus.uml.profile.ui.dialogs.ElementImportTreeSelectionDialog.ImportSpec;
import org.eclipse.papyrus.uml.tools.importsources.PackageImportSourceDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.UMLFactory;

/**
 * A {@link HyperLinkAdapter} to open the {@link PackageImportDialog} dialog.
 */
public class OpenImportUserModelDialogHyperLinkAdapter extends AbstractOpenDialogHyperLinkAdapter {

	/**
	 * Default constructor.
	 */
	public OpenImportUserModelDialogHyperLinkAdapter() {
		super();
	}

	/**
	 * Open a dialog when the link is pressed.
	 */
	@Override
	protected void openDialog() {
		// Retrieve shell instance
		Shell vShell = EclipseHelper.getActiveShell();

		// Get the first Papyrus diagram editor
		IMultiDiagramEditor vPapyrusEditor = EclipseHelper.getFirstPapyrusDiagramEditorPart();

		// Get the root model
		Model vRootModel = ChessPapyrusHelper.INSTANCE.getUmlModelFromDiagramEditor(vPapyrusEditor);

		if (Objects.nonNull(vRootModel)) {
			Map<String, String> vExtensionFilters = new LinkedHashMap<String, String>();
			vExtensionFilters.put("*.uml", "UML (*.uml)"); //$NON-NLS-1$
			vExtensionFilters.put("*.profile.uml", "UML Profiles (*.profile.uml)"); //$NON-NLS-1$ //$NON-NLS-2$
			vExtensionFilters.put("*", "All (*)"); //$NON-NLS-1$ //$NON-NLS-2$

			Collection<EObject> vSelections = new ArrayList<>();
			vSelections.add(vRootModel);

			Collection<Package> vPackages = PackageImportSourceDialog.open(vShell, "Select the models to import", //$NON-NLS-1$
					vSelections, vExtensionFilters);

			if (Objects.nonNull(vPackages) && !vPackages.isEmpty()) {
				PackageImportDialog vPackageImportDialog = new PackageImportDialog(vShell, vPackages);

				if (Window.OK == vPackageImportDialog.open()) {
					Collection<ImportSpec<Package>> vResult = vPackageImportDialog.getResult();

					for (ImportSpec<Package> vResultElement : vResult) {
						Package vSelectedPackage = vResultElement.getElement();
						switch (vResultElement.getAction()) {
						case COPY:
							handleCopyPackage(vSelectedPackage, vRootModel);
							break;
						case IMPORT:
							handleImportPackage(vSelectedPackage, vRootModel);
							break;
						default:
							handleLoadPackage(vSelectedPackage, vRootModel);
							break;
						}
					}

					// Then goto the active Papyrus editor
					EclipseHelper.getActiveWorkbenchPage().activate(vPapyrusEditor);
				}
			}
		} else {
			// Otherwise, no Chess graphical editor is already opened, ask user to open one
			ChessPapyrusHelper.INSTANCE.openNoActivePapyrusEditorMessageBox();
		}
	}

	/**
	 * Load the Package resource into the target package.
	 *
	 * @param pPackage The selected package to import
	 * @param pTargetPackage The target package
	 */
	protected void handleLoadPackage(final Package pPackage, final EObject pTargetPackage) {
		EMFHelper.reloadIntoContext(pPackage, pTargetPackage);
	}

	/**
	 * Create a PackageImport in the target package, which refers to the selected package.
	 *
	 * @param pSelectedPackage The selected package
	 * @param pTargetPackage The target package
	 */
	protected void handleImportPackage(final Package pSelectedPackage, final EObject pTargetPackage) {
		PackageImport vPackageImport = UMLFactory.eINSTANCE.createPackageImport();

		Package vImportedPackage = EMFHelper.reloadIntoContext(pSelectedPackage, pTargetPackage);

		((Package) pTargetPackage).getPackageImports().add(vPackageImport);
		vPackageImport.setImportedPackage(vImportedPackage);
	}

	/**
	 * Create a copy of the selected package in the target package.
	 *
	 * @param pSelectedPackage The selected package
	 * @param pTargetPackage The target package
	 */
	protected void handleCopyPackage(Package pSelectedPackage, final EObject pTargetPackage) {
		// FIXME: Stereotype applications are not copied
		((Package) pTargetPackage).getNestedPackages().add(EcoreUtil.copy(pSelectedPackage));
	}
}
