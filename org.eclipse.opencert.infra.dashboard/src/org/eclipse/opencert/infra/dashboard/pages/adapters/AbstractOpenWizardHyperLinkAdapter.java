/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.pages.adapters;

import org.eclipse.amalgam.explorer.activity.ui.api.editor.ActivityExplorerEditor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.opencert.infra.dashboard.helpers.EclipseHelper;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.widgets.AbstractHyperlink;
import org.eclipse.ui.wizards.IWizardDescriptor;

/**
 * A {@link HyperlinkAdapter} to open a wizard.
 */
public abstract class AbstractOpenWizardHyperLinkAdapter extends HyperlinkAdapter {

	/** ID of the project explorer view. */
	protected static final String PROJECT_EXPLORER_VIEW_ID = "org.eclipse.ui.navigator.ProjectExplorer"; //$NON-NLS-1$

	/** ID of the page standard compliance definition. */
	private static final String STANDARD_COMPLIANCE_DEFINITION_PAGE_ID = "org.eclipse.opencert.infra.dashboard.pages.standard.compliance.definition.page"; //$NON-NLS-1$

	/** The error message when a wizard can not be opened. */
	private static final String ERROR_MESSAGE = "New wizard can not be opened"; //$NON-NLS-1$

	/**
	 * Default constructor.
	 */
	public AbstractOpenWizardHyperLinkAdapter() {
		super();
	}

	/**
	 * Open a wizard when the link is pressed.
	 *
	 * @return <code>true</code> if the wizard is opened successfully, otherwise <code>false</code>
	 */
	protected boolean openWizard() {
		boolean vResult = false;

		// Instantiate Wizard
		INewWizard vWizard = getWizard();

		if (isCorrectWizardInstance(vWizard)) {
			initWizard(vWizard);

			WizardDialog vDialog = new WizardDialog(EclipseHelper.getActiveShell(), vWizard);

			// Run it
			boolean vIsOKButtonClicked = vDialog.open() == Window.OK;

			// Refresh the page to update the diagram viewers
			if (vIsOKButtonClicked && EclipseHelper.getActiveEditor() instanceof ActivityExplorerEditor) {
				ActivityExplorerEditor vActExplorerEditor = (ActivityExplorerEditor) EclipseHelper.getActiveEditor();
				vActExplorerEditor.setActivePage(STANDARD_COMPLIANCE_DEFINITION_PAGE_ID);
			}

			vResult = true;
		}

		return vResult;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void linkActivated(final HyperlinkEvent pEvent) {
		if (!openWizard()) {
			handleError(pEvent);
		}
	}

	/**
	 * Handle error when opening a wizard.
	 *
	 * @param pEvent
	 *            The hyperlink event
	 */
	protected void handleError(final HyperlinkEvent pEvent) {
		AbstractHyperlink vWidget = (AbstractHyperlink) pEvent.widget;
		MessageDialog.openError(EclipseHelper.getActiveShell(), vWidget.getText(), ERROR_MESSAGE);
	}

	/**
	 * @return The current selection from the project explorer
	 */
	protected IStructuredSelection getCurrentSelectionFromProjectExplorer(final String pWizardID) {
		IWorkbenchWindow vWorkbenchWindow = EclipseHelper.getWorkbenchWindow();

		// Get the current selection from the project explorer view
		ISelection vSelection = vWorkbenchWindow.getSelectionService().getSelection(PROJECT_EXPLORER_VIEW_ID);
		IStructuredSelection vReturnSelection = StructuredSelection.EMPTY;

		IWizardDescriptor vWizardDescriptor = EclipseHelper.getWorkbenchWindow().getWorkbench().getNewWizardRegistry()
				.findWizard(pWizardID);

		if (vSelection instanceof IStructuredSelection) {
			vReturnSelection = vWizardDescriptor.adaptedSelection((IStructuredSelection) vSelection);
		}

		return vReturnSelection;
	}

	/**
	 * @return The wizard to open
	 */
	protected abstract INewWizard getWizard();

	/**
	 * @param pWizard
	 * @return <code>true</code> if the wizard is the right instance, <code>false</code> otherwise
	 */
	protected abstract boolean isCorrectWizardInstance(INewWizard pWizard);

	/**
	 * Initialise the given wizard.
	 *
	 * @param pWizard
	 *            The wizard to be initialised
	 */
	protected abstract void initWizard(INewWizard pWizard);
}
