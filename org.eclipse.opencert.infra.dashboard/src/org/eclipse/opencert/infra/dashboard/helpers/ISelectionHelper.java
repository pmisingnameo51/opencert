/*******************************************************************************
 * Copyright (c) 2018 ALL4TEC.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Thanh Liem PHAN (ALL4TEC) - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.infra.dashboard.helpers;

import org.eclipse.core.commands.ExecutionEvent;

/**
 * Common interface for all the selection helper implementations. This allows to
 * have a common initialisation mechanism, based on a given execution event, or
 * directly from a selection.
 */
public interface ISelectionHelper {

	/**
	 * Initialise this helper from the given event.
	 *
	 * This will explore the event source to find the related part and extract
	 * the current selection from it.
	 *
	 * @param pEvent
	 *            Execution event defining the context of this helper
	 *            initialisation
	 */
	void initFromEvent(ExecutionEvent pEvent);
}
