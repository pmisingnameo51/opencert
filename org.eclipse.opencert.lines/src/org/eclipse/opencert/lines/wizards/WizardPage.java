package org.eclipse.opencert.lines.wizards;

import java.util.Map;

import org.eclipse.epf.ui.wizards.BaseWizardPage;

/**
 * The abstract class for a Open Method Library wizard page.
 */
public abstract class WizardPage extends BaseWizardPage {

	/**
	 * Creates a new instance.
	 */
	public WizardPage(String name) {
		super(name);
	}

	/**
	 * Returns the library specific user selections.
	 */
	public abstract Map getSelections();

}
