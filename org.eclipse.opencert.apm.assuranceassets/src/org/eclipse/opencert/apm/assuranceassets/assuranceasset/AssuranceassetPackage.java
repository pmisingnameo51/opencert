/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.assuranceassets.assuranceasset;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.opencert.infra.general.general.GeneralPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetFactory
 * @model kind="package"
 * @generated
 */
public interface AssuranceassetPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "assuranceasset";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://assuranceasset/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "assuranceasset";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AssuranceassetPackage eINSTANCE = org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceassetPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetsModelImpl <em>Assurance Assets Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetsModelImpl
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceassetPackageImpl#getAssuranceAssetsModel()
	 * @generated
	 */
	int ASSURANCE_ASSETS_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSETS_MODEL__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSETS_MODEL__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSETS_MODEL__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Assurance Asset</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSETS_MODEL__ASSURANCE_ASSET = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Assurance Assets Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSETS_MODEL_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Assurance Assets Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSETS_MODEL_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetImpl <em>Assurance Asset</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetImpl
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceassetPackageImpl#getAssuranceAsset()
	 * @generated
	 */
	int ASSURANCE_ASSET = 1;

	/**
	 * The number of structural features of the '<em>Assurance Asset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSET_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Assurance Asset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.ManageableAssuranceAssetImpl <em>Manageable Assurance Asset</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.ManageableAssuranceAssetImpl
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceassetPackageImpl#getManageableAssuranceAsset()
	 * @generated
	 */
	int MANAGEABLE_ASSURANCE_ASSET = 2;

	/**
	 * The feature id for the '<em><b>Evaluation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGEABLE_ASSURANCE_ASSET__EVALUATION = ASSURANCE_ASSET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Lifecycle Event</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGEABLE_ASSURANCE_ASSET__LIFECYCLE_EVENT = ASSURANCE_ASSET_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Manageable Assurance Asset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGEABLE_ASSURANCE_ASSET_FEATURE_COUNT = ASSURANCE_ASSET_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Manageable Assurance Asset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGEABLE_ASSURANCE_ASSET_OPERATION_COUNT = ASSURANCE_ASSET_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetEventImpl <em>Assurance Asset Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetEventImpl
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceassetPackageImpl#getAssuranceAssetEvent()
	 * @generated
	 */
	int ASSURANCE_ASSET_EVENT = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSET_EVENT__ID = ASSURANCE_ASSET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSET_EVENT__NAME = ASSURANCE_ASSET_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSET_EVENT__DESCRIPTION = ASSURANCE_ASSET_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Resulting Evaluation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSET_EVENT__RESULTING_EVALUATION = ASSURANCE_ASSET_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSET_EVENT__TYPE = ASSURANCE_ASSET_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSET_EVENT__TIME = ASSURANCE_ASSET_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Assurance Asset Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSET_EVENT_FEATURE_COUNT = ASSURANCE_ASSET_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Assurance Asset Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSET_EVENT_OPERATION_COUNT = ASSURANCE_ASSET_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetEvaluationImpl <em>Assurance Asset Evaluation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetEvaluationImpl
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceassetPackageImpl#getAssuranceAssetEvaluation()
	 * @generated
	 */
	int ASSURANCE_ASSET_EVALUATION = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSET_EVALUATION__ID = GeneralPackage.NAMED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSET_EVALUATION__NAME = GeneralPackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Evaluation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSET_EVALUATION__EVALUATION = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Lifecycle Event</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSET_EVALUATION__LIFECYCLE_EVENT = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Criterion</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSET_EVALUATION__CRITERION = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Criterion Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSET_EVALUATION__CRITERION_DESCRIPTION = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Evaluation Result</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSET_EVALUATION__EVALUATION_RESULT = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Rationale</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSET_EVALUATION__RATIONALE = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Assurance Asset Evaluation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSET_EVALUATION_FEATURE_COUNT = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Assurance Asset Evaluation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_ASSET_EVALUATION_OPERATION_COUNT = GeneralPackage.NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind <em>Event Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceassetPackageImpl#getEventKind()
	 * @generated
	 */
	int EVENT_KIND = 5;


	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetsModel <em>Assurance Assets Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assurance Assets Model</em>'.
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetsModel
	 * @generated
	 */
	EClass getAssuranceAssetsModel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetsModel#getAssuranceAsset <em>Assurance Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Assurance Asset</em>'.
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetsModel#getAssuranceAsset()
	 * @see #getAssuranceAssetsModel()
	 * @generated
	 */
	EReference getAssuranceAssetsModel_AssuranceAsset();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset <em>Assurance Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assurance Asset</em>'.
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset
	 * @generated
	 */
	EClass getAssuranceAsset();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.ManageableAssuranceAsset <em>Manageable Assurance Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Manageable Assurance Asset</em>'.
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.ManageableAssuranceAsset
	 * @generated
	 */
	EClass getManageableAssuranceAsset();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.ManageableAssuranceAsset#getEvaluation <em>Evaluation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Evaluation</em>'.
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.ManageableAssuranceAsset#getEvaluation()
	 * @see #getManageableAssuranceAsset()
	 * @generated
	 */
	EReference getManageableAssuranceAsset_Evaluation();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.ManageableAssuranceAsset#getLifecycleEvent <em>Lifecycle Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Lifecycle Event</em>'.
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.ManageableAssuranceAsset#getLifecycleEvent()
	 * @see #getManageableAssuranceAsset()
	 * @generated
	 */
	EReference getManageableAssuranceAsset_LifecycleEvent();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent <em>Assurance Asset Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assurance Asset Event</em>'.
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent
	 * @generated
	 */
	EClass getAssuranceAssetEvent();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent#getResultingEvaluation <em>Resulting Evaluation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Resulting Evaluation</em>'.
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent#getResultingEvaluation()
	 * @see #getAssuranceAssetEvent()
	 * @generated
	 */
	EReference getAssuranceAssetEvent_ResultingEvaluation();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent#getType()
	 * @see #getAssuranceAssetEvent()
	 * @generated
	 */
	EAttribute getAssuranceAssetEvent_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time</em>'.
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent#getTime()
	 * @see #getAssuranceAssetEvent()
	 * @generated
	 */
	EAttribute getAssuranceAssetEvent_Time();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation <em>Assurance Asset Evaluation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assurance Asset Evaluation</em>'.
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation
	 * @generated
	 */
	EClass getAssuranceAssetEvaluation();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation#getCriterion <em>Criterion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Criterion</em>'.
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation#getCriterion()
	 * @see #getAssuranceAssetEvaluation()
	 * @generated
	 */
	EAttribute getAssuranceAssetEvaluation_Criterion();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation#getCriterionDescription <em>Criterion Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Criterion Description</em>'.
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation#getCriterionDescription()
	 * @see #getAssuranceAssetEvaluation()
	 * @generated
	 */
	EAttribute getAssuranceAssetEvaluation_CriterionDescription();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation#getEvaluationResult <em>Evaluation Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Evaluation Result</em>'.
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation#getEvaluationResult()
	 * @see #getAssuranceAssetEvaluation()
	 * @generated
	 */
	EAttribute getAssuranceAssetEvaluation_EvaluationResult();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation#getRationale <em>Rationale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rationale</em>'.
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation#getRationale()
	 * @see #getAssuranceAssetEvaluation()
	 * @generated
	 */
	EAttribute getAssuranceAssetEvaluation_Rationale();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind <em>Event Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Event Kind</em>'.
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind
	 * @generated
	 */
	EEnum getEventKind();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AssuranceassetFactory getAssuranceassetFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetsModelImpl <em>Assurance Assets Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetsModelImpl
		 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceassetPackageImpl#getAssuranceAssetsModel()
		 * @generated
		 */
		EClass ASSURANCE_ASSETS_MODEL = eINSTANCE.getAssuranceAssetsModel();

		/**
		 * The meta object literal for the '<em><b>Assurance Asset</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSURANCE_ASSETS_MODEL__ASSURANCE_ASSET = eINSTANCE.getAssuranceAssetsModel_AssuranceAsset();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetImpl <em>Assurance Asset</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetImpl
		 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceassetPackageImpl#getAssuranceAsset()
		 * @generated
		 */
		EClass ASSURANCE_ASSET = eINSTANCE.getAssuranceAsset();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.ManageableAssuranceAssetImpl <em>Manageable Assurance Asset</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.ManageableAssuranceAssetImpl
		 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceassetPackageImpl#getManageableAssuranceAsset()
		 * @generated
		 */
		EClass MANAGEABLE_ASSURANCE_ASSET = eINSTANCE.getManageableAssuranceAsset();

		/**
		 * The meta object literal for the '<em><b>Evaluation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MANAGEABLE_ASSURANCE_ASSET__EVALUATION = eINSTANCE.getManageableAssuranceAsset_Evaluation();

		/**
		 * The meta object literal for the '<em><b>Lifecycle Event</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MANAGEABLE_ASSURANCE_ASSET__LIFECYCLE_EVENT = eINSTANCE.getManageableAssuranceAsset_LifecycleEvent();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetEventImpl <em>Assurance Asset Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetEventImpl
		 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceassetPackageImpl#getAssuranceAssetEvent()
		 * @generated
		 */
		EClass ASSURANCE_ASSET_EVENT = eINSTANCE.getAssuranceAssetEvent();

		/**
		 * The meta object literal for the '<em><b>Resulting Evaluation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSURANCE_ASSET_EVENT__RESULTING_EVALUATION = eINSTANCE.getAssuranceAssetEvent_ResultingEvaluation();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSURANCE_ASSET_EVENT__TYPE = eINSTANCE.getAssuranceAssetEvent_Type();

		/**
		 * The meta object literal for the '<em><b>Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSURANCE_ASSET_EVENT__TIME = eINSTANCE.getAssuranceAssetEvent_Time();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetEvaluationImpl <em>Assurance Asset Evaluation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceAssetEvaluationImpl
		 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceassetPackageImpl#getAssuranceAssetEvaluation()
		 * @generated
		 */
		EClass ASSURANCE_ASSET_EVALUATION = eINSTANCE.getAssuranceAssetEvaluation();

		/**
		 * The meta object literal for the '<em><b>Criterion</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSURANCE_ASSET_EVALUATION__CRITERION = eINSTANCE.getAssuranceAssetEvaluation_Criterion();

		/**
		 * The meta object literal for the '<em><b>Criterion Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSURANCE_ASSET_EVALUATION__CRITERION_DESCRIPTION = eINSTANCE.getAssuranceAssetEvaluation_CriterionDescription();

		/**
		 * The meta object literal for the '<em><b>Evaluation Result</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSURANCE_ASSET_EVALUATION__EVALUATION_RESULT = eINSTANCE.getAssuranceAssetEvaluation_EvaluationResult();

		/**
		 * The meta object literal for the '<em><b>Rationale</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSURANCE_ASSET_EVALUATION__RATIONALE = eINSTANCE.getAssuranceAssetEvaluation_Rationale();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind <em>Event Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind
		 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl.AssuranceassetPackageImpl#getEventKind()
		 * @generated
		 */
		EEnum EVENT_KIND = eINSTANCE.getEventKind();

	}

} //AssuranceassetPackage
