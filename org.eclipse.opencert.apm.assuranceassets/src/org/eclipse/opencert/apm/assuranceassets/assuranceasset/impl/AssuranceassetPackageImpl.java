/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.assuranceassets.assuranceasset.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetsModel;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetFactory;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.ManageableAssuranceAsset;
import org.eclipse.opencert.infra.general.general.GeneralPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AssuranceassetPackageImpl extends EPackageImpl implements AssuranceassetPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assuranceAssetsModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assuranceAssetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass manageableAssuranceAssetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assuranceAssetEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assuranceAssetEvaluationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum eventKindEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AssuranceassetPackageImpl() {
		super(eNS_URI, AssuranceassetFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link AssuranceassetPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AssuranceassetPackage init() {
		if (isInited) return (AssuranceassetPackage)EPackage.Registry.INSTANCE.getEPackage(AssuranceassetPackage.eNS_URI);

		// Obtain or create and register package
		AssuranceassetPackageImpl theAssuranceassetPackage = (AssuranceassetPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof AssuranceassetPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new AssuranceassetPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		GeneralPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theAssuranceassetPackage.createPackageContents();

		// Initialize created meta-data
		theAssuranceassetPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAssuranceassetPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AssuranceassetPackage.eNS_URI, theAssuranceassetPackage);
		return theAssuranceassetPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssuranceAssetsModel() {
		return assuranceAssetsModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssuranceAssetsModel_AssuranceAsset() {
		return (EReference)assuranceAssetsModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssuranceAsset() {
		return assuranceAssetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getManageableAssuranceAsset() {
		return manageableAssuranceAssetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getManageableAssuranceAsset_Evaluation() {
		return (EReference)manageableAssuranceAssetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getManageableAssuranceAsset_LifecycleEvent() {
		return (EReference)manageableAssuranceAssetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssuranceAssetEvent() {
		return assuranceAssetEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssuranceAssetEvent_ResultingEvaluation() {
		return (EReference)assuranceAssetEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssuranceAssetEvent_Type() {
		return (EAttribute)assuranceAssetEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssuranceAssetEvent_Time() {
		return (EAttribute)assuranceAssetEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssuranceAssetEvaluation() {
		return assuranceAssetEvaluationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssuranceAssetEvaluation_Criterion() {
		return (EAttribute)assuranceAssetEvaluationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssuranceAssetEvaluation_CriterionDescription() {
		return (EAttribute)assuranceAssetEvaluationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssuranceAssetEvaluation_EvaluationResult() {
		return (EAttribute)assuranceAssetEvaluationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssuranceAssetEvaluation_Rationale() {
		return (EAttribute)assuranceAssetEvaluationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getEventKind() {
		return eventKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssuranceassetFactory getAssuranceassetFactory() {
		return (AssuranceassetFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		assuranceAssetsModelEClass = createEClass(ASSURANCE_ASSETS_MODEL);
		createEReference(assuranceAssetsModelEClass, ASSURANCE_ASSETS_MODEL__ASSURANCE_ASSET);

		assuranceAssetEClass = createEClass(ASSURANCE_ASSET);

		manageableAssuranceAssetEClass = createEClass(MANAGEABLE_ASSURANCE_ASSET);
		createEReference(manageableAssuranceAssetEClass, MANAGEABLE_ASSURANCE_ASSET__EVALUATION);
		createEReference(manageableAssuranceAssetEClass, MANAGEABLE_ASSURANCE_ASSET__LIFECYCLE_EVENT);

		assuranceAssetEventEClass = createEClass(ASSURANCE_ASSET_EVENT);
		createEReference(assuranceAssetEventEClass, ASSURANCE_ASSET_EVENT__RESULTING_EVALUATION);
		createEAttribute(assuranceAssetEventEClass, ASSURANCE_ASSET_EVENT__TYPE);
		createEAttribute(assuranceAssetEventEClass, ASSURANCE_ASSET_EVENT__TIME);

		assuranceAssetEvaluationEClass = createEClass(ASSURANCE_ASSET_EVALUATION);
		createEAttribute(assuranceAssetEvaluationEClass, ASSURANCE_ASSET_EVALUATION__CRITERION);
		createEAttribute(assuranceAssetEvaluationEClass, ASSURANCE_ASSET_EVALUATION__CRITERION_DESCRIPTION);
		createEAttribute(assuranceAssetEvaluationEClass, ASSURANCE_ASSET_EVALUATION__EVALUATION_RESULT);
		createEAttribute(assuranceAssetEvaluationEClass, ASSURANCE_ASSET_EVALUATION__RATIONALE);

		// Create enums
		eventKindEEnum = createEEnum(EVENT_KIND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		GeneralPackage theGeneralPackage = (GeneralPackage)EPackage.Registry.INSTANCE.getEPackage(GeneralPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		assuranceAssetsModelEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		manageableAssuranceAssetEClass.getESuperTypes().add(this.getAssuranceAsset());
		assuranceAssetEventEClass.getESuperTypes().add(this.getAssuranceAsset());
		assuranceAssetEventEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		assuranceAssetEvaluationEClass.getESuperTypes().add(theGeneralPackage.getNamedElement());
		assuranceAssetEvaluationEClass.getESuperTypes().add(this.getManageableAssuranceAsset());

		// Initialize classes, features, and operations; add parameters
		initEClass(assuranceAssetsModelEClass, AssuranceAssetsModel.class, "AssuranceAssetsModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssuranceAssetsModel_AssuranceAsset(), this.getAssuranceAsset(), null, "AssuranceAsset", null, 0, -1, AssuranceAssetsModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assuranceAssetEClass, AssuranceAsset.class, "AssuranceAsset", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(manageableAssuranceAssetEClass, ManageableAssuranceAsset.class, "ManageableAssuranceAsset", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getManageableAssuranceAsset_Evaluation(), this.getAssuranceAssetEvaluation(), null, "evaluation", null, 0, -1, ManageableAssuranceAsset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getManageableAssuranceAsset_LifecycleEvent(), this.getAssuranceAssetEvent(), null, "lifecycleEvent", null, 0, -1, ManageableAssuranceAsset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assuranceAssetEventEClass, AssuranceAssetEvent.class, "AssuranceAssetEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssuranceAssetEvent_ResultingEvaluation(), this.getAssuranceAssetEvaluation(), null, "resultingEvaluation", null, 0, 1, AssuranceAssetEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssuranceAssetEvent_Type(), this.getEventKind(), "type", null, 0, 1, AssuranceAssetEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssuranceAssetEvent_Time(), ecorePackage.getEDate(), "time", null, 0, 1, AssuranceAssetEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assuranceAssetEvaluationEClass, AssuranceAssetEvaluation.class, "AssuranceAssetEvaluation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAssuranceAssetEvaluation_Criterion(), ecorePackage.getEString(), "criterion", null, 0, 1, AssuranceAssetEvaluation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssuranceAssetEvaluation_CriterionDescription(), ecorePackage.getEString(), "criterionDescription", null, 0, 1, AssuranceAssetEvaluation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssuranceAssetEvaluation_EvaluationResult(), ecorePackage.getEString(), "evaluationResult", null, 0, 1, AssuranceAssetEvaluation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssuranceAssetEvaluation_Rationale(), ecorePackage.getEString(), "rationale", null, 0, 1, AssuranceAssetEvaluation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(eventKindEEnum, EventKind.class, "EventKind");
		addEEnumLiteral(eventKindEEnum, EventKind.CREATION);
		addEEnumLiteral(eventKindEEnum, EventKind.MODIFICATION);
		addEEnumLiteral(eventKindEEnum, EventKind.EVALUATION);
		addEEnumLiteral(eventKindEEnum, EventKind.APPROVAL);
		addEEnumLiteral(eventKindEEnum, EventKind.REVOCATION);

		// Create resource
		createResource(eNS_URI);
	}

} //AssuranceassetPackageImpl
