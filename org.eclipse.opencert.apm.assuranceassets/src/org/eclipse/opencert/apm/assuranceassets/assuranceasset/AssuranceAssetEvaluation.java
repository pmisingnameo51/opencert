/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
  * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.apm.assuranceassets.assuranceasset;

import org.eclipse.opencert.infra.general.general.NamedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assurance Asset Evaluation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation#getCriterion <em>Criterion</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation#getCriterionDescription <em>Criterion Description</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation#getEvaluationResult <em>Evaluation Result</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation#getRationale <em>Rationale</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage#getAssuranceAssetEvaluation()
 * @model
 * @generated
 */
public interface AssuranceAssetEvaluation extends NamedElement, ManageableAssuranceAsset {
	/**
	 * Returns the value of the '<em><b>Criterion</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Criterion</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Criterion</em>' attribute.
	 * @see #setCriterion(String)
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage#getAssuranceAssetEvaluation_Criterion()
	 * @model
	 * @generated
	 */
	String getCriterion();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation#getCriterion <em>Criterion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Criterion</em>' attribute.
	 * @see #getCriterion()
	 * @generated
	 */
	void setCriterion(String value);

	/**
	 * Returns the value of the '<em><b>Criterion Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Criterion Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Criterion Description</em>' attribute.
	 * @see #setCriterionDescription(String)
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage#getAssuranceAssetEvaluation_CriterionDescription()
	 * @model
	 * @generated
	 */
	String getCriterionDescription();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation#getCriterionDescription <em>Criterion Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Criterion Description</em>' attribute.
	 * @see #getCriterionDescription()
	 * @generated
	 */
	void setCriterionDescription(String value);

	/**
	 * Returns the value of the '<em><b>Evaluation Result</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Evaluation Result</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluation Result</em>' attribute.
	 * @see #setEvaluationResult(String)
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage#getAssuranceAssetEvaluation_EvaluationResult()
	 * @model
	 * @generated
	 */
	String getEvaluationResult();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation#getEvaluationResult <em>Evaluation Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Evaluation Result</em>' attribute.
	 * @see #getEvaluationResult()
	 * @generated
	 */
	void setEvaluationResult(String value);

	/**
	 * Returns the value of the '<em><b>Rationale</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rationale</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rationale</em>' attribute.
	 * @see #setRationale(String)
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage#getAssuranceAssetEvaluation_Rationale()
	 * @model
	 * @generated
	 */
	String getRationale();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation#getRationale <em>Rationale</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rationale</em>' attribute.
	 * @see #getRationale()
	 * @generated
	 */
	void setRationale(String value);

} // AssuranceAssetEvaluation
