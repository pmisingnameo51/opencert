/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.infra.mappings.mapping.impl;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.opencert.infra.general.general.impl.NamedElementImpl;
import org.eclipse.opencert.infra.mappings.mapping.Map;
import org.eclipse.opencert.infra.mappings.mapping.MapGroup;
import org.eclipse.opencert.infra.mappings.mapping.MapJustification;
import org.eclipse.opencert.infra.mappings.mapping.MapKind;
import org.eclipse.opencert.infra.mappings.mapping.MappingPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Map</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.infra.mappings.mapping.impl.MapImpl#getMapGroup <em>Map Group</em>}</li>
 *   <li>{@link org.eclipse.opencert.infra.mappings.mapping.impl.MapImpl#getMapJustification <em>Map Justification</em>}</li>
 *   <li>{@link org.eclipse.opencert.infra.mappings.mapping.impl.MapImpl#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MapImpl extends NamedElementImpl implements Map {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final MapKind TYPE_EDEFAULT = MapKind.FULL;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MapImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MappingPackage.Literals.MAP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MapGroup getMapGroup() {
		return (MapGroup)eDynamicGet(MappingPackage.MAP__MAP_GROUP, MappingPackage.Literals.MAP__MAP_GROUP, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MapGroup basicGetMapGroup() {
		return (MapGroup)eDynamicGet(MappingPackage.MAP__MAP_GROUP, MappingPackage.Literals.MAP__MAP_GROUP, false, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMapGroup(MapGroup newMapGroup) {
		eDynamicSet(MappingPackage.MAP__MAP_GROUP, MappingPackage.Literals.MAP__MAP_GROUP, newMapGroup);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MapJustification getMapJustification() {
		return (MapJustification)eDynamicGet(MappingPackage.MAP__MAP_JUSTIFICATION, MappingPackage.Literals.MAP__MAP_JUSTIFICATION, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMapJustification(MapJustification newMapJustification, NotificationChain msgs) {
		msgs = eDynamicInverseAdd((InternalEObject)newMapJustification, MappingPackage.MAP__MAP_JUSTIFICATION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMapJustification(MapJustification newMapJustification) {
		eDynamicSet(MappingPackage.MAP__MAP_JUSTIFICATION, MappingPackage.Literals.MAP__MAP_JUSTIFICATION, newMapJustification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MapKind getType() {
		return (MapKind)eDynamicGet(MappingPackage.MAP__TYPE, MappingPackage.Literals.MAP__TYPE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(MapKind newType) {
		eDynamicSet(MappingPackage.MAP__TYPE, MappingPackage.Literals.MAP__TYPE, newType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MappingPackage.MAP__MAP_JUSTIFICATION:
				return basicSetMapJustification(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MappingPackage.MAP__MAP_GROUP:
				if (resolve) return getMapGroup();
				return basicGetMapGroup();
			case MappingPackage.MAP__MAP_JUSTIFICATION:
				return getMapJustification();
			case MappingPackage.MAP__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MappingPackage.MAP__MAP_GROUP:
				setMapGroup((MapGroup)newValue);
				return;
			case MappingPackage.MAP__MAP_JUSTIFICATION:
				setMapJustification((MapJustification)newValue);
				return;
			case MappingPackage.MAP__TYPE:
				setType((MapKind)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MappingPackage.MAP__MAP_GROUP:
				setMapGroup((MapGroup)null);
				return;
			case MappingPackage.MAP__MAP_JUSTIFICATION:
				setMapJustification((MapJustification)null);
				return;
			case MappingPackage.MAP__TYPE:
				setType(TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MappingPackage.MAP__MAP_GROUP:
				return basicGetMapGroup() != null;
			case MappingPackage.MAP__MAP_JUSTIFICATION:
				return getMapJustification() != null;
			case MappingPackage.MAP__TYPE:
				return getType() != TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //MapImpl
