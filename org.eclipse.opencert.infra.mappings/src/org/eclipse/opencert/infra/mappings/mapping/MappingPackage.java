/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.infra.mappings.mapping;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.opencert.infra.general.general.GeneralPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.opencert.infra.mappings.mapping.MappingFactory
 * @model kind="package"
 * @generated
 */
public interface MappingPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "mapping";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://mapping/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mapping";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MappingPackage eINSTANCE = org.eclipse.opencert.infra.mappings.mapping.impl.MappingPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.infra.mappings.mapping.impl.MapModelImpl <em>Map Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.infra.mappings.mapping.impl.MapModelImpl
	 * @see org.eclipse.opencert.infra.mappings.mapping.impl.MappingPackageImpl#getMapModel()
	 * @generated
	 */
	int MAP_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_MODEL__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_MODEL__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_MODEL__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Map Group Model</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_MODEL__MAP_GROUP_MODEL = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Map Model</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_MODEL__MAP_MODEL = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Map Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_MODEL_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Map Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_MODEL_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.infra.mappings.mapping.impl.MapGroupImpl <em>Map Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.infra.mappings.mapping.impl.MapGroupImpl
	 * @see org.eclipse.opencert.infra.mappings.mapping.impl.MappingPackageImpl#getMapGroup()
	 * @generated
	 */
	int MAP_GROUP = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_GROUP__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_GROUP__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_GROUP__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The number of structural features of the '<em>Map Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_GROUP_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Map Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_GROUP_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.infra.mappings.mapping.impl.MapImpl <em>Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.infra.mappings.mapping.impl.MapImpl
	 * @see org.eclipse.opencert.infra.mappings.mapping.impl.MappingPackageImpl#getMap()
	 * @generated
	 */
	int MAP = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP__ID = GeneralPackage.NAMED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP__NAME = GeneralPackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Map Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP__MAP_GROUP = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Map Justification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP__MAP_JUSTIFICATION = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP__TYPE = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_FEATURE_COUNT = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_OPERATION_COUNT = GeneralPackage.NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.infra.mappings.mapping.impl.MapJustificationImpl <em>Map Justification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.infra.mappings.mapping.impl.MapJustificationImpl
	 * @see org.eclipse.opencert.infra.mappings.mapping.impl.MappingPackageImpl#getMapJustification()
	 * @generated
	 */
	int MAP_JUSTIFICATION = 3;

	/**
	 * The feature id for the '<em><b>Explanation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_JUSTIFICATION__EXPLANATION = 0;

	/**
	 * The number of structural features of the '<em>Map Justification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_JUSTIFICATION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Map Justification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_JUSTIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.infra.mappings.mapping.impl.ComplianceMapImpl <em>Compliance Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.infra.mappings.mapping.impl.ComplianceMapImpl
	 * @see org.eclipse.opencert.infra.mappings.mapping.impl.MappingPackageImpl#getComplianceMap()
	 * @generated
	 */
	int COMPLIANCE_MAP = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLIANCE_MAP__ID = MAP__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLIANCE_MAP__NAME = MAP__NAME;

	/**
	 * The feature id for the '<em><b>Map Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLIANCE_MAP__MAP_GROUP = MAP__MAP_GROUP;

	/**
	 * The feature id for the '<em><b>Map Justification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLIANCE_MAP__MAP_JUSTIFICATION = MAP__MAP_JUSTIFICATION;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLIANCE_MAP__TYPE = MAP__TYPE;

	/**
	 * The number of structural features of the '<em>Compliance Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLIANCE_MAP_FEATURE_COUNT = MAP_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Compliance Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLIANCE_MAP_OPERATION_COUNT = MAP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.infra.mappings.mapping.impl.EquivalenceMapImpl <em>Equivalence Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.infra.mappings.mapping.impl.EquivalenceMapImpl
	 * @see org.eclipse.opencert.infra.mappings.mapping.impl.MappingPackageImpl#getEquivalenceMap()
	 * @generated
	 */
	int EQUIVALENCE_MAP = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENCE_MAP__ID = MAP__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENCE_MAP__NAME = MAP__NAME;

	/**
	 * The feature id for the '<em><b>Map Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENCE_MAP__MAP_GROUP = MAP__MAP_GROUP;

	/**
	 * The feature id for the '<em><b>Map Justification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENCE_MAP__MAP_JUSTIFICATION = MAP__MAP_JUSTIFICATION;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENCE_MAP__TYPE = MAP__TYPE;

	/**
	 * The number of structural features of the '<em>Equivalence Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENCE_MAP_FEATURE_COUNT = MAP_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Equivalence Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENCE_MAP_OPERATION_COUNT = MAP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.infra.mappings.mapping.MapKind <em>Map Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.infra.mappings.mapping.MapKind
	 * @see org.eclipse.opencert.infra.mappings.mapping.impl.MappingPackageImpl#getMapKind()
	 * @generated
	 */
	int MAP_KIND = 6;


	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.infra.mappings.mapping.MapModel <em>Map Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Map Model</em>'.
	 * @see org.eclipse.opencert.infra.mappings.mapping.MapModel
	 * @generated
	 */
	EClass getMapModel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.infra.mappings.mapping.MapModel#getMapGroupModel <em>Map Group Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Map Group Model</em>'.
	 * @see org.eclipse.opencert.infra.mappings.mapping.MapModel#getMapGroupModel()
	 * @see #getMapModel()
	 * @generated
	 */
	EReference getMapModel_MapGroupModel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.infra.mappings.mapping.MapModel#getMapModel <em>Map Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Map Model</em>'.
	 * @see org.eclipse.opencert.infra.mappings.mapping.MapModel#getMapModel()
	 * @see #getMapModel()
	 * @generated
	 */
	EReference getMapModel_MapModel();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.infra.mappings.mapping.MapGroup <em>Map Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Map Group</em>'.
	 * @see org.eclipse.opencert.infra.mappings.mapping.MapGroup
	 * @generated
	 */
	EClass getMapGroup();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.infra.mappings.mapping.Map <em>Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Map</em>'.
	 * @see org.eclipse.opencert.infra.mappings.mapping.Map
	 * @generated
	 */
	EClass getMap();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.infra.mappings.mapping.Map#getMapGroup <em>Map Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Map Group</em>'.
	 * @see org.eclipse.opencert.infra.mappings.mapping.Map#getMapGroup()
	 * @see #getMap()
	 * @generated
	 */
	EReference getMap_MapGroup();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.opencert.infra.mappings.mapping.Map#getMapJustification <em>Map Justification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Map Justification</em>'.
	 * @see org.eclipse.opencert.infra.mappings.mapping.Map#getMapJustification()
	 * @see #getMap()
	 * @generated
	 */
	EReference getMap_MapJustification();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.infra.mappings.mapping.Map#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.opencert.infra.mappings.mapping.Map#getType()
	 * @see #getMap()
	 * @generated
	 */
	EAttribute getMap_Type();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.infra.mappings.mapping.MapJustification <em>Map Justification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Map Justification</em>'.
	 * @see org.eclipse.opencert.infra.mappings.mapping.MapJustification
	 * @generated
	 */
	EClass getMapJustification();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.infra.mappings.mapping.MapJustification#getExplanation <em>Explanation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Explanation</em>'.
	 * @see org.eclipse.opencert.infra.mappings.mapping.MapJustification#getExplanation()
	 * @see #getMapJustification()
	 * @generated
	 */
	EAttribute getMapJustification_Explanation();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.infra.mappings.mapping.ComplianceMap <em>Compliance Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Compliance Map</em>'.
	 * @see org.eclipse.opencert.infra.mappings.mapping.ComplianceMap
	 * @generated
	 */
	EClass getComplianceMap();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.infra.mappings.mapping.EquivalenceMap <em>Equivalence Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equivalence Map</em>'.
	 * @see org.eclipse.opencert.infra.mappings.mapping.EquivalenceMap
	 * @generated
	 */
	EClass getEquivalenceMap();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.opencert.infra.mappings.mapping.MapKind <em>Map Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Map Kind</em>'.
	 * @see org.eclipse.opencert.infra.mappings.mapping.MapKind
	 * @generated
	 */
	EEnum getMapKind();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MappingFactory getMappingFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.infra.mappings.mapping.impl.MapModelImpl <em>Map Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.infra.mappings.mapping.impl.MapModelImpl
		 * @see org.eclipse.opencert.infra.mappings.mapping.impl.MappingPackageImpl#getMapModel()
		 * @generated
		 */
		EClass MAP_MODEL = eINSTANCE.getMapModel();

		/**
		 * The meta object literal for the '<em><b>Map Group Model</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAP_MODEL__MAP_GROUP_MODEL = eINSTANCE.getMapModel_MapGroupModel();

		/**
		 * The meta object literal for the '<em><b>Map Model</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAP_MODEL__MAP_MODEL = eINSTANCE.getMapModel_MapModel();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.infra.mappings.mapping.impl.MapGroupImpl <em>Map Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.infra.mappings.mapping.impl.MapGroupImpl
		 * @see org.eclipse.opencert.infra.mappings.mapping.impl.MappingPackageImpl#getMapGroup()
		 * @generated
		 */
		EClass MAP_GROUP = eINSTANCE.getMapGroup();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.infra.mappings.mapping.impl.MapImpl <em>Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.infra.mappings.mapping.impl.MapImpl
		 * @see org.eclipse.opencert.infra.mappings.mapping.impl.MappingPackageImpl#getMap()
		 * @generated
		 */
		EClass MAP = eINSTANCE.getMap();

		/**
		 * The meta object literal for the '<em><b>Map Group</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAP__MAP_GROUP = eINSTANCE.getMap_MapGroup();

		/**
		 * The meta object literal for the '<em><b>Map Justification</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAP__MAP_JUSTIFICATION = eINSTANCE.getMap_MapJustification();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MAP__TYPE = eINSTANCE.getMap_Type();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.infra.mappings.mapping.impl.MapJustificationImpl <em>Map Justification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.infra.mappings.mapping.impl.MapJustificationImpl
		 * @see org.eclipse.opencert.infra.mappings.mapping.impl.MappingPackageImpl#getMapJustification()
		 * @generated
		 */
		EClass MAP_JUSTIFICATION = eINSTANCE.getMapJustification();

		/**
		 * The meta object literal for the '<em><b>Explanation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MAP_JUSTIFICATION__EXPLANATION = eINSTANCE.getMapJustification_Explanation();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.infra.mappings.mapping.impl.ComplianceMapImpl <em>Compliance Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.infra.mappings.mapping.impl.ComplianceMapImpl
		 * @see org.eclipse.opencert.infra.mappings.mapping.impl.MappingPackageImpl#getComplianceMap()
		 * @generated
		 */
		EClass COMPLIANCE_MAP = eINSTANCE.getComplianceMap();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.infra.mappings.mapping.impl.EquivalenceMapImpl <em>Equivalence Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.infra.mappings.mapping.impl.EquivalenceMapImpl
		 * @see org.eclipse.opencert.infra.mappings.mapping.impl.MappingPackageImpl#getEquivalenceMap()
		 * @generated
		 */
		EClass EQUIVALENCE_MAP = eINSTANCE.getEquivalenceMap();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.infra.mappings.mapping.MapKind <em>Map Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.infra.mappings.mapping.MapKind
		 * @see org.eclipse.opencert.infra.mappings.mapping.impl.MappingPackageImpl#getMapKind()
		 * @generated
		 */
		EEnum MAP_KIND = eINSTANCE.getMapKind();

	}

} //MappingPackage
