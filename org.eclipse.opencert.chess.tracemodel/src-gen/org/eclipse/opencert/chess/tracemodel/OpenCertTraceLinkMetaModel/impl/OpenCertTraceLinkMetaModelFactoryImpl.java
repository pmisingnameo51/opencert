/*******************************************************************************
 * Copyright (c) 2017 Intecs SpA 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OpenCertTraceLinkMetaModelFactoryImpl extends EFactoryImpl implements OpenCertTraceLinkMetaModelFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OpenCertTraceLinkMetaModelFactory init() {
		try {
			OpenCertTraceLinkMetaModelFactory theOpenCertTraceLinkMetaModelFactory = (OpenCertTraceLinkMetaModelFactory)EPackage.Registry.INSTANCE.getEFactory(OpenCertTraceLinkMetaModelPackage.eNS_URI);
			if (theOpenCertTraceLinkMetaModelFactory != null) {
				return theOpenCertTraceLinkMetaModelFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OpenCertTraceLinkMetaModelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpenCertTraceLinkMetaModelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case OpenCertTraceLinkMetaModelPackage.CONTRACT_CLAIM_LINK: return createContractClaimLink();
			case OpenCertTraceLinkMetaModelPackage.CONTRACT_ARTEFACT_LINK: return createContractArtefactLink();
			case OpenCertTraceLinkMetaModelPackage.FORMAL_PROPERTY_CLAIM_LINK: return createFormalPropertyClaimLink();
			case OpenCertTraceLinkMetaModelPackage.CONTRACT_AGREEMENT_LINK: return createContractAgreementLink();
			case OpenCertTraceLinkMetaModelPackage.ANALYSIS_CONTEXT_ARTEFACT_LINK: return createAnalysisContextArtefactLink();
			case OpenCertTraceLinkMetaModelPackage.COMPONENT_ARGUMENTATION_ELEMENT_LINK: return createComponentArgumentationElementLink();
			case OpenCertTraceLinkMetaModelPackage.REQUIREMENT_CLAIM_LINK: return createRequirementClaimLink();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContractClaimLink createContractClaimLink() {
		ContractClaimLinkImpl contractClaimLink = new ContractClaimLinkImpl();
		return contractClaimLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContractArtefactLink createContractArtefactLink() {
		ContractArtefactLinkImpl contractArtefactLink = new ContractArtefactLinkImpl();
		return contractArtefactLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalPropertyClaimLink createFormalPropertyClaimLink() {
		FormalPropertyClaimLinkImpl formalPropertyClaimLink = new FormalPropertyClaimLinkImpl();
		return formalPropertyClaimLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContractAgreementLink createContractAgreementLink() {
		ContractAgreementLinkImpl contractAgreementLink = new ContractAgreementLinkImpl();
		return contractAgreementLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnalysisContextArtefactLink createAnalysisContextArtefactLink() {
		AnalysisContextArtefactLinkImpl analysisContextArtefactLink = new AnalysisContextArtefactLinkImpl();
		return analysisContextArtefactLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentArgumentationElementLink createComponentArgumentationElementLink() {
		ComponentArgumentationElementLinkImpl componentArgumentationElementLink = new ComponentArgumentationElementLinkImpl();
		return componentArgumentationElementLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequirementClaimLink createRequirementClaimLink() {
		RequirementClaimLinkImpl requirementClaimLink = new RequirementClaimLinkImpl();
		return requirementClaimLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpenCertTraceLinkMetaModelPackage getOpenCertTraceLinkMetaModelPackage() {
		return (OpenCertTraceLinkMetaModelPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OpenCertTraceLinkMetaModelPackage getPackage() {
		return OpenCertTraceLinkMetaModelPackage.eINSTANCE;
	}

} //OpenCertTraceLinkMetaModelFactoryImpl
