/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.export.docx;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.wml.P;
import org.eclipse.opencert.webapp.reports.containers.BaseAssetComplianceStatus;
import org.eclipse.opencert.webapp.reports.containers.ComplianceDetails;
import org.eclipse.opencert.webapp.reports.containers.visitors.ComplianceDetailsTraverser;
import org.eclipse.opencert.webapp.reports.manager.BaseActivityManager;
import org.eclipse.opencert.webapp.reports.manager.BaseArtefactManager;
import org.eclipse.opencert.webapp.reports.manager.BaseRequirementManager;
import org.eclipse.opencert.webapp.reports.manager.compliance.ComplianceDetailsManager;
import org.eclipse.opencert.webapp.reports.manager.compliance.ComplianceDetailsStatusDocxVisitor;
import org.eclipse.opencert.webapp.reports.manager.compliance.ComplianceManager;
import org.eclipse.opencert.webapp.reports.manager.compliance.ComplianceStatusDataRowProcessor;
import org.eclipse.opencert.webapp.reports.util.BaselineElementType;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;

import com.vaadin.data.util.HierarchicalContainer;

public class ComplianceEstimationReportDocxGenerator implements IDocxGenerator 
{
    private final String _docTemplateBasePath;
    
    public ComplianceEstimationReportDocxGenerator(String basePath)
    {
        _docTemplateBasePath = basePath;
    }
    
    @Override
	public ByteArrayInputStream generateProjectBaselineReport(long baselineFrameworkID, long projectID) throws Docx4JException 
	{
		DocxBuilder docxBuilder = initDocxFromTemplate(baselineFrameworkID, projectID);
		
		final ComplianceManager<BaseAssetComplianceStatus> complianceManager = new ComplianceManager<BaseAssetComplianceStatus>(new ComplianceStatusDataRowProcessor(false));
		HierarchicalContainer hcBaseAssets = complianceManager.readBaseAssetDataFromDb(baselineFrameworkID);
		
		fillDocxFromComplianceData(docxBuilder, hcBaseAssets, complianceManager);

		final ByteArrayInputStream result = docxBuilder.getResult();
		return result;
	}

	private DocxBuilder initDocxFromTemplate(long baselineFrameworkID, long projectID) throws Docx4JException 
	{
	    final String templatePath = _docTemplateBasePath + File.separator + "ComplianceEstimationReportTemplate.docx";
	    final DocxBuilder docxBuilder =  new DocxBuilder(new File(templatePath), baselineFrameworkID, projectID,true);
	    
	    // ugly fix - for unknown reasons *first* usage of HEADER does not work, the subsequent usages work
	    docxBuilder.appendHeaderNL("", DocxBuilder.HeaderStyle.HEADER_5);
	    
	    return docxBuilder;
	}

	private void fillDocxFromComplianceData(DocxBuilder docxBuilder, HierarchicalContainer hcBaseAssets, ComplianceManager<BaseAssetComplianceStatus> complianceManager) 
	{
		Collection<?> allBaseAssetIDs = hcBaseAssets.getItemIds();
		Set<Object> itemIDsProcessed = new HashSet<>();
		
		fillDocxFromBaseAssetsComplianceData(docxBuilder, hcBaseAssets, allBaseAssetIDs, itemIDsProcessed, complianceManager);
	}

	private void fillDocxFromBaseAssetsComplianceData(DocxBuilder docxBuilder, HierarchicalContainer hcBaseAssets, Collection<?> allBaseAssetIDs, 
			Set<Object> itemIDsProcessed, ComplianceManager<BaseAssetComplianceStatus> complianceManager) 
	{
		for (Object itemID : allBaseAssetIDs)
		{			
			if (itemIDsProcessed.contains(itemID)) {
				continue;
			}
			if (!(itemID instanceof BaseAssetComplianceStatus)) {
				throw new IllegalArgumentException();
			}
			itemIDsProcessed.add(itemID);
			fillDocxForBaseAsset(docxBuilder, (BaseAssetComplianceStatus)itemID, complianceManager);
			Collection<?> children = hcBaseAssets.getChildren(itemID);
			if (children == null || children.size() == 0) {
				continue;
			}
			fillDocxFromBaseAssetsComplianceData(docxBuilder, hcBaseAssets, children, itemIDsProcessed, complianceManager);
		}
	}

	private void fillDocxForBaseAsset(DocxBuilder docxBuilder, BaseAssetComplianceStatus item, ComplianceManager<BaseAssetComplianceStatus> complianceManager) 
	{
		final long itemCdoID = item.getItemId();
		
		final String baselineItemName = item.getName().getValue();
		docxBuilder.appendHeaderNL(baselineItemName, DocxBuilder.HeaderStyle.HEADER_1);
		
		String itemType = complianceManager.findBaselineElementTypeById(itemCdoID);
		final ComplianceDetailsManager complianceDetailsManager = new ComplianceDetailsManager();
		List<ComplianceDetails> complianceDetailsData = null;
		
		if (BaselineElementType.BASE_ARTEFACT.equals(itemType)) 
		{
			BaseArtefactManager baseArtefactManager = (BaseArtefactManager)SpringContextHelper.getBeanFromWebappContext(BaseArtefactManager.SPRING_NAME);
			BaseArtefact baseArtefact = baseArtefactManager.getBaseArtefact(itemCdoID);
            
	        if (baseArtefact != null) {
	        	complianceDetailsData = complianceDetailsManager.readComplianceDetailsData(baseArtefact, itemCdoID, baselineItemName, true);
	        }
		}
	    if (BaselineElementType.BASE_ACTIVITY.equals(itemType)) 
	    {
	    	BaseActivityManager baseActivityManager = (BaseActivityManager)SpringContextHelper.getBeanFromWebappContext(BaseActivityManager.SPRING_NAME);
	        BaseActivity baseActivity = baseActivityManager.getBaseActivity(itemCdoID);
	          
	        if (baseActivity != null) {
	        	complianceDetailsData = complianceDetailsManager.readComplianceDetailsData(baseActivity, itemCdoID, baselineItemName, true);
	        }
	    }
	    if (BaselineElementType.BASE_REQUIREMENT.equals(itemType)) 
	    {
            BaseRequirementManager baseRequirementManager = (BaseRequirementManager)SpringContextHelper.getBeanFromWebappContext(BaseRequirementManager.SPRING_NAME);
            BaseRequirement baseRequirement = baseRequirementManager.getBaseRequirement(itemCdoID);
            
            if (baseRequirement != null) {
                complianceDetailsData = complianceDetailsManager.readComplianceDetailsData(baseRequirement, itemCdoID, baselineItemName, true);
            }
        }
	    
	    if (complianceDetailsData != null) {
	    	appendComplianceDetailsBaseAsset(complianceDetailsData, docxBuilder);
	    }
	}

	private void appendComplianceDetailsBaseAsset(List<ComplianceDetails> complianceDetailsAll, DocxBuilder docxBuilder) 
	{
		if (complianceDetailsAll == null || complianceDetailsAll.size() == 0) {
			final String txt = "There is no compliace evidence for this safety standard requirement.";
			final P p = docxBuilder.startParagraph();
			docxBuilder.appendNL(p);
			docxBuilder.appendText(p, txt, EnumSet.of(DocxBuilder.FontStyle.ITALIC, DocxBuilder.FontStyle.BOLD), DocxBuilder.FontColor.RED);
			docxBuilder.appendNL(p);
			docxBuilder.finishParagraph(p);
			return;
		}

		final ComplianceDetailsStatusDocxVisitor docxVisitor = new ComplianceDetailsStatusDocxVisitor(docxBuilder);
		ComplianceDetailsTraverser<Integer> cdTraverser = new ComplianceDetailsTraverser<Integer>(docxVisitor);
		cdTraverser.traverseComplianceDetails(complianceDetailsAll);
	}

	@Override
	public String getReportName() {
		
		return "ComplianceReport_";
	}

	@Override
	public InputStream generateRefFrameworkToRefFrameworkReport(
			long frombaselineFrameworkID, long tobaselineFrameworkID)
			throws Docx4JException {
		// TODO Auto-generated method stub
		return null;
	}
}
