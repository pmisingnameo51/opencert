/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.util;

import java.util.Collection;

import com.vaadin.ui.ComboBox;

public class VaadinUtil 
{
	private VaadinUtil()
	{
	}

	public static void initComboBox(ComboBox comboBox)
	{
		if (comboBox == null) {
			throw new IllegalArgumentException("Null comboBox passed as argument.");
		}
		comboBox.setImmediate(true);
        comboBox.setNullSelectionAllowed(false);
        comboBox.setInvalidAllowed(false);   
        comboBox.setTextInputAllowed(false);
        
        Collection<?> items = comboBox.getItemIds();
        if (items != null && items.size() > 0) {
        	comboBox.select(items.iterator().next());
        }
	}
}
