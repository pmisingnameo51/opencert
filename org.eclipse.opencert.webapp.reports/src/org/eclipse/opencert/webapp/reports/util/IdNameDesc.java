/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.util;

import java.util.LinkedList;
import java.util.List;

public class IdNameDesc
{
    public final long id;
    public final String name;
    public final String desc;

    public final List<Long> relatedIDs;
    public final Long oldCDOId;
    
    public IdNameDesc(long i, String n, String d)
    {
        this(i, n, d, new LinkedList<>(), -1l);
    }
    
    public IdNameDesc(long i, String n, String d, List<Long> relIDs, Long oldCDOId)
    {
        id = i;
        name = n;
        desc = d;
        relatedIDs = new LinkedList<>();
        if (relIDs != null) {
            relIDs.forEach(e -> relatedIDs.add(e));
        }
        this.oldCDOId = oldCDOId;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((desc == null) ? 0 : desc.hashCode());
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((oldCDOId == null) ? 0 : oldCDOId.hashCode());
        result = prime * result + ((relatedIDs == null) ? 0 : relatedIDs.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        IdNameDesc other = (IdNameDesc) obj;
        if (desc == null) {
            if (other.desc != null)
                return false;
        } else if (!desc.equals(other.desc))
            return false;
        if (id != other.id)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (oldCDOId == null) {
            if (other.oldCDOId != null)
                return false;
        } else if (!oldCDOId.equals(other.oldCDOId))
            return false;
        if (relatedIDs == null) {
            if (other.relatedIDs != null)
                return false;
        } else if (!relatedIDs.equals(other.relatedIDs))
            return false;
        return true;
    }
    
    
}
