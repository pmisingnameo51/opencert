/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.containers;

import com.vaadin.ui.Button;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;

public class BaseAssetComplianceNumber
{
    private long itemId;
    
    private Image typeImage;
    
    private Label name;
    
    private Button fullyCompliant;  
    
    private Button partiallyCompliant;
    
    private BaseAssetComplianceNumber parentDataRow = null;
    
    public BaseAssetComplianceNumber(long itemId, Image typeImage, Label name, BaseAssetComplianceNumber parentDataRow)
    {
        this.itemId = itemId;
        this.typeImage = typeImage;
        this.name = name;
        this.parentDataRow = parentDataRow;
    }
    
    public long getItemId()
    {
        return itemId;
    }

    public void setItemId(long itemId)
    {
        this.itemId = itemId;
    }

    public Image getType()
    {
        return typeImage;
    }

    public void setType(Image type)
    {
        this.typeImage = type;
    }

    public Label getName()
    {
        return name;
    }

    public void setName(Label name)
    {
        this.name = name;
    }

    public Button getFullyCompliant()
    {
        return fullyCompliant;
    }

    public void setFullyCompliant(Button fullyCompliant)
    {
        this.fullyCompliant = fullyCompliant;
    }

    public Button getPartiallyCompliant()
    {
        return partiallyCompliant;
    }

    public void setPartiallyCompliant(Button partiallyCompliant)
    {
        this.partiallyCompliant = partiallyCompliant;
    }

    public BaseAssetComplianceNumber getParent()
    {
        return parentDataRow;
    }

    public void setParent(BaseAssetComplianceNumber parent)
    {
        this.parentDataRow = parent;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((fullyCompliant == null) ? 0 : fullyCompliant.hashCode());
        result = prime * result + (int) (itemId ^ (itemId >>> 32));
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((parentDataRow == null) ? 0 : parentDataRow.hashCode());
        result = prime
                * result
                + ((partiallyCompliant == null) ? 0 : partiallyCompliant
                        .hashCode());
        result = prime * result + ((typeImage == null) ? 0 : typeImage.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BaseAssetComplianceNumber other = (BaseAssetComplianceNumber) obj;
        if (fullyCompliant == null) {
            if (other.fullyCompliant != null)
                return false;
        } else if (!fullyCompliant.equals(other.fullyCompliant))
            return false;
        if (itemId != other.itemId)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (parentDataRow == null) {
            if (other.parentDataRow != null)
                return false;
        } else if (!parentDataRow.equals(other.parentDataRow))
            return false;
        if (partiallyCompliant == null) {
            if (other.partiallyCompliant != null)
                return false;
        } else if (!partiallyCompliant.equals(other.partiallyCompliant))
            return false;
        if (typeImage == null) {
            if (other.typeImage != null)
                return false;
        } else if (!typeImage.equals(other.typeImage))
            return false;
        return true;
    } 

    @Override
    public String toString()
    {
        return "BaseAssetComplianceNumber [itemId=" + itemId + ", type=" + typeImage
                + ", name=" + name + ", fullyCompliant=" + fullyCompliant
                + ", partiallyCompliant=" + partiallyCompliant + ", parent="
                + parentDataRow + "]";
    }
}


