/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.containers;

import org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement;

import com.vaadin.ui.Button;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;

public class BaseAssetComplianceStatus
{
    private long itemId;

    private Image img;

    private Label name;

    private Label status;
    
    private Label iaStatus;
    
    private boolean isExternalToolResultColumnClicked = false;
    
    private Button externalToolResult;

    private BaseAssetComplianceStatus parent = null;

    private BaseAssurableElement baseAssurableElement = null;

    public BaseAssetComplianceStatus(long itemId, Image img, Label nameLabel, Button externalToolResultButton)
    {
        this.itemId = itemId;
        this.img = img;
        this.name = nameLabel;
        this.externalToolResult = externalToolResultButton;
    }

    public long getItemId()
    {
        return itemId;
    }

    public void setItemId(long itemId)
    {
        this.itemId = itemId;
    }

    public Image getType()
    {
        return img;
    }

    public void setType(Image type)
    {
        this.img = type;
    }

    public Label getName()
    {
        return name;
    }

    public void setName(Label name)
    {
        this.name = name;
    }

    public BaseAssetComplianceStatus getParent()
    {
        return parent;
    }

    public void setParent(BaseAssetComplianceStatus parent)
    {
        this.parent = parent;
    }

    public BaseAssurableElement getBaseAssurableElement()
    {
        return baseAssurableElement;
    }

    public void setBaseAssurableElement(BaseAssurableElement baseAssurableElement)
    {
        this.baseAssurableElement = baseAssurableElement;
    }

    public Label getStatus()
    {
        return status;
    }

    public void setStatus(Label status)
    {
        this.status = status;
    }
    
    
    public Label getIAStatus()
    {
        return iaStatus;
    }

    public Button getExternalToolResult()
    {
        return externalToolResult;
    }

    public void setExternalToolResult(Button externalToolResult)
    {
        this.externalToolResult = externalToolResult;
    }
        
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((baseAssurableElement == null) ? 0 : baseAssurableElement.hashCode());
        result = prime * result + ((externalToolResult == null) ? 0 : externalToolResult.hashCode());
        result = prime * result + (int) (itemId ^ (itemId >>> 32));
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((img == null) ? 0 : img.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof BaseAssetComplianceStatus))
            return false;
        BaseAssetComplianceStatus other = (BaseAssetComplianceStatus) obj;
        if (baseAssurableElement == null) {
            if (other.baseAssurableElement != null)
                return false;
        } else if (!baseAssurableElement.equals(other.baseAssurableElement))
            return false;
        if (externalToolResult == null) {
            if (other.externalToolResult != null)
                return false;
        } else if (!externalToolResult.equals(other.externalToolResult))
            return false;
        if (itemId != other.itemId)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (parent == null) {
            if (other.parent != null)
                return false;
        } else if (!parent.equals(other.parent))
            return false;
        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status))
            return false;
        if (img == null) {
            if (other.img != null)
                return false;
        } else if (!img.equals(other.img))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "BaseAssetComplianceStatus [itemId=" + itemId + ", type=" + img + ", name=" + name + ", status=" + status + ", externalToolResult="
                + externalToolResult + ", parent=" + parent + ", baseArtefact=" + baseAssurableElement + "]";
    }

    public boolean isExternalToolResultColumnClicked()
    {
        return isExternalToolResultColumnClicked;
    }

    public void setExternalToolResultColumnClicked(boolean isExternalToolResultColumnClicked)
    {
        this.isExternalToolResultColumnClicked = isExternalToolResultColumnClicked;
    }

    public void setIAStatus(Label iaStatusLabel)
    {
        this.iaStatus = iaStatusLabel;
    }
}
