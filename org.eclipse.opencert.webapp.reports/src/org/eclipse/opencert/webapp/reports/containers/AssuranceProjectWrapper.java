/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.containers;

import java.util.List;

import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;

public class AssuranceProjectWrapper
{
    private Long id;

    private String name;

    private String description;

    private List<FrameworkWrapper> baseFrameworks;

    private AssuranceProject assuranceProject;

    public AssuranceProjectWrapper()
    {

    }

    public AssuranceProjectWrapper(Long id, String name, String description, List<FrameworkWrapper> baseFrameworks, AssuranceProject assuranceProject)
    {
        super();
        this.id = id;
        this.name = name;
        this.description = description;
        this.baseFrameworks = baseFrameworks;
        this.assuranceProject = assuranceProject;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public List<FrameworkWrapper> getBaseFrameworks()
    {
        return baseFrameworks;
    }

    public void setBaseFrameworks(List<FrameworkWrapper> baseFrameworks)
    {
        this.baseFrameworks = baseFrameworks;
    }

    public AssuranceProject getAssuranceProject()
    {
        return assuranceProject;
    }

    public void setAssuranceProject(AssuranceProject assuranceProject)
    {
        this.assuranceProject = assuranceProject;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((assuranceProject == null) ? 0 : assuranceProject.hashCode());
        result = prime * result + ((baseFrameworks == null) ? 0 : baseFrameworks.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AssuranceProjectWrapper other = (AssuranceProjectWrapper) obj;
        if (assuranceProject == null) {
            if (other.assuranceProject != null)
                return false;
        } else if (!assuranceProject.equals(other.assuranceProject))
            return false;
        if (baseFrameworks == null) {
            if (other.baseFrameworks != null)
                return false;
        } else if (!baseFrameworks.equals(other.baseFrameworks))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "AssuranceProjectWrapper [id=" + id + ", name=" + name + ", baseFrameworks=" + baseFrameworks + ", assuranceProject=" + assuranceProject + "]";
    }

}
