/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.webapp;

import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.listeners.ReportChangeNotifier;
import org.eclipse.opencert.webapp.reports.security.SecurityErrorHandler;
import org.eclipse.opencert.webapp.reports.view.common.GUIMode;
import org.eclipse.opencert.webapp.reports.view.common.ProjectAndMenuPanel;
import org.eclipse.opencert.webapp.reports.view.common.ProjectComboWrapper;
import org.eclipse.opencert.webapp.reports.view.common.ReportContainer;
import org.eclipse.opencert.webapp.reports.view.common.ReportProvider;
import org.eclipse.opencert.webapp.reports.view.common.TopPanel;
import org.eclipse.opencert.webapp.reports.view.url.URLParamHandler;
import org.eclipse.opencert.webapp.reports.view.url.URLParamsContainer;

import com.vaadin.annotations.Theme;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@Theme("opencerttheme")
//@JavaScript({"http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"})
public class OpencertApplication extends UI
{
    @Override
    protected void init(VaadinRequest request)
    {   
        URLParamsContainer urlParams = URLParamHandler.processURLParams(request);
        
    	VerticalLayout mainLayout = new VerticalLayout();
    	mainLayout.setMargin(true);
    	mainLayout.setWidth("100%");
    	mainLayout.setHeight("100%");
    	mainLayout.setStyleName("mainLayout");
    	setContent(mainLayout);
    	setStyleName("mainPanel");
    	
    	if (urlParams.isCorrect()) 
    	{
    	    setupMainPageLayout(mainLayout, urlParams);
    	} else {
    	    setupErrorPageLayout(mainLayout, urlParams);
    	}
    	
    	UI.getCurrent().setErrorHandler(new SecurityErrorHandler());
    }

    private void setupErrorPageLayout(VerticalLayout mainLayout, URLParamsContainer urlParamsContainer)
    {
        if (urlParamsContainer.isCorrect()) {
            throw new IllegalArgumentException("Correct urlParams");
        }
        if (urlParamsContainer.getErrorMessage() == null) {
            throw new IllegalArgumentException("Null error msg");
        }
        final Label title = new Label("Illegal parameters passed.");
        title.setStyleName("ErrorPageMessageHeader");
        final Label msg = new Label(urlParamsContainer.getErrorMessage());
        msg.setContentMode(ContentMode.PREFORMATTED);
        msg.setStyleName("ErrorPageMessageText");
        
        mainLayout.addComponent(title);
        mainLayout.addComponent(msg);
        final String generalURL = URLParamHandler.generateWebURL(urlParamsContainer, GUIMode.NORMAL);
        Link link = new Link(generalURL, new ExternalResource(generalURL));
        mainLayout.addComponent(link);
        
        Label spacer = new Label();
        mainLayout.addComponent(spacer);
        mainLayout.setExpandRatio(spacer, 1.0f);
    }

    private void setupMainPageLayout(VerticalLayout mainLayout, URLParamsContainer urlParamsContainer)
    {
        final ProjectComboWrapper projectComboWrapper = new ProjectComboWrapper(urlParamsContainer.getDetectedProjectName(), urlParamsContainer.getGUIMode());
        final ProjectAndMenuPanel projectAndMenuPanel = new ProjectAndMenuPanel(projectComboWrapper);
        
        final ReportProvider reportProvider = new ReportProvider(urlParamsContainer);
        projectAndMenuPanel.setReportProvider(reportProvider);
        reportProvider.setProjectComboSelector((IProjectComboSelector)projectComboWrapper);
        
        final ReportContainer reportContainer = new ReportContainer(projectComboWrapper, urlParamsContainer);
        
        ReportChangeNotifier reportChangeNotifier = new ReportChangeNotifier();
        reportChangeNotifier.addListener(reportContainer);
        reportChangeNotifier.addListener(projectComboWrapper);
        reportChangeNotifier.addListener(urlParamsContainer);
        
        projectAndMenuPanel.setReportChangeNotifier(reportChangeNotifier);
        final Component topPanel = new TopPanel(reportProvider, reportChangeNotifier, urlParamsContainer);
        
        projectComboWrapper.addProjectChangeListener(reportContainer);
        projectComboWrapper.addProjectChangeListener(urlParamsContainer);
        reportContainer.reportChanged(reportProvider.getInitialReport(urlParamsContainer.getReportID(), urlParamsContainer.getGUIMode()));
        AssuranceProjectWrapper selectedProject = projectComboWrapper.getSelectedProject();
        reportContainer.projectChanged(selectedProject);
        
        if (urlParamsContainer.getGUIMode() == GUIMode.NORMAL)
        {
            mainLayout.addComponent(topPanel);
            mainLayout.addComponent(projectAndMenuPanel);
            mainLayout.addComponent(reportContainer);
        } else if (urlParamsContainer.getGUIMode() == GUIMode.COMPACT)
        {
            mainLayout.addComponent(reportContainer);
        }
        mainLayout.setExpandRatio(reportContainer, 1.0f);
    }
}