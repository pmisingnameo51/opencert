/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.webapp.reports.containers.ArgumentationWrapper;
import org.eclipse.opencert.webapp.reports.view.common.ReportID;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.sam.arg.arg.ArgumentElement;
import org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation;
import org.eclipse.opencert.sam.arg.arg.Argumentation;
import org.eclipse.opencert.sam.arg.arg.ArgumentationElement;
import org.eclipse.opencert.sam.arg.arg.AssertedEvidence;
import org.eclipse.opencert.sam.arg.arg.AssertedInference;
import org.eclipse.opencert.sam.arg.arg.Case;
import org.eclipse.opencert.sam.arg.arg.Claim;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;

import com.vaadin.data.Item;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;

public class ArgumentationItemsPage extends ItemsPage {
	private static final String TITLE = "Argumentation Items";

	private static final String CASE_RESOURCE = "CaseResource";
	private static final String CASE_RESOURCE_LABEL = "Case Resource Name";
	private static final String CLAIMS = "Claims";
	private static final String CLAIMS_LABEL = "Number of All Claims";
	private static final String PUBLIC_CLAIMS = "PublicClaims";
	private static final String PUBLIC_CLAIMS_LABEL = "Number of Public Claims";
	private static final String ARGUMENTATIONS = "Argumentations";
	private static final String ARGUMENTATIONS_LABEL = "Number of Argumentations";
	private static final String ARGUMENT_ELEMENT_CITATION = "ArgumentElementCitation";
	private static final String ARGUMENT_ELEMENT_CITATION_LABEL = "Number of ArgumentElementCitation";
	private static final String TO_BE_SUPPORTED_CLAIMS = "ToBeSupportedClaims";
	private static final String TO_BE_SUPPORTED_CLAIMS_LABEL = "Number of Claims To Be Supported";
	private static final String TO_BE_INSTANTIATED_CLAIMS = "ToBeInstantiatedClaims";
	private static final String TO_BE_INSTANTIATED_CLAIMS_LABEL = "Number of Claims To Be Instantiated";
	private static final String CLAIMS_WITH_NO_EVIDENCE_ASSOCIATED = "ClaimsWithNoEvidenceAssociated";
	private static final String CLAIMS_WITH_NO_EVIDENCE_ASSOCIATED_LABEL = "Number of Claims With No Evidence Associated";
	
	@Override
	public ReportID getReportID() {
		return ReportID.ARGUMENTATION_ITEMS;
	}

	@Override
	public String getTitle() {
		return TITLE;
	}
	
	@Override
	protected Component addAdditionalComponent() {
		Label evidenceItemsLabel = new Label("Argumentation items:");
		evidenceItemsLabel.setStyleName("complianceDetailsTitle");
		return evidenceItemsLabel;
	}
	
	@Override
	protected HierarchicalContainer createDataContainer(AssuranceProject project) {
		HierarchicalContainer hc = createEmptyTableDataContainer();
				
		EList<AssetsPackage> assetsPackages = project.getAssetsPackage();
		for (AssetsPackage assetsPackage : assetsPackages) {
			if (assetsPackage.isIsActive()) {
				EList<Case> argumentationModels = assetsPackage.getArgumentationModel();
				for (Case argumentationModel : argumentationModels) {
					ArgumentationWrapper argWrapper = buildArgumentationData(argumentationModel);
					addItemToConatiner(argWrapper, hc);
				}
			}
		}
		
		return hc;
	}
	
	private ArgumentationWrapper buildArgumentationData(Case argumentationModel) {
		String resourceName = argumentationModel.cdoResource().getName();
		int publicClaimsCount = 0;
		int argumentElementCitationCount = 0;
		int toBeSupportedClaimsCount = 0;
		int toBeInstantiatedClaimsCount = 0;
		List<Long> claimIds = new ArrayList<Long>();
		List<Long> sourceAssertedInferenceIds = new ArrayList<Long>();
		List<Long> sourceAssertedEvidenceIds = new ArrayList<Long>();
		
		EList<ArgumentElement> arguments = argumentationModel.getArgument();
		for (ArgumentElement argument : arguments) {
			if (argument instanceof Claim) {
				claimIds.add(CDOStorageUtil.getCDOId(argument));
				Claim claim = (Claim)argument;
				if (claim.getPublic()) {
					publicClaimsCount++;
				}
				if (claim.getToBeSupported()) {
					toBeSupportedClaimsCount++;
				}
				if (claim.getToBeInstantiated()) {
					toBeInstantiatedClaimsCount++;
				}
			} else if (argument instanceof ArgumentElementCitation) {
				argumentElementCitationCount++;
			} else if (argument instanceof AssertedInference) {
				buildSourceAssertedInferenceIds(sourceAssertedInferenceIds, argument);
			} else if (argument instanceof AssertedEvidence) {
				buildSourceAssertedEvidenceIds(sourceAssertedEvidenceIds, argument);
			}
		}
		int claimsWithNoEvidenceAssociated = countClaimsWithNoEvidenceAssociated(claimIds, sourceAssertedInferenceIds, sourceAssertedEvidenceIds);
		
		EList<Argumentation> argumentations = argumentationModel.getArgumentation();
		int argumentationCount = argumentations.size();
		
		ArgumentationWrapper argWrapper = new ArgumentationWrapper(createLabel(resourceName), 
				createButton(claimIds.size()), createButton(publicClaimsCount),
				createButton(argumentationCount), createButton(argumentElementCitationCount),
				createButton(toBeSupportedClaimsCount), createButton(toBeInstantiatedClaimsCount),
				createButton(claimsWithNoEvidenceAssociated));
		
		return argWrapper;
	}
	
	private void buildSourceAssertedInferenceIds(List<Long> sourceAssertedInferenceIds, ArgumentElement argument) {
		AssertedInference assertedInf = (AssertedInference)argument;
		EList<ArgumentationElement> sources = assertedInf.getSource();
		EList<ArgumentationElement> targets = assertedInf.getTarget();
		if (targets != null && targets.size() > 0) {
			for (ArgumentationElement source: sources) {
				long sourceId = CDOStorageUtil.getCDOId(source);
				if (!sourceAssertedInferenceIds.contains(sourceId)) {
					sourceAssertedInferenceIds.add(sourceId);
				}
			}
		}
	}
	
	private void buildSourceAssertedEvidenceIds(List<Long> sourceAssertedEvidenceIds, ArgumentElement argument) {
		AssertedEvidence assertedEvid = (AssertedEvidence)argument;
		EList<ArgumentElement> sources = assertedEvid.getSource();
		EList<ArgumentElement> targets = assertedEvid.getTarget();
		if (targets != null && targets.size() > 0) {
			for (ArgumentElement source: sources) {
				long sourceId = CDOStorageUtil.getCDOId(source);
				if (!sourceAssertedEvidenceIds.contains(sourceId)) {
					sourceAssertedEvidenceIds.add(sourceId);
				}
			}
		}
	}
	
	private int countClaimsWithNoEvidenceAssociated(List<Long> claimsIds, List<Long> sourceAssertedInferenceIds, List<Long> sourceAssertedEvidenceIds) {
		int claimsWithoutEvidencesCount = 0;
		for (Long claimsId : claimsIds) {
			if (!sourceAssertedInferenceIds.contains(claimsId) && !sourceAssertedEvidenceIds.contains(claimsId)) {
				claimsWithoutEvidencesCount++;
			}
		}
	
		return claimsWithoutEvidencesCount;
	}
	
	private void addItemToConatiner(ArgumentationWrapper argWrapper, HierarchicalContainer hc) {
		Item item =  hc.addItem(argWrapper);
		mergePropertiesWithData(item, argWrapper);
		hc.setChildrenAllowed(argWrapper, false);
	}
	
    @SuppressWarnings("unchecked")
	private void mergePropertiesWithData(Item item, ArgumentationWrapper argWrapper) {
    	item.getItemProperty(CASE_RESOURCE).setValue(argWrapper.getCaseResource());
    	item.getItemProperty(CLAIMS).setValue(argWrapper.getClaimsCount());
        item.getItemProperty(PUBLIC_CLAIMS).setValue(argWrapper.getPublicClaimsCount());
        item.getItemProperty(ARGUMENTATIONS).setValue(argWrapper.getArgumentationCount());
        item.getItemProperty(ARGUMENT_ELEMENT_CITATION).setValue(argWrapper.getArgumentElementCitationCount());
        item.getItemProperty(TO_BE_SUPPORTED_CLAIMS).setValue(argWrapper.getToBeSupportedClaimsCount());
        item.getItemProperty(TO_BE_INSTANTIATED_CLAIMS).setValue(argWrapper.getToBeInstantiatedClaimsCount());
        item.getItemProperty(CLAIMS_WITH_NO_EVIDENCE_ASSOCIATED).setValue(argWrapper.getClaimsWithNoEvidenceAssociatedCount());
    }
        
    @Override
    protected void addColumnHeaders() {
		itemsTable.setColumnHeader(CASE_RESOURCE, CASE_RESOURCE_LABEL);
		itemsTable.setColumnHeader(CLAIMS, CLAIMS_LABEL);
		itemsTable.setColumnHeader(PUBLIC_CLAIMS, PUBLIC_CLAIMS_LABEL);
		itemsTable.setColumnHeader(ARGUMENTATIONS, ARGUMENTATIONS_LABEL);
		itemsTable.setColumnHeader(ARGUMENT_ELEMENT_CITATION, ARGUMENT_ELEMENT_CITATION_LABEL);
		itemsTable.setColumnHeader(TO_BE_SUPPORTED_CLAIMS, TO_BE_SUPPORTED_CLAIMS_LABEL);
		itemsTable.setColumnHeader(TO_BE_INSTANTIATED_CLAIMS, TO_BE_INSTANTIATED_CLAIMS_LABEL);
		itemsTable.setColumnHeader(CLAIMS_WITH_NO_EVIDENCE_ASSOCIATED, CLAIMS_WITH_NO_EVIDENCE_ASSOCIATED_LABEL);
		
		itemsTable.setColumnExpandRatio(CASE_RESOURCE, 16);
		itemsTable.setColumnExpandRatio(CLAIMS, 12);
		itemsTable.setColumnExpandRatio(PUBLIC_CLAIMS, 12);
		itemsTable.setColumnExpandRatio(ARGUMENTATIONS, 12);
		itemsTable.setColumnExpandRatio(ARGUMENT_ELEMENT_CITATION, 12);
		itemsTable.setColumnExpandRatio(TO_BE_SUPPORTED_CLAIMS, 12);
		itemsTable.setColumnExpandRatio(TO_BE_INSTANTIATED_CLAIMS, 12);
		itemsTable.setColumnExpandRatio(CLAIMS_WITH_NO_EVIDENCE_ASSOCIATED, 12);
	}
	
	@Override
	protected void addContainerProperties(HierarchicalContainer hc) {
        hc.addContainerProperty(CASE_RESOURCE, Label.class, "");
        hc.addContainerProperty(CLAIMS, Button.class, "");
        hc.addContainerProperty(PUBLIC_CLAIMS, Button.class, "");
        hc.addContainerProperty(ARGUMENTATIONS, Button.class, "");
        hc.addContainerProperty(ARGUMENT_ELEMENT_CITATION, Button.class, "");
        hc.addContainerProperty(TO_BE_SUPPORTED_CLAIMS, Button.class, "");
        hc.addContainerProperty(TO_BE_INSTANTIATED_CLAIMS, Button.class, "");
        hc.addContainerProperty(CLAIMS_WITH_NO_EVIDENCE_ASSOCIATED, Button.class, "");
    }
}
