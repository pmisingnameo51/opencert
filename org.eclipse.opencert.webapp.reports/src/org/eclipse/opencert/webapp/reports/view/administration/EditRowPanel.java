/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.administration;

import org.eclipse.opencert.webapp.reports.view.common.ConfirmationWindow;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
class EditRowPanel
    extends HorizontalLayout
{
    private boolean isEditMode = false;
    
    private Button editSaveRowButton;
    private Button cancelRowButton;
    private Button deleteRowButton;

    private boolean isVolatile = false;

    EditRowPanel()
    {
        editSaveRowButton = addButton("", "", new String[] { "ProjectAdminEditSaveRowButton", "ProjectAdminRowButton", "commonButton" });
        cancelRowButton = addButton("Cancel", "Cancel the changes. [Esc]", new String[] { "ProjectAdminRowButton", "commonButton" });
        deleteRowButton = addButton("Delete", "Delete the specific Baseline Artefact.", new String[] { "ProjectAdminRowButton", "commonButton" });
        
        setViewMode();
    }

    private Button addButton(String name, String desc, String[] styles)
    {
        Button resultButton = new Button(name);
        resultButton.setDescription(desc);
        for (String s: styles)
        {
            resultButton.addStyleName(s);
        }
        addComponent(resultButton);
        return resultButton;
    }

    void setButtonListeners(BaselineElementDataRow dataRow, ProjectBaselineArtefactTable projectBaselineArtefactTable)
    {
        setListenerEditSaveRowButton(dataRow, projectBaselineArtefactTable);
        setListenerEditCancelRowButton(dataRow, projectBaselineArtefactTable);
        setListenerDeleteRowButton(dataRow, projectBaselineArtefactTable);
    }

    void setViewMode()
    {
        isEditMode = false;
        refreshButtons();
        setButtonsShortcuts(false);
    }
    
    void setEditMode()
    {
        isEditMode = true;
        refreshButtons();
    }

    void clickEditButton()
    {
        if (isEditMode) {
            return;
        }
        editSaveRowButton.click();
    }
    
    void setButtonsShortcuts(boolean bTurnOn)
    {
        if (bTurnOn) 
        {
            editSaveRowButton.setClickShortcut(KeyCode.ENTER);
            cancelRowButton.setClickShortcut(KeyCode.ESCAPE);
        } else {
            editSaveRowButton.removeClickShortcut();
            cancelRowButton.removeClickShortcut();
        }
    }
    
    private void setListenerEditSaveRowButton(BaselineElementDataRow dataRow, ProjectBaselineArtefactTable parentTable)
    {
        editSaveRowButton.addClickListener((e) -> 
        {
            if (isEditMode) 
            {
                isVolatile = false;
                if (dataRow.isChangedInGUI()) 
                {
                    parentTable.saveEditedRow(dataRow);
                } else {
                    parentTable.cancelEditedRow(dataRow);
                }
            } else {
                parentTable.editRow(dataRow);
            }
            toggleMode();
        });
    }



    private void toggleMode()
    {
        if (isEditMode) {
            setViewMode();
        } else {
            setEditMode();
        }
    }

    private void setListenerEditCancelRowButton(BaselineElementDataRow dataRow, ProjectBaselineArtefactTable parentTable)
    {
        cancelRowButton.addClickListener((e) -> 
        {
            if (isEditMode) 
            {
                if (isVolatile) {
                    parentTable.deleteRow(dataRow, false);
                } else {
                    parentTable.cancelEditedRow(dataRow);
                }
            } else {
                throw new IllegalStateException("Cancel button triggered action in View mode.");
            }
            toggleMode();
        });
    }

    private void setListenerDeleteRowButton(BaselineElementDataRow dataRow, ProjectBaselineArtefactTable parentTable)
    {
        deleteRowButton.addClickListener((e) -> 
        {
            confirmAndDeleteRow(dataRow, parentTable);
        });
    }

    public void confirmAndDeleteRow(BaselineElementDataRow dataRow, ProjectBaselineArtefactTable projectBaselineArtefactTable)
    {
        ConfirmationWindow confirmDeleteRowWindow = new ConfirmationWindow("Delete baseline artefact", "Are your sure you want to remove the selected baseline artefact?", "");
        confirmDeleteRowWindow.addYesAction(e -> 
        {
            if (!isEditMode) 
            {
                projectBaselineArtefactTable.deleteRow(dataRow, true);
            } else {
                throw new IllegalStateException("Delete button triggered action in Edit mode.");
            }
            confirmDeleteRowWindow.close();
        });
        
        if (!(UI.getCurrent().getWindows().contains(confirmDeleteRowWindow))) {
            UI.getCurrent().addWindow(confirmDeleteRowWindow);  
        }
    }
    
    private void refreshButtons()
    {
        final String editSaveCaption = (isEditMode ? "Done" : "Edit");
        editSaveRowButton.setCaption(editSaveCaption);
        
        String description = (isEditMode ? "Save the changes. [Enter]" : "Edit this Baseline Artefact. [Dbl-click]");
        editSaveRowButton.setDescription(description);
        
        cancelRowButton.setVisible(isEditMode);
        deleteRowButton.setVisible(!isEditMode);
    }

    void setVolatile2(boolean b)
    {
        isVolatile = b;
    }

    public void clickCancelEditedRow(BaselineElementDataRow dataRowToCancel)
    {
        if (!isEditMode) {
            throw new IllegalStateException();
        }
        cancelRowButton.click();
    }
}
