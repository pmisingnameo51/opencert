/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.metrics;


import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.vaadin.annotations.Theme;
import com.vaadin.data.Container.Hierarchical;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.VerticalLayout;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.manager.AssuranceProjectManager;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.pam.procspec.process.Activity;
import org.eclipse.opencert.pam.procspec.process.Participant;
import org.eclipse.opencert.pam.procspec.process.ProcessModel;




@Theme("opencerttheme")
public class ResourceEfficiencyChartsMetrics 
	extends CustomComponent

{

	private static final long serialVersionUID = -4700474738113265442L;
	private Hierarchical dataDescription;

	public ResourceEfficiencyChartsMetrics(AssuranceProjectWrapper project)
    {      
         
        VerticalLayout mainPanel = new VerticalLayout();
		
		mainPanel.setStyleName("chartsSpacing");
		mainPanel.setSpacing(true);
		
 	
		mainPanel.addComponent(resourceEfficiencyProcessMetrics(getActivitiesEvents(project)));
		
		//add mainPanel component
		setCompositionRoot(mainPanel);
		setSizeFull();
        
 
    }



    public Hierarchical getDataDescription() {
		return dataDescription;
	}




	private List<Activity> getActivitiesEvents(AssuranceProjectWrapper newProject) {
    	
    	List<Activity> activities = new ArrayList<Activity>();
    	
    	AssuranceProjectManager projectManager = 
				(AssuranceProjectManager)SpringContextHelper.getBeanFromWebappContext(AssuranceProjectManager.SPRING_NAME);
    	AssuranceProject project = projectManager.getProject(newProject.getId());
    	
    	if(project != null){
	    	EList<AssetsPackage> assetsPackages = project.getAssetsPackage();
			for (AssetsPackage assetsPackage : assetsPackages) {
				if (assetsPackage.isIsActive()) {				
					EList<ProcessModel> processModels = assetsPackage.getProcessModel();
					for (ProcessModel processModel : processModels) {
						
						/**/
						EObject eProcessModel= (EObject) processModel; 
			    		 for (Iterator<EObject> iterator = eProcessModel.eAllContents(); iterator.hasNext();) {
			        		 EObject aEObject = iterator.next();
			        		 if (aEObject instanceof Activity){
			        			 Activity activity = (Activity) aEObject;
			        			 if (activity.getStartTime()!=null) activities.add(activity);
			        		 }
			    		 }
						/**/
								
					}
				}
			}
    	}
    	
    	return activities;

	}




	private Component resourceEfficiencyProcessMetrics(List<Activity> list) {

		
		TreeTable treetable = new TreeTable("");
		
		treetable.setWidth("100%");
		treetable.addStyleName("resourceEfficiencyTable");

		// Define two columns for the built-in container
		treetable.addContainerProperty("Activity", String.class, "");
		treetable.addContainerProperty("Description",  String.class, "");

	
		Date today = new Date();
		
	
		if(!list.isEmpty()){
			for(Activity activity : list){
				
				Object act = treetable.addItem(new Object[]{"Activity ID: "+activity.getId() + " Name: " + activity.getName(), activity.getDescription()},null); 
				
				treetable.setCollapsed(act, false);
				
				if (activity.getStartTime() == null || activity.getStartTime().after(today)) {
					Object hours = treetable.addItem(new Object[]{"Hours: - ","Not Started" },null);	
					treetable.setParent(hours,act);
					treetable.setChildrenAllowed(hours, false);
				}
				
				else if (activity.getEndTime() == null) {
					Object hours = treetable.addItem(new Object[]{"Hours: "+countHours(activity.getStartTime(), today),"Still Under Development" },null);	
					treetable.setParent(hours,act);
					treetable.setChildrenAllowed(hours, false);
				}
				else if(activity.getStartTime().before(activity.getEndTime())){
					Object hours = treetable.addItem(new Object[]{"Hours: "+ countHours(activity.getStartTime(), activity.getEndTime()),""},null);	
					treetable.setParent(hours,act);
					treetable.setChildrenAllowed(hours, false);

				}
				else{
					Object hours = treetable.addItem(new Object[]{"Activity wrongly defined, cause:  ", "End Date is before Start Date"},null);	
					treetable.setParent(hours,act);
					treetable.setChildrenAllowed(hours, false);
				}
				
				Object participants = treetable.addItem(new Object[]{"Participants: ", ""},null);
				treetable.setCollapsed(participants, false);
				treetable.setParent(participants, act);
				
				List<Participant> part = activity.getParticipant();
				if(!part.isEmpty()){
					for(Participant p : part){
						Object participant = treetable.addItem(new Object[]{"ID: "+p.getId(), " Name: " + p.getName()},null); 
						treetable.setParent(participant, participants);
						treetable.setChildrenAllowed(participant, false);
					}
				}
				
			}
		}
		

		
		dataDescription = treetable.getContainerDataSource();
		return treetable;
		
		
	}




	private String countHours(Date startTime, Date endTime) {
		
		long different = (endTime==null ? new Date().getTime() :endTime.getTime()) - (startTime==null ? new Date().getTime() : startTime.getTime());
	
		long secondsInMilli = 1000;
		long minutesInMilli = secondsInMilli * 60;
		long hoursInMilli = minutesInMilli * 60;
	
		long hours = different / hoursInMilli;
		long days = hours / 24;
		long numberWeeks = days / 7;
		
		days = days - numberWeeks*2;
		long workinghours = days * 8;
		return Long.toString(workinghours);

	}





	 
	



	
}

