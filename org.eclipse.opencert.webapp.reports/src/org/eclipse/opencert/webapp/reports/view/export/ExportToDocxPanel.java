/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.export;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.export.docx.IDocxGenerator;
import org.eclipse.opencert.webapp.reports.util.IOUtil;
import org.eclipse.opencert.webapp.reports.util.OpencertLogger;
import org.eclipse.opencert.webapp.reports.view.common.IBaselineFrameworkChangeListener;
import org.eclipse.opencert.webapp.reports.view.common.IRefFrameworksChangeListener;

import com.vaadin.server.Page;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;

public class ExportToDocxPanel
        implements IBaselineFrameworkChangeListener, IRefFrameworksChangeListener
{
    private long _baselineFrameworkID = -1;
    private long _projectID = -1;
    private long _fromRefFrameworkID = -1;
    private long _toRefFrameworkID = -1;
    
    private Button _button = null;
    private final IDocxGenerator docxGenerator;
    private String fileName;
    
    public ExportToDocxPanel(IDocxGenerator generator)
    {
        docxGenerator = generator;
        fileName="report";
    }
    
    @Override
    public void setBaselineFramework(long baselineFrameworkID)
    {
        _baselineFrameworkID = baselineFrameworkID;
    }

    public void setNewProject(AssuranceProjectWrapper newProject)
    {
        _projectID = newProject.getId();
    }

    @SuppressWarnings("serial")
    public Button getButton()
    {
        if (_button == null) {
            _button = new Button("Export to MS Word");
            _button.setDescription("Export the project baseline compliance data to summary textual report in .docx format.");
            _button.setStyleName("ExportToDocxButton");
            _button.addClickListener((e) ->
            {
                OpencertLogger.info("Export to docx pressed. _baselineFrameworkID: " + _baselineFrameworkID);
                
                if (_baselineFrameworkID < 0 && _fromRefFrameworkID < 0 && _toRefFrameworkID < 0) {
                    Notification.show("No baseline framework selected. Please select a baseline framework for which the report should be generated.");
                } 
                else 
                {
                    InputStream resultStream = null;
                    try {
                        
                    	if(_projectID != -1)
                    		resultStream = docxGenerator.generateProjectBaselineReport(_baselineFrameworkID, _projectID);
                    	else 
                    		resultStream = docxGenerator.generateRefFrameworkToRefFrameworkReport(_fromRefFrameworkID, _toRefFrameworkID);
                    	
                        fileName = docxGenerator.getReportName();

                        streamToCurrentResponse(resultStream);

                    } catch (Docx4JException | IOException ex) {
                        ex.printStackTrace();
                        Notification.show(ex.getMessage(), Notification.TYPE_ERROR_MESSAGE);
                    } finally {
                        IOUtil.close(resultStream);
                    }
                }
            });
        }
        return _button;
    }
    
    private void streamToCurrentResponse(final InputStream inStream) throws IOException
    {
        @SuppressWarnings("serial")
        StreamSource streamSource = new StreamResource.StreamSource() {
            @Override
            public InputStream getStream()
            {
                return inStream;
            }
        };

        final String completeFileName = fileName + new Random().nextInt(10000) + ".docx";
        StreamResource resource = new StreamResource(streamSource, completeFileName);
        resource.setMIMEType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        resource.getStream().setParameter("Content-Disposition", "attachment; filename=\"" + completeFileName + "\"");
        resource.getStream().setParameter("Content-Transfer-Encoding", "binary");

        // I have to use the depricated API because the proper usage with Link (ExportToDocxWindow _downloadWindow) does not work fully on FireFox
        Page.getCurrent().open(resource, "_parent", false);
        
        //IMPORTANT! The below does not work - no links (AJAX generated by Vaadin) would work after downloading the resource
        //Page.getCurrent().open(resource, "_self", false);
        
        // There are issues with closing the download window on FireFox (IE and Chrome works well)
        //_downloadWindow.show(resource);
    }

	@Override
	public void setEquivalenceFrameworks(long fromFrameworkID, long toRefFramework) {
		_projectID = -1;
		_fromRefFrameworkID = fromFrameworkID;
		_toRefFrameworkID = toRefFramework;
		
	}
}