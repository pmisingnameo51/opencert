/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view;

import java.io.File;

import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.view.common.GUIMode;
import org.eclipse.opencert.webapp.reports.view.common.IReport;
import org.eclipse.opencert.webapp.reports.view.common.ReportID;
import org.eclipse.opencert.storage.cdo.property.OpencertPropertiesReader;

import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class ConfigurationSettingsPage implements IReport {

	private static final String TITLE = "Configuration Settings";
	
	private static final String CDO_CONFIGURATION_TITLE = "CDO Server Configuration:";
	private static final String DATABASE_CONFIGURATION_TITLE = "Database Configuration:";
	
	private static final String FILE_PATH_LABEL = "The configuration settings can be edited in the following file:";
	private static final String CDO_SERVER_ADDRESS_LABEL = "CDO Server Address:";
	private static final String DB_HOST_LABEL = "Database Host:";
	private static final String DB_PORT_LABEL = "Database Port:";
	private static final String DB_NAME_LABEL = "Database Name:";
	private static final String DB_USER_LABEL = "Database User:";
	private static final String DB_PASSWORD_LABEL = "Database Password:";
	
	private static final String CONFIGURATION_SETTINGS_TITLE_STYLE = "confSettingsTitle";
	private static final String CONFIGURATION_SETTINGS_STYLE = "confSettings";
	private static final String CONFIGURATION_SETTINGS_ITALIC_STYLE = "confSettingsItalic";
	private static final String CONFIGURATION_SETTINGS_ITALIC_BOLD_STYLE = "confSettingsItalicBold";

	@Override
	public void projectChanged(AssuranceProjectWrapper newProject) {	
	}

	@Override
    public boolean isSupportsProjects()
    {
        return false;
    }
	
	@Override
	public ReportID getReportID() {
		return ReportID.ADMIN_CONFIGURATION_SETTINGS;
	}

	@Override
	public String getTitle() {
		return TITLE;
	}

	@Override
	public Component getParametersComponent(GUIMode guiMode) 
	{
		return null;
	}

	@Override
	public Component getMainComponent(GUIMode guiMode) 
	{
		OpencertPropertiesReader opencertProperties = OpencertPropertiesReader.getInstance();
		VerticalLayout mainPanel = new VerticalLayout();
		mainPanel.setHeight("500px");
		VerticalLayout formLayout = new VerticalLayout();
		
		HorizontalLayout filePathLayout = new HorizontalLayout();
		String configurationFilePath = System.getProperty("user.home") + File.separator + "opencert-properties.xml";
		Label configurationSettingsFileLabel = new Label(FILE_PATH_LABEL);
		configurationSettingsFileLabel.setStyleName(CONFIGURATION_SETTINGS_ITALIC_STYLE);
		Label configurationSettingsFile = new Label(configurationFilePath);
		configurationSettingsFile.setStyleName(CONFIGURATION_SETTINGS_ITALIC_BOLD_STYLE);
		filePathLayout.addComponent(configurationSettingsFileLabel);
		filePathLayout.addComponent(configurationSettingsFile);
		
		Label cdoServerConf = new Label(CDO_CONFIGURATION_TITLE);
		cdoServerConf.setStyleName(CONFIGURATION_SETTINGS_TITLE_STYLE);
		
		GridLayout cdoServerAddressLayout = new GridLayout(2,1);
		cdoServerAddressLayout.setStyleName(CONFIGURATION_SETTINGS_STYLE);
		
		Label cdoServerAddressLabel = new Label(CDO_SERVER_ADDRESS_LABEL);
		Label cdoServerAddress = new Label(opencertProperties.getCDOServerAddress());
		cdoServerAddressLayout.addComponent(cdoServerAddressLabel);
		cdoServerAddressLayout.addComponent(cdoServerAddress);
		
		Label databaseConfTitle = new Label(DATABASE_CONFIGURATION_TITLE);
		databaseConfTitle.setStyleName(CONFIGURATION_SETTINGS_TITLE_STYLE);

		GridLayout databaseConfLayout = new GridLayout(2, 5);
		databaseConfLayout.setStyleName(CONFIGURATION_SETTINGS_STYLE);
	
		Label databaseHostLabel = new Label(DB_HOST_LABEL);
		Label databaseHost = new Label(opencertProperties.getDbHost());
		Label databasePortLabel = new Label(DB_PORT_LABEL);
		Label databasePort = new Label(Integer.toString(opencertProperties.getDbPort()));
		Label databaseNameLabel = new Label(DB_NAME_LABEL);
		Label databaseName = new Label(opencertProperties.getDbName());
		Label databaseUserLabel = new Label(DB_USER_LABEL);
		Label databaseUser = new Label(opencertProperties.getDbUser());
		Label databasePasswordLabel = new Label(DB_PASSWORD_LABEL);
		Label databasePassword = new Label("*****");
	
		databaseConfLayout.addComponent(databaseHostLabel);
		databaseConfLayout.addComponent(databaseHost);
		databaseConfLayout.addComponent(databasePortLabel);
		databaseConfLayout.addComponent(databasePort);
		databaseConfLayout.addComponent(databaseNameLabel);
		databaseConfLayout.addComponent(databaseName);
		databaseConfLayout.addComponent(databaseUserLabel);
		databaseConfLayout.addComponent(databaseUser);
		databaseConfLayout.addComponent(databasePasswordLabel);
		databaseConfLayout.addComponent(databasePassword);
		
		formLayout.addComponent(filePathLayout);
		formLayout.addComponent(cdoServerConf);
		formLayout.addComponent(cdoServerAddressLayout);
		formLayout.addComponent(databaseConfTitle);
		formLayout.addComponent(databaseConfLayout);
		
		mainPanel.addComponent(formLayout);

		return mainPanel;
	}
}
