/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.export;

import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;

import com.vaadin.server.StreamResource;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Link;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class ExportToDocxWindow
        extends Window
{
    private Link _link = null;
    private StreamResource _resource = null;
    
    public ExportToDocxWindow()
    {
        super("The document has been generated successfully.");
        VerticalLayout content = new VerticalLayout();
        content.addComponent(createLinkWrapper());
        setContent(content);
        
        setStyleName("ExportToDocxWindow");
        setResizable(false);
        setModal(true);
    }
    
    public void show(StreamResource resource)
    {
        _resource  = resource;
        
        _link.setResource(resource);
        center();
        
        if (isWindowAdded(this)) {
            setVisible(true);
        } else {
            UI.getCurrent().addWindow(this);
        }
    }
    
    private boolean isWindowAdded(ExportToDocxWindow exportToDocxWindow)    
    {
        final Collection<Window> windows  = UI.getCurrent().getWindows();
        if (windows == null) {
            return false;
        }
        for (Window w : windows)
        {
            if (this.equals(w)) {
                return true;
            }
        }
        return false;
    }

    private Component createLinkWrapper()
    {
        final ExportToDocxWindow win = this;
        
        HorizontalLayout panel = new HorizontalLayout();
        _link = new Link();
        _link.setCaption("[Download the document]");
        _link.setStyleName("ExportToDocxLink");
        panel.addComponent(_link);
        panel.addLayoutClickListener((e) ->
        {
            Timer timer = new Timer();//timer.cancel(); //this will cancel the current task. if there is no active task, nothing happens

            TimerTask action = new TimerTask() {
                public void run() {
                    win.setImmediate(true);
                    setVisible(false);
                    UI.getCurrent().removeWindow(win);                    
                    win.close();
                    win.setPositionX(100);
                    win.requestRepaint();
                }

            };

            timer.schedule(action, 1000); //this starts the task
//            setVisible(false);
        }
        );
        
        return panel;
    }
}