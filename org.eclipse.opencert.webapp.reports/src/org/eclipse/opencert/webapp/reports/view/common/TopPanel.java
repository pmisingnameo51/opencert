/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.eclipse.opencert.webapp.reports.listeners.ReportChangeNotifier;
import org.eclipse.opencert.webapp.reports.security.SecurityUtil;
import org.eclipse.opencert.webapp.reports.view.url.URLParamHandler;
import org.eclipse.opencert.webapp.reports.view.url.URLParamsContainer;

import com.vaadin.server.ExternalResource;
import com.vaadin.server.Page;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Notification;

@SuppressWarnings("serial")
public class TopPanel extends CustomComponent
{
	private final ReportProvider reportProvider;
	private final ReportChangeNotifier reportChangeNotifier;
	
	private static final String OPENCERT_OFFICIAL_SITE_MENU = "OPENCERT Official Site";
	private static final String CLIENT_DEVELOPER_GUIDE_MENU = "Developer Guide - Client";
	private static final String SERVER_DEVELOPER_GUIDE_MENU = "Developer Guide - Server";
	private static final String USER_GUIDE_MENU = "User Guide - Client and Server";
	private static final String HELP_MENU = "Help";
	private static final String ADMINISTRATION_MENU = "Administration";	
	private static final String CREATE_SAMPLE_DATA_MENU = "Create Sample Data";
	private static final String CONFIGURATION_SETTINGS_MENU = "Configuration Settings";
    private static final String PROJECTS_ADMINISTRATION_MENU = "Projects Administration";

	public TopPanel(ReportProvider rp, ReportChangeNotifier notifier, URLParamsContainer urlParams)
	{
		reportProvider = rp;
		reportChangeNotifier = notifier;
		final HorizontalLayout mainLayout = new HorizontalLayout();
		setCompositionRoot(mainLayout);
		setStyleName("topPanel");
		
		mainLayout.setWidth("100%");

		final Link opencertImageLink = createOpencertLogo(urlParams, GUIMode.NORMAL);
		mainLayout.addComponent(opencertImageLink);
		
		Label spacer = new Label();
		mainLayout.addComponent(spacer);
		
		if (SecurityUtil.isSecurityEnabled()) 
		{
		    HorizontalLayout securityPanel = new HorizontalLayout();
		    securityPanel.setStyleName("securityPanel");
		    mainLayout.addComponent(securityPanel);
		    
    		Label loggedAsLabel = new Label("User: " + SecurityUtil.getLoginName());
    		loggedAsLabel.addStyleName("loggedAsLabel");
            securityPanel.addComponent(loggedAsLabel);
            
    		Link logoutLink = new Link("Logout", new ExternalResource("logout"));
    		securityPanel.addComponent(logoutLink);
		}
        
        mainLayout.setExpandRatio(spacer, 0.7f);
        		
		MenuBar topPanelMenu = new MenuBar();
		topPanelMenu.setStyleName("topPanelMenu");
		MenuBar.Command menuCommand =  new MenuBar.Command() 
		{
			@Override
			public void menuSelected(MenuItem selectedItem) {
				final String menuName = selectedItem.getText();
				switch (menuName) {
					case USER_GUIDE_MENU:
						openPdfInHelpDir("OPENCERT_UserManual.pdf");
						break;
					case SERVER_DEVELOPER_GUIDE_MENU:
						openPdfInHelpDir("OPENCERT_DeveloperGuide_Server.pdf");
						break;
					case CLIENT_DEVELOPER_GUIDE_MENU:
						openPdfInHelpDir("OPENCERT_DeveloperGuide_Client.pdf");
						break;						
					case OPENCERT_OFFICIAL_SITE_MENU:
						openWebSite("http://www.opencert-project.eu/");
						break;
					default:
						Notification.show("To be implemented", "", Notification.Type.HUMANIZED_MESSAGE);
						break;	
				}
			}
		}; 
		MenuItem administrationMenu = topPanelMenu.addItem(ADMINISTRATION_MENU, new ThemeResource("images/administration.png"), null);
		administrationMenu.addItem(PROJECTS_ADMINISTRATION_MENU, new ThemeResource("images/administration.png"), (e) ->
		{
		    fireReportChanged(ReportID.ADMIN_PROJECT);
		});
		administrationMenu.addItem(CREATE_SAMPLE_DATA_MENU, new ThemeResource("images/sampleData.png"), (e) ->
		{
			fireReportChanged(ReportID.ADMIN_CREATE_SAMPLE_DATA);
		});
		administrationMenu.addItem(CONFIGURATION_SETTINGS_MENU, new ThemeResource("images/administration.png"), (e) ->  
		{
		    fireReportChanged(ReportID.ADMIN_CONFIGURATION_SETTINGS);
		});

	    MenuItem helpItem = topPanelMenu.addItem(HELP_MENU, new ThemeResource("images/help.png"), null);
	    helpItem.addItem(USER_GUIDE_MENU, new ThemeResource("images/userManual.png"), menuCommand);
	    helpItem.addSeparator();
	    helpItem.addItem(CLIENT_DEVELOPER_GUIDE_MENU, new ThemeResource("images/keyboard.png"), menuCommand);
	    helpItem.addItem(SERVER_DEVELOPER_GUIDE_MENU, new ThemeResource("images/keyboard.png"), menuCommand);
	    helpItem.addSeparator();
	    helpItem.addItem(OPENCERT_OFFICIAL_SITE_MENU, new ThemeResource("images/webSite.png"), menuCommand);
	    
	    mainLayout.addComponent(topPanelMenu);
	}

    public static Link createOpencertLogo(URLParamsContainer urlParamsContainer, GUIMode guiMode)
    {
        final String url = URLParamHandler.generateWebURL(urlParamsContainer, guiMode); 
        final Link opencertImageLink = new Link(null, new ExternalResource(url));
		opencertImageLink.setTargetName("_blank");
		opencertImageLink.setIcon(new ThemeResource("images/opencertLogo.png"));
		
		urlParamsContainer.addURLParamsChangeListener(() -> 
		{
		    final String newURL = URLParamHandler.generateWebURL(urlParamsContainer, guiMode);
		    opencertImageLink.setResource(new ExternalResource(newURL));
		});
		
        return opencertImageLink;
    }
	
	private void fireReportChanged(ReportID reportId)
    {
	    final IReport report = reportProvider.getOrCreateReport(reportId);
	    if (reportChangeNotifier != null) 
	    {
	        reportChangeNotifier.fireReportChanged(report);
	    }
    }
	
	@SuppressWarnings("deprecation")
	private void openPdfInHelpDir(final String helpFileName) 
	{		
		StreamSource streamSource = new StreamResource.StreamSource() {
			@Override
			public InputStream getStream() {
				try {
					final File helpDir = new File(VaadinService.getCurrent().getBaseDirectory(), "help");					
					FileInputStream fis = new FileInputStream(new File(helpDir, helpFileName));
					return fis;
				} catch (Exception e) {
					e.printStackTrace();
					return null;
				}
			}
		};
		StreamResource resource = new StreamResource(streamSource, helpFileName);
		resource.setMIMEType("application/pdf");
	    resource.getStream().setParameter("Content-Disposition", "attachment; filename=" + helpFileName);
		
		Page.getCurrent().open(resource, "_blank", false);
	}
	
	@SuppressWarnings("deprecation")
	private void openWebSite(String url) 
	{
		ExternalResource resource = new ExternalResource(url);
		Page.getCurrent().open(resource, "_blank", false);
	}

}