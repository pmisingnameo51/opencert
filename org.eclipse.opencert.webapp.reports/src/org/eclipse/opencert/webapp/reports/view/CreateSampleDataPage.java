/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view;

import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.data.generator.ReportDataGenerator;
import org.eclipse.opencert.webapp.reports.listeners.IProjectChangeListener;
import org.eclipse.opencert.webapp.reports.util.OPException;
import org.eclipse.opencert.webapp.reports.view.common.GUIMode;
import org.eclipse.opencert.webapp.reports.view.common.IReport;
import org.eclipse.opencert.webapp.reports.view.common.ReportID;

import com.vaadin.server.Page;
import com.vaadin.server.UserError;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class CreateSampleDataPage implements IReport, IProjectChangeListener 
{
    private static final String NAME_OF_BASEFRAMEWORK = "Name of BaseFramework";
    
    private static final String NUMBER_OF_BASEARTEFACTS = "Number of BaseArtefacts";
    
    private static final String NUMBER_OF_BASEACTIVITIES = "Number of BaseActivities";
    
    private static final String RANGE_OF_ELEMENTS = "Range of Artefacts/Activities for BaseElements";
    
    private static final String TITLE = "Create Sample Data";
    
    private static final String ASSURANCE_PROJECT = "Assurance Project";
    
    private static final String GENERATE_DB = "Generate Data";
    
    private static final String CREATE_SAMPLE_DATA_FORM_STYLE = "createSampleDataFormStyle";
    
    private static final String CREATE_SAMPLE_DATA_LAYOUT_STYLE = "createSampleData";
    
    private AssuranceProjectWrapper assuranceProject;
        
    private TextField assuranceProjectField = new TextField(ASSURANCE_PROJECT);
    private Button submitButton;

	@Override
	public void projectChanged(AssuranceProjectWrapper newProject) 
	{    
	    assuranceProject = newProject;
	    if (newProject != null) {
	    	assuranceProjectField.setValue(newProject.getName());
	    	submitButton.setEnabled(true);
	    } else {
	    	submitButton.setEnabled(false);
	    }
	}

	@Override
    public boolean isSupportsProjects()
    {
        return true;
    }
	
	@Override
	public ReportID getReportID() 
	{
		return ReportID.ADMIN_CREATE_SAMPLE_DATA;
	}

	@Override
	public String getTitle() 
	{
		return TITLE;
	}

	@Override
	public Component getParametersComponent(GUIMode guiMode) 
	{
		return null;
	}

	@Override
	public Component getMainComponent(GUIMode guiMode) 
	{
		VerticalLayout mainPanel = new VerticalLayout();
		mainPanel.setHeight("500px");
		
		VerticalLayout formLayout = new VerticalLayout();
		formLayout.setStyleName(CREATE_SAMPLE_DATA_LAYOUT_STYLE);
		//formLayout.setWidth("400px");

		assuranceProjectField.setEnabled(false);
		assuranceProjectField.setStyleName(CREATE_SAMPLE_DATA_FORM_STYLE);
		formLayout.addComponent(assuranceProjectField);
		
		final TextField baseFrameworkField = new TextField(NAME_OF_BASEFRAMEWORK);
		baseFrameworkField.setStyleName(CREATE_SAMPLE_DATA_FORM_STYLE);
		formLayout.addComponent(baseFrameworkField);
		
		final ComboBox baseArtefactNumberCombo = new ComboBox(NUMBER_OF_BASEARTEFACTS);
		baseArtefactNumberCombo.setStyleName(CREATE_SAMPLE_DATA_FORM_STYLE);
		addItemIntoComboBox(baseArtefactNumberCombo, 100);
		baseArtefactNumberCombo.setInvalidAllowed(false);
		baseArtefactNumberCombo.setNullSelectionAllowed(false);
		formLayout.addComponent(baseArtefactNumberCombo);
		
		final ComboBox baseActivityNumberCombo = new ComboBox(NUMBER_OF_BASEACTIVITIES);
		baseActivityNumberCombo.setStyleName(CREATE_SAMPLE_DATA_FORM_STYLE);
        addItemIntoComboBox(baseActivityNumberCombo, 100);
        baseActivityNumberCombo.setInvalidAllowed(false);
        baseActivityNumberCombo.setNullSelectionAllowed(false);
        formLayout.addComponent(baseActivityNumberCombo);
        
        final ComboBox rangeOfEObjectsCombo = new ComboBox(RANGE_OF_ELEMENTS);
        rangeOfEObjectsCombo.setStyleName(CREATE_SAMPLE_DATA_FORM_STYLE);
        addItemIntoComboBox(rangeOfEObjectsCombo, 99);
        rangeOfEObjectsCombo.setInvalidAllowed(false);
        rangeOfEObjectsCombo.setNullSelectionAllowed(false);
        formLayout.addComponent(rangeOfEObjectsCombo);
        
        submitButton = new Button(GENERATE_DB);
        submitButton.setStyleName(CREATE_SAMPLE_DATA_FORM_STYLE);
        submitButton.addClickListener(new ClickListener()
        {
            private static final long serialVersionUID = 1L;

            @Override
            public void buttonClick(final ClickEvent event)
            {                
            	if (markEntryDataErrors()) {
            		return;
            	}
                int nOfBaseArtefacts = (int) baseArtefactNumberCombo.getValue();
                int nOfBaseActivities = (int)  baseActivityNumberCombo.getValue();
                int rangeOfElements = (int) rangeOfEObjectsCombo.getValue();
                
                ReportDataGenerator reportDataGenerator = new ReportDataGenerator(assuranceProject, baseFrameworkField.getValue(), 
                        nOfBaseArtefacts, nOfBaseActivities, rangeOfElements);
                try {
                    reportDataGenerator.populateData();
                    
                    Page.getCurrent().getJavaScript().execute("window.location.reload();");
                    
                } catch (OPException e) {
                    Notification.show(e.getMessage(), Notification.Type.ERROR_MESSAGE);
                }
            }

			private boolean markEntryDataErrors() 
			{
				boolean result = markEntryDataError(baseArtefactNumberCombo);
				result |= markEntryDataError(baseActivityNumberCombo);
				result |= markEntryDataError(rangeOfEObjectsCombo);
				result |= markEntryDataError(baseFrameworkField);
				
				return result;
			}

			private boolean markEntryDataError(AbstractField<?> dataEntryField) 
			{
				final Object value = dataEntryField.getValue();
				boolean isError = false;
				dataEntryField.setComponentError(null);
				if (value == null) {
					isError = true;
				}
				if (value instanceof String && ((String)value).trim().length()==0) {
					isError = true;
				}
				if (isError) {
					dataEntryField.setComponentError(new UserError("The data cannot be empty!"));					
				}				
				return isError;
			}
        });
        
        formLayout.addComponent(submitButton);
        mainPanel.addComponent(formLayout);
		
		return mainPanel;
	}
	
	private void addItemIntoComboBox(ComboBox combo, int numberOfItems) 
	{
	    for (int i = 0; i <= numberOfItems; ++i) {
	        combo.addItem(new Integer(i));
	    }
	}
}
