/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.url;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.listeners.IProjectChangeListener;
import org.eclipse.opencert.webapp.reports.listeners.IReportChangeListener;
import org.eclipse.opencert.webapp.reports.view.common.GUIMode;
import org.eclipse.opencert.webapp.reports.view.common.IReport;
import org.eclipse.opencert.webapp.reports.view.common.ReportID;


public class URLParamsContainer
    implements IReportChangeListener, IProjectChangeListener
{
    private final boolean isCorrect;
    private final String errorMessage;
    private ReportID detectedReportID;
    private GUIMode detectedGUIMode;
    private String detectedProjectName;
    private final String host;
    private final int port;
    private final String path;

    private List<IURLParamsChangeListener> urlParamsChangeListeners =  new LinkedList<IURLParamsChangeListener>();
    
    public URLParamsContainer(ReportID reportID, GUIMode guiMode, String projectName, String host, int port, String path)
    {
        this(true, null, host, port, path);
        detectedReportID = reportID;
        detectedGUIMode = guiMode;
        detectedProjectName = projectName;
    }
    
    public URLParamsContainer(boolean isCorrect, String errorMessage, String host, int port, String path)
    {
        this.errorMessage = errorMessage;
        this.isCorrect = isCorrect;
        this.host = host;
        this.port = port;
        this.path = path;
    }

    public ReportID getReportID()
    {
        return this.detectedReportID;
    }

    public GUIMode getGUIMode()
    {
        return this.detectedGUIMode;
    }
    
    public String getDetectedProjectName()
    {
        return this.detectedProjectName;
    }
    
    public String getHost()
    {
        return host;
    }

    public int getPort()
    {
        return port;
    }
    
    public String getPath()
    {
        return path;
    }
    
    public boolean isCorrect()
    {
        return this.isCorrect;
    }

    public String getErrorMessage()
    {
        return this.errorMessage;
    }

    @Override
    public void projectChanged(AssuranceProjectWrapper newProject)
    {
        String newProjectName = null;
        if (newProject != null) {
            newProjectName = newProject.getName();
        }
        detectedProjectName = newProjectName;
        fireURLParamChanged();
    }

    @Override
    public void reportChanged(IReport report)
    {
        if (report == null) {
            return;
        }
        detectedReportID = report.getReportID();
        fireURLParamChanged();
    }
    
    private void fireURLParamChanged()
    {
        urlParamsChangeListeners.forEach(li -> li.urlParamsChanged());
    }

    public void addURLParamsChangeListener(IURLParamsChangeListener listener)
    {
        urlParamsChangeListeners.add(listener);
    }
}