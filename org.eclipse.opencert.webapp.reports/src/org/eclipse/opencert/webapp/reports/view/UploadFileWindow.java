/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.Cookie;

import org.eclipse.opencert.webapp.reports.containers.BaseAssetComplianceStatus;
import org.eclipse.opencert.webapp.reports.containers.ComplianceAssetResource;
import org.eclipse.opencert.webapp.reports.containers.SvnProperties;
import org.eclipse.opencert.webapp.reports.listeners.EvidenceFileUploaderListener;
import org.eclipse.opencert.webapp.reports.listeners.UnassignOrAssignArtefactListener;
import org.eclipse.opencert.webapp.reports.util.ComplianceLevel;
import org.eclipse.opencert.webapp.reports.util.IUploadFile;
import org.eclipse.opencert.webapp.reports.util.UploadFileMode;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinServletRequest;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;

public class UploadFileWindow 
		extends Window 
{
	private static final long serialVersionUID = -2338698195201192012L;
	private EvidenceFileUploader evidenceFileUploader;
    private Label secondTopInfoLabel = new Label();
    private Label dragAndDropFileName;
    private TextField artefactName;
    private TextField resourceName;
    private Button assignButton;
    private OptionGroup complianceGroup;
    private TextArea complianceJustificationArea;
    private PopupDateField modificationDate;
    private Label bottomEvidenceNote = new Label("", ContentMode.HTML);
    private byte[] fileByteArray;
    
    private boolean isDragAndDropFunctionalityEnabled = false;
    
    private List<UnassignOrAssignArtefactListener> unassignOrAssignArtefactListeners = new ArrayList<UnassignOrAssignArtefactListener>();
    
    private TextField svnPath;
    private TextField svnUser;
    private ComboBox svnUrlsCombo;
    private PasswordField svnPassword;
    private Button checkSVNLocation;
    private SvnProperties svnPropertiesFromCookies;
    
    private static final String _MSG_FOR_COMPLIANCE_WITH_THE_FOLLOWING_BASELINE_ITEM_BR_B = "as an evidence for compliance with the following baseline item:<br/><b>\"";
    private UploadFileMode uploadFileMode = UploadFileMode.ADD_FILE; 
    private ComplianceAssetResource complianceResource;
    
	public UploadFileWindow(UploadFileMode uploadFileMode, ComplianceAssetResource complianceResource, 
			EvidenceFileUploaderListener evidenceFileUploaderListener, String baselineElementName, String reportTitle)
	{
		super("Assign Compliance Evidence File");
		this.uploadFileMode = uploadFileMode;
		this.complianceResource = complianceResource;
		
		if (uploadFileMode == UploadFileMode.MODIFY_FILE) {
			setCaption("Modify Compliance Evidence File");
		}
		
		evidenceFileUploader = new EvidenceFileUploader(this, evidenceFileUploaderListener, reportTitle);
		if (baselineElementName != null) {
			evidenceFileUploader.getArtefactFileDetails().setBaseElementName(baselineElementName);
		}
		setResizable(false);
		setStyleName("assignComplianceEvidenceWindow");
		center();
		setModal(true);
		
		addCloseListener(new CloseListener() {
            private static final long serialVersionUID = 6673378500979019062L;

            @Override
            public void windowClose(CloseEvent e)
            {
                complianceGroup.select(null);
                complianceJustificationArea.setValue("");
                artefactName.setValue("");
                if (uploadFileMode != UploadFileMode.MODIFY_FILE) {
                	resourceName.setValue("");
                }
                evidenceFileUploader.getArtefactFileDetails().setFileName("");
            }
        });
		
		
		FormLayout assignWindowContent = new FormLayout();
        assignWindowContent.setStyleName("assignEvidenceWindowContent");
        generateAssignComplianceEvidenceWindowContent(assignWindowContent);
        setContent(assignWindowContent);
        
        if (!isDragAndDropFunctionalityEnabled) {
        	createSvnPropertiesData();
        	bottomEvidenceNote.setValue(generateBottomEvidenceNoteValue());
        	dragAndDropFileName.setVisible(false);
        	evidenceFileUploader.getUpload().setVisible(true);
        }
	}
	
	private void generateAssignComplianceEvidenceWindowContent(final FormLayout content)
	{
		final Label topInfoLabel = new Label();
        if (uploadFileMode == UploadFileMode.MODIFY_FILE) {
        	topInfoLabel.setContentMode(ContentMode.HTML);
        	String complianceResourceFile = "";
        	if (complianceResource != null) {
        		complianceResourceFile = complianceResource.getLocation();
        	}
        	topInfoLabel.setValue("Modify the following resource file: <b>" + complianceResourceFile + "</b>");
        } else {
        	topInfoLabel.setValue("Assign the following file: ");
        }

        secondTopInfoLabel.setContentMode(ContentMode.HTML);
       	secondTopInfoLabel.setValue(_MSG_FOR_COMPLIANCE_WITH_THE_FOLLOWING_BASELINE_ITEM_BR_B
                + evidenceFileUploader.getArtefactFileDetails().getBaseElementName() + "\"");

        VerticalLayout topInfoAndFileLayout = new VerticalLayout();
        topInfoAndFileLayout.addComponent(topInfoLabel);

        dragAndDropFileName = new Label();
        dragAndDropFileName.setStyleName("dragAndDropLabelStyle");

        HorizontalLayout uplaodOrTextFieldLayout = new HorizontalLayout();
        uplaodOrTextFieldLayout.addComponent(dragAndDropFileName);
        uplaodOrTextFieldLayout.addComponent(evidenceFileUploader);

        topInfoAndFileLayout.addComponent(uplaodOrTextFieldLayout);

        topInfoAndFileLayout.addComponent(secondTopInfoLabel);
        content.addComponent(topInfoAndFileLayout);
        
        content.addComponent(generateResourceAndArtefactNameField(evidenceFileUploader));
        content.addComponent(createComplianceGroup());
        content.addComponent(createComplianceJustificationArea());
        content.addComponent(createModificationDateArea());
        content.addComponent(createSVNPropertiesLayout());
        bottomEvidenceNote.setStyleName("bottomEvidenceNote");

        HorizontalLayout assignAndCancelButtonsLayout = createAssignAndCancelButtons(evidenceFileUploader);

        content.addComponent(assignAndCancelButtonsLayout);
	}
	
    private Component generateResourceAndArtefactNameField(final EvidenceFileUploader evidenceFileUploader)
    {
        VerticalLayout textFieldsLayout = new VerticalLayout();

        artefactName = new TextField("Artefact name: ");
        artefactName.setStyleName("dndArtefactLabel");
        artefactName.setImmediate(true);
        artefactName.setValue(evidenceFileUploader.getArtefactFileDetails().getFileName());
        artefactName.addValueChangeListener(new ValueChangeListener() {
            private static final long serialVersionUID = 8456735657984104944L;

            @Override
            public void valueChange(ValueChangeEvent event)
            {
                evidenceFileUploader.getArtefactFileDetails().setArtefactName(event.getProperty().getValue().toString());
            }
        });

        resourceName = new TextField("Resource name:");
        resourceName.setStyleName("dndResourceLabel");
        resourceName.setImmediate(true);
        resourceName.setValue(evidenceFileUploader.getArtefactFileDetails().getFileName());
        resourceName.addValueChangeListener(new ValueChangeListener() {
            private static final long serialVersionUID = 8456735657984104944L;

            @Override
            public void valueChange(ValueChangeEvent event)
            {
                evidenceFileUploader.getArtefactFileDetails().setResourceName(event.getProperty().getValue().toString());
            }
        });
        
        if (uploadFileMode == UploadFileMode.MODIFY_FILE) {
        	String complianceResourceName = "";
        	if (complianceResource != null) {
        		complianceResourceName = complianceResource.getName();
        	}
        	resourceName.setValue(complianceResourceName);
        	artefactName.setVisible(false);
        } else {
        	artefactName.setVisible(true);
        }

        textFieldsLayout.addComponent(artefactName);
        textFieldsLayout.addComponent(resourceName);

        return textFieldsLayout;
    }
	
    private OptionGroup createComplianceGroup()
    {
        complianceGroup = new OptionGroup();
        complianceGroup.addItem(ComplianceLevel.FULL.getValue());
        complianceGroup.addItem(ComplianceLevel.PARTIAL.getValue());
        complianceGroup.addItem(ComplianceLevel.NO_MAP.getValue());
        complianceGroup.addStyleName("horizontalComplianceGroup");
        complianceGroup.setSizeUndefined();
        complianceGroup.setImmediate(true);
        
        if (uploadFileMode == UploadFileMode.MODIFY_FILE) {
        	complianceGroup.setVisible(false);
        } else {
        	complianceGroup.setVisible(true);
        }

        return complianceGroup;
    }

    private VerticalLayout createComplianceJustificationArea()
    {
        VerticalLayout complianceJustificationAreaLayout = new VerticalLayout();
        complianceJustificationAreaLayout.setStyleName("complianceJustificationAreaLayout");
        complianceJustificationAreaLayout.addComponent(new Label("Compliance Justification:"));

        complianceJustificationArea = new TextArea();
        complianceJustificationArea.setImmediate(true);
        complianceJustificationArea.setHeight("50px");
        complianceJustificationArea.setWidth("440px");

        complianceJustificationAreaLayout.addComponent(complianceJustificationArea);
        if (uploadFileMode == UploadFileMode.MODIFY_FILE) {
        	complianceJustificationAreaLayout.setVisible(false);
        } else {
        	complianceJustificationAreaLayout.setVisible(true);
        }

        return complianceJustificationAreaLayout;
    }
    
    private HorizontalLayout createModificationDateArea()
    {
    	HorizontalLayout dateLayout = new HorizontalLayout();
    	dateLayout.setWidth("100%");
    	
    	Label modificationDateLabel = new Label("Artefact date:");
    	modificationDateLabel.setWidth("112px");
    	modificationDateLabel.setStyleName("modificationDate");
    	
    	modificationDate = new PopupDateField();
        modificationDate.setValue(new Date());
        modificationDate.setWidth("200px");
        modificationDate.setDateFormat(" yyyy-MM-dd HH:mm a ");
        modificationDate.setImmediate(true);
        // modificationDate.setTimeZone(TimeZone.getTimeZone("UTC"));
        // modificationDate.setLocale(Locale.US);
        modificationDate.setResolution(Resolution.MINUTE);
        modificationDate.setDescription("Please specify Artefact Date");
         
        modificationDate.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = -236377117863327943L;

			@Override
        	public void valueChange(final ValueChangeEvent event) {
        		Date modificDate = (Date) event.getProperty().getValue();
        		if (modificDate != null) {
        			evidenceFileUploader.getArtefactFileDetails().setModificationDate(modificDate);
        		}
        	}
        });
 
    	dateLayout.addComponent(modificationDateLabel);
    	dateLayout.addComponent(modificationDate);
    	dateLayout.setExpandRatio(modificationDate, 1.0f);
    	
    	return dateLayout;
    }
    
    private Component createSVNPropertiesLayout()
    {
    	VerticalLayout svnFieldsLayout = new VerticalLayout();
    	
    	Label svnPathLabel = new Label("SVN path:");
    	svnPathLabel.setStyleName("svnPathLabel");
    	if (uploadFileMode == UploadFileMode.MODIFY_FILE) {
    		svnPathLabel.setDescription("SVN http URL for the resource file being modified.");
    	} else {
    		svnPathLabel.setDescription("Please specify SVN http URL to the *existing* target folder on your SVN server. "
    				+ "The evidence file will be committed into this very location.");
    	}
    	
    	svnUrlsCombo = new ComboBox();
    	svnUrlsCombo.setImmediate(true);
        svnUrlsCombo.setNullSelectionAllowed(false);
        svnUrlsCombo.setInvalidAllowed(false);   
        svnUrlsCombo.setTextInputAllowed(true);
        svnUrlsCombo.setNewItemsAllowed(true);
        svnUrlsCombo.setStyleName("svnPathsStyle");
        svnUrlsCombo.addValueChangeListener(new Property.ValueChangeListener() {
			private static final long serialVersionUID = -5963200533041017441L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				String svnUrl = convertSvnUrl((String)svnUrlsCombo.getValue());
				setSVNProperties(svnUrl);
			}
		});
        
        svnPath = new TextField(); 
    	svnPath.setStyleName("dndSvnPathLabel");
        svnPath.setImmediate(true);
        svnPath.setValue("");
        svnPath.addValueChangeListener(new Property.ValueChangeListener() {
			private static final long serialVersionUID = -5963200533041017441L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				setSVNProperties(convertSvnUrl(svnPath.getValue()));
			}
		});
        
        
    	svnUser = new TextField("SVN user: ");       
    	svnUser.setStyleName("dndSvnUserLabel");
        svnUser.setImmediate(true);
        svnUser.setValue("");
        
    	svnPassword = new PasswordField("SVN password: ");       
    	svnPassword.setStyleName("dndSvnPasswordLabel");
        svnPassword.setImmediate(true);
        svnPassword.setValue("");
        
        checkSVNLocation = new Button("Test");
        checkSVNLocation.setStyleName("svnTestButtonStyle");
        checkSVNLocation.addClickListener(new Button.ClickListener() {
			private static final long serialVersionUID = 5610727568971157808L;

			public void buttonClick(ClickEvent event) {
                evidenceFileUploader.getArtefactFileDetails().getSvnProperties().setSvnUser(svnUser.getValue());
                evidenceFileUploader.getArtefactFileDetails().getSvnProperties().setSvnPassword(svnPassword.getValue());
            	boolean connectionOK = evidenceFileUploader.checkSVNLocation();
            	if (connectionOK) {
            		evidenceFileUploader.infoNotification(EvidenceFileUploader.connectionToSvnCorrect);
            	} else {
            		evidenceFileUploader.warningNotification(EvidenceFileUploader.connectionToSvnNotCorrect);
            	}
            }
        });
        
        HorizontalLayout svnCheckLayout = new HorizontalLayout();
        svnCheckLayout.addComponent(svnPassword);
        svnCheckLayout.addComponent(checkSVNLocation);
        
        svnFieldsLayout.addComponent(svnPathLabel);
        svnFieldsLayout.addComponent(svnUrlsCombo);
        svnFieldsLayout.addComponent(svnPath);
        svnFieldsLayout.addComponent(svnUser);
        svnFieldsLayout.addComponent(svnCheckLayout);
        
        svnFieldsLayout.addComponent(bottomEvidenceNote);
    	
    	return svnFieldsLayout;
    }
    
    private String convertSvnUrl(String svnUrl) {
    	if (svnUrl == null || "".equals(svnUrl)) {
    		return svnUrl;
    	}
    	
		if (svnUrl.endsWith("/") || svnUrl.endsWith("\\")) {
			svnUrl = svnUrl.substring(0, svnUrl.length() -1 );
			return convertSvnUrl(svnUrl);
		} else {
			return svnUrl;
		}
    }
    
    private HorizontalLayout createAssignAndCancelButtons(final EvidenceFileUploader evidenceFileUploader)
    {
    	if (uploadFileMode == UploadFileMode.MODIFY_FILE) {
    		assignButton = new Button("Modify");
    	} else {
    		assignButton = new Button("Assign");
    	}
        
        assignButton.setStyleName("assignAndCancelButtonStyle");
        assignButton.addClickListener((e) ->
        {
            String fileName = evidenceFileUploader.getArtefactFileDetails().getFileName();
            if (!isDragAndDropFunctionalityEnabled && (fileName == null || ("").equals(fileName))) {
                evidenceFileUploader.errorNotification(EvidenceFileUploader.emptyEvidenceFileNameMsg);
                return;
            }

            if (uploadFileMode != UploadFileMode.MODIFY_FILE) {
	            if (complianceGroup.getValue() == null) {
	                evidenceFileUploader.errorNotification(EvidenceFileUploader.emptyComplianceGroup);
	                return;
	            }
	
	            if (complianceJustificationArea.getValue() == null || complianceJustificationArea.getValue().equals("")) {
	                evidenceFileUploader.errorNotification(EvidenceFileUploader.emptyJustificationArea);
	                return;
	            }
            }
            
            if (modificationDate.getValue() == null) {
            	evidenceFileUploader.errorNotification(EvidenceFileUploader.wrongModificationDate);
                return;
            }

            evidenceFileUploader.getArtefactFileDetails().setResourceName(resourceName.getValue());
            evidenceFileUploader.getArtefactFileDetails().setArtefactName(artefactName.getValue());
            evidenceFileUploader.getArtefactFileDetails().setJustification(complianceJustificationArea.getValue());
    		evidenceFileUploader.getArtefactFileDetails().setModificationDate(modificationDate.getValue());
            
            if (uploadFileMode == UploadFileMode.ADD_FILE) {
            	evidenceFileUploader.getArtefactFileDetails().setComplianceValue(ComplianceLevel.getTypeByValue(complianceGroup.getValue().toString()));
            }
            
            String svnUrlValue = "";
            if (svnPath.isVisible()) {
            	svnUrlValue = convertSvnUrl(svnPath.getValue());
            } else {
            	svnUrlValue = convertSvnUrl((String)svnUrlsCombo.getValue());
            }
            String svnUserValue = svnUser.getValue();
            String svnPasswordValue = svnPassword.getValue();
            
            if (svnUrlValue == null || "".equals(svnUrlValue)) {
            	evidenceFileUploader.errorNotification(EvidenceFileUploader.emptySvnUrlMsg);
                return;
            }
            if (svnUserValue == null || "".equals(svnUserValue)) {
            	evidenceFileUploader.errorNotification(EvidenceFileUploader.emptySvnUserMsg);
                return;
            }
            
            evidenceFileUploader.getArtefactFileDetails().getSvnProperties().setSvnUrl(svnUrlValue);
            evidenceFileUploader.getArtefactFileDetails().getSvnProperties().setSvnUser(svnUserValue);
            evidenceFileUploader.getArtefactFileDetails().getSvnProperties().setSvnPassword(svnPasswordValue);

            if (isDragAndDropFunctionalityEnabled) {
            	//drag and drop
                evidenceFileUploader.commitAfterDragAndDrop(fileByteArray);
            } else {
            	//open a file
                evidenceFileUploader.getUpload().submitUpload();
            }
            
            evidenceFileUploader.saveSVNPropertiesDataToCookies();
            
            complianceGroup.select(null);
            complianceJustificationArea.setValue("");
        });

        Button cancelButton = new Button("Cancel");
        cancelButton.setStyleName("assignAndCancelButtonStyle");
        cancelButton.addClickListener((e) -> this.close());

        HorizontalLayout assignAndCancelButtonsLayout = new HorizontalLayout();
        assignAndCancelButtonsLayout.addComponent(assignButton);
        assignAndCancelButtonsLayout.addComponent(cancelButton);
        
        return assignAndCancelButtonsLayout;
    }
    
    private void setSVNProperties(String svnUrl)
    {
    	evidenceFileUploader.getArtefactFileDetails().getSvnProperties().setSvnUrl(svnUrl);
		
		if (svnPropertiesFromCookies != null && svnUrl != null && svnUrl.equals(svnPropertiesFromCookies.getSvnUrl())) {
			//update svnUser and svnPassword from cookies:
			svnUser.setValue(svnPropertiesFromCookies.getSvnUser());
			svnPassword.setValue(svnPropertiesFromCookies.getSvnPassword());
			evidenceFileUploader.getArtefactFileDetails().getSvnProperties().setSvnUser(svnPropertiesFromCookies.getSvnUser());
			evidenceFileUploader.getArtefactFileDetails().getSvnProperties().setSvnPassword(svnPropertiesFromCookies.getSvnPassword());
		} else {
			svnUser.setValue("");
			svnPassword.setValue("");
			evidenceFileUploader.getArtefactFileDetails().getSvnProperties().setSvnUser("");
			evidenceFileUploader.getArtefactFileDetails().getSvnProperties().setSvnPassword("");
		}
		
		bottomEvidenceNote.setValue(generateBottomEvidenceNoteValue());
    }
    
    public String generateBottomEvidenceNoteValue()
    {	
        if (evidenceFileUploader.getArtefactFileDetails().getSvnProperties() == null
                || evidenceFileUploader.getArtefactFileDetails().getSvnProperties().getSvnUrl() == null) {
            return "Note: this will commit your evidence file to SVN and assign as a compliance evidence";
        }

        if (uploadFileMode == UploadFileMode.MODIFY_FILE) {
        	return "Note: this will commit your evidence file to SVN <br>(" + evidenceFileUploader.getArtefactFileDetails().getSvnProperties().getSvnUrl()
                    + ") <br>and update the compliance information.";
        } else {
        	return "Note: this will commit your evidence file to SVN <br>(" + evidenceFileUploader.getArtefactFileDetails().getSvnProperties().getSvnUrl()
                + ") <br>and assign it as a compliance evidence.";
        }
    }
    
    public void createSvnPropertiesData() {
    	//readCookies();
    	svnPropertiesFromCookies = getSvnPropertiesFromCookies();
    	
    	if (uploadFileMode == UploadFileMode.MODIFY_FILE && complianceResource != null) {
    		String svnUrl = complianceResource.getRepoUrl();
    		if (svnPropertiesFromCookies != null && svnPropertiesFromCookies.getSvnUrl().equals(svnUrl)) {
				createOneSvnPropertiesData(svnUrl, svnPropertiesFromCookies.getSvnUser(), svnPropertiesFromCookies.getSvnPassword());
			} else {
				createOneSvnPropertiesData(svnUrl, "", "");
			}
    		svnPath.setEnabled(false);
    	} else {
	    	//Svn from server settings
	        createDefaultSVNProperties();
	        //Svn from Model Artefacts
	        List<SvnProperties> svnPropertiesDataFromProject = null;
	        
	        if (evidenceFileUploader.getArtefactFileDetails().getProjectName() != null && 
	        		(!"".equals(evidenceFileUploader.getArtefactFileDetails().getProjectName()))) {
		    	try {
		    		svnPropertiesDataFromProject = evidenceFileUploader.searchSvnPropertiesFromProject();
				} catch (Exception e) {
					e.printStackTrace();
				}
	        }
	    	String defaultSvnUrl = evidenceFileUploader.getArtefactFileDetails().getSvnProperties().getSvnUrl();
	    	String defaultSvnUser = evidenceFileUploader.getArtefactFileDetails().getSvnProperties().getSvnUser();
	    	String defaultSvnPassword = evidenceFileUploader.getArtefactFileDetails().getSvnProperties().getSvnPassword();
	    	
			if (svnPropertiesDataFromProject == null || svnPropertiesDataFromProject.size() == 0) {
				createOneSvnPropertiesData(defaultSvnUrl, defaultSvnUser, defaultSvnPassword);
			} else if (svnPropertiesDataFromProject.size() == 1) {
				String svnUrl = svnPropertiesDataFromProject.get(0).getSvnUrl();
				if ("".equals(defaultSvnUrl) || svnUrl.equals(defaultSvnUrl)) {
					if (svnPropertiesFromCookies != null && svnPropertiesFromCookies.getSvnUrl().equals(svnUrl)) {
						createOneSvnPropertiesData(svnUrl, svnPropertiesFromCookies.getSvnUser(), svnPropertiesFromCookies.getSvnPassword());
					} else {
						createOneSvnPropertiesData(svnPropertiesDataFromProject.get(0).getSvnUrl(), "", "");
					}
				} else {
					createMoreSvnPropertiesData(defaultSvnUrl, svnPropertiesDataFromProject);
				}
			} else if (svnPropertiesDataFromProject.size() > 1) {
				createMoreSvnPropertiesData(defaultSvnUrl, svnPropertiesDataFromProject);
			}
    	}
    }
    
    private void createOneSvnPropertiesData(String url, String user, String password) {
		svnUrlsCombo.setVisible(false);
		svnPath.setVisible(true);
		svnPath.setEnabled(true);
		
		svnPath.setValue(url);
		svnUser.setValue(user);
		svnPassword.setValue(password);
		
		evidenceFileUploader.getArtefactFileDetails().getSvnProperties().setSvnUrl(url);
    }
    
    private void createMoreSvnPropertiesData(String defaultSvnUrl, List<SvnProperties> svnPropertiesDataFromProject) {
		svnUrlsCombo.removeAllItems();
		svnPath.setVisible(false);
		svnUrlsCombo.setVisible(true);
		boolean defaultUrlAdded = false;
		
		for (SvnProperties svnProperties : svnPropertiesDataFromProject) {
			svnUrlsCombo.addItem(svnProperties.getSvnUrl());
			if (defaultSvnUrl.equals(svnProperties.getSvnUrl())) {
				defaultUrlAdded = true;
			}
		}
		if (!defaultUrlAdded && !"".equals(defaultSvnUrl)) {
			svnUrlsCombo.addItem(defaultSvnUrl);
		}
		
		//select proper svn url
		if (svnPropertiesFromCookies != null) {
			selectSvnProperties(defaultSvnUrl, svnPropertiesFromCookies, svnPropertiesDataFromProject);
		} else {
			svnUrlsCombo.select(svnPropertiesDataFromProject.get(0).getSvnUrl());
			svnUser.setValue("");
			svnPassword.setValue("");
		}
    }
    
    private void selectSvnProperties(String defaultSvnUrl, SvnProperties svnPropertiesFromCookies, List<SvnProperties> svnPropertiesDataFromProject) {
    	
    	boolean setFromCookies = false;
    	String svnUrlFromCookies = svnPropertiesFromCookies.getSvnUrl();
    	for (SvnProperties svnProperties : svnPropertiesDataFromProject) {
			String svnUrl = svnProperties.getSvnUrl();
			if (svnUrlFromCookies.equals(svnUrl)) {
				svnUrlsCombo.select(svnUrl);
				//set svnUser and svnPassword from cookies: 
				svnUser.setValue(svnPropertiesFromCookies.getSvnUser());
				svnPassword.setValue(svnPropertiesFromCookies.getSvnPassword());
				setFromCookies = true;
				break;
			}
		}

    	if (!setFromCookies) {
			if (defaultSvnUrl.equals(svnUrlFromCookies)) {
				svnUrlsCombo.select(defaultSvnUrl);
				svnUser.setValue(svnPropertiesFromCookies.getSvnUser());
				svnPassword.setValue(svnPropertiesFromCookies.getSvnPassword());
			} else {
				svnUrlsCombo.select(svnPropertiesDataFromProject.get(0).getSvnUrl());
				svnUser.setValue("");
				svnPassword.setValue("");
			}
		}
    }
    
    private SvnProperties getSvnPropertiesFromCookies() {
    	String urlFromCookies = "";
    	String userFromCookies = "";
    	String passwordFromCookies = "";
    	
    	Cookie[] cookies = ((VaadinServletRequest) VaadinService.getCurrentRequest()).getCookies();
        for (Cookie cookie : cookies) {
        	if ("svnUrl".equals(cookie.getName())) {
        		urlFromCookies = readSvnPropertyFromCookies(cookie.getValue());
        	}
        	if ("svnUser".equals(cookie.getName())) {
        		userFromCookies = readSvnPropertyFromCookies(cookie.getValue());
        	}
        	if ("svnPassword".equals(cookie.getName())) {
        		passwordFromCookies = readSvnPropertyFromCookies(cookie.getValue());
        	}
        }
        if (!"".equals(urlFromCookies) && (!"".equals(userFromCookies)) && (!"".equals(passwordFromCookies))) {
        	return new SvnProperties(urlFromCookies, userFromCookies, passwordFromCookies);
        }
        
        return null;
    }
    
    private String readSvnPropertyFromCookies(String cookieValue) {
    	String valueFromCookies = "";
    	try {
			String decoded = URLDecoder.decode(cookieValue, "UTF-8");
			valueFromCookies = decoded;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
    	
    	return valueFromCookies;
    }
    
    private void createDefaultSVNProperties()
    {
    	evidenceFileUploader.readSVNPropertiesFromServerSettings();
    	String svnUrlFromServerSettings = evidenceFileUploader.getArtefactFileDetails().getSvnProperties().getSvnUrl();
    	String svnPath = "";
    	
    	if (svnUrlFromServerSettings != null && (!"".equals(svnUrlFromServerSettings))) {
    		if (svnUrlFromServerSettings.endsWith("/")) {
    			svnPath = svnUrlFromServerSettings  + IUploadFile.INSERTED_BY_OPENCERT_WEB + "/" + evidenceFileUploader.getArtefactFileDetails().getProjectName();
    		} else {
    			svnPath = svnUrlFromServerSettings  + "/" + IUploadFile.INSERTED_BY_OPENCERT_WEB + "/" + evidenceFileUploader.getArtefactFileDetails().getProjectName();
    		}
    	}
    	
    	evidenceFileUploader.getArtefactFileDetails().getSvnProperties().setSvnUrl(svnPath);
    	
    	if (evidenceFileUploader.getArtefactFileDetails().getSvnProperties().getSvnUser() == null) {
    		evidenceFileUploader.getArtefactFileDetails().getSvnProperties().setSvnUser("");
    	}
    	
    	if (evidenceFileUploader.getArtefactFileDetails().getSvnProperties().getSvnPassword() == null) {
    		evidenceFileUploader.getArtefactFileDetails().getSvnProperties().setSvnPassword("");
    	}
    }
    
    public void setInfoMessage(String baseAssetName)
    {
        secondTopInfoLabel.setValue(_MSG_FOR_COMPLIANCE_WITH_THE_FOLLOWING_BASELINE_ITEM_BR_B + baseAssetName + "\"");
    }

	public UploadFileMode getUploadFileMode()
    {
    	return uploadFileMode;
    }
	
	public void setFileByteArray(byte[] fileByteArray) 
	{
		this.fileByteArray = fileByteArray;
	}
		
    public TextField getArtefactName()
    {
        return artefactName;
    }

    public TextField getResourceName()
    {
        return resourceName;
    }
    
    public ComplianceAssetResource getComplianceResource()
    {
    	return complianceResource;
    }

    public List<UnassignOrAssignArtefactListener> getUnassignOrAssignArtefactListeners()
    {
        return unassignOrAssignArtefactListeners;
    }

    public void addUnassignOrAssignArtefactListener(UnassignOrAssignArtefactListener unassignOrAssignArtefactListener)
    {
        this.unassignOrAssignArtefactListeners.add(unassignOrAssignArtefactListener);
    }
    
    public EvidenceFileUploader getEvidenceFileUploader()
    {
        return evidenceFileUploader;
    }
    
    public Label getBottomEvidenceNote()
    {
    	return bottomEvidenceNote;
    }
    
    public Label getDragAndDropFileName()
    {
    	return dragAndDropFileName;
    }
    
    public PopupDateField getModificationDate()
    {
    	return modificationDate;
    }
    
    public boolean isDragAndDropFunctionalityEnabled()
    {
        return isDragAndDropFunctionalityEnabled;
    }

    public void setDragAndDropFunctionalityEnabled(boolean isDragAndDropFunctionalityEnabled)
    {
        this.isDragAndDropFunctionalityEnabled = isDragAndDropFunctionalityEnabled;
    }
    
    public void setBaseAssetComplianceStatus(BaseAssetComplianceStatus baseAssetComplianceStatus)
    {
    	evidenceFileUploader.getArtefactFileDetails().setBaseAssetComplianceStatus(baseAssetComplianceStatus);
    }
}
