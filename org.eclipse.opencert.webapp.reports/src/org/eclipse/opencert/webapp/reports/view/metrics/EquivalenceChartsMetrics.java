/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.metrics;

import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.vaadin.annotations.Theme;
import com.vaadin.data.Container.Hierarchical;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.VerticalLayout;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.opencert.webapp.reports.manager.RefframeworkManager;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.webapp.reports.view.common.IRefFrameworksChangeListener;
import org.eclipse.opencert.webapp.reports.view.metrics.JFreeChartWrapper;
import org.jfree.chart.ChartColor;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.TextAnchor;
import org.eclipse.opencert.infra.mappings.mapping.MapKind;
import org.eclipse.opencert.pkm.refframework.refframework.RefActivity;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefact;
import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;
import org.eclipse.opencert.pkm.refframework.refframework.RefEquivalenceMap;
import org.eclipse.opencert.pkm.refframework.refframework.RefFramework;
import org.eclipse.opencert.pkm.refframework.refframework.RefRequirement;
import org.eclipse.opencert.pkm.refframework.refframework.RefRole;
import org.eclipse.opencert.pkm.refframework.refframework.RefTechnique;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;




@Theme("opencerttheme")
public class EquivalenceChartsMetrics 
extends CustomComponent implements IRefFrameworksChangeListener

{

	private static final long serialVersionUID = 2540135928003576252L;
	private static final String REFFRAMEWORK_MAPS = "Refframework Maps";
	private static final String NO_DATA_AVAILABLE = "No Data Available";


	private byte[] reffChart;
    private Hierarchical dataDescription;


	public String getRefframeworkMaps() {
		return REFFRAMEWORK_MAPS;
	}

	public byte[] getChart() {
		return reffChart;
	}

	public Hierarchical getDataDescription() {
		return dataDescription;
	}

	protected class ComplianceInfo{
		protected String id;
		protected String name;
		protected List<String> fullMaps;
		protected List<String> partialMaps;
		protected List<String> noMaps;

		private ComplianceInfo(String id, String name){
			this.id=id;
			this.name=name;
			fullMaps=new ArrayList<String>();
			partialMaps=new ArrayList<String>();
			noMaps=new ArrayList<String>();	
		}

		protected String getId() {
			return id;
		}

		protected String getName() {
			return name;
		}

		protected List<String> getFullMaps() {
			return fullMaps;
		}

		protected void addFullMap(String info){
			fullMaps.add(info);
		}

		protected List<String> getPartialMaps() {
			return partialMaps;
		}

		protected void addPartialMap(String info){
			partialMaps.add(info);
		}

		protected List<String> getNoMaps() {
			return noMaps;
		}

		protected void addNoMap(String info){
			noMaps.add(info);
		}


	}

	protected class Triplet{
		protected int full;
		protected int partial;
		protected int noMap;
		protected List<ComplianceInfo> data;

		protected Triplet(){
			this.full=0;
			this.partial=0;
			this.noMap=0;
			data=new ArrayList<ComplianceInfo>();
		}

		protected int getFull() {
			return full;
		}

		protected void setFull(int full) {
			this.full = full;
		}

		protected int getPartial() {
			return partial;
		}

		protected void setPartial(int partial) {
			this.partial = partial;
		}

		protected int getNoMap() {
			return noMap;
		}

		protected void setNoMap(int noMap) {
			this.noMap = noMap;
		}

		protected List<ComplianceInfo> getData(){
			return data;
		}

		protected void addData(ComplianceInfo e){
			data.add(e);
		}

	}



	public EquivalenceChartsMetrics()
	{      


	}

	public EquivalenceChartsMetrics(long fromFrameworkID, long toFrameworkID)
	{      
		init(fromFrameworkID, toFrameworkID);
	}

	private void init(long fromFrameworkID, long toFrameworkID) {
		VerticalLayout mainPanel = new VerticalLayout();

		mainPanel.setStyleName("chartsSpacing");
		mainPanel.setSpacing(true);


		RefframeworkManager refframeworkManager = (RefframeworkManager)SpringContextHelper.getBeanFromWebappContext(RefframeworkManager.SPRING_NAME);

		Triplet req = countMapRequirements(refframeworkManager.getRefRequirements(fromFrameworkID,true), toFrameworkID);
		Triplet art = countMapArtefacts(refframeworkManager.getRefArtefacts(fromFrameworkID), toFrameworkID);
		Triplet roles = countMapRoles(refframeworkManager.getRefRoles(fromFrameworkID), toFrameworkID);
		Triplet tech = countMapTechniques(refframeworkManager.getRefTechniques(fromFrameworkID), toFrameworkID);
		Triplet act = countMapActivities(refframeworkManager.getRefActivities(fromFrameworkID,true), toFrameworkID);

		mainPanel.addComponent(newDataSet(populateWithData(req,art,roles,tech,act)));
		mainPanel.addComponent(newTableInfo(req,art,roles,tech,act));


		//add mainPanel component
		setCompositionRoot(mainPanel);
		setSizeFull();
	}












	private Component newTableInfo(Triplet req, Triplet art, Triplet rol, Triplet tech, Triplet act) {

		final TreeTable treetable = new TreeTable();
		treetable.setWidth("80%");
		treetable.addStyleName("resourceEfficiencyTable");

		// Define two columns for the built-in container
		treetable.addContainerProperty("Maps", String.class, "");
		treetable.addContainerProperty("Description",  String.class, "");

		Object requirement = treetable.addItem(new Object[]{"Requirements: ", ""},null);
		Object artefacts = treetable.addItem(new Object[]{"Artefacts: ", ""},null);
		Object roles = treetable.addItem(new Object[]{"Roles: ", ""},null);
		Object techniques = treetable.addItem(new Object[]{"Techniques: ", ""},null);
		Object activities = treetable.addItem(new Object[]{"Activities: ", ""},null);

		treetable.setCollapsed(requirement, false);
		treetable.setCollapsed(artefacts, false);
		treetable.setCollapsed(roles, false);
		treetable.setCollapsed(techniques, false);
		treetable.setCollapsed(activities, false);

		List<ComplianceInfo> reqInfo = req.getData();
		if(!reqInfo.isEmpty()){
			for(ComplianceInfo inf : reqInfo){
				Object r = treetable.addItem(new Object[]{"ID: " + inf.getId() + " Name: " + inf.getName(), ""},null);
				treetable.setParent(r,requirement);
				treetable.setCollapsed(r, false);

				Object fullMaps = treetable.addItem(new Object[]{"Full Maps:", ""},null);
				treetable.setParent(fullMaps,r);
				treetable.setCollapsed(fullMaps, false);
				List<String> fm = inf.getFullMaps();
				if(!fm.isEmpty()){
					for(String s : fm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,fullMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}

				Object partialMaps = treetable.addItem(new Object[]{"Partial Maps:", ""},null);
				treetable.setParent(partialMaps,r);
				treetable.setCollapsed(partialMaps, false);
				List<String> pm = inf.getPartialMaps();
				if(!pm.isEmpty()){
					for(String s : pm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,partialMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}

				Object noMaps = treetable.addItem(new Object[]{"No Maps:", ""},null);
				treetable.setParent(noMaps,r);
				treetable.setCollapsed(noMaps, false);
				List<String> nm = inf.getNoMaps();
				if(!nm.isEmpty()){
					for(String s : nm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,noMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}

			}
		}

		List<ComplianceInfo> artInfo = art.getData();
		if(!artInfo.isEmpty()){
			for(ComplianceInfo inf : artInfo){
				Object r = treetable.addItem(new Object[]{"ID: " + inf.getId() + " Name: " + inf.getName(), ""},null);
				treetable.setParent(r,artefacts);
				treetable.setCollapsed(r, false);

				Object fullMaps = treetable.addItem(new Object[]{"Full Maps:", ""},null);
				treetable.setParent(fullMaps,r);
				treetable.setCollapsed(fullMaps, false);
				List<String> fm = inf.getFullMaps();
				if(!fm.isEmpty()){
					for(String s : fm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,fullMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}

				Object partialMaps = treetable.addItem(new Object[]{"Partial Maps:", ""},null);
				treetable.setParent(partialMaps,r);
				treetable.setCollapsed(partialMaps, false);
				List<String> pm = inf.getPartialMaps();
				if(!pm.isEmpty()){
					for(String s : pm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,partialMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}

				Object noMaps = treetable.addItem(new Object[]{"No Maps:", ""},null);
				treetable.setParent(noMaps,r);
				treetable.setCollapsed(noMaps, false);
				List<String> nm = inf.getNoMaps();
				if(!nm.isEmpty()){
					for(String s : nm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,noMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}

			}
		}


		List<ComplianceInfo> rolInfo = rol.getData();
		if(!rolInfo.isEmpty()){
			for(ComplianceInfo inf : rolInfo){
				Object r = treetable.addItem(new Object[]{"ID: " + inf.getId() + " Name: " + inf.getName(), ""},null);
				treetable.setParent(r,roles);
				treetable.setCollapsed(r, false);

				Object fullMaps = treetable.addItem(new Object[]{"Full Maps:", ""},null);
				treetable.setParent(fullMaps,r);
				treetable.setCollapsed(fullMaps, false);
				List<String> fm = inf.getFullMaps();
				if(!fm.isEmpty()){
					for(String s : fm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,fullMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}

				Object partialMaps = treetable.addItem(new Object[]{"Partial Maps:", ""},null);
				treetable.setParent(partialMaps,r);
				treetable.setCollapsed(partialMaps, false);
				List<String> pm = inf.getPartialMaps();
				if(!pm.isEmpty()){
					for(String s : pm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,partialMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}

				Object noMaps = treetable.addItem(new Object[]{"No Maps:", ""},null);
				treetable.setParent(noMaps,r);
				treetable.setCollapsed(noMaps, false);
				List<String> nm = inf.getNoMaps();
				if(!nm.isEmpty()){
					for(String s : nm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,noMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}

			}
		}

		List<ComplianceInfo> techInfo = tech.getData();
		if(!techInfo.isEmpty()){
			for(ComplianceInfo inf : techInfo){
				Object r = treetable.addItem(new Object[]{"ID: " + inf.getId() + " Name: " + inf.getName(), ""},null);
				treetable.setParent(r,techniques);
				treetable.setCollapsed(r, false);

				Object fullMaps = treetable.addItem(new Object[]{"Full Maps:", ""},null);
				treetable.setParent(fullMaps,r);
				treetable.setCollapsed(fullMaps, false);
				List<String> fm = inf.getFullMaps();
				if(!fm.isEmpty()){
					for(String s : fm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,fullMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}

				Object partialMaps = treetable.addItem(new Object[]{"Partial Maps:", ""},null);
				treetable.setParent(partialMaps,r);
				treetable.setCollapsed(partialMaps, false);
				List<String> pm = inf.getPartialMaps();
				if(!pm.isEmpty()){
					for(String s : pm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,partialMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}

				Object noMaps = treetable.addItem(new Object[]{"No Maps:", ""},null);
				treetable.setParent(noMaps,r);
				treetable.setCollapsed(noMaps, false);
				List<String> nm = inf.getNoMaps();
				if(!nm.isEmpty()){
					for(String s : nm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,noMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}

			}
		}

		List<ComplianceInfo> actInfo = act.getData();
		if(!actInfo.isEmpty()){
			for(ComplianceInfo inf : actInfo){
				Object r = treetable.addItem(new Object[]{"ID: " + inf.getId() + " Name: " + inf.getName(), ""},null);
				treetable.setParent(r,activities);
				treetable.setCollapsed(r, false);

				Object fullMaps = treetable.addItem(new Object[]{"Full Maps:", ""},null);
				treetable.setParent(fullMaps,r);
				treetable.setCollapsed(fullMaps, false);
				List<String> fm = inf.getFullMaps();
				if(!fm.isEmpty()){
					for(String s : fm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,fullMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}

				Object partialMaps = treetable.addItem(new Object[]{"Partial Maps:", ""},null);
				treetable.setParent(partialMaps,r);
				treetable.setCollapsed(partialMaps, false);
				List<String> pm = inf.getPartialMaps();
				if(!pm.isEmpty()){
					for(String s : pm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,partialMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}

				Object noMaps = treetable.addItem(new Object[]{"No Maps:", ""},null);
				treetable.setParent(noMaps,r);
				treetable.setCollapsed(noMaps, false);
				List<String> nm = inf.getNoMaps();
				if(!nm.isEmpty()){
					for(String s : nm){
						String[] sp = s.split("@");
						Object i = treetable.addItem(new Object[]{"ID: " + sp[0] + " Name: " + sp[1], sp[2]},null);
						treetable.setParent(i,noMaps);
						treetable.setChildrenAllowed(i,false);
					}
				}

			}
		}

		dataDescription = treetable.getContainerDataSource();
		
		return treetable;
	}

	@Override
	public void setEquivalenceFrameworks(long fromFrameworkID, long toFrameworkID) {
		init(fromFrameworkID,toFrameworkID);

	}

	private DefaultCategoryDataset populateWithData(Triplet requirements,
			Triplet artefacts, Triplet roles, Triplet techniques,
			Triplet activities) {

		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		dataset.addValue(requirements.getFull(), "Full Map", "Requirements");
		dataset.addValue(requirements.getPartial(), "Partial Map", "Requirements");
		dataset.addValue(requirements.getNoMap(), "No Map", "Requirements");

		dataset.addValue(artefacts.getFull(), "Full Map", "Artefacts");
		dataset.addValue(artefacts.getPartial(), "Partial Map", "Artefacts");
		dataset.addValue(artefacts.getNoMap(), "No Map", "Artefacts");

		dataset.addValue(roles.getFull(), "Full Map", "Roles");
		dataset.addValue(roles.getPartial(), "Partial Map", "Roles");
		dataset.addValue(roles.getNoMap(), "No Map", "Roles");

		dataset.addValue(techniques.getFull(), "Full Map", "Techniques");
		dataset.addValue(techniques.getPartial(), "Partial Map", "Techniques");
		dataset.addValue(techniques.getNoMap(), "No Map", "Techniques");

		dataset.addValue(activities.getFull(), "Full Map", "Activities");
		dataset.addValue(activities.getPartial(), "Partial Map", "Activities");
		dataset.addValue(activities.getNoMap(), "No Map", "Activities");

		return dataset;
	}


	private Component newDataSet(final DefaultCategoryDataset dataset) {



		final JFreeChart chart = ChartFactory.createBarChart(
				REFFRAMEWORK_MAPS,      // chart title
				"Type of Equivalence Map",               // domain axis label
				"Number of Maps", // range axis label
				dataset,                  // data
				PlotOrientation.VERTICAL, // orientation
				true,                     // include legend
				true,                     // tooltips
				false                     // urls
				);

		// get a reference to the plot for further customisation...
		final CategoryPlot plot = chart.getCategoryPlot();
		plot.setNoDataMessage(NO_DATA_AVAILABLE);
		plot.setNoDataMessageFont(new Font("Arial",Font.BOLD,14));
		plot.setNoDataMessagePaint(Color.RED);  

		plot.setBackgroundPaint(Color.white);
		plot.setDomainGridlinePaint(Color.black);
		plot.setRangeGridlinePaint(Color.black);
		((BarRenderer) plot.getRenderer()).setBarPainter(new StandardBarPainter());

		// set the range axis to display integers only...
		final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

		final BarRenderer renderer = (BarRenderer) plot.getRenderer();
		renderer.setDrawBarOutline(false);
		renderer.setBaseItemLabelGenerator(
				new StandardCategoryItemLabelGenerator());
		renderer.setBaseItemLabelsVisible(true);
		renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
				ItemLabelAnchor.CENTER, TextAnchor.CENTER));       
		renderer.setPositiveItemLabelPositionFallback(new ItemLabelPosition(
				ItemLabelAnchor.OUTSIDE12, TextAnchor.CENTER));


		final CategoryItemRenderer categoryRenderer = plot.getRenderer();
		categoryRenderer.setBaseItemLabelsVisible(true);
		categoryRenderer.setSeriesPaint(0, ChartColor.LIGHT_GREEN);
		categoryRenderer.setSeriesPaint(1, ChartColor.LIGHT_YELLOW);
		categoryRenderer.setSeriesPaint(2, ChartColor.LIGHT_RED);

		chart.setBackgroundPaint(Color.white);
		chart.setAntiAlias(true);
		chart.setBorderVisible(false);

		
		  try {
			  reffChart = ChartUtilities.encodeAsPNG(chart.createBufferedImage(600, 600));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		Component chartWrapper = new JFreeChartWrapper(chart);
		chartWrapper.setHeight("80%");
		chartWrapper.setWidth("80%");
		chartWrapper.setStyleName("leftChartsSpacing");
		return chartWrapper;


	}


	private Triplet countMapActivities(List<RefActivity> refActivities, long toFrameworkID) {
		Triplet t = new Triplet();
		for(RefActivity refActivity : refActivities){
			boolean hasTarget = false;
			ComplianceInfo e = new ComplianceInfo(refActivity.getId(),refActivity.getName());
			List<RefEquivalenceMap> mapList = refActivity.getEquivalence();
			for(RefEquivalenceMap map : mapList){
				List<RefAssurableElement> targets = map.getTarget();
				if(targets!=null&&!targets.isEmpty()){
					
					for(RefAssurableElement refAssurableElement : targets){
									
						EObject o = refAssurableElement.eContainer();
						while (!(o instanceof RefFramework)){
							o = o.eContainer();
						}
						
						long refId = CDOStorageUtil.getCDOId(o);
						
						if(toFrameworkID == refId){
							if (!hasTarget) hasTarget=true;
							MapKind kind = map.getType();
							String literal = kind.getLiteral();  
							String exp = map.getMapJustification()==null ? " " : map.getMapJustification().getExplanation();
							if(literal.equals("full")) {
								t.setFull(t.getFull()+1);
								e.addFullMap(map.getId()+"@"+map.getName()+"@"+exp);
							}
							else if(literal.equals("partial")){
								t.setPartial(t.getPartial()+1);
								e.addPartialMap(map.getId()+"@"+map.getName()+"@"+exp);
							}
							else if(literal.equals("noMap")){
								t.setNoMap(t.getNoMap()+1);
								e.addNoMap(map.getId()+"@"+map.getName()+"@"+exp);
							}
						}
					}
				}
			}
			if (hasTarget) t.addData(e);
		}
		return t;
	}

	private Triplet countMapTechniques(List<RefTechnique> refTechniques, long toFrameworkID) {
		Triplet t = new Triplet();
		for(RefTechnique refTechnique : refTechniques){
			boolean hasTarget = false;
			ComplianceInfo e = new ComplianceInfo(refTechnique.getId(),refTechnique.getName());
			List<RefEquivalenceMap> mapList = refTechnique.getEquivalence();
			for(RefEquivalenceMap map : mapList){
				List<RefAssurableElement> targets = map.getTarget();
				if(targets!=null&&!targets.isEmpty()){
					
					for(RefAssurableElement refAssurableElement : targets){
									
						EObject o = refAssurableElement.eContainer();
						while (!(o instanceof RefFramework)){
							o = o.eContainer();
						}
						
						long refId = CDOStorageUtil.getCDOId(o);
						
						if(toFrameworkID == refId){
							if (!hasTarget) hasTarget=true;
							MapKind kind = map.getType();
							String literal = kind.getLiteral();  
							String exp = map.getMapJustification()==null ? " " : map.getMapJustification().getExplanation();
							if(literal.equals("full")) {
								t.setFull(t.getFull()+1);
								e.addFullMap(map.getId()+"@"+map.getName()+"@"+exp);
							}
							else if(literal.equals("partial")){
								t.setPartial(t.getPartial()+1);
								e.addPartialMap(map.getId()+"@"+map.getName()+"@"+exp);
							}
							else if(literal.equals("noMap")){
								t.setNoMap(t.getNoMap()+1);
								e.addNoMap(map.getId()+"@"+map.getName()+"@"+exp);
							}
						}
					}
				}
			}
			if (hasTarget) t.addData(e);
		}
		return t;
	}

	private Triplet countMapRoles(List<RefRole> refRoles, long toFrameworkID) {
		Triplet t = new Triplet();
		for(RefRole refRole : refRoles){
			boolean hasTarget = false;
			ComplianceInfo e = new ComplianceInfo(refRole.getId(),refRole.getName());
			List<RefEquivalenceMap> mapList = refRole.getEquivalence();
			for(RefEquivalenceMap map : mapList){
				
				List<RefAssurableElement> targets = map.getTarget();
				if(targets!=null&&!targets.isEmpty()){
					
					for(RefAssurableElement refAssurableElement : targets){
									
						EObject o = refAssurableElement.eContainer();
						while (!(o instanceof RefFramework)){
							o = o.eContainer();
						}
						
						long refId = CDOStorageUtil.getCDOId(o);
						
						if(toFrameworkID == refId){
							if (!hasTarget) hasTarget=true;
							MapKind kind = map.getType();
							String literal = kind.getLiteral(); 
							String exp = map.getMapJustification()==null ? " " : map.getMapJustification().getExplanation(); 
							if(literal.equals("full")) {
								t.setFull(t.getFull()+1);
								e.addFullMap(map.getId()+"@"+map.getName()+"@"+exp);
							}
							else if(literal.equals("partial")){
								t.setPartial(t.getPartial()+1);
								e.addPartialMap(map.getId()+"@"+map.getName()+"@"+exp);
							}
							else if(literal.equals("noMap")){
								t.setNoMap(t.getNoMap()+1);
								e.addNoMap(map.getId()+"@"+map.getName()+"@"+exp);
							}
						}
					}
				}
			}
			if (hasTarget) t.addData(e);
		}
		return t;
	}

	private Triplet countMapArtefacts(List<RefArtefact> refArtefacts, long toFrameworkID) {
		Triplet t = new Triplet();
		for(RefArtefact refArtefact : refArtefacts){
			boolean hasTarget = false;
			ComplianceInfo e = new ComplianceInfo(refArtefact.getId(),refArtefact.getName());
			List<RefEquivalenceMap> mapList = refArtefact.getEquivalence();
			for(RefEquivalenceMap map : mapList){
				List<RefAssurableElement> targets = map.getTarget();
				if(targets!=null&&!targets.isEmpty()){
					
					for(RefAssurableElement refAssurableElement : targets){
									
						EObject o = refAssurableElement.eContainer();
						while (!(o instanceof RefFramework)){
							o = o.eContainer();
						}
						
						long refId = CDOStorageUtil.getCDOId(o);
						
						if(toFrameworkID == refId){
							if (!hasTarget) hasTarget=true;
							MapKind kind = map.getType();
							String literal = kind.getLiteral(); 
							String exp = map.getMapJustification()==null ? " " : map.getMapJustification().getExplanation(); 
							if(literal.equals("full")) {
								t.setFull(t.getFull()+1);
								e.addFullMap(map.getId()+"@"+map.getName()+"@"+exp);
							}
							else if(literal.equals("partial")){
								t.setPartial(t.getPartial()+1);
								e.addPartialMap(map.getId()+"@"+map.getName()+"@"+exp);
							}
							else if(literal.equals("noMap")){
								t.setNoMap(t.getNoMap()+1);
								e.addNoMap(map.getId()+"@"+map.getName()+"@"+exp);
							}
						}
					}
				}
				
			}
			if (hasTarget) t.addData(e);
		}
		return t;
	}

	private Triplet countMapRequirements(List<RefRequirement> refRequirements, long toFrameworkID) {
		Triplet t = new Triplet();
		for(RefRequirement refRequirement : refRequirements){
			boolean hasTarget = false;
			ComplianceInfo e = new ComplianceInfo(refRequirement.getId(),refRequirement.getName());
			List<RefEquivalenceMap> mapList = refRequirement.getEquivalence();
			for(RefEquivalenceMap map : mapList){
				List<RefAssurableElement> targets = map.getTarget();
				if(targets!=null&&!targets.isEmpty()){
					
					for(RefAssurableElement refAssurableElement : targets){
						
						EObject o = refAssurableElement.eContainer();
						while (!(o instanceof RefFramework)){
							o = o.eContainer();
						}
						
						long refId = CDOStorageUtil.getCDOId(o);
						
						if(toFrameworkID == refId){
							if (!hasTarget) hasTarget=true;
							MapKind kind = map.getType();
							String literal = kind.getLiteral(); 
							String exp = map.getMapJustification()==null ? " " : map.getMapJustification().getExplanation();
							if(literal.equals("full")) {
								t.setFull(t.getFull()+1);
								e.addFullMap(map.getId()+"@"+map.getName()+"@"+exp);
							}
							else if(literal.equals("partial")){
								t.setPartial(t.getPartial()+1);
								e.addPartialMap(map.getId()+"@"+map.getName()+"@"+exp);
							}
							else if(literal.equals("noMap")){
								t.setNoMap(t.getNoMap()+1);
								e.addNoMap(map.getId()+"@"+map.getName()+"@"+exp);
							}
						}
					
					}
						
				}
			
				
			}
			if (hasTarget) t.addData(e);
		}
		return t;
	}



}

