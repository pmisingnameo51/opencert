/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.view.common;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.opencert.webapp.reports.containers.AssuranceProjectWrapper;
import org.eclipse.opencert.webapp.reports.listeners.IProjectChangeListener;
import org.eclipse.opencert.webapp.reports.listeners.IReportChangeListener;
import org.eclipse.opencert.webapp.reports.manager.AssuranceProjectManager;
import org.eclipse.opencert.webapp.reports.security.SecurityUtil;
import org.eclipse.opencert.webapp.reports.util.OpencertLogger;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.webapp.reports.util.VaadinUtil;
import org.eclipse.opencert.webapp.reports.webapp.IProjectComboSelector;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;

public class ProjectComboWrapper
    implements IReportChangeListener, IProjectComboSelector
{
    private ComboBox projectsCombo = null;
    private Label projectLabel;
    
    private List<IProjectChangeListener> projectChangeListeners = new LinkedList<IProjectChangeListener>();
    private final String initialProjectName;
    private HorizontalLayout projectsPanel;
    private GUIMode guiMode;    
    
    public ProjectComboWrapper(String initialProjectName, GUIMode guiMode)
    {
        this.initialProjectName = initialProjectName;
        this.guiMode = guiMode;
    }

    public Component getProjectsPanel() 
    {
        if (projectsPanel != null) {
            return projectsPanel;
        }
        projectsPanel = new HorizontalLayout();
        
        AssuranceProjectManager projectManager = 
                (AssuranceProjectManager)SpringContextHelper.getBeanFromWebappContext(AssuranceProjectManager.SPRING_NAME);
        
        final List<AssuranceProjectWrapper> projects = projectManager.getProjects(true);
        BeanItemContainer<AssuranceProjectWrapper> projectsContainer = new BeanItemContainer<AssuranceProjectWrapper>(AssuranceProjectWrapper.class, projects);
        
        projectsCombo = new ComboBox(null, projectsContainer);
        projectsCombo.setItemCaptionPropertyId("name");
        projectsCombo.setPageLength(20);
        VaadinUtil.initComboBox(projectsCombo);
        if (initialProjectName != null)
        {
            @SuppressWarnings("unchecked")
            Collection<AssuranceProjectWrapper> itemIds = (Collection<AssuranceProjectWrapper>)projectsCombo.getItemIds();
            
            AssuranceProjectWrapper comboItemToBeSelected = itemIds.stream().filter(pr -> initialProjectName.equals(pr.getName())).findFirst().orElse(null);
            if (comboItemToBeSelected != null) 
            {
                projectsCombo.select(comboItemToBeSelected);
            } else {
                // such project name is not in the combo - do nothing
            }
        }
        
        projectsCombo.addValueChangeListener(new ValueChangeListener() 
        {
            private static final long serialVersionUID = -8409602655504188462L;

            @Override
            public void valueChange(ValueChangeEvent event)
            {
                fireProjectChanged();       
            }
        });
        
        if (guiMode == GUIMode.NORMAL) 
        {
            projectsCombo.setStyleName("projectCombo");
            
            projectLabel = new Label("Project: ");
            projectLabel.setStyleName("mainStyle");
            projectsPanel.addComponent(projectLabel);
        } 
        else if (guiMode == GUIMode.COMPACT) 
        {
            projectsCombo.setStyleName("projectComboCompact");
            projectsCombo.setDescription("Project");
        }
        
        projectsPanel.addComponent(projectsCombo);
        projectsPanel.setHeight("20px");
        projectsPanel.setStyleName("projectsPanel");
        
        return projectsPanel;
    }
    
    /**
     *  @param projectID CDOID of the project. In case the {projectID} is < 0 - select the first available project.
     */
    @Override
    public String reloadAndSelectProjectCombo(long projectID)
    {
    	String message = "";
        AssuranceProjectManager projectManager = 
                (AssuranceProjectManager)SpringContextHelper.getBeanFromWebappContext(AssuranceProjectManager.SPRING_NAME);
        
        List<AssuranceProjectWrapper> projects = projectManager.getProjects(true);
        
        AssuranceProjectWrapper theProject;
        if (projectID >= 0) {
            List<AssuranceProjectWrapper> theProjs = projects.stream().filter(p -> p.getId()==projectID).collect(Collectors.toList());
            if (theProjs.size() != 1) {
            	theProject = projects.get(0);
            	
            	AssuranceProject project = projectManager.getProject(projectID);
            	String projectName = project.getName();
            	
            	message = "<br><br>You do not have the access right to \"" + projectName + "\"" +  
            				" project.<br> You have access rights to the following projects: " + SecurityUtil.getProjectsAccess() + ".<br> " + 
            				"Please contact the administrator to add permission to this project.";
            } else {
            	theProject = theProjs.get(0);
            }
        } else {
            theProject = projects.get(0);
        }
        
        BeanItemContainer<AssuranceProjectWrapper> projectsContainer = new BeanItemContainer<AssuranceProjectWrapper>(AssuranceProjectWrapper.class, projects);
        projectsCombo.setContainerDataSource(projectsContainer);
       	projectsCombo.setValue(theProject);
       	
       	return message;
    }

    @Override
    public void reportChanged(IReport report)
    {
        if (report == null) {
            return;
        }
        final boolean isProjectSupported = report.isSupportsProjects();
        projectLabel.setVisible(isProjectSupported);
        projectsCombo.setVisible(isProjectSupported);        
    }

    public AssuranceProjectWrapper getSelectedProject()
    {
        AssuranceProjectWrapper project = (AssuranceProjectWrapper)projectsCombo.getValue();
        
        if (project != null) {
            SecurityUtil.verifyAccessRight(SecurityUtil.ACCESS_RIGHT_PROJECT_PREFIX + project.getName());
        }
        
        return project;
    }
    

    void fireProjectChanged() 
    {
        final AssuranceProjectWrapper newProject = getSelectedProject();
        OpencertLogger.info("Project changed to: " + (newProject != null ? newProject.getName() : "null"));
        
        projectChangeListeners.forEach((listener) -> listener.projectChanged(newProject));
    }
    
    public void addProjectChangeListener(IProjectChangeListener projectChangeListener) 
    {
        projectChangeListeners.add(projectChangeListener);
    }
}
