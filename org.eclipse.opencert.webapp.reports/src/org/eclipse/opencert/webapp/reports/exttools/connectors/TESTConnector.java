/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.exttools.connectors;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.opencert.externaltools.api.AbstractHTTPExternalToolConnector;
import org.eclipse.opencert.externaltools.api.ExternalToolQuery;

public class TESTConnector
        extends AbstractHTTPExternalToolConnector
{

    @Override
    public String getName()
    {
        return "Test Connector";
    }

    @Override
    public String getDescription()
    {
        return "It is an artifficial example test connector.";
    }

    @Override
    public List<String> getSettingKeys()
    {
        List<String> keys = new ArrayList<String>();
        keys.add("URL");
        keys.add("User");
        keys.add("Password");

        return keys;
    }

    @Override
    public Map<String, String> getSettingDefaultValues()
    {
        List<String> keys = getSettingKeys();
        
        Map <String, String> keysAndValuesMap = new LinkedHashMap<String, String>();
        keysAndValuesMap.put(keys.get(0), "https://test:8443");
        keysAndValuesMap.put(keys.get(1), "test");
        keysAndValuesMap.put(keys.get(2), "test");
        
        return keysAndValuesMap;
    }

    @Override
    public String getUrlSuffix()
    {
        return "localhost";
    }

    @Override
    public int processHttpResponse(String httpResponse)
    {
        return 100;
    }

    @Override
    public int connectAndProcessResponse(ExternalToolQuery externalToolQuery)
    {
        return 100;
    }

}
