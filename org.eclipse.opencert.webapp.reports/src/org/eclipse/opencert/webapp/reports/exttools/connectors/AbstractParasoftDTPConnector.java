/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.exttools.connectors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.opencert.webapp.reports.util.OpencertLogger;
import org.eclipse.opencert.externaltools.api.AbstractHTTPExternalToolConnector;
import org.eclipse.opencert.externaltools.api.ExternalToolQuery;

public abstract class AbstractParasoftDTPConnector
        extends AbstractHTTPExternalToolConnector
{
    private static final String PASSWORD_KEY = "Password";

    @Override
    public List<String> getSettingKeys()
    {
        List<String> keys = new ArrayList<String>();
        
        keys.add(URL_KEY);
        keys.add(USER_KEY);
        keys.add(PASSWORD_KEY);
        
        return keys;
    }

    @Override
    public Map<String, String> getSettingDefaultValues()
    {
        Map <String, String> keysAndValuesMap = new LinkedHashMap<String, String>();
//        keysAndValuesMap.put(URL_KEY, "https://argon.parasoft.com.pl:8443");
//        keysAndValuesMap.put(USER_KEY, "opencert-report");
//        keysAndValuesMap.put(PASSWORD_KEY, "opencert-report");
//        
        keysAndValuesMap.put(URL_KEY, "https://snake.parasoft.com:443");
        keysAndValuesMap.put(USER_KEY, "januszst");
        keysAndValuesMap.put(PASSWORD_KEY, "grs30");
//        
        return keysAndValuesMap;
    }

    @Override
    public int connectAndProcessResponse(ExternalToolQuery externalToolQuery) throws IOException
    {
        OpencertLogger.info("Connecting to the external tool: " + externalToolQuery.getExternalToolQuerySettings());
        
        putSettingsIntoConnectorSettingsMap(externalToolQuery.getSettingsAsMap());
        
        final String url = getUrlSuffix();
        String httpResponse = getDataFromServer(url);

        final int result = processHttpResponse(httpResponse);
        
        return result;
    } 
    
    
}
