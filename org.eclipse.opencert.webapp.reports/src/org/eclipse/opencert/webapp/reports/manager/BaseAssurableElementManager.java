/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.manager;

import org.eclipse.opencert.webapp.reports.dao.BaseAssurableElementDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BaseAssurableElementManager {
    public static final String SPRING_NAME = "baseAssurableElementManager";
    
    @Autowired
    private BaseAssurableElementDAO baseAssurableElementDAO;
    
	public void removeBaseComplianceMap(Long baseComplianceMapUid) 
	{   
	    baseAssurableElementDAO.removeBaseComplianceMap(baseComplianceMapUid);
	}
}
