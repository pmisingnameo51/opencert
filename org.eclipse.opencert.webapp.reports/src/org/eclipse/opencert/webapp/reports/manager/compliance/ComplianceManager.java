/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.manager.compliance;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.opencert.webapp.reports.manager.BaselineManager;
import org.eclipse.opencert.webapp.reports.util.BaselineElementType;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;

import com.vaadin.data.Item;
import com.vaadin.data.util.DefaultItemSorter;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;

public class ComplianceManager<DATA_ROW>
{
    private static final String ART_TYPE = "Art";
    private static final String BASE_ARTEFACT_LABEL = "BaseArtefact";
    private static final String ACT_TYPE = "Act";
    private static final String BASE_ACTIVITY_LABEL = "BaseActivity";
    private static final String REQ_TYPE = "Req";
    private static final String BASE_REQUIREMENT_LABEL = "BaseRequirement";

    private IDataRowProcessor<DATA_ROW> _dataRowProcessor;
    private Map<Long, String> _idToBaselineElementTypeMap = new HashMap<Long, String>();

    public ComplianceManager(IDataRowProcessor<DATA_ROW> rowProcessor)
    {
        _dataRowProcessor = rowProcessor;
    }

    public final HierarchicalContainer readBaseAssetDataFromDb(long baselineFrameworkID)
    {
        HierarchicalContainer hierarchicalContainer = initEmptyTableDataContainer();

        readBaseArtefactsFromDb(hierarchicalContainer, baselineFrameworkID);
        
        if (_dataRowProcessor.supportsActivities()) {
            readBaseActivitiesFromDb(hierarchicalContainer, baselineFrameworkID);
        }
        
        readBaseRequirementsFromDb(hierarchicalContainer, baselineFrameworkID);

        _dataRowProcessor.generateParentChildRelationsInContainer(hierarchicalContainer);

        return hierarchicalContainer;
    }

    public String findBaselineElementTypeById(long cdoID)
    {
        return _idToBaselineElementTypeMap.get(cdoID);
    }

    private final void readBaseArtefactsFromDb(HierarchicalContainer hierarchicalContainer, long baselineFrameworkID)
    {
        BaselineManager baselineManager = (BaselineManager) SpringContextHelper.getBeanFromWebappContext(BaselineManager.SPRING_NAME);
        List<BaseArtefact> artefacts = baselineManager.getBaseArtefacts(baselineFrameworkID, true);

        for (BaseArtefact bArtefact : artefacts) {
            long itemId = CDOStorageUtil.getCDOId(bArtefact);
            _idToBaselineElementTypeMap.put(itemId, BaselineElementType.BASE_ARTEFACT);

            Image artefactImage = getBaseArtefactImage();

            DATA_ROW artefactDataRow = _dataRowProcessor.createBaseAssetDataRow(itemId, bArtefact, null, artefactImage);

            Item item = hierarchicalContainer.addItem(artefactDataRow);
            hierarchicalContainer.setChildrenAllowed(artefactDataRow, false);
            _dataRowProcessor.setItemPropsFromDataRow(item, artefactDataRow);
        }
    }

    public static Image getBaseArtefactImage()
    {
        Image artefactImage = new Image(ART_TYPE, new ThemeResource("images/Artefact.png"));
        artefactImage.setDescription(BASE_ARTEFACT_LABEL);
        return artefactImage;
    }

    private final void readBaseActivitiesFromDb(HierarchicalContainer hierarchicalContainer, long baselineFrameworkID)
    {
        BaselineManager baselineManager = (BaselineManager) SpringContextHelper.getBeanFromWebappContext(BaselineManager.SPRING_NAME);
        List<BaseActivity> activities = baselineManager.getBaseActivities(baselineFrameworkID, true);

        prepareDataAndCreateBaseActivityWrapper(activities, hierarchicalContainer, null);
    }

    private final void prepareDataAndCreateBaseActivityWrapper(List<BaseActivity> activities, HierarchicalContainer hierarchicalContainer,
            DATA_ROW parentComplianceElement)
    {
        for (BaseActivity bActivity : activities) 
        {
            long itemId = CDOStorageUtil.getCDOId(bActivity);
            _idToBaselineElementTypeMap.put(itemId, BaselineElementType.BASE_ACTIVITY);

            Image activityImage = new Image(ACT_TYPE, new ThemeResource("images/Activity.png"));
            activityImage.setDescription(BASE_ACTIVITY_LABEL);

            DATA_ROW baseAssetCompliance = _dataRowProcessor.createBaseAssetDataRow(itemId, bActivity, parentComplianceElement, activityImage);
            if (baseAssetCompliance == null) {
                continue;
            }

            Item item = hierarchicalContainer.addItem(baseAssetCompliance);
            _dataRowProcessor.setItemPropsFromDataRow(item, baseAssetCompliance);

            List<BaseActivity> subActList = bActivity.getSubActivity();
            List<BaseActivity> selectedSubActList = new ArrayList<BaseActivity>();
            for (BaseActivity subAct : subActList) {
            	if (subAct.isIsSelected()) {
            		selectedSubActList.add(subAct);
            	}
            }
            if (selectedSubActList.isEmpty()) {
                hierarchicalContainer.setChildrenAllowed(baseAssetCompliance, false);
            } else {
                addSubActivityToContainer(baseAssetCompliance, selectedSubActList, hierarchicalContainer);
            }
        }
    }

    private final void addSubActivityToContainer(DATA_ROW parentComplianceElement, List<BaseActivity> subActivitiesList, HierarchicalContainer hierarchicalContainer)
    {
        prepareDataAndCreateBaseActivityWrapper(subActivitiesList, hierarchicalContainer, parentComplianceElement);
    }
    
    private final void readBaseRequirementsFromDb(HierarchicalContainer hierarchicalContainer, long baselineFrameworkID)
    {
        BaselineManager baselineManager = (BaselineManager) SpringContextHelper.getBeanFromWebappContext(BaselineManager.SPRING_NAME);
        List<BaseRequirement> requirements = baselineManager.getBaseRequirements(baselineFrameworkID, true);
        
        prepareDataAndCreateBaseRequirementWrapper(requirements, hierarchicalContainer, null);
    }
    
    private final void prepareDataAndCreateBaseRequirementWrapper(List<BaseRequirement> requirements, HierarchicalContainer hierarchicalContainer,
            DATA_ROW parentComplianceElement)
    {
        for (BaseRequirement bRequirement : requirements) {
            long itemId = CDOStorageUtil.getCDOId(bRequirement);
            _idToBaselineElementTypeMap.put(itemId, BaselineElementType.BASE_REQUIREMENT);

            Image requirementImage = getBaseRequirementImage();

            DATA_ROW requirementDataRow = _dataRowProcessor.createBaseAssetDataRow(itemId, bRequirement, parentComplianceElement, requirementImage);
            if (requirementDataRow == null) {
                continue;
            }

            Item item = hierarchicalContainer.addItem(requirementDataRow);
            _dataRowProcessor.setItemPropsFromDataRow(item, requirementDataRow);
            
            List<BaseRequirement> subReqList = bRequirement.getSubRequirement();
            List<BaseRequirement> selectedSubReqList = new ArrayList<BaseRequirement>();
            for (BaseRequirement subReq : subReqList) {
            	if (subReq.isIsSelected()) {
            		selectedSubReqList.add(subReq);
            	}
            }
            if (selectedSubReqList.isEmpty()) {
            	hierarchicalContainer.setChildrenAllowed(requirementDataRow, false);
            } else {
            	addSubRequirementToContainer(requirementDataRow, selectedSubReqList, hierarchicalContainer);
            }
        }
    }
    
    private final void addSubRequirementToContainer(DATA_ROW parentComplianceElement, List<BaseRequirement> subRequirementsList, 
    		HierarchicalContainer hierarchicalContainer)
    {
        prepareDataAndCreateBaseRequirementWrapper(subRequirementsList, hierarchicalContainer, parentComplianceElement);
    }

    public static Image getBaseRequirementImage()
    {
        Image requirementImage = new Image(REQ_TYPE, new ThemeResource("images/Requirement.gif"));
        requirementImage.setDescription(BASE_REQUIREMENT_LABEL);
        return requirementImage;
    }

    public HierarchicalContainer initEmptyTableDataContainer()
    {
        HierarchicalContainer hc = createEmptyTableDataContainer();

        _dataRowProcessor.initEmptyTableDataContainer(hc);

        return hc;
    }

    private HierarchicalContainer createEmptyTableDataContainer()
    {
        HierarchicalContainer hc = new HierarchicalContainer() {
            private static final long serialVersionUID = -643117478357512806L;

            @Override
            public Collection<?> getSortableContainerPropertyIds()
            {
                return getContainerPropertyIds();
            }
        };
        hc.setItemSorter(new DefaultItemSorter(new Comparator<Object>() {
            public int compare(Object o1, Object o2)
            {
                if (o1 instanceof Button && o2 instanceof Button) {
                    Integer caption1 = null;
                    Integer caption2 = null;
                    try {
                        caption1 = Integer.parseInt(((Button) o1).getCaption());
                        caption2 = Integer.parseInt(((Button) o2).getCaption());
                    } catch (NumberFormatException e) {
                        return -1;
                    }

                    return caption1.compareTo(caption2);
                } else if (o1 instanceof Image && o2 instanceof Image) {
                    return (((Image) o1).getCaption()).compareTo(((Image) o2).getCaption());
                } else if (o1 instanceof Label && o2 instanceof Label) {
                    String caption1 = ((Label) o1).getValue();
                    String caption2 = ((Label) o2).getValue();
                    return caption1.compareTo(caption2);
                }
                return 0;
            }
        }));

        return hc;
    }
}
