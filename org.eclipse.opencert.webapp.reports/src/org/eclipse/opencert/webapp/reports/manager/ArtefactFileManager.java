/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.manager;

import java.util.ArrayList;
import java.util.Date;

import org.eclipse.emf.cdo.CDOObject;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CDOUtil;
import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.webapp.reports.containers.ArtefactFileDetails;
import org.eclipse.opencert.webapp.reports.containers.SvnProperties;
import org.eclipse.opencert.webapp.reports.dao.ArtefactFileManagerDAO;
import org.eclipse.opencert.webapp.reports.util.IUploadFile;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.assurproj.assuranceproject.impl.AssuranceProjectImpl;
import org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement;
import org.eclipse.opencert.apm.baseline.baseline.BaselineFactory;
import org.eclipse.opencert.apm.baseline.baseline.impl.BaseComplianceMapImpl;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel;
import org.eclipse.opencert.evm.evidspec.evidence.EvidenceFactory;
import org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactDefinitionImpl;
import org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactImpl;
import org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactModelImpl;
import org.eclipse.opencert.evm.evidspec.evidence.impl.EvidenceFactoryImpl;
import org.eclipse.opencert.evm.evidspec.evidence.impl.ResourceImpl;
import org.eclipse.opencert.infra.mappings.mapping.MapKind;
import org.eclipse.opencert.infra.mappings.mapping.MappingFactory;
import org.eclipse.opencert.infra.mappings.mapping.impl.MapJustificationImpl;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;
import org.eclipse.opencert.storage.cdo.executors.HttpRequestDrivenInTransactionExecutor;
import org.eclipse.opencert.storage.cdo.executors.TemplatedHttpRequestDrivenInTransactionExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ArtefactFileManager
{
    public static final String SPRING_NAME = "artefactFileManager";

    private static final String artefactDefinitionId = "#defaultArtefactDefinitionInsertedByOPENCERTWeb";

    private ArtefactModelImpl artefactModelImpl;

    private long projectCdoId;

    @Autowired
    private ArtefactFileManagerDAO artefactFileManagerDAO;
    
    public ArrayList<SvnProperties> searchSvnPropertiesFromProject(final ArtefactFileDetails aFileDetails) throws Exception
    {	
    	ArrayList<SvnProperties> svnPropertiesFromProject = new ArrayList<SvnProperties>();
    	
    	try {
    		projectCdoId = CDOStorageUtil.getCDOId(aFileDetails.getAssuranceProject());
	        
    		new HttpRequestDrivenInTransactionExecutor() {
	            @Override
	            public void executeInTransaction(CDOTransaction cdoTransaction)
	            {
	            	CDOObject projectElement = (CDOObject) cdoTransaction.getObject(CDOUtil.getCDOObject(aFileDetails.getAssuranceProject()));
	            		     
			        AssuranceProject assuranceProject = (AssuranceProject)projectElement;
			        EList<AssetsPackage> assets = assuranceProject.getAssetsPackage();
			        for (AssetsPackage asset: assets) {
			            if (asset.isIsActive()) {
			            	EList<ArtefactModel> artefactModels = asset.getArtefactsModel();
			             	for (ArtefactModel artefactModel : artefactModels) {
			             		String artefactModelUrl = artefactModel.getRepoUrl();
			             		if (artefactModelUrl != null && !("".equals(artefactModelUrl))) {
			             			String artefactModelUser = artefactModel.getRepoUser();
			             			String artefactModelPassword = artefactModel.getRepoPassword();
			             			SvnProperties svnProperties = new SvnProperties(artefactModelUrl, artefactModelUser, artefactModelPassword);
			             			if (!svnPropertiesAlreadyAdded(svnPropertiesFromProject, svnProperties)) {
			             				svnPropertiesFromProject.add(svnProperties);
			             			}
			             		}
			             	}
			            }
			        }
	            }   
	        }.executeReadOnlyOperation();
	        
	    } catch (Exception e) {
	    	e.printStackTrace();
	        throw new ArtefactSvnUrlException("Unexpected error " + e.getMessage(), e);
	    }
	    
	    return svnPropertiesFromProject;
    }
    
    private boolean svnPropertiesAlreadyAdded(ArrayList<SvnProperties> svnPropertiesFromProject, SvnProperties svnProperties) {
    	for (SvnProperties svnPropertiesFromArtefactModel : svnPropertiesFromProject) {
    		String svnUrlFromArtefactModel = svnPropertiesFromArtefactModel.getSvnUrl();
    		
    		if (svnUrlFromArtefactModel.equals(svnProperties.getSvnUrl())) {
    			return true;
    		}
    	}
    	
    	return false;
    }
    
    public Artefact createArtefactForEvidenceFile(final ArtefactFileDetails aFileDetails, String projectResourcePath) throws Exception
    {
        Artefact createdArtefact = null;
        
        try {
            final ArtefactFileChecker aChecker = new ArtefactFileChecker();

            projectCdoId = CDOStorageUtil.getCDOId(aFileDetails.getAssuranceProject());

            createdArtefact = new TemplatedHttpRequestDrivenInTransactionExecutor<Artefact>() {
                
                @Override
                public Artefact executeInTransaction(CDOTransaction cdoTransaction)
                {
                	String projectName = "";
                	CDOResource resource = null;
                	if (projectResourcePath.contains("/ASSURANCE_PROJECT")) {
                		int endIndex = projectResourcePath.indexOf("/ASSURANCE_PROJECT");
                		projectName = projectResourcePath.substring(0, endIndex);
                		resource = cdoTransaction.getOrCreateResource(projectName + "/EVIDENCE/" + projectName + "_web" + ".evidence");
                	} else {
                		resource = cdoTransaction.getOrCreateResource(IUploadFile.INSERTED_BY_OPENCERT_WEB + ".evidence");
                	}
                    
                    CDOObject projectElement = (CDOObject) cdoTransaction.getObject(CDOUtil.getCDOObject(aFileDetails.getAssuranceProject()));
			        AssuranceProject assuranceProject = (AssuranceProject)projectElement;
			        EList<AssetsPackage> assets = assuranceProject.getAssetsPackage();
			        for (AssetsPackage asset : assets) {
			            if (asset.isIsActive()) {
			            	EList<ArtefactModel> artefactModelsFromProject = asset.getArtefactsModel();
		                    artefactModelImpl = getOrCreateDefaultArtefactModel(artefactModelsFromProject, aFileDetails, cdoTransaction, resource);
			            }
			        }

			        if (artefactModelImpl == null) {
			        	aChecker.setActiveAssetPackage(false);
			        	return null;
			        }
			        
                    if (!(aChecker.checkArtefactDefinition(artefactModelImpl.getArtefact()))) {
                        return null;
                    }

                    ArtefactDefinitionImpl artefactDefinitionImpl = (ArtefactDefinitionImpl) artefactModelImpl.getArtefact().get(0);

                    ResourceImpl fileResource = createFileResource(aFileDetails);
                    resource.getContents().add(fileResource);

                    ArtefactImpl artefact = createArtefact(fileResource, aFileDetails);
                    resource.getContents().add(artefact);

                    artefactDefinitionImpl.getArtefact().add(artefact);

                    CDOObject baseElement = (CDOObject) cdoTransaction.getObject(CDOUtil.getCDOObject(aFileDetails.getBaseAssetComplianceStatus()
                            .getBaseAssurableElement()));
                    BaseAssurableElement baseAssurableElement = (BaseAssurableElement) baseElement;

                    BaseComplianceMapImpl baseComplianceMap = (BaseComplianceMapImpl) BaselineFactory.eINSTANCE.createBaseComplianceMap();
                    baseComplianceMap.getTarget().add(artefact);

                    createComplianceType(baseComplianceMap, aFileDetails.getComplianceValue());

                    MapJustificationImpl mapJustificationImpl = (MapJustificationImpl) MappingFactory.eINSTANCE.createMapJustification();
                    mapJustificationImpl.setExplanation(aFileDetails.getJustification());
                    resource.getContents().add(mapJustificationImpl);

                    baseComplianceMap.setMapJustification(mapJustificationImpl);

                    baseAssurableElement.getComplianceMap().add(baseComplianceMap);

                    resource.setModified(true);
                    
                    return artefact;
                }
            }.executeReadWriteOperation();

            if (!(aChecker.isCorrectArtefactDefinition())) {
                throw new ArtefactFileCheckerException("Incorrect ArtefactDefinition");
            }

            new HttpRequestDrivenInTransactionExecutor() {
                @Override
                public void executeInTransaction(CDOTransaction cdoTransaction)
                {
                    CDOObject aProj = (CDOObject) cdoTransaction.getObject(CDOUtil.getCDOObject(aFileDetails.getAssuranceProject()));
                    AssuranceProjectImpl assuranceProject = (AssuranceProjectImpl) aProj;

                    if (assuranceProject.getAssetsPackage().isEmpty()) {
                        aChecker.setEmptyAssetPackageList(true);
                        return;
                    }

                    for (AssetsPackage assetsP : assuranceProject.getAssetsPackage()) {
                        if (assetsP.isIsActive()) {
                            aChecker.setActiveAssetPackage(true);
                            assetsP.getArtefactsModel().add(artefactModelImpl);
                            break;
                        }
                    }

                    assuranceProject.eResource().setModified(true);
                }
            }.executeReadWriteOperation();

            if (aChecker.isEmptyAssetPackageList()) {
                throw new ArtefactFileCheckerException("Empty AssetPackage List in AssuranceProject");
            }

            if (!(aChecker.isActiveAssetPackage())) {
                throw new ArtefactFileCheckerException("Not active AssetPackage in AssuranceProject");
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new ArtefactFileCheckerException("Unexpected error " + e.getMessage(), e);
        }
        
        
        return createdArtefact;
    }

    private ArtefactImpl createArtefact(ResourceImpl fileResource, ArtefactFileDetails aFileDetails)
    {
        ArtefactImpl artefact = (ArtefactImpl) new EvidenceFactoryImpl().createArtefact();
        artefact.setName(aFileDetails.getArtefactName());
        artefact.setVersionID(IUploadFile.INSERTED_BY_OPENCERT_WEB);
        artefact.setIsLastVersion(true);
        artefact.getResource().add(fileResource);
        
        ArtefactFileManagerDAO.setModificationDate(fileResource, aFileDetails.getModificationDate());

        return artefact;
    }

    private ResourceImpl createFileResource(ArtefactFileDetails aFileDetails)
    {
        ResourceImpl fileResource = (ResourceImpl) new EvidenceFactoryImpl().createResource();

        String fileName = aFileDetails.getFileName();

        fileResource.setName(aFileDetails.getResourceName());
        fileResource.setLocation(fileName);
        fileResource.setFormat(fileName.lastIndexOf(".") != -1 ? fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length()) : "");

        return fileResource;
    }
    
    public void updateResourceFile(final long resourceUid, String resourceName, String resourceLocation, Date modificationDate)
    {
    	artefactFileManagerDAO.updateResourceFile(resourceUid, resourceName, resourceLocation, modificationDate);
    }
    
    public long getArtefactCdoIdFromResource(final long resourceUid)
    {
    	return artefactFileManagerDAO.getArtefactCdoIdFromResource(resourceUid);
    }

    private void createComplianceType(BaseComplianceMapImpl baseComplianceMap, Object complianceType)
    {
        if (complianceType.toString().toLowerCase().equals(MapKind.FULL.getName())) {
            baseComplianceMap.setType(MapKind.FULL);
        } else if (complianceType.toString().toLowerCase().equals(MapKind.PARTIAL.getName())) {
            baseComplianceMap.setType(MapKind.PARTIAL);
        } else {
            baseComplianceMap.setType(MapKind.NO_MAP);
        }
    }

    private ArtefactModelImpl getOrCreateDefaultArtefactModel(EList<ArtefactModel> artefactModels, ArtefactFileDetails aFileDetails, CDOTransaction cdoTransaction, CDOResource resource)
    {
        ArtefactModelImpl artefactModelImpl = null;
        
        String svnUrl = aFileDetails.getSvnProperties().getSvnUrl();
        for (ArtefactModel artefactModel : artefactModels) {
            if (artefactModel.getId() != null && 
            		artefactModel.getId().equals(ArtefactModelIdGenerator.generateArtefactModelId(svnUrl, projectCdoId))) {
            	if (artefactModel.getRepoUrl().equals(svnUrl)) {
            		artefactModelImpl = (ArtefactModelImpl)artefactModel;
            		return artefactModelImpl;
            	}
            }
        }

        artefactModelImpl = addSvnArtefactModelWithDefinition(aFileDetails, cdoTransaction, resource);

        return artefactModelImpl;
    }
    
    private ArtefactModelImpl addSvnArtefactModelWithDefinition(final ArtefactFileDetails aFileDetails, CDOTransaction cdoTransaction, CDOResource resource)
    {
        SvnProperties svnProperties = aFileDetails.getSvnProperties();
        String svnUrl = svnProperties.getSvnUrl();

        ArtefactModelImpl artefactModelImpl = (ArtefactModelImpl) EvidenceFactory.eINSTANCE.createArtefactModel();
        artefactModelImpl.setId(ArtefactModelIdGenerator.generateArtefactModelId(svnUrl, projectCdoId));
        artefactModelImpl.setDescription("This artefactModel has been inserted by OpencertWeb");
        artefactModelImpl.setName(aFileDetails.getProjectName() + " " + svnUrl);
        artefactModelImpl.setRepoUrl(svnUrl);
        artefactModelImpl.setRepoUser(svnProperties.getSvnUser());
        artefactModelImpl.setRepoPassword(svnProperties.getSvnPassword());

        ArtefactDefinitionImpl artefactDefinitionImpl = (ArtefactDefinitionImpl) EvidenceFactory.eINSTANCE.createArtefactDefinition();
        artefactDefinitionImpl.setId(artefactDefinitionId + ":" + projectCdoId);
        artefactDefinitionImpl.setDescription("This artefactDefinition has been inserted by OpencertWeb");
        artefactDefinitionImpl.setName(IUploadFile.INSERTED_BY_OPENCERT_WEB);

        resource.getContents().add(artefactDefinitionImpl);
        artefactModelImpl.getArtefact().add(artefactDefinitionImpl);
        resource.getContents().add(artefactModelImpl);

        return artefactModelImpl;
    }

    private class ArtefactFileChecker
    {
        private boolean isActiveAssetPackage;

        private boolean isEmptyAssetPackageList;

        private boolean isCorrectArtefactDefinition;

        public ArtefactFileChecker()
        {
            this.setActiveAssetPackage(false);
            this.setEmptyAssetPackageList(false);
            this.setCorrectArtefactDefinition(true);
        }

        public boolean checkArtefactDefinition(EList<ArtefactDefinition> artefact)
        {
            if (artefact.size() != 1) {
                isCorrectArtefactDefinition = false;
            } else {
                ArtefactDefinitionImpl artefactDefinitionImpl = (ArtefactDefinitionImpl) artefactModelImpl.getArtefact().get(0);

                if (!(artefactDefinitionImpl.getId().equals(artefactDefinitionId + ":" + projectCdoId))) {
                    isCorrectArtefactDefinition = false;
                }
            }

            return isCorrectArtefactDefinition;
        }

        public boolean isActiveAssetPackage()
        {
            return isActiveAssetPackage;
        }

        public void setActiveAssetPackage(boolean isActiveAssetPackage)
        {
            this.isActiveAssetPackage = isActiveAssetPackage;
        }

        public boolean isEmptyAssetPackageList()
        {
            return isEmptyAssetPackageList;
        }

        public void setEmptyAssetPackageList(boolean isEmptyAssetPackageList)
        {
            this.isEmptyAssetPackageList = isEmptyAssetPackageList;
        }

        public boolean isCorrectArtefactDefinition()
        {
            return isCorrectArtefactDefinition;
        }

        public void setCorrectArtefactDefinition(boolean isCorrectArtefactDefinition)
        {
            this.isCorrectArtefactDefinition = isCorrectArtefactDefinition;
        }
    }

    class ArtefactFileCheckerException
            extends Exception
    {
        private static final long serialVersionUID = 7186609210503460891L;

        public ArtefactFileCheckerException()
        {
            this("");
        }

        public ArtefactFileCheckerException(String s)
        {
            super("ArtefactFileCheckerException\n" + s);
        }
        
        public ArtefactFileCheckerException(String s, Throwable t)
        {
            super("ArtefactFileCheckerException\n" + s, t);
        }
    }
    
    class ArtefactSvnUrlException
    		extends Exception
	{
		private static final long serialVersionUID = 7186609210503460891L;
		
		public ArtefactSvnUrlException()
		{
		    this("");
		}
	
		public ArtefactSvnUrlException(String s)
		{
		    super("ArtefactSvnUrlException\n" + s);
		}
		
		public ArtefactSvnUrlException(String s, Throwable t)
		{
		    super("ArtefactSvnUrlException\n" + s, t);
		}
	}
    
    static class ArtefactModelIdGenerator {    	
		public static String generateArtefactModelId(String svnUrl, long projectCdoId) {
			return svnUrl + ":" + projectCdoId;
		}
    	
		public static long getProjectCdoIdFromArtefactModelId(String artefactModelId) {
			long projectCdoId = 0;
			String sProjectCdoId = artefactModelId.substring(artefactModelId.lastIndexOf(":") + 1);
			
			 try {
				 projectCdoId = Long.parseLong(sProjectCdoId);
		      } catch (NumberFormatException nfe) {
		      }
			
			return projectCdoId;
		}
    }
}
