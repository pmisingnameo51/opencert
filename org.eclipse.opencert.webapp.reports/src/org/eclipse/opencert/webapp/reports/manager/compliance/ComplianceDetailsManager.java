/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.manager.compliance;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.opencert.webapp.reports.containers.ComplianceAsset;
import org.eclipse.opencert.webapp.reports.containers.ComplianceAssetEvaluation;
import org.eclipse.opencert.webapp.reports.containers.ComplianceAssetProperty;
import org.eclipse.opencert.webapp.reports.containers.ComplianceAssetResource;
import org.eclipse.opencert.webapp.reports.containers.ComplianceDetails;
import org.eclipse.opencert.webapp.reports.manager.AssuranceAssetEventManager;
import org.eclipse.opencert.webapp.reports.util.BaselineElementType;
import org.eclipse.opencert.webapp.reports.util.ComplianceAssetType;
import org.eclipse.opencert.webapp.reports.util.ComplianceElementType;
import org.eclipse.opencert.webapp.reports.util.ComplianceType;
import org.eclipse.opencert.webapp.reports.util.OpencertLogger;
import org.eclipse.opencert.webapp.reports.util.SpringContextHelper;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement;
import org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel;
import org.eclipse.opencert.evm.evidspec.evidence.Resource;
import org.eclipse.opencert.evm.evidspec.evidence.Value;
import org.eclipse.opencert.infra.mappings.mapping.MapJustification;
import org.eclipse.opencert.infra.mappings.mapping.MapKind;
import org.eclipse.opencert.pam.procspec.process.Activity;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;

public class ComplianceDetailsManager 
{
	private Map<Long, String> idToComplianceElementTypeMap = new HashMap<Long, String>();
	
	//Build Compliance Details Data from BaseAssurableElement
	public List<ComplianceDetails> readComplianceDetailsData(BaseAssurableElement baseAssurableElement, String complianceType, Long baseAssetCdoId,
			String baselineElementName, boolean includeNoMap) {
		
		String baselineElementType = "";
		if (baseAssurableElement instanceof BaseArtefact) {
			baselineElementType = BaselineElementType.BASE_ARTEFACT;
		} else if (baseAssurableElement instanceof BaseActivity) {
			baselineElementType = BaselineElementType.BASE_ACTIVITY;
		} else if (baseAssurableElement instanceof BaseRequirement) {
			baselineElementType = BaselineElementType.BASE_REQUIREMENT;
		}
		
		List<ComplianceDetails> complianceJustifications = new ArrayList<ComplianceDetails>();
		for(BaseComplianceMap baseAssurableElementComplianceMap : baseAssurableElement.getComplianceMap()) {
			int type = baseAssurableElementComplianceMap.getType().getValue();
			if (MapKind.NO_MAP_VALUE != type) {
				if (ComplianceType.COMPLIANCE_ALL.equals(complianceType) ||
						ComplianceType.COMPLIANCE_FULLY.equals(complianceType) && MapKind.FULL_VALUE == type ||
						ComplianceType.COMPLIANCE_PARTIALLY.equals(complianceType) && MapKind.PARTIAL_VALUE == type) {
					complianceJustifications.add(readDetailsData(baseAssurableElementComplianceMap, baselineElementType, 
							baseAssetCdoId, baselineElementName));
				}
			} else if (complianceType != null && ComplianceType.COMPLIANCE_ALL.equals(complianceType) && includeNoMap) {
				complianceJustifications.add(readDetailsData(baseAssurableElementComplianceMap, baselineElementType, 
						baseAssetCdoId, baselineElementName));
			}
		}
		return complianceJustifications;
	}
	
	public List<ComplianceDetails> readComplianceDetailsData(BaseAssurableElement baseAssurableElement, Long baseActivityCdoId, String baselineElementName, boolean includeNoMap) 
	{
		return readComplianceDetailsData(baseAssurableElement, ComplianceType.COMPLIANCE_ALL, baseActivityCdoId, baselineElementName, includeNoMap);
	}

    public String findComplianceElementTypeById(long cdoID) 
    {
        return idToComplianceElementTypeMap.get(cdoID);
    }

	private ComplianceDetails readDetailsData(BaseComplianceMap baseComplianceMap, String baseType, Long baseAssetCdoId,
			String baselineElementName) {
		
		ComplianceDetails complianceDetails = new ComplianceDetails();
		MapJustification mapJustification = baseComplianceMap.getMapJustification();
		if (mapJustification != null) {
			String complinaceJustificationExplanation = mapJustification.getExplanation();
			if (complinaceJustificationExplanation == null) {
				complinaceJustificationExplanation = "";
			}
			complianceDetails.setComplianceMapJustification(complinaceJustificationExplanation);
		} else {
			complianceDetails.setComplianceMapJustification("");
		}
		complianceDetails.setComplianceMapId(baseComplianceMap.getId());
		complianceDetails.setComplianceMapName(baseComplianceMap.getName());
		complianceDetails.setComplianceMapType(baseComplianceMap.getType().getName());
		complianceDetails.setBaseAssetType(baseType);
		long complianceMapCdoId = CDOStorageUtil.getCDOId(baseComplianceMap);
		idToComplianceElementTypeMap.put(complianceMapCdoId, ComplianceElementType.JUSTIFICATION);
		complianceDetails.setComplianceMapCdoId(complianceMapCdoId);
		complianceDetails.setBaseAssetId(baseAssetCdoId);
		complianceDetails.setBaseAssetName(baselineElementName);
		
		List<ComplianceAsset> complianceAssets = new ArrayList<ComplianceAsset>();
		for (AssuranceAsset assuranceAsset : baseComplianceMap.getTarget()) {
			if (assuranceAsset instanceof Artefact) {
				if (BaselineElementType.BASE_ARTEFACT.equals(baseType) || BaselineElementType.BASE_REQUIREMENT.equals(baseType)) {
					long assuranceAssetId = CDOStorageUtil.getCDOId(assuranceAsset);
					idToComplianceElementTypeMap.put(assuranceAssetId, ComplianceElementType.ARTEFACT);
					Artefact targetArtefact = (Artefact)assuranceAsset;
					complianceAssetsData(complianceAssets, targetArtefact);
				}
			}
			if (assuranceAsset instanceof Activity) {
				if (BaselineElementType.BASE_ACTIVITY.equals(baseType) || BaselineElementType.BASE_REQUIREMENT.equals(baseType)) {
					long assuranceAssetId = CDOStorageUtil.getCDOId(assuranceAsset);
					idToComplianceElementTypeMap.put(assuranceAssetId, ComplianceElementType.ACTIVITY);
					Activity targetActivity = (Activity)assuranceAsset;
					complianceAssetsData(complianceAssets, targetActivity);
				}
			}
		}
		complianceDetails.setComplianceAssets(complianceAssets);
		
		return complianceDetails;
	}

	private void complianceAssetsData(List<ComplianceAsset> complianceAssets, Artefact targetArtefact) {
		String complianceId = targetArtefact.getId();
		String complianceName = targetArtefact.getName();
		String complianceDescription = targetArtefact.getDescription();
		String complianceAssetType = ComplianceAssetType.ARTEFACT;
		long complianceCdoId = CDOStorageUtil.getCDOId(targetArtefact);
		
		List<ComplianceAssetProperty> complianceAssetProperties = complianceAssetPropertiesData(targetArtefact.getPropertyValue());
		List<ComplianceAssetEvaluation> complianceAssetEvaluations = complianceAssetEvaluationsData(targetArtefact.getEvaluation());		
		List<ComplianceAssetResource> complianceAssetResources = complianceAssetResourcessData(targetArtefact.getResource());
		
		IAEvaluation iaEvaluation = IAEvaluation.createForArtefact(targetArtefact);
		
		ComplianceAsset complianceAsset = new ComplianceAsset(complianceId, complianceName, complianceDescription, complianceAssetType, complianceCdoId,
				complianceAssetProperties, complianceAssetEvaluations, complianceAssetResources, iaEvaluation);
		
		complianceAssets.add(complianceAsset);
	}
	
	private void complianceAssetsData(List<ComplianceAsset> complianceAssets, Activity targetActivity) {
		String complianceId = targetActivity.getId();
		String complianceName = targetActivity.getName();
		String complianceDescription = targetActivity.getDescription();
		String complianceAssetType = ComplianceAssetType.ACTIVITY;
		long complianceCdoId = CDOStorageUtil.getCDOId(targetActivity);
		
		List<ComplianceAssetEvaluation> complianceAssetEvaluations = complianceAssetEvaluationsData (targetActivity.getEvaluation());

		ComplianceAsset complianceAsset = new ComplianceAsset(complianceId, complianceName, complianceDescription, complianceAssetType, complianceCdoId,  
				null, complianceAssetEvaluations, null, null);
		complianceAssets.add(complianceAsset);
	}

	private List<ComplianceAssetEvaluation> complianceAssetEvaluationsData (EList<AssuranceAssetEvaluation> assuranceAssetEvaluations) {
		List<ComplianceAssetEvaluation> complianceAssetEvaluations = new ArrayList<ComplianceAssetEvaluation>();
		
		for (AssuranceAssetEvaluation assuranceAssetEvaluation : assuranceAssetEvaluations) {
			String complianceAssetEvaluationId = assuranceAssetEvaluation.getId();
			String complianceAssetEvaluationName = assuranceAssetEvaluation.getName();
			String complianceAssetEvaluationCriterion = assuranceAssetEvaluation.getCriterion();
			String complianceAssetEvaluationCriterionDesc = assuranceAssetEvaluation.getCriterionDescription();
			String complianceAssetEvaluationResult = assuranceAssetEvaluation.getEvaluationResult();
			String complianceAssetEvaluationRational = assuranceAssetEvaluation.getRationale();
			
			Long cdoId = CDOStorageUtil.getCDOId(assuranceAssetEvaluation);
			String evaluationEvent = complianceAssetEvaluationEventsData(cdoId);
			
			ComplianceAssetEvaluation complianceAssetEvaluation = 
					new ComplianceAssetEvaluation(complianceAssetEvaluationId, complianceAssetEvaluationName, 
							complianceAssetEvaluationCriterion, complianceAssetEvaluationCriterionDesc, 
							complianceAssetEvaluationResult, complianceAssetEvaluationRational, evaluationEvent);
			complianceAssetEvaluations.add(complianceAssetEvaluation);
		}
		
		return complianceAssetEvaluations;
	}
	
	private String complianceAssetEvaluationEventsData(Long cdoId) {
		
		AssuranceAssetEventManager assuranceAssetEventManager = (AssuranceAssetEventManager)SpringContextHelper.getBeanFromWebappContext(AssuranceAssetEventManager.SPRING_NAME);
	    List<AssuranceAssetEvent> events = assuranceAssetEventManager.getAssuranceAssetEvents(cdoId);
	    String evaluationEvent = "";
	    
	    for (AssuranceAssetEvent event : events) {
	    	if (event.getTime() != null) {
	    		if (!"".equals(evaluationEvent)) {
	    			evaluationEvent += ", ";
	    		}
	    	
	    		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	    		try {
	    			evaluationEvent += df.format(event.getTime());
	    		} catch (Exception ex) {
	    			OpencertLogger.error("", ex);
	    		}
	    	}
	    }
	    
	    return evaluationEvent;
	}  
	
	private List<ComplianceAssetProperty> complianceAssetPropertiesData(EList<Value> propertyValues) {
		List<ComplianceAssetProperty> complianceAssetProperties = new ArrayList<ComplianceAssetProperty>();
		
		for (Value value : propertyValues) {
			String propertyName = value.getName();
			String propertyValue = value.getValue();
			ComplianceAssetProperty complianceProperty = new ComplianceAssetProperty(propertyName, propertyValue);
			complianceAssetProperties.add(complianceProperty);
		}
		
		return complianceAssetProperties;
	}
	
	private List<ComplianceAssetResource> complianceAssetResourcessData(EList<Resource> resources) {
		List<ComplianceAssetResource> complianceAssetResources = new ArrayList<ComplianceAssetResource>();
		
		for (Resource resource : resources) {
			long resourceCdoId = CDOStorageUtil.getCDOId(resource);
			idToComplianceElementTypeMap.put(resourceCdoId, ComplianceElementType.RESOURCE);
			String resourceId = resource.getId();
			String resourceName = resource.getName();
			String resourceDescription = resource.getDescription();
			String resourceLocation = resource.getLocation();
			String resourceFormat = resource.getFormat();
			
            Artefact artefactObject = (Artefact)resource.eContainer();
            
            EObject parent= artefactObject.eContainer();
            //Up to find the Artefact Definition
            while(parent instanceof Artefact){
            	parent= parent.eContainer();
            }
            
            String repoUrl = null;
            ArtefactDefinition artefactDefObject = (ArtefactDefinition)parent;
            if (artefactDefObject != null) {
            	ArtefactModel artefactModelObject = (ArtefactModel)artefactDefObject.eContainer();
            	if (artefactModelObject != null) {
            		repoUrl = artefactModelObject.getRepoUrl();
//            		String localpathArtefact = artefactModelObject.getRepoLocalPath();
//            		String userArtefact = artefactModelObject.getRepoUser();
//            		String passwordArtefact = artefactModelObject.getRepoPassword();
            	}
            }
			
			ComplianceAssetResource complianceResource = new ComplianceAssetResource(resourceId, resourceName, resourceDescription, resourceLocation,
					resourceFormat, repoUrl, resourceCdoId);
			complianceAssetResources.add(complianceResource);
		}
		
		return complianceAssetResources;
	}
}
