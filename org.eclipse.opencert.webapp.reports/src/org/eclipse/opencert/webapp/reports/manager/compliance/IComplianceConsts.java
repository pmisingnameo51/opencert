/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.manager.compliance;

public interface IComplianceConsts
{
    String SELECTED_ROW_PROPERTY_ID = "selectedRowID";
    String CDO_PROPERTY_ID = "cdoID";
    String TYPE_PROPERTY_ID = "typeID";
    String NAME_PROPERTY_ID = "nameID";
    String DESCRIPTION_PROPERTY_ID = "descriptionID";
    String RELATIONS_PROPERTY_ID = "relationsID";
    String BUTTON_PANEL_PROPERTY_ID = "buttonPanelID";
    
    String FULLY_COMPLIANT_PROPERTY_ID = "fullyCompliantID";
    String PARTIALLY_COMPLIANT_PROPERTY_ID = "partiallyCompliantID";

    String STATUS_PROPERTY_ID = "statusID";
    String IA_STATUS_PROPERTY_ID = "iaStatusID";
    
    String EXTERNAL_TOOL_PROPERTY_ID = "externalToolResultID";
}
