/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.manager.compliance;

public enum IAEvaluationStatus {
    NOT_AVAILABLE("Not available", "Not available", "Not available"),
    OK("Ok", "Not available", "Not available"),
    NEEDS_MODIFICATION("To modify", "Modified", "Impact Analysis designated this artefact as a candidate for modification. Please report/suppress modification using action button"),
    NEEDS_VALIDATION("To validate", "Validated", "Impact Analysis designated this artefact as requiring validation. Please confirm validation using action button"),
    REVOKED("Revoked", "Not available",  "Artefact in \"Revoked\" state should no longer be mapped. Please unassign this artefact using the \"Unassign\" icon");
    
    private String userFriendlyLabel;
    private String quickActionLabel;
    private String requiredUserAction;
    
    private IAEvaluationStatus(String userFriendlyLabel, String quickActionLabel, String requiredUserAction)
    {
        this.userFriendlyLabel = userFriendlyLabel;
        this.quickActionLabel = quickActionLabel;
        this.requiredUserAction = requiredUserAction;
    }

    public String getUserFriendlyLabel()
    {
        return userFriendlyLabel;
    }

    public String getQuickActionLabel()
    {
        return quickActionLabel;
    }

    public String getRequiredUserAction()
    {
        return requiredUserAction;
    }
}
