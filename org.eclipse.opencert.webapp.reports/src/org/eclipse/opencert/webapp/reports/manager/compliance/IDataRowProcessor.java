/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.manager.compliance;

import org.eclipse.opencert.webapp.reports.util.StringUtil;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;

import com.vaadin.data.Item;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;

public interface IDataRowProcessor<DATA_ROW> 
{
	void initEmptyTableDataContainer(HierarchicalContainer hc);
	
    DATA_ROW createBaseAssetDataRow(long itemId, BaseAssurableElement baseArtefactOrActivity, DATA_ROW parentDataRow, Image activityImage);
    
    void setItemPropsFromDataRow(Item item, DATA_ROW baseAssetDataRow);
    
    void generateParentChildRelationsInContainer(HierarchicalContainer hierarchicalContainer);

    boolean supportsActivities();
    
       
    default Label createComplianceAssetNameLabel(BaseAssurableElement baseAssurableElement)
    {
        String assetName;
        if (baseAssurableElement instanceof BaseArtefact) {
            assetName = ((BaseArtefact)baseAssurableElement).getName();
        } 
        else if (baseAssurableElement instanceof BaseActivity) {
            assetName = ((BaseActivity)baseAssurableElement).getName();
        }
        else if (baseAssurableElement instanceof BaseRequirement) {
            assetName = ((BaseRequirement)baseAssurableElement).getName();
        }
        else throw new IllegalArgumentException();
        
        final Label assetNameLabel = new Label(StringUtil.ensureNotNull(assetName));

        assetNameLabel.setStyleName(BASE_ASSET_NAME_STYLE);

        return assetNameLabel;
    }
    
    final String BASE_ASSET_NAME_STYLE = "baseAssetName";
}
