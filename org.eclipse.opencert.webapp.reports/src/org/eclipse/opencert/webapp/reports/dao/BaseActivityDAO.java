/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.dao;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOQuery;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;
import org.eclipse.opencert.storage.cdo.executors.HttpRequestDrivenInTransactionExecutor;
import org.springframework.stereotype.Component;

@Component
public class BaseActivityDAO {
	public BaseActivity getBaseActivity(final long baseActivityUid) 
	{
        final List<BaseActivity> result = new ArrayList<BaseActivity>();
        
        new HttpRequestDrivenInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                String baseActivitySQL = "select * from baseline_baseactivity where cdo_id = '" + baseActivityUid + "'" + CDOStorageUtil.getMandatoryCDOQuerySuffix();
                CDOQuery baseActivityQuery = cdoTransaction.createQuery("sql", baseActivitySQL);
                
                result.addAll(baseActivityQuery.getResult(BaseActivity.class));         
            }
        }.executeReadOnlyOperation();
        
        if (result.isEmpty()) {
        	return null;
        }
        return result.get(0);
	}
	
}
