/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.dao;

import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOQuery;
import org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement;
import org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;
import org.eclipse.opencert.storage.cdo.executors.HttpRequestDrivenInTransactionExecutor;
import org.springframework.stereotype.Component;

@Component
public class BaseAssurableElementDAO {

    public void removeBaseComplianceMap(final long baseComplianceMapUid)
    {
        new HttpRequestDrivenInTransactionExecutor() {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                String baseComplianceMapSQL = "select * from baseline_basecompliancemap where cdo_id = '" + baseComplianceMapUid + "'"
                        + CDOStorageUtil.getMandatoryCDOQuerySuffix();
                CDOQuery cdoq = cdoTransaction.createQuery("sql", baseComplianceMapSQL);
                BaseComplianceMap baseComplianceMap = cdoq.getResultValue(BaseComplianceMap.class);

                String baseAssurableElementSQL = "select cdo_container from baseline_basecompliancemap where cdo_id = '" + baseComplianceMapUid + "'"
                        + CDOStorageUtil.getMandatoryCDOQuerySuffix();
                cdoq = cdoTransaction.createQuery("sql", baseAssurableElementSQL);
                BaseAssurableElement baseAssurableElement = cdoq.getResultValue(BaseAssurableElement.class);

                baseAssurableElement.getComplianceMap().remove(baseComplianceMap);
            }
        }.executeReadWriteOperation();
    }
}
