/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.webapp.reports.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOQuery;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.opencert.webapp.reports.util.DateUtil;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.EvidenceFactory;
import org.eclipse.opencert.evm.evidspec.evidence.Value;
import org.eclipse.opencert.evm.evidspec.evidence.impl.ResourceImpl;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;
import org.eclipse.opencert.storage.cdo.executors.HttpRequestDrivenInTransactionExecutor;
import org.springframework.stereotype.Component;

@Component
public class ArtefactFileManagerDAO {
	
	private static final String MODIFICATION_DATE_PROPERTY = "Artefact Date";
	
	public void updateResourceFile(final long resourceUid, String resourceName, String resourceLocation, Date modificationDate) 
	{
        new HttpRequestDrivenInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                String resourceSQL = "select * from evidence_resource where cdo_id = '" + resourceUid + "'" + CDOStorageUtil.getMandatoryCDOQuerySuffix();
                CDOQuery resourceQuery = cdoTransaction.createQuery("sql", resourceSQL);
                
                ResourceImpl resourceFile = resourceQuery.getResultValue(ResourceImpl.class);
                resourceFile.setName(resourceName);
                resourceFile.setLocation(resourceLocation);
                resourceFile.setFormat(resourceName.lastIndexOf(".") != -1 ? resourceName.substring(resourceName.lastIndexOf(".") + 1, resourceName.length()) : "");
                
                setModificationDate(resourceFile, modificationDate);
            }
        }.executeReadWriteOperation();
	}
	
	public static void setModificationDate(ResourceImpl resourceFile, Date modificationDate) {
		String modifDate = DateUtil.convertDateToString(modificationDate);
		
		boolean modifiedDate = false;
        EObject resourceContainer = resourceFile.eContainer();
        if (resourceContainer instanceof Artefact) {
        	Artefact artefact = (Artefact)resourceContainer;
        	EList<Value> properties = artefact.getPropertyValue();
        	for (Value property : properties) {
        		if (MODIFICATION_DATE_PROPERTY.equals(property.getName())) {
        			property.setValue(modifDate);
        			modifiedDate = true;
        			break;
        		}
        	}
        	if (!modifiedDate) {
        		//create new MODIFICATION_DATE_PROPERTY property
        		Value value = EvidenceFactory.eINSTANCE.createValue();
        		value.setName(MODIFICATION_DATE_PROPERTY);
        		value.setValue(modifDate);
        		artefact.getPropertyValue().add(value);
        	}
        }
	}
	
	public long getArtefactCdoIdFromResource(final long resourceUid)
	{
		final List<ResourceImpl> result = new ArrayList<>();
		
		new HttpRequestDrivenInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                String resourceSQL = "select * from evidence_resource where cdo_id = '" + resourceUid + "'" + CDOStorageUtil.getMandatoryCDOQuerySuffix();
                CDOQuery resourceQuery = cdoTransaction.createQuery("sql", resourceSQL);
                
                result.addAll(resourceQuery.getResult(ResourceImpl.class));
            }
        }.executeReadOnlyOperation();
        
        long res = 0;
        if (result.size() > 0) {
        	EObject parent = result.get(0).eContainer();
            if (parent instanceof Artefact) {
            	res = CDOStorageUtil.getCDOId(parent);
            }
        }
        
        return res;
	}
}
