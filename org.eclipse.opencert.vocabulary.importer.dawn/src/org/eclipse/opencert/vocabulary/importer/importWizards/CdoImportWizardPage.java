/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.vocabulary.importer.importWizards;

import java.io.File;
import java.util.List;

import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class CdoImportWizardPage extends WizardPage {

	private static final String xmlSuffix = ".xml";

	private FileFieldEditor importFileEditor;
	private TreeViewer resourcesTreeViewer;
	private Text resourceNameText;

	private boolean importFileOkay = false;
	private boolean resourceNameOkay = false;

	private List<CDOResourceNode> nodeList;

	public CdoImportWizardPage(String pageName, List<CDOResourceNode> nodeList) {
		super(pageName);
		this.nodeList = nodeList;
		setTitle(pageName);
		setDescription("Import a vocabulary from the local file system");
	}

	public void createControl(Composite parent) {
		Composite dialogArea = new Composite(parent, SWT.NONE);
		GridData dialogData = new GridData(GridData.GRAB_HORIZONTAL
				| GridData.FILL_HORIZONTAL);
		dialogArea.setLayoutData(dialogData);
		GridLayout dialogAreaLayout = new GridLayout();
		dialogAreaLayout.numColumns = 1;
		dialogAreaLayout.makeColumnsEqualWidth = false;
		dialogAreaLayout.marginWidth = 0;
		dialogAreaLayout.marginHeight = 0;
		dialogArea.setLayout(dialogAreaLayout);

		createFileSelectionArea(dialogArea);
		createResourcesArea(dialogArea);

		setControl(dialogArea);

		setPageComplete(false);
	}

	private void createResourcesArea(Composite dialogArea) {
		Composite resourcesArea = new Composite(dialogArea, SWT.NONE);
		GridData resourcesData = new GridData(GridData.GRAB_HORIZONTAL
				| GridData.FILL_HORIZONTAL | GridData.GRAB_VERTICAL
				| GridData.FILL_VERTICAL);
		resourcesArea.setLayoutData(resourcesData);
		GridLayout resourcesLayout = new GridLayout();
		resourcesLayout.numColumns = 2;
		resourcesLayout.marginWidth = 0;
		resourcesLayout.marginHeight = 0;
		resourcesArea.setLayout(resourcesLayout);

		resourcesTreeViewer = new TreeViewer(resourcesArea, SWT.SINGLE);
		GridData treeData = new GridData(GridData.GRAB_HORIZONTAL
				| GridData.FILL_HORIZONTAL | GridData.GRAB_VERTICAL
				| GridData.FILL_VERTICAL);
		treeData.horizontalSpan = 2;
		resourcesTreeViewer.getControl().setLayoutData(treeData);
		resourcesTreeViewer.setAutoExpandLevel(2);

		CdoContainerContentProvider cp = new CdoContainerContentProvider();
		resourcesTreeViewer.setContentProvider(cp);
		resourcesTreeViewer
				.setLabelProvider(new CdoContainerContentProvider.CdoLabelProvider());
		resourcesTreeViewer.setComparator(new ViewerComparator());
		resourcesTreeViewer.setUseHashlookup(true);
		resourcesTreeViewer.addDoubleClickListener(new IDoubleClickListener() {
			@Override
			public void doubleClick(DoubleClickEvent event) {
				ISelection selection = event.getSelection();
				expand(selection);
			}

			private void expand(ISelection selection) {
				if (selection instanceof IStructuredSelection) {
					Object item = ((IStructuredSelection) selection)
							.getFirstElement();
					if (item == null) {
						return;
					}
					if (resourcesTreeViewer.getExpandedState(item)) {
						resourcesTreeViewer.collapseToLevel(item, 1);
					} else {
						resourcesTreeViewer.expandToLevel(item, 1);
					}
				}
			}
		});
		resourcesTreeViewer
				.addSelectionChangedListener(new ISelectionChangedListener() {
					@Override
					public void selectionChanged(SelectionChangedEvent event) {
						ISelection selection = event.getSelection();
						if (!selection.isEmpty()) {
							setTextValue((TreeSelection) selection);
						}
					}

					private void setTextValue(TreeSelection selection) {
						StringBuilder sb = new StringBuilder();
						createString(selection.getPaths()[0], sb);
						resourceNameText.setText(sb.toString());
					}

					private void createString(TreePath path, StringBuilder sb) {
						if (path.equals(TreePath.EMPTY)) {
							return;
						} else {
							createString(path.getParentPath(), sb);

							Object obj = path.getLastSegment();
							if (obj instanceof CdoContainerContentProvider.Root) {
								sb.append(CdoContainerContentProvider.Root
										.displayString());
							} else if (obj instanceof CDOResourceNode) {
								CDOResourceNode node = (CDOResourceNode) obj;
								String nodeName = node.getName();
								sb.append(nodeName);
								if (node instanceof CDOResourceFolder) {
									sb.append(CdoContainerContentProvider.Root
											.displayString());
								}
							}
						}

					}

				});

		// This has to be done after the viewer has been laid out
		resourcesTreeViewer.setInput(nodeList);

		Label resourceNameLabel = new Label(resourcesArea, SWT.NONE);
		resourceNameLabel.setText("Path for the new Resource:  ");

		resourceNameText = new Text(resourcesArea, SWT.SINGLE | SWT.BORDER);
		GridData resourceNameData = new GridData(GridData.GRAB_HORIZONTAL
				| GridData.FILL_HORIZONTAL);
		resourceNameText.setLayoutData(resourceNameData);
		resourceNameText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				String text = ((Text) e.getSource()).getText();
				validateFolder(text);
			}
		});
	}

	private void createFileSelectionArea(Composite dialogArea) {
		Composite fileSelectionArea = new Composite(dialogArea, SWT.NONE);
		GridData fileSelectionData = new GridData(GridData.GRAB_HORIZONTAL
				| GridData.FILL_HORIZONTAL);
		fileSelectionArea.setLayoutData(fileSelectionData);
		GridLayout fileSelectionLayout = new GridLayout();
		fileSelectionLayout.numColumns = 3;
		fileSelectionLayout.marginWidth = 0;
		fileSelectionLayout.marginHeight = 0;
		fileSelectionArea.setLayout(fileSelectionLayout);

		importFileEditor = new FileFieldEditor("fileSelect",
				"Select Vocabulary XML File: ", fileSelectionArea);
		importFileEditor.getTextControl(fileSelectionArea).addModifyListener(
				new ModifyListener() {
					public void modifyText(ModifyEvent e) {
						validateFileSelection();
					}
				});
		String[] extensions = new String[] { "*.xml" }; // NON-NLS-1
		importFileEditor.setFileExtensions(extensions);
	}

	private void validateFileSelection() {
		try {
			String filename = importFileEditor.getStringValue();
			if (filename.length() > 0) {
				File file = new File(filename);
				if (!file.exists()) {
					setErrorMessage("The file does not exist.");
					importFileOkay = false;
					updatePageComplete();
					return;
				} else if (!file.toString().toLowerCase().endsWith(xmlSuffix)) {
					setErrorMessage("The file is not an XML file.");
					importFileOkay = false;
					updatePageComplete();
					return;
				}
			}
		} catch (Exception e) {
		}

		importFileOkay = true;
		setErrorMessage(null);
		updatePageComplete();
	}

	private void validateFolder(String text) {
		if (!text.startsWith(CdoContainerContentProvider.Root.displayString())) {
			setErrorMessage("Specify a valid vocabulary resource name starting with a / character.");
			resourceNameOkay = false;
			return;
		} else if (!text.toLowerCase().endsWith(".vocabulary")) {
			setErrorMessage("Specify a valid vocabulary resource name with the '.vocabulary' extension.");
			resourceNameOkay = false;
			return;
		}

		resourceNameOkay = true;
		setErrorMessage(null);
		updatePageComplete();

	}

	private void updatePageComplete() {
		setPageComplete(importFileOkay && resourceNameOkay);
	}

	public String getInputFilename() {
		return importFileEditor.getStringValue();
	}

	public String getResourceName() {
		return resourceNameText.getText();
	}
}
