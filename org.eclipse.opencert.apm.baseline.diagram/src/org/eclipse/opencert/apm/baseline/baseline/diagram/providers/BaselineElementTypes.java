/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.apm.baseline.baseline.diagram.providers;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypeImages;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypes;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivity2EditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityPrecedingActivityEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityProducedArtefactEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityRequiredArtefactEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityRoleEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseArtefactEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseFrameworkEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseRoleEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineDiagramEditorPlugin;

/**
 * @generated
 */
public class BaselineElementTypes {

	/**
	 * @generated
	 */
	private BaselineElementTypes() {
	}

	/**
	 * @generated
	 */
	private static Map<IElementType, ENamedElement> elements;

	/**
	 * @generated
	 */
	private static DiagramElementTypeImages elementTypeImages = new DiagramElementTypeImages(
			BaselineDiagramEditorPlugin.getInstance()
					.getItemProvidersAdapterFactory());

	/**
	 * @generated
	 */
	private static Set<IElementType> KNOWN_ELEMENT_TYPES;

	/**
	 * @generated
	 */
	public static final IElementType BaseFramework_1000 = getElementType("org.eclipse.opencert.apm.baseline.diagram.BaseFramework_1000"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType BaseActivity_2001 = getElementType("org.eclipse.opencert.apm.baseline.diagram.BaseActivity_2001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType BaseArtefact_2002 = getElementType("org.eclipse.opencert.apm.baseline.diagram.BaseArtefact_2002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType BaseRole_2003 = getElementType("org.eclipse.opencert.apm.baseline.diagram.BaseRole_2003"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType BaseActivity_3001 = getElementType("org.eclipse.opencert.apm.baseline.diagram.BaseActivity_3001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType BaseActivityRequiredArtefact_4001 = getElementType("org.eclipse.opencert.apm.baseline.diagram.BaseActivityRequiredArtefact_4001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType BaseActivityProducedArtefact_4002 = getElementType("org.eclipse.opencert.apm.baseline.diagram.BaseActivityProducedArtefact_4002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType BaseActivityPrecedingActivity_4003 = getElementType("org.eclipse.opencert.apm.baseline.diagram.BaseActivityPrecedingActivity_4003"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType BaseActivityRole_4004 = getElementType("org.eclipse.opencert.apm.baseline.diagram.BaseActivityRole_4004"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(ENamedElement element) {
		return elementTypeImages.getImageDescriptor(element);
	}

	/**
	 * @generated
	 */
	public static Image getImage(ENamedElement element) {
		return elementTypeImages.getImage(element);
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(IAdaptable hint) {
		return getImageDescriptor(getElement(hint));
	}

	/**
	 * @generated
	 */
	public static Image getImage(IAdaptable hint) {
		return getImage(getElement(hint));
	}

	/**
	 * Returns 'type' of the ecore object associated with the hint.
	 * 
	 * @generated
	 */
	public static ENamedElement getElement(IAdaptable hint) {
		Object type = hint.getAdapter(IElementType.class);
		if (elements == null) {
			elements = new IdentityHashMap<IElementType, ENamedElement>();

			elements.put(BaseFramework_1000,
					BaselinePackage.eINSTANCE.getBaseFramework());

			elements.put(BaseActivity_2001,
					BaselinePackage.eINSTANCE.getBaseActivity());

			elements.put(BaseArtefact_2002,
					BaselinePackage.eINSTANCE.getBaseArtefact());

			elements.put(BaseRole_2003, BaselinePackage.eINSTANCE.getBaseRole());

			elements.put(BaseActivity_3001,
					BaselinePackage.eINSTANCE.getBaseActivity());

			elements.put(BaseActivityRequiredArtefact_4001,
					BaselinePackage.eINSTANCE
							.getBaseActivity_RequiredArtefact());

			elements.put(BaseActivityProducedArtefact_4002,
					BaselinePackage.eINSTANCE
							.getBaseActivity_ProducedArtefact());

			elements.put(BaseActivityPrecedingActivity_4003,
					BaselinePackage.eINSTANCE
							.getBaseActivity_PrecedingActivity());

			elements.put(BaseActivityRole_4004,
					BaselinePackage.eINSTANCE.getBaseActivity_Role());
		}
		return (ENamedElement) elements.get(type);
	}

	/**
	 * @generated
	 */
	private static IElementType getElementType(String id) {
		return ElementTypeRegistry.getInstance().getType(id);
	}

	/**
	 * @generated
	 */
	public static boolean isKnownElementType(IElementType elementType) {
		if (KNOWN_ELEMENT_TYPES == null) {
			KNOWN_ELEMENT_TYPES = new HashSet<IElementType>();
			KNOWN_ELEMENT_TYPES.add(BaseFramework_1000);
			KNOWN_ELEMENT_TYPES.add(BaseActivity_2001);
			KNOWN_ELEMENT_TYPES.add(BaseArtefact_2002);
			KNOWN_ELEMENT_TYPES.add(BaseRole_2003);
			KNOWN_ELEMENT_TYPES.add(BaseActivity_3001);
			KNOWN_ELEMENT_TYPES.add(BaseActivityRequiredArtefact_4001);
			KNOWN_ELEMENT_TYPES.add(BaseActivityProducedArtefact_4002);
			KNOWN_ELEMENT_TYPES.add(BaseActivityPrecedingActivity_4003);
			KNOWN_ELEMENT_TYPES.add(BaseActivityRole_4004);
		}
		return KNOWN_ELEMENT_TYPES.contains(elementType);
	}

	/**
	 * @generated
	 */
	public static IElementType getElementType(int visualID) {
		switch (visualID) {
		case BaseFrameworkEditPart.VISUAL_ID:
			return BaseFramework_1000;
		case BaseActivityEditPart.VISUAL_ID:
			return BaseActivity_2001;
		case BaseArtefactEditPart.VISUAL_ID:
			return BaseArtefact_2002;
		case BaseRoleEditPart.VISUAL_ID:
			return BaseRole_2003;
		case BaseActivity2EditPart.VISUAL_ID:
			return BaseActivity_3001;
		case BaseActivityRequiredArtefactEditPart.VISUAL_ID:
			return BaseActivityRequiredArtefact_4001;
		case BaseActivityProducedArtefactEditPart.VISUAL_ID:
			return BaseActivityProducedArtefact_4002;
		case BaseActivityPrecedingActivityEditPart.VISUAL_ID:
			return BaseActivityPrecedingActivity_4003;
		case BaseActivityRoleEditPart.VISUAL_ID:
			return BaseActivityRole_4004;
		}
		return null;
	}

	/**
	 * @generated
	 */
	public static final DiagramElementTypes TYPED_INSTANCE = new DiagramElementTypes(
			elementTypeImages) {

		/**
		 * @generated
		 */
		@Override
		public boolean isKnownElementType(IElementType elementType) {
			return org.eclipse.opencert.apm.baseline.baseline.diagram.providers.BaselineElementTypes
					.isKnownElementType(elementType);
		}

		/**
		 * @generated
		 */
		@Override
		public IElementType getElementTypeForVisualId(int visualID) {
			return org.eclipse.opencert.apm.baseline.baseline.diagram.providers.BaselineElementTypes
					.getElementType(visualID);
		}

		/**
		 * @generated
		 */
		@Override
		public ENamedElement getDefiningNamedElement(
				IAdaptable elementTypeAdapter) {
			return org.eclipse.opencert.apm.baseline.baseline.diagram.providers.BaselineElementTypes
					.getElement(elementTypeAdapter);
		}
	};

}
