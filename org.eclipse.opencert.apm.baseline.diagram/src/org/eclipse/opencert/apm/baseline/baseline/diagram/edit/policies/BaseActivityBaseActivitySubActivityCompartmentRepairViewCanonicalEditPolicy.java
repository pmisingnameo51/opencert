/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.apm.baseline.baseline.diagram.edit.policies;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.cdo.common.id.CDOID;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.util.CDOUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.core.util.ViewUtil;
import org.eclipse.gmf.runtime.diagram.ui.commands.DeferredLayoutCommand;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.commands.SetViewMutabilityCommand;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CanonicalEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.notation.Bounds;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.impl.DiagramImpl;
import org.eclipse.opencert.infra.general.general.NamedElement;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.commands.LoadViewLocations;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.commands.RepairDawnBaseActivityViewLocationsCommand;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.commands.RepairDawnBaseFrameworkViewLocationsCommand;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineDiagramUpdater;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineNodeDescriptor;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry;
import org.eclipse.opencert.apm.baseline.baseline.impl.BaseFrameworkImpl;

/**
 * @generated NOT
 */
public class BaseActivityBaseActivitySubActivityCompartmentRepairViewCanonicalEditPolicy
		extends CanonicalEditPolicy {
	// Start MCP
	private LoadViewLocations sourceViewLocations;	
	private List<EObject> nodes = new ArrayList();
	private Map<String, String> eObjectToIDMap = null;	
	public void setSourceDiagram(LoadViewLocations sourceViewLocations)
	{
		this.sourceViewLocations = sourceViewLocations;
	}

	public List<EObject> getNodes()
	{
		return nodes;
	}
	// End MCP	

	/**
	 * @generated
	 */
	protected void refreshOnActivate() {
		// Need to activate editpart children before invoking the canonical refresh for EditParts to add event listeners
		List<?> c = getHost().getChildren();
		for (int i = 0; i < c.size(); i++) {
			((EditPart) c.get(i)).activate();
		}
		super.refreshOnActivate();
	}

	/**
	 * @generated
	 */
	protected EStructuralFeature getFeatureToSynchronize() {
		return BaselinePackage.eINSTANCE.getBaseActivity_SubActivity();
	}

	/**
	 * @generated
	 */
	@SuppressWarnings("rawtypes")
	protected List getSemanticChildrenList() {
		View viewObject = (View) getHost().getModel();
		LinkedList<EObject> result = new LinkedList<EObject>();
		List<BaselineNodeDescriptor> childDescriptors = BaselineDiagramUpdater
				.getBaseActivityBaseActivitySubActivityCompartment_7001SemanticChildren(viewObject);
		for (BaselineNodeDescriptor d : childDescriptors) {
			result.add(d.getModelElement());
		}
		return result;
	}

	/**
	 * @generated
	 */
	protected boolean isOrphaned(Collection<EObject> semanticChildren,
			final View view) {
		return isMyDiagramElement(view)
				&& !semanticChildren.contains(view.getElement());
	}

	/**
	 * @generated
	 */
	private boolean isMyDiagramElement(View view) {
		return BaseActivityEditPart.VISUAL_ID == BaselineVisualIDRegistry
				.getVisualID(view);
	}

	/**
	 * @generated NOT
	 */
	// Start MCP
	private void setIDs() {
		eObjectToIDMap = new HashMap<String, String>();
		
		List<EObject> result = getSemanticChildrenList();
		
		for(EObject elem : result)
		{
    		// Si CDO Native
	        CDOID cdoID = CDOUtil.getCDOObject(elem).cdoID();
	        Long  cdoIdLong = new Long(cdoID.toURIFragment());
			//Parece que cambian los objetos por su estado!!!
			//eObjectToIDMap.put(elem, cdoID.toString());
	        eObjectToIDMap.put(cdoID.toString(), cdoID.toString());
	        NamedElement me = (NamedElement)elem;
			System.out.println("key = "+ me.getId()+"|"+me.getName()+"|"+me.getClass().getName() + ". (" + cdoID.toString() + ")");    					
			//eObjectToIDMap.put(elem, me.getId()+me.getName()+me.eClass().getName());
		}
	}	
	// End MCP	
	
	/**
	 * @generated NOT
	 */
	// Start MCP	
	private String getID(String eObjectID) {
	    if (eObjectToIDMap == null)
	    {
	      return null;
	    }
	    else
	    {
	      return eObjectToIDMap.get(eObjectID);
	    }
	  }
	// End MCP
	
	/**
	 * @generated NOT
	 */
	protected void refreshSemantic() {
		if (resolveSemanticElement() == null) {
			return;
		}
		LinkedList<IAdaptable> createdViews = new LinkedList<IAdaptable>();
		List<BaselineNodeDescriptor> childDescriptors = BaselineDiagramUpdater
				.getBaseActivityBaseActivitySubActivityCompartment_7001SemanticChildren((View) getHost()
						.getModel());
		// Start MCP
		setIDs();
		
		Set<String> rootObjectIds = new HashSet<String>();
		for(BaselineNodeDescriptor next : childDescriptors)
		{
			EObject me = (EObject)next.getModelElement();
			if(me != null) 
			{
				String meID = "-1";
				CDOID mecdoID = CDOUtil.getCDOObject(me).cdoID();
				meID = getID(mecdoID.toString());
				if (meID != null)
				{
					nodes.add(me);
				}
			}			
		}
		//End MCP		
		
		LinkedList<View> orphaned = new LinkedList<View>();
		// we care to check only views we recognize as ours
		LinkedList<View> knownViewChildren = new LinkedList<View>();
		for (View v : getViewChildren()) {
			if (isMyDiagramElement(v)) {
				knownViewChildren.add(v);
			}
		}
		// alternative to #cleanCanonicalSemanticChildren(getViewChildren(), semanticChildren)
		//
		// iteration happens over list of desired semantic elements, trying to find best matching View, while original CEP
		// iterates views, potentially losing view (size/bounds) information - i.e. if there are few views to reference same EObject, only last one 
		// to answer isOrphaned == true will be used for the domain element representation, see #cleanCanonicalSemanticChildren()
		for (Iterator<BaselineNodeDescriptor> descriptorsIterator = childDescriptors
				.iterator(); descriptorsIterator.hasNext();) {
			BaselineNodeDescriptor next = descriptorsIterator.next();
			String hint = BaselineVisualIDRegistry.getType(next.getVisualID());
			LinkedList<View> perfectMatch = new LinkedList<View>(); // both semanticElement and hint match that of NodeDescriptor
			for (View childView : getViewChildren()) {
				EObject semanticElement = childView.getElement();
				if (next.getModelElement().equals(semanticElement)) {
					if (hint.equals(childView.getType())) {
						perfectMatch.add(childView);
						// actually, can stop iteration over view children here, but
						// may want to use not the first view but last one as a 'real' match (the way original CEP does
						// with its trick with viewToSemanticMap inside #cleanCanonicalSemanticChildren
					}
				}
			}
			if (perfectMatch.size() > 0) {
				descriptorsIterator.remove(); // precise match found no need to create anything for the NodeDescriptor
				// use only one view (first or last?), keep rest as orphaned for further consideration
				knownViewChildren.remove(perfectMatch.getFirst());
			}
		}
		// those left in knownViewChildren are subject to removal - they are our diagram elements we didn't find match to,
		// or those we have potential matches to, and thus need to be recreated, preserving size/location information.
		orphaned.addAll(knownViewChildren);
		//
		ArrayList<CreateViewRequest.ViewDescriptor> viewDescriptors = new ArrayList<CreateViewRequest.ViewDescriptor>(
				childDescriptors.size());
		for (BaselineNodeDescriptor next : childDescriptors) {
			String hint = BaselineVisualIDRegistry.getType(next.getVisualID());
			IAdaptable elementAdapter = new CanonicalElementAdapter(
					next.getModelElement(), hint);
			CreateViewRequest.ViewDescriptor descriptor = new CreateViewRequest.ViewDescriptor(
					elementAdapter, Node.class, hint, ViewUtil.APPEND, false,
					host().getDiagramPreferencesHint());
			viewDescriptors.add(descriptor);
		}

		boolean changed = deleteViews(orphaned.iterator());
		//
		CreateViewRequest request = getCreateViewRequest(viewDescriptors);
		Command cmd = getCreateViewCommand(request);
		/* cross project
		if (cmd != null && cmd.canExecute()) {
			SetViewMutabilityCommand.makeMutable(
					new EObjectAdapter(host().getNotationView())).execute();
			executeCommand(cmd);
			@SuppressWarnings("unchecked")
			List<IAdaptable> nl = (List<IAdaptable>) request.getNewObject();
			createdViews.addAll(nl);
		}
		*/
		if (changed || createdViews.size() > 0) {
			postProcessRefreshSemantic(createdViews);
		}
		
		// cross project if (createdViews.size() > 1) {
			// perform a layout of the container
			// Start MCP
			/*
			DeferredLayoutCommand layoutCmd = new DeferredLayoutCommand(host()
					.getEditingDomain(), createdViews, host());
			*/
			NamedElement me = (NamedElement)((View)getHost().getModel()).getElement();
			Bounds lc = (Bounds) sourceViewLocations.getObjectLocation(me.getId()+me.getName()+me.eClass().getName());
			Point offset = new Point(0,0);
			if(lc != null)
			{
				offset = new Point(lc.getX(), lc.getY());
			}
			//????MCPRepairDawnBaseActivityViewLocationsCommand layoutCmd = new RepairDawnBaseActivityViewLocationsCommand(sourceDiagramResource, offset, nodes, host().getEditingDomain(), (View)getHost().getModel(), host());
			RepairDawnBaseActivityViewLocationsCommand layoutCmd = new RepairDawnBaseActivityViewLocationsCommand(sourceViewLocations, offset, host().getEditingDomain(), (View)getHost().getModel(), host());
			// End MCP
			executeCommand(new ICommandProxy(layoutCmd));
		//}

		makeViewsImmutable(createdViews);
	}
}
