/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.opencert.apm.baseline.baseline.BaselineElement;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.RoundedRectangle;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.graphics.Color;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.policies.BaseActivityItemSemanticEditPolicy;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.policies.OpenDiagramEditPolicy;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry;
import org.eclipse.gmf.runtime.notation.Node;
/**
 * @generated
 */
public class BaseActivityEditPart extends ShapeNodeEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 2001;

	/**
	 * @generated
	 */
	protected IFigure contentPane;

	/**
	 * @generated
	 */
	protected IFigure primaryShape;

	/**
	 * @generated
	 */
	public BaseActivityEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new BaseActivityItemSemanticEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		installEditPolicy(EditPolicyRoles.OPEN_ROLE,
				new OpenDiagramEditPolicy());
		// XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated
	 */
	protected LayoutEditPolicy createLayoutEditPolicy() {
		org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy lep = new org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy() {

			protected EditPolicy createChildEditPolicy(EditPart child) {
				EditPolicy result = child
						.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null) {
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request) {
				return null;
			}

			protected Command getCreateCommand(CreateRequest request) {
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated NOT
	 */
	protected IFigure createNodeShape() {
		//return primaryShape = new BaseActivityFigure();
		//ALC Start
		// Start MCP
		if (!(((View) this.getModel()).getElement() instanceof BaseActivity)) {
			System.out.println("createNodeShape(): Error model not allowed");
			return primaryShape = new BaseActivityFigure(0);
		}		
		// End MCP
		BaseActivity elem = (BaseActivity) ((Node) this.getModel()).getElement();		
				
		// Start MCP
		if(elem == null){
			return primaryShape = new BaseActivityFigure(0);
		}else
		// End MCP
		if(elem.isIsSelected()){
			return primaryShape = new BaseActivityFigure(1);						
			
		}
		else{			
			return primaryShape = new BaseActivityFigure(0);
			
			
			
		}
		//ALC End
	}

	/**
	 * @generated
	 */
	public BaseActivityFigure getPrimaryShape() {
		return (BaseActivityFigure) primaryShape;
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof BaseActivityNameEditPart) {
			((BaseActivityNameEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureBaseActivityLabelFigure());
			return true;
		}
		if (childEditPart instanceof BaseActivityBaseActivitySubActivityCompartmentEditPart) {
			IFigure pane = getPrimaryShape()
					.getBaseActivitySubActivityCompartmentFigure();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane.add(((BaseActivityBaseActivitySubActivityCompartmentEditPart) childEditPart)
					.getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof BaseActivityNameEditPart) {
			return true;
		}
		if (childEditPart instanceof BaseActivityBaseActivitySubActivityCompartmentEditPart) {
			IFigure pane = getPrimaryShape()
					.getBaseActivitySubActivityCompartmentFigure();
			pane.remove(((BaseActivityBaseActivitySubActivityCompartmentEditPart) childEditPart)
					.getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart) {
		if (editPart instanceof BaseActivityBaseActivitySubActivityCompartmentEditPart) {
			return getPrimaryShape()
					.getBaseActivitySubActivityCompartmentFigure();
		}
		return getContentPane();
	}

	/**
	 * @generated
	 */
	protected NodeFigure createNodePlate() {
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(40, 40);
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createNodeFigure() {
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane.
	 * Respects layout one may have set for generated figure.
	 * @param nodeShape instance of generated figure class
	 * @generated
	 */
	protected IFigure setupContentPane(IFigure nodeShape) {
		if (nodeShape.getLayoutManager() == null) {
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			layout.setSpacing(5);
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane() {
		if (contentPane != null) {
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	protected void setForegroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setForegroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setBackgroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setBackgroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineWidth(int width) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineWidth(width);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineType(int style) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineStyle(style);
		}
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart() {
		return getChildBySemanticHint(BaselineVisualIDRegistry
				.getType(BaseActivityNameEditPart.VISUAL_ID));
	}

	

	/**
	 * @generated NOT
	 */
	protected void handleNotificationEvent(Notification event) {
		if (event.getNotifier() == getModel()
				&& EcorePackage.eINSTANCE.getEModelElement_EAnnotations()
						.equals(event.getFeature())) {
			handleMajorSemanticChange();
		} 
		//ALC Start
		else if (event.getNotifier() instanceof BaseActivity) {
			
			
			BaseActivity newElem = (BaseActivity) ((Node) this.getModel()).getElement();
			BaseActivityFigure figure = (BaseActivityFigure) this.getPrimaryShape();	
			
			// Start MCP
			if (newElem == null )
			{
				figure.setForegroundColor(new Color(null, 200, 200, 200));
			}else
			// End MCP
			if(newElem.isIsSelected()){
				
				

				EObject model=((Node) this.getModel()).getElement();												
				
				/*IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
				IWorkbenchPart  part = window.getActivePage().getActivePart();
				ISelection selection = window.getActivePage().getSelection();
				IStructuredSelection s = (IStructuredSelection)selection;
				if(s == null || s.isEmpty() || 
						!(s.getFirstElement() instanceof RefArtefactEditPart  || s.getFirstElement() instanceof RefRoleEditPart ))
				{*/
					updateChildSelection(model, true);

				/*}*/
																				
				
				///Select too the parents elements
				if (model.eContainer() != null){
					//updateParentSelection(model, true);
				}
				
				
				figure.setForegroundColor(new Color(null, 0, 0, 0));
								
				
			}
			else{
				
				EObject model=((Node) this.getModel()).getElement();
				
				updateChildSelection(model, false);
				
				
				figure.setForegroundColor(new Color(null, 200, 200, 200));							
				
				
			}
			//ALC End
			super.handleNotificationEvent(event);
		}
		// Start MCP
		else {
			super.handleNotificationEvent(event);
		}		
		// End MCP
	}

	//ALC Start
	private void updateParentSelection(EObject model, boolean selection) {
		if(model.eContainer() instanceof BaselineElement){
			BaselineElement metaclass = (BaselineElement)model.eContainer();
			metaclass.setIsSelected(true);
			
			if (metaclass.eContainer() != null){
				updateParentSelection(metaclass, selection);
			}
		}
	}

	private void updateChildSelection(EObject model, boolean selection) {
		for (int i=0;i<model.eContents().size();i++){	
			if(model.eContents().get(i) instanceof BaselineElement){
				BaselineElement metaclass = (BaselineElement)model.eContents().get(i);
				metaclass.setIsSelected(selection);
				
				if (metaclass.eContents().size()>0){
					updateChildSelection(metaclass,selection);
				}
			}
		}
	}
	//ALC End
	
	
	/**
	 * @generated
	 */
	public class BaseActivityFigure extends RoundedRectangle {

		/**
		 * @generated
		 */
		private WrappingLabel fFigureBaseActivityLabelFigure;
		/**
		 * @generated
		 */
		private RectangleFigure fBaseActivitySubActivityCompartmentFigure;

		/**
		 * @generated
		 */
		public BaseActivityFigure(int selected)  {
			this.setCornerDimensions(new Dimension(getMapMode().DPtoLP(8),
					getMapMode().DPtoLP(8)));
			this.setLineWidth(2);
			//this.setForegroundColor(THIS_FORE);
			
			if(selected==1){								
				
				this.setForegroundColor(new Color(null, 0, 0, 0));
			}
			else{				
				this.setForegroundColor(THIS_FORE);
			}

			this.setBorder(new MarginBorder(getMapMode().DPtoLP(5),
					getMapMode().DPtoLP(5), getMapMode().DPtoLP(5),
					getMapMode().DPtoLP(5)));
			createContents();
		}

		/**
		 * @generated
		 */
		private void createContents() {

			fFigureBaseActivityLabelFigure = new WrappingLabel();

			fFigureBaseActivityLabelFigure.setText("BaseActivity");
			fFigureBaseActivityLabelFigure.setMaximumSize(new Dimension(
					getMapMode().DPtoLP(10000), getMapMode().DPtoLP(50)));

			this.add(fFigureBaseActivityLabelFigure);

			fBaseActivitySubActivityCompartmentFigure = new RectangleFigure();

			fBaseActivitySubActivityCompartmentFigure.setOutline(false);

			this.add(fBaseActivitySubActivityCompartmentFigure);

		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureBaseActivityLabelFigure() {
			return fFigureBaseActivityLabelFigure;
		}

		/**
		 * @generated
		 */
		public RectangleFigure getBaseActivitySubActivityCompartmentFigure() {
			return fBaseActivitySubActivityCompartmentFigure;
		}

	}

	/**
	 * @generated NOT
	 */
	//ALC start
	static final Color THIS_FORE = new Color(null, 200, 200, 200);
	//ALC End

}
