/*******************************************************************************
 * Copyright (c) 2017 The Reuse Company
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 *   Lu�s Alonso - initial API and implementation
 *   Borja L�pez - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.evm.oslc.km.importevid.wizard;

import java.util.Hashtable;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class MyPageOne extends WizardPage {

	// Literals in the wizard
    private final String PageTitle = "Select the file to import";
    private final String PageDescription = "The file to be added as an evidence to a project";
    private final String InstructionsLabelText = "Select OSLC-KM parameters for the file to be imported as evidence:";
    private final String OslcKmTypeLabelText = "OSLC-KM type:";
    private final String SelectTransformationFileDialogTitle = "Select Transformation File";
    private final String SelectFileDialogTitle = "Select File";
    private final String FileLabelText = "File:";
    private final String AdditionalTransformationFileLabelText = "Additional transformation file:";
    private final String SearchButtonText = "...";
    
	// Default values
	private final int DefaultSelectedOslcKmType = 3;
	
	// instance variables
    private Composite container;
    private Text fileSelector;
    private Combo oslcKmTypeComboBox;
    private Text additionalTranformationFileTextBox;
    private Hashtable<String, Integer> oslcKmTypeToCode;
    private Hashtable<String, Boolean> oslcKmTypeNeedsExtraTranformationFile;
    private Hashtable<String, Boolean> oslcKmTypeMandatoryNeedsExtraTranformationFile;
    private Hashtable<String, String[]> oslcKmTypeNeedsExtraTranformationFileExtensions;
    private Hashtable<String, String[]> oslcKmTypeNeedsExtraTranformationFileExtensionsTitle;
    private Hashtable<String, String[]> oslcKmTypeFileExtensions;
    private Hashtable<String, String[]> oslcKmTypeFileExtensionsTitle;

    public MyPageOne() {
        super("OSLC KM Import Page");
        setTitle(PageTitle);
        setDescription(PageDescription);
    }

    @Override
    public void createControl(Composite parent) {
        GridLayout layout = new GridLayout();
        layout.numColumns = 3;
        container = new Composite(parent, SWT.NONE);
        container.setLayout(layout);
        // Instructions label
        Label instructionsLabel = new Label(container, SWT.NONE);
        instructionsLabel.setText(InstructionsLabelText);
        GridData instructionsGridLayout = new GridData();
        instructionsGridLayout.horizontalSpan = 3;
        instructionsLabel.setLayoutData(instructionsGridLayout);

        // OSLC-KM TYPE field
        Label oslcKmTypeLabel = new Label(container, SWT.NONE);
        oslcKmTypeLabel.setText(OslcKmTypeLabelText);
        oslcKmTypeComboBox = new Combo(container, SWT.BORDER | SWT.READ_ONLY);
        GridData oslcKmTypeComboBoxLayout = new GridData(GridData.FILL_HORIZONTAL);
        oslcKmTypeComboBoxLayout.horizontalSpan = 2;
        oslcKmTypeComboBox.setLayoutData(oslcKmTypeComboBoxLayout);
        Integer oslcTypeCode = -1;
        String oslcType = MyWizard.StringEmpty;
        String[] oslcTypeExtensions = null;
        String[] oslcTypeExtensionsTitle = null;
        String[] additionaTranformationFileExtensions = null;
        String[] additionaTranformationFileExtensionsTitle = null;
        boolean needsExtraFile = false;
        boolean mandatoryNeedsExtraFile = false;
        oslcKmTypeToCode = new Hashtable<>(MyWizard.OslcKmTypes.length);
        oslcKmTypeNeedsExtraTranformationFile = new Hashtable<>(MyWizard.OslcKmTypes.length);
        oslcKmTypeMandatoryNeedsExtraTranformationFile = new Hashtable<>(MyWizard.OslcKmTypes.length);
        oslcKmTypeFileExtensions = new Hashtable<>(MyWizard.OslcKmTypes.length);
        oslcKmTypeFileExtensionsTitle = new Hashtable<>(MyWizard.OslcKmTypes.length);
        oslcKmTypeNeedsExtraTranformationFileExtensions = new Hashtable<>(MyWizard.OslcKmTypes.length);
        oslcKmTypeNeedsExtraTranformationFileExtensionsTitle = new Hashtable<>(MyWizard.OslcKmTypes.length);
        for (int oslcKmTypeIndex = 0; oslcKmTypeIndex < MyWizard.OslcKmTypes.length; oslcKmTypeIndex++){
        	oslcTypeCode = MyWizard.OslcKmTypeCodes[oslcKmTypeIndex];
        	oslcTypeExtensions = MyWizard.OslcKmTypesFileExtensions[oslcKmTypeIndex];
        	oslcTypeExtensionsTitle = MyWizard.OslcKmTypesFileTitle[oslcKmTypeIndex];
        	additionaTranformationFileExtensions = MyWizard.OslcKmTypesAdditionalTransformationFileExtensions[oslcKmTypeIndex];
        	additionaTranformationFileExtensionsTitle = MyWizard.OslcKmTypesAdditionalTransformationFileTitle[oslcKmTypeIndex];
        	oslcType = MyWizard.OslcKmTypes[oslcKmTypeIndex];
        	needsExtraFile = MyWizard.OslcKmTypesNeedAdditionalFile[oslcKmTypeIndex];
        	mandatoryNeedsExtraFile = MyWizard.OslcKmTypesMandatoryNeedAdditionalFile[oslcKmTypeIndex];
        	if (oslcType != MyWizard.StringEmpty) {
        		oslcKmTypeComboBox.add(oslcType);
        		oslcKmTypeToCode.put(oslcType, oslcTypeCode);
        		oslcKmTypeNeedsExtraTranformationFile.put(oslcType, needsExtraFile);
        		oslcKmTypeFileExtensions.put(oslcType, oslcTypeExtensions);
        		oslcKmTypeFileExtensionsTitle.put(oslcType, oslcTypeExtensionsTitle);
        		oslcKmTypeNeedsExtraTranformationFileExtensions.put(oslcType, additionaTranformationFileExtensions);
        		oslcKmTypeNeedsExtraTranformationFileExtensionsTitle.put(oslcType, additionaTranformationFileExtensionsTitle);
        		oslcKmTypeMandatoryNeedsExtraTranformationFile.put(oslcType, mandatoryNeedsExtraFile);
        	}
        }
        
        // File field
        Label label1 = new Label(container, SWT.NONE);
        label1.setText(FileLabelText);
        fileSelector = new Text(container, SWT.READ_ONLY | SWT.BORDER | SWT.SINGLE);
        fileSelector.setText(MyWizard.StringEmpty);
        fileSelector.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        fileSelector.addKeyListener(new KeyListener() {

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
            	CheckPageIsCompleted();
            }

        });

        Button selectFileButton;
        selectFileButton = new Button(container, SWT.NONE);
        selectFileButton.setText(SearchButtonText);
        selectFileButton.addMouseListener(new MouseListener() {

			@Override
			public void mouseDoubleClick(MouseEvent arg0) {
			}

			@Override
			public void mouseDown(MouseEvent arg0) {
			}

			@Override
			public void mouseUp(MouseEvent arg0) {
				String oslcKmType; 
                oslcKmType =  oslcKmTypeComboBox.getItem(oslcKmTypeComboBox.getSelectionIndex());
                if (oslcKmType != MyWizard.StringEmpty) {
                	String[] oslcTypeExtensions = null;
                	oslcTypeExtensions = oslcKmTypeFileExtensions.get(oslcKmType);
                    String[] oslcTypeExtensionsTitle = null;
                    oslcTypeExtensionsTitle = oslcKmTypeFileExtensionsTitle.get(oslcKmType);
					// File standard dialog
					FileDialog fileDialog = new FileDialog(getShell());
					// Set the text
					fileDialog.setText(SelectFileDialogTitle);
					// Set filter on files
					fileDialog.setFilterExtensions(oslcTypeExtensions);
					// Put in a readable name for the filter
					fileDialog.setFilterNames(oslcTypeExtensionsTitle);
					// Open Dialog and save result of selection
					String selected = fileDialog.open();
    				if (selected != null && !selected.isEmpty()) {
    					fileSelector.setText(selected);
    				}
					CheckPageIsCompleted();
                }
			}
        	
        });
        
        // Additional transformation field
        Label additionalTranformationFileLabel = new Label(container, SWT.NONE);
        additionalTranformationFileLabel.setText(AdditionalTransformationFileLabelText);
        additionalTranformationFileTextBox = new Text(container, SWT.READ_ONLY | SWT.BORDER | SWT.SINGLE);
        additionalTranformationFileTextBox.setText(MyWizard.StringEmpty);
        additionalTranformationFileTextBox.addKeyListener(new KeyListener() {

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
            	CheckPageIsCompleted();
            }

        });
        additionalTranformationFileTextBox.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        Button selectAdditionalTransformationFileButton;
        selectAdditionalTransformationFileButton = new Button(container, SWT.NONE);
        selectAdditionalTransformationFileButton.setText(SearchButtonText);
        selectAdditionalTransformationFileButton.addMouseListener(new MouseListener() {

			@Override
			public void mouseDoubleClick(MouseEvent arg0) {
			}

			@Override
			public void mouseDown(MouseEvent arg0) {
			}

			@Override
			public void mouseUp(MouseEvent arg0) {
				String oslcKmType; 
                oslcKmType =  oslcKmTypeComboBox.getItem(oslcKmTypeComboBox.getSelectionIndex());
                if (oslcKmType != MyWizard.StringEmpty) {
            	    String[] additionaTranformationFileExtensions = null;
	                additionaTranformationFileExtensions = oslcKmTypeNeedsExtraTranformationFileExtensions.get(oslcKmType);
	                String[] additionaTranformationFileExtensionsTitle = null;
	                additionaTranformationFileExtensionsTitle = oslcKmTypeNeedsExtraTranformationFileExtensionsTitle.get(oslcKmType);
    				// File standard dialog
    				FileDialog fileDialog = new FileDialog(getShell());
    				// Set the text
    				fileDialog.setText(SelectTransformationFileDialogTitle);
    				// Set filter on files
    				fileDialog.setFilterExtensions(additionaTranformationFileExtensions);
    				// Put in a readable name for the filter
    				fileDialog.setFilterNames(additionaTranformationFileExtensionsTitle);
    				// Open Dialog and save result of selection
    				String selected = fileDialog.open();
    				if (selected != null && !selected.isEmpty()) {
    					additionalTranformationFileTextBox.setText(selected);	
    				}
    				CheckPageIsCompleted();
                }
			}
       	
        });
        
        
        oslcKmTypeComboBox.addSelectionListener(new SelectionAdapter() {
        	
        	@Override
        	 public void widgetSelected(SelectionEvent e) {
                String oslcKmType; 
                oslcKmType =  oslcKmTypeComboBox.getItem(oslcKmTypeComboBox.getSelectionIndex());
                if (oslcKmType != MyWizard.StringEmpty) {
                    Boolean needsExtraTransformationFile;
                    needsExtraTransformationFile = oslcKmTypeNeedsExtraTranformationFile.get(oslcKmType);
                	additionalTranformationFileTextBox.setVisible(needsExtraTransformationFile);
                	additionalTranformationFileLabel.setVisible(needsExtraTransformationFile);
                	selectAdditionalTransformationFileButton.setVisible(needsExtraTransformationFile);
                }
                fileSelector.setText(MyWizard.StringEmpty);
                additionalTranformationFileTextBox.setText(MyWizard.StringEmpty);
                CheckPageIsCompleted();
            }
        });
        oslcKmTypeComboBox.select(DefaultSelectedOslcKmType);

        // required to avoid an error in the system
        setControl(container);
        setPageComplete(false);

    }

    private void CheckPageIsCompleted() {
    	boolean isMandatoryAdditionalTranformationFile;
    	isMandatoryAdditionalTranformationFile = false;
		String oslcKmType; 
		oslcKmType =  oslcKmTypeComboBox.getItem(oslcKmTypeComboBox.getSelectionIndex());
		if (oslcKmType != MyWizard.StringEmpty) {
			isMandatoryAdditionalTranformationFile = oslcKmTypeMandatoryNeedsExtraTranformationFile.get(oslcKmType);
		}
    	boolean isCompleted;
    	isCompleted = oslcKmTypeComboBox.getSelectionIndex() >= 0 && 
		    			!fileSelector.getText().isEmpty() && (
		    					(additionalTranformationFileTextBox.getVisible() && (
		    																			(isMandatoryAdditionalTranformationFile && !additionalTranformationFileTextBox.getText().isEmpty())
		    																			|| 
		    																			!isMandatoryAdditionalTranformationFile
		    																		)
								) 
		    					|| !additionalTranformationFileTextBox.getVisible()
		    			); 
    	setPageComplete(isCompleted);
    }
    
    public int getOslcKMType() {
    	int type;
    	type = -1;
    	String oslcKmType; 
        oslcKmType =  oslcKmTypeComboBox.getItem(oslcKmTypeComboBox.getSelectionIndex());
        if (oslcKmType != MyWizard.StringEmpty) {
        	type = oslcKmTypeToCode.get(oslcKmType);
        }
        return type;
    }
    
    public String getFileFullPath() {
        return fileSelector.getText();
    }
    

    public String getAdditionalTransformationFileFullPath() {
        return additionalTranformationFileTextBox.getText();
    }

}