/*******************************************************************************
 * Copyright (c) 2017 The Reuse Company
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 *   Lu�s Alonso - initial API and implementation
 *   Borja L�pez - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.evm.oslc.km.importevid.wizard;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.HashMap;
import java.util.Scanner;

import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel;
import org.eclipse.opencert.evm.evidspec.evidence.EvidenceFactory;
import org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage;
import org.eclipse.opencert.evm.evidspec.evidence.Resource;
import org.eclipse.opencert.evm.evidspec.evidence.Value;
import org.eclipse.opencert.evm.evidspec.evidence.presentation.EvidenceEditorPlugin;
import org.eclipse.opencert.infra.general.general.ChangeEffectKind;
import org.eclipse.ui.PlatformUI;
// import org.eclipse.opencert.evm.oslc.km.preferences;
import com.google.gson.Gson;
import com.reusecompany.oslc.km.vocabs.srl.Artifact;
import com.reusecompany.oslc.km.vocabs.srl.MetaProperty;
import com.reusecompany.oslc.km.vocabs.srl.RSHP;

public class MyWizard extends Wizard {
    
	// General constants  
	public static final String StringEmpty = "";
	private static final String Delimiter = "\\A";

	// CDO Constants
	private static final String CDOEvidenceExtension = ".evidence";
	private static final String CDOEvidenceFolder = "/EVIDENCE/";
	
	// Default Remote Uri configuration:
	public static final String OSLCKM_WS_URL = "OSLCKM_WS_URL";
	// public static final String TrcAmassWebServiceUrl = "http://authoring.reusecompany.com:9999/OslcKmService.svc/GetSrlFromContent";
	// Debug: 
	// public static final String TrcAmassWebServiceUrl = "http://localhost:38306/OslcKmService.svc/GetSrlFromContent";
	// Release: test
	// public static final String TrcAmassWebServiceUrl = "http://authoring.reusecompany.com:9999/OslcKmService.svc/GetData/4";
	
	// File to be checked
	// private static final String PapyrusModel = "D:\\Projects\\RQA v17\\Rqa.Face.OslcKm\\Oslc.Km.Service.Amass.Tests\\Tests\\Papyrus\\model.uml";
	public static String[] OslcKmTypes = {
											"", // RshpLl = 0
									        "Excel", // 1
									        "XMI", // 2
									        "Rhapsody", // 3
									        "Papyrus", // 4
									        "MagicDraw", // 5
									        "", // IntegrityModeller = 6
									        "Other", //100
									        "", // 7
									        "Simulink", //8
									        "ASCE", // 9
									        "", // 10
									        "FMI_FMU", //11
									        "PureVariance", //12
									        "", // 13
									        "Metadata", //14
									        "SQL", // 15
									        "Xml", //16
									        "SRL as Json" //17
									        };
	private static int OslcKmType_Json = 17;
	
	public static int[] OslcKmTypeCodes = {
											0,
										    1,
										    2,
										    3,
										    4,
										    5,
										    6,
										    100,
										    7,
										    8,
										    9,
										    10,
										    11,
										    12,
										    13,
										    14,
										    15,
										    16,
										    OslcKmType_Json 
										  };
	
	public static boolean[] OslcKmTypesNeedAdditionalFile = {
																false,
														        true,
														        false,
														        false,
														        false,
														        false,
														        false,
														        true,
														        false,
														        true,
														        true,
														        true,
														        false,
														        false,
														        false,
														        false,
														        true,
														        true,
														        false,
													        };
	
	public static boolean[] OslcKmTypesMandatoryNeedAdditionalFile = {
																		false,
																		false,
																        false,
																        false,
																        false,
																        false,
																        false,
																        true,
																        false,
																        false,
																        false,
																        false,
																        false,
																        false,
																        false,
																        false,
																        false,
																        true,
																        false,
															        };

	private static String AllFilesExtension = "*.*";
	private static String AllFilesTitle = "All files (*.*)";
	
	private static String TextualFilesExtension = "*.txt";
	private static String TextualFilesTitle = "Text files (*.txt)";

	private static String ExcelFilesExtension = "*.xlsx";
	private static String ExcelFilesTitle = "Excel files (*.xlsx)";

	private static String ExcelFiles2003Extension = "*.xls";
	private static String ExcelFiles2003Title = "Excel 97-2003 files (*.xls)";

	private static String XmiFilesExtension = "*.xmi";
	private static String XmiFilesTitle = "XMI files (*.xmi)";
	
	private static String PapyrusFilesExtension = "*.uml";
	private static String PapyrusFilesTitle = "Papyrus files (*.uml)";

	private static String XmlFilesExtension = "*.xml";
	private static String XmlFilesTitle = "XML files (*.xml)";
	
	private static String MagicDrawFilesExtension = "*.xml";
	private static String MagicDrawFilesTitle = "Magic Draw files (*.xml)";
	
	private static String Simulink1FilesExtension = "*.mdl";
	private static String Simulink1FilesTitle = "Simulink files (*.mdl)";

	private static String Simulink2FilesExtension = "*.slx";
	private static String Simulink2FilesTitle = "Simulink files (*.slx)";

	private static String AsceFilesExtension = "*.axml";
	private static String AsceFilesTitle = "ASCE files (*.axml)";

	private static String FmuFilesExtension = "*.fmu";
	private static String FmuFilesTitle = "FMI/FMU files (*.fmu)";
	
	private static String PureVarianceFilesExtension = "*.xfm";
	private static String PureVarianceFilesTitle = "Pure Variance files (*.xfm)";
	
	private static String JsonFilesExtension = "*.json";
	private static String JsonFilesTitle = "JSON files (*.json)";
	
	private static String ExcelTransformationFilesExtension = "*.sqatplt";
	private static String ExcelTransformationFilesTitle = "EXCEL Transformation files (*.sqatplt)";
	
	private static String XMLTransformationFilesExtension = "*.xslt";
	private static String XMLTransformationFilesTitle = "XML Transformation files (*.xslt)";
	
	public static final String[][] OslcKmTypesFileExtensions  = {
																new String[] { TextualFilesExtension, AllFilesExtension },
																new String[] { ExcelFilesExtension, ExcelFiles2003Extension, AllFilesExtension },
																new String[] { XmiFilesExtension, AllFilesExtension },
																new String[] { XmiFilesExtension, AllFilesExtension },
																new String[] { PapyrusFilesExtension, AllFilesExtension },
																new String[] { MagicDrawFilesExtension, AllFilesExtension },
																null,
																new String[] { XmlFilesExtension, AllFilesExtension },
														        null,
														        new String[] { Simulink1FilesExtension, Simulink2FilesExtension, AllFilesExtension },
														        new String[] { AsceFilesExtension, AllFilesExtension },
														        null,
														        new String[] { FmuFilesExtension, AllFilesExtension },
														        new String[] { PureVarianceFilesExtension, AllFilesExtension },
														        null,
														        new String[] { AllFilesExtension },
														        new String[] { XmlFilesExtension, AllFilesExtension },
														        new String[] { XmlFilesExtension, AllFilesExtension },
														        new String[] { JsonFilesExtension, AllFilesExtension },

													        };
	
	public static final String[][] OslcKmTypesFileTitle  = {
																new String[] { TextualFilesTitle, AllFilesTitle },
																new String[] { ExcelFilesTitle, ExcelFiles2003Title, AllFilesTitle },
																new String[] { XmiFilesTitle, AllFilesTitle },
																new String[] { XmiFilesTitle, AllFilesTitle },
																new String[] { PapyrusFilesTitle, AllFilesTitle },
																new String[] { MagicDrawFilesTitle, AllFilesTitle },
																null,
																new String[] { XmlFilesTitle, AllFilesTitle },
														        null,
														        new String[] { Simulink1FilesTitle, Simulink2FilesTitle, AllFilesTitle },
														        new String[] { AsceFilesTitle, AllFilesTitle },
														        null,
														        new String[] { FmuFilesTitle, AllFilesTitle },
														        new String[] { PureVarianceFilesTitle, AllFilesTitle  },
														        null,
														        new String[] { AllFilesTitle },
														        new String[] { XmlFilesTitle, AllFilesTitle },
														        new String[] { XmlFilesExtension, AllFilesTitle },
														        new String[] { JsonFilesTitle, AllFilesTitle },
															};
	
	public static final String[][] OslcKmTypesAdditionalTransformationFileExtensions  = {
																							new String[] { AllFilesExtension },
																							new String[] { ExcelTransformationFilesExtension, AllFilesExtension },
																							new String[] { AllFilesExtension },
																							new String[] { AllFilesExtension },
																							new String[] { AllFilesExtension },
																							new String[] { AllFilesExtension },
																							null,
																							new String[] { XMLTransformationFilesExtension, AllFilesExtension },
																					        null,
																					        new String[] { AllFilesExtension },
																					        new String[] { AllFilesExtension },
																					        null,
																					        new String[] { AllFilesExtension },
																					        new String[] { AllFilesExtension },
																					        null,
																					        new String[] { AllFilesExtension },
																					        new String[] { AllFilesExtension },
																					        new String[] { XmlFilesExtension, AllFilesExtension },
																					        new String[] { AllFilesExtension },
																				        };

	public static final String[][] OslcKmTypesAdditionalTransformationFileTitle  = {
																					new String[] { AllFilesTitle },
																					new String[] { ExcelTransformationFilesTitle, AllFilesTitle },
																					new String[] { AllFilesTitle },
																					new String[] { AllFilesTitle },
																					new String[] { AllFilesTitle },
																					new String[] { AllFilesTitle },
																					null,
																					new String[] { XMLTransformationFilesTitle, AllFilesTitle },
																			        null,
																			        new String[] { AllFilesTitle },
																			        new String[] { AllFilesTitle },
																			        null,
																			        new String[] { AllFilesTitle },
																			        new String[] { AllFilesTitle  },
																			        null,
																			        new String[] { AllFilesTitle },
																			        new String[] { AllFilesTitle },
																			        new String[] { XmlFilesExtension, AllFilesTitle },
																			        new String[] { AllFilesTitle },
																				};
	
	// Messages
    private static final String Title =  "OSLC-KM Evidence Manager Importer";
	private static final String SuccessMessage = "Evidences imported successfully!";
	private static final String InvalidSrlFormat = "Cannot read SRL from the selected document!";
	private static final String EmptyModelMessage = "The selected model is empty!";
	private static final String WsNotRunningMessage = "OSLC-KM Web Service Not Running at: ";
	
    protected MyPageOne one;
    protected MyPageTwo two;

    public MyWizard() {
        super();
        setNeedsProgressMonitor(true);
    }

    @Override
    public String getWindowTitle() {
        return Title;
    }

    @Override
    public void addPages() {
        one = new MyPageOne();
        addPage(one);
        two = new MyPageTwo(CDOConnectionUtil.instance.getCurrentSession());
        addPage(two);
    }

    @Override
    public boolean performFinish() {
    	boolean success;
    	success = false;
		String json;
		String webServiceUrl;
		webServiceUrl = PlatformUI.getPreferenceStore().getString(OSLCKM_WS_URL);
		try {
			int oslcKmType = one.getOslcKMType();
			if (oslcKmType != OslcKmType_Json) {
				json = getJsonFromTrcAmassWebServicePost(oslcKmType, 
														one.getFileFullPath(), 
														Charset.forName(Utf8Format), 
														one.getAdditionalTransformationFileFullPath(), 
														webServiceUrl);
			} else {
				json = getJsonFromFile(one.getFileFullPath(), Charset.forName(Utf8Format));
			}
			if (json != null && !json.isEmpty()) {
				Artifact srlArtifact;
				srlArtifact = null;
				String errorMessage;
				errorMessage = null;
				try {
					srlArtifact = GetSrlArtifactFromJson(json);
					success = true;
				} catch (Exception ex){
					success = false;
					errorMessage = ex.getMessage();
				}
				if (success) {
					ConnectCDO();
					EObject cdoArtefactModel;
					cdoArtefactModel = MapSrlArtifactToEvidence(srlArtifact);
					String resourceURI;
					resourceURI = two.getSelectedCertificationProjectUri();
					String evidenceName;
					evidenceName = two.getNewEvidenceName();
					saveToCdo(cdoArtefactModel, resourceURI, evidenceName);
					MessageDialog.openInformation(getShell(), Title, SuccessMessage);
					success = true;
				} else {
					MessageDialog.openInformation(getShell(), Title, InvalidSrlFormat + errorMessage + "\r\n'" + json + "'");
				}
			} else {
				MessageDialog.openInformation(getShell(), Title, EmptyModelMessage);
			}
		} catch (java.net.ConnectException e) {
			MessageDialog.openInformation(getShell(), Title, WsNotRunningMessage + webServiceUrl);
		}
        return success;
    }
    
	
	// Web request constants
	//   Web request header properties and values:
	private static final String HttpPostProtocolName = "POST";
	private static final String ContentTypeRequestProperty = "Content-Type";
	private static final String JsonContentTypeRequestPropertyValue = "application/json";
	private static final String ContentLengthRequestProperty = "Content-Length";
	private static final String AsciiFormat = "ASCII";
	private static final String Utf8Format = "UTF-8";
	//   Parameters:
	private static final String OslcKmTypeParam = "type";
	private static final String AdditionalParametersParam = "additionalParameters";
	private static final String FileContentParam = "fileContent";
	//   Json parts:
	private static final String JsonStartWebRequest = "{\"";
	private static final String JsonEndWebRequest = "\"}";
	private static final String JsonAttributeValueSeparatorWebRequest = "\":\"";
	private static final String JsonElementSeparatorWebRequest = "\",\"";
	//   Answer headers / footers:
	private static final String XMLResponseHeader = "<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">";
	private static final String XMLResponseFooter = "</string>";
	//   Debug messages:
	private static final String HttpWebRequestErrorMessage = "Failed : HTTP error code : ";

	public String getJsonFromFile(String fileName, Charset encoding) {
		String fileContent;
		fileContent = null;
		try {
			fileContent = readFile(fileName, encoding);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return fileContent;
	}
	
	public String getJsonFromTrcAmassWebServicePost(int oslcKmType, String fileName, Charset encoding, String additionalParameters, String oslcKmWsUri) throws java.net.ConnectException {
		String result;
		result = null;
		HttpURLConnection conn;
		conn = null; 
		InputStream inputStream;
		inputStream = null;
		Scanner s ;
		s = null;
		try {
			byte[] fileContent;
			fileContent = readFile(fileName);
			if (fileContent != null && fileContent.length>0) {
				byte[] additionalParametersContent;
				additionalParametersContent = null;
				if (additionalParameters != null && !additionalParameters.equals(StringEmpty)) {
					additionalParametersContent = readFile(additionalParameters);
				}
				// Building POST parameters
				StringBuilder postData = new StringBuilder();
				postData.append(JsonStartWebRequest);
				postData.append(OslcKmTypeParam);
				postData.append(JsonAttributeValueSeparatorWebRequest);
				postData.append(oslcKmType);
				postData.append(JsonElementSeparatorWebRequest);
				postData.append(AdditionalParametersParam);
				postData.append(JsonAttributeValueSeparatorWebRequest);
				postData.append(GetBase64StringFromBytes(additionalParametersContent));
				postData.append(JsonElementSeparatorWebRequest);
				postData.append(FileContentParam);
				postData.append(JsonAttributeValueSeparatorWebRequest);
				postData.append(GetBase64StringFromBytes(fileContent));
				postData.append(JsonEndWebRequest);
				
		        byte[] postDataBytes = postData.toString().getBytes(AsciiFormat);
	
				URL url = new URL(oslcKmWsUri);
				
		        conn = (HttpURLConnection)url.openConnection();
		        conn.setRequestMethod(HttpPostProtocolName);
		        conn.setRequestProperty(ContentTypeRequestProperty, JsonContentTypeRequestPropertyValue);
		        conn.setRequestProperty(ContentLengthRequestProperty, String.valueOf(postDataBytes.length));
		        conn.setDoOutput(true);
		        conn.getOutputStream().write(postDataBytes);
	
				if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
					System.out.println(HttpWebRequestErrorMessage + conn.getResponseCode());
				} else {
					inputStream = conn.getInputStream();
					s = new Scanner(inputStream);
					s.useDelimiter(Delimiter);
					result = s.hasNext() ? s.next() : StringEmpty;
					if (result != null && !result.equals(StringEmpty)) {
						result = result.replace(XMLResponseHeader, StringEmpty);
						result = result.replace(XMLResponseFooter, StringEmpty);
					}
				}
			}
		} catch (java.net.ConnectException e) {
			result = e.getMessage() + " @ " + oslcKmWsUri;
			throw e;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (s != null) {
				s.close();
				s = null;
			}
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch(Exception ex1) {
					
				}
				inputStream = null;
			}
			if (conn != null) {
				conn.disconnect();
				conn = null;
			} 
		}
		return result;
	}
	
	private static String readFile(String path, Charset encoding) 
			  throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}
	private static byte[] readFile(String path) 
			   {
		byte[] encoded=null;
		try {
			encoded = Files.readAllBytes(Paths.get(path));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return encoded;
	}	
	private static String GetBase64StringFromBytes(byte[] bytes){
		String value;
		value = StringEmpty;
		if (bytes != null ) {
			value = Base64.getEncoder().encodeToString(bytes);
		}
		return value;
	}
	
	protected Artifact GetSrlArtifactFromJson(String json) {
		final Gson gson;
		gson = new Gson();
		Artifact srlArtifact;
		srlArtifact = null;
		Reader inputString;
		inputString = null;
		BufferedReader inFromUser;
		inFromUser = null;
		try {
			inputString = new StringReader(json);
			inFromUser = new BufferedReader(inputString);
			srlArtifact = gson.fromJson(inFromUser, Artifact.class);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			throw ex;
		} finally {
			if (inputString != null) {
				try {
					inputString.close();
					inputString = null;
				} catch (Exception ex) {
				}
			}
			if (inFromUser != null) {
				try {
					inFromUser.close();
					inFromUser = null;
				} catch (Exception ex) {
				}
			}
		}
		return srlArtifact;
	}
	
	protected EObject MapSrlArtifactToEvidence(Artifact srlArtifact) {
		EvidencePackage evidencePackage = EvidencePackage.eINSTANCE;
		EvidenceFactory evidenceFactory = evidencePackage.getEvidenceFactory();
		// Mapping
		Artefact cdoArtefact;
		HashMap<String, Artefact> createdCdoArtefacts;
		createdCdoArtefacts = new HashMap<String, Artefact>();
		cdoArtefact = CreateCdoArtefactFromSrlArtefact(evidenceFactory, srlArtifact, createdCdoArtefacts);
		createdCdoArtefacts.clear();
		createdCdoArtefacts = null;
		ArtefactDefinition cdoArtefactDefinition;
		cdoArtefactDefinition = evidenceFactory.createArtefactDefinition();
		if (cdoArtefact !=  null) {
			cdoArtefactDefinition.getArtefact().add(cdoArtefact);
		}
		ArtefactModel cdoArtefactModel;
		cdoArtefactModel = evidenceFactory.createArtefactModel();
		if (srlArtifact != null && srlArtifact.getTitle() != null) {
			cdoArtefactModel.setName(srlArtifact.getTitle());
		}
		cdoArtefactModel.getArtefact().add(cdoArtefactDefinition);
		return cdoArtefactModel;
	}
	
	private static Artefact CreateCdoArtefactFromSrlArtefact(EvidenceFactory evidenceFactory, Artifact srlArtifact, HashMap<String, Artefact> createdCdoArtefacts) {
		Artefact cdoArtefact;
		cdoArtefact = null;
		if (evidenceFactory != null && srlArtifact != null) {
			String srlArtifactIdentifier;
			srlArtifactIdentifier = srlArtifact.getIdentifier();
			if (!createdCdoArtefacts.containsKey(srlArtifactIdentifier)) {
				cdoArtefact = evidenceFactory.createArtefact();
				cdoArtefact.setId(srlArtifactIdentifier);
				createdCdoArtefacts.put(srlArtifact.getIdentifier(), cdoArtefact);
				cdoArtefact.setName(GetSrlArtifactName(srlArtifact));
				Resource cdoResource;
				cdoResource = evidenceFactory.createResource();
				if (srlArtifact.getArtifactType() != null && srlArtifact.getArtifactType().getName() != null) {
					cdoResource.setFormat(srlArtifact.getArtifactType().getName());
				}
				cdoArtefact.getResource().add(cdoResource);
				Artifact sourceSrlArtifact, targetSrlArtifact;
				Artefact sourceCdoArtifact, targetCdoArtifact;
				ArtefactRel cdoArtefactRel;
				String relationshipTypeName;
				String relationshipTypeNameFormat;
				String sourceSrlArtifactName;
				String targetSrlArtifactName;
				
				for (RSHP relationship : srlArtifact.getRshpsAsList()) {
					sourceSrlArtifact = relationship.getFrom();
					sourceSrlArtifactName = GetSrlArtifactName(sourceSrlArtifact);
					targetSrlArtifact = relationship.getTo();
					targetSrlArtifactName = GetSrlArtifactName(targetSrlArtifact);
					relationshipTypeName = null;
					if (relationship.getRshpType() !=  null) {
						relationshipTypeName = relationship.getRshpType().getName();
					}
					if (sourceSrlArtifact != null && targetSrlArtifact != null) {
						sourceCdoArtifact = CreateCdoArtefactFromSrlArtefact(evidenceFactory, sourceSrlArtifact, createdCdoArtefacts);
						targetCdoArtifact = CreateCdoArtefactFromSrlArtefact(evidenceFactory, targetSrlArtifact, createdCdoArtefacts);
						if (sourceCdoArtifact != null && targetCdoArtifact != null) {
							cdoArtefact.getArtefactPart().add(sourceCdoArtifact);
							cdoArtefact.getArtefactPart().add(targetCdoArtifact);
							cdoArtefactRel = evidenceFactory.createArtefactRel();
							cdoArtefactRel.setId( sourceSrlArtifactName +  " - " + targetSrlArtifactName);
							relationshipTypeNameFormat =  "�" + relationshipTypeName + "�: ";
							if (relationshipTypeName == null) {
								relationshipTypeNameFormat =  "�Generic�: ";
							}
							cdoArtefactRel.setName(relationshipTypeNameFormat + "('" + sourceSrlArtifactName + "', '" + targetSrlArtifactName+ "')");
							cdoArtefactRel.setSource(sourceCdoArtifact);
							cdoArtefactRel.setTarget(targetCdoArtifact);
							cdoArtefactRel.setModificationEffect(ChangeEffectKind.MODIFY);
							cdoArtefactRel.setRevocationEffect(ChangeEffectKind.REVOKE);
							cdoArtefact.getOwnedRel().add(cdoArtefactRel);
						}
					}
				}
				// Owned Elements
				Artefact cdoAOwnedrtefact;
				for (Artifact ownedArtifact : srlArtifact.getOwnedArtifactsAsList()) {
					cdoAOwnedrtefact = CreateCdoArtefactFromSrlArtefact(evidenceFactory, ownedArtifact, createdCdoArtefacts);
					if (cdoAOwnedrtefact != null) {
						cdoArtefact.getArtefactPart().add(cdoAOwnedrtefact);
					}
				}
				Value cdoValue;
				for (MetaProperty meta : srlArtifact.getMetaPropertiesAsList()) {
					if (meta != null) {
						if (meta.GetTag() != null && meta.GetTag().getTerm() != null && meta.GetTag().getTerm().getPrefLabel() != null
								&& meta.GetValue() != null && meta.GetValue().getTerm() != null && meta.GetValue().getTerm().getPrefLabel() != null) {
							cdoValue = evidenceFactory.createValue();
							cdoValue.setName(meta.GetTag().getTerm().getPrefLabel());
							cdoValue.setValue(meta.GetValue().getTerm().getPrefLabel());
							cdoArtefact.getPropertyValue().add(cdoValue);
						}
					}
				}
			} else {
				cdoArtefact = createdCdoArtefacts.get(srlArtifactIdentifier);	
			}
		}
		return cdoArtefact;
	}
	
	private static String GetSrlArtifactName(Artifact srlArtifact) {
		String name;
		name = StringEmpty;
		if (srlArtifact.getDescription() != null) {
			name = srlArtifact.getDescription();
		} else if (srlArtifact.getTerm() != null && srlArtifact.getTerm().getPrefLabel() != null) {
			name = srlArtifact.getTerm().getPrefLabel();		
		}
		return name;
	}
	
	// private CDOView view;
	private CDOResource resource;
	private CDOTransaction transaction;
	
	private void ConnectCDO() {
		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		CDOSession cdoSession = CDOConnectionUtil.instance.getCurrentSession();
		transaction = cdoSession.openTransaction();
	}
	
	public boolean saveToCdo(EObject cdoArtefactModel, String projectURI, String evidenceName) {
		boolean success;
		success = Boolean.FALSE;
		try {
			if (evidenceName != null) {
				if (!evidenceName.endsWith(CDOEvidenceExtension)) {
					evidenceName = evidenceName + CDOEvidenceExtension;
				}
				resource = transaction.getOrCreateResource(projectURI + CDOEvidenceFolder + evidenceName);

				if (cdoArtefactModel != null) {
					resource.getContents().add(cdoArtefactModel);
				}

				HashMap<Object, Object> options = new HashMap<Object, Object>();
				options.put(XMLResource.OPTION_ENCODING, Utf8Format);

				resource.save(options);
				
				transaction.close();
				success = Boolean.TRUE;
			}
		} catch (Exception exception) {
			success = Boolean.FALSE;
			EvidenceEditorPlugin.INSTANCE.log(exception);
			throw new RuntimeException(exception);
		} finally {

		}
		return success;
	}

	
	/*
	private Artifact getDummyArtifact() {
		ArtifactType srlArtifactType;
		srlArtifactType = new ArtifactType("dummy");
		Artifact srlArtifact, sourceSrlArtifact, targetSrlArtifact;
		Term sourceTerm;
		sourceTerm = new Term();
		sourceTerm.setPrefLabel("sourceTerm");
		Term targetTerm;
		targetTerm = new Term();
		targetTerm.setPrefLabel("targetTerm");
		sourceSrlArtifact = new Artifact();
		sourceSrlArtifact.setIdentifier("Artifact 1");
		sourceSrlArtifact.setTitle("Artifact 1 Title");
		sourceSrlArtifact.setTerm(sourceTerm);
		targetSrlArtifact = new Artifact();
		targetSrlArtifact.setIdentifier("Artifact 2");
		targetSrlArtifact.setTitle("Artifact 2 Title");
		targetSrlArtifact.setTerm(targetTerm);
		srlArtifact = new Artifact();
		srlArtifact.setArtifactType(srlArtifactType);
		srlArtifact.setIdentifier("Root Artifact");
		srlArtifact.setTitle("Root Artifact Title");
		RSHP relationship;
		relationship = new RSHP();
		relationship.setTo(sourceSrlArtifact);
		relationship.setFrom(targetSrlArtifact);
		RSHPType relationshipType;
		relationshipType = new RSHPType();
		relationshipType.setName("Relationship Type 1");
		relationship.setRshpType(relationshipType);
		srlArtifact.getRshpsAsList().add(relationship);
		return srlArtifact;
	}
	
	private String getDummyJson() {
		String json;
		Artifact srlArtifact;
		srlArtifact = getDummyArtifact();
		final GsonBuilder gsonBuilder = new GsonBuilder();
	    gsonBuilder.setPrettyPrinting();
		Gson gson;
		gson = gsonBuilder.create();
		json = gson.toJson(srlArtifact);
		System.out.println(json);
		return json;
	}
	*/
    
}