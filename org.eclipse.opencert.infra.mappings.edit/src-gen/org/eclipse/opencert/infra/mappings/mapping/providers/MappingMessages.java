/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.infra.mappings.mapping.providers;

import org.eclipse.osgi.util.NLS;

/**
 * 
 * 
 */
public class MappingMessages extends NLS {
	
	private static final String BUNDLE_NAME = "org.eclipse.opencert.infra.mappings.mapping.providers.mappingMessages"; //$NON-NLS-1$

	
	public static String MapModelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String MapGroupPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String MapPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String MapJustificationPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ComplianceMapPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String EquivalenceMapPropertiesEditionPart_PropertiesGroupLabel;


	
	public static String MapModel_ReadOnly;

	
	public static String MapModel_Part_Title;

	
	public static String MapGroup_ReadOnly;

	
	public static String MapGroup_Part_Title;

	
	public static String Map_ReadOnly;

	
	public static String Map_Part_Title;

	
	public static String MapJustification_ReadOnly;

	
	public static String MapJustification_Part_Title;

	
	public static String ComplianceMap_ReadOnly;

	
	public static String ComplianceMap_Part_Title;

	
	public static String EquivalenceMap_ReadOnly;

	
	public static String EquivalenceMap_Part_Title;


	
	public static String MapModelPropertiesEditionPart_IdLabel;

	
	public static String MapModelPropertiesEditionPart_NameLabel;

	
	public static String MapModelPropertiesEditionPart_DescriptionLabel;

	
	public static String MapModelPropertiesEditionPart_MapGroupModelLabel;

	
	public static String MapModelPropertiesEditionPart_MapModelLabel;

	
	public static String MapGroupPropertiesEditionPart_IdLabel;

	
	public static String MapGroupPropertiesEditionPart_NameLabel;

	
	public static String MapGroupPropertiesEditionPart_DescriptionLabel;

	
	public static String MapPropertiesEditionPart_IdLabel;

	
	public static String MapPropertiesEditionPart_NameLabel;

	
	public static String MapPropertiesEditionPart_MapGroupLabel;

	
	public static String MapPropertiesEditionPart_TypeLabel;

	
	public static String MapJustificationPropertiesEditionPart_ExplanationLabel;

	
	public static String ComplianceMapPropertiesEditionPart_IdLabel;

	
	public static String ComplianceMapPropertiesEditionPart_NameLabel;

	
	public static String ComplianceMapPropertiesEditionPart_MapGroupLabel;

	
	public static String ComplianceMapPropertiesEditionPart_TypeLabel;

	
	public static String EquivalenceMapPropertiesEditionPart_IdLabel;

	
	public static String EquivalenceMapPropertiesEditionPart_NameLabel;

	
	public static String EquivalenceMapPropertiesEditionPart_MapGroupLabel;

	
	public static String EquivalenceMapPropertiesEditionPart_TypeLabel;


	
	public static String PropertiesEditionPart_DocumentationLabel;

	
	public static String PropertiesEditionPart_IntegerValueMessage;

	
	public static String PropertiesEditionPart_FloatValueMessage;

	
	public static String PropertiesEditionPart_ShortValueMessage;

	
	public static String PropertiesEditionPart_LongValueMessage;

	
	public static String PropertiesEditionPart_ByteValueMessage;

	
	public static String PropertiesEditionPart_BigIntegerValueMessage;

	
	public static String PropertiesEditionPart_BigDecimalValueMessage;

	
	public static String PropertiesEditionPart_DoubleValueMessage;

	
	public static String PropertiesEditionPart_PropertiesGroupLabel;

	
	public static String PropertiesEditionPart_RequiredFeatureMessage;

	
	public static String PropertiesEditionPart_AddTableViewerLabel;

	
	public static String PropertiesEditionPart_EditTableViewerLabel;

	
	public static String PropertiesEditionPart_RemoveTableViewerLabel;

	
	public static String PropertiesEditionPart_AddListViewerLabel;

	
	public static String PropertiesEditionPart_RemoveListViewerLabel;

	// Start of user code for additionnal NLS Constants
	
	// End of user code

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, MappingMessages.class);
	}

	
	private MappingMessages() {
		//protect instanciation
	}
}
