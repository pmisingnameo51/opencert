/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.baseline.baseline.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.infra.properties.property.PropertyPackage;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivityRel;
import org.eclipse.opencert.apm.baseline.baseline.BaseApplicability;
import org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityLevel;
import org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityRel;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel;
import org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement;
import org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap;
import org.eclipse.opencert.apm.baseline.baseline.BaseControlCategory;
import org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityApplicability;
import org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityLevel;
import org.eclipse.opencert.apm.baseline.baseline.BaseEquivalenceMap;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.apm.baseline.baseline.BaseIndependencyLevel;
import org.eclipse.opencert.apm.baseline.baseline.BaseRecommendationLevel;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirementRel;
import org.eclipse.opencert.apm.baseline.baseline.BaseRole;
import org.eclipse.opencert.apm.baseline.baseline.BaseTechnique;
import org.eclipse.opencert.apm.baseline.baseline.BaselineElement;
import org.eclipse.opencert.apm.baseline.baseline.BaselineFactory;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.infra.mappings.mapping.MappingPackage;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BaselinePackageImpl extends EPackageImpl implements BaselinePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseFrameworkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseRequirementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseArtefactEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseActivityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseRequirementRelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseRoleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseApplicabilityLevelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseCriticalityLevelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseTechniqueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseArtefactRelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseCriticalityApplicabilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseActivityRelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseAssurableElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseIndependencyLevelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseRecommendationLevelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseControlCategoryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseApplicabilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseApplicabilityRelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baselineElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseEquivalenceMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseComplianceMapEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private BaselinePackageImpl() {
		super(eNS_URI, BaselineFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link BaselinePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static BaselinePackage init() {
		if (isInited) return (BaselinePackage)EPackage.Registry.INSTANCE.getEPackage(BaselinePackage.eNS_URI);

		// Obtain or create and register package
		BaselinePackageImpl theBaselinePackage = (BaselinePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof BaselinePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new BaselinePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		AssuranceassetPackage.eINSTANCE.eClass();
		RefframeworkPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theBaselinePackage.createPackageContents();

		// Initialize created meta-data
		theBaselinePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theBaselinePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(BaselinePackage.eNS_URI, theBaselinePackage);
		return theBaselinePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseFramework() {
		return baseFrameworkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseFramework_Scope() {
		return (EAttribute)baseFrameworkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseFramework_Rev() {
		return (EAttribute)baseFrameworkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseFramework_Purpose() {
		return (EAttribute)baseFrameworkEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseFramework_Publisher() {
		return (EAttribute)baseFrameworkEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseFramework_Issued() {
		return (EAttribute)baseFrameworkEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseFramework_OwnedActivities() {
		return (EReference)baseFrameworkEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseFramework_OwnedArtefact() {
		return (EReference)baseFrameworkEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseFramework_OwnedRequirement() {
		return (EReference)baseFrameworkEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseFramework_OwnedApplicLevel() {
		return (EReference)baseFrameworkEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseFramework_OwnedCriticLevel() {
		return (EReference)baseFrameworkEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseFramework_OwnedRole() {
		return (EReference)baseFrameworkEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseFramework_OwnedTechnique() {
		return (EReference)baseFrameworkEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseFramework_RefFramework() {
		return (EReference)baseFrameworkEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseRequirement() {
		return baseRequirementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseRequirement_Reference() {
		return (EAttribute)baseRequirementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseRequirement_Assumptions() {
		return (EAttribute)baseRequirementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseRequirement_Rationale() {
		return (EAttribute)baseRequirementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseRequirement_Image() {
		return (EAttribute)baseRequirementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseRequirement_Annotations() {
		return (EAttribute)baseRequirementEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseRequirement_OwnedRel() {
		return (EReference)baseRequirementEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseRequirement_Applicability() {
		return (EReference)baseRequirementEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseRequirement_SubRequirement() {
		return (EReference)baseRequirementEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseArtefact() {
		return baseArtefactEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseArtefact_Reference() {
		return (EAttribute)baseArtefactEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseArtefact_ConstrainingRequirement() {
		return (EReference)baseArtefactEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseArtefact_ApplicableTechnique() {
		return (EReference)baseArtefactEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseArtefact_OwnedRel() {
		return (EReference)baseArtefactEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseArtefact_Property() {
		return (EReference)baseArtefactEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseActivity() {
		return baseActivityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseActivity_Objective() {
		return (EAttribute)baseActivityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseActivity_Scope() {
		return (EAttribute)baseActivityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseActivity_RequiredArtefact() {
		return (EReference)baseActivityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseActivity_ProducedArtefact() {
		return (EReference)baseActivityEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseActivity_SubActivity() {
		return (EReference)baseActivityEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseActivity_PrecedingActivity() {
		return (EReference)baseActivityEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseActivity_OwnedRequirement() {
		return (EReference)baseActivityEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseActivity_Role() {
		return (EReference)baseActivityEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseActivity_ApplicableTechnique() {
		return (EReference)baseActivityEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseActivity_OwnedRel() {
		return (EReference)baseActivityEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseActivity_Applicability() {
		return (EReference)baseActivityEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseRequirementRel() {
		return baseRequirementRelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseRequirementRel_Target() {
		return (EReference)baseRequirementRelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseRequirementRel_Source() {
		return (EReference)baseRequirementRelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseRequirementRel_Type() {
		return (EAttribute)baseRequirementRelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseRole() {
		return baseRoleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseApplicabilityLevel() {
		return baseApplicabilityLevelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseCriticalityLevel() {
		return baseCriticalityLevelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseTechnique() {
		return baseTechniqueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseTechnique_CriticApplic() {
		return (EReference)baseTechniqueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseTechnique_Aim() {
		return (EAttribute)baseTechniqueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseArtefactRel() {
		return baseArtefactRelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseArtefactRel_MaxMultiplicitySource() {
		return (EAttribute)baseArtefactRelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseArtefactRel_MinMultiplicitySource() {
		return (EAttribute)baseArtefactRelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseArtefactRel_MaxMultiplicityTarget() {
		return (EAttribute)baseArtefactRelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseArtefactRel_MinMultiplicityTarget() {
		return (EAttribute)baseArtefactRelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseArtefactRel_ModificationEffect() {
		return (EAttribute)baseArtefactRelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseArtefactRel_RevocationEffect() {
		return (EAttribute)baseArtefactRelEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseArtefactRel_Source() {
		return (EReference)baseArtefactRelEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseArtefactRel_Target() {
		return (EReference)baseArtefactRelEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseCriticalityApplicability() {
		return baseCriticalityApplicabilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseCriticalityApplicability_ApplicLevel() {
		return (EReference)baseCriticalityApplicabilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseCriticalityApplicability_CriticLevel() {
		return (EReference)baseCriticalityApplicabilityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseCriticalityApplicability_Comment() {
		return (EAttribute)baseCriticalityApplicabilityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseActivityRel() {
		return baseActivityRelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseActivityRel_Type() {
		return (EAttribute)baseActivityRelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseActivityRel_Source() {
		return (EReference)baseActivityRelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseActivityRel_Target() {
		return (EReference)baseActivityRelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseAssurableElement() {
		return baseAssurableElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseAssurableElement_EquivalenceMap() {
		return (EReference)baseAssurableElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseAssurableElement_ComplianceMap() {
		return (EReference)baseAssurableElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseAssurableElement_RefAssurableElement() {
		return (EReference)baseAssurableElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseIndependencyLevel() {
		return baseIndependencyLevelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseRecommendationLevel() {
		return baseRecommendationLevelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseControlCategory() {
		return baseControlCategoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseApplicability() {
		return baseApplicabilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseApplicability_ApplicCritic() {
		return (EReference)baseApplicabilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseApplicability_Comments() {
		return (EAttribute)baseApplicabilityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseApplicability_ApplicTarget() {
		return (EReference)baseApplicabilityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseApplicability_OwnedRel() {
		return (EReference)baseApplicabilityEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseApplicabilityRel() {
		return baseApplicabilityRelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseApplicabilityRel_Type() {
		return (EAttribute)baseApplicabilityRelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseApplicabilityRel_Source() {
		return (EReference)baseApplicabilityRelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseApplicabilityRel_Target() {
		return (EReference)baseApplicabilityRelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaselineElement() {
		return baselineElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaselineElement_IsSelected() {
		return (EAttribute)baselineElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaselineElement_SelectionJustification() {
		return (EAttribute)baselineElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseEquivalenceMap() {
		return baseEquivalenceMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseEquivalenceMap_Target() {
		return (EReference)baseEquivalenceMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseComplianceMap() {
		return baseComplianceMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseComplianceMap_Target() {
		return (EReference)baseComplianceMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaselineFactory getBaselineFactory() {
		return (BaselineFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		baseFrameworkEClass = createEClass(BASE_FRAMEWORK);
		createEAttribute(baseFrameworkEClass, BASE_FRAMEWORK__SCOPE);
		createEAttribute(baseFrameworkEClass, BASE_FRAMEWORK__REV);
		createEAttribute(baseFrameworkEClass, BASE_FRAMEWORK__PURPOSE);
		createEAttribute(baseFrameworkEClass, BASE_FRAMEWORK__PUBLISHER);
		createEAttribute(baseFrameworkEClass, BASE_FRAMEWORK__ISSUED);
		createEReference(baseFrameworkEClass, BASE_FRAMEWORK__OWNED_ACTIVITIES);
		createEReference(baseFrameworkEClass, BASE_FRAMEWORK__OWNED_ARTEFACT);
		createEReference(baseFrameworkEClass, BASE_FRAMEWORK__OWNED_REQUIREMENT);
		createEReference(baseFrameworkEClass, BASE_FRAMEWORK__OWNED_APPLIC_LEVEL);
		createEReference(baseFrameworkEClass, BASE_FRAMEWORK__OWNED_CRITIC_LEVEL);
		createEReference(baseFrameworkEClass, BASE_FRAMEWORK__OWNED_ROLE);
		createEReference(baseFrameworkEClass, BASE_FRAMEWORK__OWNED_TECHNIQUE);
		createEReference(baseFrameworkEClass, BASE_FRAMEWORK__REF_FRAMEWORK);

		baseRequirementEClass = createEClass(BASE_REQUIREMENT);
		createEAttribute(baseRequirementEClass, BASE_REQUIREMENT__REFERENCE);
		createEAttribute(baseRequirementEClass, BASE_REQUIREMENT__ASSUMPTIONS);
		createEAttribute(baseRequirementEClass, BASE_REQUIREMENT__RATIONALE);
		createEAttribute(baseRequirementEClass, BASE_REQUIREMENT__IMAGE);
		createEAttribute(baseRequirementEClass, BASE_REQUIREMENT__ANNOTATIONS);
		createEReference(baseRequirementEClass, BASE_REQUIREMENT__OWNED_REL);
		createEReference(baseRequirementEClass, BASE_REQUIREMENT__APPLICABILITY);
		createEReference(baseRequirementEClass, BASE_REQUIREMENT__SUB_REQUIREMENT);

		baseArtefactEClass = createEClass(BASE_ARTEFACT);
		createEAttribute(baseArtefactEClass, BASE_ARTEFACT__REFERENCE);
		createEReference(baseArtefactEClass, BASE_ARTEFACT__CONSTRAINING_REQUIREMENT);
		createEReference(baseArtefactEClass, BASE_ARTEFACT__APPLICABLE_TECHNIQUE);
		createEReference(baseArtefactEClass, BASE_ARTEFACT__OWNED_REL);
		createEReference(baseArtefactEClass, BASE_ARTEFACT__PROPERTY);

		baseActivityEClass = createEClass(BASE_ACTIVITY);
		createEAttribute(baseActivityEClass, BASE_ACTIVITY__OBJECTIVE);
		createEAttribute(baseActivityEClass, BASE_ACTIVITY__SCOPE);
		createEReference(baseActivityEClass, BASE_ACTIVITY__REQUIRED_ARTEFACT);
		createEReference(baseActivityEClass, BASE_ACTIVITY__PRODUCED_ARTEFACT);
		createEReference(baseActivityEClass, BASE_ACTIVITY__SUB_ACTIVITY);
		createEReference(baseActivityEClass, BASE_ACTIVITY__PRECEDING_ACTIVITY);
		createEReference(baseActivityEClass, BASE_ACTIVITY__OWNED_REQUIREMENT);
		createEReference(baseActivityEClass, BASE_ACTIVITY__ROLE);
		createEReference(baseActivityEClass, BASE_ACTIVITY__APPLICABLE_TECHNIQUE);
		createEReference(baseActivityEClass, BASE_ACTIVITY__OWNED_REL);
		createEReference(baseActivityEClass, BASE_ACTIVITY__APPLICABILITY);

		baseRequirementRelEClass = createEClass(BASE_REQUIREMENT_REL);
		createEReference(baseRequirementRelEClass, BASE_REQUIREMENT_REL__TARGET);
		createEReference(baseRequirementRelEClass, BASE_REQUIREMENT_REL__SOURCE);
		createEAttribute(baseRequirementRelEClass, BASE_REQUIREMENT_REL__TYPE);

		baseRoleEClass = createEClass(BASE_ROLE);

		baseApplicabilityLevelEClass = createEClass(BASE_APPLICABILITY_LEVEL);

		baseCriticalityLevelEClass = createEClass(BASE_CRITICALITY_LEVEL);

		baseTechniqueEClass = createEClass(BASE_TECHNIQUE);
		createEReference(baseTechniqueEClass, BASE_TECHNIQUE__CRITIC_APPLIC);
		createEAttribute(baseTechniqueEClass, BASE_TECHNIQUE__AIM);

		baseArtefactRelEClass = createEClass(BASE_ARTEFACT_REL);
		createEAttribute(baseArtefactRelEClass, BASE_ARTEFACT_REL__MAX_MULTIPLICITY_SOURCE);
		createEAttribute(baseArtefactRelEClass, BASE_ARTEFACT_REL__MIN_MULTIPLICITY_SOURCE);
		createEAttribute(baseArtefactRelEClass, BASE_ARTEFACT_REL__MAX_MULTIPLICITY_TARGET);
		createEAttribute(baseArtefactRelEClass, BASE_ARTEFACT_REL__MIN_MULTIPLICITY_TARGET);
		createEAttribute(baseArtefactRelEClass, BASE_ARTEFACT_REL__MODIFICATION_EFFECT);
		createEAttribute(baseArtefactRelEClass, BASE_ARTEFACT_REL__REVOCATION_EFFECT);
		createEReference(baseArtefactRelEClass, BASE_ARTEFACT_REL__SOURCE);
		createEReference(baseArtefactRelEClass, BASE_ARTEFACT_REL__TARGET);

		baseCriticalityApplicabilityEClass = createEClass(BASE_CRITICALITY_APPLICABILITY);
		createEReference(baseCriticalityApplicabilityEClass, BASE_CRITICALITY_APPLICABILITY__APPLIC_LEVEL);
		createEReference(baseCriticalityApplicabilityEClass, BASE_CRITICALITY_APPLICABILITY__CRITIC_LEVEL);
		createEAttribute(baseCriticalityApplicabilityEClass, BASE_CRITICALITY_APPLICABILITY__COMMENT);

		baseActivityRelEClass = createEClass(BASE_ACTIVITY_REL);
		createEAttribute(baseActivityRelEClass, BASE_ACTIVITY_REL__TYPE);
		createEReference(baseActivityRelEClass, BASE_ACTIVITY_REL__SOURCE);
		createEReference(baseActivityRelEClass, BASE_ACTIVITY_REL__TARGET);

		baseAssurableElementEClass = createEClass(BASE_ASSURABLE_ELEMENT);
		createEReference(baseAssurableElementEClass, BASE_ASSURABLE_ELEMENT__EQUIVALENCE_MAP);
		createEReference(baseAssurableElementEClass, BASE_ASSURABLE_ELEMENT__COMPLIANCE_MAP);
		createEReference(baseAssurableElementEClass, BASE_ASSURABLE_ELEMENT__REF_ASSURABLE_ELEMENT);

		baseIndependencyLevelEClass = createEClass(BASE_INDEPENDENCY_LEVEL);

		baseRecommendationLevelEClass = createEClass(BASE_RECOMMENDATION_LEVEL);

		baseControlCategoryEClass = createEClass(BASE_CONTROL_CATEGORY);

		baseApplicabilityEClass = createEClass(BASE_APPLICABILITY);
		createEReference(baseApplicabilityEClass, BASE_APPLICABILITY__APPLIC_CRITIC);
		createEAttribute(baseApplicabilityEClass, BASE_APPLICABILITY__COMMENTS);
		createEReference(baseApplicabilityEClass, BASE_APPLICABILITY__APPLIC_TARGET);
		createEReference(baseApplicabilityEClass, BASE_APPLICABILITY__OWNED_REL);

		baseApplicabilityRelEClass = createEClass(BASE_APPLICABILITY_REL);
		createEAttribute(baseApplicabilityRelEClass, BASE_APPLICABILITY_REL__TYPE);
		createEReference(baseApplicabilityRelEClass, BASE_APPLICABILITY_REL__SOURCE);
		createEReference(baseApplicabilityRelEClass, BASE_APPLICABILITY_REL__TARGET);

		baselineElementEClass = createEClass(BASELINE_ELEMENT);
		createEAttribute(baselineElementEClass, BASELINE_ELEMENT__IS_SELECTED);
		createEAttribute(baselineElementEClass, BASELINE_ELEMENT__SELECTION_JUSTIFICATION);

		baseEquivalenceMapEClass = createEClass(BASE_EQUIVALENCE_MAP);
		createEReference(baseEquivalenceMapEClass, BASE_EQUIVALENCE_MAP__TARGET);

		baseComplianceMapEClass = createEClass(BASE_COMPLIANCE_MAP);
		createEReference(baseComplianceMapEClass, BASE_COMPLIANCE_MAP__TARGET);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		GeneralPackage theGeneralPackage = (GeneralPackage)EPackage.Registry.INSTANCE.getEPackage(GeneralPackage.eNS_URI);
		RefframeworkPackage theRefframeworkPackage = (RefframeworkPackage)EPackage.Registry.INSTANCE.getEPackage(RefframeworkPackage.eNS_URI);
		PropertyPackage thePropertyPackage = (PropertyPackage)EPackage.Registry.INSTANCE.getEPackage(PropertyPackage.eNS_URI);
		MappingPackage theMappingPackage = (MappingPackage)EPackage.Registry.INSTANCE.getEPackage(MappingPackage.eNS_URI);
		AssuranceassetPackage theAssuranceassetPackage = (AssuranceassetPackage)EPackage.Registry.INSTANCE.getEPackage(AssuranceassetPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		baseFrameworkEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		baseRequirementEClass.getESuperTypes().add(this.getBaseAssurableElement());
		baseRequirementEClass.getESuperTypes().add(this.getBaselineElement());
		baseRequirementEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		baseArtefactEClass.getESuperTypes().add(this.getBaseAssurableElement());
		baseArtefactEClass.getESuperTypes().add(this.getBaselineElement());
		baseArtefactEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		baseActivityEClass.getESuperTypes().add(this.getBaseAssurableElement());
		baseActivityEClass.getESuperTypes().add(this.getBaselineElement());
		baseActivityEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		baseRoleEClass.getESuperTypes().add(this.getBaseAssurableElement());
		baseRoleEClass.getESuperTypes().add(this.getBaselineElement());
		baseRoleEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		baseApplicabilityLevelEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		baseCriticalityLevelEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		baseTechniqueEClass.getESuperTypes().add(this.getBaseAssurableElement());
		baseTechniqueEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		baseTechniqueEClass.getESuperTypes().add(this.getBaselineElement());
		baseArtefactRelEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		baseIndependencyLevelEClass.getESuperTypes().add(this.getBaseApplicabilityLevel());
		baseRecommendationLevelEClass.getESuperTypes().add(this.getBaseApplicabilityLevel());
		baseControlCategoryEClass.getESuperTypes().add(this.getBaseApplicabilityLevel());
		baseApplicabilityEClass.getESuperTypes().add(theGeneralPackage.getNamedElement());
		baseEquivalenceMapEClass.getESuperTypes().add(theMappingPackage.getEquivalenceMap());
		baseComplianceMapEClass.getESuperTypes().add(theMappingPackage.getComplianceMap());

		// Initialize classes, features, and operations; add parameters
		initEClass(baseFrameworkEClass, BaseFramework.class, "BaseFramework", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBaseFramework_Scope(), ecorePackage.getEString(), "scope", null, 0, 1, BaseFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBaseFramework_Rev(), ecorePackage.getEString(), "rev", null, 0, 1, BaseFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBaseFramework_Purpose(), ecorePackage.getEString(), "purpose", null, 0, 1, BaseFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBaseFramework_Publisher(), ecorePackage.getEString(), "publisher", null, 0, 1, BaseFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBaseFramework_Issued(), ecorePackage.getEDate(), "issued", null, 0, 1, BaseFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseFramework_OwnedActivities(), this.getBaseActivity(), null, "ownedActivities", null, 0, -1, BaseFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseFramework_OwnedArtefact(), this.getBaseArtefact(), null, "ownedArtefact", null, 0, -1, BaseFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseFramework_OwnedRequirement(), this.getBaseRequirement(), null, "ownedRequirement", null, 0, -1, BaseFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseFramework_OwnedApplicLevel(), this.getBaseApplicabilityLevel(), null, "ownedApplicLevel", null, 0, -1, BaseFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseFramework_OwnedCriticLevel(), this.getBaseCriticalityLevel(), null, "ownedCriticLevel", null, 0, -1, BaseFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseFramework_OwnedRole(), this.getBaseRole(), null, "ownedRole", null, 0, -1, BaseFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseFramework_OwnedTechnique(), this.getBaseTechnique(), null, "ownedTechnique", null, 0, -1, BaseFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseFramework_RefFramework(), theRefframeworkPackage.getRefFramework(), null, "refFramework", null, 0, 1, BaseFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(baseRequirementEClass, BaseRequirement.class, "BaseRequirement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBaseRequirement_Reference(), ecorePackage.getEString(), "reference", null, 0, 1, BaseRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBaseRequirement_Assumptions(), ecorePackage.getEString(), "assumptions", null, 0, 1, BaseRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBaseRequirement_Rationale(), ecorePackage.getEString(), "rationale", null, 0, 1, BaseRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBaseRequirement_Image(), ecorePackage.getEString(), "image", null, 0, 1, BaseRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBaseRequirement_Annotations(), ecorePackage.getEString(), "annotations", null, 0, 1, BaseRequirement.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseRequirement_OwnedRel(), this.getBaseRequirementRel(), null, "ownedRel", null, 0, -1, BaseRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseRequirement_Applicability(), this.getBaseApplicability(), null, "applicability", null, 0, -1, BaseRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseRequirement_SubRequirement(), this.getBaseRequirement(), null, "subRequirement", null, 0, -1, BaseRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(baseArtefactEClass, BaseArtefact.class, "BaseArtefact", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBaseArtefact_Reference(), ecorePackage.getEString(), "reference", null, 0, 1, BaseArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseArtefact_ConstrainingRequirement(), this.getBaseRequirement(), null, "constrainingRequirement", null, 0, -1, BaseArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseArtefact_ApplicableTechnique(), this.getBaseTechnique(), null, "applicableTechnique", null, 0, -1, BaseArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseArtefact_OwnedRel(), this.getBaseArtefactRel(), null, "ownedRel", null, 0, -1, BaseArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseArtefact_Property(), thePropertyPackage.getProperty(), null, "property", null, 0, -1, BaseArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(baseActivityEClass, BaseActivity.class, "BaseActivity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBaseActivity_Objective(), ecorePackage.getEString(), "objective", null, 0, 1, BaseActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBaseActivity_Scope(), ecorePackage.getEString(), "scope", null, 0, 1, BaseActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseActivity_RequiredArtefact(), this.getBaseArtefact(), null, "requiredArtefact", null, 0, -1, BaseActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseActivity_ProducedArtefact(), this.getBaseArtefact(), null, "producedArtefact", null, 0, -1, BaseActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseActivity_SubActivity(), this.getBaseActivity(), null, "subActivity", null, 0, -1, BaseActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseActivity_PrecedingActivity(), this.getBaseActivity(), null, "precedingActivity", null, 0, -1, BaseActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseActivity_OwnedRequirement(), this.getBaseRequirement(), null, "ownedRequirement", null, 0, -1, BaseActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseActivity_Role(), this.getBaseRole(), null, "role", null, 0, -1, BaseActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseActivity_ApplicableTechnique(), this.getBaseTechnique(), null, "applicableTechnique", null, 0, 1, BaseActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseActivity_OwnedRel(), this.getBaseActivityRel(), null, "ownedRel", null, 0, -1, BaseActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseActivity_Applicability(), this.getBaseApplicability(), null, "applicability", null, 0, -1, BaseActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(baseRequirementRelEClass, BaseRequirementRel.class, "BaseRequirementRel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBaseRequirementRel_Target(), this.getBaseRequirement(), null, "target", null, 1, 1, BaseRequirementRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseRequirementRel_Source(), this.getBaseRequirement(), null, "source", null, 1, 1, BaseRequirementRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBaseRequirementRel_Type(), theGeneralPackage.getRequirementRelKind(), "type", null, 0, 1, BaseRequirementRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(baseRoleEClass, BaseRole.class, "BaseRole", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(baseApplicabilityLevelEClass, BaseApplicabilityLevel.class, "BaseApplicabilityLevel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(baseCriticalityLevelEClass, BaseCriticalityLevel.class, "BaseCriticalityLevel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(baseTechniqueEClass, BaseTechnique.class, "BaseTechnique", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBaseTechnique_CriticApplic(), this.getBaseCriticalityApplicability(), null, "criticApplic", null, 0, -1, BaseTechnique.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBaseTechnique_Aim(), ecorePackage.getEString(), "aim", null, 0, 1, BaseTechnique.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(baseArtefactRelEClass, BaseArtefactRel.class, "BaseArtefactRel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBaseArtefactRel_MaxMultiplicitySource(), ecorePackage.getEInt(), "maxMultiplicitySource", null, 0, 1, BaseArtefactRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBaseArtefactRel_MinMultiplicitySource(), ecorePackage.getEInt(), "minMultiplicitySource", null, 0, 1, BaseArtefactRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBaseArtefactRel_MaxMultiplicityTarget(), ecorePackage.getEInt(), "maxMultiplicityTarget", null, 0, 1, BaseArtefactRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBaseArtefactRel_MinMultiplicityTarget(), ecorePackage.getEInt(), "minMultiplicityTarget", null, 0, 1, BaseArtefactRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBaseArtefactRel_ModificationEffect(), theGeneralPackage.getChangeEffectKind(), "modificationEffect", null, 0, 1, BaseArtefactRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBaseArtefactRel_RevocationEffect(), theGeneralPackage.getChangeEffectKind(), "revocationEffect", null, 0, 1, BaseArtefactRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseArtefactRel_Source(), this.getBaseArtefact(), null, "source", null, 1, 1, BaseArtefactRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseArtefactRel_Target(), this.getBaseArtefact(), null, "target", null, 1, 1, BaseArtefactRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(baseCriticalityApplicabilityEClass, BaseCriticalityApplicability.class, "BaseCriticalityApplicability", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBaseCriticalityApplicability_ApplicLevel(), this.getBaseApplicabilityLevel(), null, "applicLevel", null, 1, 1, BaseCriticalityApplicability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseCriticalityApplicability_CriticLevel(), this.getBaseCriticalityLevel(), null, "criticLevel", null, 1, 1, BaseCriticalityApplicability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBaseCriticalityApplicability_Comment(), ecorePackage.getEString(), "comment", null, 0, 1, BaseCriticalityApplicability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(baseActivityRelEClass, BaseActivityRel.class, "BaseActivityRel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBaseActivityRel_Type(), theGeneralPackage.getActivityRelKind(), "type", null, 0, 1, BaseActivityRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseActivityRel_Source(), this.getBaseActivity(), null, "source", null, 1, 1, BaseActivityRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseActivityRel_Target(), this.getBaseActivity(), null, "target", null, 1, 1, BaseActivityRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(baseAssurableElementEClass, BaseAssurableElement.class, "BaseAssurableElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBaseAssurableElement_EquivalenceMap(), this.getBaseEquivalenceMap(), null, "equivalenceMap", null, 0, -1, BaseAssurableElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseAssurableElement_ComplianceMap(), this.getBaseComplianceMap(), null, "complianceMap", null, 0, -1, BaseAssurableElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseAssurableElement_RefAssurableElement(), theRefframeworkPackage.getRefAssurableElement(), null, "refAssurableElement", null, 0, 1, BaseAssurableElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(baseIndependencyLevelEClass, BaseIndependencyLevel.class, "BaseIndependencyLevel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(baseRecommendationLevelEClass, BaseRecommendationLevel.class, "BaseRecommendationLevel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(baseControlCategoryEClass, BaseControlCategory.class, "BaseControlCategory", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(baseApplicabilityEClass, BaseApplicability.class, "BaseApplicability", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBaseApplicability_ApplicCritic(), this.getBaseCriticalityApplicability(), null, "applicCritic", null, 0, -1, BaseApplicability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBaseApplicability_Comments(), ecorePackage.getEString(), "comments", null, 0, 1, BaseApplicability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseApplicability_ApplicTarget(), this.getBaseAssurableElement(), null, "applicTarget", null, 0, 1, BaseApplicability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseApplicability_OwnedRel(), this.getBaseApplicabilityRel(), null, "ownedRel", null, 0, -1, BaseApplicability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(baseApplicabilityRelEClass, BaseApplicabilityRel.class, "BaseApplicabilityRel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBaseApplicabilityRel_Type(), theGeneralPackage.getApplicabilityKind(), "type", null, 0, 1, BaseApplicabilityRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseApplicabilityRel_Source(), this.getBaseApplicability(), null, "source", null, 1, 1, BaseApplicabilityRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaseApplicabilityRel_Target(), this.getBaseApplicability(), null, "target", null, 1, 1, BaseApplicabilityRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(baselineElementEClass, BaselineElement.class, "BaselineElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBaselineElement_IsSelected(), ecorePackage.getEBoolean(), "isSelected", null, 0, 1, BaselineElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBaselineElement_SelectionJustification(), ecorePackage.getEString(), "selectionJustification", null, 0, 1, BaselineElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(baseEquivalenceMapEClass, BaseEquivalenceMap.class, "BaseEquivalenceMap", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBaseEquivalenceMap_Target(), theRefframeworkPackage.getRefAssurableElement(), null, "target", null, 0, -1, BaseEquivalenceMap.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(baseComplianceMapEClass, BaseComplianceMap.class, "BaseComplianceMap", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBaseComplianceMap_Target(), theAssuranceassetPackage.getAssuranceAsset(), null, "target", null, 0, -1, BaseComplianceMap.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// gmf
		createGmfAnnotations();
		// gmf.diagram
		createGmf_1Annotations();
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
		// gmf.node
		createGmf_2Annotations();
		// gmf.link
		createGmf_3Annotations();
		// gmf.compartment
		createGmf_4Annotations();
	}

	/**
	 * Initializes the annotations for <b>gmf</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGmfAnnotations() {
		String source = "gmf";		
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
		   });										
	}

	/**
	 * Initializes the annotations for <b>gmf.diagram</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGmf_1Annotations() {
		String source = "gmf.diagram";			
		addAnnotation
		  (baseFrameworkEClass, 
		   source, 
		   new String[] {
		   });									
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";				
		addAnnotation
		  (getBaseRequirement_Annotations(), 
		   source, 
		   new String[] {
		   });								
	}

	/**
	 * Initializes the annotations for <b>gmf.node</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGmf_2Annotations() {
		String source = "gmf.node";					
		addAnnotation
		  (baseArtefactEClass, 
		   source, 
		   new String[] {
			 "label", "name",
			 "figure", "figures.ArtefactFigure",
			 "label.icon", "false",
			 "label.placement", "external"
		   });		
		addAnnotation
		  (baseActivityEClass, 
		   source, 
		   new String[] {
			 "label", "name",
			 "border.color", "0,0,0",
			 "border.width", "2"
		   });							
		addAnnotation
		  (baseRoleEClass, 
		   source, 
		   new String[] {
			 "label", "name",
			 "figure", "figures.RefRoleFigure",
			 "label.icon", "false",
			 "label.placement", "external"
		   });
	}

	/**
	 * Initializes the annotations for <b>gmf.link</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGmf_3Annotations() {
		String source = "gmf.link";							
		addAnnotation
		  (getBaseActivity_RequiredArtefact(), 
		   source, 
		   new String[] {
			 "color", "255,0,0",
			 "source.decoration", "closedarrow",
			 "style", "dash",
			 "tool.small.bundle", "org.eclipse.opencert.pkm.refframework",
			 "tool.small.path", "icons/Require.gif"
		   });		
		addAnnotation
		  (getBaseActivity_ProducedArtefact(), 
		   source, 
		   new String[] {
			 "color", "0,255,0",
			 "target.decoration", "filledclosedarrow",
			 "style", "solid",
			 "tool.small.bundle", "org.eclipse.opencert.pkm.refframework",
			 "tool.small.path", "icons/Produce.gif"
		   });			
		addAnnotation
		  (getBaseActivity_PrecedingActivity(), 
		   source, 
		   new String[] {
			 "source.decoration", "arrow",
			 "tool.small.bundle", "org.eclipse.opencert.pkm.refframework",
			 "tool.small.path", "icons/Precedence.gif"
		   });		
		addAnnotation
		  (getBaseActivity_Role(), 
		   source, 
		   new String[] {
			 "color", "0,0,255",
			 "source.decoration", "closedarrow",
			 "style", "dash",
			 "tool.small.bundle", "org.eclipse.opencert.pkm.refframework",
			 "tool.small.path", "icons/Executing.gif"
		   });	
	}

	/**
	 * Initializes the annotations for <b>gmf.compartment</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGmf_4Annotations() {
		String source = "gmf.compartment";									
		addAnnotation
		  (getBaseActivity_SubActivity(), 
		   source, 
		   new String[] {
		   });			
	}

} //BaselinePackageImpl
