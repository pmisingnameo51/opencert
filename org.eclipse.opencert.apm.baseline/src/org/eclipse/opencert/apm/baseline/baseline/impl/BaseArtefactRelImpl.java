/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.baseline.baseline.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.opencert.infra.general.general.ChangeEffectKind;
import org.eclipse.opencert.infra.general.general.impl.DescribableElementImpl;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Base Artefact Rel</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseArtefactRelImpl#getMaxMultiplicitySource <em>Max Multiplicity Source</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseArtefactRelImpl#getMinMultiplicitySource <em>Min Multiplicity Source</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseArtefactRelImpl#getMaxMultiplicityTarget <em>Max Multiplicity Target</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseArtefactRelImpl#getMinMultiplicityTarget <em>Min Multiplicity Target</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseArtefactRelImpl#getModificationEffect <em>Modification Effect</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseArtefactRelImpl#getRevocationEffect <em>Revocation Effect</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseArtefactRelImpl#getSource <em>Source</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseArtefactRelImpl#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BaseArtefactRelImpl extends DescribableElementImpl implements BaseArtefactRel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BaseArtefactRelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BaselinePackage.Literals.BASE_ARTEFACT_REL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMaxMultiplicitySource() {
		return (Integer)eGet(BaselinePackage.Literals.BASE_ARTEFACT_REL__MAX_MULTIPLICITY_SOURCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxMultiplicitySource(int newMaxMultiplicitySource) {
		eSet(BaselinePackage.Literals.BASE_ARTEFACT_REL__MAX_MULTIPLICITY_SOURCE, newMaxMultiplicitySource);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMinMultiplicitySource() {
		return (Integer)eGet(BaselinePackage.Literals.BASE_ARTEFACT_REL__MIN_MULTIPLICITY_SOURCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinMultiplicitySource(int newMinMultiplicitySource) {
		eSet(BaselinePackage.Literals.BASE_ARTEFACT_REL__MIN_MULTIPLICITY_SOURCE, newMinMultiplicitySource);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMaxMultiplicityTarget() {
		return (Integer)eGet(BaselinePackage.Literals.BASE_ARTEFACT_REL__MAX_MULTIPLICITY_TARGET, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxMultiplicityTarget(int newMaxMultiplicityTarget) {
		eSet(BaselinePackage.Literals.BASE_ARTEFACT_REL__MAX_MULTIPLICITY_TARGET, newMaxMultiplicityTarget);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMinMultiplicityTarget() {
		return (Integer)eGet(BaselinePackage.Literals.BASE_ARTEFACT_REL__MIN_MULTIPLICITY_TARGET, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinMultiplicityTarget(int newMinMultiplicityTarget) {
		eSet(BaselinePackage.Literals.BASE_ARTEFACT_REL__MIN_MULTIPLICITY_TARGET, newMinMultiplicityTarget);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeEffectKind getModificationEffect() {
		return (ChangeEffectKind)eGet(BaselinePackage.Literals.BASE_ARTEFACT_REL__MODIFICATION_EFFECT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModificationEffect(ChangeEffectKind newModificationEffect) {
		eSet(BaselinePackage.Literals.BASE_ARTEFACT_REL__MODIFICATION_EFFECT, newModificationEffect);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeEffectKind getRevocationEffect() {
		return (ChangeEffectKind)eGet(BaselinePackage.Literals.BASE_ARTEFACT_REL__REVOCATION_EFFECT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRevocationEffect(ChangeEffectKind newRevocationEffect) {
		eSet(BaselinePackage.Literals.BASE_ARTEFACT_REL__REVOCATION_EFFECT, newRevocationEffect);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseArtefact getSource() {
		return (BaseArtefact)eGet(BaselinePackage.Literals.BASE_ARTEFACT_REL__SOURCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource(BaseArtefact newSource) {
		eSet(BaselinePackage.Literals.BASE_ARTEFACT_REL__SOURCE, newSource);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseArtefact getTarget() {
		return (BaseArtefact)eGet(BaselinePackage.Literals.BASE_ARTEFACT_REL__TARGET, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTarget(BaseArtefact newTarget) {
		eSet(BaselinePackage.Literals.BASE_ARTEFACT_REL__TARGET, newTarget);
	}

} //BaseArtefactRelImpl
