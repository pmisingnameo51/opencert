/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.baseline.baseline;

import org.eclipse.emf.cdo.CDOObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaselineElement#isIsSelected <em>Is Selected</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaselineElement#getSelectionJustification <em>Selection Justification</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaselineElement()
 * @model abstract="true"
 * @extends CDOObject
 * @generated
 */
public interface BaselineElement extends CDOObject {
	/**
	 * Returns the value of the '<em><b>Is Selected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Selected</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Selected</em>' attribute.
	 * @see #setIsSelected(boolean)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaselineElement_IsSelected()
	 * @model
	 * @generated
	 */
	boolean isIsSelected();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaselineElement#isIsSelected <em>Is Selected</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Selected</em>' attribute.
	 * @see #isIsSelected()
	 * @generated
	 */
	void setIsSelected(boolean value);

	/**
	 * Returns the value of the '<em><b>Selection Justification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selection Justification</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selection Justification</em>' attribute.
	 * @see #setSelectionJustification(String)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaselineElement_SelectionJustification()
	 * @model
	 * @generated
	 */
	String getSelectionJustification();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaselineElement#getSelectionJustification <em>Selection Justification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selection Justification</em>' attribute.
	 * @see #getSelectionJustification()
	 * @generated
	 */
	void setSelectionJustification(String value);

} // BaselineElement
