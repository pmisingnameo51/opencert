/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.baseline.baseline.impl;

import java.util.Date;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.opencert.infra.general.general.impl.DescribableElementImpl;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityLevel;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityLevel;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;
import org.eclipse.opencert.apm.baseline.baseline.BaseRole;
import org.eclipse.opencert.apm.baseline.baseline.BaseTechnique;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.pkm.refframework.refframework.RefFramework;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Base Framework</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseFrameworkImpl#getScope <em>Scope</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseFrameworkImpl#getRev <em>Rev</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseFrameworkImpl#getPurpose <em>Purpose</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseFrameworkImpl#getPublisher <em>Publisher</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseFrameworkImpl#getIssued <em>Issued</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseFrameworkImpl#getOwnedActivities <em>Owned Activities</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseFrameworkImpl#getOwnedArtefact <em>Owned Artefact</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseFrameworkImpl#getOwnedRequirement <em>Owned Requirement</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseFrameworkImpl#getOwnedApplicLevel <em>Owned Applic Level</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseFrameworkImpl#getOwnedCriticLevel <em>Owned Critic Level</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseFrameworkImpl#getOwnedRole <em>Owned Role</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseFrameworkImpl#getOwnedTechnique <em>Owned Technique</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.impl.BaseFrameworkImpl#getRefFramework <em>Ref Framework</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BaseFrameworkImpl extends DescribableElementImpl implements BaseFramework {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BaseFrameworkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BaselinePackage.Literals.BASE_FRAMEWORK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getScope() {
		return (String)eGet(BaselinePackage.Literals.BASE_FRAMEWORK__SCOPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScope(String newScope) {
		eSet(BaselinePackage.Literals.BASE_FRAMEWORK__SCOPE, newScope);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRev() {
		return (String)eGet(BaselinePackage.Literals.BASE_FRAMEWORK__REV, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRev(String newRev) {
		eSet(BaselinePackage.Literals.BASE_FRAMEWORK__REV, newRev);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPurpose() {
		return (String)eGet(BaselinePackage.Literals.BASE_FRAMEWORK__PURPOSE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPurpose(String newPurpose) {
		eSet(BaselinePackage.Literals.BASE_FRAMEWORK__PURPOSE, newPurpose);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPublisher() {
		return (String)eGet(BaselinePackage.Literals.BASE_FRAMEWORK__PUBLISHER, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPublisher(String newPublisher) {
		eSet(BaselinePackage.Literals.BASE_FRAMEWORK__PUBLISHER, newPublisher);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getIssued() {
		return (Date)eGet(BaselinePackage.Literals.BASE_FRAMEWORK__ISSUED, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIssued(Date newIssued) {
		eSet(BaselinePackage.Literals.BASE_FRAMEWORK__ISSUED, newIssued);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<BaseActivity> getOwnedActivities() {
		return (EList<BaseActivity>)eGet(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_ACTIVITIES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<BaseArtefact> getOwnedArtefact() {
		return (EList<BaseArtefact>)eGet(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_ARTEFACT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<BaseRequirement> getOwnedRequirement() {
		return (EList<BaseRequirement>)eGet(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_REQUIREMENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<BaseApplicabilityLevel> getOwnedApplicLevel() {
		return (EList<BaseApplicabilityLevel>)eGet(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_APPLIC_LEVEL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<BaseCriticalityLevel> getOwnedCriticLevel() {
		return (EList<BaseCriticalityLevel>)eGet(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_CRITIC_LEVEL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<BaseRole> getOwnedRole() {
		return (EList<BaseRole>)eGet(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_ROLE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<BaseTechnique> getOwnedTechnique() {
		return (EList<BaseTechnique>)eGet(BaselinePackage.Literals.BASE_FRAMEWORK__OWNED_TECHNIQUE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefFramework getRefFramework() {
		return (RefFramework)eGet(BaselinePackage.Literals.BASE_FRAMEWORK__REF_FRAMEWORK, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRefFramework(RefFramework newRefFramework) {
		eSet(BaselinePackage.Literals.BASE_FRAMEWORK__REF_FRAMEWORK, newRefFramework);
	}

} //BaseFrameworkImpl
