/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.baseline.baseline.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import org.eclipse.opencert.infra.general.general.DescribableElement;
import org.eclipse.opencert.infra.general.general.NamedElement;
import org.eclipse.opencert.apm.baseline.baseline.*;
import org.eclipse.opencert.infra.mappings.mapping.ComplianceMap;
import org.eclipse.opencert.infra.mappings.mapping.EquivalenceMap;
import org.eclipse.opencert.infra.mappings.mapping.Map;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage
 * @generated
 */
public class BaselineSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static BaselinePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaselineSwitch() {
		if (modelPackage == null) {
			modelPackage = BaselinePackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case BaselinePackage.BASE_FRAMEWORK: {
				BaseFramework baseFramework = (BaseFramework)theEObject;
				T result = caseBaseFramework(baseFramework);
				if (result == null) result = caseDescribableElement(baseFramework);
				if (result == null) result = caseNamedElement(baseFramework);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BaselinePackage.BASE_REQUIREMENT: {
				BaseRequirement baseRequirement = (BaseRequirement)theEObject;
				T result = caseBaseRequirement(baseRequirement);
				if (result == null) result = caseBaseAssurableElement(baseRequirement);
				if (result == null) result = caseBaselineElement(baseRequirement);
				if (result == null) result = caseDescribableElement(baseRequirement);
				if (result == null) result = caseNamedElement(baseRequirement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BaselinePackage.BASE_ARTEFACT: {
				BaseArtefact baseArtefact = (BaseArtefact)theEObject;
				T result = caseBaseArtefact(baseArtefact);
				if (result == null) result = caseBaseAssurableElement(baseArtefact);
				if (result == null) result = caseBaselineElement(baseArtefact);
				if (result == null) result = caseDescribableElement(baseArtefact);
				if (result == null) result = caseNamedElement(baseArtefact);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BaselinePackage.BASE_ACTIVITY: {
				BaseActivity baseActivity = (BaseActivity)theEObject;
				T result = caseBaseActivity(baseActivity);
				if (result == null) result = caseBaseAssurableElement(baseActivity);
				if (result == null) result = caseBaselineElement(baseActivity);
				if (result == null) result = caseDescribableElement(baseActivity);
				if (result == null) result = caseNamedElement(baseActivity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BaselinePackage.BASE_REQUIREMENT_REL: {
				BaseRequirementRel baseRequirementRel = (BaseRequirementRel)theEObject;
				T result = caseBaseRequirementRel(baseRequirementRel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BaselinePackage.BASE_ROLE: {
				BaseRole baseRole = (BaseRole)theEObject;
				T result = caseBaseRole(baseRole);
				if (result == null) result = caseBaseAssurableElement(baseRole);
				if (result == null) result = caseBaselineElement(baseRole);
				if (result == null) result = caseDescribableElement(baseRole);
				if (result == null) result = caseNamedElement(baseRole);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BaselinePackage.BASE_APPLICABILITY_LEVEL: {
				BaseApplicabilityLevel baseApplicabilityLevel = (BaseApplicabilityLevel)theEObject;
				T result = caseBaseApplicabilityLevel(baseApplicabilityLevel);
				if (result == null) result = caseDescribableElement(baseApplicabilityLevel);
				if (result == null) result = caseNamedElement(baseApplicabilityLevel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BaselinePackage.BASE_CRITICALITY_LEVEL: {
				BaseCriticalityLevel baseCriticalityLevel = (BaseCriticalityLevel)theEObject;
				T result = caseBaseCriticalityLevel(baseCriticalityLevel);
				if (result == null) result = caseDescribableElement(baseCriticalityLevel);
				if (result == null) result = caseNamedElement(baseCriticalityLevel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BaselinePackage.BASE_TECHNIQUE: {
				BaseTechnique baseTechnique = (BaseTechnique)theEObject;
				T result = caseBaseTechnique(baseTechnique);
				if (result == null) result = caseBaseAssurableElement(baseTechnique);
				if (result == null) result = caseDescribableElement(baseTechnique);
				if (result == null) result = caseBaselineElement(baseTechnique);
				if (result == null) result = caseNamedElement(baseTechnique);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BaselinePackage.BASE_ARTEFACT_REL: {
				BaseArtefactRel baseArtefactRel = (BaseArtefactRel)theEObject;
				T result = caseBaseArtefactRel(baseArtefactRel);
				if (result == null) result = caseDescribableElement(baseArtefactRel);
				if (result == null) result = caseNamedElement(baseArtefactRel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BaselinePackage.BASE_CRITICALITY_APPLICABILITY: {
				BaseCriticalityApplicability baseCriticalityApplicability = (BaseCriticalityApplicability)theEObject;
				T result = caseBaseCriticalityApplicability(baseCriticalityApplicability);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BaselinePackage.BASE_ACTIVITY_REL: {
				BaseActivityRel baseActivityRel = (BaseActivityRel)theEObject;
				T result = caseBaseActivityRel(baseActivityRel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BaselinePackage.BASE_ASSURABLE_ELEMENT: {
				BaseAssurableElement baseAssurableElement = (BaseAssurableElement)theEObject;
				T result = caseBaseAssurableElement(baseAssurableElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BaselinePackage.BASE_INDEPENDENCY_LEVEL: {
				BaseIndependencyLevel baseIndependencyLevel = (BaseIndependencyLevel)theEObject;
				T result = caseBaseIndependencyLevel(baseIndependencyLevel);
				if (result == null) result = caseBaseApplicabilityLevel(baseIndependencyLevel);
				if (result == null) result = caseDescribableElement(baseIndependencyLevel);
				if (result == null) result = caseNamedElement(baseIndependencyLevel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BaselinePackage.BASE_RECOMMENDATION_LEVEL: {
				BaseRecommendationLevel baseRecommendationLevel = (BaseRecommendationLevel)theEObject;
				T result = caseBaseRecommendationLevel(baseRecommendationLevel);
				if (result == null) result = caseBaseApplicabilityLevel(baseRecommendationLevel);
				if (result == null) result = caseDescribableElement(baseRecommendationLevel);
				if (result == null) result = caseNamedElement(baseRecommendationLevel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BaselinePackage.BASE_CONTROL_CATEGORY: {
				BaseControlCategory baseControlCategory = (BaseControlCategory)theEObject;
				T result = caseBaseControlCategory(baseControlCategory);
				if (result == null) result = caseBaseApplicabilityLevel(baseControlCategory);
				if (result == null) result = caseDescribableElement(baseControlCategory);
				if (result == null) result = caseNamedElement(baseControlCategory);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BaselinePackage.BASE_APPLICABILITY: {
				BaseApplicability baseApplicability = (BaseApplicability)theEObject;
				T result = caseBaseApplicability(baseApplicability);
				if (result == null) result = caseNamedElement(baseApplicability);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BaselinePackage.BASE_APPLICABILITY_REL: {
				BaseApplicabilityRel baseApplicabilityRel = (BaseApplicabilityRel)theEObject;
				T result = caseBaseApplicabilityRel(baseApplicabilityRel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BaselinePackage.BASELINE_ELEMENT: {
				BaselineElement baselineElement = (BaselineElement)theEObject;
				T result = caseBaselineElement(baselineElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BaselinePackage.BASE_EQUIVALENCE_MAP: {
				BaseEquivalenceMap baseEquivalenceMap = (BaseEquivalenceMap)theEObject;
				T result = caseBaseEquivalenceMap(baseEquivalenceMap);
				if (result == null) result = caseEquivalenceMap(baseEquivalenceMap);
				if (result == null) result = caseMap(baseEquivalenceMap);
				if (result == null) result = caseNamedElement(baseEquivalenceMap);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BaselinePackage.BASE_COMPLIANCE_MAP: {
				BaseComplianceMap baseComplianceMap = (BaseComplianceMap)theEObject;
				T result = caseBaseComplianceMap(baseComplianceMap);
				if (result == null) result = caseComplianceMap(baseComplianceMap);
				if (result == null) result = caseMap(baseComplianceMap);
				if (result == null) result = caseNamedElement(baseComplianceMap);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Framework</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Framework</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseFramework(BaseFramework object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Requirement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseRequirement(BaseRequirement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Artefact</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Artefact</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseArtefact(BaseArtefact object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Activity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Activity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseActivity(BaseActivity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Requirement Rel</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Requirement Rel</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseRequirementRel(BaseRequirementRel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Role</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Role</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseRole(BaseRole object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Applicability Level</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Applicability Level</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseApplicabilityLevel(BaseApplicabilityLevel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Criticality Level</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Criticality Level</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseCriticalityLevel(BaseCriticalityLevel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Technique</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Technique</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseTechnique(BaseTechnique object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Artefact Rel</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Artefact Rel</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseArtefactRel(BaseArtefactRel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Criticality Applicability</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Criticality Applicability</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseCriticalityApplicability(BaseCriticalityApplicability object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Activity Rel</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Activity Rel</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseActivityRel(BaseActivityRel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Assurable Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Assurable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseAssurableElement(BaseAssurableElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Independency Level</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Independency Level</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseIndependencyLevel(BaseIndependencyLevel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Recommendation Level</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Recommendation Level</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseRecommendationLevel(BaseRecommendationLevel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Control Category</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Control Category</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseControlCategory(BaseControlCategory object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Applicability</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Applicability</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseApplicability(BaseApplicability object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Applicability Rel</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Applicability Rel</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseApplicabilityRel(BaseApplicabilityRel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaselineElement(BaselineElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Equivalence Map</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Equivalence Map</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseEquivalenceMap(BaseEquivalenceMap object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Compliance Map</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Compliance Map</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseComplianceMap(BaseComplianceMap object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Describable Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Describable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDescribableElement(DescribableElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Map</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Map</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMap(Map object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equivalence Map</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equivalence Map</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquivalenceMap(EquivalenceMap object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Compliance Map</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Compliance Map</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComplianceMap(ComplianceMap object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //BaselineSwitch
