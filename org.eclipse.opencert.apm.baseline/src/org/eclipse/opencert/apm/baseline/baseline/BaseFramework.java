/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.baseline.baseline;

import java.util.Date;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.infra.general.general.DescribableElement;
import org.eclipse.opencert.pkm.refframework.refframework.RefFramework;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Framework</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getScope <em>Scope</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getRev <em>Rev</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getPurpose <em>Purpose</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getPublisher <em>Publisher</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getIssued <em>Issued</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getOwnedActivities <em>Owned Activities</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getOwnedArtefact <em>Owned Artefact</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getOwnedRequirement <em>Owned Requirement</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getOwnedApplicLevel <em>Owned Applic Level</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getOwnedCriticLevel <em>Owned Critic Level</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getOwnedRole <em>Owned Role</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getOwnedTechnique <em>Owned Technique</em>}</li>
 *   <li>{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getRefFramework <em>Ref Framework</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseFramework()
 * @model
 * @generated
 */
public interface BaseFramework extends DescribableElement {
	/**
	 * Returns the value of the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope</em>' attribute.
	 * @see #setScope(String)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseFramework_Scope()
	 * @model
	 * @generated
	 */
	String getScope();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getScope <em>Scope</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scope</em>' attribute.
	 * @see #getScope()
	 * @generated
	 */
	void setScope(String value);

	/**
	 * Returns the value of the '<em><b>Rev</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rev</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rev</em>' attribute.
	 * @see #setRev(String)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseFramework_Rev()
	 * @model
	 * @generated
	 */
	String getRev();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getRev <em>Rev</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rev</em>' attribute.
	 * @see #getRev()
	 * @generated
	 */
	void setRev(String value);

	/**
	 * Returns the value of the '<em><b>Purpose</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Purpose</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Purpose</em>' attribute.
	 * @see #setPurpose(String)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseFramework_Purpose()
	 * @model
	 * @generated
	 */
	String getPurpose();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getPurpose <em>Purpose</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Purpose</em>' attribute.
	 * @see #getPurpose()
	 * @generated
	 */
	void setPurpose(String value);

	/**
	 * Returns the value of the '<em><b>Publisher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Publisher</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Publisher</em>' attribute.
	 * @see #setPublisher(String)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseFramework_Publisher()
	 * @model
	 * @generated
	 */
	String getPublisher();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getPublisher <em>Publisher</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Publisher</em>' attribute.
	 * @see #getPublisher()
	 * @generated
	 */
	void setPublisher(String value);

	/**
	 * Returns the value of the '<em><b>Issued</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Issued</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Issued</em>' attribute.
	 * @see #setIssued(Date)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseFramework_Issued()
	 * @model
	 * @generated
	 */
	Date getIssued();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getIssued <em>Issued</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Issued</em>' attribute.
	 * @see #getIssued()
	 * @generated
	 */
	void setIssued(Date value);

	/**
	 * Returns the value of the '<em><b>Owned Activities</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.baseline.baseline.BaseActivity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Activities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Activities</em>' containment reference list.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseFramework_OwnedActivities()
	 * @model containment="true"
	 * @generated
	 */
	EList<BaseActivity> getOwnedActivities();

	/**
	 * Returns the value of the '<em><b>Owned Artefact</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.baseline.baseline.BaseArtefact}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Artefact</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Artefact</em>' containment reference list.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseFramework_OwnedArtefact()
	 * @model containment="true"
	 * @generated
	 */
	EList<BaseArtefact> getOwnedArtefact();

	/**
	 * Returns the value of the '<em><b>Owned Requirement</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.baseline.baseline.BaseRequirement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Requirement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Requirement</em>' containment reference list.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseFramework_OwnedRequirement()
	 * @model containment="true"
	 * @generated
	 */
	EList<BaseRequirement> getOwnedRequirement();

	/**
	 * Returns the value of the '<em><b>Owned Applic Level</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityLevel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Applic Level</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Applic Level</em>' containment reference list.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseFramework_OwnedApplicLevel()
	 * @model containment="true"
	 * @generated
	 */
	EList<BaseApplicabilityLevel> getOwnedApplicLevel();

	/**
	 * Returns the value of the '<em><b>Owned Critic Level</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityLevel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Critic Level</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Critic Level</em>' containment reference list.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseFramework_OwnedCriticLevel()
	 * @model containment="true"
	 * @generated
	 */
	EList<BaseCriticalityLevel> getOwnedCriticLevel();

	/**
	 * Returns the value of the '<em><b>Owned Role</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.baseline.baseline.BaseRole}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Role</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Role</em>' containment reference list.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseFramework_OwnedRole()
	 * @model containment="true"
	 * @generated
	 */
	EList<BaseRole> getOwnedRole();

	/**
	 * Returns the value of the '<em><b>Owned Technique</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.baseline.baseline.BaseTechnique}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Technique</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Technique</em>' containment reference list.
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseFramework_OwnedTechnique()
	 * @model containment="true"
	 * @generated
	 */
	EList<BaseTechnique> getOwnedTechnique();

	/**
	 * Returns the value of the '<em><b>Ref Framework</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ref Framework</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ref Framework</em>' reference.
	 * @see #setRefFramework(RefFramework)
	 * @see org.eclipse.opencert.apm.baseline.baseline.BaselinePackage#getBaseFramework_RefFramework()
	 * @model
	 * @generated
	 */
	RefFramework getRefFramework();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.apm.baseline.baseline.BaseFramework#getRefFramework <em>Ref Framework</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ref Framework</em>' reference.
	 * @see #getRefFramework()
	 * @generated
	 */
	void setRefFramework(RefFramework value);

} // BaseFramework
