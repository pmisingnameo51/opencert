/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.infra.properties.property;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.infra.general.general.NamedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.infra.properties.property.Property#getDatatype <em>Datatype</em>}</li>
 *   <li>{@link org.eclipse.opencert.infra.properties.property.Property#getEnumValues <em>Enum Values</em>}</li>
 *   <li>{@link org.eclipse.opencert.infra.properties.property.Property#getUnit <em>Unit</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.infra.properties.property.PropertyPackage#getProperty()
 * @model
 * @generated
 */
public interface Property extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Datatype</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.opencert.infra.properties.property.DataTypeKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datatype</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datatype</em>' attribute.
	 * @see org.eclipse.opencert.infra.properties.property.DataTypeKind
	 * @see #setDatatype(DataTypeKind)
	 * @see org.eclipse.opencert.infra.properties.property.PropertyPackage#getProperty_Datatype()
	 * @model
	 * @generated
	 */
	DataTypeKind getDatatype();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.infra.properties.property.Property#getDatatype <em>Datatype</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Datatype</em>' attribute.
	 * @see org.eclipse.opencert.infra.properties.property.DataTypeKind
	 * @see #getDatatype()
	 * @generated
	 */
	void setDatatype(DataTypeKind value);

	/**
	 * Returns the value of the '<em><b>Enum Values</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enum Values</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enum Values</em>' attribute list.
	 * @see org.eclipse.opencert.infra.properties.property.PropertyPackage#getProperty_EnumValues()
	 * @model
	 * @generated
	 */
	EList<String> getEnumValues();

	/**
	 * Returns the value of the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit</em>' attribute.
	 * @see #setUnit(String)
	 * @see org.eclipse.opencert.infra.properties.property.PropertyPackage#getProperty_Unit()
	 * @model
	 * @generated
	 */
	String getUnit();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.infra.properties.property.Property#getUnit <em>Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit</em>' attribute.
	 * @see #getUnit()
	 * @generated
	 */
	void setUnit(String value);

} // Property
