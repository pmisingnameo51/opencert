/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.infra.properties.property.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.opencert.infra.general.general.impl.NamedElementImpl;
import org.eclipse.opencert.infra.properties.property.DataTypeKind;
import org.eclipse.opencert.infra.properties.property.Property;
import org.eclipse.opencert.infra.properties.property.PropertyPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.infra.properties.property.impl.PropertyImpl#getDatatype <em>Datatype</em>}</li>
 *   <li>{@link org.eclipse.opencert.infra.properties.property.impl.PropertyImpl#getEnumValues <em>Enum Values</em>}</li>
 *   <li>{@link org.eclipse.opencert.infra.properties.property.impl.PropertyImpl#getUnit <em>Unit</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PropertyImpl extends NamedElementImpl implements Property {
	/**
	 * The default value of the '{@link #getDatatype() <em>Datatype</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatatype()
	 * @generated
	 * @ordered
	 */
	protected static final DataTypeKind DATATYPE_EDEFAULT = DataTypeKind.ENUMERATION;

	/**
	 * The default value of the '{@link #getUnit() <em>Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnit()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PropertyPackage.Literals.PROPERTY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataTypeKind getDatatype() {
		return (DataTypeKind)eDynamicGet(PropertyPackage.PROPERTY__DATATYPE, PropertyPackage.Literals.PROPERTY__DATATYPE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDatatype(DataTypeKind newDatatype) {
		eDynamicSet(PropertyPackage.PROPERTY__DATATYPE, PropertyPackage.Literals.PROPERTY__DATATYPE, newDatatype);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<String> getEnumValues() {
		return (EList<String>)eDynamicGet(PropertyPackage.PROPERTY__ENUM_VALUES, PropertyPackage.Literals.PROPERTY__ENUM_VALUES, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnit() {
		return (String)eDynamicGet(PropertyPackage.PROPERTY__UNIT, PropertyPackage.Literals.PROPERTY__UNIT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnit(String newUnit) {
		eDynamicSet(PropertyPackage.PROPERTY__UNIT, PropertyPackage.Literals.PROPERTY__UNIT, newUnit);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PropertyPackage.PROPERTY__DATATYPE:
				return getDatatype();
			case PropertyPackage.PROPERTY__ENUM_VALUES:
				return getEnumValues();
			case PropertyPackage.PROPERTY__UNIT:
				return getUnit();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PropertyPackage.PROPERTY__DATATYPE:
				setDatatype((DataTypeKind)newValue);
				return;
			case PropertyPackage.PROPERTY__ENUM_VALUES:
				getEnumValues().clear();
				getEnumValues().addAll((Collection<? extends String>)newValue);
				return;
			case PropertyPackage.PROPERTY__UNIT:
				setUnit((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PropertyPackage.PROPERTY__DATATYPE:
				setDatatype(DATATYPE_EDEFAULT);
				return;
			case PropertyPackage.PROPERTY__ENUM_VALUES:
				getEnumValues().clear();
				return;
			case PropertyPackage.PROPERTY__UNIT:
				setUnit(UNIT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PropertyPackage.PROPERTY__DATATYPE:
				return getDatatype() != DATATYPE_EDEFAULT;
			case PropertyPackage.PROPERTY__ENUM_VALUES:
				return !getEnumValues().isEmpty();
			case PropertyPackage.PROPERTY__UNIT:
				return UNIT_EDEFAULT == null ? getUnit() != null : !UNIT_EDEFAULT.equals(getUnit());
		}
		return super.eIsSet(featureID);
	}

} //PropertyImpl
