/*******************************************************************************
 * Copyright (C) 2017 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic;

import java.io.Closeable;
import java.util.Iterator;
import org.apache.http.HttpHost;

/**
 * Simple client side interface to talk to an Elastic server via REST.
 * 
 * @author mauersberger
 */
public interface ElasticClient extends Closeable {

	/**
	 * Store the given {@link de.ikv.analyze.census.elasticsearch.ElasticDocument}
	 * 
	 * @param document
	 * @return this
	 * @throws Exception
	 */
	ElasticClient store(ElasticDocument document) throws Exception;

	/**
	 * Store the given {@link java.util.List} of
	 * {@link de.ikv.analyze.census.elasticsearch.ElasticDocument}
	 * 
	 * @param documents
	 * @return this
	 * @throws Exception
	 */
	ElasticClient storeAll(Iterator<ElasticDocument> documents) throws Exception;
	
	/**
	 * Attempt to ping the client
	 *
	 * @return this on success
	 * @throws Exception
	 *             on failure
	 */
	ElasticClient ping() throws Exception;

	/**
	 * Deletes the index with the given name.
	 * 
	 * @param indexName
	 *            the name of the index
	 * @return this on success
	 * @throws Exception
	 *             on failure
	 */
	ElasticClient delete(String indexName) throws Exception;

	/**
	 * Retrieves the server version.
	 *
	 * @return the version string
	 * @throws Exception
	 *             on failure
	 */
	String version() throws Exception;

	/**
	 * Returns the communication end point for this client, i.e. the connection
	 * settings used to speak with the server.
	 * 
	 * @return the {@link HttpHost} having the settings, never <code>null</code>
	 */
	HttpHost endPoint();

	/**
	 * Retrieves the status of the index with the given name.
	 *
	 * @param indexName
	 *            the name of the index, never <code>null</code>
	 * @return the status, never <code>null</code>
	 * @throws Exception
	 *             on failure
	 */
	String indexStatus(String indexName) throws Exception;
}
