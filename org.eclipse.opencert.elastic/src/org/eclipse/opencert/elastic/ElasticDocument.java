/*******************************************************************************
 * Copyright (C) 2017 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic;

import com.google.gson.JsonObject;

/**
 * Simply wraps a JSON object (the source) by additional Elastic information as
 * document ID, index and type. To avoid complexity we have no setter / getter
 * but public fields.
 * 
 * @author mauersberger
 */
public class ElasticDocument {

	/*
	 * The Underscore or Low Dash.
	 */
	private static final String LOWDASH = "_"; //$NON-NLS-1$

	/*
	 * The Slash, nothing more, nothing less.
	 */
	private static final String SLASH = "/".intern(); //$NON-NLS-1$

	/**
	 * The source {@link JsonObject}
	 */
	public JsonObject source;

	/**
	 * The index name
	 */
	public String index;

	/**
	 * The document type
	 */
	public String type;

	/**
	 * The unique document id
	 */
	public String id;

	/**
	 * CTOR
	 *
	 * @param source
	 *            the source {@link JsonObject}
	 * @param index
	 *            the index name, never <code>null</code>
	 * @param type
	 *            the document type, never <code>null</code>
	 * @param id
	 *            the unique document id, never <code>null</code>
	 */
	public ElasticDocument(JsonObject source, String index, String type, String id) {
		this.source = source;
		this.index = index;
		this.type = type;
		this.id = id;
	}

	/**
	 * @return the Elasticsearch end point for this document
	 */
	public String getEndPoint() {
		assert ((this.index != null) && !this.index.isEmpty());
		assert ((this.type != null) && !this.type.isEmpty());
		assert ((this.id != null) && !this.id.isEmpty());

		StringBuffer buffer = new StringBuffer(SLASH);
		buffer.append(this.index);
		buffer.append(SLASH).append(this.type);
		// TODO Slashes are reserved characters, better use a checksum?
		buffer.append(SLASH).append(this.id.replace(SLASH, LOWDASH));
		return buffer.toString();
	}

}
