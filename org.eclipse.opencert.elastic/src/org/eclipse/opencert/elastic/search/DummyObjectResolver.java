/*******************************************************************************
 * Copyright (C) 2018 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic.search;

import java.util.Collection;

/**
 * XXX Dummy hack - TO BE REMOVED LATER
 * 
 * @author mauersberger
 *
 */
public class DummyObjectResolver implements ObjectResolver<Object> {

	@Override
	public Object resolve(String objectId, Object context) {
		// XXX hack
		@SuppressWarnings("unchecked")
		Collection<Object> data = (Collection<Object>) context;
		for (Object object : data) {
			String document = DummyData.INSTANCE.asDocument(object);
			String id = DummyData.INSTANCE.getId(document);
			if (objectId.equals(id)) {
				return object;
			}
		}
		return null;
	}

}
