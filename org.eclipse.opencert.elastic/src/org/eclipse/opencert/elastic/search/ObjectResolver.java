/*******************************************************************************
 * Copyright (C) 2018 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic.search;

/**
 * A generic resolution interface to resolve an arbitrary object identifier
 * (e.g. a CDI ID or a generic EMF resource URI) to an object in a given
 * context.
 *
 * @author mauersberger
 */
public interface ObjectResolver<T> {

	T resolve(String objectId, Object context);
}
