/*******************************************************************************
 * Copyright (C) 2018 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic.search;

/**
 * Simple structure that holds search hits, combining an identifier to the found
 * document, a score number as well as an object identifier to the original
 * object that was indexed.
 *
 * @author mauersberger
 */
public class Hit {

	/**
	 * A URL to the document.
	 */
	public String document;

	/**
	 * A score value returned by elastic.
	 */
	public int score;

	/**
	 * The object identifier i.e. a unique elastic search ID
	 */
	public String objectId;

	/**
	 * The object URI e.g. a EMF URI or a CDO URI
	 */
	public String uri;

	@Override
	public String toString() {
		return "Hit (score=" + score + ", objectId=" + objectId + ", uri=" + uri + ", document=" + document + ")";
	}
}
