/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.pkm.refframework.utils.figures.activator;

//import org.eclipse.core.runtime.Plugin;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;


public class PluginActivator extends AbstractUIPlugin {

  public static final String ID = "org.eclipse.opencert.pkm.refframework.utils"; //$NON-NLS-1$


  private static PluginActivator ourInstance;

  public PluginActivator() {}

  public void start(BundleContext context) throws Exception {
    super.start(context);
    ourInstance = this;
  }

  public void stop(BundleContext context) throws Exception {
    ourInstance = null;
    super.stop(context);
  }

  public static PluginActivator getDefault() {
    return ourInstance;
  }
}
