/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.wizards.ui.wizards;

import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.emf.cdo.dawn.commands.CreateSemanticResourceRecordingCommand;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.transaction.DawnGMFEditingDomainFactory;
import org.eclipse.emf.cdo.dawn.ui.DawnEditorInput;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.core.services.ViewService;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CanonicalEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.gmf.runtime.diagram.ui.parts.IDiagramWorkbenchPart;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.opencert.apm.assurproj.assuranceproject.presentation.AssuranceprojectEditorPlugin;
import org.eclipse.swt.SWTException;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.opencert.apm.baseline.baseline.BaselineFactory;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.commands.LoadViewLocations;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityBaseActivitySubActivityCompartment2EditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.parts.BaseActivityBaseActivitySubActivityCompartmentEditPart;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.policies.BaseActivityBaseActivitySubActivityCompartment2RepairViewCanonicalEditPolicy;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.policies.BaseActivityBaseActivitySubActivityCompartmentRepairViewCanonicalEditPolicy;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.policies.DawnBaseFrameworkPersistedCanonicalEditPolicy;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineDiagramEditorPlugin;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.BaselineVisualIDRegistry;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.DawnBaselineDiagramEditor;
import org.eclipse.opencert.apm.baseline.baseline.diagram.part.Messages;
import org.eclipse.opencert.apm.baseline.baseline.diagram.edit.policies.DawnBaseFrameworkPersistedSemiCanonicalEditPolicy;


// Start MCP
public class DawnBaselineDiagramUtil {
	private CDOResource sourceDiagramResource; // for location and size views	
	private URI modelURI;
	private URI diagramURI;
	protected Resource diagram;
	private CDOResourceFolder assuranceprojectFolder;
	private CDOTransaction transaction;
	private List<EObject> listRootObjects = null;	

	private static String DawnBaselineDiagramEditor_ID = "org.eclipse.opencert.apm.baseline.baseline.diagram.part.DawnBaselineDiagramEditor";
	private final static String DawnBaseFrameworkEditPart_MODEL_ID = "Baseline";
	
	/**
	 * Constructor 
	 */
	/*
	public DawnBaselineDiagramUtil(URI model, URI diagram, CDOResourceFolder assuranceprojectFolder, CDOTransaction transaction) {
		super();
		this.modelURI = model;
		this.diagramURI = diagram;
		this.assuranceprojectFolder = assuranceprojectFolder;
		this.transaction = transaction;
	}
	*/
	
	/**
	 * Constructor 
	 */
	public DawnBaselineDiagramUtil(CDOResource sourceDiagramResource, List<EObject> lro, URI model, URI diagram, CDOResourceFolder assuranceprojectFolder, CDOTransaction transaction) {
		super();
		this.sourceDiagramResource = sourceDiagramResource;
		this.listRootObjects = lro;
		this.modelURI = model;
		this.diagramURI = diagram;
		this.assuranceprojectFolder = assuranceprojectFolder;
		this.transaction = transaction;
	}

	public void generateDiagram(IProgressMonitor monitor) {
		try {
			generateDiagram0(monitor);
			/* MCP
			IWorkspace workspace = ResourcesPlugin.getWorkspace();
			workspace.getRoot().refreshLocal(IResource.DEPTH_INFINITE, null);
			*/
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateEditPartChildren(IGraphicalEditPart editPart, String eClassName, EditPolicy newPolicy) {
		if(editPart == null) {
			return;
		}
		View view = editPart.getNotationView();
		if(view == null) {
			return;
		}

		for(Object child : editPart.getChildren()) {
			if(child instanceof IGraphicalEditPart) {
				updateEditPartChildren(((IGraphicalEditPart)child), eClassName, newPolicy);
			}
		}

		EObject element = view.getElement();
		//if(eClass != null && eClass.isInstance(element)) {
		if(eClassName.contains("BaseActivityBaseActivitySubActivityCompartment2EditPart"))
		{
			if(element  instanceof BaseActivityBaseActivitySubActivityCompartment2EditPart) {
				List editPolicies = CanonicalEditPolicy.getRegisteredEditPolicies(element);
				while(!editPolicies.isEmpty())
				{
					editPolicies.remove(0);
				}
				IGraphicalEditPart temp = (IGraphicalEditPart)element;
				temp.installEditPolicy(EditPolicyRoles.CANONICAL_ROLE, newPolicy);
			}
		}
		else if(eClassName.contains("BaseActivityBaseActivitySubActivityCompartmentEditPart"))
		{
			if(element  instanceof BaseActivityBaseActivitySubActivityCompartmentEditPart) {
				List editPolicies = CanonicalEditPolicy.getRegisteredEditPolicies(element);
				while(!editPolicies.isEmpty())
				{
					editPolicies.remove(0);
				}
				IGraphicalEditPart temp = (IGraphicalEditPart)element;
				temp.installEditPolicy(EditPolicyRoles.CANONICAL_ROLE, newPolicy);
			}
		}
	}

	
	//generates a diagram for the given domain model file name
	public void generateDiagram0(IProgressMonitor monitor) throws IOException {
		//diagram = DawnBaselineDiagramEditorUtil.createDiagram(diagramURI, modelURI, monitor);
		diagram = createDiagram(diagramURI, modelURI, monitor);
		if (diagram != null) {
			try {
				//DawnBaselineDiagramEditorUtil.openDiagram(diagram);
				openDiagram(diagram); // para que el editor de baseline_diagram este activo
				// Start MCP
				IEditorPart editorPart = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage()
						.getActiveEditor();
				if (editorPart instanceof IDiagramWorkbenchPart) {
					DiagramEditPart  editp = ((IDiagramWorkbenchPart) editorPart).getDiagramEditPart();
					editp.removeEditPolicy(EditPolicyRoles.CANONICAL_ROLE);
					/* FUNCIONA PERO CREA TODAS LAS VISTAS!!!!
 					editp.installEditPolicy(EditPolicyRoles.CANONICAL_ROLE,
							new DawnBaseFrameworkPersistedCanonicalEditPolicy());
 					*/
					/* NO FUNCIONA PORQUE TODAVIA NO SE HAN CREADO SUS ELEMENTOS
					updateEditPartChildren((IGraphicalEditPart)editp,  "BaseActivityBaseActivitySubActivityCompartment2EditPart", 
																	  new BaseActivityBaseActivitySubActivityCompartment2RepairViewCanonicalEditPolicy());
					updateEditPartChildren((IGraphicalEditPart)editp,  "BaseActivityBaseActivitySubActivityCompartmentEditPart", 
							  new BaseActivityBaseActivitySubActivityCompartmentRepairViewCanonicalEditPolicy());
					*/
					LoadViewLocations sourceViewLocations = new LoadViewLocations(sourceDiagramResource);
					sourceViewLocations.LoadObjectLocations();
					BaseActivityBaseActivitySubActivityCompartmentEditPart.NOT_FROM_REFFRAMEWORK = 0;
					BaseActivityBaseActivitySubActivityCompartmentEditPart.sourceViewLocations = sourceViewLocations;
					BaseActivityBaseActivitySubActivityCompartment2EditPart.NOT_FROM_REFFRAMEWORK = 0;
					BaseActivityBaseActivitySubActivityCompartment2EditPart.sourceViewLocations = sourceViewLocations;					
					DawnBaseFrameworkPersistedSemiCanonicalEditPolicy scp = new DawnBaseFrameworkPersistedSemiCanonicalEditPolicy();
					scp.setObjects(listRootObjects);
					scp.setSourceDiagram(sourceViewLocations);
 					//editp.installEditPolicy(EditPolicyRoles.CANONICAL_ROLE, scp);
					scp.setHost(editp);
					scp.refresh();                
					scp.deactivate(); // para que no invoque refreshSemantic() al eliminar del diagrama uno de los objetos
					
					
	        		// No puedo cerrar el diagrama porque no se crean las views!!! y pierdo la perspectiva
	        		DawnBaselineDiagramEditor dawnEditorPart = (DawnBaselineDiagramEditor)editorPart;
	        		dawnEditorPart.doSave(monitor);

	        		if(editorPart instanceof DiagramEditor) {
	        			boolean save = true;

	        	        try {
	            			DiagramEditor deditor = (DiagramEditor)editorPart;
	            			deditor.getSite().getPage().closeEditor(deditor, save); // cross project si quiero no cerrarlo comenatr esta linea
	        	        } catch (SWTException e) {
	        	        	AssuranceprojectEditorPlugin.INSTANCE.log(e);
	        	        }      			
	        		}

	        		BaseActivityBaseActivitySubActivityCompartmentEditPart.NOT_FROM_REFFRAMEWORK = 1;
					BaseActivityBaseActivitySubActivityCompartment2EditPart.NOT_FROM_REFFRAMEWORK = 1;
				}				
				// End MCP
			} catch (PartInitException e) {
				AssuranceprojectEditorPlugin.INSTANCE.log(e);
			}
		}
	}
	
	public Resource createDiagram(URI diagramURI, URI modelURI,
			IProgressMonitor progressMonitor) {
		TransactionalEditingDomain editingDomain = DawnGMFEditingDomainFactory
				.getInstance().createEditingDomain();

		progressMonitor
				.beginTask(
						Messages.BaselineDiagramEditorUtil_CreateDiagramProgressTask,
						3);

		/*
		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		CDOConnectionUtil.instance.getCurrentSession();

		ResourceSet resourceSet = editingDomain.getResourceSet();

		CDOTransaction transaction = CDOConnectionUtil.instance
				.openCurrentTransaction(resourceSet, diagramURI.toString());
		final Resource diagramResource = resourceSet.createResource(diagramURI);				
		*/

		String diagramResourceStr = diagramURI.toString().substring("cdo://opencert".length(), diagramURI.toString().length());
		final Resource diagramResource = transaction.getOrCreateResource(diagramResourceStr);

		CreateSemanticResourceRecordingCommand createSemanticResourceCommand = new CreateSemanticResourceRecordingCommand(
				editingDomain, transaction, modelURI.path());

		editingDomain.getCommandStack().execute(createSemanticResourceCommand);
		final Resource modelResource = createSemanticResourceCommand
				.getResource();

		final String diagramName = diagramURI.lastSegment();
		AbstractTransactionalCommand command = new AbstractTransactionalCommand(
				editingDomain,
				Messages.BaselineDiagramEditorUtil_CreateDiagramCommandLabel,
				Collections.EMPTY_LIST) {
			@Override
			protected CommandResult doExecuteWithResult(
					IProgressMonitor monitor, IAdaptable info)
					throws ExecutionException {
				EObject model = null;
				if (modelResource.getContents().size() > 0) {
					model = modelResource.getContents().get(0);
				} else {
					model = createInitialModel();
					attachModelToResource(model, modelResource);
				}

				Diagram diagram = ViewService.createDiagram(model,
						DawnBaseFrameworkEditPart_MODEL_ID,
						BaselineDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);
				if (diagram != null) {
					diagramResource.getContents().add(diagram);
					diagram.setName(diagramName);
					diagram.setElement(model);
				}

				try {
					modelResource.save(Collections.EMPTY_MAP);
					diagramResource.save(Collections.EMPTY_MAP);
				} catch (IOException e) {
					BaselineDiagramEditorPlugin.getInstance().logError(
							"Unable to store model and diagram resources", e); //$NON-NLS-1$
				}

				return CommandResult.newOKCommandResult();
			}
		};
		try {
			OperationHistoryFactory.getOperationHistory().execute(command,
					new SubProgressMonitor(progressMonitor, 1), null);
		} catch (ExecutionException e) {
			BaselineDiagramEditorPlugin.getInstance().logError(
					"Unable to create model and diagram", e); //$NON-NLS-1$
		}
		setCharset(WorkspaceSynchronizer.getFile(modelResource));
		setCharset(WorkspaceSynchronizer.getFile(diagramResource));
		return diagramResource;
	}

	private static EObject createInitialModel() {
		return BaselineFactory.eINSTANCE.createBaseFramework();
	}

	private static void attachModelToResource(EObject model, Resource resource) {
		resource.getContents().add(model);
	}
	
	public static boolean openDiagram(Resource diagram)
			throws PartInitException {
		IWorkbenchPage page = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		return null != page.openEditor(new DawnEditorInput(diagram.getURI()),
				DawnBaselineDiagramEditor_ID);
	}
	
	public static void setCharset(IFile file) {
		if (file == null) {
			return;
		}
		try {
			file.setCharset("UTF-8", new NullProgressMonitor()); //$NON-NLS-1$
		} catch (CoreException e) {
			BaselineDiagramEditorPlugin.getInstance().logError(
					"Unable to set charset for file " + file.getFullPath(), e); //$NON-NLS-1$
		}
	}
}
// End MCP