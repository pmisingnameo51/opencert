/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.vocabulary;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Category</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.vocabulary.Category#getTerms <em>Terms</em>}</li>
 *   <li>{@link org.eclipse.opencert.vocabulary.Category#getSubCategories <em>Sub Categories</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.vocabulary.VocabularyPackage#getCategory()
 * @model annotation="gmf.node label='name' color='252,252,230' tool.name='Category' tool.description='Create a category for terms.' tool.small.bundle='org.eclipse.opencert.vocabulary' tool.small.path='icons/famfamfam_silk_icons_v013/icons/folder.png' tool.large.bundle='org.eclipse.opencert.vocabulary' tool.large.path='icons/famfamfam_silk_icons_v013/icons/folder.png' label.icon='false'"
 * @generated
 */
public interface Category extends VocabularyElement
{
  /**
   * Returns the value of the '<em><b>Terms</b></em>' reference list.
   * The list contents are of type {@link org.eclipse.opencert.vocabulary.Term}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Terms</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Terms</em>' reference list.
   * @see org.eclipse.opencert.vocabulary.VocabularyPackage#getCategory_Terms()
   * @model annotation="gmf.link label.text='categorized term' color='50,50,50' target.decoration='arrow' tool.name='categorized term' tool.description='Create a connection between a category and a term.' tool.small.bundle='org.eclipse.opencert.vocabulary' tool.small.path='icons/link.png' tool.large.bundle='org.eclipse.opencert.vocabulary' tool.large.path='icons/link.png'"
   * @generated
   */
  EList<Term> getTerms();

  /**
   * Returns the value of the '<em><b>Sub Categories</b></em>' reference list.
   * The list contents are of type {@link org.eclipse.opencert.vocabulary.Category}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sub Categories</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sub Categories</em>' reference list.
   * @see org.eclipse.opencert.vocabulary.VocabularyPackage#getCategory_SubCategories()
   * @model annotation="gmf.link label.text='subcategory' color='50,50,50' target.decoration='arrow' tool.name='subcategory' tool.description='Create a subcategory relationship between two categories.' tool.small.bundle='org.eclipse.opencert.vocabulary' tool.small.path='icons/link.png' tool.large.bundle='org.eclipse.opencert.vocabulary' tool.large.path='icons/link.png'"
   * @generated
   */
  EList<Category> getSubCategories();

} // Category
