/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.vocabulary;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.opencert.vocabulary.VocabularyFactory
 * @model kind="package"
 *        annotation="emf.gen basePackage='org.eclipse.opencert'"
 *        annotation="emf.gen modelDirectory='org.eclipse.opencert.vocabulary/src-gen'"
 * @generated
 */
public interface VocabularyPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "vocabulary";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://opencert.org/vocabulary/2.0";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "vocabulary";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  VocabularyPackage eINSTANCE = org.eclipse.opencert.vocabulary.impl.VocabularyPackageImpl.init();

  /**
   * The meta object id for the '{@link org.eclipse.opencert.vocabulary.VocabularyElement <em>Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.opencert.vocabulary.VocabularyElement
   * @see org.eclipse.opencert.vocabulary.impl.VocabularyPackageImpl#getVocabularyElement()
   * @generated
   */
  int VOCABULARY_ELEMENT = 4;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VOCABULARY_ELEMENT__NAME = 0;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VOCABULARY_ELEMENT__DESCRIPTION = 1;

  /**
   * The number of structural features of the '<em>Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VOCABULARY_ELEMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.eclipse.opencert.vocabulary.impl.VocabularyImpl <em>Vocabulary</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.opencert.vocabulary.impl.VocabularyImpl
   * @see org.eclipse.opencert.vocabulary.impl.VocabularyPackageImpl#getVocabulary()
   * @generated
   */
  int VOCABULARY = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VOCABULARY__NAME = VOCABULARY_ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VOCABULARY__DESCRIPTION = VOCABULARY_ELEMENT__DESCRIPTION;

  /**
   * The feature id for the '<em><b>Terms</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VOCABULARY__TERMS = VOCABULARY_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Categories</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VOCABULARY__CATEGORIES = VOCABULARY_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Sources Of Definition</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VOCABULARY__SOURCES_OF_DEFINITION = VOCABULARY_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Vocabulary</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VOCABULARY_FEATURE_COUNT = VOCABULARY_ELEMENT_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.eclipse.opencert.vocabulary.impl.CategoryImpl <em>Category</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.opencert.vocabulary.impl.CategoryImpl
   * @see org.eclipse.opencert.vocabulary.impl.VocabularyPackageImpl#getCategory()
   * @generated
   */
  int CATEGORY = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CATEGORY__NAME = VOCABULARY_ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CATEGORY__DESCRIPTION = VOCABULARY_ELEMENT__DESCRIPTION;

  /**
   * The feature id for the '<em><b>Terms</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CATEGORY__TERMS = VOCABULARY_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Sub Categories</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CATEGORY__SUB_CATEGORIES = VOCABULARY_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Category</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CATEGORY_FEATURE_COUNT = VOCABULARY_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.eclipse.opencert.vocabulary.impl.TermImpl <em>Term</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.opencert.vocabulary.impl.TermImpl
   * @see org.eclipse.opencert.vocabulary.impl.VocabularyPackageImpl#getTerm()
   * @generated
   */
  int TERM = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM__NAME = VOCABULARY_ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM__DESCRIPTION = VOCABULARY_ELEMENT__DESCRIPTION;

  /**
   * The feature id for the '<em><b>Definitions</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM__DEFINITIONS = VOCABULARY_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Notes</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM__NOTES = VOCABULARY_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Examples</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM__EXAMPLES = VOCABULARY_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Synonyms</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM__SYNONYMS = VOCABULARY_ELEMENT_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Defined By</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM__DEFINED_BY = VOCABULARY_ELEMENT_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Is A</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM__IS_A = VOCABULARY_ELEMENT_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Has A</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM__HAS_A = VOCABULARY_ELEMENT_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Refers To</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM__REFERS_TO = VOCABULARY_ELEMENT_FEATURE_COUNT + 7;

  /**
   * The number of structural features of the '<em>Term</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM_FEATURE_COUNT = VOCABULARY_ELEMENT_FEATURE_COUNT + 8;

  /**
   * The meta object id for the '{@link org.eclipse.opencert.vocabulary.impl.SourceOfDefinitionImpl <em>Source Of Definition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.opencert.vocabulary.impl.SourceOfDefinitionImpl
   * @see org.eclipse.opencert.vocabulary.impl.VocabularyPackageImpl#getSourceOfDefinition()
   * @generated
   */
  int SOURCE_OF_DEFINITION = 3;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE_OF_DEFINITION__NAME = VOCABULARY_ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE_OF_DEFINITION__DESCRIPTION = VOCABULARY_ELEMENT__DESCRIPTION;

  /**
   * The feature id for the '<em><b>Uri</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE_OF_DEFINITION__URI = VOCABULARY_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Source Of Definition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE_OF_DEFINITION_FEATURE_COUNT = VOCABULARY_ELEMENT_FEATURE_COUNT + 1;


  /**
   * Returns the meta object for class '{@link org.eclipse.opencert.vocabulary.Vocabulary <em>Vocabulary</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Vocabulary</em>'.
   * @see org.eclipse.opencert.vocabulary.Vocabulary
   * @generated
   */
  EClass getVocabulary();

  /**
   * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.vocabulary.Vocabulary#getTerms <em>Terms</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Terms</em>'.
   * @see org.eclipse.opencert.vocabulary.Vocabulary#getTerms()
   * @see #getVocabulary()
   * @generated
   */
  EReference getVocabulary_Terms();

  /**
   * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.vocabulary.Vocabulary#getCategories <em>Categories</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Categories</em>'.
   * @see org.eclipse.opencert.vocabulary.Vocabulary#getCategories()
   * @see #getVocabulary()
   * @generated
   */
  EReference getVocabulary_Categories();

  /**
   * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.vocabulary.Vocabulary#getSourcesOfDefinition <em>Sources Of Definition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Sources Of Definition</em>'.
   * @see org.eclipse.opencert.vocabulary.Vocabulary#getSourcesOfDefinition()
   * @see #getVocabulary()
   * @generated
   */
  EReference getVocabulary_SourcesOfDefinition();

  /**
   * Returns the meta object for class '{@link org.eclipse.opencert.vocabulary.Category <em>Category</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Category</em>'.
   * @see org.eclipse.opencert.vocabulary.Category
   * @generated
   */
  EClass getCategory();

  /**
   * Returns the meta object for the reference list '{@link org.eclipse.opencert.vocabulary.Category#getTerms <em>Terms</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Terms</em>'.
   * @see org.eclipse.opencert.vocabulary.Category#getTerms()
   * @see #getCategory()
   * @generated
   */
  EReference getCategory_Terms();

  /**
   * Returns the meta object for the reference list '{@link org.eclipse.opencert.vocabulary.Category#getSubCategories <em>Sub Categories</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Sub Categories</em>'.
   * @see org.eclipse.opencert.vocabulary.Category#getSubCategories()
   * @see #getCategory()
   * @generated
   */
  EReference getCategory_SubCategories();

  /**
   * Returns the meta object for class '{@link org.eclipse.opencert.vocabulary.Term <em>Term</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Term</em>'.
   * @see org.eclipse.opencert.vocabulary.Term
   * @generated
   */
  EClass getTerm();

  /**
   * Returns the meta object for the attribute list '{@link org.eclipse.opencert.vocabulary.Term#getDefinitions <em>Definitions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Definitions</em>'.
   * @see org.eclipse.opencert.vocabulary.Term#getDefinitions()
   * @see #getTerm()
   * @generated
   */
  EAttribute getTerm_Definitions();

  /**
   * Returns the meta object for the attribute list '{@link org.eclipse.opencert.vocabulary.Term#getNotes <em>Notes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Notes</em>'.
   * @see org.eclipse.opencert.vocabulary.Term#getNotes()
   * @see #getTerm()
   * @generated
   */
  EAttribute getTerm_Notes();

  /**
   * Returns the meta object for the attribute list '{@link org.eclipse.opencert.vocabulary.Term#getExamples <em>Examples</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Examples</em>'.
   * @see org.eclipse.opencert.vocabulary.Term#getExamples()
   * @see #getTerm()
   * @generated
   */
  EAttribute getTerm_Examples();

  /**
   * Returns the meta object for the attribute list '{@link org.eclipse.opencert.vocabulary.Term#getSynonyms <em>Synonyms</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Synonyms</em>'.
   * @see org.eclipse.opencert.vocabulary.Term#getSynonyms()
   * @see #getTerm()
   * @generated
   */
  EAttribute getTerm_Synonyms();

  /**
   * Returns the meta object for the reference '{@link org.eclipse.opencert.vocabulary.Term#getDefinedBy <em>Defined By</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Defined By</em>'.
   * @see org.eclipse.opencert.vocabulary.Term#getDefinedBy()
   * @see #getTerm()
   * @generated
   */
  EReference getTerm_DefinedBy();

  /**
   * Returns the meta object for the reference list '{@link org.eclipse.opencert.vocabulary.Term#getIsA <em>Is A</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Is A</em>'.
   * @see org.eclipse.opencert.vocabulary.Term#getIsA()
   * @see #getTerm()
   * @generated
   */
  EReference getTerm_IsA();

  /**
   * Returns the meta object for the reference list '{@link org.eclipse.opencert.vocabulary.Term#getHasA <em>Has A</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Has A</em>'.
   * @see org.eclipse.opencert.vocabulary.Term#getHasA()
   * @see #getTerm()
   * @generated
   */
  EReference getTerm_HasA();

  /**
   * Returns the meta object for the reference list '{@link org.eclipse.opencert.vocabulary.Term#getRefersTo <em>Refers To</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Refers To</em>'.
   * @see org.eclipse.opencert.vocabulary.Term#getRefersTo()
   * @see #getTerm()
   * @generated
   */
  EReference getTerm_RefersTo();

  /**
   * Returns the meta object for class '{@link org.eclipse.opencert.vocabulary.SourceOfDefinition <em>Source Of Definition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Source Of Definition</em>'.
   * @see org.eclipse.opencert.vocabulary.SourceOfDefinition
   * @generated
   */
  EClass getSourceOfDefinition();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.opencert.vocabulary.SourceOfDefinition#getUri <em>Uri</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uri</em>'.
   * @see org.eclipse.opencert.vocabulary.SourceOfDefinition#getUri()
   * @see #getSourceOfDefinition()
   * @generated
   */
  EAttribute getSourceOfDefinition_Uri();

  /**
   * Returns the meta object for class '{@link org.eclipse.opencert.vocabulary.VocabularyElement <em>Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Element</em>'.
   * @see org.eclipse.opencert.vocabulary.VocabularyElement
   * @generated
   */
  EClass getVocabularyElement();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.opencert.vocabulary.VocabularyElement#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.eclipse.opencert.vocabulary.VocabularyElement#getName()
   * @see #getVocabularyElement()
   * @generated
   */
  EAttribute getVocabularyElement_Name();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.opencert.vocabulary.VocabularyElement#getDescription <em>Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Description</em>'.
   * @see org.eclipse.opencert.vocabulary.VocabularyElement#getDescription()
   * @see #getVocabularyElement()
   * @generated
   */
  EAttribute getVocabularyElement_Description();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  VocabularyFactory getVocabularyFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link org.eclipse.opencert.vocabulary.impl.VocabularyImpl <em>Vocabulary</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.eclipse.opencert.vocabulary.impl.VocabularyImpl
     * @see org.eclipse.opencert.vocabulary.impl.VocabularyPackageImpl#getVocabulary()
     * @generated
     */
    EClass VOCABULARY = eINSTANCE.getVocabulary();

    /**
     * The meta object literal for the '<em><b>Terms</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VOCABULARY__TERMS = eINSTANCE.getVocabulary_Terms();

    /**
     * The meta object literal for the '<em><b>Categories</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VOCABULARY__CATEGORIES = eINSTANCE.getVocabulary_Categories();

    /**
     * The meta object literal for the '<em><b>Sources Of Definition</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VOCABULARY__SOURCES_OF_DEFINITION = eINSTANCE.getVocabulary_SourcesOfDefinition();

    /**
     * The meta object literal for the '{@link org.eclipse.opencert.vocabulary.impl.CategoryImpl <em>Category</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.eclipse.opencert.vocabulary.impl.CategoryImpl
     * @see org.eclipse.opencert.vocabulary.impl.VocabularyPackageImpl#getCategory()
     * @generated
     */
    EClass CATEGORY = eINSTANCE.getCategory();

    /**
     * The meta object literal for the '<em><b>Terms</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CATEGORY__TERMS = eINSTANCE.getCategory_Terms();

    /**
     * The meta object literal for the '<em><b>Sub Categories</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CATEGORY__SUB_CATEGORIES = eINSTANCE.getCategory_SubCategories();

    /**
     * The meta object literal for the '{@link org.eclipse.opencert.vocabulary.impl.TermImpl <em>Term</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.eclipse.opencert.vocabulary.impl.TermImpl
     * @see org.eclipse.opencert.vocabulary.impl.VocabularyPackageImpl#getTerm()
     * @generated
     */
    EClass TERM = eINSTANCE.getTerm();

    /**
     * The meta object literal for the '<em><b>Definitions</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TERM__DEFINITIONS = eINSTANCE.getTerm_Definitions();

    /**
     * The meta object literal for the '<em><b>Notes</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TERM__NOTES = eINSTANCE.getTerm_Notes();

    /**
     * The meta object literal for the '<em><b>Examples</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TERM__EXAMPLES = eINSTANCE.getTerm_Examples();

    /**
     * The meta object literal for the '<em><b>Synonyms</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TERM__SYNONYMS = eINSTANCE.getTerm_Synonyms();

    /**
     * The meta object literal for the '<em><b>Defined By</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TERM__DEFINED_BY = eINSTANCE.getTerm_DefinedBy();

    /**
     * The meta object literal for the '<em><b>Is A</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TERM__IS_A = eINSTANCE.getTerm_IsA();

    /**
     * The meta object literal for the '<em><b>Has A</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TERM__HAS_A = eINSTANCE.getTerm_HasA();

    /**
     * The meta object literal for the '<em><b>Refers To</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TERM__REFERS_TO = eINSTANCE.getTerm_RefersTo();

    /**
     * The meta object literal for the '{@link org.eclipse.opencert.vocabulary.impl.SourceOfDefinitionImpl <em>Source Of Definition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.eclipse.opencert.vocabulary.impl.SourceOfDefinitionImpl
     * @see org.eclipse.opencert.vocabulary.impl.VocabularyPackageImpl#getSourceOfDefinition()
     * @generated
     */
    EClass SOURCE_OF_DEFINITION = eINSTANCE.getSourceOfDefinition();

    /**
     * The meta object literal for the '<em><b>Uri</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SOURCE_OF_DEFINITION__URI = eINSTANCE.getSourceOfDefinition_Uri();

    /**
     * The meta object literal for the '{@link org.eclipse.opencert.vocabulary.VocabularyElement <em>Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.eclipse.opencert.vocabulary.VocabularyElement
     * @see org.eclipse.opencert.vocabulary.impl.VocabularyPackageImpl#getVocabularyElement()
     * @generated
     */
    EClass VOCABULARY_ELEMENT = eINSTANCE.getVocabularyElement();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VOCABULARY_ELEMENT__NAME = eINSTANCE.getVocabularyElement_Name();

    /**
     * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VOCABULARY_ELEMENT__DESCRIPTION = eINSTANCE.getVocabularyElement_Description();

  }

} //VocabularyPackage
