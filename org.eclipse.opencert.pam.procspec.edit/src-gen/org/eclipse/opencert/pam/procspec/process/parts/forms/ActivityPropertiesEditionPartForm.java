/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pam.procspec.process.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;

import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.FormUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;

import org.eclipse.emf.eef.runtime.ui.widgets.TabElementTreeSelectionDialog;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart;
import org.eclipse.opencert.pam.procspec.process.parts.ProcessViewsRepository;

import org.eclipse.opencert.pam.procspec.process.providers.ProcessMessages;

// End of user code

/**
 * 
 * 
 */
public class ActivityPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, ActivityPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected Text startTime;
	protected Text endTime;
	protected ReferencesTable subActivity;
	protected List<ViewerFilter> subActivityBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> subActivityFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable precedingActivity;
	protected List<ViewerFilter> precedingActivityBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> precedingActivityFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable ownedRel;
	protected List<ViewerFilter> ownedRelBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedRelFilters = new ArrayList<ViewerFilter>();



	/**
	 * For {@link ISection} use only.
	 */
	public ActivityPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ActivityPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence activityStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = activityStep.addStep(ProcessViewsRepository.Activity.Properties.class);
		propertiesStep.addStep(ProcessViewsRepository.Activity.Properties.id);
		propertiesStep.addStep(ProcessViewsRepository.Activity.Properties.name);
		propertiesStep.addStep(ProcessViewsRepository.Activity.Properties.description);
		propertiesStep.addStep(ProcessViewsRepository.Activity.Properties.startTime);
		propertiesStep.addStep(ProcessViewsRepository.Activity.Properties.endTime);
		propertiesStep.addStep(ProcessViewsRepository.Activity.Properties.subActivity);
		propertiesStep.addStep(ProcessViewsRepository.Activity.Properties.precedingActivity);
		propertiesStep.addStep(ProcessViewsRepository.Activity.Properties.ownedRel);
		
		
		composer = new PartComposer(activityStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == ProcessViewsRepository.Activity.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.Activity.Properties.id) {
					return createIdText(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.Activity.Properties.name) {
					return createNameText(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.Activity.Properties.description) {
					return createDescriptionText(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.Activity.Properties.startTime) {
					return createStartTimeText(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.Activity.Properties.endTime) {
					return createEndTimeText(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.Activity.Properties.subActivity) {
					return createSubActivityTableComposition(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.Activity.Properties.precedingActivity) {
					return createPrecedingActivityReferencesTable(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.Activity.Properties.ownedRel) {
					return createOwnedRelTableComposition(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(ProcessMessages.ActivityPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	
	protected Composite createIdText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ProcessViewsRepository.Activity.Properties.id, ProcessMessages.ActivityPropertiesEditionPart_IdLabel);
		id = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		id.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ActivityPropertiesEditionPartForm.this,
							ProcessViewsRepository.Activity.Properties.id,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ActivityPropertiesEditionPartForm.this,
									ProcessViewsRepository.Activity.Properties.id,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, id.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ActivityPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		id.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityPropertiesEditionPartForm.this, ProcessViewsRepository.Activity.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}
		});
		EditingUtils.setID(id, ProcessViewsRepository.Activity.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Activity.Properties.id, ProcessViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ProcessViewsRepository.Activity.Properties.name, ProcessMessages.ActivityPropertiesEditionPart_NameLabel);
		name = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		name.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ActivityPropertiesEditionPartForm.this,
							ProcessViewsRepository.Activity.Properties.name,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ActivityPropertiesEditionPartForm.this,
									ProcessViewsRepository.Activity.Properties.name,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, name.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ActivityPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		name.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityPropertiesEditionPartForm.this, ProcessViewsRepository.Activity.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}
		});
		EditingUtils.setID(name, ProcessViewsRepository.Activity.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Activity.Properties.name, ProcessViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ProcessViewsRepository.Activity.Properties.description, ProcessMessages.ActivityPropertiesEditionPart_DescriptionLabel);
		description = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		description.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ActivityPropertiesEditionPartForm.this,
							ProcessViewsRepository.Activity.Properties.description,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ActivityPropertiesEditionPartForm.this,
									ProcessViewsRepository.Activity.Properties.description,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, description.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ActivityPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		description.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityPropertiesEditionPartForm.this, ProcessViewsRepository.Activity.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
				}
			}
		});
		EditingUtils.setID(description, ProcessViewsRepository.Activity.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Activity.Properties.description, ProcessViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionText

		// End of user code
		return parent;
	}

	
	protected Composite createStartTimeText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ProcessViewsRepository.Activity.Properties.startTime, ProcessMessages.ActivityPropertiesEditionPart_StartTimeLabel);
		startTime = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		startTime.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData startTimeData = new GridData(GridData.FILL_HORIZONTAL);
		startTime.setLayoutData(startTimeData);
		startTime.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ActivityPropertiesEditionPartForm.this,
							ProcessViewsRepository.Activity.Properties.startTime,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, startTime.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ActivityPropertiesEditionPartForm.this,
									ProcessViewsRepository.Activity.Properties.startTime,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, startTime.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ActivityPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		startTime.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityPropertiesEditionPartForm.this, ProcessViewsRepository.Activity.Properties.startTime, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, startTime.getText()));
				}
			}
		});
		EditingUtils.setID(startTime, ProcessViewsRepository.Activity.Properties.startTime);
		EditingUtils.setEEFtype(startTime, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Activity.Properties.startTime, ProcessViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createStartTimeText

		// End of user code
		return parent;
	}

	
	protected Composite createEndTimeText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ProcessViewsRepository.Activity.Properties.endTime, ProcessMessages.ActivityPropertiesEditionPart_EndTimeLabel);
		endTime = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		endTime.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData endTimeData = new GridData(GridData.FILL_HORIZONTAL);
		endTime.setLayoutData(endTimeData);
		endTime.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							ActivityPropertiesEditionPartForm.this,
							ProcessViewsRepository.Activity.Properties.endTime,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, endTime.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ActivityPropertiesEditionPartForm.this,
									ProcessViewsRepository.Activity.Properties.endTime,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, endTime.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									ActivityPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		endTime.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityPropertiesEditionPartForm.this, ProcessViewsRepository.Activity.Properties.endTime, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, endTime.getText()));
				}
			}
		});
		EditingUtils.setID(endTime, ProcessViewsRepository.Activity.Properties.endTime);
		EditingUtils.setEEFtype(endTime, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Activity.Properties.endTime, ProcessViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createEndTimeText

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createSubActivityTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.subActivity = new ReferencesTable(getDescription(ProcessViewsRepository.Activity.Properties.subActivity, ProcessMessages.ActivityPropertiesEditionPart_SubActivityLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityPropertiesEditionPartForm.this, ProcessViewsRepository.Activity.Properties.subActivity, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				subActivity.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityPropertiesEditionPartForm.this, ProcessViewsRepository.Activity.Properties.subActivity, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				subActivity.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityPropertiesEditionPartForm.this, ProcessViewsRepository.Activity.Properties.subActivity, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				subActivity.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityPropertiesEditionPartForm.this, ProcessViewsRepository.Activity.Properties.subActivity, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				subActivity.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.subActivityFilters) {
			this.subActivity.addFilter(filter);
		}
		this.subActivity.setHelpText(propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Activity.Properties.subActivity, ProcessViewsRepository.FORM_KIND));
		this.subActivity.createControls(parent, widgetFactory);
		this.subActivity.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityPropertiesEditionPartForm.this, ProcessViewsRepository.Activity.Properties.subActivity, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData subActivityData = new GridData(GridData.FILL_HORIZONTAL);
		subActivityData.horizontalSpan = 3;
		this.subActivity.setLayoutData(subActivityData);
		this.subActivity.setLowerBound(0);
		this.subActivity.setUpperBound(-1);
		subActivity.setID(ProcessViewsRepository.Activity.Properties.subActivity);
		subActivity.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createSubActivityTableComposition

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected Composite createPrecedingActivityReferencesTable(FormToolkit widgetFactory, Composite parent) {
		this.precedingActivity = new ReferencesTable(getDescription(ProcessViewsRepository.Activity.Properties.precedingActivity, ProcessMessages.ActivityPropertiesEditionPart_PrecedingActivityLabel), new ReferencesTableListener	() {
			public void handleAdd() { addPrecedingActivity(); }
			public void handleEdit(EObject element) { editPrecedingActivity(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { movePrecedingActivity(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromPrecedingActivity(element); }
			public void navigateTo(EObject element) { }
		});
		this.precedingActivity.setHelpText(propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Activity.Properties.precedingActivity, ProcessViewsRepository.FORM_KIND));
		this.precedingActivity.createControls(parent, widgetFactory);
		this.precedingActivity.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityPropertiesEditionPartForm.this, ProcessViewsRepository.Activity.Properties.precedingActivity, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData precedingActivityData = new GridData(GridData.FILL_HORIZONTAL);
		precedingActivityData.horizontalSpan = 3;
		this.precedingActivity.setLayoutData(precedingActivityData);
		this.precedingActivity.disableMove();
		precedingActivity.setID(ProcessViewsRepository.Activity.Properties.precedingActivity);
		precedingActivity.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		// Start of user code for createPrecedingActivityReferencesTable

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected void addPrecedingActivity() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(precedingActivity.getInput(), precedingActivityFilters, precedingActivityBusinessFilters,
		"precedingActivity", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityPropertiesEditionPartForm.this, ProcessViewsRepository.Activity.Properties.precedingActivity,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				precedingActivity.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void movePrecedingActivity(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityPropertiesEditionPartForm.this, ProcessViewsRepository.Activity.Properties.precedingActivity, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		precedingActivity.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromPrecedingActivity(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityPropertiesEditionPartForm.this, ProcessViewsRepository.Activity.Properties.precedingActivity, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		precedingActivity.refresh();
	}

	/**
	 * 
	 */
	protected void editPrecedingActivity(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				precedingActivity.refresh();
			}
		}
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createOwnedRelTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.ownedRel = new ReferencesTable(getDescription(ProcessViewsRepository.Activity.Properties.ownedRel, ProcessMessages.ActivityPropertiesEditionPart_OwnedRelLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityPropertiesEditionPartForm.this, ProcessViewsRepository.Activity.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				ownedRel.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityPropertiesEditionPartForm.this, ProcessViewsRepository.Activity.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				ownedRel.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityPropertiesEditionPartForm.this, ProcessViewsRepository.Activity.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				ownedRel.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityPropertiesEditionPartForm.this, ProcessViewsRepository.Activity.Properties.ownedRel, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				ownedRel.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.ownedRelFilters) {
			this.ownedRel.addFilter(filter);
		}
		this.ownedRel.setHelpText(propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Activity.Properties.ownedRel, ProcessViewsRepository.FORM_KIND));
		this.ownedRel.createControls(parent, widgetFactory);
		this.ownedRel.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ActivityPropertiesEditionPartForm.this, ProcessViewsRepository.Activity.Properties.ownedRel, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedRelData = new GridData(GridData.FILL_HORIZONTAL);
		ownedRelData.horizontalSpan = 3;
		this.ownedRel.setLayoutData(ownedRelData);
		this.ownedRel.setLowerBound(0);
		this.ownedRel.setUpperBound(-1);
		ownedRel.setID(ProcessViewsRepository.Activity.Properties.ownedRel);
		ownedRel.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createOwnedRelTableComposition

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Activity.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(ProcessMessages.Activity_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Activity.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(ProcessMessages.Activity_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Activity.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setToolTipText(ProcessMessages.Activity_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#getStartTime()
	 * 
	 */
	public String getStartTime() {
		return startTime.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#setStartTime(String newValue)
	 * 
	 */
	public void setStartTime(String newValue) {
		if (newValue != null) {
			startTime.setText(newValue);
		} else {
			startTime.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Activity.Properties.startTime);
		if (eefElementEditorReadOnlyState && startTime.isEnabled()) {
			startTime.setEnabled(false);
			startTime.setToolTipText(ProcessMessages.Activity_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !startTime.isEnabled()) {
			startTime.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#getEndTime()
	 * 
	 */
	public String getEndTime() {
		return endTime.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#setEndTime(String newValue)
	 * 
	 */
	public void setEndTime(String newValue) {
		if (newValue != null) {
			endTime.setText(newValue);
		} else {
			endTime.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Activity.Properties.endTime);
		if (eefElementEditorReadOnlyState && endTime.isEnabled()) {
			endTime.setEnabled(false);
			endTime.setToolTipText(ProcessMessages.Activity_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !endTime.isEnabled()) {
			endTime.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#initSubActivity(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initSubActivity(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		subActivity.setContentProvider(contentProvider);
		subActivity.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Activity.Properties.subActivity);
		if (eefElementEditorReadOnlyState && subActivity.isEnabled()) {
			subActivity.setEnabled(false);
			subActivity.setToolTipText(ProcessMessages.Activity_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !subActivity.isEnabled()) {
			subActivity.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#updateSubActivity()
	 * 
	 */
	public void updateSubActivity() {
	subActivity.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#addFilterSubActivity(ViewerFilter filter)
	 * 
	 */
	public void addFilterToSubActivity(ViewerFilter filter) {
		subActivityFilters.add(filter);
		if (this.subActivity != null) {
			this.subActivity.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#addBusinessFilterSubActivity(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToSubActivity(ViewerFilter filter) {
		subActivityBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#isContainedInSubActivityTable(EObject element)
	 * 
	 */
	public boolean isContainedInSubActivityTable(EObject element) {
		return ((ReferencesTableSettings)subActivity.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#initPrecedingActivity(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initPrecedingActivity(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		precedingActivity.setContentProvider(contentProvider);
		precedingActivity.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Activity.Properties.precedingActivity);
		if (eefElementEditorReadOnlyState && precedingActivity.getTable().isEnabled()) {
			precedingActivity.setEnabled(false);
			precedingActivity.setToolTipText(ProcessMessages.Activity_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !precedingActivity.getTable().isEnabled()) {
			precedingActivity.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#updatePrecedingActivity()
	 * 
	 */
	public void updatePrecedingActivity() {
	precedingActivity.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#addFilterPrecedingActivity(ViewerFilter filter)
	 * 
	 */
	public void addFilterToPrecedingActivity(ViewerFilter filter) {
		precedingActivityFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#addBusinessFilterPrecedingActivity(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToPrecedingActivity(ViewerFilter filter) {
		precedingActivityBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#isContainedInPrecedingActivityTable(EObject element)
	 * 
	 */
	public boolean isContainedInPrecedingActivityTable(EObject element) {
		return ((ReferencesTableSettings)precedingActivity.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#initOwnedRel(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initOwnedRel(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedRel.setContentProvider(contentProvider);
		ownedRel.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Activity.Properties.ownedRel);
		if (eefElementEditorReadOnlyState && ownedRel.isEnabled()) {
			ownedRel.setEnabled(false);
			ownedRel.setToolTipText(ProcessMessages.Activity_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedRel.isEnabled()) {
			ownedRel.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#updateOwnedRel()
	 * 
	 */
	public void updateOwnedRel() {
	ownedRel.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#addFilterOwnedRel(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedRel(ViewerFilter filter) {
		ownedRelFilters.add(filter);
		if (this.ownedRel != null) {
			this.ownedRel.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#addBusinessFilterOwnedRel(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedRel(ViewerFilter filter) {
		ownedRelBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart#isContainedInOwnedRelTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedRelTable(EObject element) {
		return ((ReferencesTableSettings)ownedRel.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return ProcessMessages.Activity_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
