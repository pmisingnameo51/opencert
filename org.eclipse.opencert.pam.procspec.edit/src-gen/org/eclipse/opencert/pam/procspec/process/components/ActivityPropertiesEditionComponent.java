/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pam.procspec.process.components;

// Start of user code for imports

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.opencert.pam.procspec.process.Activity;

import org.eclipse.opencert.pam.procspec.process.parts.ActivityArtefactsPropertiesEditionPart;
import org.eclipse.opencert.pam.procspec.process.parts.ActivityAssuranceAssetPropertiesEditionPart;
import org.eclipse.opencert.pam.procspec.process.parts.ActivityEvaluationPropertiesEditionPart;
import org.eclipse.opencert.pam.procspec.process.parts.ActivityEventsPropertiesEditionPart;
import org.eclipse.opencert.pam.procspec.process.parts.ActivityParticipantsPropertiesEditionPart;
import org.eclipse.opencert.pam.procspec.process.parts.ActivityPropertiesEditionPart;
import org.eclipse.opencert.pam.procspec.process.parts.ActivityTechniquesPropertiesEditionPart;
import org.eclipse.opencert.pam.procspec.process.parts.ProcessViewsRepository;

// End of user code

/**
 * 
 * 
 */
public class ActivityPropertiesEditionComponent extends ComposedPropertiesEditionComponent {

	/**
	 * The Base part
	 * 
	 */
	private ActivityPropertiesEditionPart basePart;

	/**
	 * The ActivityBasePropertiesEditionComponent sub component
	 * 
	 */
	protected ActivityBasePropertiesEditionComponent activityBasePropertiesEditionComponent;

	/**
	 * The Activity Artefacts part
	 * 
	 */
	private ActivityArtefactsPropertiesEditionPart activityArtefactsPart;

	/**
	 * The ActivityActivityArtefactsPropertiesEditionComponent sub component
	 * 
	 */
	protected ActivityActivityArtefactsPropertiesEditionComponent activityActivityArtefactsPropertiesEditionComponent;

	/**
	 * The Activity Participants part
	 * 
	 */
	private ActivityParticipantsPropertiesEditionPart activityParticipantsPart;

	/**
	 * The ActivityActivityParticipantsPropertiesEditionComponent sub component
	 * 
	 */
	protected ActivityActivityParticipantsPropertiesEditionComponent activityActivityParticipantsPropertiesEditionComponent;

	/**
	 * The Activity Techniques part
	 * 
	 */
	private ActivityTechniquesPropertiesEditionPart activityTechniquesPart;

	/**
	 * The ActivityActivityTechniquesPropertiesEditionComponent sub component
	 * 
	 */
	protected ActivityActivityTechniquesPropertiesEditionComponent activityActivityTechniquesPropertiesEditionComponent;

	/**
	 * The Activity Evaluation part
	 * 
	 */
	private ActivityEvaluationPropertiesEditionPart activityEvaluationPart;

	/**
	 * The ActivityActivityEvaluationPropertiesEditionComponent sub component
	 * 
	 */
	protected ActivityActivityEvaluationPropertiesEditionComponent activityActivityEvaluationPropertiesEditionComponent;

	/**
	 * The Activity Events part
	 * 
	 */
	private ActivityEventsPropertiesEditionPart activityEventsPart;

	/**
	 * The ActivityActivityEventsPropertiesEditionComponent sub component
	 * 
	 */
	protected ActivityActivityEventsPropertiesEditionComponent activityActivityEventsPropertiesEditionComponent;

	/**
	 * The Activity AssuranceAsset part
	 * 
	 */
	private ActivityAssuranceAssetPropertiesEditionPart activityAssuranceAssetPart;

	/**
	 * The ActivityActivityAssuranceAssetPropertiesEditionComponent sub component
	 * 
	 */
	protected ActivityActivityAssuranceAssetPropertiesEditionComponent activityActivityAssuranceAssetPropertiesEditionComponent;

	/**
	 * Parameterized constructor
	 * 
	 * @param activity the EObject to edit
	 * 
	 */
	public ActivityPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject activity, String editing_mode) {
		super(editingContext, editing_mode);
		if (activity instanceof Activity) {
			PropertiesEditingProvider provider = null;
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(activity, PropertiesEditingProvider.class);
			activityBasePropertiesEditionComponent = (ActivityBasePropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, ActivityBasePropertiesEditionComponent.BASE_PART, ActivityBasePropertiesEditionComponent.class);
			addSubComponent(activityBasePropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(activity, PropertiesEditingProvider.class);
			activityActivityArtefactsPropertiesEditionComponent = (ActivityActivityArtefactsPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, ActivityActivityArtefactsPropertiesEditionComponent.ACTIVITYARTEFACTS_PART, ActivityActivityArtefactsPropertiesEditionComponent.class);
			addSubComponent(activityActivityArtefactsPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(activity, PropertiesEditingProvider.class);
			activityActivityParticipantsPropertiesEditionComponent = (ActivityActivityParticipantsPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, ActivityActivityParticipantsPropertiesEditionComponent.ACTIVITYPARTICIPANTS_PART, ActivityActivityParticipantsPropertiesEditionComponent.class);
			addSubComponent(activityActivityParticipantsPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(activity, PropertiesEditingProvider.class);
			activityActivityTechniquesPropertiesEditionComponent = (ActivityActivityTechniquesPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, ActivityActivityTechniquesPropertiesEditionComponent.ACTIVITYTECHNIQUES_PART, ActivityActivityTechniquesPropertiesEditionComponent.class);
			addSubComponent(activityActivityTechniquesPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(activity, PropertiesEditingProvider.class);
			activityActivityEvaluationPropertiesEditionComponent = (ActivityActivityEvaluationPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, ActivityActivityEvaluationPropertiesEditionComponent.ACTIVITYEVALUATION_PART, ActivityActivityEvaluationPropertiesEditionComponent.class);
			addSubComponent(activityActivityEvaluationPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(activity, PropertiesEditingProvider.class);
			activityActivityEventsPropertiesEditionComponent = (ActivityActivityEventsPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, ActivityActivityEventsPropertiesEditionComponent.ACTIVITYEVENTS_PART, ActivityActivityEventsPropertiesEditionComponent.class);
			addSubComponent(activityActivityEventsPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(activity, PropertiesEditingProvider.class);
			activityActivityAssuranceAssetPropertiesEditionComponent = (ActivityActivityAssuranceAssetPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, ActivityActivityAssuranceAssetPropertiesEditionComponent.ACTIVITYASSURANCEASSET_PART, ActivityActivityAssuranceAssetPropertiesEditionComponent.class);
			addSubComponent(activityActivityAssuranceAssetPropertiesEditionComponent);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      getPropertiesEditionPart(int, java.lang.String)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(int kind, String key) {
		if (ActivityBasePropertiesEditionComponent.BASE_PART.equals(key)) {
			basePart = (ActivityPropertiesEditionPart)activityBasePropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)basePart;
		}
		if (ActivityActivityArtefactsPropertiesEditionComponent.ACTIVITYARTEFACTS_PART.equals(key)) {
			activityArtefactsPart = (ActivityArtefactsPropertiesEditionPart)activityActivityArtefactsPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)activityArtefactsPart;
		}
		if (ActivityActivityParticipantsPropertiesEditionComponent.ACTIVITYPARTICIPANTS_PART.equals(key)) {
			activityParticipantsPart = (ActivityParticipantsPropertiesEditionPart)activityActivityParticipantsPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)activityParticipantsPart;
		}
		if (ActivityActivityTechniquesPropertiesEditionComponent.ACTIVITYTECHNIQUES_PART.equals(key)) {
			activityTechniquesPart = (ActivityTechniquesPropertiesEditionPart)activityActivityTechniquesPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)activityTechniquesPart;
		}
		if (ActivityActivityEvaluationPropertiesEditionComponent.ACTIVITYEVALUATION_PART.equals(key)) {
			activityEvaluationPart = (ActivityEvaluationPropertiesEditionPart)activityActivityEvaluationPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)activityEvaluationPart;
		}
		if (ActivityActivityEventsPropertiesEditionComponent.ACTIVITYEVENTS_PART.equals(key)) {
			activityEventsPart = (ActivityEventsPropertiesEditionPart)activityActivityEventsPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)activityEventsPart;
		}
		if (ActivityActivityAssuranceAssetPropertiesEditionComponent.ACTIVITYASSURANCEASSET_PART.equals(key)) {
			activityAssuranceAssetPart = (ActivityAssuranceAssetPropertiesEditionPart)activityActivityAssuranceAssetPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)activityAssuranceAssetPart;
		}
		return super.getPropertiesEditionPart(kind, key);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      setPropertiesEditionPart(java.lang.Object, int,
	 *      org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart)
	 * 
	 */
	public void setPropertiesEditionPart(java.lang.Object key, int kind, IPropertiesEditionPart propertiesEditionPart) {
		if (ProcessViewsRepository.Activity.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			basePart = (ActivityPropertiesEditionPart)propertiesEditionPart;
		}
		if (ProcessViewsRepository.ActivityArtefacts.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			activityArtefactsPart = (ActivityArtefactsPropertiesEditionPart)propertiesEditionPart;
		}
		if (ProcessViewsRepository.ActivityParticipants.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			activityParticipantsPart = (ActivityParticipantsPropertiesEditionPart)propertiesEditionPart;
		}
		if (ProcessViewsRepository.ActivityTechniques.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			activityTechniquesPart = (ActivityTechniquesPropertiesEditionPart)propertiesEditionPart;
		}
		if (ProcessViewsRepository.ActivityEvaluation.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			activityEvaluationPart = (ActivityEvaluationPropertiesEditionPart)propertiesEditionPart;
		}
		if (ProcessViewsRepository.ActivityEvents.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			activityEventsPart = (ActivityEventsPropertiesEditionPart)propertiesEditionPart;
		}
		if (ProcessViewsRepository.ActivityAssuranceAsset.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			activityAssuranceAssetPart = (ActivityAssuranceAssetPropertiesEditionPart)propertiesEditionPart;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(java.lang.Object key, int kind, EObject element, ResourceSet allResource) {
		if (key == ProcessViewsRepository.Activity.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == ProcessViewsRepository.ActivityArtefacts.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == ProcessViewsRepository.ActivityParticipants.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == ProcessViewsRepository.ActivityTechniques.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == ProcessViewsRepository.ActivityEvaluation.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == ProcessViewsRepository.ActivityEvents.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == ProcessViewsRepository.ActivityAssuranceAsset.class) {
			super.initPart(key, kind, element, allResource);
		}
	}
}
