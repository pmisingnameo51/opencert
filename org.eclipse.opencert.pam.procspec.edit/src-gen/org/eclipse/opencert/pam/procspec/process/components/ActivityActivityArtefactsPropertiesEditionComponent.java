/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pam.procspec.process.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.ecore.util.EcoreUtil;

import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;

import org.eclipse.emf.eef.runtime.impl.filters.EObjectFilter;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage;

import org.eclipse.opencert.pam.procspec.process.Activity;
import org.eclipse.opencert.pam.procspec.process.ProcessPackage;

import org.eclipse.opencert.pam.procspec.process.parts.ActivityArtefactsPropertiesEditionPart;
import org.eclipse.opencert.pam.procspec.process.parts.ProcessViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class ActivityActivityArtefactsPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String ACTIVITYARTEFACTS_PART = "Activity Artefacts"; //$NON-NLS-1$

	
	/**
	 * Settings for requiredArtefact ReferencesTable
	 */
	private ReferencesTableSettings requiredArtefactSettings;
	
	/**
	 * Settings for producedArtefact ReferencesTable
	 */
	private ReferencesTableSettings producedArtefactSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public ActivityActivityArtefactsPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject activity, String editing_mode) {
		super(editingContext, activity, editing_mode);
		parts = new String[] { ACTIVITYARTEFACTS_PART };
		repositoryKey = ProcessViewsRepository.class;
		partKey = ProcessViewsRepository.ActivityArtefacts.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final Activity activity = (Activity)elt;
			final ActivityArtefactsPropertiesEditionPart activityArtefactsPart = (ActivityArtefactsPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(ProcessViewsRepository.ActivityArtefacts.Properties.requiredArtefact)) {
				requiredArtefactSettings = new ReferencesTableSettings(activity, ProcessPackage.eINSTANCE.getActivity_RequiredArtefact());
				activityArtefactsPart.initRequiredArtefact(requiredArtefactSettings);
			}
			if (isAccessible(ProcessViewsRepository.ActivityArtefacts.Properties.producedArtefact)) {
				producedArtefactSettings = new ReferencesTableSettings(activity, ProcessPackage.eINSTANCE.getActivity_ProducedArtefact());
				activityArtefactsPart.initProducedArtefact(producedArtefactSettings);
			}
			// init filters
			if (isAccessible(ProcessViewsRepository.ActivityArtefacts.Properties.requiredArtefact)) {
				activityArtefactsPart.addFilterToRequiredArtefact(new EObjectFilter(EvidencePackage.Literals.ARTEFACT));
				// Start of user code for additional businessfilters for requiredArtefact
				// End of user code
			}
			if (isAccessible(ProcessViewsRepository.ActivityArtefacts.Properties.producedArtefact)) {
				activityArtefactsPart.addFilterToProducedArtefact(new EObjectFilter(EvidencePackage.Literals.ARTEFACT));
				// Start of user code for additional businessfilters for producedArtefact
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}





	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == ProcessViewsRepository.ActivityArtefacts.Properties.requiredArtefact) {
			return ProcessPackage.eINSTANCE.getActivity_RequiredArtefact();
		}
		if (editorKey == ProcessViewsRepository.ActivityArtefacts.Properties.producedArtefact) {
			return ProcessPackage.eINSTANCE.getActivity_ProducedArtefact();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		Activity activity = (Activity)semanticObject;
		if (ProcessViewsRepository.ActivityArtefacts.Properties.requiredArtefact == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof Artefact) {
					requiredArtefactSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				requiredArtefactSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				requiredArtefactSettings.move(event.getNewIndex(), (Artefact) event.getNewValue());
			}
		}
		if (ProcessViewsRepository.ActivityArtefacts.Properties.producedArtefact == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof Artefact) {
					producedArtefactSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				producedArtefactSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				producedArtefactSettings.move(event.getNewIndex(), (Artefact) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			ActivityArtefactsPropertiesEditionPart activityArtefactsPart = (ActivityArtefactsPropertiesEditionPart)editingPart;
			if (ProcessPackage.eINSTANCE.getActivity_RequiredArtefact().equals(msg.getFeature())  && isAccessible(ProcessViewsRepository.ActivityArtefacts.Properties.requiredArtefact))
				activityArtefactsPart.updateRequiredArtefact();
			if (ProcessPackage.eINSTANCE.getActivity_ProducedArtefact().equals(msg.getFeature())  && isAccessible(ProcessViewsRepository.ActivityArtefacts.Properties.producedArtefact))
				activityArtefactsPart.updateProducedArtefact();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			ProcessPackage.eINSTANCE.getActivity_RequiredArtefact(),
			ProcessPackage.eINSTANCE.getActivity_ProducedArtefact()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
