/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pam.procspec.process.providers;

import org.eclipse.osgi.util.NLS;

/**
 * 
 * 
 */
public class ProcessMessages extends NLS {
	
	private static final String BUNDLE_NAME = "org.eclipse.opencert.pam.procspec.process.providers.processMessages"; //$NON-NLS-1$

	
	public static String ProcessModelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ActivityPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ParticipantPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String TechniquePropertiesEditionPart_PropertiesGroupLabel;

	
	public static String PersonPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ToolPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String OrganizationPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ActivityRelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ActivityArtefactsPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ActivityParticipantsPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ActivityTechniquesPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ActivityEvaluationPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ActivityEventsPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ActivityAssuranceAssetPropertiesEditionPart_PropertiesGroupLabel;


	
	public static String ProcessModel_ReadOnly;

	
	public static String ProcessModel_Part_Title;

	
	public static String Activity_ReadOnly;

	
	public static String Activity_Part_Title;

	
	public static String Participant_ReadOnly;

	
	public static String Participant_Part_Title;

	
	public static String Technique_ReadOnly;

	
	public static String Technique_Part_Title;

	
	public static String Person_ReadOnly;

	
	public static String Person_Part_Title;

	
	public static String Tool_ReadOnly;

	
	public static String Tool_Part_Title;

	
	public static String Organization_ReadOnly;

	
	public static String Organization_Part_Title;

	
	public static String ActivityRel_ReadOnly;

	
	public static String ActivityRel_Part_Title;

	
	public static String ActivityArtefacts_ReadOnly;

	
	public static String ActivityArtefacts_Part_Title;

	
	public static String ActivityParticipants_ReadOnly;

	
	public static String ActivityParticipants_Part_Title;

	
	public static String ActivityTechniques_ReadOnly;

	
	public static String ActivityTechniques_Part_Title;

	
	public static String ActivityEvaluation_ReadOnly;

	
	public static String ActivityEvaluation_Part_Title;

	
	public static String ActivityEvents_ReadOnly;

	
	public static String ActivityEvents_Part_Title;

	
	public static String ActivityAssuranceAsset_ReadOnly;

	
	public static String ActivityAssuranceAsset_Part_Title;


	
	public static String ProcessModelPropertiesEditionPart_IdLabel;

	
	public static String ProcessModelPropertiesEditionPart_NameLabel;

	
	public static String ProcessModelPropertiesEditionPart_DescriptionLabel;

	
	public static String ProcessModelPropertiesEditionPart_OwnedActivityLabel;

	
	public static String ProcessModelPropertiesEditionPart_OwnedParticipantLabel;

	
	public static String ProcessModelPropertiesEditionPart_OwnedTechniqueLabel;

	
	public static String ActivityPropertiesEditionPart_IdLabel;

	
	public static String ActivityPropertiesEditionPart_NameLabel;

	
	public static String ActivityPropertiesEditionPart_DescriptionLabel;

	
	public static String ActivityPropertiesEditionPart_StartTimeLabel;

	
	public static String ActivityPropertiesEditionPart_EndTimeLabel;

	
	public static String ActivityPropertiesEditionPart_SubActivityLabel;

	
	public static String ActivityPropertiesEditionPart_PrecedingActivityLabel;

	
	public static String ActivityPropertiesEditionPart_OwnedRelLabel;

	
	public static String ParticipantPropertiesEditionPart_IdLabel;

	
	public static String ParticipantPropertiesEditionPart_NameLabel;

	
	public static String ParticipantPropertiesEditionPart_DescriptionLabel;

	
	public static String ParticipantPropertiesEditionPart_OwnedArtefactLabel;

	
	public static String ParticipantPropertiesEditionPart_TriggeredAssetEventLabel;

	
	public static String TechniquePropertiesEditionPart_IdLabel;

	
	public static String TechniquePropertiesEditionPart_NameLabel;

	
	public static String TechniquePropertiesEditionPart_DescriptionLabel;

	
	public static String TechniquePropertiesEditionPart_CreatedArtefactLabel;

	
	public static String PersonPropertiesEditionPart_IdLabel;

	
	public static String PersonPropertiesEditionPart_NameLabel;

	
	public static String PersonPropertiesEditionPart_DescriptionLabel;

	
	public static String PersonPropertiesEditionPart_OwnedArtefactLabel;

	
	public static String PersonPropertiesEditionPart_TriggeredAssetEventLabel;

	
	public static String PersonPropertiesEditionPart_EmailLabel;

	
	public static String PersonPropertiesEditionPart_OrganizationLabel;

	
	public static String ToolPropertiesEditionPart_IdLabel;

	
	public static String ToolPropertiesEditionPart_NameLabel;

	
	public static String ToolPropertiesEditionPart_DescriptionLabel;

	
	public static String ToolPropertiesEditionPart_OwnedArtefactLabel;

	
	public static String ToolPropertiesEditionPart_TriggeredAssetEventLabel;

	
	public static String ToolPropertiesEditionPart_VersionLabel;

	
	public static String OrganizationPropertiesEditionPart_IdLabel;

	
	public static String OrganizationPropertiesEditionPart_NameLabel;

	
	public static String OrganizationPropertiesEditionPart_DescriptionLabel;

	
	public static String OrganizationPropertiesEditionPart_OwnedArtefactLabel;

	
	public static String OrganizationPropertiesEditionPart_TriggeredAssetEventLabel;

	
	public static String OrganizationPropertiesEditionPart_AddressLabel;

	
	public static String OrganizationPropertiesEditionPart_SubOrganizationLabel;

	
	public static String ActivityRelPropertiesEditionPart_TargetLabel;

	
	public static String ActivityRelPropertiesEditionPart_SourceLabel;

	
	public static String ActivityRelPropertiesEditionPart_TypeLabel;

	
	public static String ActivityArtefactsPropertiesEditionPart_RequiredArtefactLabel;

	
	public static String ActivityArtefactsPropertiesEditionPart_ProducedArtefactLabel;

	
	public static String ActivityParticipantsPropertiesEditionPart_ParticipantLabel;

	
	public static String ActivityTechniquesPropertiesEditionPart_TechniqueLabel;

	
	public static String ActivityEvaluationPropertiesEditionPart_EvaluationLabel;

	
	public static String ActivityEventsPropertiesEditionPart_LifecycleEventLabel;

	
	public static String ActivityAssuranceAssetPropertiesEditionPart_AssetEventLabel;


	
	public static String PropertiesEditionPart_DocumentationLabel;

	
	public static String PropertiesEditionPart_IntegerValueMessage;

	
	public static String PropertiesEditionPart_FloatValueMessage;

	
	public static String PropertiesEditionPart_ShortValueMessage;

	
	public static String PropertiesEditionPart_LongValueMessage;

	
	public static String PropertiesEditionPart_ByteValueMessage;

	
	public static String PropertiesEditionPart_BigIntegerValueMessage;

	
	public static String PropertiesEditionPart_BigDecimalValueMessage;

	
	public static String PropertiesEditionPart_DoubleValueMessage;

	
	public static String PropertiesEditionPart_PropertiesGroupLabel;

	
	public static String PropertiesEditionPart_RequiredFeatureMessage;

	
	public static String PropertiesEditionPart_AddTableViewerLabel;

	
	public static String PropertiesEditionPart_EditTableViewerLabel;

	
	public static String PropertiesEditionPart_RemoveTableViewerLabel;

	
	public static String PropertiesEditionPart_AddListViewerLabel;

	
	public static String PropertiesEditionPart_RemoveListViewerLabel;

	// Start of user code for additionnal NLS Constants
	
	// End of user code

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, ProcessMessages.class);
	}

	
	private ProcessMessages() {
		//protect instanciation
	}
}
