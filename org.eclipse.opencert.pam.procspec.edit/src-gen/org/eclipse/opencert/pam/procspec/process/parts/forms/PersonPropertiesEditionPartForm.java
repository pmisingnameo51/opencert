/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pam.procspec.process.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;

import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.FormUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;

import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;

import org.eclipse.emf.eef.runtime.ui.widgets.TabElementTreeSelectionDialog;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart;
import org.eclipse.opencert.pam.procspec.process.parts.ProcessViewsRepository;

import org.eclipse.opencert.pam.procspec.process.providers.ProcessMessages;

// End of user code

/**
 * 
 * 
 */
public class PersonPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, PersonPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected ReferencesTable ownedArtefact;
	protected List<ViewerFilter> ownedArtefactBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> ownedArtefactFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable triggeredAssetEvent;
	protected List<ViewerFilter> triggeredAssetEventBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> triggeredAssetEventFilters = new ArrayList<ViewerFilter>();
	protected Text email;
	protected ReferencesTable organization;
	protected List<ViewerFilter> organizationBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> organizationFilters = new ArrayList<ViewerFilter>();



	/**
	 * For {@link ISection} use only.
	 */
	public PersonPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public PersonPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence personStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = personStep.addStep(ProcessViewsRepository.Person.Properties.class);
		propertiesStep.addStep(ProcessViewsRepository.Person.Properties.id);
		propertiesStep.addStep(ProcessViewsRepository.Person.Properties.name);
		propertiesStep.addStep(ProcessViewsRepository.Person.Properties.description);
		propertiesStep.addStep(ProcessViewsRepository.Person.Properties.ownedArtefact);
		propertiesStep.addStep(ProcessViewsRepository.Person.Properties.triggeredAssetEvent);
		propertiesStep.addStep(ProcessViewsRepository.Person.Properties.email);
		propertiesStep.addStep(ProcessViewsRepository.Person.Properties.organization);
		
		
		composer = new PartComposer(personStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == ProcessViewsRepository.Person.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.Person.Properties.id) {
					return createIdText(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.Person.Properties.name) {
					return createNameText(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.Person.Properties.description) {
					return createDescriptionText(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.Person.Properties.ownedArtefact) {
					return createOwnedArtefactReferencesTable(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.Person.Properties.triggeredAssetEvent) {
					return createTriggeredAssetEventReferencesTable(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.Person.Properties.email) {
					return createEmailText(widgetFactory, parent);
				}
				if (key == ProcessViewsRepository.Person.Properties.organization) {
					return createOrganizationReferencesTable(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(ProcessMessages.PersonPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	
	protected Composite createIdText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ProcessViewsRepository.Person.Properties.id, ProcessMessages.PersonPropertiesEditionPart_IdLabel);
		id = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		id.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							PersonPropertiesEditionPartForm.this,
							ProcessViewsRepository.Person.Properties.id,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									PersonPropertiesEditionPartForm.this,
									ProcessViewsRepository.Person.Properties.id,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, id.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									PersonPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		id.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PersonPropertiesEditionPartForm.this, ProcessViewsRepository.Person.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}
		});
		EditingUtils.setID(id, ProcessViewsRepository.Person.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Person.Properties.id, ProcessViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ProcessViewsRepository.Person.Properties.name, ProcessMessages.PersonPropertiesEditionPart_NameLabel);
		name = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		name.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							PersonPropertiesEditionPartForm.this,
							ProcessViewsRepository.Person.Properties.name,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									PersonPropertiesEditionPartForm.this,
									ProcessViewsRepository.Person.Properties.name,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, name.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									PersonPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		name.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PersonPropertiesEditionPartForm.this, ProcessViewsRepository.Person.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}
		});
		EditingUtils.setID(name, ProcessViewsRepository.Person.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Person.Properties.name, ProcessViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ProcessViewsRepository.Person.Properties.description, ProcessMessages.PersonPropertiesEditionPart_DescriptionLabel);
		description = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		description.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							PersonPropertiesEditionPartForm.this,
							ProcessViewsRepository.Person.Properties.description,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									PersonPropertiesEditionPartForm.this,
									ProcessViewsRepository.Person.Properties.description,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, description.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									PersonPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		description.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PersonPropertiesEditionPartForm.this, ProcessViewsRepository.Person.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
				}
			}
		});
		EditingUtils.setID(description, ProcessViewsRepository.Person.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Person.Properties.description, ProcessViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionText

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected Composite createOwnedArtefactReferencesTable(FormToolkit widgetFactory, Composite parent) {
		this.ownedArtefact = new ReferencesTable(getDescription(ProcessViewsRepository.Person.Properties.ownedArtefact, ProcessMessages.PersonPropertiesEditionPart_OwnedArtefactLabel), new ReferencesTableListener	() {
			public void handleAdd() { addOwnedArtefact(); }
			public void handleEdit(EObject element) { editOwnedArtefact(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveOwnedArtefact(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromOwnedArtefact(element); }
			public void navigateTo(EObject element) { }
		});
		this.ownedArtefact.setHelpText(propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Person.Properties.ownedArtefact, ProcessViewsRepository.FORM_KIND));
		this.ownedArtefact.createControls(parent, widgetFactory);
		this.ownedArtefact.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PersonPropertiesEditionPartForm.this, ProcessViewsRepository.Person.Properties.ownedArtefact, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData ownedArtefactData = new GridData(GridData.FILL_HORIZONTAL);
		ownedArtefactData.horizontalSpan = 3;
		this.ownedArtefact.setLayoutData(ownedArtefactData);
		this.ownedArtefact.disableMove();
		ownedArtefact.setID(ProcessViewsRepository.Person.Properties.ownedArtefact);
		ownedArtefact.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		// Start of user code for createOwnedArtefactReferencesTable

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected void addOwnedArtefact() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(ownedArtefact.getInput(), ownedArtefactFilters, ownedArtefactBusinessFilters,
		"ownedArtefact", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PersonPropertiesEditionPartForm.this, ProcessViewsRepository.Person.Properties.ownedArtefact,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				ownedArtefact.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveOwnedArtefact(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PersonPropertiesEditionPartForm.this, ProcessViewsRepository.Person.Properties.ownedArtefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		ownedArtefact.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromOwnedArtefact(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PersonPropertiesEditionPartForm.this, ProcessViewsRepository.Person.Properties.ownedArtefact, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		ownedArtefact.refresh();
	}

	/**
	 * 
	 */
	protected void editOwnedArtefact(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				ownedArtefact.refresh();
			}
		}
	}

	/**
	 * 
	 */
	protected Composite createTriggeredAssetEventReferencesTable(FormToolkit widgetFactory, Composite parent) {
		this.triggeredAssetEvent = new ReferencesTable(getDescription(ProcessViewsRepository.Person.Properties.triggeredAssetEvent, ProcessMessages.PersonPropertiesEditionPart_TriggeredAssetEventLabel), new ReferencesTableListener	() {
			public void handleAdd() { addTriggeredAssetEvent(); }
			public void handleEdit(EObject element) { editTriggeredAssetEvent(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveTriggeredAssetEvent(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromTriggeredAssetEvent(element); }
			public void navigateTo(EObject element) { }
		});
		this.triggeredAssetEvent.setHelpText(propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Person.Properties.triggeredAssetEvent, ProcessViewsRepository.FORM_KIND));
		this.triggeredAssetEvent.createControls(parent, widgetFactory);
		this.triggeredAssetEvent.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PersonPropertiesEditionPartForm.this, ProcessViewsRepository.Person.Properties.triggeredAssetEvent, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData triggeredAssetEventData = new GridData(GridData.FILL_HORIZONTAL);
		triggeredAssetEventData.horizontalSpan = 3;
		this.triggeredAssetEvent.setLayoutData(triggeredAssetEventData);
		this.triggeredAssetEvent.disableMove();
		triggeredAssetEvent.setID(ProcessViewsRepository.Person.Properties.triggeredAssetEvent);
		triggeredAssetEvent.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		// Start of user code for createTriggeredAssetEventReferencesTable

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected void addTriggeredAssetEvent() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(triggeredAssetEvent.getInput(), triggeredAssetEventFilters, triggeredAssetEventBusinessFilters,
		"triggeredAssetEvent", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PersonPropertiesEditionPartForm.this, ProcessViewsRepository.Person.Properties.triggeredAssetEvent,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				triggeredAssetEvent.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveTriggeredAssetEvent(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PersonPropertiesEditionPartForm.this, ProcessViewsRepository.Person.Properties.triggeredAssetEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		triggeredAssetEvent.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromTriggeredAssetEvent(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PersonPropertiesEditionPartForm.this, ProcessViewsRepository.Person.Properties.triggeredAssetEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		triggeredAssetEvent.refresh();
	}

	/**
	 * 
	 */
	protected void editTriggeredAssetEvent(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				triggeredAssetEvent.refresh();
			}
		}
	}

	
	protected Composite createEmailText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, ProcessViewsRepository.Person.Properties.email, ProcessMessages.PersonPropertiesEditionPart_EmailLabel);
		email = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		email.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData emailData = new GridData(GridData.FILL_HORIZONTAL);
		email.setLayoutData(emailData);
		email.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							PersonPropertiesEditionPartForm.this,
							ProcessViewsRepository.Person.Properties.email,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, email.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									PersonPropertiesEditionPartForm.this,
									ProcessViewsRepository.Person.Properties.email,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, email.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									PersonPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		email.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PersonPropertiesEditionPartForm.this, ProcessViewsRepository.Person.Properties.email, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, email.getText()));
				}
			}
		});
		EditingUtils.setID(email, ProcessViewsRepository.Person.Properties.email);
		EditingUtils.setEEFtype(email, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Person.Properties.email, ProcessViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createEmailText

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected Composite createOrganizationReferencesTable(FormToolkit widgetFactory, Composite parent) {
		this.organization = new ReferencesTable(getDescription(ProcessViewsRepository.Person.Properties.organization, ProcessMessages.PersonPropertiesEditionPart_OrganizationLabel), new ReferencesTableListener	() {
			public void handleAdd() { addOrganization(); }
			public void handleEdit(EObject element) { editOrganization(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveOrganization(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromOrganization(element); }
			public void navigateTo(EObject element) { }
		});
		this.organization.setHelpText(propertiesEditionComponent.getHelpContent(ProcessViewsRepository.Person.Properties.organization, ProcessViewsRepository.FORM_KIND));
		this.organization.createControls(parent, widgetFactory);
		this.organization.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PersonPropertiesEditionPartForm.this, ProcessViewsRepository.Person.Properties.organization, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData organizationData = new GridData(GridData.FILL_HORIZONTAL);
		organizationData.horizontalSpan = 3;
		this.organization.setLayoutData(organizationData);
		this.organization.disableMove();
		organization.setID(ProcessViewsRepository.Person.Properties.organization);
		organization.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		// Start of user code for createOrganizationReferencesTable

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected void addOrganization() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(organization.getInput(), organizationFilters, organizationBusinessFilters,
		"organization", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PersonPropertiesEditionPartForm.this, ProcessViewsRepository.Person.Properties.organization,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				organization.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveOrganization(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PersonPropertiesEditionPartForm.this, ProcessViewsRepository.Person.Properties.organization, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		organization.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromOrganization(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(PersonPropertiesEditionPartForm.this, ProcessViewsRepository.Person.Properties.organization, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		organization.refresh();
	}

	/**
	 * 
	 */
	protected void editOrganization(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				organization.refresh();
			}
		}
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Person.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(ProcessMessages.Person_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Person.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(ProcessMessages.Person_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Person.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setToolTipText(ProcessMessages.Person_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#initOwnedArtefact(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initOwnedArtefact(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		ownedArtefact.setContentProvider(contentProvider);
		ownedArtefact.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Person.Properties.ownedArtefact);
		if (eefElementEditorReadOnlyState && ownedArtefact.getTable().isEnabled()) {
			ownedArtefact.setEnabled(false);
			ownedArtefact.setToolTipText(ProcessMessages.Person_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !ownedArtefact.getTable().isEnabled()) {
			ownedArtefact.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#updateOwnedArtefact()
	 * 
	 */
	public void updateOwnedArtefact() {
	ownedArtefact.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#addFilterOwnedArtefact(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOwnedArtefact(ViewerFilter filter) {
		ownedArtefactFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#addBusinessFilterOwnedArtefact(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOwnedArtefact(ViewerFilter filter) {
		ownedArtefactBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#isContainedInOwnedArtefactTable(EObject element)
	 * 
	 */
	public boolean isContainedInOwnedArtefactTable(EObject element) {
		return ((ReferencesTableSettings)ownedArtefact.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#initTriggeredAssetEvent(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initTriggeredAssetEvent(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		triggeredAssetEvent.setContentProvider(contentProvider);
		triggeredAssetEvent.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Person.Properties.triggeredAssetEvent);
		if (eefElementEditorReadOnlyState && triggeredAssetEvent.getTable().isEnabled()) {
			triggeredAssetEvent.setEnabled(false);
			triggeredAssetEvent.setToolTipText(ProcessMessages.Person_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !triggeredAssetEvent.getTable().isEnabled()) {
			triggeredAssetEvent.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#updateTriggeredAssetEvent()
	 * 
	 */
	public void updateTriggeredAssetEvent() {
	triggeredAssetEvent.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#addFilterTriggeredAssetEvent(ViewerFilter filter)
	 * 
	 */
	public void addFilterToTriggeredAssetEvent(ViewerFilter filter) {
		triggeredAssetEventFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#addBusinessFilterTriggeredAssetEvent(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToTriggeredAssetEvent(ViewerFilter filter) {
		triggeredAssetEventBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#isContainedInTriggeredAssetEventTable(EObject element)
	 * 
	 */
	public boolean isContainedInTriggeredAssetEventTable(EObject element) {
		return ((ReferencesTableSettings)triggeredAssetEvent.getInput()).contains(element);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#getEmail()
	 * 
	 */
	public String getEmail() {
		return email.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#setEmail(String newValue)
	 * 
	 */
	public void setEmail(String newValue) {
		if (newValue != null) {
			email.setText(newValue);
		} else {
			email.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Person.Properties.email);
		if (eefElementEditorReadOnlyState && email.isEnabled()) {
			email.setEnabled(false);
			email.setToolTipText(ProcessMessages.Person_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !email.isEnabled()) {
			email.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#initOrganization(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initOrganization(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		organization.setContentProvider(contentProvider);
		organization.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(ProcessViewsRepository.Person.Properties.organization);
		if (eefElementEditorReadOnlyState && organization.getTable().isEnabled()) {
			organization.setEnabled(false);
			organization.setToolTipText(ProcessMessages.Person_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !organization.getTable().isEnabled()) {
			organization.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#updateOrganization()
	 * 
	 */
	public void updateOrganization() {
	organization.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#addFilterOrganization(ViewerFilter filter)
	 * 
	 */
	public void addFilterToOrganization(ViewerFilter filter) {
		organizationFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#addBusinessFilterOrganization(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToOrganization(ViewerFilter filter) {
		organizationBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pam.procspec.process.parts.PersonPropertiesEditionPart#isContainedInOrganizationTable(EObject element)
	 * 
	 */
	public boolean isContainedInOrganizationTable(EObject element) {
		return ((ReferencesTableSettings)organization.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return ProcessMessages.Person_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
