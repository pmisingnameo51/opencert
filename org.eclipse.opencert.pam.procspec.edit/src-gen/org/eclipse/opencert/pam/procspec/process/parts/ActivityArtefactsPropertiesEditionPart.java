/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pam.procspec.process.parts;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;


// End of user code

/**
 * 
 * 
 */
public interface ActivityArtefactsPropertiesEditionPart {



	/**
	 * Init the requiredArtefact
	 * @param settings settings for the requiredArtefact ReferencesTable 
	 */
	public void initRequiredArtefact(ReferencesTableSettings settings);

	/**
	 * Update the requiredArtefact
	 * @param newValue the requiredArtefact to update
	 * 
	 */
	public void updateRequiredArtefact();

	/**
	 * Adds the given filter to the requiredArtefact edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToRequiredArtefact(ViewerFilter filter);

	/**
	 * Adds the given filter to the requiredArtefact edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToRequiredArtefact(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the requiredArtefact table
	 * 
	 */
	public boolean isContainedInRequiredArtefactTable(EObject element);




	/**
	 * Init the producedArtefact
	 * @param settings settings for the producedArtefact ReferencesTable 
	 */
	public void initProducedArtefact(ReferencesTableSettings settings);

	/**
	 * Update the producedArtefact
	 * @param newValue the producedArtefact to update
	 * 
	 */
	public void updateProducedArtefact();

	/**
	 * Adds the given filter to the producedArtefact edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToProducedArtefact(ViewerFilter filter);

	/**
	 * Adds the given filter to the producedArtefact edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToProducedArtefact(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the producedArtefact table
	 * 
	 */
	public boolean isContainedInProducedArtefactTable(EObject element);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
