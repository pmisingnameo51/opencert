/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pam.procspec.process.parts;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;


// End of user code

/**
 * 
 * 
 */
public interface ProcessModelPropertiesEditionPart {

	/**
	 * @return the id
	 * 
	 */
	public String getId();

	/**
	 * Defines a new id
	 * @param newValue the new id to set
	 * 
	 */
	public void setId(String newValue);


	/**
	 * @return the name
	 * 
	 */
	public String getName();

	/**
	 * Defines a new name
	 * @param newValue the new name to set
	 * 
	 */
	public void setName(String newValue);


	/**
	 * @return the description
	 * 
	 */
	public String getDescription();

	/**
	 * Defines a new description
	 * @param newValue the new description to set
	 * 
	 */
	public void setDescription(String newValue);




	/**
	 * Init the ownedActivity
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initOwnedActivity(ReferencesTableSettings settings);

	/**
	 * Update the ownedActivity
	 * @param newValue the ownedActivity to update
	 * 
	 */
	public void updateOwnedActivity();

	/**
	 * Adds the given filter to the ownedActivity edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToOwnedActivity(ViewerFilter filter);

	/**
	 * Adds the given filter to the ownedActivity edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToOwnedActivity(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the ownedActivity table
	 * 
	 */
	public boolean isContainedInOwnedActivityTable(EObject element);




	/**
	 * Init the ownedParticipant
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initOwnedParticipant(ReferencesTableSettings settings);

	/**
	 * Update the ownedParticipant
	 * @param newValue the ownedParticipant to update
	 * 
	 */
	public void updateOwnedParticipant();

	/**
	 * Adds the given filter to the ownedParticipant edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToOwnedParticipant(ViewerFilter filter);

	/**
	 * Adds the given filter to the ownedParticipant edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToOwnedParticipant(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the ownedParticipant table
	 * 
	 */
	public boolean isContainedInOwnedParticipantTable(EObject element);




	/**
	 * Init the ownedTechnique
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initOwnedTechnique(ReferencesTableSettings settings);

	/**
	 * Update the ownedTechnique
	 * @param newValue the ownedTechnique to update
	 * 
	 */
	public void updateOwnedTechnique();

	/**
	 * Adds the given filter to the ownedTechnique edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToOwnedTechnique(ViewerFilter filter);

	/**
	 * Adds the given filter to the ownedTechnique edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToOwnedTechnique(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the ownedTechnique table
	 * 
	 */
	public boolean isContainedInOwnedTechniqueTable(EObject element);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
