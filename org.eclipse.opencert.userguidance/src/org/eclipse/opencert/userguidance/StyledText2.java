/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.userguidance;

import java.util.List;

import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Composite;

public class StyledText2 extends StyledText {

	public StyledText2(Composite parent, int style) {
		super(parent, style);
	}

	// This disables new lines. Would be better if Ctrl+Return was
	// allowed.
	@Override
	public String getLineDelimiter() {
		checkWidget();
		// return super.getLineDelimiter();
		return "";
	}

	public void setStyleRanges(List<StyleRange> ranges) {
		StyleRange[] rangeArray = ranges.toArray(new StyleRange[ranges.size()]);
		setStyleRanges(rangeArray);
	}
}
