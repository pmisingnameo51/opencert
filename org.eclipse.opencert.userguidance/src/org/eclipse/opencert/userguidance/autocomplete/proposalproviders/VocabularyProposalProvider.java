/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.userguidance.autocomplete.proposalproviders;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.opencert.userguidance.autocomplete.ContentProposal2;
import org.eclipse.opencert.userguidance.util.ImageUtil;
import org.eclipse.opencert.userguidance.util.VocabFetcher;
import org.eclipse.opencert.vocabulary.Category;
import org.eclipse.opencert.vocabulary.Term;
import org.eclipse.opencert.vocabulary.Vocabulary;
import org.eclipse.opencert.vocabulary.VocabularyElement;
import org.eclipse.swt.graphics.Image;

public class VocabularyProposalProvider extends AbstractProposalProvider {

	private static final String VOC = "voc";

	private static final String VAR = "var";

	private static enum MatchType {
		noMatch, match, singleQuoteMatch, doubleQuoteMatch
	}

	private final static String path = "/icons/famfamfam/book_open.png";

	private static Image icon = null;

	public VocabularyProposalProvider(EObject eobj, int featureID) {
		super(eobj, featureID);
	}

	@Override
	protected List<ContentProposal2> createProposals(final String stringToComplete, EObject eobj, int featureID) {
		final List<ContentProposal2> result = new ArrayList<ContentProposal2>();
		VocabFetcher fetcher = new VocabFetcher() {
			@Override
			protected void readFromVocab(List<Vocabulary> vocabularies) {
				for (Vocabulary vocabulary : vocabularies) {
					if (vocabulary != null) {
						for (Category category : vocabulary.getCategories()) {
							MatchType matchType = getMatchType(VAR, category.getName(), stringToComplete);
							if (matchType != MatchType.noMatch) {
								String proposalString = createProposalString(VAR, category, matchType);
								String proposalDescription = createProposalDescription(category);
								ContentProposal2 proposal = new ContentProposal2(proposalString, proposalDescription,
										getImage());
								result.add(proposal);
							}
						}

						for (Term t : vocabulary.getTerms()) {
							MatchType matchType = getMatchType(VOC, t.getName(), stringToComplete);
							if (matchType != MatchType.noMatch) {
								String proposalString = createProposalString(VOC, t, matchType);
								String proposalDescription = createProposalDescription(t);
								ContentProposal2 proposal = new ContentProposal2(proposalString, proposalDescription,
										getImage());
								result.add(proposal);
							}
						}

					}
				}
			}
		};
		fetcher.execute();

		return result;
	}

	private MatchType getMatchType(String prefix, String elementName, String stringToComplete) {
		MatchType result = null;
		if ((prefix + ":" + elementName).toLowerCase().startsWith(stringToComplete.toLowerCase())
				&& !elementName.contains(" ")) {
			result = MatchType.match;
		} else if ((prefix + ":\"" + elementName + "\"").toLowerCase().startsWith(stringToComplete.toLowerCase())) {
			result = MatchType.doubleQuoteMatch;
		} else if ((prefix + ":'" + elementName + "'").toLowerCase().startsWith(stringToComplete.toLowerCase())) {
			result = MatchType.singleQuoteMatch;
		} else {
			result = MatchType.noMatch;
		}

		return result;
	}

	private String createProposalString(String prefix, VocabularyElement el, MatchType matchType) {
		StringBuilder result = new StringBuilder();
		result.append(prefix);
		result.append(":");

		switch (matchType) {
		case singleQuoteMatch:
			result.append("'");
			break;
		case doubleQuoteMatch:
			result.append("\"");
			break;
		default:
			break;
		}

		result.append(el.getName());

		switch (matchType) {
		case singleQuoteMatch:
			result.append("'");
			break;
		case doubleQuoteMatch:
			result.append("\"");
			break;
		default:
			break;
		}

		return result.toString();
	}

	private String createProposalDescription(VocabularyElement el) {
		if (el instanceof Term) {
			Term t = (Term) el;
			StringBuilder result = new StringBuilder();
			for (int i = 0; i < t.getDefinitions().size(); i++) {
				if (i > 0) {
					result.append("\n\n");
				}
				result.append(t.getDefinitions().get(i));
			}
			return result.toString();
		} else {
			return el.getDescription();
		}
	}

	@Override
	protected Image createImage() {
		if (icon == null) {
			icon = ImageUtil.createImage(this, path);
		}

		return icon;
	}
}
