/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.userguidance.util;

import java.util.List;

import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.opencert.infra.ui.loadresource.EditorTypeUtil;
import org.eclipse.opencert.vocabulary.Vocabulary;
import org.eclipse.opencert.vocabulary.VocabularyPackage;

public abstract class VocabFetcher {
	@SuppressWarnings("unchecked")
	public void execute() {
		List<Vocabulary> vocabularies = null;
		CdoConnection connection = null;
		try {
			IEditorPart editor = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage()
					.getActiveEditor();
			if (editor != null) {
				if (EditorTypeUtil.isFileEditor(editor)) {
					vocabularies = (List<Vocabulary>) (List<?>) FileUtil
							.fetch("vocabulary");
					readFromVocab(vocabularies);
				} else {
					connection = new CdoConnection();
					vocabularies = (List<Vocabulary>) (List<?>) CdoUtil.fetch(
							connection,
							VocabularyPackage.eINSTANCE.getVocabulary(), true);
					readFromVocab(vocabularies);
					connection.dispose();
				}
			}
		} finally {
			if (connection != null) {
				connection.dispose();
			}
		}
	}

	protected abstract void readFromVocab(List<Vocabulary> vocabularies);
}