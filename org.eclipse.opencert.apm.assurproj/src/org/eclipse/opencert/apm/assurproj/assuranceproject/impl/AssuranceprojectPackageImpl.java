/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectFactory;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig;
import org.eclipse.opencert.apm.assurproj.assuranceproject.PermissionConfig;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage;
import org.eclipse.opencert.infra.mappings.mapping.MappingPackage;
import org.eclipse.opencert.pam.procspec.process.ProcessPackage;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AssuranceprojectPackageImpl extends EPackageImpl implements AssuranceprojectPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assuranceProjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass permissionConfigEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetsPackageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baselineConfigEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AssuranceprojectPackageImpl() {
		super(eNS_URI, AssuranceprojectFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link AssuranceprojectPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AssuranceprojectPackage init() {
		if (isInited) return (AssuranceprojectPackage)EPackage.Registry.INSTANCE.getEPackage(AssuranceprojectPackage.eNS_URI);

		// Obtain or create and register package
		AssuranceprojectPackageImpl theAssuranceprojectPackage = (AssuranceprojectPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof AssuranceprojectPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new AssuranceprojectPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ArgPackage.eINSTANCE.eClass();
		BaselinePackage.eINSTANCE.eClass();
		ProcessPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theAssuranceprojectPackage.createPackageContents();

		// Initialize created meta-data
		theAssuranceprojectPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAssuranceprojectPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AssuranceprojectPackage.eNS_URI, theAssuranceprojectPackage);
		return theAssuranceprojectPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssuranceProject() {
		return assuranceProjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssuranceProject_CreatedBy() {
		return (EAttribute)assuranceProjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssuranceProject_Responsible() {
		return (EAttribute)assuranceProjectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssuranceProject_Date() {
		return (EAttribute)assuranceProjectEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssuranceProject_Version() {
		return (EAttribute)assuranceProjectEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssuranceProject_AssetsPackage() {
		return (EReference)assuranceProjectEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssuranceProject_PermissionConf() {
		return (EReference)assuranceProjectEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssuranceProject_BaselineConfig() {
		return (EReference)assuranceProjectEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssuranceProject_SubProject() {
		return (EReference)assuranceProjectEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPermissionConfig() {
		return permissionConfigEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPermissionConfig_IsActive() {
		return (EAttribute)permissionConfigEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssetsPackage() {
		return assetsPackageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssetsPackage_IsActive() {
		return (EAttribute)assetsPackageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetsPackage_ArtefactsModel() {
		return (EReference)assetsPackageEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetsPackage_ArgumentationModel() {
		return (EReference)assetsPackageEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetsPackage_ProcessModel() {
		return (EReference)assetsPackageEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaselineConfig() {
		return baselineConfigEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaselineConfig_ComplianceMapGroup() {
		return (EReference)baselineConfigEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaselineConfig_IsActive() {
		return (EAttribute)baselineConfigEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaselineConfig_RefFramework() {
		return (EReference)baselineConfigEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssuranceprojectFactory getAssuranceprojectFactory() {
		return (AssuranceprojectFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		assuranceProjectEClass = createEClass(ASSURANCE_PROJECT);
		createEAttribute(assuranceProjectEClass, ASSURANCE_PROJECT__CREATED_BY);
		createEAttribute(assuranceProjectEClass, ASSURANCE_PROJECT__RESPONSIBLE);
		createEAttribute(assuranceProjectEClass, ASSURANCE_PROJECT__DATE);
		createEAttribute(assuranceProjectEClass, ASSURANCE_PROJECT__VERSION);
		createEReference(assuranceProjectEClass, ASSURANCE_PROJECT__ASSETS_PACKAGE);
		createEReference(assuranceProjectEClass, ASSURANCE_PROJECT__PERMISSION_CONF);
		createEReference(assuranceProjectEClass, ASSURANCE_PROJECT__BASELINE_CONFIG);
		createEReference(assuranceProjectEClass, ASSURANCE_PROJECT__SUB_PROJECT);

		permissionConfigEClass = createEClass(PERMISSION_CONFIG);
		createEAttribute(permissionConfigEClass, PERMISSION_CONFIG__IS_ACTIVE);

		assetsPackageEClass = createEClass(ASSETS_PACKAGE);
		createEAttribute(assetsPackageEClass, ASSETS_PACKAGE__IS_ACTIVE);
		createEReference(assetsPackageEClass, ASSETS_PACKAGE__ARTEFACTS_MODEL);
		createEReference(assetsPackageEClass, ASSETS_PACKAGE__ARGUMENTATION_MODEL);
		createEReference(assetsPackageEClass, ASSETS_PACKAGE__PROCESS_MODEL);

		baselineConfigEClass = createEClass(BASELINE_CONFIG);
		createEReference(baselineConfigEClass, BASELINE_CONFIG__COMPLIANCE_MAP_GROUP);
		createEAttribute(baselineConfigEClass, BASELINE_CONFIG__IS_ACTIVE);
		createEReference(baselineConfigEClass, BASELINE_CONFIG__REF_FRAMEWORK);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		GeneralPackage theGeneralPackage = (GeneralPackage)EPackage.Registry.INSTANCE.getEPackage(GeneralPackage.eNS_URI);
		EvidencePackage theEvidencePackage = (EvidencePackage)EPackage.Registry.INSTANCE.getEPackage(EvidencePackage.eNS_URI);
		ArgPackage theArgPackage = (ArgPackage)EPackage.Registry.INSTANCE.getEPackage(ArgPackage.eNS_URI);
		ProcessPackage theProcessPackage = (ProcessPackage)EPackage.Registry.INSTANCE.getEPackage(ProcessPackage.eNS_URI);
		MappingPackage theMappingPackage = (MappingPackage)EPackage.Registry.INSTANCE.getEPackage(MappingPackage.eNS_URI);
		BaselinePackage theBaselinePackage = (BaselinePackage)EPackage.Registry.INSTANCE.getEPackage(BaselinePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		assuranceProjectEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		permissionConfigEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		assetsPackageEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		baselineConfigEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(assuranceProjectEClass, AssuranceProject.class, "AssuranceProject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAssuranceProject_CreatedBy(), ecorePackage.getEString(), "createdBy", null, 0, 1, AssuranceProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssuranceProject_Responsible(), ecorePackage.getEString(), "responsible", null, 0, 1, AssuranceProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssuranceProject_Date(), ecorePackage.getEDate(), "date", null, 0, 1, AssuranceProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssuranceProject_Version(), ecorePackage.getEString(), "version", null, 0, 1, AssuranceProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssuranceProject_AssetsPackage(), this.getAssetsPackage(), null, "assetsPackage", null, 0, -1, AssuranceProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssuranceProject_PermissionConf(), this.getPermissionConfig(), null, "permissionConf", null, 0, -1, AssuranceProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssuranceProject_BaselineConfig(), this.getBaselineConfig(), null, "baselineConfig", null, 0, -1, AssuranceProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssuranceProject_SubProject(), this.getAssuranceProject(), null, "subProject", null, 0, -1, AssuranceProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(permissionConfigEClass, PermissionConfig.class, "PermissionConfig", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPermissionConfig_IsActive(), ecorePackage.getEBoolean(), "isActive", null, 0, 1, PermissionConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assetsPackageEClass, AssetsPackage.class, "AssetsPackage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAssetsPackage_IsActive(), ecorePackage.getEBoolean(), "isActive", null, 0, 1, AssetsPackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetsPackage_ArtefactsModel(), theEvidencePackage.getArtefactModel(), null, "artefactsModel", null, 0, -1, AssetsPackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetsPackage_ArgumentationModel(), theArgPackage.getCase(), null, "argumentationModel", null, 0, -1, AssetsPackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetsPackage_ProcessModel(), theProcessPackage.getProcessModel(), null, "processModel", null, 0, -1, AssetsPackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(baselineConfigEClass, BaselineConfig.class, "BaselineConfig", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBaselineConfig_ComplianceMapGroup(), theMappingPackage.getMapGroup(), null, "complianceMapGroup", null, 0, 1, BaselineConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBaselineConfig_IsActive(), ecorePackage.getEBoolean(), "isActive", null, 0, 1, BaselineConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBaselineConfig_RefFramework(), theBaselinePackage.getBaseFramework(), null, "refFramework", null, 0, -1, BaselineConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //AssuranceprojectPackageImpl
