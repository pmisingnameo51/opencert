/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.parts.impl;

// Start of user code for imports
import java.util.Iterator;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.providers.EMFListContentProvider;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;
import org.eclipse.emf.eef.runtime.ui.widgets.EMFComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.EObjectFlatComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.infra.properties.property.Property;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.opencert.evm.evidspec.evidence.parts.EvidenceViewsRepository;
import org.eclipse.opencert.evm.evidspec.evidence.parts.ValuePropertiesEditionPart;
import org.eclipse.opencert.evm.evidspec.evidence.parts.forms.ValuePropertiesEditionPartForm;
import org.eclipse.opencert.evm.evidspec.evidence.providers.EvidenceMessages;

// End of user code

/**
 * 
 * 
 */
public class ValuePropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, ValuePropertiesEditionPart {

	protected Text name;
	protected Text value;
	protected EObjectFlatComboViewer propertyReference;
	protected EMFComboViewer propertyCombo;

	//Start IRR
	protected Combo propertyValueCombo;
	protected EList enumerationList;
	//End IRR

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ValuePropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence valueStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = valueStep.addStep(EvidenceViewsRepository.Value.Properties.class);
		propertiesStep.addStep(EvidenceViewsRepository.Value.Properties.name);
		propertiesStep.addStep(EvidenceViewsRepository.Value.Properties.value_);
		
		// Start IRR
		//propertiesStep.addStep(EvidenceViewsRepository.Value.Properties.propertyReference);
		// End IRR		
		
		propertiesStep.addStep(EvidenceViewsRepository.Value.Properties.propertyCombo);
		
		
		composer = new PartComposer(valueStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == EvidenceViewsRepository.Value.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == EvidenceViewsRepository.Value.Properties.name) {
					return createNameText(parent);
				}
				if (key == EvidenceViewsRepository.Value.Properties.value_) {
					return createValueText(parent);
				}
				if (key == EvidenceViewsRepository.Value.Properties.propertyReference) {
					return createPropertyReferenceFlatComboViewer(parent);
				}
				if (key == EvidenceViewsRepository.Value.Properties.propertyCombo) {
					return createPropertyComboEMFComboViewer(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(EvidenceMessages.ValuePropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	
	protected Composite createNameText(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.Value.Properties.name, EvidenceMessages.ValuePropertiesEditionPart_NameLabel);
		name = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ValuePropertiesEditionPartImpl.this, EvidenceViewsRepository.Value.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
			}

		});
		name.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ValuePropertiesEditionPartImpl.this, EvidenceViewsRepository.Value.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}

		});
		EditingUtils.setID(name, EvidenceViewsRepository.Value.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.Value.Properties.name, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createValueText(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.Value.Properties.value_, EvidenceMessages.ValuePropertiesEditionPart_ValueLabel);
		value = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData valueData = new GridData(GridData.FILL_HORIZONTAL);
		value.setLayoutData(valueData);
		value.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ValuePropertiesEditionPartImpl.this, EvidenceViewsRepository.Value.Properties.value_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, value.getText()));
			}

		});
		value.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ValuePropertiesEditionPartImpl.this, EvidenceViewsRepository.Value.Properties.value_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, value.getText()));
				}
			}

		});
		EditingUtils.setID(value, EvidenceViewsRepository.Value.Properties.value_);
		EditingUtils.setEEFtype(value, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.Value.Properties.value_, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createValueText

		// End of user code
		return parent;
	}

	/**
	 * @param parent the parent composite
	 * 
	 */
	protected Composite createPropertyReferenceFlatComboViewer(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.Value.Properties.propertyReference, EvidenceMessages.ValuePropertiesEditionPart_PropertyReferenceLabel);
		propertyReference = new EObjectFlatComboViewer(parent, !propertiesEditionComponent.isRequired(EvidenceViewsRepository.Value.Properties.propertyReference, EvidenceViewsRepository.SWT_KIND));
		propertyReference.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));

		propertyReference.addSelectionChangedListener(new ISelectionChangedListener() {

			public void selectionChanged(SelectionChangedEvent event) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ValuePropertiesEditionPartImpl.this, EvidenceViewsRepository.Value.Properties.propertyReference, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SET, null, getPropertyReference()));
			}

		});
		GridData propertyReferenceData = new GridData(GridData.FILL_HORIZONTAL);
		propertyReference.setLayoutData(propertyReferenceData);
		propertyReference.setID(EvidenceViewsRepository.Value.Properties.propertyReference);
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.Value.Properties.propertyReference, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createPropertyReferenceFlatComboViewer

		// End of user code
		return parent;
	}

	
	protected Composite createPropertyComboEMFComboViewer(Composite parent) {
		createDescription(parent, EvidenceViewsRepository.Value.Properties.propertyCombo, EvidenceMessages.ValuePropertiesEditionPart_PropertyComboLabel);
		propertyCombo = new EMFComboViewer(parent);
		GridData propertyComboData = new GridData(GridData.FILL_HORIZONTAL);
		propertyCombo.getCombo().setLayoutData(propertyComboData);
		propertyCombo.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		propertyCombo.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null){
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ValuePropertiesEditionPartImpl.this, EvidenceViewsRepository.Value.Properties.propertyCombo, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getPropertyCombo()));
					//Start IRR Add values to combo

					String seleccion = (String)propertyCombo.getSelection().toString();

					if (!seleccion.contentEquals("[]")){
						Property firstElement = (Property)((StructuredSelection) propertyCombo.getSelection()).getFirstElement();

						// Por defecto le asigno el mismo nombre a la propiedad
						name.setText(firstElement.getName());
						if (propertiesEditionComponent != null)
							propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ValuePropertiesEditionPartImpl.this, EvidenceViewsRepository.Value.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
						
						enumerationList = firstElement.getEnumValues();
						//REcorremos la lista para rellenar la combo. Primero la vacio
						propertyValueCombo.removeAll();
						int i = 0;
						int j = 0;
						Iterator iter = enumerationList.iterator();
						String sValor = "";
						while (iter.hasNext()) {
							sValor =  iter.next().toString();
							propertyValueCombo.add(sValor);
							if (sValor.contentEquals(value.getText())) {
								j = i;
							}
							i++;
						}	
						propertyValueCombo.select(j);
					}
					//End IRR
				}
			}
		});
		
		propertyCombo.setContentProvider(new EMFListContentProvider());
		
		// Start IRR
		propertyValueCombo = new Combo(parent, SWT.READ_ONLY);

		GridData propertyValueComboData = new GridData(GridData.FILL_HORIZONTAL);
		propertyValueCombo.setLayoutData(propertyValueComboData);
		propertyValueCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// System.out.println("Selection: " +
				// propertyValueCombo.getItem(propertyValueCombo.getSelectionIndex()));
				value.setText(propertyValueCombo.getItem(propertyValueCombo
						.getSelectionIndex()));
				propertiesEditionComponent
						.firePropertiesChanged(new PropertiesEditionEvent(
								ValuePropertiesEditionPartImpl.this,
								EvidenceViewsRepository.Value.Properties.value_,
								PropertiesEditionEvent.COMMIT,
								PropertiesEditionEvent.SET, null, value
										.getText()));
			}
		});

		// System.out.println("seleccion");
		// End IRR

		
		EditingUtils.setID(propertyCombo.getCombo(), EvidenceViewsRepository.Value.Properties.propertyCombo);
		EditingUtils.setEEFtype(propertyCombo.getCombo(), "eef::Combo"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(EvidenceViewsRepository.Value.Properties.propertyCombo, EvidenceViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createPropertyComboEMFComboViewer

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ValuePropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ValuePropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.Value.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(EvidenceMessages.Value_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ValuePropertiesEditionPart#getValue()
	 * 
	 */
	public String getValue() {
		return value.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ValuePropertiesEditionPart#setValue(String newValue)
	 * 
	 */
	public void setValue(String newValue) {
		if (newValue != null) {
			value.setText(newValue);
		} else {
			value.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.Value.Properties.value_);
		if (eefElementEditorReadOnlyState && value.isEnabled()) {
			value.setEnabled(false);
			value.setToolTipText(EvidenceMessages.Value_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !value.isEnabled()) {
			value.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ValuePropertiesEditionPart#getPropertyReference()
	 * 
	 */
	public EObject getPropertyReference() {
		if (propertyReference.getSelection() instanceof StructuredSelection) {
			Object firstElement = ((StructuredSelection) propertyReference.getSelection()).getFirstElement();
			if (firstElement instanceof EObject)
				return (EObject) firstElement;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ValuePropertiesEditionPart#initPropertyReference(EObjectFlatComboSettings)
	 */
	public void initPropertyReference(EObjectFlatComboSettings settings) {
		propertyReference.setInput(settings);
		if (current != null) {
			propertyReference.setSelection(new StructuredSelection(settings.getValue()));
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.Value.Properties.propertyReference);
		if (eefElementEditorReadOnlyState && propertyReference.isEnabled()) {
			propertyReference.setEnabled(false);
			propertyReference.setToolTipText(EvidenceMessages.Value_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !propertyReference.isEnabled()) {
			propertyReference.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ValuePropertiesEditionPart#setPropertyReference(EObject newValue)
	 * 
	 */
	public void setPropertyReference(EObject newValue) {
		if (newValue != null) {
			propertyReference.setSelection(new StructuredSelection(newValue));
		} else {
			propertyReference.setSelection(new StructuredSelection()); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.Value.Properties.propertyReference);
		if (eefElementEditorReadOnlyState && propertyReference.isEnabled()) {
			propertyReference.setEnabled(false);
			propertyReference.setToolTipText(EvidenceMessages.Value_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !propertyReference.isEnabled()) {
			propertyReference.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ValuePropertiesEditionPart#setPropertyReferenceButtonMode(ButtonsModeEnum newValue)
	 */
	public void setPropertyReferenceButtonMode(ButtonsModeEnum newValue) {
		propertyReference.setButtonMode(newValue);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ValuePropertiesEditionPart#addFilterPropertyReference(ViewerFilter filter)
	 * 
	 */
	public void addFilterToPropertyReference(ViewerFilter filter) {
		propertyReference.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ValuePropertiesEditionPart#addBusinessFilterPropertyReference(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToPropertyReference(ViewerFilter filter) {
		propertyReference.addBusinessRuleFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ValuePropertiesEditionPart#getPropertyCombo()
	 * 
	 */
	public Object getPropertyCombo() {
		if (propertyCombo.getSelection() instanceof StructuredSelection) {
			return ((StructuredSelection) propertyCombo.getSelection()).getFirstElement();
		}
		return "";
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ValuePropertiesEditionPart#initPropertyCombo(Object input, Object currentValue)
	 */
	public void initPropertyCombo(Object input, Object currentValue) {
		propertyCombo.setInput(input);
		if (currentValue != null) {
			propertyCombo.setSelection(new StructuredSelection(currentValue));
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ValuePropertiesEditionPart#setPropertyCombo(Object newValue)
	 * 
	 */
	public void setPropertyCombo(Object newValue) {
		if (newValue != null) {
			propertyCombo.modelUpdating(new StructuredSelection(newValue));
		} else {
			propertyCombo.modelUpdating(new StructuredSelection("")); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(EvidenceViewsRepository.Value.Properties.propertyCombo);
		if (eefElementEditorReadOnlyState && propertyCombo.isEnabled()) {
			propertyCombo.setEnabled(false);
			propertyCombo.setToolTipText(EvidenceMessages.Value_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !propertyCombo.isEnabled()) {
			propertyCombo.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.evm.evidspec.evidence.parts.ValuePropertiesEditionPart#addFilterPropertyCombo(ViewerFilter filter)
	 * 
	 */
	public void addFilterToPropertyCombo(ViewerFilter filter) {
		propertyCombo.addFilter(filter);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return EvidenceMessages.Value_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
