/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.providers;

import org.eclipse.emf.common.notify.Adapter;

import org.eclipse.opencert.evm.evidspec.evidence.util.EvidenceAdapterFactory;

/**
 * 
 * 
 */
public class EvidenceEEFAdapterFactory extends EvidenceAdapterFactory {

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.evm.evidspec.evidence.util.EvidenceAdapterFactory#createArtefactModelAdapter()
	 * 
	 */
	public Adapter createArtefactModelAdapter() {
		return new ArtefactModelPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.evm.evidspec.evidence.util.EvidenceAdapterFactory#createArtefactAdapter()
	 * 
	 */
	public Adapter createArtefactAdapter() {
		return new ArtefactPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.evm.evidspec.evidence.util.EvidenceAdapterFactory#createValueAdapter()
	 * 
	 */
	public Adapter createValueAdapter() {
		return new ValuePropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.evm.evidspec.evidence.util.EvidenceAdapterFactory#createResourceAdapter()
	 * 
	 */
	public Adapter createResourceAdapter() {
		return new ResourcePropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.evm.evidspec.evidence.util.EvidenceAdapterFactory#createArtefactRelAdapter()
	 * 
	 */
	public Adapter createArtefactRelAdapter() {
		return new ArtefactRelPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.evm.evidspec.evidence.util.EvidenceAdapterFactory#createArtefactDefinitionAdapter()
	 * 
	 */
	public Adapter createArtefactDefinitionAdapter() {
		return new ArtefactDefinitionPropertiesEditionProvider();
	}

}
