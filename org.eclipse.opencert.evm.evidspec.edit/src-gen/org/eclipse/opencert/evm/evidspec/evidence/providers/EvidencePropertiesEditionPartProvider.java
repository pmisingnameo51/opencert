/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.evm.evidspec.evidence.providers;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.api.providers.IPropertiesEditionPartProvider;

import org.eclipse.opencert.evm.evidspec.evidence.parts.EvidenceViewsRepository;

import org.eclipse.opencert.evm.evidspec.evidence.parts.forms.ArtefactDefinitionArtefactPropertiesEditionPartForm;
import org.eclipse.opencert.evm.evidspec.evidence.parts.forms.ArtefactDefinitionEvaluationPropertiesEditionPartForm;
import org.eclipse.opencert.evm.evidspec.evidence.parts.forms.ArtefactDefinitionEventsPropertiesEditionPartForm;
import org.eclipse.opencert.evm.evidspec.evidence.parts.forms.ArtefactDefinitionPropertiesEditionPartForm;
import org.eclipse.opencert.evm.evidspec.evidence.parts.forms.ArtefactEvaluationPropertiesEditionPartForm;
import org.eclipse.opencert.evm.evidspec.evidence.parts.forms.ArtefactEventsPropertiesEditionPartForm;
import org.eclipse.opencert.evm.evidspec.evidence.parts.forms.ArtefactModelPropertiesEditionPartForm;
import org.eclipse.opencert.evm.evidspec.evidence.parts.forms.ArtefactPropertiesEditionPartForm;
import org.eclipse.opencert.evm.evidspec.evidence.parts.forms.ArtefactPropertyValuePropertiesEditionPartForm;
import org.eclipse.opencert.evm.evidspec.evidence.parts.forms.ArtefactRelPropertiesEditionPartForm;
import org.eclipse.opencert.evm.evidspec.evidence.parts.forms.ArtefactVersionPropertiesEditionPartForm;
import org.eclipse.opencert.evm.evidspec.evidence.parts.forms.ResourcePropertiesEditionPartForm;
import org.eclipse.opencert.evm.evidspec.evidence.parts.forms.ValuePropertiesEditionPartForm;

import org.eclipse.opencert.evm.evidspec.evidence.parts.impl.ArtefactDefinitionArtefactPropertiesEditionPartImpl;
import org.eclipse.opencert.evm.evidspec.evidence.parts.impl.ArtefactDefinitionEvaluationPropertiesEditionPartImpl;
import org.eclipse.opencert.evm.evidspec.evidence.parts.impl.ArtefactDefinitionEventsPropertiesEditionPartImpl;
import org.eclipse.opencert.evm.evidspec.evidence.parts.impl.ArtefactDefinitionPropertiesEditionPartImpl;
import org.eclipse.opencert.evm.evidspec.evidence.parts.impl.ArtefactEvaluationPropertiesEditionPartImpl;
import org.eclipse.opencert.evm.evidspec.evidence.parts.impl.ArtefactEventsPropertiesEditionPartImpl;
import org.eclipse.opencert.evm.evidspec.evidence.parts.impl.ArtefactModelPropertiesEditionPartImpl;
import org.eclipse.opencert.evm.evidspec.evidence.parts.impl.ArtefactPropertiesEditionPartImpl;
import org.eclipse.opencert.evm.evidspec.evidence.parts.impl.ArtefactPropertyValuePropertiesEditionPartImpl;
import org.eclipse.opencert.evm.evidspec.evidence.parts.impl.ArtefactRelPropertiesEditionPartImpl;
import org.eclipse.opencert.evm.evidspec.evidence.parts.impl.ArtefactVersionPropertiesEditionPartImpl;
import org.eclipse.opencert.evm.evidspec.evidence.parts.impl.ResourcePropertiesEditionPartImpl;
import org.eclipse.opencert.evm.evidspec.evidence.parts.impl.ValuePropertiesEditionPartImpl;

/**
 * 
 * 
 */
public class EvidencePropertiesEditionPartProvider implements IPropertiesEditionPartProvider {

	/** 
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPartProvider#provides(java.lang.Object)
	 * 
	 */
	public boolean provides(Object key) {
		return key == EvidenceViewsRepository.class;
	}

	/** 
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPartProvider#getPropertiesEditionPart(java.lang.Object, int, org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(Object key, int kind, IPropertiesEditionComponent component) {
		if (key == EvidenceViewsRepository.ArtefactModel.class) {
			if (kind == EvidenceViewsRepository.SWT_KIND)
				return new ArtefactModelPropertiesEditionPartImpl(component);
			if (kind == EvidenceViewsRepository.FORM_KIND)
				return new ArtefactModelPropertiesEditionPartForm(component);
		}
		if (key == EvidenceViewsRepository.ArtefactDefinition.class) {
			if (kind == EvidenceViewsRepository.SWT_KIND)
				return new ArtefactDefinitionPropertiesEditionPartImpl(component);
			if (kind == EvidenceViewsRepository.FORM_KIND)
				return new ArtefactDefinitionPropertiesEditionPartForm(component);
		}
		if (key == EvidenceViewsRepository.Artefact.class) {
			if (kind == EvidenceViewsRepository.SWT_KIND)
				return new ArtefactPropertiesEditionPartImpl(component);
			if (kind == EvidenceViewsRepository.FORM_KIND)
				return new ArtefactPropertiesEditionPartForm(component);
		}
		if (key == EvidenceViewsRepository.Value.class) {
			if (kind == EvidenceViewsRepository.SWT_KIND)
				return new ValuePropertiesEditionPartImpl(component);
			if (kind == EvidenceViewsRepository.FORM_KIND)
				return new ValuePropertiesEditionPartForm(component);
		}
		if (key == EvidenceViewsRepository.Resource.class) {
			if (kind == EvidenceViewsRepository.SWT_KIND)
				return new ResourcePropertiesEditionPartImpl(component);
			if (kind == EvidenceViewsRepository.FORM_KIND)
				return new ResourcePropertiesEditionPartForm(component);
		}
		if (key == EvidenceViewsRepository.ArtefactRel.class) {
			if (kind == EvidenceViewsRepository.SWT_KIND)
				return new ArtefactRelPropertiesEditionPartImpl(component);
			if (kind == EvidenceViewsRepository.FORM_KIND)
				return new ArtefactRelPropertiesEditionPartForm(component);
		}
		if (key == EvidenceViewsRepository.ArtefactDefinitionArtefact.class) {
			if (kind == EvidenceViewsRepository.SWT_KIND)
				return new ArtefactDefinitionArtefactPropertiesEditionPartImpl(component);
			if (kind == EvidenceViewsRepository.FORM_KIND)
				return new ArtefactDefinitionArtefactPropertiesEditionPartForm(component);
		}
		if (key == EvidenceViewsRepository.ArtefactDefinitionEvaluation.class) {
			if (kind == EvidenceViewsRepository.SWT_KIND)
				return new ArtefactDefinitionEvaluationPropertiesEditionPartImpl(component);
			if (kind == EvidenceViewsRepository.FORM_KIND)
				return new ArtefactDefinitionEvaluationPropertiesEditionPartForm(component);
		}
		if (key == EvidenceViewsRepository.ArtefactDefinitionEvents.class) {
			if (kind == EvidenceViewsRepository.SWT_KIND)
				return new ArtefactDefinitionEventsPropertiesEditionPartImpl(component);
			if (kind == EvidenceViewsRepository.FORM_KIND)
				return new ArtefactDefinitionEventsPropertiesEditionPartForm(component);
		}
		if (key == EvidenceViewsRepository.ArtefactVersion.class) {
			if (kind == EvidenceViewsRepository.SWT_KIND)
				return new ArtefactVersionPropertiesEditionPartImpl(component);
			if (kind == EvidenceViewsRepository.FORM_KIND)
				return new ArtefactVersionPropertiesEditionPartForm(component);
		}
		if (key == EvidenceViewsRepository.ArtefactPropertyValue.class) {
			if (kind == EvidenceViewsRepository.SWT_KIND)
				return new ArtefactPropertyValuePropertiesEditionPartImpl(component);
			if (kind == EvidenceViewsRepository.FORM_KIND)
				return new ArtefactPropertyValuePropertiesEditionPartForm(component);
		}
		if (key == EvidenceViewsRepository.ArtefactEvaluation.class) {
			if (kind == EvidenceViewsRepository.SWT_KIND)
				return new ArtefactEvaluationPropertiesEditionPartImpl(component);
			if (kind == EvidenceViewsRepository.FORM_KIND)
				return new ArtefactEvaluationPropertiesEditionPartForm(component);
		}
		if (key == EvidenceViewsRepository.ArtefactEvents.class) {
			if (kind == EvidenceViewsRepository.SWT_KIND)
				return new ArtefactEventsPropertiesEditionPartImpl(component);
			if (kind == EvidenceViewsRepository.FORM_KIND)
				return new ArtefactEventsPropertiesEditionPartForm(component);
		}
		return null;
	}

}
