/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.pkm.refframework.refframework.RefRequirement;
import org.eclipse.opencert.pkm.refframework.refframework.RefRequirementRel;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefRequirementPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class RefRequirementBasePropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for ownedRel ReferencesTable
	 */
	protected ReferencesTableSettings ownedRelSettings;
	
	/**
	 * Settings for subRequirement ReferencesTable
	 */
	protected ReferencesTableSettings subRequirementSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public RefRequirementBasePropertiesEditionComponent(PropertiesEditingContext editingContext, EObject refRequirement, String editing_mode) {
		super(editingContext, refRequirement, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = RefframeworkViewsRepository.class;
		partKey = RefframeworkViewsRepository.RefRequirement.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final RefRequirement refRequirement = (RefRequirement)elt;
			final RefRequirementPropertiesEditionPart basePart = (RefRequirementPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(RefframeworkViewsRepository.RefRequirement.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, refRequirement.getId()));
			
			if (isAccessible(RefframeworkViewsRepository.RefRequirement.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, refRequirement.getName()));
			
			if (isAccessible(RefframeworkViewsRepository.RefRequirement.Properties.description))
				basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, refRequirement.getDescription()));
			if (isAccessible(RefframeworkViewsRepository.RefRequirement.Properties.reference))
				basePart.setReference(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, refRequirement.getReference()));
			
			if (isAccessible(RefframeworkViewsRepository.RefRequirement.Properties.assumptions))
				basePart.setAssumptions(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, refRequirement.getAssumptions()));
			
			if (isAccessible(RefframeworkViewsRepository.RefRequirement.Properties.rationale))
				basePart.setRationale(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, refRequirement.getRationale()));
			
			if (isAccessible(RefframeworkViewsRepository.RefRequirement.Properties.image))
				basePart.setImage(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, refRequirement.getImage()));
			
			if (isAccessible(RefframeworkViewsRepository.RefRequirement.Properties.annotations))
				basePart.setAnnotations(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, refRequirement.getAnnotations()));
			
			if (isAccessible(RefframeworkViewsRepository.RefRequirement.Properties.ownedRel)) {
				ownedRelSettings = new ReferencesTableSettings(refRequirement, RefframeworkPackage.eINSTANCE.getRefRequirement_OwnedRel());
				basePart.initOwnedRel(ownedRelSettings);
			}
			if (isAccessible(RefframeworkViewsRepository.RefRequirement.Properties.subRequirement)) {
				subRequirementSettings = new ReferencesTableSettings(refRequirement, RefframeworkPackage.eINSTANCE.getRefRequirement_SubRequirement());
				basePart.initSubRequirement(subRequirementSettings);
			}
			// init filters
			
			
			
			
			
			
			
			
			if (isAccessible(RefframeworkViewsRepository.RefRequirement.Properties.ownedRel)) {
				basePart.addFilterToOwnedRel(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof RefRequirementRel); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for ownedRel
				// End of user code
			}
			if (isAccessible(RefframeworkViewsRepository.RefRequirement.Properties.subRequirement)) {
				basePart.addFilterToSubRequirement(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof RefRequirement); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for subRequirement
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}













	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == RefframeworkViewsRepository.RefRequirement.Properties.id) {
			return GeneralPackage.eINSTANCE.getNamedElement_Id();
		}
		if (editorKey == RefframeworkViewsRepository.RefRequirement.Properties.name) {
			return GeneralPackage.eINSTANCE.getNamedElement_Name();
		}
		if (editorKey == RefframeworkViewsRepository.RefRequirement.Properties.description) {
			return GeneralPackage.eINSTANCE.getDescribableElement_Description();
		}
		if (editorKey == RefframeworkViewsRepository.RefRequirement.Properties.reference) {
			return RefframeworkPackage.eINSTANCE.getRefRequirement_Reference();
		}
		if (editorKey == RefframeworkViewsRepository.RefRequirement.Properties.assumptions) {
			return RefframeworkPackage.eINSTANCE.getRefRequirement_Assumptions();
		}
		if (editorKey == RefframeworkViewsRepository.RefRequirement.Properties.rationale) {
			return RefframeworkPackage.eINSTANCE.getRefRequirement_Rationale();
		}
		if (editorKey == RefframeworkViewsRepository.RefRequirement.Properties.image) {
			return RefframeworkPackage.eINSTANCE.getRefRequirement_Image();
		}
		if (editorKey == RefframeworkViewsRepository.RefRequirement.Properties.annotations) {
			return RefframeworkPackage.eINSTANCE.getRefRequirement_Annotations();
		}
		if (editorKey == RefframeworkViewsRepository.RefRequirement.Properties.ownedRel) {
			return RefframeworkPackage.eINSTANCE.getRefRequirement_OwnedRel();
		}
		if (editorKey == RefframeworkViewsRepository.RefRequirement.Properties.subRequirement) {
			return RefframeworkPackage.eINSTANCE.getRefRequirement_SubRequirement();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		RefRequirement refRequirement = (RefRequirement)semanticObject;
		if (RefframeworkViewsRepository.RefRequirement.Properties.id == event.getAffectedEditor()) {
			refRequirement.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefRequirement.Properties.name == event.getAffectedEditor()) {
			refRequirement.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefRequirement.Properties.description == event.getAffectedEditor()) {
			refRequirement.setDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefRequirement.Properties.reference == event.getAffectedEditor()) {
			refRequirement.setReference((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefRequirement.Properties.assumptions == event.getAffectedEditor()) {
			refRequirement.setAssumptions((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefRequirement.Properties.rationale == event.getAffectedEditor()) {
			refRequirement.setRationale((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefRequirement.Properties.image == event.getAffectedEditor()) {
			refRequirement.setImage((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefRequirement.Properties.annotations == event.getAffectedEditor()) {
			refRequirement.setAnnotations((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefRequirement.Properties.ownedRel == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, ownedRelSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				ownedRelSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				ownedRelSettings.move(event.getNewIndex(), (RefRequirementRel) event.getNewValue());
			}
		}
		if (RefframeworkViewsRepository.RefRequirement.Properties.subRequirement == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, subRequirementSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				subRequirementSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				subRequirementSettings.move(event.getNewIndex(), (RefRequirement) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			RefRequirementPropertiesEditionPart basePart = (RefRequirementPropertiesEditionPart)editingPart;
			if (GeneralPackage.eINSTANCE.getNamedElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefRequirement.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (GeneralPackage.eINSTANCE.getNamedElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefRequirement.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (GeneralPackage.eINSTANCE.getDescribableElement_Description().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefRequirement.Properties.description)){
				if (msg.getNewValue() != null) {
					basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setDescription("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefRequirement_Reference().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefRequirement.Properties.reference)) {
				if (msg.getNewValue() != null) {
					basePart.setReference(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setReference("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefRequirement_Assumptions().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefRequirement.Properties.assumptions)) {
				if (msg.getNewValue() != null) {
					basePart.setAssumptions(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setAssumptions("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefRequirement_Rationale().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefRequirement.Properties.rationale)) {
				if (msg.getNewValue() != null) {
					basePart.setRationale(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setRationale("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefRequirement_Image().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefRequirement.Properties.image)) {
				if (msg.getNewValue() != null) {
					basePart.setImage(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setImage("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefRequirement_Annotations().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefRequirement.Properties.annotations)) {
				if (msg.getNewValue() != null) {
					basePart.setAnnotations(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setAnnotations("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefRequirement_OwnedRel().equals(msg.getFeature()) && isAccessible(RefframeworkViewsRepository.RefRequirement.Properties.ownedRel))
				basePart.updateOwnedRel();
			if (RefframeworkPackage.eINSTANCE.getRefRequirement_SubRequirement().equals(msg.getFeature()) && isAccessible(RefframeworkViewsRepository.RefRequirement.Properties.subRequirement))
				basePart.updateSubRequirement();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			GeneralPackage.eINSTANCE.getNamedElement_Id(),
			GeneralPackage.eINSTANCE.getNamedElement_Name(),
			GeneralPackage.eINSTANCE.getDescribableElement_Description(),
			RefframeworkPackage.eINSTANCE.getRefRequirement_Reference(),
			RefframeworkPackage.eINSTANCE.getRefRequirement_Assumptions(),
			RefframeworkPackage.eINSTANCE.getRefRequirement_Rationale(),
			RefframeworkPackage.eINSTANCE.getRefRequirement_Image(),
			RefframeworkPackage.eINSTANCE.getRefRequirement_Annotations(),
			RefframeworkPackage.eINSTANCE.getRefRequirement_OwnedRel(),
			RefframeworkPackage.eINSTANCE.getRefRequirement_SubRequirement()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (RefframeworkViewsRepository.RefRequirement.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefRequirement.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefRequirement.Properties.description == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefRequirement.Properties.reference == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(RefframeworkPackage.eINSTANCE.getRefRequirement_Reference().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(RefframeworkPackage.eINSTANCE.getRefRequirement_Reference().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefRequirement.Properties.assumptions == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(RefframeworkPackage.eINSTANCE.getRefRequirement_Assumptions().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(RefframeworkPackage.eINSTANCE.getRefRequirement_Assumptions().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefRequirement.Properties.rationale == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(RefframeworkPackage.eINSTANCE.getRefRequirement_Rationale().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(RefframeworkPackage.eINSTANCE.getRefRequirement_Rationale().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefRequirement.Properties.image == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(RefframeworkPackage.eINSTANCE.getRefRequirement_Image().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(RefframeworkPackage.eINSTANCE.getRefRequirement_Image().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefRequirement.Properties.annotations == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(RefframeworkPackage.eINSTANCE.getRefRequirement_Annotations().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(RefframeworkPackage.eINSTANCE.getRefRequirement_Annotations().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
