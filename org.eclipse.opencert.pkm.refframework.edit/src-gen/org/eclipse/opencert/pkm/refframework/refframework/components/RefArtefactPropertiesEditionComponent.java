/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.components;

// Start of user code for imports

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.opencert.pkm.refframework.refframework.RefArtefact;

import org.eclipse.opencert.pkm.refframework.refframework.parts.ArtefactEquivalenceMapPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;

// End of user code

/**
 * 
 * 
 */
public class RefArtefactPropertiesEditionComponent extends ComposedPropertiesEditionComponent {

	/**
	 * The Base part
	 * 
	 */
	private RefArtefactPropertiesEditionPart basePart;

	/**
	 * The RefArtefactBasePropertiesEditionComponent sub component
	 * 
	 */
	protected RefArtefactBasePropertiesEditionComponent refArtefactBasePropertiesEditionComponent;

	/**
	 * The ArtefactEquivalenceMap part
	 * 
	 */
	private ArtefactEquivalenceMapPropertiesEditionPart artefactEquivalenceMapPart;

	/**
	 * The RefArtefactArtefactEquivalenceMapPropertiesEditionComponent sub component
	 * 
	 */
	protected RefArtefactArtefactEquivalenceMapPropertiesEditionComponent refArtefactArtefactEquivalenceMapPropertiesEditionComponent;

	/**
	 * Parameterized constructor
	 * 
	 * @param refArtefact the EObject to edit
	 * 
	 */
	public RefArtefactPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject refArtefact, String editing_mode) {
		super(editingContext, editing_mode);
		if (refArtefact instanceof RefArtefact) {
			PropertiesEditingProvider provider = null;
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(refArtefact, PropertiesEditingProvider.class);
			refArtefactBasePropertiesEditionComponent = (RefArtefactBasePropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, RefArtefactBasePropertiesEditionComponent.BASE_PART, RefArtefactBasePropertiesEditionComponent.class);
			addSubComponent(refArtefactBasePropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(refArtefact, PropertiesEditingProvider.class);
			refArtefactArtefactEquivalenceMapPropertiesEditionComponent = (RefArtefactArtefactEquivalenceMapPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, RefArtefactArtefactEquivalenceMapPropertiesEditionComponent.ARTEFACTEQUIVALENCEMAP_PART, RefArtefactArtefactEquivalenceMapPropertiesEditionComponent.class);
			addSubComponent(refArtefactArtefactEquivalenceMapPropertiesEditionComponent);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      getPropertiesEditionPart(int, java.lang.String)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(int kind, String key) {
		if (RefArtefactBasePropertiesEditionComponent.BASE_PART.equals(key)) {
			basePart = (RefArtefactPropertiesEditionPart)refArtefactBasePropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)basePart;
		}
		if (RefArtefactArtefactEquivalenceMapPropertiesEditionComponent.ARTEFACTEQUIVALENCEMAP_PART.equals(key)) {
			artefactEquivalenceMapPart = (ArtefactEquivalenceMapPropertiesEditionPart)refArtefactArtefactEquivalenceMapPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)artefactEquivalenceMapPart;
		}
		return super.getPropertiesEditionPart(kind, key);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      setPropertiesEditionPart(java.lang.Object, int,
	 *      org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart)
	 * 
	 */
	public void setPropertiesEditionPart(java.lang.Object key, int kind, IPropertiesEditionPart propertiesEditionPart) {
		if (RefframeworkViewsRepository.RefArtefact.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			basePart = (RefArtefactPropertiesEditionPart)propertiesEditionPart;
		}
		if (RefframeworkViewsRepository.ArtefactEquivalenceMap.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			artefactEquivalenceMapPart = (ArtefactEquivalenceMapPropertiesEditionPart)propertiesEditionPart;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(java.lang.Object key, int kind, EObject element, ResourceSet allResource) {
		if (key == RefframeworkViewsRepository.RefArtefact.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == RefframeworkViewsRepository.ArtefactEquivalenceMap.class) {
			super.initPart(key, kind, element, allResource);
		}
	}
}
