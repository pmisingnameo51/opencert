/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.providers;

import org.eclipse.osgi.util.NLS;

/**
 * 
 * 
 */
public class RefframeworkMessages extends NLS {
	
	private static final String BUNDLE_NAME = "org.eclipse.opencert.pkm.refframework.refframework.providers.refframeworkMessages"; //$NON-NLS-1$

	
	public static String RefFrameworkPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String RefRequirementPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String RefArtefactPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String RefActivityPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String RefRequirementRelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String RefRolePropertiesEditionPart_PropertiesGroupLabel;

	
	public static String RefApplicabilityLevelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String RefCriticalityLevelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String RefTechniquePropertiesEditionPart_PropertiesGroupLabel;

	
	public static String RefArtefactRelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String RefCriticalityApplicabilityPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String RefActivityRelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String RefIndependencyLevelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String RefRecommendationLevelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String RefControlCategoryPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String RefApplicabilityPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String RefApplicabilityRelPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String RefEquivalenceMapPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ActivityRequirementPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ActivityApplicabilityPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ActivityEquivalenceMapPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String RequirementApplicabilityPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String RequirementEquivalenceMapPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String ArtefactEquivalenceMapPropertiesEditionPart_PropertiesGroupLabel;

	
	public static String RoleEquivalenceMapPropertiesEditionPart_PropertiesGroupLabel;


	
	public static String RefFramework_ReadOnly;

	
	public static String RefFramework_Part_Title;

	
	public static String RefRequirement_ReadOnly;

	
	public static String RefRequirement_Part_Title;

	
	public static String RefArtefact_ReadOnly;

	
	public static String RefArtefact_Part_Title;

	
	public static String RefActivity_ReadOnly;

	
	public static String RefActivity_Part_Title;

	
	public static String RefRequirementRel_ReadOnly;

	
	public static String RefRequirementRel_Part_Title;

	
	public static String RefRole_ReadOnly;

	
	public static String RefRole_Part_Title;

	
	public static String RefApplicabilityLevel_ReadOnly;

	
	public static String RefApplicabilityLevel_Part_Title;

	
	public static String RefCriticalityLevel_ReadOnly;

	
	public static String RefCriticalityLevel_Part_Title;

	
	public static String RefTechnique_ReadOnly;

	
	public static String RefTechnique_Part_Title;

	
	public static String RefArtefactRel_ReadOnly;

	
	public static String RefArtefactRel_Part_Title;

	
	public static String RefCriticalityApplicability_ReadOnly;

	
	public static String RefCriticalityApplicability_Part_Title;

	
	public static String RefActivityRel_ReadOnly;

	
	public static String RefActivityRel_Part_Title;

	
	public static String RefIndependencyLevel_ReadOnly;

	
	public static String RefIndependencyLevel_Part_Title;

	
	public static String RefRecommendationLevel_ReadOnly;

	
	public static String RefRecommendationLevel_Part_Title;

	
	public static String RefControlCategory_ReadOnly;

	
	public static String RefControlCategory_Part_Title;

	
	public static String RefApplicability_ReadOnly;

	
	public static String RefApplicability_Part_Title;

	
	public static String RefApplicabilityRel_ReadOnly;

	
	public static String RefApplicabilityRel_Part_Title;

	
	public static String RefEquivalenceMap_ReadOnly;

	
	public static String RefEquivalenceMap_Part_Title;

	
	public static String ActivityRequirement_ReadOnly;

	
	public static String ActivityRequirement_Part_Title;

	
	public static String ActivityApplicability_ReadOnly;

	
	public static String ActivityApplicability_Part_Title;

	
	public static String ActivityEquivalenceMap_ReadOnly;

	
	public static String ActivityEquivalenceMap_Part_Title;

	
	public static String RequirementApplicability_ReadOnly;

	
	public static String RequirementApplicability_Part_Title;

	
	public static String RequirementEquivalenceMap_ReadOnly;

	
	public static String RequirementEquivalenceMap_Part_Title;

	
	public static String ArtefactEquivalenceMap_ReadOnly;

	
	public static String ArtefactEquivalenceMap_Part_Title;

	
	public static String RoleEquivalenceMap_ReadOnly;

	
	public static String RoleEquivalenceMap_Part_Title;

	
	public static String TechniqueEquivalenceMap_ReadOnly;

	
	public static String TechniqueEquivalenceMap_Part_Title;


	
	public static String RefFrameworkPropertiesEditionPart_IdLabel;

	
	public static String RefFrameworkPropertiesEditionPart_NameLabel;

	
	public static String RefFrameworkPropertiesEditionPart_DescriptionLabel;

	
	public static String RefFrameworkPropertiesEditionPart_ScopeLabel;

	
	public static String RefFrameworkPropertiesEditionPart_RevLabel;

	
	public static String RefFrameworkPropertiesEditionPart_PurposeLabel;

	
	public static String RefFrameworkPropertiesEditionPart_PublisherLabel;

	
	public static String RefFrameworkPropertiesEditionPart_IssuedLabel;

	
	public static String RefFrameworkPropertiesEditionPart_OwnedActivitiesLabel;

	
	public static String RefFrameworkPropertiesEditionPart_OwnedArtefactLabel;

	
	public static String RefFrameworkPropertiesEditionPart_OwnedRequirementLabel;

	
	public static String RefFrameworkPropertiesEditionPart_OwnedApplicLevelLabel;

	
	public static String RefFrameworkPropertiesEditionPart_OwnedCriticLevelLabel;

	
	public static String RefFrameworkPropertiesEditionPart_OwnedRoleLabel;

	
	public static String RefFrameworkPropertiesEditionPart_OwnedTechniqueLabel;

	
	public static String RefRequirementPropertiesEditionPart_IdLabel;

	
	public static String RefRequirementPropertiesEditionPart_NameLabel;

	
	public static String RefRequirementPropertiesEditionPart_DescriptionLabel;

	
	public static String RefRequirementPropertiesEditionPart_ReferenceLabel;

	
	public static String RefRequirementPropertiesEditionPart_AssumptionsLabel;

	
	public static String RefRequirementPropertiesEditionPart_RationaleLabel;

	
	public static String RefRequirementPropertiesEditionPart_ImageLabel;

	
	public static String RefRequirementPropertiesEditionPart_AnnotationsLabel;

	
	public static String RefRequirementPropertiesEditionPart_OwnedRelLabel;

	
	public static String RefRequirementPropertiesEditionPart_SubRequirementLabel;

	
	public static String RefArtefactPropertiesEditionPart_IdLabel;

	
	public static String RefArtefactPropertiesEditionPart_NameLabel;

	
	public static String RefArtefactPropertiesEditionPart_DescriptionLabel;

	
	public static String RefArtefactPropertiesEditionPart_ReferenceLabel;

	
	public static String RefArtefactPropertiesEditionPart_ConstrainingRequirementLabel;

	
	public static String RefArtefactPropertiesEditionPart_ApplicableTechniqueLabel;

	
	public static String RefArtefactPropertiesEditionPart_OwnedRelLabel;

	
	public static String RefArtefactPropertiesEditionPart_PropertyLabel;

	
	public static String RefActivityPropertiesEditionPart_IdLabel;

	
	public static String RefActivityPropertiesEditionPart_NameLabel;

	
	public static String RefActivityPropertiesEditionPart_DescriptionLabel;

	
	public static String RefActivityPropertiesEditionPart_ObjectiveLabel;

	
	public static String RefActivityPropertiesEditionPart_ScopeLabel;

	
	public static String RefActivityPropertiesEditionPart_RequiredArtefactLabel;

	
	public static String RefActivityPropertiesEditionPart_ProducedArtefactLabel;

	
	public static String RefActivityPropertiesEditionPart_SubActivityLabel;

	
	public static String RefActivityPropertiesEditionPart_PrecedingActivityLabel;

	
	public static String RefActivityPropertiesEditionPart_RoleLabel;

	
	public static String RefActivityPropertiesEditionPart_ApplicableTechniqueLabel;

	
	public static String RefActivityPropertiesEditionPart_OwnedRelLabel;

	
	public static String RefRequirementRelPropertiesEditionPart_TargetLabel;

	
	public static String RefRequirementRelPropertiesEditionPart_SourceLabel;

	
	public static String RefRequirementRelPropertiesEditionPart_TypeLabel;

	
	public static String RefRolePropertiesEditionPart_IdLabel;

	
	public static String RefRolePropertiesEditionPart_NameLabel;

	
	public static String RefRolePropertiesEditionPart_DescriptionLabel;

	
	public static String RefApplicabilityLevelPropertiesEditionPart_IdLabel;

	
	public static String RefApplicabilityLevelPropertiesEditionPart_NameLabel;

	
	public static String RefApplicabilityLevelPropertiesEditionPart_DescriptionLabel;

	
	public static String RefCriticalityLevelPropertiesEditionPart_IdLabel;

	
	public static String RefCriticalityLevelPropertiesEditionPart_NameLabel;

	
	public static String RefCriticalityLevelPropertiesEditionPart_DescriptionLabel;

	
	public static String RefTechniquePropertiesEditionPart_IdLabel;

	
	public static String RefTechniquePropertiesEditionPart_NameLabel;

	
	public static String RefTechniquePropertiesEditionPart_DescriptionLabel;

	
	public static String RefTechniquePropertiesEditionPart_CriticApplicLabel;

	
	public static String RefTechniquePropertiesEditionPart_AimLabel;

	
	public static String RefArtefactRelPropertiesEditionPart_IdLabel;

	
	public static String RefArtefactRelPropertiesEditionPart_NameLabel;

	
	public static String RefArtefactRelPropertiesEditionPart_DescriptionLabel;

	
	public static String RefArtefactRelPropertiesEditionPart_MaxMultiplicitySourceLabel;

	
	public static String RefArtefactRelPropertiesEditionPart_MinMultiplicitySourceLabel;

	
	public static String RefArtefactRelPropertiesEditionPart_MaxMultiplicityTargetLabel;

	
	public static String RefArtefactRelPropertiesEditionPart_MinMultiplicityTargetLabel;

	
	public static String RefArtefactRelPropertiesEditionPart_ModificationEffectLabel;

	
	public static String RefArtefactRelPropertiesEditionPart_RevocationEffectLabel;

	
	public static String RefArtefactRelPropertiesEditionPart_SourceLabel;

	
	public static String RefArtefactRelPropertiesEditionPart_TargetLabel;

	
	public static String RefCriticalityApplicabilityPropertiesEditionPart_ApplicLevelLabel;

	
	public static String RefCriticalityApplicabilityPropertiesEditionPart_ApplicLevelComboLabel;

	
	public static String RefCriticalityApplicabilityPropertiesEditionPart_CriticLevelLabel;

	
	public static String RefCriticalityApplicabilityPropertiesEditionPart_CriticLevelComboLabel;

	
	public static String RefCriticalityApplicabilityPropertiesEditionPart_CommentLabel;

	
	public static String RefActivityRelPropertiesEditionPart_TypeLabel;

	
	public static String RefActivityRelPropertiesEditionPart_SourceLabel;

	
	public static String RefActivityRelPropertiesEditionPart_TargetLabel;

	
	public static String RefIndependencyLevelPropertiesEditionPart_IdLabel;

	
	public static String RefIndependencyLevelPropertiesEditionPart_NameLabel;

	
	public static String RefIndependencyLevelPropertiesEditionPart_DescriptionLabel;

	
	public static String RefRecommendationLevelPropertiesEditionPart_IdLabel;

	
	public static String RefRecommendationLevelPropertiesEditionPart_NameLabel;

	
	public static String RefRecommendationLevelPropertiesEditionPart_DescriptionLabel;

	
	public static String RefControlCategoryPropertiesEditionPart_IdLabel;

	
	public static String RefControlCategoryPropertiesEditionPart_NameLabel;

	
	public static String RefControlCategoryPropertiesEditionPart_DescriptionLabel;

	
	public static String RefApplicabilityPropertiesEditionPart_IdLabel;

	
	public static String RefApplicabilityPropertiesEditionPart_NameLabel;

	
	public static String RefApplicabilityPropertiesEditionPart_ApplicCriticLabel;

	
	public static String RefApplicabilityPropertiesEditionPart_ApplicCriticTableLabel;

	
	public static String RefApplicabilityPropertiesEditionPart_CommentsLabel;

	
	public static String RefApplicabilityPropertiesEditionPart_ApplicTargetLabel;

	
	public static String RefApplicabilityPropertiesEditionPart_OwnedRelLabel;

	
	public static String RefApplicabilityRelPropertiesEditionPart_TypeLabel;

	
	public static String RefApplicabilityRelPropertiesEditionPart_SourceLabel;

	
	public static String RefApplicabilityRelPropertiesEditionPart_TargetLabel;

	
	public static String RefEquivalenceMapPropertiesEditionPart_IdLabel;

	
	public static String RefEquivalenceMapPropertiesEditionPart_NameLabel;

	
	public static String RefEquivalenceMapPropertiesEditionPart_MapGroupLabel;

	
	public static String RefEquivalenceMapPropertiesEditionPart_MapGroupComboLabel;

	
	public static String RefEquivalenceMapPropertiesEditionPart_TypeLabel;

	
	public static String RefEquivalenceMapPropertiesEditionPart_JustificationLabel;

	
	public static String RefEquivalenceMapPropertiesEditionPart_TargetLabel;

	
	public static String ActivityRequirementPropertiesEditionPart_OwnedRequirementLabel;

	
	public static String ActivityApplicabilityPropertiesEditionPart_ApplicabilityLabel;

	
	public static String ActivityApplicabilityPropertiesEditionPart_ApplicabilityTableLabel;

	
	public static String ActivityEquivalenceMapPropertiesEditionPart_EquivalenceLabel;

	
	public static String ActivityEquivalenceMapPropertiesEditionPart_ActivityEquivalenceMapLabel;

	
	public static String RequirementApplicabilityPropertiesEditionPart_ApplicabilityLabel;

	
	public static String RequirementApplicabilityPropertiesEditionPart_ApplicabilityTableLabel;

	
	public static String RequirementEquivalenceMapPropertiesEditionPart_EquivalenceLabel;

	
	public static String RequirementEquivalenceMapPropertiesEditionPart_RequirementEquivalenceMapLabel;

	
	public static String ArtefactEquivalenceMapPropertiesEditionPart_EquivalenceLabel;

	
	public static String ArtefactEquivalenceMapPropertiesEditionPart_ArtefactEquivalenceMapLabel;

	
	public static String RoleEquivalenceMapPropertiesEditionPart_EquivalenceLabel;

	
	public static String RoleEquivalenceMapPropertiesEditionPart_RoleEquivalenceMapLabel;

	
	public static String TechniqueEquivalenceMapPropertiesEditionPart_EquivalenceLabel;

	
	public static String TechniqueEquivalenceMapPropertiesEditionPart_TechniqueEquivalenceMapLabel;


	
	public static String PropertiesEditionPart_DocumentationLabel;

	
	public static String PropertiesEditionPart_IntegerValueMessage;

	
	public static String PropertiesEditionPart_FloatValueMessage;

	
	public static String PropertiesEditionPart_ShortValueMessage;

	
	public static String PropertiesEditionPart_LongValueMessage;

	
	public static String PropertiesEditionPart_ByteValueMessage;

	
	public static String PropertiesEditionPart_BigIntegerValueMessage;

	
	public static String PropertiesEditionPart_BigDecimalValueMessage;

	
	public static String PropertiesEditionPart_DoubleValueMessage;

	
	public static String PropertiesEditionPart_PropertiesGroupLabel;

	
	public static String PropertiesEditionPart_RequiredFeatureMessage;

	
	public static String PropertiesEditionPart_AddTableViewerLabel;

	
	public static String PropertiesEditionPart_EditTableViewerLabel;

	
	public static String PropertiesEditionPart_RemoveTableViewerLabel;

	
	public static String PropertiesEditionPart_AddListViewerLabel;

	
	public static String PropertiesEditionPart_RemoveListViewerLabel;

	// Start of user code for additionnal NLS Constants
	
	// End of user code

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, RefframeworkMessages.class);
	}

	
	private RefframeworkMessages() {
		//protect instanciation
	}
}
