/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.parts.forms;

// Start of user code for imports
import org.eclipse.emf.common.util.Enumerator;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.EcoreAdapterFactory;

import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;

import org.eclipse.emf.eef.runtime.EEFRuntimePlugin;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;

import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;

import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;

import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;
import org.eclipse.emf.eef.runtime.ui.widgets.EMFComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.EObjectFlatComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.FormUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;

import org.eclipse.opencert.pkm.refframework.refframework.providers.RefframeworkMessages;

// End of user code

/**
 * 
 * 
 */
public class RefArtefactRelPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, RefArtefactRelPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected Text description;
	protected Text maxMultiplicitySource;
	protected Text minMultiplicitySource;
	protected Text maxMultiplicityTarget;
	protected Text minMultiplicityTarget;
	protected EMFComboViewer modificationEffect;
	protected EMFComboViewer revocationEffect;
	protected EObjectFlatComboViewer source;
	protected EObjectFlatComboViewer target;



	/**
	 * For {@link ISection} use only.
	 */
	public RefArtefactRelPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public RefArtefactRelPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence refArtefactRelStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = refArtefactRelStep.addStep(RefframeworkViewsRepository.RefArtefactRel.Properties.class);
		propertiesStep.addStep(RefframeworkViewsRepository.RefArtefactRel.Properties.id);
		propertiesStep.addStep(RefframeworkViewsRepository.RefArtefactRel.Properties.name);
		propertiesStep.addStep(RefframeworkViewsRepository.RefArtefactRel.Properties.description);
		propertiesStep.addStep(RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicitySource);
		propertiesStep.addStep(RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicitySource);
		propertiesStep.addStep(RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicityTarget);
		propertiesStep.addStep(RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicityTarget);
		propertiesStep.addStep(RefframeworkViewsRepository.RefArtefactRel.Properties.modificationEffect);
		propertiesStep.addStep(RefframeworkViewsRepository.RefArtefactRel.Properties.revocationEffect);
		propertiesStep.addStep(RefframeworkViewsRepository.RefArtefactRel.Properties.source);
		propertiesStep.addStep(RefframeworkViewsRepository.RefArtefactRel.Properties.target);
		
		
		composer = new PartComposer(refArtefactRelStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == RefframeworkViewsRepository.RefArtefactRel.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefArtefactRel.Properties.id) {
					return createIdText(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefArtefactRel.Properties.name) {
					return createNameText(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefArtefactRel.Properties.description) {
					return createDescriptionText(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicitySource) {
					return createMaxMultiplicitySourceText(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicitySource) {
					return createMinMultiplicitySourceText(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicityTarget) {
					return createMaxMultiplicityTargetText(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicityTarget) {
					return createMinMultiplicityTargetText(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefArtefactRel.Properties.modificationEffect) {
					return createModificationEffectEMFComboViewer(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefArtefactRel.Properties.revocationEffect) {
					return createRevocationEffectEMFComboViewer(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RefArtefactRel.Properties.source) {
					return createSourceFlatComboViewer(parent, widgetFactory);
				}
				if (key == RefframeworkViewsRepository.RefArtefactRel.Properties.target) {
					return createTargetFlatComboViewer(parent, widgetFactory);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(RefframeworkMessages.RefArtefactRelPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	
	protected Composite createIdText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefArtefactRel.Properties.id, RefframeworkMessages.RefArtefactRelPropertiesEditionPart_IdLabel);
		id = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		id.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							RefArtefactRelPropertiesEditionPartForm.this,
							RefframeworkViewsRepository.RefArtefactRel.Properties.id,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefArtefactRelPropertiesEditionPartForm.this,
									RefframeworkViewsRepository.RefArtefactRel.Properties.id,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, id.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefArtefactRelPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		id.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactRelPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefArtefactRel.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}
		});
		EditingUtils.setID(id, RefframeworkViewsRepository.RefArtefactRel.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefArtefactRel.Properties.id, RefframeworkViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefArtefactRel.Properties.name, RefframeworkMessages.RefArtefactRelPropertiesEditionPart_NameLabel);
		name = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		name.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							RefArtefactRelPropertiesEditionPartForm.this,
							RefframeworkViewsRepository.RefArtefactRel.Properties.name,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefArtefactRelPropertiesEditionPartForm.this,
									RefframeworkViewsRepository.RefArtefactRel.Properties.name,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, name.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefArtefactRelPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		name.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactRelPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefArtefactRel.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}
		});
		EditingUtils.setID(name, RefframeworkViewsRepository.RefArtefactRel.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefArtefactRel.Properties.name, RefframeworkViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	
	protected Composite createDescriptionText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefArtefactRel.Properties.description, RefframeworkMessages.RefArtefactRelPropertiesEditionPart_DescriptionLabel);
		description = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		description.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData descriptionData = new GridData(GridData.FILL_HORIZONTAL);
		description.setLayoutData(descriptionData);
		description.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							RefArtefactRelPropertiesEditionPartForm.this,
							RefframeworkViewsRepository.RefArtefactRel.Properties.description,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefArtefactRelPropertiesEditionPartForm.this,
									RefframeworkViewsRepository.RefArtefactRel.Properties.description,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, description.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefArtefactRelPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		description.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactRelPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefArtefactRel.Properties.description, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, description.getText()));
				}
			}
		});
		EditingUtils.setID(description, RefframeworkViewsRepository.RefArtefactRel.Properties.description);
		EditingUtils.setEEFtype(description, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefArtefactRel.Properties.description, RefframeworkViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createDescriptionText

		// End of user code
		return parent;
	}

	
	protected Composite createMaxMultiplicitySourceText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicitySource, RefframeworkMessages.RefArtefactRelPropertiesEditionPart_MaxMultiplicitySourceLabel);
		maxMultiplicitySource = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		maxMultiplicitySource.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData maxMultiplicitySourceData = new GridData(GridData.FILL_HORIZONTAL);
		maxMultiplicitySource.setLayoutData(maxMultiplicitySourceData);
		maxMultiplicitySource.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							RefArtefactRelPropertiesEditionPartForm.this,
							RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicitySource,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, maxMultiplicitySource.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefArtefactRelPropertiesEditionPartForm.this,
									RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicitySource,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, maxMultiplicitySource.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefArtefactRelPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		maxMultiplicitySource.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactRelPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicitySource, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, maxMultiplicitySource.getText()));
				}
			}
		});
		EditingUtils.setID(maxMultiplicitySource, RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicitySource);
		EditingUtils.setEEFtype(maxMultiplicitySource, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicitySource, RefframeworkViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createMaxMultiplicitySourceText

		// End of user code
		return parent;
	}

	
	protected Composite createMinMultiplicitySourceText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicitySource, RefframeworkMessages.RefArtefactRelPropertiesEditionPart_MinMultiplicitySourceLabel);
		minMultiplicitySource = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		minMultiplicitySource.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData minMultiplicitySourceData = new GridData(GridData.FILL_HORIZONTAL);
		minMultiplicitySource.setLayoutData(minMultiplicitySourceData);
		minMultiplicitySource.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							RefArtefactRelPropertiesEditionPartForm.this,
							RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicitySource,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, minMultiplicitySource.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefArtefactRelPropertiesEditionPartForm.this,
									RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicitySource,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, minMultiplicitySource.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefArtefactRelPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		minMultiplicitySource.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactRelPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicitySource, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, minMultiplicitySource.getText()));
				}
			}
		});
		EditingUtils.setID(minMultiplicitySource, RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicitySource);
		EditingUtils.setEEFtype(minMultiplicitySource, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicitySource, RefframeworkViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createMinMultiplicitySourceText

		// End of user code
		return parent;
	}

	
	protected Composite createMaxMultiplicityTargetText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicityTarget, RefframeworkMessages.RefArtefactRelPropertiesEditionPart_MaxMultiplicityTargetLabel);
		maxMultiplicityTarget = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		maxMultiplicityTarget.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData maxMultiplicityTargetData = new GridData(GridData.FILL_HORIZONTAL);
		maxMultiplicityTarget.setLayoutData(maxMultiplicityTargetData);
		maxMultiplicityTarget.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							RefArtefactRelPropertiesEditionPartForm.this,
							RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicityTarget,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, maxMultiplicityTarget.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefArtefactRelPropertiesEditionPartForm.this,
									RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicityTarget,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, maxMultiplicityTarget.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefArtefactRelPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		maxMultiplicityTarget.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactRelPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicityTarget, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, maxMultiplicityTarget.getText()));
				}
			}
		});
		EditingUtils.setID(maxMultiplicityTarget, RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicityTarget);
		EditingUtils.setEEFtype(maxMultiplicityTarget, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicityTarget, RefframeworkViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createMaxMultiplicityTargetText

		// End of user code
		return parent;
	}

	
	protected Composite createMinMultiplicityTargetText(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicityTarget, RefframeworkMessages.RefArtefactRelPropertiesEditionPart_MinMultiplicityTargetLabel);
		minMultiplicityTarget = widgetFactory.createText(parent, ""); //$NON-NLS-1$
		minMultiplicityTarget.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TEXT_BORDER);
		widgetFactory.paintBordersFor(parent);
		GridData minMultiplicityTargetData = new GridData(GridData.FILL_HORIZONTAL);
		minMultiplicityTarget.setLayoutData(minMultiplicityTargetData);
		minMultiplicityTarget.addFocusListener(new FocusAdapter() {
			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(
							RefArtefactRelPropertiesEditionPartForm.this,
							RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicityTarget,
							PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, minMultiplicityTarget.getText()));
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefArtefactRelPropertiesEditionPartForm.this,
									RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicityTarget,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_LOST,
									null, minMultiplicityTarget.getText()));
				}
			}

			/**
			 * @see org.eclipse.swt.events.FocusAdapter#focusGained(org.eclipse.swt.events.FocusEvent)
			 */
			@Override
			public void focusGained(FocusEvent e) {
				if (propertiesEditionComponent != null) {
					propertiesEditionComponent
							.firePropertiesChanged(new PropertiesEditionEvent(
									RefArtefactRelPropertiesEditionPartForm.this,
									null,
									PropertiesEditionEvent.FOCUS_CHANGED, PropertiesEditionEvent.FOCUS_GAINED,
									null, null));
				}
			}
		});
		minMultiplicityTarget.addKeyListener(new KeyAdapter() {
			/**
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactRelPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicityTarget, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, minMultiplicityTarget.getText()));
				}
			}
		});
		EditingUtils.setID(minMultiplicityTarget, RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicityTarget);
		EditingUtils.setEEFtype(minMultiplicityTarget, "eef::Text"); //$NON-NLS-1$
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicityTarget, RefframeworkViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createMinMultiplicityTargetText

		// End of user code
		return parent;
	}

	
	protected Composite createModificationEffectEMFComboViewer(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefArtefactRel.Properties.modificationEffect, RefframeworkMessages.RefArtefactRelPropertiesEditionPart_ModificationEffectLabel);
		modificationEffect = new EMFComboViewer(parent);
		modificationEffect.setContentProvider(new ArrayContentProvider());
		modificationEffect.setLabelProvider(new AdapterFactoryLabelProvider(EEFRuntimePlugin.getDefault().getAdapterFactory()));
		GridData modificationEffectData = new GridData(GridData.FILL_HORIZONTAL);
		modificationEffect.getCombo().setLayoutData(modificationEffectData);
		modificationEffect.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 * 	
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactRelPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefArtefactRel.Properties.modificationEffect, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getModificationEffect()));
			}

		});
		modificationEffect.setID(RefframeworkViewsRepository.RefArtefactRel.Properties.modificationEffect);
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefArtefactRel.Properties.modificationEffect, RefframeworkViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createModificationEffectEMFComboViewer

		// End of user code
		return parent;
	}

	
	protected Composite createRevocationEffectEMFComboViewer(FormToolkit widgetFactory, Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefArtefactRel.Properties.revocationEffect, RefframeworkMessages.RefArtefactRelPropertiesEditionPart_RevocationEffectLabel);
		revocationEffect = new EMFComboViewer(parent);
		revocationEffect.setContentProvider(new ArrayContentProvider());
		revocationEffect.setLabelProvider(new AdapterFactoryLabelProvider(EEFRuntimePlugin.getDefault().getAdapterFactory()));
		GridData revocationEffectData = new GridData(GridData.FILL_HORIZONTAL);
		revocationEffect.getCombo().setLayoutData(revocationEffectData);
		revocationEffect.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 * 	
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactRelPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefArtefactRel.Properties.revocationEffect, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getRevocationEffect()));
			}

		});
		revocationEffect.setID(RefframeworkViewsRepository.RefArtefactRel.Properties.revocationEffect);
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefArtefactRel.Properties.revocationEffect, RefframeworkViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createRevocationEffectEMFComboViewer

		// End of user code
		return parent;
	}

	/**
	 * @param parent the parent composite
	 * @param widgetFactory factory to use to instanciante widget of the form
	 * 
	 */
	protected Composite createSourceFlatComboViewer(Composite parent, FormToolkit widgetFactory) {
		createDescription(parent, RefframeworkViewsRepository.RefArtefactRel.Properties.source, RefframeworkMessages.RefArtefactRelPropertiesEditionPart_SourceLabel);
		source = new EObjectFlatComboViewer(parent, !propertiesEditionComponent.isRequired(RefframeworkViewsRepository.RefArtefactRel.Properties.source, RefframeworkViewsRepository.FORM_KIND));
		widgetFactory.adapt(source);
		source.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		GridData sourceData = new GridData(GridData.FILL_HORIZONTAL);
		source.setLayoutData(sourceData);
		source.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactRelPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefArtefactRel.Properties.source, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getSource()));
			}

		});
		source.setID(RefframeworkViewsRepository.RefArtefactRel.Properties.source);
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefArtefactRel.Properties.source, RefframeworkViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createSourceFlatComboViewer

		// End of user code
		return parent;
	}

	/**
	 * @param parent the parent composite
	 * @param widgetFactory factory to use to instanciante widget of the form
	 * 
	 */
	protected Composite createTargetFlatComboViewer(Composite parent, FormToolkit widgetFactory) {
		createDescription(parent, RefframeworkViewsRepository.RefArtefactRel.Properties.target, RefframeworkMessages.RefArtefactRelPropertiesEditionPart_TargetLabel);
		target = new EObjectFlatComboViewer(parent, !propertiesEditionComponent.isRequired(RefframeworkViewsRepository.RefArtefactRel.Properties.target, RefframeworkViewsRepository.FORM_KIND));
		widgetFactory.adapt(target);
		target.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		GridData targetData = new GridData(GridData.FILL_HORIZONTAL);
		target.setLayoutData(targetData);
		target.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefArtefactRelPropertiesEditionPartForm.this, RefframeworkViewsRepository.RefArtefactRel.Properties.target, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getTarget()));
			}

		});
		target.setID(RefframeworkViewsRepository.RefArtefactRel.Properties.target);
		FormUtils.createHelpButton(widgetFactory, parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefArtefactRel.Properties.target, RefframeworkViewsRepository.FORM_KIND), null); //$NON-NLS-1$
		// Start of user code for createTargetFlatComboViewer

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefactRel.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(RefframeworkMessages.RefArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefactRel.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(RefframeworkMessages.RefArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#getDescription()
	 * 
	 */
	public String getDescription() {
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#setDescription(String newValue)
	 * 
	 */
	public void setDescription(String newValue) {
		if (newValue != null) {
			description.setText(newValue);
		} else {
			description.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefactRel.Properties.description);
		if (eefElementEditorReadOnlyState && description.isEnabled()) {
			description.setEnabled(false);
			description.setToolTipText(RefframeworkMessages.RefArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !description.isEnabled()) {
			description.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#getMaxMultiplicitySource()
	 * 
	 */
	public String getMaxMultiplicitySource() {
		return maxMultiplicitySource.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#setMaxMultiplicitySource(String newValue)
	 * 
	 */
	public void setMaxMultiplicitySource(String newValue) {
		if (newValue != null) {
			maxMultiplicitySource.setText(newValue);
		} else {
			maxMultiplicitySource.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicitySource);
		if (eefElementEditorReadOnlyState && maxMultiplicitySource.isEnabled()) {
			maxMultiplicitySource.setEnabled(false);
			maxMultiplicitySource.setToolTipText(RefframeworkMessages.RefArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !maxMultiplicitySource.isEnabled()) {
			maxMultiplicitySource.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#getMinMultiplicitySource()
	 * 
	 */
	public String getMinMultiplicitySource() {
		return minMultiplicitySource.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#setMinMultiplicitySource(String newValue)
	 * 
	 */
	public void setMinMultiplicitySource(String newValue) {
		if (newValue != null) {
			minMultiplicitySource.setText(newValue);
		} else {
			minMultiplicitySource.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicitySource);
		if (eefElementEditorReadOnlyState && minMultiplicitySource.isEnabled()) {
			minMultiplicitySource.setEnabled(false);
			minMultiplicitySource.setToolTipText(RefframeworkMessages.RefArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !minMultiplicitySource.isEnabled()) {
			minMultiplicitySource.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#getMaxMultiplicityTarget()
	 * 
	 */
	public String getMaxMultiplicityTarget() {
		return maxMultiplicityTarget.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#setMaxMultiplicityTarget(String newValue)
	 * 
	 */
	public void setMaxMultiplicityTarget(String newValue) {
		if (newValue != null) {
			maxMultiplicityTarget.setText(newValue);
		} else {
			maxMultiplicityTarget.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicityTarget);
		if (eefElementEditorReadOnlyState && maxMultiplicityTarget.isEnabled()) {
			maxMultiplicityTarget.setEnabled(false);
			maxMultiplicityTarget.setToolTipText(RefframeworkMessages.RefArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !maxMultiplicityTarget.isEnabled()) {
			maxMultiplicityTarget.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#getMinMultiplicityTarget()
	 * 
	 */
	public String getMinMultiplicityTarget() {
		return minMultiplicityTarget.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#setMinMultiplicityTarget(String newValue)
	 * 
	 */
	public void setMinMultiplicityTarget(String newValue) {
		if (newValue != null) {
			minMultiplicityTarget.setText(newValue);
		} else {
			minMultiplicityTarget.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicityTarget);
		if (eefElementEditorReadOnlyState && minMultiplicityTarget.isEnabled()) {
			minMultiplicityTarget.setEnabled(false);
			minMultiplicityTarget.setToolTipText(RefframeworkMessages.RefArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !minMultiplicityTarget.isEnabled()) {
			minMultiplicityTarget.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#getModificationEffect()
	 * 
	 */
	public Enumerator getModificationEffect() {
		Enumerator selection = (Enumerator) ((StructuredSelection) modificationEffect.getSelection()).getFirstElement();
		return selection;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#initModificationEffect(Object input, Enumerator current)
	 */
	public void initModificationEffect(Object input, Enumerator current) {
		modificationEffect.setInput(input);
		modificationEffect.modelUpdating(new StructuredSelection(current));
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefactRel.Properties.modificationEffect);
		if (eefElementEditorReadOnlyState && modificationEffect.isEnabled()) {
			modificationEffect.setEnabled(false);
			modificationEffect.setToolTipText(RefframeworkMessages.RefArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !modificationEffect.isEnabled()) {
			modificationEffect.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#setModificationEffect(Enumerator newValue)
	 * 
	 */
	public void setModificationEffect(Enumerator newValue) {
		modificationEffect.modelUpdating(new StructuredSelection(newValue));
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefactRel.Properties.modificationEffect);
		if (eefElementEditorReadOnlyState && modificationEffect.isEnabled()) {
			modificationEffect.setEnabled(false);
			modificationEffect.setToolTipText(RefframeworkMessages.RefArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !modificationEffect.isEnabled()) {
			modificationEffect.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#getRevocationEffect()
	 * 
	 */
	public Enumerator getRevocationEffect() {
		Enumerator selection = (Enumerator) ((StructuredSelection) revocationEffect.getSelection()).getFirstElement();
		return selection;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#initRevocationEffect(Object input, Enumerator current)
	 */
	public void initRevocationEffect(Object input, Enumerator current) {
		revocationEffect.setInput(input);
		revocationEffect.modelUpdating(new StructuredSelection(current));
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefactRel.Properties.revocationEffect);
		if (eefElementEditorReadOnlyState && revocationEffect.isEnabled()) {
			revocationEffect.setEnabled(false);
			revocationEffect.setToolTipText(RefframeworkMessages.RefArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !revocationEffect.isEnabled()) {
			revocationEffect.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#setRevocationEffect(Enumerator newValue)
	 * 
	 */
	public void setRevocationEffect(Enumerator newValue) {
		revocationEffect.modelUpdating(new StructuredSelection(newValue));
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefactRel.Properties.revocationEffect);
		if (eefElementEditorReadOnlyState && revocationEffect.isEnabled()) {
			revocationEffect.setEnabled(false);
			revocationEffect.setToolTipText(RefframeworkMessages.RefArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !revocationEffect.isEnabled()) {
			revocationEffect.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#getSource()
	 * 
	 */
	public EObject getSource() {
		if (source.getSelection() instanceof StructuredSelection) {
			Object firstElement = ((StructuredSelection) source.getSelection()).getFirstElement();
			if (firstElement instanceof EObject)
				return (EObject) firstElement;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#initSource(EObjectFlatComboSettings)
	 */
	public void initSource(EObjectFlatComboSettings settings) {
		source.setInput(settings);
		if (current != null) {
			source.setSelection(new StructuredSelection(settings.getValue()));
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefactRel.Properties.source);
		if (eefElementEditorReadOnlyState && source.isEnabled()) {
			source.setEnabled(false);
			source.setToolTipText(RefframeworkMessages.RefArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !source.isEnabled()) {
			source.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#setSource(EObject newValue)
	 * 
	 */
	public void setSource(EObject newValue) {
		if (newValue != null) {
			source.setSelection(new StructuredSelection(newValue));
		} else {
			source.setSelection(new StructuredSelection()); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefactRel.Properties.source);
		if (eefElementEditorReadOnlyState && source.isEnabled()) {
			source.setEnabled(false);
			source.setToolTipText(RefframeworkMessages.RefArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !source.isEnabled()) {
			source.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#setSourceButtonMode(ButtonsModeEnum newValue)
	 */
	public void setSourceButtonMode(ButtonsModeEnum newValue) {
		source.setButtonMode(newValue);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#addFilterSource(ViewerFilter filter)
	 * 
	 */
	public void addFilterToSource(ViewerFilter filter) {
		source.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#addBusinessFilterSource(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToSource(ViewerFilter filter) {
		source.addBusinessRuleFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#getTarget()
	 * 
	 */
	public EObject getTarget() {
		if (target.getSelection() instanceof StructuredSelection) {
			Object firstElement = ((StructuredSelection) target.getSelection()).getFirstElement();
			if (firstElement instanceof EObject)
				return (EObject) firstElement;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#initTarget(EObjectFlatComboSettings)
	 */
	public void initTarget(EObjectFlatComboSettings settings) {
		target.setInput(settings);
		if (current != null) {
			target.setSelection(new StructuredSelection(settings.getValue()));
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefactRel.Properties.target);
		if (eefElementEditorReadOnlyState && target.isEnabled()) {
			target.setEnabled(false);
			target.setToolTipText(RefframeworkMessages.RefArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !target.isEnabled()) {
			target.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#setTarget(EObject newValue)
	 * 
	 */
	public void setTarget(EObject newValue) {
		if (newValue != null) {
			target.setSelection(new StructuredSelection(newValue));
		} else {
			target.setSelection(new StructuredSelection()); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefArtefactRel.Properties.target);
		if (eefElementEditorReadOnlyState && target.isEnabled()) {
			target.setEnabled(false);
			target.setToolTipText(RefframeworkMessages.RefArtefactRel_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !target.isEnabled()) {
			target.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#setTargetButtonMode(ButtonsModeEnum newValue)
	 */
	public void setTargetButtonMode(ButtonsModeEnum newValue) {
		target.setButtonMode(newValue);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#addFilterTarget(ViewerFilter filter)
	 * 
	 */
	public void addFilterToTarget(ViewerFilter filter) {
		target.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart#addBusinessFilterTarget(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToTarget(ViewerFilter filter) {
		target.addBusinessRuleFilter(filter);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return RefframeworkMessages.RefArtefactRel_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
