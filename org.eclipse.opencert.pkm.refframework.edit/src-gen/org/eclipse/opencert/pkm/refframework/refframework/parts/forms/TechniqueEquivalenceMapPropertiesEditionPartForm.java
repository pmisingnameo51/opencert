/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.views.properties.tabbed.ISection;
import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;
import org.eclipse.opencert.pkm.refframework.refframework.RefEquivalenceMap;
import org.eclipse.opencert.pkm.refframework.refframework.RefTechnique;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;
import org.eclipse.opencert.pkm.refframework.refframework.parts.TechniqueEquivalenceMapPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.providers.RefframeworkMessages;

// End of user code

/**
 * 
 * 
 */
public class TechniqueEquivalenceMapPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, TechniqueEquivalenceMapPropertiesEditionPart {

	protected ReferencesTable equivalence;
	protected List<ViewerFilter> equivalenceBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> equivalenceFilters = new ArrayList<ViewerFilter>();
	protected TableViewer techniqueEquivalenceMap;
	protected List<ViewerFilter> techniqueEquivalenceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> techniqueEquivalenceMapFilters = new ArrayList<ViewerFilter>();
	protected Button addTechniqueEquivalenceMap;
	protected Button removeTechniqueEquivalenceMap;
	protected Button editTechniqueEquivalenceMap;



	/**
	 * For {@link ISection} use only.
	 */
	public TechniqueEquivalenceMapPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public TechniqueEquivalenceMapPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence techniqueEquivalenceMapStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = techniqueEquivalenceMapStep.addStep(RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.class);
		// Start IRR
		// propertiesStep.addStep(RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.equivalence);
		// End IRR
		propertiesStep.addStep(RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.techniqueEquivalenceMap_);
		
		
		composer = new PartComposer(techniqueEquivalenceMapStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.class) {
					//FIXME INVALID CASE INTO template public implementation(editor : ViewElement) in Form Impl for ViewElement properties
				}
				if (key == RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.equivalence) {
					return createEquivalenceTableComposition(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.techniqueEquivalenceMap_) {
					return createTechniqueEquivalenceMapTableComposition(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
//FIXME INVALID CASE INTO template public additionalImplementation(editor : ViewElement, pec : PropertiesEditionComponent, inputPEC : PropertiesEditionComponent) in Form Impl for ViewElement properties

	/**
	 * @param container
	 * 
	 */
	protected Composite createEquivalenceTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.equivalence = new ReferencesTable(getDescription(RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.equivalence, RefframeworkMessages.TechniqueEquivalenceMapPropertiesEditionPart_EquivalenceLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(TechniqueEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				equivalence.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(TechniqueEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				equivalence.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(TechniqueEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				equivalence.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(TechniqueEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				equivalence.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.equivalenceFilters) {
			this.equivalence.addFilter(filter);
		}
		this.equivalence.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.equivalence, RefframeworkViewsRepository.FORM_KIND));
		this.equivalence.createControls(parent, widgetFactory);
		this.equivalence.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(TechniqueEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData equivalenceData = new GridData(GridData.FILL_HORIZONTAL);
		equivalenceData.horizontalSpan = 3;
		this.equivalence.setLayoutData(equivalenceData);
		this.equivalence.setLowerBound(0);
		this.equivalence.setUpperBound(-1);
		equivalence.setID(RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.equivalence);
		equivalence.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createEquivalenceTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createTechniqueEquivalenceMapTableComposition(FormToolkit widgetFactory, Composite container) {
		Composite tableContainer = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableTechniqueEquivalenceMap = widgetFactory.createTable(tableContainer, SWT.FULL_SELECTION | SWT.BORDER);
		tableTechniqueEquivalenceMap.setHeaderVisible(true);
		GridData gdTechniqueEquivalenceMap = new GridData();
		gdTechniqueEquivalenceMap.grabExcessHorizontalSpace = true;
		gdTechniqueEquivalenceMap.horizontalAlignment = GridData.FILL;
		gdTechniqueEquivalenceMap.grabExcessVerticalSpace = true;
		gdTechniqueEquivalenceMap.verticalAlignment = GridData.FILL;
		tableTechniqueEquivalenceMap.setLayoutData(gdTechniqueEquivalenceMap);
		tableTechniqueEquivalenceMap.setLinesVisible(true);

		// Start of user code for columns definition for TechniqueEquivalenceMap
		// Start IRR
		/*
		 * TableColumn name = new TableColumn(tableTechniqueEquivalenceMap,
		 * SWT.NONE); name.setWidth(80); name.setText("Label"); //$NON-NLS-1$
		 */
		TableColumn name = new TableColumn(tableTechniqueEquivalenceMap,
				SWT.NONE);
		name.setWidth(80);
		name.setText("Map"); //$NON-NLS-1$
		TableColumn nameGroup = new TableColumn(tableTechniqueEquivalenceMap,
				SWT.NONE);
		nameGroup.setWidth(100);
		nameGroup.setText("Map Group"); //$NON-NLS-1$
		TableColumn nameArtf = new TableColumn(tableTechniqueEquivalenceMap,
				SWT.NONE);
		nameArtf.setWidth(200);
		nameArtf.setText("Techniques"); //$NON-NLS-1$
		// End IRR
		// End of user code

		techniqueEquivalenceMap = new TableViewer(tableTechniqueEquivalenceMap);
		techniqueEquivalenceMap.setContentProvider(new ArrayContentProvider());
		techniqueEquivalenceMap.setLabelProvider(new ITableLabelProvider() {
			// Start of user code for label provider definition for
			// TechniqueEquivalenceMap
			public String getColumnText(Object object, int columnIndex) {
				// Start IRR
				/*
				 * AdapterFactoryLabelProvider labelProvider = new
				 * AdapterFactoryLabelProvider(adapterFactory); if (object
				 * instanceof EObject) { switch (columnIndex) { case 0: return
				 * labelProvider.getText(object); } }
				 */
				if (object instanceof EObject) {
					RefEquivalenceMap refEquivalenceMap = (RefEquivalenceMap) object;
					switch (columnIndex) {
					case 0:
						return refEquivalenceMap.getId();
					case 1:
						if (refEquivalenceMap.getMapGroup() == null)
							return "";
						else
							return refEquivalenceMap.getMapGroup().getName();
					case 2:
						if (refEquivalenceMap.getTarget() == null)
							return "";
						else {
							EList<RefAssurableElement> LstElement = refEquivalenceMap.getTarget();
							Iterator<RefAssurableElement> iter = LstElement.iterator();
							String sElement = "[";

							// Start IRR 20141205
							
							EObject object1 = null;
							while (iter.hasNext()) {
								object1 = iter.next();
								if (object1 instanceof RefTechnique) {
									RefTechnique refElement = (RefTechnique) object1;
									if (!(refEquivalenceMap.cdoResource().toString().equals(refElement.cdoResource().toString()))) {
										sElement = sElement + refElement.getName() + ",";
									}	 
								}
							}
							
							// End IRR 20141205
							// delete ,
							sElement = sElement.substring(0,
									sElement.length() - 1);
							sElement = sElement + "]";
							return sElement;
						}
					}
				}
				// End IRR
				return ""; //$NON-NLS-1$
			}
			
						public Image getColumnImage(Object element, int columnIndex) {
							return null;
						}
			
			//End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		techniqueEquivalenceMap.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (techniqueEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) techniqueEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(TechniqueEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.techniqueEquivalenceMap_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						techniqueEquivalenceMap.refresh();
					}
				}
			}

		});
		GridData techniqueEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		techniqueEquivalenceMapData.minimumHeight = 120;
		techniqueEquivalenceMapData.heightHint = 120;
		techniqueEquivalenceMap.getTable().setLayoutData(techniqueEquivalenceMapData);
		for (ViewerFilter filter : this.techniqueEquivalenceMapFilters) {
			techniqueEquivalenceMap.addFilter(filter);
		}
		EditingUtils.setID(techniqueEquivalenceMap.getTable(), RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.techniqueEquivalenceMap_);
		EditingUtils.setEEFtype(techniqueEquivalenceMap.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createTechniqueEquivalenceMapPanel(widgetFactory, tableContainer);
		// Start of user code for createTechniqueEquivalenceMapTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createTechniqueEquivalenceMapPanel(FormToolkit widgetFactory, Composite container) {
		Composite techniqueEquivalenceMapPanel = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout techniqueEquivalenceMapPanelLayout = new GridLayout();
		techniqueEquivalenceMapPanelLayout.numColumns = 1;
		techniqueEquivalenceMapPanel.setLayout(techniqueEquivalenceMapPanelLayout);
		addTechniqueEquivalenceMap = widgetFactory.createButton(techniqueEquivalenceMapPanel, RefframeworkMessages.PropertiesEditionPart_AddTableViewerLabel, SWT.NONE);
		GridData addTechniqueEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		addTechniqueEquivalenceMap.setLayoutData(addTechniqueEquivalenceMapData);
		addTechniqueEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(TechniqueEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.techniqueEquivalenceMap_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				techniqueEquivalenceMap.refresh();
			}
		});
		EditingUtils.setID(addTechniqueEquivalenceMap, RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.techniqueEquivalenceMap_);
		EditingUtils.setEEFtype(addTechniqueEquivalenceMap, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeTechniqueEquivalenceMap = widgetFactory.createButton(techniqueEquivalenceMapPanel, RefframeworkMessages.PropertiesEditionPart_RemoveTableViewerLabel, SWT.NONE);
		GridData removeTechniqueEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		removeTechniqueEquivalenceMap.setLayoutData(removeTechniqueEquivalenceMapData);
		removeTechniqueEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (techniqueEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) techniqueEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(TechniqueEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.techniqueEquivalenceMap_, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						techniqueEquivalenceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeTechniqueEquivalenceMap, RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.techniqueEquivalenceMap_);
		EditingUtils.setEEFtype(removeTechniqueEquivalenceMap, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editTechniqueEquivalenceMap = widgetFactory.createButton(techniqueEquivalenceMapPanel, RefframeworkMessages.PropertiesEditionPart_EditTableViewerLabel, SWT.NONE);
		GridData editTechniqueEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		editTechniqueEquivalenceMap.setLayoutData(editTechniqueEquivalenceMapData);
		editTechniqueEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (techniqueEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) techniqueEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(TechniqueEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.techniqueEquivalenceMap_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						techniqueEquivalenceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editTechniqueEquivalenceMap, RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.techniqueEquivalenceMap_);
		EditingUtils.setEEFtype(editTechniqueEquivalenceMap, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createTechniqueEquivalenceMapPanel

		// End of user code
		return techniqueEquivalenceMapPanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.TechniqueEquivalenceMapPropertiesEditionPart#initEquivalence(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initEquivalence(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		equivalence.setContentProvider(contentProvider);
		equivalence.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.equivalence);
		if (eefElementEditorReadOnlyState && equivalence.isEnabled()) {
			equivalence.setEnabled(false);
			equivalence.setToolTipText(RefframeworkMessages.TechniqueEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !equivalence.isEnabled()) {
			equivalence.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.TechniqueEquivalenceMapPropertiesEditionPart#updateEquivalence()
	 * 
	 */
	public void updateEquivalence() {
	equivalence.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.TechniqueEquivalenceMapPropertiesEditionPart#addFilterEquivalence(ViewerFilter filter)
	 * 
	 */
	public void addFilterToEquivalence(ViewerFilter filter) {
		equivalenceFilters.add(filter);
		if (this.equivalence != null) {
			this.equivalence.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.TechniqueEquivalenceMapPropertiesEditionPart#addBusinessFilterEquivalence(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToEquivalence(ViewerFilter filter) {
		equivalenceBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.TechniqueEquivalenceMapPropertiesEditionPart#isContainedInEquivalenceTable(EObject element)
	 * 
	 */
	public boolean isContainedInEquivalenceTable(EObject element) {
		return ((ReferencesTableSettings)equivalence.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.TechniqueEquivalenceMapPropertiesEditionPart#initTechniqueEquivalenceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initTechniqueEquivalenceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		techniqueEquivalenceMap.setContentProvider(contentProvider);
		techniqueEquivalenceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.TechniqueEquivalenceMap.Properties.techniqueEquivalenceMap_);
		if (eefElementEditorReadOnlyState && techniqueEquivalenceMap.getTable().isEnabled()) {
			techniqueEquivalenceMap.getTable().setEnabled(false);
			techniqueEquivalenceMap.getTable().setToolTipText(RefframeworkMessages.TechniqueEquivalenceMap_ReadOnly);
			addTechniqueEquivalenceMap.setEnabled(false);
			addTechniqueEquivalenceMap.setToolTipText(RefframeworkMessages.TechniqueEquivalenceMap_ReadOnly);
			removeTechniqueEquivalenceMap.setEnabled(false);
			removeTechniqueEquivalenceMap.setToolTipText(RefframeworkMessages.TechniqueEquivalenceMap_ReadOnly);
			editTechniqueEquivalenceMap.setEnabled(false);
			editTechniqueEquivalenceMap.setToolTipText(RefframeworkMessages.TechniqueEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !techniqueEquivalenceMap.getTable().isEnabled()) {
			techniqueEquivalenceMap.getTable().setEnabled(true);
			addTechniqueEquivalenceMap.setEnabled(true);
			removeTechniqueEquivalenceMap.setEnabled(true);
			editTechniqueEquivalenceMap.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.TechniqueEquivalenceMapPropertiesEditionPart#updateTechniqueEquivalenceMap()
	 * 
	 */
	public void updateTechniqueEquivalenceMap() {
	techniqueEquivalenceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.TechniqueEquivalenceMapPropertiesEditionPart#addFilterTechniqueEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToTechniqueEquivalenceMap(ViewerFilter filter) {
		techniqueEquivalenceMapFilters.add(filter);
		if (this.techniqueEquivalenceMap != null) {
			this.techniqueEquivalenceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.TechniqueEquivalenceMapPropertiesEditionPart#addBusinessFilterTechniqueEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToTechniqueEquivalenceMap(ViewerFilter filter) {
		techniqueEquivalenceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.TechniqueEquivalenceMapPropertiesEditionPart#isContainedInTechniqueEquivalenceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInTechniqueEquivalenceMapTable(EObject element) {
		return ((ReferencesTableSettings)techniqueEquivalenceMap.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return RefframeworkMessages.TechniqueEquivalenceMap_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
