/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;
import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.pkm.refframework.refframework.RefActivity;
import org.eclipse.opencert.pkm.refframework.refframework.RefTechnique;
import org.eclipse.opencert.pkm.refframework.refframework.RefApplicability;
import org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityRel;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefact;
import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;
import org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityApplicability;
import org.eclipse.opencert.pkm.refframework.refframework.RefRequirement;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefApplicabilityPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class RefApplicabilityPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for applicCritic ReferencesTable
	 */
	protected ReferencesTableSettings applicCriticSettings;
	
	/**
	 * Settings for applicCriticTable ReferencesTable
	 */
	protected ReferencesTableSettings applicCriticTableSettings;
	
	/**
	 * Settings for applicTarget EObjectFlatComboViewer
	 */
	private EObjectFlatComboSettings applicTargetSettings;
	
	/**
	 * Settings for ownedRel ReferencesTable
	 */
	protected ReferencesTableSettings ownedRelSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public RefApplicabilityPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject refApplicability, String editing_mode) {
		super(editingContext, refApplicability, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = RefframeworkViewsRepository.class;
		partKey = RefframeworkViewsRepository.RefApplicability.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		//ALC Start
		final Resource thisResource = elt.eResource();
		final EObject thisSourceObject  = elt.eContainer();
		//ALC End
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final RefApplicability refApplicability = (RefApplicability)elt;
			final RefApplicabilityPropertiesEditionPart basePart = (RefApplicabilityPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(RefframeworkViewsRepository.RefApplicability.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, refApplicability.getId()));
			
			if (isAccessible(RefframeworkViewsRepository.RefApplicability.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, refApplicability.getName()));
			
			if (isAccessible(RefframeworkViewsRepository.RefApplicability.Properties.applicCritic)) {
				applicCriticSettings = new ReferencesTableSettings(refApplicability, RefframeworkPackage.eINSTANCE.getRefApplicability_ApplicCritic());
				basePart.initApplicCritic(applicCriticSettings);
			}
			if (isAccessible(RefframeworkViewsRepository.RefApplicability.Properties.applicCriticTable)) {
				applicCriticTableSettings = new ReferencesTableSettings(refApplicability, RefframeworkPackage.eINSTANCE.getRefApplicability_ApplicCritic());
				basePart.initApplicCriticTable(applicCriticTableSettings);
			}
			if (isAccessible(RefframeworkViewsRepository.RefApplicability.Properties.comments))
				basePart.setComments(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, refApplicability.getComments()));
			if (isAccessible(RefframeworkViewsRepository.RefApplicability.Properties.applicTarget)) {
				// init part
				applicTargetSettings = new EObjectFlatComboSettings(refApplicability, RefframeworkPackage.eINSTANCE.getRefApplicability_ApplicTarget());
				basePart.initApplicTarget(applicTargetSettings);
				// set the button mode
				basePart.setApplicTargetButtonMode(ButtonsModeEnum.BROWSE);
			}
			if (isAccessible(RefframeworkViewsRepository.RefApplicability.Properties.ownedRel)) {
				ownedRelSettings = new ReferencesTableSettings(refApplicability, RefframeworkPackage.eINSTANCE.getRefApplicability_OwnedRel());
				basePart.initOwnedRel(ownedRelSettings);
			}
			// init filters
			
			
			if (isAccessible(RefframeworkViewsRepository.RefApplicability.Properties.applicCritic)) {
				basePart.addFilterToApplicCritic(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof RefCriticalityApplicability); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for applicCritic
				// End of user code
			}
			if (isAccessible(RefframeworkViewsRepository.RefApplicability.Properties.applicCriticTable)) {
				basePart.addFilterToApplicCriticTable(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof RefCriticalityApplicability); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for applicCriticTable
				// End of user code
			}
			
			if (isAccessible(RefframeworkViewsRepository.RefApplicability.Properties.applicTarget)) {
				basePart.addFilterToApplicTarget(new ViewerFilter() {
				
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						//*** Start ALC***: 
						//Filter changed, only can be target of an Applicability table of a RefActivity, any RefRequirement in the same model as the RefActivity 
						//only can be target of an Applicability table of a RefRequirement, any RefArtefact or RefTechnique in the same model as the RefRequirement 
						{
							if	(element instanceof String && element.equals("")) return true;
							
							if(thisSourceObject instanceof RefActivity){
								if (element instanceof RefRequirement){
									EObject elementEObject = ((EObject)element);
									if (elementEObject.eResource().equals(thisResource)) return true; //$NON-NLS-1$
								}
							}
							if(thisSourceObject instanceof RefRequirement){
								if ((element instanceof RefArtefact) || (element instanceof RefTechnique)){
									EObject elementEObject = ((EObject)element);
									if (elementEObject.eResource().equals(thisResource)) return true; //$NON-NLS-1$
								}
							}

						}
						return false;
						//*** End ALC***: Filter changed
					}
					
				});
				// Start of user code for additional businessfilters for applicTarget
				// End of user code
			}
			if (isAccessible(RefframeworkViewsRepository.RefApplicability.Properties.ownedRel)) {
				basePart.addFilterToOwnedRel(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof RefApplicabilityRel); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for ownedRel
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}










	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == RefframeworkViewsRepository.RefApplicability.Properties.id) {
			return GeneralPackage.eINSTANCE.getNamedElement_Id();
		}
		if (editorKey == RefframeworkViewsRepository.RefApplicability.Properties.name) {
			return GeneralPackage.eINSTANCE.getNamedElement_Name();
		}
		if (editorKey == RefframeworkViewsRepository.RefApplicability.Properties.applicCritic) {
			return RefframeworkPackage.eINSTANCE.getRefApplicability_ApplicCritic();
		}
		if (editorKey == RefframeworkViewsRepository.RefApplicability.Properties.applicCriticTable) {
			return RefframeworkPackage.eINSTANCE.getRefApplicability_ApplicCritic();
		}
		if (editorKey == RefframeworkViewsRepository.RefApplicability.Properties.comments) {
			return RefframeworkPackage.eINSTANCE.getRefApplicability_Comments();
		}
		if (editorKey == RefframeworkViewsRepository.RefApplicability.Properties.applicTarget) {
			return RefframeworkPackage.eINSTANCE.getRefApplicability_ApplicTarget();
		}
		if (editorKey == RefframeworkViewsRepository.RefApplicability.Properties.ownedRel) {
			return RefframeworkPackage.eINSTANCE.getRefApplicability_OwnedRel();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		RefApplicability refApplicability = (RefApplicability)semanticObject;
		if (RefframeworkViewsRepository.RefApplicability.Properties.id == event.getAffectedEditor()) {
			refApplicability.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefApplicability.Properties.name == event.getAffectedEditor()) {
			refApplicability.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefApplicability.Properties.applicCritic == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, applicCriticSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				applicCriticSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				applicCriticSettings.move(event.getNewIndex(), (RefCriticalityApplicability) event.getNewValue());
			}
		}
		if (RefframeworkViewsRepository.RefApplicability.Properties.applicCriticTable == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, applicCriticTableSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				applicCriticTableSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				applicCriticTableSettings.move(event.getNewIndex(), (RefCriticalityApplicability) event.getNewValue());
			}
		}
		if (RefframeworkViewsRepository.RefApplicability.Properties.comments == event.getAffectedEditor()) {
			refApplicability.setComments((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefApplicability.Properties.applicTarget == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.SET) {
				applicTargetSettings.setToReference((RefAssurableElement)event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, applicTargetSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			}
		}
		if (RefframeworkViewsRepository.RefApplicability.Properties.ownedRel == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, ownedRelSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				ownedRelSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				ownedRelSettings.move(event.getNewIndex(), (RefApplicabilityRel) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			RefApplicabilityPropertiesEditionPart basePart = (RefApplicabilityPropertiesEditionPart)editingPart;
			if (GeneralPackage.eINSTANCE.getNamedElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefApplicability.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (GeneralPackage.eINSTANCE.getNamedElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefApplicability.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefApplicability_ApplicCritic().equals(msg.getFeature()) && isAccessible(RefframeworkViewsRepository.RefApplicability.Properties.applicCritic))
				basePart.updateApplicCritic();
			if (RefframeworkPackage.eINSTANCE.getRefApplicability_ApplicCritic().equals(msg.getFeature()) && isAccessible(RefframeworkViewsRepository.RefApplicability.Properties.applicCriticTable))
				basePart.updateApplicCriticTable();
			if (RefframeworkPackage.eINSTANCE.getRefApplicability_Comments().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefApplicability.Properties.comments)){
				if (msg.getNewValue() != null) {
					basePart.setComments(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setComments("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefApplicability_ApplicTarget().equals(msg.getFeature()) && basePart != null && isAccessible(RefframeworkViewsRepository.RefApplicability.Properties.applicTarget))
				basePart.setApplicTarget((EObject)msg.getNewValue());
			if (RefframeworkPackage.eINSTANCE.getRefApplicability_OwnedRel().equals(msg.getFeature()) && isAccessible(RefframeworkViewsRepository.RefApplicability.Properties.ownedRel))
				basePart.updateOwnedRel();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			GeneralPackage.eINSTANCE.getNamedElement_Id(),
			GeneralPackage.eINSTANCE.getNamedElement_Name(),
			RefframeworkPackage.eINSTANCE.getRefApplicability_ApplicCritic(),
			RefframeworkPackage.eINSTANCE.getRefApplicability_ApplicCritic(),
			RefframeworkPackage.eINSTANCE.getRefApplicability_Comments(),
			RefframeworkPackage.eINSTANCE.getRefApplicability_ApplicTarget(),
			RefframeworkPackage.eINSTANCE.getRefApplicability_OwnedRel()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (RefframeworkViewsRepository.RefApplicability.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefApplicability.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefApplicability.Properties.comments == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(RefframeworkPackage.eINSTANCE.getRefApplicability_Comments().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(RefframeworkPackage.eINSTANCE.getRefApplicability_Comments().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
