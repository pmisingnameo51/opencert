/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.views.properties.tabbed.ISection;
import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;
import org.eclipse.opencert.pkm.refframework.refframework.RefEquivalenceMap;
import org.eclipse.opencert.pkm.refframework.refframework.RefRole;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RoleEquivalenceMapPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.providers.RefframeworkMessages;

// End of user code

/**
 * 
 * 
 */
public class RoleEquivalenceMapPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, RoleEquivalenceMapPropertiesEditionPart {

	protected ReferencesTable equivalence;
	protected List<ViewerFilter> equivalenceBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> equivalenceFilters = new ArrayList<ViewerFilter>();
	protected TableViewer roleEquivalenceMap;
	protected List<ViewerFilter> roleEquivalenceMapBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> roleEquivalenceMapFilters = new ArrayList<ViewerFilter>();
	protected Button addRoleEquivalenceMap;
	protected Button removeRoleEquivalenceMap;
	protected Button editRoleEquivalenceMap;



	/**
	 * For {@link ISection} use only.
	 */
	public RoleEquivalenceMapPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public RoleEquivalenceMapPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence roleEquivalenceMapStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = roleEquivalenceMapStep.addStep(RefframeworkViewsRepository.RoleEquivalenceMap.Properties.class);
		// Start IRR
		// propertiesStep.addStep(RefframeworkViewsRepository.RoleEquivalenceMap.Properties.equivalence);
		// End IRR
		propertiesStep.addStep(RefframeworkViewsRepository.RoleEquivalenceMap.Properties.roleEquivalenceMap_);
		
		
		composer = new PartComposer(roleEquivalenceMapStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == RefframeworkViewsRepository.RoleEquivalenceMap.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RoleEquivalenceMap.Properties.equivalence) {
					return createEquivalenceTableComposition(widgetFactory, parent);
				}
				if (key == RefframeworkViewsRepository.RoleEquivalenceMap.Properties.roleEquivalenceMap_) {
					return createRoleEquivalenceMapTableComposition(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(RefframeworkMessages.RoleEquivalenceMapPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createEquivalenceTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.equivalence = new ReferencesTable(getDescription(RefframeworkViewsRepository.RoleEquivalenceMap.Properties.equivalence, RefframeworkMessages.RoleEquivalenceMapPropertiesEditionPart_EquivalenceLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RoleEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.RoleEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				equivalence.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RoleEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.RoleEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				equivalence.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RoleEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.RoleEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				equivalence.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RoleEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.RoleEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				equivalence.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.equivalenceFilters) {
			this.equivalence.addFilter(filter);
		}
		this.equivalence.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RoleEquivalenceMap.Properties.equivalence, RefframeworkViewsRepository.FORM_KIND));
		this.equivalence.createControls(parent, widgetFactory);
		this.equivalence.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RoleEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.RoleEquivalenceMap.Properties.equivalence, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData equivalenceData = new GridData(GridData.FILL_HORIZONTAL);
		equivalenceData.horizontalSpan = 3;
		this.equivalence.setLayoutData(equivalenceData);
		this.equivalence.setLowerBound(0);
		this.equivalence.setUpperBound(-1);
		equivalence.setID(RefframeworkViewsRepository.RoleEquivalenceMap.Properties.equivalence);
		equivalence.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createEquivalenceTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createRoleEquivalenceMapTableComposition(FormToolkit widgetFactory, Composite container) {
		Composite tableContainer = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout tableContainerLayout = new GridLayout();
		GridData tableContainerData = new GridData(GridData.FILL_BOTH);
		tableContainerData.horizontalSpan = 3;
		tableContainer.setLayoutData(tableContainerData);
		tableContainerLayout.numColumns = 2;
		tableContainer.setLayout(tableContainerLayout);
		org.eclipse.swt.widgets.Table tableRoleEquivalenceMap = widgetFactory.createTable(tableContainer, SWT.FULL_SELECTION | SWT.BORDER);
		tableRoleEquivalenceMap.setHeaderVisible(true);
		GridData gdRoleEquivalenceMap = new GridData();
		gdRoleEquivalenceMap.grabExcessHorizontalSpace = true;
		gdRoleEquivalenceMap.horizontalAlignment = GridData.FILL;
		gdRoleEquivalenceMap.grabExcessVerticalSpace = true;
		gdRoleEquivalenceMap.verticalAlignment = GridData.FILL;
		tableRoleEquivalenceMap.setLayoutData(gdRoleEquivalenceMap);
		tableRoleEquivalenceMap.setLinesVisible(true);

		// Start of user code for columns definition for RoleEquivalenceMap
		/*
		 * TableColumn name = new TableColumn(tableRoleEquivalenceMap,
		 * SWT.NONE); name.setWidth(80); name.setText("Label"); //$NON-NLS-1$
		 */
		TableColumn name = new TableColumn(tableRoleEquivalenceMap, SWT.NONE);
		name.setWidth(80);
		name.setText("Map"); //$NON-NLS-1$
		TableColumn nameGroup = new TableColumn(tableRoleEquivalenceMap,
				SWT.NONE);
		nameGroup.setWidth(100);
		nameGroup.setText("Map Group"); //$NON-NLS-1$
		TableColumn nameArtf = new TableColumn(tableRoleEquivalenceMap,
				SWT.NONE);
		nameArtf.setWidth(200);
		nameArtf.setText("Roles"); //$NON-NLS-1$
		// End IRR
		// End of user code

		roleEquivalenceMap = new TableViewer(tableRoleEquivalenceMap);
		roleEquivalenceMap.setContentProvider(new ArrayContentProvider());
		roleEquivalenceMap.setLabelProvider(new ITableLabelProvider() {
			//Start of user code for label provider definition for RoleEquivalenceMap
			public String getColumnText(Object object, int columnIndex) {
				// Start IRR
				/*
				 * AdapterFactoryLabelProvider labelProvider = new
				 * AdapterFactoryLabelProvider(adapterFactory); if (object
				 * instanceof EObject) { switch (columnIndex) { case 0: return
				 * labelProvider.getText(object); } }
				 */
				if (object instanceof EObject) {
					RefEquivalenceMap refEquivalenceMap = (RefEquivalenceMap) object;
					switch (columnIndex) {
					case 0:
						return refEquivalenceMap.getId();
					case 1:
						if (refEquivalenceMap.getMapGroup() == null)
							return "";
						else
							return refEquivalenceMap.getMapGroup().getName();
					case 2:
						if (refEquivalenceMap.getTarget() == null)
							return "";
						else {
							EList<RefAssurableElement> LstElement = refEquivalenceMap
									.getTarget();
							Iterator<RefAssurableElement> iter = LstElement
									.iterator();
							String sElement = "[";
							// Start IRR 20141205
							
							EObject object1 = null;
							while (iter.hasNext()) {
								object1 = iter.next();
								if (object1 instanceof RefRole) {
									RefRole refElement = (RefRole) object1;
									if (!(refEquivalenceMap.cdoResource().toString().equals(refElement.cdoResource().toString()))) {
										sElement = sElement + refElement.getName() + ",";
									}	
								}	
							}
							
							// End IRR 20141205
							// delete ,
							sElement = sElement.substring(0,
									sElement.length() - 1);
							sElement = sElement + "]";
							return sElement;
						}

					}
				}
				// End IRR
				return ""; //$NON-NLS-1$
			}

			public Image getColumnImage(Object element, int columnIndex) {
				return null;
			}

			// End of user code

			public void addListener(ILabelProviderListener listener) {
			}

			public void dispose() {
			}

			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			public void removeListener(ILabelProviderListener listener) {
			}

		});
		roleEquivalenceMap.getTable().addListener(SWT.MouseDoubleClick, new Listener(){

			public void handleEvent(Event event) {
				if (roleEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) roleEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RoleEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.RoleEquivalenceMap.Properties.roleEquivalenceMap_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						roleEquivalenceMap.refresh();
					}
				}
			}

		});
		GridData roleEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		roleEquivalenceMapData.minimumHeight = 120;
		roleEquivalenceMapData.heightHint = 120;
		roleEquivalenceMap.getTable().setLayoutData(roleEquivalenceMapData);
		for (ViewerFilter filter : this.roleEquivalenceMapFilters) {
			roleEquivalenceMap.addFilter(filter);
		}
		EditingUtils.setID(roleEquivalenceMap.getTable(), RefframeworkViewsRepository.RoleEquivalenceMap.Properties.roleEquivalenceMap_);
		EditingUtils.setEEFtype(roleEquivalenceMap.getTable(), "eef::TableComposition::field"); //$NON-NLS-1$
		createRoleEquivalenceMapPanel(widgetFactory, tableContainer);
		// Start of user code for createRoleEquivalenceMapTableComposition

		// End of user code
		return container;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createRoleEquivalenceMapPanel(FormToolkit widgetFactory, Composite container) {
		Composite roleEquivalenceMapPanel = widgetFactory.createComposite(container, SWT.NONE);
		GridLayout roleEquivalenceMapPanelLayout = new GridLayout();
		roleEquivalenceMapPanelLayout.numColumns = 1;
		roleEquivalenceMapPanel.setLayout(roleEquivalenceMapPanelLayout);
		addRoleEquivalenceMap = widgetFactory.createButton(roleEquivalenceMapPanel, RefframeworkMessages.PropertiesEditionPart_AddTableViewerLabel, SWT.NONE);
		GridData addRoleEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		addRoleEquivalenceMap.setLayoutData(addRoleEquivalenceMapData);
		addRoleEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RoleEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.RoleEquivalenceMap.Properties.roleEquivalenceMap_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				roleEquivalenceMap.refresh();
			}
		});
		EditingUtils.setID(addRoleEquivalenceMap, RefframeworkViewsRepository.RoleEquivalenceMap.Properties.roleEquivalenceMap_);
		EditingUtils.setEEFtype(addRoleEquivalenceMap, "eef::TableComposition::addbutton"); //$NON-NLS-1$
		removeRoleEquivalenceMap = widgetFactory.createButton(roleEquivalenceMapPanel, RefframeworkMessages.PropertiesEditionPart_RemoveTableViewerLabel, SWT.NONE);
		GridData removeRoleEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		removeRoleEquivalenceMap.setLayoutData(removeRoleEquivalenceMapData);
		removeRoleEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (roleEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) roleEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						EObject selectedElement = (EObject) selection.getFirstElement();
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RoleEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.RoleEquivalenceMap.Properties.roleEquivalenceMap_, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.REMOVE, null, selectedElement));
						roleEquivalenceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(removeRoleEquivalenceMap, RefframeworkViewsRepository.RoleEquivalenceMap.Properties.roleEquivalenceMap_);
		EditingUtils.setEEFtype(removeRoleEquivalenceMap, "eef::TableComposition::removebutton"); //$NON-NLS-1$
		editRoleEquivalenceMap = widgetFactory.createButton(roleEquivalenceMapPanel, RefframeworkMessages.PropertiesEditionPart_EditTableViewerLabel, SWT.NONE);
		GridData editRoleEquivalenceMapData = new GridData(GridData.FILL_HORIZONTAL);
		editRoleEquivalenceMap.setLayoutData(editRoleEquivalenceMapData);
		editRoleEquivalenceMap.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 * 
			 */
			public void widgetSelected(SelectionEvent e) {
				if (roleEquivalenceMap.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) roleEquivalenceMap.getSelection();
					if (selection.getFirstElement() instanceof EObject) {
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RoleEquivalenceMapPropertiesEditionPartForm.this, RefframeworkViewsRepository.RoleEquivalenceMap.Properties.roleEquivalenceMap_, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, selection.getFirstElement()));
						roleEquivalenceMap.refresh();
					}
				}
			}

		});
		EditingUtils.setID(editRoleEquivalenceMap, RefframeworkViewsRepository.RoleEquivalenceMap.Properties.roleEquivalenceMap_);
		EditingUtils.setEEFtype(editRoleEquivalenceMap, "eef::TableComposition::editbutton"); //$NON-NLS-1$
		// Start of user code for createRoleEquivalenceMapPanel

		// End of user code
		return roleEquivalenceMapPanel;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RoleEquivalenceMapPropertiesEditionPart#initEquivalence(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initEquivalence(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		equivalence.setContentProvider(contentProvider);
		equivalence.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RoleEquivalenceMap.Properties.equivalence);
		if (eefElementEditorReadOnlyState && equivalence.isEnabled()) {
			equivalence.setEnabled(false);
			equivalence.setToolTipText(RefframeworkMessages.RoleEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !equivalence.isEnabled()) {
			equivalence.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RoleEquivalenceMapPropertiesEditionPart#updateEquivalence()
	 * 
	 */
	public void updateEquivalence() {
	equivalence.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RoleEquivalenceMapPropertiesEditionPart#addFilterEquivalence(ViewerFilter filter)
	 * 
	 */
	public void addFilterToEquivalence(ViewerFilter filter) {
		equivalenceFilters.add(filter);
		if (this.equivalence != null) {
			this.equivalence.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RoleEquivalenceMapPropertiesEditionPart#addBusinessFilterEquivalence(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToEquivalence(ViewerFilter filter) {
		equivalenceBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RoleEquivalenceMapPropertiesEditionPart#isContainedInEquivalenceTable(EObject element)
	 * 
	 */
	public boolean isContainedInEquivalenceTable(EObject element) {
		return ((ReferencesTableSettings)equivalence.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RoleEquivalenceMapPropertiesEditionPart#initRoleEquivalenceMap(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initRoleEquivalenceMap(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		roleEquivalenceMap.setContentProvider(contentProvider);
		roleEquivalenceMap.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RoleEquivalenceMap.Properties.roleEquivalenceMap_);
		if (eefElementEditorReadOnlyState && roleEquivalenceMap.getTable().isEnabled()) {
			roleEquivalenceMap.getTable().setEnabled(false);
			roleEquivalenceMap.getTable().setToolTipText(RefframeworkMessages.RoleEquivalenceMap_ReadOnly);
			addRoleEquivalenceMap.setEnabled(false);
			addRoleEquivalenceMap.setToolTipText(RefframeworkMessages.RoleEquivalenceMap_ReadOnly);
			removeRoleEquivalenceMap.setEnabled(false);
			removeRoleEquivalenceMap.setToolTipText(RefframeworkMessages.RoleEquivalenceMap_ReadOnly);
			editRoleEquivalenceMap.setEnabled(false);
			editRoleEquivalenceMap.setToolTipText(RefframeworkMessages.RoleEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !roleEquivalenceMap.getTable().isEnabled()) {
			roleEquivalenceMap.getTable().setEnabled(true);
			addRoleEquivalenceMap.setEnabled(true);
			removeRoleEquivalenceMap.setEnabled(true);
			editRoleEquivalenceMap.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RoleEquivalenceMapPropertiesEditionPart#updateRoleEquivalenceMap()
	 * 
	 */
	public void updateRoleEquivalenceMap() {
	roleEquivalenceMap.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RoleEquivalenceMapPropertiesEditionPart#addFilterRoleEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addFilterToRoleEquivalenceMap(ViewerFilter filter) {
		roleEquivalenceMapFilters.add(filter);
		if (this.roleEquivalenceMap != null) {
			this.roleEquivalenceMap.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RoleEquivalenceMapPropertiesEditionPart#addBusinessFilterRoleEquivalenceMap(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToRoleEquivalenceMap(ViewerFilter filter) {
		roleEquivalenceMapBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RoleEquivalenceMapPropertiesEditionPart#isContainedInRoleEquivalenceMapTable(EObject element)
	 * 
	 */
	public boolean isContainedInRoleEquivalenceMapTable(EObject element) {
		return ((ReferencesTableSettings)roleEquivalenceMap.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return RefframeworkMessages.RoleEquivalenceMap_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
