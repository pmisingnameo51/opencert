/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.parts.impl;

// Start of user code for imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.eef.runtime.EEFRuntimePlugin;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.filters.EObjectFilter;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.parts.CompositePropertiesEditionPart;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.providers.EMFListContentProvider;
import org.eclipse.emf.eef.runtime.ui.utils.EditingUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;
import org.eclipse.emf.eef.runtime.ui.widgets.EMFComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.EObjectFlatComboViewer;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.SWTUtils;
import org.eclipse.emf.eef.runtime.ui.widgets.TabElementTreeSelectionDialog;
import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.opencert.pkm.refframework.refframework.RefActivity;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefact;
import org.eclipse.opencert.pkm.refframework.refframework.RefRequirement;
import org.eclipse.opencert.pkm.refframework.refframework.RefRole;
import org.eclipse.opencert.pkm.refframework.refframework.RefTechnique;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;
import org.eclipse.opencert.pkm.refframework.refframework.providers.RefframeworkMessages;
import org.eclipse.opencert.pkm.refframework.refframework.utils.CheckboxTreeViewerExt;
import org.eclipse.opencert.pkm.refframework.refframework.utils.TreeViewerExt;

// End of user code

/**
 * 
 * 
 */
public class RefEquivalenceMapPropertiesEditionPartImpl extends CompositePropertiesEditionPart implements ISWTPropertiesEditionPart, RefEquivalenceMapPropertiesEditionPart {

	protected Text id;
	protected Text name;
	protected EObjectFlatComboViewer mapGroup;
	protected EMFComboViewer mapGroupCombo;
	protected EMFComboViewer type;
	protected Text justification;
	protected ReferencesTable target;
	protected List<ViewerFilter> targetBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> targetFilters = new ArrayList<ViewerFilter>();
	
	/*// Start IRR
	private static final String OPENCERT_REFFRAMEWORK_DIR_KEY = "OPENCERT_REFFRAMEWORK_DIR_KEY";
	private static final String REFFRAMEWORK = ".refframework";
	protected ArrayList<String> refList;
	protected ArrayList<String> refListDir;
	protected String qref;
	protected TreeViewerExt modelViewer;
	protected CheckboxTreeViewerExt modelViewer1;
*/
	// End IRR


	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public RefEquivalenceMapPropertiesEditionPartImpl(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createFigure(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public Composite createFigure(final Composite parent) {
		view = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(view);
		return view;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.ISWTPropertiesEditionPart#
	 * 			createControls(org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(Composite view) { 
		CompositionSequence refEquivalenceMapStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = refEquivalenceMapStep.addStep(RefframeworkViewsRepository.RefEquivalenceMap.Properties.class);
		propertiesStep.addStep(RefframeworkViewsRepository.RefEquivalenceMap.Properties.id);
		propertiesStep.addStep(RefframeworkViewsRepository.RefEquivalenceMap.Properties.name);
		// Start IRR
		//propertiesStep.addStep(RefframeworkViewsRepository.RefEquivalenceMap.Properties.mapGroup);
		// End IRR
		propertiesStep.addStep(RefframeworkViewsRepository.RefEquivalenceMap.Properties.mapGroupCombo);
		propertiesStep.addStep(RefframeworkViewsRepository.RefEquivalenceMap.Properties.type);
		propertiesStep.addStep(RefframeworkViewsRepository.RefEquivalenceMap.Properties.justification);
		propertiesStep.addStep(RefframeworkViewsRepository.RefEquivalenceMap.Properties.target);
		
		
		composer = new PartComposer(refEquivalenceMapStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == RefframeworkViewsRepository.RefEquivalenceMap.Properties.class) {
					return createPropertiesGroup(parent);
				}
				if (key == RefframeworkViewsRepository.RefEquivalenceMap.Properties.id) {
					return createIdText(parent);
				}
				if (key == RefframeworkViewsRepository.RefEquivalenceMap.Properties.name) {
					return createNameText(parent);
				}
				if (key == RefframeworkViewsRepository.RefEquivalenceMap.Properties.mapGroup) {
					return createMapGroupFlatComboViewer(parent);
				}
				if (key == RefframeworkViewsRepository.RefEquivalenceMap.Properties.mapGroupCombo) {
					return createMapGroupComboEMFComboViewer(parent);
				}
				if (key == RefframeworkViewsRepository.RefEquivalenceMap.Properties.type) {
					return createTypeEMFComboViewer(parent);
				}
				if (key == RefframeworkViewsRepository.RefEquivalenceMap.Properties.justification) {
					return createJustificationTextarea(parent);
				}
				if (key == RefframeworkViewsRepository.RefEquivalenceMap.Properties.target) {
					return createTargetAdvancedReferencesTable(parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}

	/**
	 * 
	 */
	protected Composite createPropertiesGroup(Composite parent) {
		Group propertiesGroup = new Group(parent, SWT.NONE);
		propertiesGroup.setText(RefframeworkMessages.RefEquivalenceMapPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesGroupData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesGroupData.horizontalSpan = 3;
		propertiesGroup.setLayoutData(propertiesGroupData);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		return propertiesGroup;
	}

	
	protected Composite createIdText(Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefEquivalenceMap.Properties.id, RefframeworkMessages.RefEquivalenceMapPropertiesEditionPart_IdLabel);
		id = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData idData = new GridData(GridData.FILL_HORIZONTAL);
		id.setLayoutData(idData);
		id.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefEquivalenceMapPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefEquivalenceMap.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
			}

		});
		id.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefEquivalenceMapPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefEquivalenceMap.Properties.id, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, id.getText()));
				}
			}

		});
		EditingUtils.setID(id, RefframeworkViewsRepository.RefEquivalenceMap.Properties.id);
		EditingUtils.setEEFtype(id, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefEquivalenceMap.Properties.id, RefframeworkViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createIdText

		// End of user code
		return parent;
	}

	
	protected Composite createNameText(Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefEquivalenceMap.Properties.name, RefframeworkMessages.RefEquivalenceMapPropertiesEditionPart_NameLabel);
		name = SWTUtils.createScrollableText(parent, SWT.BORDER);
		GridData nameData = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(nameData);
		name.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefEquivalenceMapPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefEquivalenceMap.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
			}

		});
		name.addKeyListener(new KeyAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.KeyAdapter#keyPressed(org.eclipse.swt.events.KeyEvent)
			 * 
			 */
			@Override
			@SuppressWarnings("synthetic-access")
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					if (propertiesEditionComponent != null)
						propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefEquivalenceMapPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefEquivalenceMap.Properties.name, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, name.getText()));
				}
			}

		});
		EditingUtils.setID(name, RefframeworkViewsRepository.RefEquivalenceMap.Properties.name);
		EditingUtils.setEEFtype(name, "eef::Text"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefEquivalenceMap.Properties.name, RefframeworkViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createNameText

		// End of user code
		return parent;
	}

	/**
	 * @param parent the parent composite
	 * 
	 */
	protected Composite createMapGroupFlatComboViewer(Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefEquivalenceMap.Properties.mapGroup, RefframeworkMessages.RefEquivalenceMapPropertiesEditionPart_MapGroupLabel);
		mapGroup = new EObjectFlatComboViewer(parent, !propertiesEditionComponent.isRequired(RefframeworkViewsRepository.RefEquivalenceMap.Properties.mapGroup, RefframeworkViewsRepository.SWT_KIND));
		mapGroup.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));

		mapGroup.addSelectionChangedListener(new ISelectionChangedListener() {

			public void selectionChanged(SelectionChangedEvent event) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefEquivalenceMapPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefEquivalenceMap.Properties.mapGroup, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SET, null, getMapGroup()));
			}

		});
		GridData mapGroupData = new GridData(GridData.FILL_HORIZONTAL);
		mapGroup.setLayoutData(mapGroupData);
		mapGroup.setID(RefframeworkViewsRepository.RefEquivalenceMap.Properties.mapGroup);
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefEquivalenceMap.Properties.mapGroup, RefframeworkViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createMapGroupFlatComboViewer

		// End of user code
		return parent;
	}

	
	protected Composite createMapGroupComboEMFComboViewer(Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefEquivalenceMap.Properties.mapGroupCombo, RefframeworkMessages.RefEquivalenceMapPropertiesEditionPart_MapGroupComboLabel);
		mapGroupCombo = new EMFComboViewer(parent);
		GridData mapGroupComboData = new GridData(GridData.FILL_HORIZONTAL);
		mapGroupCombo.getCombo().setLayoutData(mapGroupComboData);
		mapGroupCombo.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		mapGroupCombo.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefEquivalenceMapPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefEquivalenceMap.Properties.mapGroupCombo, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getMapGroupCombo()));
			}

		});
		mapGroupCombo.setContentProvider(new EMFListContentProvider());
		EditingUtils.setID(mapGroupCombo.getCombo(), RefframeworkViewsRepository.RefEquivalenceMap.Properties.mapGroupCombo);
		EditingUtils.setEEFtype(mapGroupCombo.getCombo(), "eef::Combo"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefEquivalenceMap.Properties.mapGroupCombo, RefframeworkViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createMapGroupComboEMFComboViewer

		// End of user code
		return parent;
	}

	
	protected Composite createTypeEMFComboViewer(Composite parent) {
		createDescription(parent, RefframeworkViewsRepository.RefEquivalenceMap.Properties.type, RefframeworkMessages.RefEquivalenceMapPropertiesEditionPart_TypeLabel);
		type = new EMFComboViewer(parent);
		type.setContentProvider(new ArrayContentProvider());
		type.setLabelProvider(new AdapterFactoryLabelProvider(EEFRuntimePlugin.getDefault().getAdapterFactory()));
		GridData typeData = new GridData(GridData.FILL_HORIZONTAL);
		type.getCombo().setLayoutData(typeData);
		type.addSelectionChangedListener(new ISelectionChangedListener() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
			 * 	
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefEquivalenceMapPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefEquivalenceMap.Properties.type, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, getType()));
			}

		});
		type.setID(RefframeworkViewsRepository.RefEquivalenceMap.Properties.type);
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefEquivalenceMap.Properties.type, RefframeworkViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createTypeEMFComboViewer

		// End of user code
		return parent;
	}

	
	protected Composite createJustificationTextarea(Composite parent) {
		Label justificationLabel = createDescription(parent, RefframeworkViewsRepository.RefEquivalenceMap.Properties.justification, RefframeworkMessages.RefEquivalenceMapPropertiesEditionPart_JustificationLabel);
		GridData justificationLabelData = new GridData(GridData.FILL_HORIZONTAL);
		justificationLabelData.horizontalSpan = 3;
		justificationLabel.setLayoutData(justificationLabelData);
		justification = SWTUtils.createScrollableText(parent, SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.V_SCROLL);
		GridData justificationData = new GridData(GridData.FILL_HORIZONTAL);
		justificationData.horizontalSpan = 2;
		justificationData.heightHint = 80;
		justificationData.widthHint = 200;
		justification.setLayoutData(justificationData);
		justification.addFocusListener(new FocusAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.FocusAdapter#focusLost(org.eclipse.swt.events.FocusEvent)
			 * 
			 */
			public void focusLost(FocusEvent e) {
				if (propertiesEditionComponent != null)
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefEquivalenceMapPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefEquivalenceMap.Properties.justification, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.SET, null, justification.getText()));
			}

		});
		EditingUtils.setID(justification, RefframeworkViewsRepository.RefEquivalenceMap.Properties.justification);
		EditingUtils.setEEFtype(justification, "eef::Textarea"); //$NON-NLS-1$
		SWTUtils.createHelpButton(parent, propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefEquivalenceMap.Properties.justification, RefframeworkViewsRepository.SWT_KIND), null); //$NON-NLS-1$
		// Start of user code for createJustificationTextArea

		// End of user code
		return parent;
	}

	/**
	 * 
	 */
	protected Composite createTargetAdvancedReferencesTable(Composite parent) {
		String label = getDescription(RefframeworkViewsRepository.RefEquivalenceMap.Properties.target, RefframeworkMessages.RefEquivalenceMapPropertiesEditionPart_TargetLabel);		 
		this.target = new ReferencesTable(label, new ReferencesTableListener() {
			public void handleAdd() { addTarget(); }
			public void handleEdit(EObject element) { editTarget(element); }
			public void handleMove(EObject element, int oldIndex, int newIndex) { moveTarget(element, oldIndex, newIndex); }
			public void handleRemove(EObject element) { removeFromTarget(element); }
			public void navigateTo(EObject element) { }
		});
		this.target.setHelpText(propertiesEditionComponent.getHelpContent(RefframeworkViewsRepository.RefEquivalenceMap.Properties.target, RefframeworkViewsRepository.SWT_KIND));
		this.target.createControls(parent);
		this.target.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefEquivalenceMapPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefEquivalenceMap.Properties.target, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData targetData = new GridData(GridData.FILL_HORIZONTAL);
		targetData.horizontalSpan = 3;
		this.target.setLayoutData(targetData);
		this.target.disableMove();
		target.setID(RefframeworkViewsRepository.RefEquivalenceMap.Properties.target);
		target.setEEFType("eef::AdvancedReferencesTable"); //$NON-NLS-1$
		return parent;
	}

	/**
	 * 
	 */
	protected void addTarget() {
		TabElementTreeSelectionDialog dialog = new TabElementTreeSelectionDialog(target.getInput(), targetFilters, targetBusinessFilters,
		"target", propertiesEditionComponent.getEditingContext().getAdapterFactory(), current.eResource()) {
			@Override
			public void process(IStructuredSelection selection) {
				for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
					EObject elem = (EObject) iter.next();
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefEquivalenceMapPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefEquivalenceMap.Properties.target,
						PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, elem));
				}
				target.refresh();
			}
		};
		dialog.open();
	}

	/**
	 * 
	 */
	protected void moveTarget(EObject element, int oldIndex, int newIndex) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefEquivalenceMapPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefEquivalenceMap.Properties.target, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
		target.refresh();
	}

	/**
	 * 
	 */
	protected void removeFromTarget(EObject element) {
		propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(RefEquivalenceMapPropertiesEditionPartImpl.this, RefframeworkViewsRepository.RefEquivalenceMap.Properties.target, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
		target.refresh();
	}

	/**
	 * 
	 */
	protected void editTarget(EObject element) {
		EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(propertiesEditionComponent.getEditingContext(), propertiesEditionComponent, element, adapterFactory);
		PropertiesEditingProvider provider = (PropertiesEditingProvider)adapterFactory.adapt(element, PropertiesEditingProvider.class);
		if (provider != null) {
			PropertiesEditingPolicy policy = provider.getPolicy(context);
			if (policy != null) {
				policy.execute();
				target.refresh();
			}
		}
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#getId()
	 * 
	 */
	public String getId() {
		return id.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#setId(String newValue)
	 * 
	 */
	public void setId(String newValue) {
		if (newValue != null) {
			id.setText(newValue);
		} else {
			id.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefEquivalenceMap.Properties.id);
		if (eefElementEditorReadOnlyState && id.isEnabled()) {
			id.setEnabled(false);
			id.setToolTipText(RefframeworkMessages.RefEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !id.isEnabled()) {
			id.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#getName()
	 * 
	 */
	public String getName() {
		return name.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#setName(String newValue)
	 * 
	 */
	public void setName(String newValue) {
		if (newValue != null) {
			name.setText(newValue);
		} else {
			name.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefEquivalenceMap.Properties.name);
		if (eefElementEditorReadOnlyState && name.isEnabled()) {
			name.setEnabled(false);
			name.setToolTipText(RefframeworkMessages.RefEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !name.isEnabled()) {
			name.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#getMapGroup()
	 * 
	 */
	public EObject getMapGroup() {
		if (mapGroup.getSelection() instanceof StructuredSelection) {
			Object firstElement = ((StructuredSelection) mapGroup.getSelection()).getFirstElement();
			if (firstElement instanceof EObject)
				return (EObject) firstElement;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#initMapGroup(EObjectFlatComboSettings)
	 */
	public void initMapGroup(EObjectFlatComboSettings settings) {
		mapGroup.setInput(settings);
		if (current != null) {
			mapGroup.setSelection(new StructuredSelection(settings.getValue()));
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefEquivalenceMap.Properties.mapGroup);
		if (eefElementEditorReadOnlyState && mapGroup.isEnabled()) {
			mapGroup.setEnabled(false);
			mapGroup.setToolTipText(RefframeworkMessages.RefEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !mapGroup.isEnabled()) {
			mapGroup.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#setMapGroup(EObject newValue)
	 * 
	 */
	public void setMapGroup(EObject newValue) {
		if (newValue != null) {
			mapGroup.setSelection(new StructuredSelection(newValue));
		} else {
			mapGroup.setSelection(new StructuredSelection()); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefEquivalenceMap.Properties.mapGroup);
		if (eefElementEditorReadOnlyState && mapGroup.isEnabled()) {
			mapGroup.setEnabled(false);
			mapGroup.setToolTipText(RefframeworkMessages.RefEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !mapGroup.isEnabled()) {
			mapGroup.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#setMapGroupButtonMode(ButtonsModeEnum newValue)
	 */
	public void setMapGroupButtonMode(ButtonsModeEnum newValue) {
		mapGroup.setButtonMode(newValue);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#addFilterMapGroup(ViewerFilter filter)
	 * 
	 */
	public void addFilterToMapGroup(ViewerFilter filter) {
		mapGroup.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#addBusinessFilterMapGroup(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToMapGroup(ViewerFilter filter) {
		mapGroup.addBusinessRuleFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#getMapGroupCombo()
	 * 
	 */
	public Object getMapGroupCombo() {
		if (mapGroupCombo.getSelection() instanceof StructuredSelection) {
			return ((StructuredSelection) mapGroupCombo.getSelection()).getFirstElement();
		}
		return "";
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#initMapGroupCombo(Object input, Object currentValue)
	 */
	public void initMapGroupCombo(Object input, Object currentValue) {
		mapGroupCombo.setInput(input);
		if (currentValue != null) {
			mapGroupCombo.setSelection(new StructuredSelection(currentValue));
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#setMapGroupCombo(Object newValue)
	 * 
	 */
	public void setMapGroupCombo(Object newValue) {
		if (newValue != null) {
			mapGroupCombo.modelUpdating(new StructuredSelection(newValue));
		} else {
			mapGroupCombo.modelUpdating(new StructuredSelection("")); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefEquivalenceMap.Properties.mapGroupCombo);
		if (eefElementEditorReadOnlyState && mapGroupCombo.isEnabled()) {
			mapGroupCombo.setEnabled(false);
			mapGroupCombo.setToolTipText(RefframeworkMessages.RefEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !mapGroupCombo.isEnabled()) {
			mapGroupCombo.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#addFilterMapGroupCombo(ViewerFilter filter)
	 * 
	 */
	public void addFilterToMapGroupCombo(ViewerFilter filter) {
		mapGroupCombo.addFilter(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#getType()
	 * 
	 */
	public Enumerator getType() {
		Enumerator selection = (Enumerator) ((StructuredSelection) type.getSelection()).getFirstElement();
		return selection;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#initType(Object input, Enumerator current)
	 */
	public void initType(Object input, Enumerator current) {
		type.setInput(input);
		type.modelUpdating(new StructuredSelection(current));
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefEquivalenceMap.Properties.type);
		if (eefElementEditorReadOnlyState && type.isEnabled()) {
			type.setEnabled(false);
			type.setToolTipText(RefframeworkMessages.RefEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !type.isEnabled()) {
			type.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#setType(Enumerator newValue)
	 * 
	 */
	public void setType(Enumerator newValue) {
		type.modelUpdating(new StructuredSelection(newValue));
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefEquivalenceMap.Properties.type);
		if (eefElementEditorReadOnlyState && type.isEnabled()) {
			type.setEnabled(false);
			type.setToolTipText(RefframeworkMessages.RefEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !type.isEnabled()) {
			type.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#getJustification()
	 * 
	 */
	public String getJustification() {
		return justification.getText();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#setJustification(String newValue)
	 * 
	 */
	public void setJustification(String newValue) {
		if (newValue != null) {
			justification.setText(newValue);
		} else {
			justification.setText(""); //$NON-NLS-1$
		}
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefEquivalenceMap.Properties.justification);
		if (eefElementEditorReadOnlyState && justification.isEnabled()) {
			justification.setEnabled(false);
			justification.setBackground(justification.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			justification.setToolTipText(RefframeworkMessages.RefEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !justification.isEnabled()) {
			justification.setEnabled(true);
		}	
		
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#initTarget(org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings)
	 */
	public void initTarget(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		target.setContentProvider(contentProvider);
		target.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(RefframeworkViewsRepository.RefEquivalenceMap.Properties.target);
		if (eefElementEditorReadOnlyState && target.getTable().isEnabled()) {
			target.setEnabled(false);
			target.setToolTipText(RefframeworkMessages.RefEquivalenceMap_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !target.getTable().isEnabled()) {
			target.setEnabled(true);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#updateTarget()
	 * 
	 */
	public void updateTarget() {
	target.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#addFilterTarget(ViewerFilter filter)
	 * 
	 */
	public void addFilterToTarget(ViewerFilter filter) {
		targetFilters.add(filter);
		//Start IRR
		if (this.current.eContainer() instanceof RefArtefact ) {
			targetFilters.add(new EObjectFilter(RefframeworkPackage.Literals.REF_ARTEFACT));
		} else if (this.current.eContainer() instanceof RefActivity) {
			targetFilters.add(new EObjectFilter(RefframeworkPackage.Literals.REF_ACTIVITY));
		}else if (this.current.eContainer() instanceof RefRequirement) {
			targetFilters.add(new EObjectFilter(RefframeworkPackage.Literals.REF_REQUIREMENT));
		}else if (this.current.eContainer() instanceof RefRole) {
			targetFilters.add(new EObjectFilter(RefframeworkPackage.Literals.REF_ROLE));
		}else if (this.current.eContainer() instanceof RefTechnique) {
			targetFilters.add(new EObjectFilter(RefframeworkPackage.Literals.REF_TECHNIQUE));
		}	
		//End IRR
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#addBusinessFilterTarget(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToTarget(ViewerFilter filter) {
		targetBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.pkm.refframework.refframework.parts.RefEquivalenceMapPropertiesEditionPart#isContainedInTargetTable(EObject element)
	 * 
	 */
	public boolean isContainedInTargetTable(EObject element) {
		return ((ReferencesTableSettings)target.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return RefframeworkMessages.RefEquivalenceMap_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
