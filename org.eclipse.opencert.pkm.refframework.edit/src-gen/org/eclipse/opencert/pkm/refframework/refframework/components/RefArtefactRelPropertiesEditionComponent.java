/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.pkm.refframework.refframework.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.impl.utils.EEFUtils;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;
import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.infra.general.general.ChangeEffectKind;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefact;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkFactory;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefArtefactRelPropertiesEditionPart;
import org.eclipse.opencert.pkm.refframework.refframework.parts.RefframeworkViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class RefArtefactRelPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for source EObjectFlatComboViewer
	 */
	private EObjectFlatComboSettings sourceSettings;
	
	/**
	 * Settings for target EObjectFlatComboViewer
	 */
	private EObjectFlatComboSettings targetSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public RefArtefactRelPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject refArtefactRel, String editing_mode) {
		super(editingContext, refArtefactRel, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = RefframeworkViewsRepository.class;
		partKey = RefframeworkViewsRepository.RefArtefactRel.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final RefArtefactRel refArtefactRel = (RefArtefactRel)elt;
			final RefArtefactRelPropertiesEditionPart basePart = (RefArtefactRelPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, refArtefactRel.getId()));
			
			if (isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, refArtefactRel.getName()));
			
			if (isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.description))
				basePart.setDescription(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, refArtefactRel.getDescription()));
			
			if (isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicitySource)) {
				basePart.setMaxMultiplicitySource(EEFConverterUtil.convertToString(EcorePackage.Literals.EINT, refArtefactRel.getMaxMultiplicitySource()));
			}
			
			if (isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicitySource)) {
				basePart.setMinMultiplicitySource(EEFConverterUtil.convertToString(EcorePackage.Literals.EINT, refArtefactRel.getMinMultiplicitySource()));
			}
			
			if (isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicityTarget)) {
				basePart.setMaxMultiplicityTarget(EEFConverterUtil.convertToString(EcorePackage.Literals.EINT, refArtefactRel.getMaxMultiplicityTarget()));
			}
			
			if (isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicityTarget)) {
				basePart.setMinMultiplicityTarget(EEFConverterUtil.convertToString(EcorePackage.Literals.EINT, refArtefactRel.getMinMultiplicityTarget()));
			}
			
			if (isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.modificationEffect)) {
				basePart.initModificationEffect(EEFUtils.choiceOfValues(refArtefactRel, RefframeworkPackage.eINSTANCE.getRefArtefactRel_ModificationEffect()), refArtefactRel.getModificationEffect());
			}
			if (isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.revocationEffect)) {
				basePart.initRevocationEffect(EEFUtils.choiceOfValues(refArtefactRel, RefframeworkPackage.eINSTANCE.getRefArtefactRel_RevocationEffect()), refArtefactRel.getRevocationEffect());
			}
			if (isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.source)) {
				// init part
				sourceSettings = new EObjectFlatComboSettings(refArtefactRel, RefframeworkPackage.eINSTANCE.getRefArtefactRel_Source());
				basePart.initSource(sourceSettings);
				// set the button mode
				basePart.setSourceButtonMode(ButtonsModeEnum.BROWSE);
			}
			if (isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.target)) {
				// init part
				targetSettings = new EObjectFlatComboSettings(refArtefactRel, RefframeworkPackage.eINSTANCE.getRefArtefactRel_Target());
				basePart.initTarget(targetSettings);
				// set the button mode
				basePart.setTargetButtonMode(ButtonsModeEnum.BROWSE);
			}
			// init filters
			
			
			
			
			
			
			
			
			
			if (isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.source)) {
				basePart.addFilterToSource(new ViewerFilter() {
				
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof RefArtefact);
					}
					
				});
				// Start of user code for additional businessfilters for source
				// End of user code
			}
			if (isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.target)) {
				basePart.addFilterToTarget(new ViewerFilter() {
				
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof RefArtefact);
					}
					
				});
				// Start of user code for additional businessfilters for target
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}














	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == RefframeworkViewsRepository.RefArtefactRel.Properties.id) {
			return GeneralPackage.eINSTANCE.getNamedElement_Id();
		}
		if (editorKey == RefframeworkViewsRepository.RefArtefactRel.Properties.name) {
			return GeneralPackage.eINSTANCE.getNamedElement_Name();
		}
		if (editorKey == RefframeworkViewsRepository.RefArtefactRel.Properties.description) {
			return GeneralPackage.eINSTANCE.getDescribableElement_Description();
		}
		if (editorKey == RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicitySource) {
			return RefframeworkPackage.eINSTANCE.getRefArtefactRel_MaxMultiplicitySource();
		}
		if (editorKey == RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicitySource) {
			return RefframeworkPackage.eINSTANCE.getRefArtefactRel_MinMultiplicitySource();
		}
		if (editorKey == RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicityTarget) {
			return RefframeworkPackage.eINSTANCE.getRefArtefactRel_MaxMultiplicityTarget();
		}
		if (editorKey == RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicityTarget) {
			return RefframeworkPackage.eINSTANCE.getRefArtefactRel_MinMultiplicityTarget();
		}
		if (editorKey == RefframeworkViewsRepository.RefArtefactRel.Properties.modificationEffect) {
			return RefframeworkPackage.eINSTANCE.getRefArtefactRel_ModificationEffect();
		}
		if (editorKey == RefframeworkViewsRepository.RefArtefactRel.Properties.revocationEffect) {
			return RefframeworkPackage.eINSTANCE.getRefArtefactRel_RevocationEffect();
		}
		if (editorKey == RefframeworkViewsRepository.RefArtefactRel.Properties.source) {
			return RefframeworkPackage.eINSTANCE.getRefArtefactRel_Source();
		}
		if (editorKey == RefframeworkViewsRepository.RefArtefactRel.Properties.target) {
			return RefframeworkPackage.eINSTANCE.getRefArtefactRel_Target();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		RefArtefactRel refArtefactRel = (RefArtefactRel)semanticObject;
		if (RefframeworkViewsRepository.RefArtefactRel.Properties.id == event.getAffectedEditor()) {
			refArtefactRel.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefArtefactRel.Properties.name == event.getAffectedEditor()) {
			refArtefactRel.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefArtefactRel.Properties.description == event.getAffectedEditor()) {
			refArtefactRel.setDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicitySource == event.getAffectedEditor()) {
			refArtefactRel.setMaxMultiplicitySource((EEFConverterUtil.createIntFromString(EcorePackage.Literals.EINT, (String)event.getNewValue())));
		}
		if (RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicitySource == event.getAffectedEditor()) {
			refArtefactRel.setMinMultiplicitySource((EEFConverterUtil.createIntFromString(EcorePackage.Literals.EINT, (String)event.getNewValue())));
		}
		if (RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicityTarget == event.getAffectedEditor()) {
			refArtefactRel.setMaxMultiplicityTarget((EEFConverterUtil.createIntFromString(EcorePackage.Literals.EINT, (String)event.getNewValue())));
		}
		if (RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicityTarget == event.getAffectedEditor()) {
			refArtefactRel.setMinMultiplicityTarget((EEFConverterUtil.createIntFromString(EcorePackage.Literals.EINT, (String)event.getNewValue())));
		}
		if (RefframeworkViewsRepository.RefArtefactRel.Properties.modificationEffect == event.getAffectedEditor()) {
			refArtefactRel.setModificationEffect((ChangeEffectKind)event.getNewValue());
		}
		if (RefframeworkViewsRepository.RefArtefactRel.Properties.revocationEffect == event.getAffectedEditor()) {
			refArtefactRel.setRevocationEffect((ChangeEffectKind)event.getNewValue());
		}
		if (RefframeworkViewsRepository.RefArtefactRel.Properties.source == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.SET) {
				sourceSettings.setToReference((RefArtefact)event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.ADD) {
				RefArtefact eObject = RefframeworkFactory.eINSTANCE.createRefArtefact();
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, eObject, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(eObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy != null) {
						policy.execute();
					}
				}
				sourceSettings.setToReference(eObject);
			}
		}
		if (RefframeworkViewsRepository.RefArtefactRel.Properties.target == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.SET) {
				targetSettings.setToReference((RefArtefact)event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.ADD) {
				RefArtefact eObject = RefframeworkFactory.eINSTANCE.createRefArtefact();
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, eObject, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(eObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy != null) {
						policy.execute();
					}
				}
				targetSettings.setToReference(eObject);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			RefArtefactRelPropertiesEditionPart basePart = (RefArtefactRelPropertiesEditionPart)editingPart;
			if (GeneralPackage.eINSTANCE.getNamedElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (GeneralPackage.eINSTANCE.getNamedElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (GeneralPackage.eINSTANCE.getDescribableElement_Description().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.description)) {
				if (msg.getNewValue() != null) {
					basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setDescription("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefArtefactRel_MaxMultiplicitySource().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicitySource)) {
				if (msg.getNewValue() != null) {
					basePart.setMaxMultiplicitySource(EcoreUtil.convertToString(EcorePackage.Literals.EINT, msg.getNewValue()));
				} else {
					basePart.setMaxMultiplicitySource("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefArtefactRel_MinMultiplicitySource().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicitySource)) {
				if (msg.getNewValue() != null) {
					basePart.setMinMultiplicitySource(EcoreUtil.convertToString(EcorePackage.Literals.EINT, msg.getNewValue()));
				} else {
					basePart.setMinMultiplicitySource("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefArtefactRel_MaxMultiplicityTarget().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicityTarget)) {
				if (msg.getNewValue() != null) {
					basePart.setMaxMultiplicityTarget(EcoreUtil.convertToString(EcorePackage.Literals.EINT, msg.getNewValue()));
				} else {
					basePart.setMaxMultiplicityTarget("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefArtefactRel_MinMultiplicityTarget().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicityTarget)) {
				if (msg.getNewValue() != null) {
					basePart.setMinMultiplicityTarget(EcoreUtil.convertToString(EcorePackage.Literals.EINT, msg.getNewValue()));
				} else {
					basePart.setMinMultiplicityTarget("");
				}
			}
			if (RefframeworkPackage.eINSTANCE.getRefArtefactRel_ModificationEffect().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.modificationEffect))
				basePart.setModificationEffect((ChangeEffectKind)msg.getNewValue());
			
			if (RefframeworkPackage.eINSTANCE.getRefArtefactRel_RevocationEffect().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.revocationEffect))
				basePart.setRevocationEffect((ChangeEffectKind)msg.getNewValue());
			
			if (RefframeworkPackage.eINSTANCE.getRefArtefactRel_Source().equals(msg.getFeature()) && basePart != null && isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.source))
				basePart.setSource((EObject)msg.getNewValue());
			if (RefframeworkPackage.eINSTANCE.getRefArtefactRel_Target().equals(msg.getFeature()) && basePart != null && isAccessible(RefframeworkViewsRepository.RefArtefactRel.Properties.target))
				basePart.setTarget((EObject)msg.getNewValue());
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			GeneralPackage.eINSTANCE.getNamedElement_Id(),
			GeneralPackage.eINSTANCE.getNamedElement_Name(),
			GeneralPackage.eINSTANCE.getDescribableElement_Description(),
			RefframeworkPackage.eINSTANCE.getRefArtefactRel_MaxMultiplicitySource(),
			RefframeworkPackage.eINSTANCE.getRefArtefactRel_MinMultiplicitySource(),
			RefframeworkPackage.eINSTANCE.getRefArtefactRel_MaxMultiplicityTarget(),
			RefframeworkPackage.eINSTANCE.getRefArtefactRel_MinMultiplicityTarget(),
			RefframeworkPackage.eINSTANCE.getRefArtefactRel_ModificationEffect(),
			RefframeworkPackage.eINSTANCE.getRefArtefactRel_RevocationEffect(),
			RefframeworkPackage.eINSTANCE.getRefArtefactRel_Source(),
			RefframeworkPackage.eINSTANCE.getRefArtefactRel_Target()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#isRequired(java.lang.Object, int)
	 * 
	 */
	public boolean isRequired(Object key, int kind) {
		return key == RefframeworkViewsRepository.RefArtefactRel.Properties.source || key == RefframeworkViewsRepository.RefArtefactRel.Properties.target;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (RefframeworkViewsRepository.RefArtefactRel.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefArtefactRel.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefArtefactRel.Properties.description == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicitySource == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(RefframeworkPackage.eINSTANCE.getRefArtefactRel_MaxMultiplicitySource().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(RefframeworkPackage.eINSTANCE.getRefArtefactRel_MaxMultiplicitySource().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicitySource == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(RefframeworkPackage.eINSTANCE.getRefArtefactRel_MinMultiplicitySource().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(RefframeworkPackage.eINSTANCE.getRefArtefactRel_MinMultiplicitySource().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefArtefactRel.Properties.maxMultiplicityTarget == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(RefframeworkPackage.eINSTANCE.getRefArtefactRel_MaxMultiplicityTarget().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(RefframeworkPackage.eINSTANCE.getRefArtefactRel_MaxMultiplicityTarget().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefArtefactRel.Properties.minMultiplicityTarget == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(RefframeworkPackage.eINSTANCE.getRefArtefactRel_MinMultiplicityTarget().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(RefframeworkPackage.eINSTANCE.getRefArtefactRel_MinMultiplicityTarget().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefArtefactRel.Properties.modificationEffect == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(RefframeworkPackage.eINSTANCE.getRefArtefactRel_ModificationEffect().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(RefframeworkPackage.eINSTANCE.getRefArtefactRel_ModificationEffect().getEAttributeType(), newValue);
				}
				if (RefframeworkViewsRepository.RefArtefactRel.Properties.revocationEffect == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(RefframeworkPackage.eINSTANCE.getRefArtefactRel_RevocationEffect().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(RefframeworkPackage.eINSTANCE.getRefArtefactRel_RevocationEffect().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
