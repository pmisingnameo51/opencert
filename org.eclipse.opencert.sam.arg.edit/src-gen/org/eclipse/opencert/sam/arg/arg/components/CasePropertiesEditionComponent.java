/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.sam.arg.arg.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.ecore.util.EcoreUtil;

import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.context.impl.EReferencePropertiesEditionContext;

import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;

import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;

import org.eclipse.emf.eef.runtime.policies.impl.CreateEditingPolicy;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

import org.eclipse.opencert.sam.arg.arg.Agreement;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.ArgumentElement;
import org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation;
import org.eclipse.opencert.sam.arg.arg.Argumentation;
import org.eclipse.opencert.sam.arg.arg.Case;
import org.eclipse.opencert.sam.arg.arg.InformationElementCitation;

import org.eclipse.opencert.sam.arg.arg.parts.ArgViewsRepository;
import org.eclipse.opencert.sam.arg.arg.parts.Case_PropertiesEditionPart;


// End of user code

/**
 * 
 * 
 */
public class CasePropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for argument ReferencesTable
	 */
	protected ReferencesTableSettings argumentSettings;
	
	/**
	 * Settings for argumentation ReferencesTable
	 */
	protected ReferencesTableSettings argumentationSettings;
	
	/**
	 * Settings for agreement ReferencesTable
	 */
	protected ReferencesTableSettings agreementSettings;
	
	/**
	 * Settings for cited ReferencesTable
	 */
	protected ReferencesTableSettings citedSettings;
	
	/**
	 * Settings for information ReferencesTable
	 */
	protected ReferencesTableSettings informationSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public CasePropertiesEditionComponent(PropertiesEditingContext editingContext, EObject case_, String editing_mode) {
		super(editingContext, case_, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = ArgViewsRepository.class;
		partKey = ArgViewsRepository.Case_.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final Case case_ = (Case)elt;
			final Case_PropertiesEditionPart basePart = (Case_PropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(ArgViewsRepository.Case_.Properties.argument)) {
				argumentSettings = new ReferencesTableSettings(case_, ArgPackage.eINSTANCE.getCase_Argument());
				basePart.initArgument(argumentSettings);
			}
			if (isAccessible(ArgViewsRepository.Case_.Properties.argumentation)) {
				argumentationSettings = new ReferencesTableSettings(case_, ArgPackage.eINSTANCE.getCase_Argumentation());
				basePart.initArgumentation(argumentationSettings);
			}
			if (isAccessible(ArgViewsRepository.Case_.Properties.agreement)) {
				agreementSettings = new ReferencesTableSettings(case_, ArgPackage.eINSTANCE.getCase_Agreement());
				basePart.initAgreement(agreementSettings);
			}
			if (isAccessible(ArgViewsRepository.Case_.Properties.cited)) {
				citedSettings = new ReferencesTableSettings(case_, ArgPackage.eINSTANCE.getCase_Cited());
				basePart.initCited(citedSettings);
			}
			if (isAccessible(ArgViewsRepository.Case_.Properties.information)) {
				informationSettings = new ReferencesTableSettings(case_, ArgPackage.eINSTANCE.getCase_Information());
				basePart.initInformation(informationSettings);
			}
			// init filters
			if (isAccessible(ArgViewsRepository.Case_.Properties.argument)) {
				basePart.addFilterToArgument(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof ArgumentElement); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for argument
				// End of user code
			}
			if (isAccessible(ArgViewsRepository.Case_.Properties.argumentation)) {
				basePart.addFilterToArgumentation(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof Argumentation); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for argumentation
				// End of user code
			}
			if (isAccessible(ArgViewsRepository.Case_.Properties.agreement)) {
				basePart.addFilterToAgreement(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof Agreement); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for agreement
				// End of user code
			}
			if (isAccessible(ArgViewsRepository.Case_.Properties.cited)) {
				basePart.addFilterToCited(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof ArgumentElementCitation); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for cited
				// End of user code
			}
			if (isAccessible(ArgViewsRepository.Case_.Properties.information)) {
				basePart.addFilterToInformation(new ViewerFilter() {
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof InformationElementCitation); //$NON-NLS-1$ 
					}
			
				});
				// Start of user code for additional businessfilters for information
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}








	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == ArgViewsRepository.Case_.Properties.argument) {
			return ArgPackage.eINSTANCE.getCase_Argument();
		}
		if (editorKey == ArgViewsRepository.Case_.Properties.argumentation) {
			return ArgPackage.eINSTANCE.getCase_Argumentation();
		}
		if (editorKey == ArgViewsRepository.Case_.Properties.agreement) {
			return ArgPackage.eINSTANCE.getCase_Agreement();
		}
		if (editorKey == ArgViewsRepository.Case_.Properties.cited) {
			return ArgPackage.eINSTANCE.getCase_Cited();
		}
		if (editorKey == ArgViewsRepository.Case_.Properties.information) {
			return ArgPackage.eINSTANCE.getCase_Information();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		Case case_ = (Case)semanticObject;
		if (ArgViewsRepository.Case_.Properties.argument == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, argumentSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				argumentSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				argumentSettings.move(event.getNewIndex(), (ArgumentElement) event.getNewValue());
			}
		}
		if (ArgViewsRepository.Case_.Properties.argumentation == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, argumentationSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				argumentationSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				argumentationSettings.move(event.getNewIndex(), (Argumentation) event.getNewValue());
			}
		}
		if (ArgViewsRepository.Case_.Properties.agreement == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, agreementSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				agreementSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				agreementSettings.move(event.getNewIndex(), (Agreement) event.getNewValue());
			}
		}
		if (ArgViewsRepository.Case_.Properties.cited == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, citedSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				citedSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				citedSettings.move(event.getNewIndex(), (ArgumentElementCitation) event.getNewValue());
			}
		}
		if (ArgViewsRepository.Case_.Properties.information == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				EReferencePropertiesEditionContext context = new EReferencePropertiesEditionContext(editingContext, this, informationSettings, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(semanticObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy instanceof CreateEditingPolicy) {
						policy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.EDIT) {
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, (EObject) event.getNewValue(), editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt((EObject) event.getNewValue(), PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy editionPolicy = provider.getPolicy(context);
					if (editionPolicy != null) {
						editionPolicy.execute();
					}
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				informationSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				informationSettings.move(event.getNewIndex(), (InformationElementCitation) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			Case_PropertiesEditionPart basePart = (Case_PropertiesEditionPart)editingPart;
			if (ArgPackage.eINSTANCE.getCase_Argument().equals(msg.getFeature()) && isAccessible(ArgViewsRepository.Case_.Properties.argument))
				basePart.updateArgument();
			if (ArgPackage.eINSTANCE.getCase_Argumentation().equals(msg.getFeature()) && isAccessible(ArgViewsRepository.Case_.Properties.argumentation))
				basePart.updateArgumentation();
			if (ArgPackage.eINSTANCE.getCase_Agreement().equals(msg.getFeature()) && isAccessible(ArgViewsRepository.Case_.Properties.agreement))
				basePart.updateAgreement();
			if (ArgPackage.eINSTANCE.getCase_Cited().equals(msg.getFeature()) && isAccessible(ArgViewsRepository.Case_.Properties.cited))
				basePart.updateCited();
			if (ArgPackage.eINSTANCE.getCase_Information().equals(msg.getFeature()) && isAccessible(ArgViewsRepository.Case_.Properties.information))
				basePart.updateInformation();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			ArgPackage.eINSTANCE.getCase_Argument(),
			ArgPackage.eINSTANCE.getCase_Argumentation(),
			ArgPackage.eINSTANCE.getCase_Agreement(),
			ArgPackage.eINSTANCE.getCase_Cited(),
			ArgPackage.eINSTANCE.getCase_Information()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
