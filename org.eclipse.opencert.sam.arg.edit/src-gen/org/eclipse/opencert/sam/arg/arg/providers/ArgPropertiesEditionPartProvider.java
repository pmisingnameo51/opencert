/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.sam.arg.arg.providers;

import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.api.providers.IPropertiesEditionPartProvider;

import org.eclipse.opencert.sam.arg.arg.parts.ArgViewsRepository;

import org.eclipse.opencert.sam.arg.arg.parts.forms.AgreementPropertiesEditionPartForm;
import org.eclipse.opencert.sam.arg.arg.parts.forms.ArgumentElementCitationPropertiesEditionPartForm;
import org.eclipse.opencert.sam.arg.arg.parts.forms.ArgumentReasoningPropertiesEditionPartForm;
import org.eclipse.opencert.sam.arg.arg.parts.forms.ArgumentationPropertiesEditionPartForm;
import org.eclipse.opencert.sam.arg.arg.parts.forms.AssertedChallengePropertiesEditionPartForm;
import org.eclipse.opencert.sam.arg.arg.parts.forms.AssertedContextPropertiesEditionPartForm;
import org.eclipse.opencert.sam.arg.arg.parts.forms.AssertedCounterEvidencePropertiesEditionPartForm;
import org.eclipse.opencert.sam.arg.arg.parts.forms.AssertedEvidencePropertiesEditionPartForm;
import org.eclipse.opencert.sam.arg.arg.parts.forms.AssertedInferencePropertiesEditionPartForm;
import org.eclipse.opencert.sam.arg.arg.parts.forms.AssuranceCasePropertiesEditionPartForm;
import org.eclipse.opencert.sam.arg.arg.parts.forms.Case_PropertiesEditionPartForm;
import org.eclipse.opencert.sam.arg.arg.parts.forms.ChoicePropertiesEditionPartForm;
import org.eclipse.opencert.sam.arg.arg.parts.forms.ClaimPropertiesEditionPartForm;
import org.eclipse.opencert.sam.arg.arg.parts.forms.InformationElementCitationPropertiesEditionPartForm;

import org.eclipse.opencert.sam.arg.arg.parts.impl.AgreementPropertiesEditionPartImpl;
import org.eclipse.opencert.sam.arg.arg.parts.impl.ArgumentElementCitationPropertiesEditionPartImpl;
import org.eclipse.opencert.sam.arg.arg.parts.impl.ArgumentReasoningPropertiesEditionPartImpl;
import org.eclipse.opencert.sam.arg.arg.parts.impl.ArgumentationPropertiesEditionPartImpl;
import org.eclipse.opencert.sam.arg.arg.parts.impl.AssertedChallengePropertiesEditionPartImpl;
import org.eclipse.opencert.sam.arg.arg.parts.impl.AssertedContextPropertiesEditionPartImpl;
import org.eclipse.opencert.sam.arg.arg.parts.impl.AssertedCounterEvidencePropertiesEditionPartImpl;
import org.eclipse.opencert.sam.arg.arg.parts.impl.AssertedEvidencePropertiesEditionPartImpl;
import org.eclipse.opencert.sam.arg.arg.parts.impl.AssertedInferencePropertiesEditionPartImpl;
import org.eclipse.opencert.sam.arg.arg.parts.impl.AssuranceCasePropertiesEditionPartImpl;
import org.eclipse.opencert.sam.arg.arg.parts.impl.Case_PropertiesEditionPartImpl;
import org.eclipse.opencert.sam.arg.arg.parts.impl.ChoicePropertiesEditionPartImpl;
import org.eclipse.opencert.sam.arg.arg.parts.impl.ClaimPropertiesEditionPartImpl;
import org.eclipse.opencert.sam.arg.arg.parts.impl.InformationElementCitationPropertiesEditionPartImpl;

/**
 * 
 * 
 */
public class ArgPropertiesEditionPartProvider implements IPropertiesEditionPartProvider {

	/** 
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPartProvider#provides(java.lang.Object)
	 * 
	 */
	public boolean provides(Object key) {
		return key == ArgViewsRepository.class;
	}

	/** 
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPartProvider#getPropertiesEditionPart(java.lang.Object, int, org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(Object key, int kind, IPropertiesEditionComponent component) {
		if (key == ArgViewsRepository.Case_.class) {
			if (kind == ArgViewsRepository.SWT_KIND)
				return new Case_PropertiesEditionPartImpl(component);
			if (kind == ArgViewsRepository.FORM_KIND)
				return new Case_PropertiesEditionPartForm(component);
		}
		if (key == ArgViewsRepository.AssuranceCase.class) {
			if (kind == ArgViewsRepository.SWT_KIND)
				return new AssuranceCasePropertiesEditionPartImpl(component);
			if (kind == ArgViewsRepository.FORM_KIND)
				return new AssuranceCasePropertiesEditionPartForm(component);
		}
		if (key == ArgViewsRepository.Argumentation.class) {
			if (kind == ArgViewsRepository.SWT_KIND)
				return new ArgumentationPropertiesEditionPartImpl(component);
			if (kind == ArgViewsRepository.FORM_KIND)
				return new ArgumentationPropertiesEditionPartForm(component);
		}
		if (key == ArgViewsRepository.InformationElementCitation.class) {
			if (kind == ArgViewsRepository.SWT_KIND)
				return new InformationElementCitationPropertiesEditionPartImpl(component);
			if (kind == ArgViewsRepository.FORM_KIND)
				return new InformationElementCitationPropertiesEditionPartForm(component);
		}
		if (key == ArgViewsRepository.ArgumentElementCitation.class) {
			if (kind == ArgViewsRepository.SWT_KIND)
				return new ArgumentElementCitationPropertiesEditionPartImpl(component);
			if (kind == ArgViewsRepository.FORM_KIND)
				return new ArgumentElementCitationPropertiesEditionPartForm(component);
		}
		if (key == ArgViewsRepository.ArgumentReasoning.class) {
			if (kind == ArgViewsRepository.SWT_KIND)
				return new ArgumentReasoningPropertiesEditionPartImpl(component);
			if (kind == ArgViewsRepository.FORM_KIND)
				return new ArgumentReasoningPropertiesEditionPartForm(component);
		}
		if (key == ArgViewsRepository.Claim.class) {
			if (kind == ArgViewsRepository.SWT_KIND)
				return new ClaimPropertiesEditionPartImpl(component);
			if (kind == ArgViewsRepository.FORM_KIND)
				return new ClaimPropertiesEditionPartForm(component);
		}
		if (key == ArgViewsRepository.Choice.class) {
			if (kind == ArgViewsRepository.SWT_KIND)
				return new ChoicePropertiesEditionPartImpl(component);
			if (kind == ArgViewsRepository.FORM_KIND)
				return new ChoicePropertiesEditionPartForm(component);
		}
		if (key == ArgViewsRepository.AssertedInference.class) {
			if (kind == ArgViewsRepository.SWT_KIND)
				return new AssertedInferencePropertiesEditionPartImpl(component);
			if (kind == ArgViewsRepository.FORM_KIND)
				return new AssertedInferencePropertiesEditionPartForm(component);
		}
		if (key == ArgViewsRepository.AssertedEvidence.class) {
			if (kind == ArgViewsRepository.SWT_KIND)
				return new AssertedEvidencePropertiesEditionPartImpl(component);
			if (kind == ArgViewsRepository.FORM_KIND)
				return new AssertedEvidencePropertiesEditionPartForm(component);
		}
		if (key == ArgViewsRepository.AssertedContext.class) {
			if (kind == ArgViewsRepository.SWT_KIND)
				return new AssertedContextPropertiesEditionPartImpl(component);
			if (kind == ArgViewsRepository.FORM_KIND)
				return new AssertedContextPropertiesEditionPartForm(component);
		}
		if (key == ArgViewsRepository.AssertedChallenge.class) {
			if (kind == ArgViewsRepository.SWT_KIND)
				return new AssertedChallengePropertiesEditionPartImpl(component);
			if (kind == ArgViewsRepository.FORM_KIND)
				return new AssertedChallengePropertiesEditionPartForm(component);
		}
		if (key == ArgViewsRepository.AssertedCounterEvidence.class) {
			if (kind == ArgViewsRepository.SWT_KIND)
				return new AssertedCounterEvidencePropertiesEditionPartImpl(component);
			if (kind == ArgViewsRepository.FORM_KIND)
				return new AssertedCounterEvidencePropertiesEditionPartForm(component);
		}
		if (key == ArgViewsRepository.Agreement.class) {
			if (kind == ArgViewsRepository.SWT_KIND)
				return new AgreementPropertiesEditionPartImpl(component);
			if (kind == ArgViewsRepository.FORM_KIND)
				return new AgreementPropertiesEditionPartForm(component);
		}
		return null;
	}

}
