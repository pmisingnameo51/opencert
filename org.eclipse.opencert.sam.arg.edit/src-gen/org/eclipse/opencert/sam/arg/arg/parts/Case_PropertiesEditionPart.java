/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.sam.arg.arg.parts;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;


// End of user code

/**
 * 
 * 
 */
public interface Case_PropertiesEditionPart {



	/**
	 * Init the argument
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initArgument(ReferencesTableSettings settings);

	/**
	 * Update the argument
	 * @param newValue the argument to update
	 * 
	 */
	public void updateArgument();

	/**
	 * Adds the given filter to the argument edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToArgument(ViewerFilter filter);

	/**
	 * Adds the given filter to the argument edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToArgument(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the argument table
	 * 
	 */
	public boolean isContainedInArgumentTable(EObject element);




	/**
	 * Init the argumentation
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initArgumentation(ReferencesTableSettings settings);

	/**
	 * Update the argumentation
	 * @param newValue the argumentation to update
	 * 
	 */
	public void updateArgumentation();

	/**
	 * Adds the given filter to the argumentation edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToArgumentation(ViewerFilter filter);

	/**
	 * Adds the given filter to the argumentation edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToArgumentation(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the argumentation table
	 * 
	 */
	public boolean isContainedInArgumentationTable(EObject element);




	/**
	 * Init the agreement
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initAgreement(ReferencesTableSettings settings);

	/**
	 * Update the agreement
	 * @param newValue the agreement to update
	 * 
	 */
	public void updateAgreement();

	/**
	 * Adds the given filter to the agreement edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToAgreement(ViewerFilter filter);

	/**
	 * Adds the given filter to the agreement edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToAgreement(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the agreement table
	 * 
	 */
	public boolean isContainedInAgreementTable(EObject element);




	/**
	 * Init the cited
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initCited(ReferencesTableSettings settings);

	/**
	 * Update the cited
	 * @param newValue the cited to update
	 * 
	 */
	public void updateCited();

	/**
	 * Adds the given filter to the cited edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToCited(ViewerFilter filter);

	/**
	 * Adds the given filter to the cited edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToCited(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the cited table
	 * 
	 */
	public boolean isContainedInCitedTable(EObject element);




	/**
	 * Init the information
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initInformation(ReferencesTableSettings settings);

	/**
	 * Update the information
	 * @param newValue the information to update
	 * 
	 */
	public void updateInformation();

	/**
	 * Adds the given filter to the information edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToInformation(ViewerFilter filter);

	/**
	 * Adds the given filter to the information edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToInformation(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the information table
	 * 
	 */
	public boolean isContainedInInformationTable(EObject element);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
