/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.sam.arg.arg.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;

import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;

import org.eclipse.emf.eef.runtime.impl.filters.EObjectFilter;

import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;

import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.impl.utils.EEFUtils;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage;

import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.InformationElementCitation;
import org.eclipse.opencert.sam.arg.arg.InformationElementType;

import org.eclipse.opencert.sam.arg.arg.parts.ArgViewsRepository;
import org.eclipse.opencert.sam.arg.arg.parts.InformationElementCitationPropertiesEditionPart;


// End of user code

/**
 * 
 * 
 */
public class InformationElementCitationPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for artefact ReferencesTable
	 */
	private ReferencesTableSettings artefactSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public InformationElementCitationPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject informationElementCitation, String editing_mode) {
		super(editingContext, informationElementCitation, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = ArgViewsRepository.class;
		partKey = ArgViewsRepository.InformationElementCitation.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final InformationElementCitation informationElementCitation = (InformationElementCitation)elt;
			final InformationElementCitationPropertiesEditionPart basePart = (InformationElementCitationPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(ArgViewsRepository.InformationElementCitation.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, informationElementCitation.getId()));
			
			if (isAccessible(ArgViewsRepository.InformationElementCitation.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, informationElementCitation.getName()));
			
			if (isAccessible(ArgViewsRepository.InformationElementCitation.Properties.description))
				basePart.setDescription(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, informationElementCitation.getDescription()));
			
			if (isAccessible(ArgViewsRepository.InformationElementCitation.Properties.content))
				basePart.setContent(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, informationElementCitation.getContent()));
			
			if (isAccessible(ArgViewsRepository.InformationElementCitation.Properties.type)) {
				basePart.initType(EEFUtils.choiceOfValues(informationElementCitation, ArgPackage.eINSTANCE.getInformationElementCitation_Type()), informationElementCitation.getType());
			}
			if (isAccessible(ArgViewsRepository.InformationElementCitation.Properties.toBeInstantiated))
				basePart.setToBeInstantiated(EEFConverterUtil.convertToString(EcorePackage.Literals.EBOOLEAN_OBJECT, informationElementCitation.getToBeInstantiated()));
			
			if (isAccessible(ArgViewsRepository.InformationElementCitation.Properties.url))
				basePart.setUrl(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, informationElementCitation.getUrl()));
			
			if (isAccessible(ArgViewsRepository.InformationElementCitation.Properties.artefact)) {
				artefactSettings = new ReferencesTableSettings(informationElementCitation, ArgPackage.eINSTANCE.getInformationElementCitation_Artefact());
				basePart.initArtefact(artefactSettings);
			}
			// init filters
			
			
			
			
			
			
			
			if (isAccessible(ArgViewsRepository.InformationElementCitation.Properties.artefact)) {
				basePart.addFilterToArtefact(new EObjectFilter(EvidencePackage.Literals.ARTEFACT));
				// Start of user code for additional businessfilters for artefact
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}











	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == ArgViewsRepository.InformationElementCitation.Properties.id) {
			return ArgPackage.eINSTANCE.getModelElement_Id();
		}
		if (editorKey == ArgViewsRepository.InformationElementCitation.Properties.name) {
			return ArgPackage.eINSTANCE.getModelElement_Name();
		}
		if (editorKey == ArgViewsRepository.InformationElementCitation.Properties.description) {
			return ArgPackage.eINSTANCE.getArgumentationElement_Description();
		}
		if (editorKey == ArgViewsRepository.InformationElementCitation.Properties.content) {
			return ArgPackage.eINSTANCE.getArgumentationElement_Content();
		}
		if (editorKey == ArgViewsRepository.InformationElementCitation.Properties.type) {
			return ArgPackage.eINSTANCE.getInformationElementCitation_Type();
		}
		if (editorKey == ArgViewsRepository.InformationElementCitation.Properties.toBeInstantiated) {
			return ArgPackage.eINSTANCE.getInformationElementCitation_ToBeInstantiated();
		}
		if (editorKey == ArgViewsRepository.InformationElementCitation.Properties.url) {
			return ArgPackage.eINSTANCE.getInformationElementCitation_Url();
		}
		if (editorKey == ArgViewsRepository.InformationElementCitation.Properties.artefact) {
			return ArgPackage.eINSTANCE.getInformationElementCitation_Artefact();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		InformationElementCitation informationElementCitation = (InformationElementCitation)semanticObject;
		if (ArgViewsRepository.InformationElementCitation.Properties.id == event.getAffectedEditor()) {
			informationElementCitation.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.InformationElementCitation.Properties.name == event.getAffectedEditor()) {
			informationElementCitation.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.InformationElementCitation.Properties.description == event.getAffectedEditor()) {
			informationElementCitation.setDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.InformationElementCitation.Properties.content == event.getAffectedEditor()) {
			informationElementCitation.setContent((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.InformationElementCitation.Properties.type == event.getAffectedEditor()) {
			informationElementCitation.setType((InformationElementType)event.getNewValue());
		}
		if (ArgViewsRepository.InformationElementCitation.Properties.toBeInstantiated == event.getAffectedEditor()) {
			informationElementCitation.setToBeInstantiated((java.lang.Boolean)EEFConverterUtil.createFromString(EcorePackage.Literals.EBOOLEAN_OBJECT, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.InformationElementCitation.Properties.url == event.getAffectedEditor()) {
			informationElementCitation.setUrl((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (ArgViewsRepository.InformationElementCitation.Properties.artefact == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof Artefact) {
					artefactSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				artefactSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				artefactSettings.move(event.getNewIndex(), (Artefact) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			InformationElementCitationPropertiesEditionPart basePart = (InformationElementCitationPropertiesEditionPart)editingPart;
			if (ArgPackage.eINSTANCE.getModelElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.InformationElementCitation.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (ArgPackage.eINSTANCE.getModelElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.InformationElementCitation.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (ArgPackage.eINSTANCE.getArgumentationElement_Description().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.InformationElementCitation.Properties.description)) {
				if (msg.getNewValue() != null) {
					basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setDescription("");
				}
			}
			if (ArgPackage.eINSTANCE.getArgumentationElement_Content().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.InformationElementCitation.Properties.content)) {
				if (msg.getNewValue() != null) {
					basePart.setContent(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setContent("");
				}
			}
			if (ArgPackage.eINSTANCE.getInformationElementCitation_Type().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && isAccessible(ArgViewsRepository.InformationElementCitation.Properties.type))
				basePart.setType((InformationElementType)msg.getNewValue());
			
			if (ArgPackage.eINSTANCE.getInformationElementCitation_ToBeInstantiated().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.InformationElementCitation.Properties.toBeInstantiated)) {
				if (msg.getNewValue() != null) {
					basePart.setToBeInstantiated(EcoreUtil.convertToString(EcorePackage.Literals.EBOOLEAN_OBJECT, msg.getNewValue()));
				} else {
					basePart.setToBeInstantiated("");
				}
			}
			if (ArgPackage.eINSTANCE.getInformationElementCitation_Url().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(ArgViewsRepository.InformationElementCitation.Properties.url)) {
				if (msg.getNewValue() != null) {
					basePart.setUrl(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setUrl("");
				}
			}
			if (ArgPackage.eINSTANCE.getInformationElementCitation_Artefact().equals(msg.getFeature())  && isAccessible(ArgViewsRepository.InformationElementCitation.Properties.artefact))
				basePart.updateArtefact();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			ArgPackage.eINSTANCE.getModelElement_Id(),
			ArgPackage.eINSTANCE.getModelElement_Name(),
			ArgPackage.eINSTANCE.getArgumentationElement_Description(),
			ArgPackage.eINSTANCE.getArgumentationElement_Content(),
			ArgPackage.eINSTANCE.getInformationElementCitation_Type(),
			ArgPackage.eINSTANCE.getInformationElementCitation_ToBeInstantiated(),
			ArgPackage.eINSTANCE.getInformationElementCitation_Url(),
			ArgPackage.eINSTANCE.getInformationElementCitation_Artefact()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#isRequired(java.lang.Object, int)
	 * 
	 */
	public boolean isRequired(Object key, int kind) {
		return key == ArgViewsRepository.InformationElementCitation.Properties.toBeInstantiated;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (ArgViewsRepository.InformationElementCitation.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getModelElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getModelElement_Id().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.InformationElementCitation.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getModelElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getModelElement_Name().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.InformationElementCitation.Properties.description == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getArgumentationElement_Description().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getArgumentationElement_Description().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.InformationElementCitation.Properties.content == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getArgumentationElement_Content().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getArgumentationElement_Content().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.InformationElementCitation.Properties.type == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getInformationElementCitation_Type().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getInformationElementCitation_Type().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.InformationElementCitation.Properties.toBeInstantiated == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getInformationElementCitation_ToBeInstantiated().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getInformationElementCitation_ToBeInstantiated().getEAttributeType(), newValue);
				}
				if (ArgViewsRepository.InformationElementCitation.Properties.url == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(ArgPackage.eINSTANCE.getInformationElementCitation_Url().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(ArgPackage.eINSTANCE.getInformationElementCitation_Url().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
