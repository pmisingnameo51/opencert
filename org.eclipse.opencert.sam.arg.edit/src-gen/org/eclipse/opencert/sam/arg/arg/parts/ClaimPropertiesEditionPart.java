/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.sam.arg.arg.parts;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;


// End of user code

/**
 * 
 * 
 */
public interface ClaimPropertiesEditionPart {

	/**
	 * @return the id
	 * 
	 */
	public String getId();

	/**
	 * Defines a new id
	 * @param newValue the new id to set
	 * 
	 */
	public void setId(String newValue);


	/**
	 * @return the name
	 * 
	 */
	public String getName();

	/**
	 * Defines a new name
	 * @param newValue the new name to set
	 * 
	 */
	public void setName(String newValue);


	/**
	 * @return the description
	 * 
	 */
	public String getDescription();

	/**
	 * Defines a new description
	 * @param newValue the new description to set
	 * 
	 */
	public void setDescription(String newValue);


	/**
	 * @return the content
	 * 
	 */
	public String getContent();

	/**
	 * Defines a new content
	 * @param newValue the new content to set
	 * 
	 */
	public void setContent(String newValue);




	/**
	 * Init the evaluation
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initEvaluation(ReferencesTableSettings settings);

	/**
	 * Update the evaluation
	 * @param newValue the evaluation to update
	 * 
	 */
	public void updateEvaluation();

	/**
	 * Adds the given filter to the evaluation edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToEvaluation(ViewerFilter filter);

	/**
	 * Adds the given filter to the evaluation edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToEvaluation(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the evaluation table
	 * 
	 */
	public boolean isContainedInEvaluationTable(EObject element);




	/**
	 * Init the lifecycleEvent
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initLifecycleEvent(ReferencesTableSettings settings);

	/**
	 * Update the lifecycleEvent
	 * @param newValue the lifecycleEvent to update
	 * 
	 */
	public void updateLifecycleEvent();

	/**
	 * Adds the given filter to the lifecycleEvent edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToLifecycleEvent(ViewerFilter filter);

	/**
	 * Adds the given filter to the lifecycleEvent edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToLifecycleEvent(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the lifecycleEvent table
	 * 
	 */
	public boolean isContainedInLifecycleEventTable(EObject element);


	/**
	 * @return the public
	 * 
	 */
	public String getPublic_();

	/**
	 * Defines a new public
	 * @param newValue the new public to set
	 * 
	 */
	public void setPublic_(String newValue);


	/**
	 * @return the assumed
	 * 
	 */
	public String getAssumed();

	/**
	 * Defines a new assumed
	 * @param newValue the new assumed to set
	 * 
	 */
	public void setAssumed(String newValue);


	/**
	 * @return the toBeSupported
	 * 
	 */
	public String getToBeSupported();

	/**
	 * Defines a new toBeSupported
	 * @param newValue the new toBeSupported to set
	 * 
	 */
	public void setToBeSupported(String newValue);


	/**
	 * @return the toBeInstantiated
	 * 
	 */
	public String getToBeInstantiated();

	/**
	 * Defines a new toBeInstantiated
	 * @param newValue the new toBeInstantiated to set
	 * 
	 */
	public void setToBeInstantiated(String newValue);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
