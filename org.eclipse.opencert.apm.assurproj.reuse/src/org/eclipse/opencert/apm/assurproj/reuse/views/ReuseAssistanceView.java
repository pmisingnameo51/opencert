/*******************************************************************************
 * Copyright (c) 2017 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
  * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.assurproj.reuse.views;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.cdo.CDOObject;
import org.eclipse.emf.cdo.common.id.CDOID;
import org.eclipse.emf.cdo.common.security.CDOPermission;
import org.eclipse.emf.cdo.common.util.CDOException;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.ui.DawnEditorInput;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CDOUtil;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.impl.DiagramImpl;
import org.eclipse.gmf.runtime.notation.impl.ShapeImpl;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectFactory;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig;
import org.eclipse.opencert.apm.assurproj.assuranceproject.utils.DawnArgDiagramUtil;
import org.eclipse.opencert.apm.assurproj.utils.widget.CheckboxTreeViewerExt;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel;
import org.eclipse.opencert.evm.evidspec.evidence.presentation.EvidenceEditor;
import org.eclipse.opencert.sam.arg.arg.presentation.ArgEditor;
import org.eclipse.opencert.apm.baseline.baseline.presentation.BaselineEditor;
import org.eclipse.opencert.pam.procspec.process.presentation.ProcessEditor;
import org.eclipse.opencert.infra.mappings.mapping.MapGroup;
import org.eclipse.opencert.pam.procspec.process.Activity;
import org.eclipse.opencert.pam.procspec.process.Participant;
import org.eclipse.opencert.pam.procspec.process.ProcessModel;
import org.eclipse.opencert.sam.arg.arg.Case;
import org.eclipse.opencert.sam.arg.arg.Claim;
import org.eclipse.opencert.sam.arg.arg.ModelElement;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;


public class ReuseAssistanceView extends ViewPart {
	// MCP CrossProject	
	private static final String EXTENSION_DIAGRAM = "_diagram";
	private Map<String, EObject> eIDToObjectMap = new HashMap<String, EObject>(); // only for arg model elements
	// MCP CrossProject	

	protected ArrayList<String> sResult = null;
	
	private CDOSession sessionCDO = null;
	private CDOResource resourceAPSource = null; 		// Resource Source Assurance project
	private AssuranceProject assuranceProjectSource;	//model Source Assurance project
	private CDOResource resourceAPTarget = null; 
	private AssuranceProject assuranceProjectTarget;//model Target Assurance project
	
	private CDOTransaction transactionCDO = null;
	
	private Map<CDOResource, Object[]> evidenceAllSelectedObjects = new HashMap<CDOResource, Object[]>();
	private Map<CDOResource, Object[]> argAllSelectedObjects = new HashMap<CDOResource, Object[]>();
	private Map<CDOResource, Object[]> processAllSelectedObjects = new HashMap<CDOResource, Object[]>();
	private Map<CDOResource, Object[]> baselineAllSelectedObjects = new HashMap<CDOResource, Object[]>();
	
	private CheckboxTreeViewerExt checktreeBaselineSource;
	private CheckboxTreeViewerExt checktreeArgSource;
	
	
	private CheckboxTreeViewerExt checktreeProcessSource;
	
	private CDOResource baselineResource = null;
	
	private Combo sourceProjectCombo;
	private Label sourceLabel;
	private CheckboxTreeViewerExt checktreeEvidenceSource;
			
	private Label targetLabel;
	private Combo targetProjectCombo;

	private  Map<EObject, EObject> map = new HashMap<EObject, EObject>();
	HashMap<Object, Object> options = new HashMap<Object, Object>();

	private AssetsPackage targetAssetsPackage;
	
	protected AssuranceprojectPackage projectPackage;
	protected AssuranceprojectFactory projectFactory;
	
	private static final String ASSURANCEPROJECT = ".assuranceproject";	
	
	protected Button reuseBtn;
	protected ArrayList<CDOResourceNode> projects;
	protected String eviEditorID = "org.eclipse.opencert.evm.evidspec.presentation.DawnEvidenceEditorID";
	protected String argEditorID = "org.eclipse.opencert.sam.arg.presentation.DawnArgEditorID";
	protected String processEditorID = "org.eclipse.opencert.pam.procspec.presentation.DawnProcessEditorID";
	protected String baselineEditorID = "org.eclipse.opencert.apm.baseline.presentation.DawnBaselineEditorID";
	
	Action addItemAction;

	@Override
	public void createPartControl(Composite parent) {
					    
		CDOConnectionUtil.instance.init(PreferenceConstants.getRepositoryName(), PreferenceConstants.getProtocol(),
		          PreferenceConstants.getServerName());
		
		sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
		
		if(sessionCDO == null){
			try {
				//To wait the connection establishment in Repository Explorer Thread
				Thread.sleep(2000);
				sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
				//sessionCDO = CDOConnectionUtil.instance.openSession();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
		if(sessionCDO == null){
			sessionCDO = CDOConnectionUtil.instance.openSession();
		}
		
		transactionCDO = sessionCDO.openTransaction();
		createActions();
		createToolbar();
		createReuseControls(parent);
		
	}	
	
	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
	}

	protected void createReuseControls(Composite parent) {
		ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
				ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		Composite sourceComposite = new Composite(parent, SWT.NONE);
		GridData data = new GridData(SWT.FILL, SWT.FILL, false, true);
		data.horizontalAlignment = SWT.END;
		data.horizontalAlignment = SWT.FILL;
		sourceComposite.setLayoutData(data);
		GridLayout layout = new GridLayout();
		layout.marginHeight = 0;
		layout.marginWidth = 10;			
		sourceComposite.setLayout(layout);
		
		Composite compositeSourceProject = new Composite(sourceComposite, SWT.NONE);
		GridData dataNew = new GridData(SWT.FILL, SWT.FILL, false, true);
		dataNew.horizontalAlignment = SWT.END;
		compositeSourceProject.setLayoutData(dataNew);

		GridLayout layoutNew = new GridLayout();
		dataNew.horizontalAlignment = SWT.FILL;
		layoutNew.marginHeight = 0;
		layoutNew.marginWidth = 0;
		layoutNew.numColumns = 2;
		compositeSourceProject.setLayout(layoutNew);
		
		// Label
		targetLabel = new Label(compositeSourceProject, SWT.NONE);
		targetLabel.setText("Target Project:");
		GridData targetLabelGridData = new GridData();		
		targetLabelGridData.horizontalAlignment = SWT.FILL;
		targetLabelGridData.verticalAlignment = SWT.FILL;
		targetLabel.setLayoutData(targetLabelGridData);
		targetProjectCombo=new Combo(compositeSourceProject,SWT.NONE);
		GridData targetComboGridData = new GridData();		
		targetComboGridData.widthHint=200;
		targetProjectCombo.setLayoutData(targetComboGridData);
	
		targetProjectCombo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int targetindex=targetProjectCombo.getSelectionIndex();
				CDOResourceNode selectedTarget=projects.get(targetindex);
				try {	
					resourceAPTarget= transactionCDO.getResource(selectedTarget.getPath());
					assuranceProjectTarget= (AssuranceProject) resourceAPTarget.getContents().get(0);
					if(sourceProjectCombo.getSelectionIndex()>-1){
						reuseBtn.setEnabled(true);
					}
						
				} catch (CDOException cdoe) {
					cdoe.printStackTrace();
				}
			}
		});
		
//		// Text
//		projecttargetText = new Text(compositeSourceProject, SWT.NONE);
		GridData nameTextGridData = new GridData();
		nameTextGridData.grabExcessHorizontalSpace = true;
		nameTextGridData.minimumWidth = 50;
		nameTextGridData.widthHint=100;
		nameTextGridData.horizontalAlignment = SWT.FILL;
		nameTextGridData.verticalAlignment = SWT.FILL;
	
		// Label
		sourceLabel = new Label(compositeSourceProject, SWT.NONE);
		sourceLabel.setText("Source Project:");
		GridData sourceLabelGridData = new GridData();
		sourceLabelGridData.horizontalAlignment = SWT.FILL;
		sourceLabelGridData.verticalAlignment = SWT.FILL;
		sourceLabel.setLayoutData(sourceLabelGridData);
		sourceProjectCombo=new Combo(compositeSourceProject,SWT.NONE);	
		
		GridData sourceComboGridData = new GridData();		
		sourceComboGridData.widthHint=200;
		sourceProjectCombo.setLayoutData(sourceComboGridData);
		
		sourceProjectCombo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				sourceProjectChanged();
			}
			
		});
		
		//Load the baseline
		GridData modelViewerGridData = new GridData();
		modelViewerGridData.verticalAlignment = SWT.FILL;
		modelViewerGridData.horizontalAlignment = SWT.FILL;
		modelViewerGridData.widthHint = 200;
		modelViewerGridData.horizontalSpan=2;
		modelViewerGridData.grabExcessHorizontalSpace = true;
		modelViewerGridData.grabExcessVerticalSpace = true;
		
		checktreeBaselineSource = new CheckboxTreeViewerExt(compositeSourceProject,
				SWT.NONE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		
		checktreeBaselineSource.getTree().setLayoutData(modelViewerGridData);
		checktreeBaselineSource.setContentProvider(new AdapterFactoryContentProvider(
				adapterFactory));
		checktreeBaselineSource.setLabelProvider(new AdapterFactoryLabelProvider(
				adapterFactory));

		checktreeBaselineSource.addDoubleClickListener(new IDoubleClickListener(){

			@Override
			public void doubleClick(DoubleClickEvent event) {
				Object obj=((TreeSelection)event.getSelection()).getFirstElement();
				if(obj instanceof CDOResource){
					URI uri=((CDOResource)obj).getURI();
					String editorID = baselineEditorID;
			        
			          if (editorID != null && !editorID.equals(""))
			          {
			            try
			            {
			              DawnEditorInput editorInput = new DawnEditorInput(uri);
			              IWorkbenchPage page = PlatformUI.getWorkbench()
									.getActiveWorkbenchWindow().getActivePage();		              
			              BaselineEditor baselineEditor = (BaselineEditor) page.findEditor(editorInput);
			              boolean resetChecks = false;
			              if (baselineEditor==null) {
			            	  resetChecks =true;
			              }
			              page.openEditor(editorInput, editorID);
			              baselineEditor = (BaselineEditor) page.findEditor(editorInput);
						  baselineEditor.setActivePagePublic(5);
						  //boolean status = checktreeBaselineSource.getChecked(obj);
						  if (resetChecks==true) {
							   baselineEditor.setTreeViewerWithCheck(true);
			              }
			            }
			            catch (PartInitException e)
			            {
			              e.printStackTrace();
			            }
			          }
					//String text=i.getText();
				}
				
				// TODO Auto-generated method stub
				
			}
			
		});		

		// TreeViewer for Artefact models
		checktreeEvidenceSource = new CheckboxTreeViewerExt(compositeSourceProject, SWT.SINGLE
				| SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
				
		checktreeEvidenceSource.getTree().setLayoutData(modelViewerGridData);
		checktreeEvidenceSource.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
		checktreeEvidenceSource.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
	
//		checktreeEvidenceSource.getTree().setEnabled(false);

		checktreeEvidenceSource.addDoubleClickListener(new IDoubleClickListener(){

			@Override
			public void doubleClick(DoubleClickEvent event) {
				Object obj=((TreeSelection)event.getSelection()).getFirstElement();
				if(obj instanceof CDOResource){
					URI uri=((CDOResource)obj).getURI();
					String editorID = eviEditorID;
			        
			          if (editorID != null && !editorID.equals(""))
			          {
			            try
			            {
			              DawnEditorInput editorInput = new DawnEditorInput(uri);
			              IWorkbenchPage page = PlatformUI.getWorkbench()
									.getActiveWorkbenchWindow().getActivePage();		              
			              EvidenceEditor eviEditor = (EvidenceEditor) page.findEditor(editorInput);
			              
			             
			              boolean resetChecks = false;
			              if (eviEditor==null) {
			            	  resetChecks =true;
			              }
			              page.openEditor(editorInput, editorID);
			              eviEditor = (EvidenceEditor) page.findEditor(editorInput);
						  eviEditor.setActivePagePublic(5);
						  //boolean status = checktreeEvidenceSource.getChecked(obj);
						  if (resetChecks==true) {
							   eviEditor.setTreeViewerWithCheck(true);
			              }
						  eviEditor.setBaselineResource(baselineResource);
			            }
			            catch (PartInitException e)
			            {
			              e.printStackTrace();
			            }
			          }
					//String text=i.getText();
				}
				
				// TODO Auto-generated method stub
				
			}
			
		});

		// TreeViewer for Argumentation models
		checktreeArgSource = new CheckboxTreeViewerExt(compositeSourceProject, SWT.SINGLE
				| SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
				
		checktreeArgSource.getTree().setLayoutData(modelViewerGridData);
		checktreeArgSource.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
		checktreeArgSource.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		
		checktreeArgSource.addDoubleClickListener(new IDoubleClickListener(){

			@Override
			public void doubleClick(DoubleClickEvent event) {
				Object obj=((TreeSelection)event.getSelection()).getFirstElement();
				if(obj instanceof CDOResource){
					URI uri=((CDOResource)obj).getURI();
					String editorID = argEditorID;
			        
			          if (editorID != null && !editorID.equals(""))
			          {
			            try
			            {
			              DawnEditorInput editorInput = new DawnEditorInput(uri);
			              IWorkbenchPage page = PlatformUI.getWorkbench()
									.getActiveWorkbenchWindow().getActivePage();		              
			              ArgEditor argEditor = (ArgEditor) page.findEditor(editorInput);
			              boolean resetChecks = false;
			              if (argEditor==null) {
			            	  resetChecks =true;
			              }
			              page.openEditor(editorInput, editorID);
			              argEditor = (ArgEditor) page.findEditor(editorInput);
						  argEditor.setActivePagePublic(5);
						  
						  //boolean status = checktreeArgSource.getChecked(obj);
						  if (resetChecks==true) {
							   argEditor.setTreeViewerWithCheck(true);
			              }
			            }
			            catch (PartInitException e)
			            {
			              e.printStackTrace();
			            }
			          }
					//String text=i.getText();
				}
				
				// TODO Auto-generated method stub
				
			}
			
		});
				
		// TreeViewer for Process models
		checktreeProcessSource = new CheckboxTreeViewerExt(compositeSourceProject, SWT.SINGLE
				| SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
				
		checktreeProcessSource.getTree().setLayoutData(modelViewerGridData);
		checktreeProcessSource.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
		checktreeProcessSource.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		
		checktreeProcessSource.addDoubleClickListener(new IDoubleClickListener(){

			@Override
			public void doubleClick(DoubleClickEvent event) {
				Object obj=((TreeSelection)event.getSelection()).getFirstElement();
				if(obj instanceof CDOResource){
					URI uri=((CDOResource)obj).getURI();
					String editorID = processEditorID;
			        
			          if (editorID != null && !editorID.equals(""))
			          {
			            try
			            {
			              DawnEditorInput editorInput = new DawnEditorInput(uri);
			              IWorkbenchPage page = PlatformUI.getWorkbench()
									.getActiveWorkbenchWindow().getActivePage();		              
			              ProcessEditor processEditor = (ProcessEditor) page.findEditor(editorInput);
			              boolean resetChecks = false;
			              if (processEditor==null) {
			            	  resetChecks =true;
			              }
			              page.openEditor(editorInput, editorID);
			              processEditor = (ProcessEditor) page.findEditor(editorInput);
						  processEditor.setActivePagePublic(5);
						  //boolean status = checktreeProcessSource.getChecked(obj);
						  if (resetChecks==true) {
							   processEditor.setTreeViewerWithCheck(true);
			              }
			            }
			            catch (PartInitException e)
			            {
			              e.printStackTrace();
			            }
			          }
					//String text=i.getText();
				}
				
				// TODO Auto-generated method stub
				
			}
			
		});
						
		
		//checktreeProcessSource.getTree().setEnabled(false); //Huascar to enable all the lists

		reuseBtn = new Button (sourceComposite, SWT.PUSH);
	    reuseBtn.setText("Reuse");
	    reuseBtn.setEnabled(false);
	    
		GridData okGridData = new GridData();		
		okGridData.horizontalAlignment = SWT.RIGHT;
		reuseBtn.setLayoutData(okGridData);
		
	    reuseBtn.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				executeReuse();
			}
	    });	
	    
		setProjectDataToCombo();
	    
	}
	public void CreateModelFromResourceSet(TreeViewer tree, ResourceSet resourceSet) {

		try {
			tree.setInput(resourceSet);
			
			final ViewerFilter modelFilter = new ViewerFilter() {
				public boolean select(Viewer viewer, Object parentElement,
						Object element) {
					
					//if (sElementFilter.contentEquals("Artefact")) {
						if (element instanceof CDOResource) {
							return true;						
						}
//						else if (element instanceof CDOResourceNode) {
//							return true;						
//						}
						else {
							return false;
						}
							
					//} else
					//	return false;
				}
			};
			tree.addFilter(modelFilter);
		} catch (Exception ex) {
			MessageDialog
					.openError(new Shell(), "Not valid model file",
							"The provided model file couldn't be parsed as an EMF resource");
			tree.setInput(null);
		}
	}
	private boolean copyMaps() {
		String sourcePath= resourceAPSource.getPath();
		String [] pathParts= sourcePath.split("/");
		String projectFolder=pathParts[1]+ "/" + pathParts[2] ;
		
		CDOResourceFolder folder = transactionCDO.getResourceFolder(projectFolder);
		EList <CDOResourceNode> contents=null;
		if(folder instanceof CDOResourceFolder){
			contents= ((CDOResourceFolder)folder).getNodes();			
		}
		
		for(int x=0;x<contents.size();x++){
			CDOResourceNode oneNode  = contents.get(x);
			if(oneNode instanceof CDOResource){
				if(oneNode.getName().endsWith("mapping")){
					CDOResource sourceResource  = (CDOResource)oneNode;
					String projectPath= resourceAPTarget.getPath();
					pathParts= projectPath.split("ASSURANCE_PROJECT");
					String targetPath=pathParts[0]+ "ASSURANCE_PROJECT/" + oneNode.getName();
					
					targetPath = createNotExistingName(targetPath);
					CDOResource targetResource  = transactionCDO.createResource(targetPath);
					
					initCopy(sourceResource, targetResource, null);

					try {				
						targetResource.save(options);						
					} catch (Exception e) {
						MessageDialog.openError(new Shell(), "Mapping reuse failed", "Error copying model " + targetPath);

						// TODO Auto-generated catch block					
						e.printStackTrace();
						transactionCDO.rollback();
						return false;
					}
				}
			}
			
		}
		return true;
	}
	
	private boolean copyBaselines(BaselineConfig bconfigActive) {
		ArrayList<Object> allObjects =null;
		
		boolean isModelChecked = false;
									
		for ( EObject key : baselineAllSelectedObjects.keySet() ) {
			CDOResource sourceResource  = (CDOResource)key;
			baselineAllSelectedObjects.put(sourceResource, null);
			isModelChecked = checktreeBaselineSource.getChecked(key);
			if (isModelChecked) {
				CheckboxTreeViewerExt treeView = null;
				Object[] selectedObjects = null;
				
				URI uri=sourceResource.getURI();
				DawnEditorInput editorInput = new DawnEditorInput(uri);
				IWorkbenchPage page = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage();
				BaselineEditor editor = (BaselineEditor) page.findEditor(editorInput);
	            if (editor != null) {
	            	treeView = editor.getTreeViewerWithCheck();
	            }
				
				if (treeView!=null) {
					selectedObjects = treeView.getCheckedElements();
				} else {
					allObjects= new ArrayList();
					for (Iterator<EObject> iterator = sourceResource.getAllContents(); iterator.hasNext();) {
						EObject aEObject = iterator.next();
						allObjects.add(aEObject);
					}
					selectedObjects = allObjects.toArray();
				}
				baselineAllSelectedObjects.put(sourceResource, selectedObjects);
				String projectPath= resourceAPTarget.getPath();
				String [] pathParts= projectPath.split("ASSURANCE_PROJECT");
				String targetPath=pathParts[0]+ "ASSURANCE_PROJECT/" + sourceResource.getName();
	
				targetPath = createNotExistingName(targetPath);
				
				CDOResource targetResource  = transactionCDO.createResource(targetPath);
				//diagramResource  = transaction.getOrCreateResource(assuranceprojectFolder.getPath() + "/" + baselineName + ".baseline_diagram");
				//To copy the refframework as baseline
				//MCPCDOResource targetResource  = assuranceprojectFolder.addResource(baselineName + ".baseline");
	
				initCopy(sourceResource, targetResource, selectedObjects);
	
				try {				
					targetResource.save(options);
					
					bconfigActive.getRefFramework().add((BaseFramework) targetResource.getContents().get(0));
					
					resourceAPTarget.save(options);				
	
				} catch (IOException e) {
					MessageDialog.openError(new Shell(), "Baseline reuse failed", "Error copying model " + targetPath);
	
					// TODO Auto-generated catch block					
					e.printStackTrace();
					transactionCDO.rollback();
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean copyEvidences() {
		ArrayList<Object> allObjects =null;
		
		boolean isModelChecked = false;

		for ( EObject key : evidenceAllSelectedObjects.keySet() ) {
			CDOResource sourceResource  = (CDOResource)key;
			evidenceAllSelectedObjects.put(sourceResource, null);
			isModelChecked = checktreeEvidenceSource.getChecked(key);
			if (isModelChecked) {
				CheckboxTreeViewerExt treeView = null;
				Object[] selectedObjects = null;
				
				URI uri=sourceResource.getURI();
				DawnEditorInput editorInput = new DawnEditorInput(uri);
				IWorkbenchPage page = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage();
				EvidenceEditor editor = (EvidenceEditor) page.findEditor(editorInput);
	            if (editor != null) {
	            	treeView = editor.getTreeViewerWithCheck();	            		            	
	            }
				
				if (treeView!=null) {
					selectedObjects = treeView.getCheckedElements();
				} else {
					allObjects= new ArrayList();
					for (Iterator<EObject> iterator = sourceResource.getAllContents(); iterator.hasNext();) {
						EObject aEObject = iterator.next();
						allObjects.add(aEObject);
					}
					selectedObjects = allObjects.toArray();
				}
				evidenceAllSelectedObjects.put(sourceResource, selectedObjects);
				String projectPath= resourceAPTarget.getPath();
				String [] pathParts= projectPath.split("ASSURANCE_PROJECT");
				String targetPath=pathParts[0]+ "EVIDENCE/" + sourceResource.getName();
								
				targetPath = createNotExistingName(targetPath);
				CDOResource targetResource  = transactionCDO.createResource(targetPath);
				
				//diagramResource  = transaction.getOrCreateResource(assuranceprojectFolder.getPath() + "/" + baselineName + ".baseline_diagram");
				//To copy the refframework as baseline
				//MCPCDOResource targetResource  = assuranceprojectFolder.addResource(baselineName + ".baseline");

				initCopy(sourceResource, targetResource, selectedObjects);

				try {		
					
					targetResource.save(options);
					targetAssetsPackage.getArtefactsModel().add((ArtefactModel) targetResource.getContents().get(0));
					resourceAPTarget.save(options);
				} catch (IOException e) {
					MessageDialog.openError(new Shell(), "Evidence reuse failed", "Error copying model " + targetPath);

					// TODO Auto-generated catch block					
					e.printStackTrace();
					transactionCDO.rollback();
					return false;
				}
			}

		}
		return true;
	}
			
						
	private String createNotExistingName(String targetPath) {
		
		if(transactionCDO.hasResource(targetPath)){
			String [] tokens= targetPath.split("/");
			
			String [] nameTokes = tokens[tokens.length-1].split("\\.");			
			//Change only the name
			String modelName = nameTokes[0];
			String modelType = nameTokes[1];
			char lastchar = modelName.charAt(modelName.length()-1);
			char prevlastchar = modelName.charAt(modelName.length()-2);
			
			if(Character.isDigit(prevlastchar) && Character.isDigit(lastchar)){
				int number= Integer.parseInt("" + prevlastchar + lastchar);
				number++;
				modelName=modelName.substring(0, modelName.length()-2) + number;
			}
			else if(Character.isDigit(lastchar)){
				int number= Integer.parseInt("" + lastchar);
				number++;
				modelName=modelName.substring(0, modelName.length()-1) + number;
			}
			else{
				modelName=modelName + "1";
			}
			targetPath = targetPath.substring(0,targetPath.lastIndexOf("/")) + "/" + modelName + "." + modelType;
			
			//Check again the new name
			targetPath=createNotExistingName(targetPath);
		}
		return targetPath;
	}

	// MCP CrossProject
	private List<CDOResource> getDiagramsFromModel(CDOResource modelCDO) {
		List<CDOResource> res = new ArrayList<CDOResource>();
		
		/* No funciona porque los diagramas no est�n cargados en memoria o es que s�lo funciona con modelos
		EObject aEObject= modelCDO.getContents().get(0);
		List<CDOObjectReference> rc = transaction.queryXRefs(CDOUtil.getCDOObject(aEObject), new EReference[] {});
		for(CDOObjectReference oneRef:rc){
			if (oneRef.getSourceObject().eResource() != modelCDO) res.add((CDOResource)(oneRef.getSourceObject().eResource()));
			if (oneRef.getTargetObject().eResource() != modelCDO) res.add((CDOResource)(oneRef.getTargetObject().eResource()));
		}
		List<EObject> list = aEObject.eCrossReferences();
		for(EObject elem : list)
		{
			res.add((CDOResource)elem.eResource());
		}
		*/
		String temp = modelCDO.getPath();
		String sourceFolderPath = temp.substring(0, temp.lastIndexOf("/"));
		CDOResourceFolder argFolder=transactionCDO.getResourceFolder(sourceFolderPath);
		EList<CDOResourceNode> listN = argFolder.getNodes();
		for ( CDOResourceNode node : listN) {
			if (node instanceof CDOResourceFolder) {
				System.out.println("getDiagramsFromModel: found subfolder =."+ node.toString());
				List<CDOResource> res2 = getDiagramsFromModel((CDOResource) node);
				res.addAll(res2);
			} else if (node instanceof CDOResource) {					
				CDOResource elem = (CDOResource)node;
				if(elem.getPath().contains(EXTENSION_DIAGRAM)){
					EObject model = (EObject) ((DiagramImpl)elem.getContents().get(0)).basicGetElement();
					//if (elem.getContents().get(0).eResource() == modelCDO)
					if (model.eResource().getURI() == modelCDO.getURI())
					{
						res.add((CDOResource)elem.getContents().get(0).eResource());
					}
				}
			}
			//listN.remove(0); ME DA PROBLEMAS PORQUE ESTOY BORRANDO!!!
		}
		
		return res;
	}
	// MCP CrossProject
	
	private boolean copyArgumentations() {
		ArrayList<Object> allObjects =null;
		
		boolean isModelChecked = false;
		// MCP CrossProject
		IProgressMonitor monitor = null;
		ProgressMonitorDialog dialog = new ProgressMonitorDialog(new Shell());
		// MCP CrossProject
		
		for ( EObject key : argAllSelectedObjects.keySet() ) {
			CDOResource sourceResource  = (CDOResource)key;
			argAllSelectedObjects.put(sourceResource, null);
			isModelChecked = checktreeArgSource.getChecked(key);
			
			// MCP CrossProject
			dialog.open();
			monitor = dialog.getProgressMonitor();
				String CREATE_ARG_DIAGRAM = "Create Cross Arg diagram";
				monitor.beginTask(CREATE_ARG_DIAGRAM, 20);
			// MCP CrossProject	
			
			if (isModelChecked) {
				CheckboxTreeViewerExt treeView = null;
				Object[] selectedObjects = null;
				
				URI uri=sourceResource.getURI();
				DawnEditorInput editorInput = new DawnEditorInput(uri);
				IWorkbenchPage page = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage();
				ArgEditor editor = (ArgEditor) page.findEditor(editorInput);
	            if (editor != null) {
	            	treeView = editor.getTreeViewerWithCheck();
	            }
				
				if (treeView!=null) {
					selectedObjects = treeView.getCheckedElements();
				} else {
					allObjects= new ArrayList();
					for (Iterator<EObject> iterator = sourceResource.getAllContents(); iterator.hasNext();) {
						EObject aEObject = iterator.next();
						allObjects.add(aEObject);
					}
					selectedObjects = allObjects.toArray();
				}
				argAllSelectedObjects.put(sourceResource, selectedObjects);
				String projectPath= resourceAPTarget.getPath();
				
				String [] pathParts= projectPath.split("ASSURANCE_PROJECT");
				String targetPath=pathParts[0]+ "ARGUMENTATION/" + sourceResource.getName();				
								String targetFolderPath=pathParts[0]+ "ARGUMENTATION";				targetPath = createNotExistingName(targetPath);				CDOResource targetResource  = transactionCDO.createResource(targetPath);				//diagramResource  = transaction.getOrCreateResource(assuranceprojectFolder.getPath() + "/" + baselineName + ".baseline_diagram");				//To copy the refframework as baseline				//MCPCDOResource targetResource  = assuranceprojectFolder.addResource(baselineName + ".baseline");
				initCopy(sourceResource, targetResource, selectedObjects);		
				try {									targetResource.save(options);					// MCP CrossProject					// crear los diagramas del modelo					//String sourceDiagram= sourceResource.getPath() + "_diagram";					//CDOResource sourceDiagramResource  = transaction.getOrCreateResource(sourceDiagram);					List<CDOResource> listDiagrams = getDiagramsFromModel(sourceResource);					for(CDOResource sourceDiagramResource: listDiagrams)					{						// lista de objetos en diagrama						//List<EObject> listRootObjects = sourceDiagramResource.getContents();						List<EObject> listRootObjectsSource = ((Diagram)sourceDiagramResource.getContents().get(0)).getPersistedChildren();						List<EObject> listRootObjects = new ArrayList<EObject>();						for(EObject element : listRootObjectsSource)						{							EObject objsource = ((ShapeImpl)element).getElement();							//EObject obj = map.get(objsource);					        CDOID cdoID = CDOUtil.getCDOObject(objsource).cdoID();							EObject obj = eIDToObjectMap.get(cdoID.toString());							listRootObjects.add(obj);						}						// crear el recurso diagram vacio						CDOResourceFolder argFolder=transactionCDO.getResourceFolder(targetFolderPath);					
						//CDOResourceFolder argFolder=projectFolder.addResourceFolder("ARGUMENTATION");				        //CDOResource targetDiagramResource  = transaction.getOrCreateResource(targetPath + EXTENSION_DIAGRAM);						String targetDiagram = targetPath.substring(0, targetPath.lastIndexOf("/") + 1) + sourceDiagramResource.getName();						targetDiagram = createNotExistingName(targetDiagram);						CDOResource targetDiagramResource  = transactionCDO.getOrCreateResource(targetDiagram);						DawnArgDiagramUtil myDiagram = new DawnArgDiagramUtil(sourceDiagramResource, listRootObjects, targetResource.getURI(), targetDiagramResource.getURI(), argFolder, transactionCDO);						myDiagram.generateDiagram(monitor);					}					//MCP: se lanza LocalCommitConflictException!!!; los anteriores commit son sin parametro pero sigue el problema					Set<CDOObject> conf = transactionCDO.getConflicts();					for (Iterator<CDOObject>it = conf.iterator(); it.hasNext();) {						CDOObject nextObject = it.next();						System.out.println("copyArgumentations: confict "+nextObject.toString());					}					transactionCDO.commit();//???MCP					// MCP CrossProject					targetAssetsPackage.getArgumentationModel().add((Case) targetResource.getContents().get(0));					resourceAPTarget.save(options);					// MCP CrossProject					//???transaction.close();					// MCP CrossProject							} catch (Exception e) {					MessageDialog.openError(new Shell(), "Argumentation reuse failed", "Error copying model " + targetPath);					// TODO Auto-generated catch block										e.printStackTrace();					transactionCDO.rollback();					dialog.close();
					return false;				}
			}
			dialog.close();
		}// cycle for
		
		return true;
	}	 
			
	
	private boolean copyProcesses() {
		ArrayList<Object> allObjects =null;
		
		boolean isModelChecked = false;

		for ( EObject key : processAllSelectedObjects.keySet() ) {
			CDOResource sourceResource  = (CDOResource)key;
			processAllSelectedObjects.put(sourceResource, null);
			isModelChecked = checktreeProcessSource.getChecked(key);
			if (isModelChecked) {
				CheckboxTreeViewerExt treeView = null;
				Object[] selectedObjects = null;
				
				URI uri=sourceResource.getURI();
				DawnEditorInput editorInput = new DawnEditorInput(uri);
				IWorkbenchPage page = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage();
				ProcessEditor editor = (ProcessEditor) page.findEditor(editorInput);
	            if (editor != null) {
	            	treeView = editor.getTreeViewerWithCheck();
	            }
	            
				if (treeView!=null) {
					selectedObjects = treeView.getCheckedElements();
				} else {
					allObjects= new ArrayList();
					for (Iterator<EObject> iterator = sourceResource.getAllContents(); iterator.hasNext();) {
						EObject aEObject = iterator.next();
						allObjects.add(aEObject);
					}
					selectedObjects = allObjects.toArray();
				}
				processAllSelectedObjects.put(sourceResource, selectedObjects);
				String projectPath= resourceAPTarget.getPath();				

				String [] pathParts= projectPath.split("ASSURANCE_PROJECT");
				String targetPath=pathParts[0]+ "PROCESSES/" + sourceResource.getName();
											
				targetPath = createNotExistingName(targetPath);
				CDOResource targetResource  = transactionCDO.createResource(targetPath);
				//diagramResource  = transaction.getOrCreateResource(assuranceprojectFolder.getPath() + "/" + baselineName + ".baseline_diagram");
				//To copy the refframework as baseline
				//MCPCDOResource targetResource  = assuranceprojectFolder.addResource(baselineName + ".baseline");
	
				initCopy(sourceResource, targetResource, selectedObjects);			    

				try {				
					targetResource.save(options);
					
					targetAssetsPackage.getProcessModel().add((ProcessModel) targetResource.getContents().get(0));
					resourceAPTarget.save(options);
	
				} catch (IOException e) {
					MessageDialog.openError(new Shell(), "Process reuse failed", "Error copying model " + targetPath);
	
					// TODO Auto-generated catch block
					e.printStackTrace();
					transactionCDO.rollback();
					return false;
				}
			}	
		}
		return true;
	}

	private void initCopy(CDOResource sourceResource, CDOResource targetResource, Object[] selectedObjects) {
		// Create empty roots
		boolean objChecked = false;
		for (EObject aEObject : sourceResource.getContents()) {
			targetResource.getContents().add(getCorrespondingEObject(aEObject));
		}

		// Copy EObjects, containment in the resource is done automatically since we have
		// already attached the corresponding root EObject to the target resource
		for (Iterator<EObject> iterator = sourceResource.getAllContents(); iterator.hasNext();) {
			EObject aEObject = iterator.next();
			
			//Avoid copy assurance assent events
			if(aEObject instanceof AssuranceAssetEvent){
				continue;
			}
			
			if(selectedObjects!=null){
				objChecked = false;
				objChecked = existObject(aEObject, selectedObjects);
				if (objChecked==false) {
					  continue;
				}
			}
			
			EObject bEObject = getCorrespondingEObject(aEObject);

			copyAEObjectToBEObject(aEObject, bEObject, selectedObjects);

			// MCP CrossProject			
			if(aEObject instanceof ModelElement)
			{
		        CDOID cdoID = CDOUtil.getCDOObject(aEObject).cdoID();
		        Long  cdoIdLong = new Long(cdoID.toURIFragment());
				eIDToObjectMap.put(cdoID.toString(), bEObject);
			}
			// MCP CrossProject			
		}
	}	
	
	private boolean existObject(Object searchedObj, Object[] selectedObjects){
		boolean exist = false;
		for(Object element: selectedObjects){
			if (element.toString().equals(searchedObj.toString())) {
				exist = true;
				//selectedObjects= ArrayUtils.removeElement(selectedObjects,element);
				break;
			}
		}
		return exist;
	}
	
	private  EObject getCorrespondingEObject(EObject aEObject) {
		
		EObject bEObject = map.get(aEObject);			
		if (bEObject == null) {			
			if(aEObject instanceof MapGroup){
				for ( EObject key : map.keySet() ) {
				   if(key instanceof MapGroup){
					   if(CDOUtil.getCDOObject(key).cdoID() == CDOUtil.getCDOObject(aEObject).cdoID()){
						   return map.get(key);
					   }
				   }
				}
			}
			
			EClass aEClass = aEObject.eClass();			
			//if(aEClass.getName().startsWith("Ref")){
			
				//String bClassName = aEClass.getName().replaceFirst("Ref", "Base");
				// Second search the corresponding EClass in BPackage
				//EClass bEClass = (EClass) BaselinePackage.eINSTANCE.getEClassifier(bClassName);
				// Create a new empty instance and register to avoid dups
				bEObject = EcoreUtil.create(aEClass);
				map.put(aEObject, bEObject);
			//}
			 /*if (aEClass.getName().equals("MapJustification")){
				bEObject = EcoreUtil.create(aEClass);
				map.put(aEObject, bEObject);
			}
			else if (aEClass.getName().equals("MapGroup")){
				bEObject = EcoreUtil.create(aEClass);
				map.put(aEObject, bEObject);
			}
			else{*/
				//bEObject=aEObject;
				//map.put(aEObject, aEObject);
			//}
		}
		return bEObject;
	}
	
	private  void copyAEObjectToBEObject(EObject aEObject, EObject bEObject, Object[] selectedObjects) {
		boolean objChecked = false;
		for (EStructuralFeature aFeature : aEObject.eClass().getEAllStructuralFeatures()) {
			if (aEObject.eIsSet(aFeature)) {
				// Get the corresponding feature in the target EClass.
				// Get simply the feature with the same name
				
				EStructuralFeature bFeature = bEObject.eClass().getEStructuralFeature(aFeature.getName());								
				
				if(bFeature != null){
					if (aFeature instanceof EAttribute) {
						bEObject.eSet(bFeature, aEObject.eGet(aFeature));
					} else { // EReference
																													
						if(aEObject.eClass().getName().equals("BaseEquivalenceMap") && aFeature.getName().equals("target") ){
							bEObject.eSet(bFeature,aEObject.eGet(aFeature));
						}
						else if(aFeature.getName().equals("refFramework") || aFeature.getName().equals("refAssurableElement")){
							bEObject.eSet(bFeature,aEObject.eGet(aFeature));
						}
						else if(aEObject.eClass().getName().equals("BaseComplianceMap") && aFeature.getName().equals("target") ){
							if(aEObject.eGet(aFeature) instanceof EList){
								@SuppressWarnings("unchecked")
								EList<EObject> aList = (EList<EObject>) aEObject.eGet(aFeature);
								EList<EObject> bList = new BasicEList<EObject>(); 
								boolean hasElements = false;
								for (int i = 0; i < aList.size(); i++) {
									if ((aList.get(i) instanceof Artefact) || (aList.get(i) instanceof ArtefactDefinition)) {
										objChecked = false;
										for ( EObject key : evidenceAllSelectedObjects.keySet() ) {
											if(evidenceAllSelectedObjects.get(key)!=null){
												objChecked = existObject(aList.get(i), evidenceAllSelectedObjects.get(key));
												if (objChecked==true) {
													bList.add(getCorrespondingEObject(aList.get(i)));
													hasElements = true;
													break;
												}
											}
										}
									}
									else if ((aList.get(i) instanceof Activity) || (aList.get(i) instanceof Participant)) {
										objChecked = false;
										for ( EObject key : processAllSelectedObjects.keySet() ) {
											if(processAllSelectedObjects.get(key)!=null){
												objChecked = existObject(aList.get(i), processAllSelectedObjects.get(key));
												if (objChecked==true) {
													bList.add(getCorrespondingEObject(aList.get(i)));
													hasElements = true;
													break;
												}
											}
										}
									}
									else if (aList.get(i) instanceof Claim) {
										objChecked = false;
										for ( EObject key : argAllSelectedObjects.keySet() ) {
											if(argAllSelectedObjects.get(key)!=null){
												objChecked = existObject(aList.get(i), argAllSelectedObjects.get(key));
												if (objChecked==true) {
													bList.add(getCorrespondingEObject(aList.get(i)));
													hasElements = true;
													break;
												}
											}
										}
									}
								}
								if (hasElements) {
									bEObject.eSet(bFeature, bList);
								}
							}	
						
						}
						else if(aEObject.eClass().getName().equals("InformationElementCitation") && aFeature.getName().equals("artefact") ){
							if(aEObject.eGet(aFeature) instanceof EList){
								@SuppressWarnings("unchecked")
								EList<EObject> aList = (EList<EObject>) aEObject.eGet(aFeature);
								EList<EObject> bList = new BasicEList<EObject>(); 
								boolean hasElements = false;
								for (int i = 0; i < aList.size(); i++) {
									objChecked = false;
									for ( EObject key : evidenceAllSelectedObjects.keySet() ) {
										if(evidenceAllSelectedObjects.get(key)!=null){
											objChecked = existObject(aList.get(i), evidenceAllSelectedObjects.get(key));
											if (objChecked==true) {
												bList.add(getCorrespondingEObject(aList.get(i)));
												hasElements = true;
												break;
											}
										}
									}
								}
								if (hasElements) {
									bEObject.eSet(bFeature, bList);
								}
							}	
						}
						
						else if( (aEObject.eClass().getName().equals("Participant") && aFeature.getName().equals("ownedArtefact") ) ||
								 (aEObject.eClass().getName().equals("Activity") && aFeature.getName().equals("requiredArtefact") ) ||
								 (aEObject.eClass().getName().equals("Activity") && aFeature.getName().equals("producedArtefact") ) ||
								 (aEObject.eClass().getName().equals("Technique") && aFeature.getName().equals("createdArtefact") ) ){
							if(aEObject.eGet(aFeature) instanceof EList){
								@SuppressWarnings("unchecked")
								EList<EObject> aList = (EList<EObject>) aEObject.eGet(aFeature);
								EList<EObject> bList = new BasicEList<EObject>(); 
								boolean hasElements = false;
								for (int i = 0; i < aList.size(); i++) {
									objChecked = false;
									for ( EObject key : evidenceAllSelectedObjects.keySet() ) {
										if(evidenceAllSelectedObjects.get(key)!=null){
											objChecked = existObject(aList.get(i), evidenceAllSelectedObjects.get(key));
											if (objChecked==true) {
												bList.add(getCorrespondingEObject(aList.get(i)));
												hasElements = true;
												break;
											}
										}
									}
								}
								if (hasElements) {
									bEObject.eSet(bFeature, bList);
								}
							}	
						}
						/*else if(aEObject.eClass().getName().equals("MapGroup")){
							EObject copy= getCorrespondingEObject((EObject)aEObject.eGet(aFeature));
							bEObject.eSet(bFeature, copy);							
						}
						
						else if(aFeature.getName().equals("refFramework") || aFeature.getName().equals("refAssurableElement")){
							bEObject.eSet(bFeature,aEObject.eGet(aFeature));
						}
						/*else if (aFeature.getName().equals("mapJustification")){
							EObject copy= getCorrespondingEObject((EObject)aEObject.eGet(aFeature));
							//copyAEObjectToBEObject((EObject)aEObject.eGet(aFeature),copy, null);
							
							bEObject.eSet(bFeature, copy);
						}*/
						/*else if (aFeature.getName().equals("ownedTechnique")){
							EObject copy= getCorrespondingEObject((EObject)aEObject.eGet(aFeature));
							//copyAEObjectToBEObject((EObject)aEObject.eGet(aFeature),copy, null);
							
							bEObject.eSet(bFeature, copy);
						}*/						
						else if (aFeature.getName().equals("mapGroup")){
							if(aEObject.eClass().getName().equals("BaseComplianceMap")){
								EObject copy= getCorrespondingEObject((EObject)aEObject.eGet(aFeature));
								bEObject.eSet(bFeature, copy);
							}
							else{
							//EObject copy= getCorrespondingEObject((EObject)aEObject.eGet(aFeature));
							//copyAEObjectToBEObject((EObject)aEObject.eGet(aFeature),copy, null);
							
							//bEObject.eSet(bFeature, copy);
							
								bEObject.eSet(bFeature,aEObject.eGet(aFeature));
							}
						}
						else{	
							if(!aFeature.getName().equals("lifecycleEvent")){
								if(aEObject.eGet(aFeature) instanceof EList){
									@SuppressWarnings("unchecked")
									EList<EObject> aList = (EList<EObject>) aEObject.eGet(aFeature);
									EList<EObject> bList = new BasicEList<EObject>(); 
									for (int i = 0; i < aList.size(); i++) {
										if(selectedObjects!=null){
											objChecked = false;
											objChecked = existObject(aList.get(i), selectedObjects);
											if (objChecked==false) {
												  continue;
											}
										}
										bList.add(getCorrespondingEObject(aList.get(i)));
									}
									bEObject.eSet(bFeature, bList);
								}
								else{
									if(selectedObjects!=null){
										objChecked = false;
										objChecked = existObject((EObject)aEObject.eGet(aFeature), selectedObjects);
										if (objChecked==false) {
											  continue;
										}
									}
									//System.out.println("Feature " + aFeature.getName() + " of " + aEObject.eClass().getName() + " not copied to Baseline");
									EObject copy= getCorrespondingEObject((EObject)aEObject.eGet(aFeature));
									//copyAEObjectToBEObject((EObject)aEObject.eGet(aFeature),copy, null);
									
									bEObject.eSet(bFeature, copy);
								}
							}
						}
					}
				}
			}
		}
		
		//Add the linkf to the refframeork model concepts.
		
		/*if(aEObject instanceof RefFramework){
			 EStructuralFeature bFeature = bEObject.eClass().getEStructuralFeature("refFramework");
			 bEObject.eSet(bFeature,aEObject);
		}
		else if(bEObject instanceof BaseAssurableElement){
			EStructuralFeature bFeature = bEObject.eClass().getEStructuralFeature("refAssurableElement");
			 bEObject.eSet(bFeature,aEObject);
		}*/
		
	}

	ArrayList<CDOResourceNode> findAssuranceProjects(){
		ArrayList<CDOResourceNode> projectList=new ArrayList<CDOResourceNode>();
		CDOResourceNode[] listR=  transactionCDO.getElements();			
		for (int i = 0; i < listR.length; i++) {
			if (listR[i] instanceof CDOResourceFolder) {
				checkFolderContents((CDOResourceFolder) listR[i],ASSURANCEPROJECT, projectList);
			} else if (listR[i].getName().endsWith(ASSURANCEPROJECT)) {
				projectList.add(listR[i]);
//				refListDirMap.add(listR[i].getPath());
				//System.out.println(listR[i].getPath());
			}
		}
		return projectList;
	}
	

	public static boolean hasReadPermission(CDOResourceNode node) {
		// force to get the last revision
		node.cdoReload();
		CDOPermission permission = node.cdoRevision().getPermission();
		return permission.isReadable();
	}
	
	private ArrayList<CDOResourceNode> checkFolderContents(CDOResourceFolder cdoResourceFolder, String sCadena, ArrayList<CDOResourceNode>projectList) {
		if (hasReadPermission(cdoResourceFolder)) {
			EList<CDOResourceNode> listN = cdoResourceFolder.getNodes();
			for (int i = 0; i < listN.size(); i++) {
				if (listN.get(i) instanceof CDOResourceFolder) {
					checkFolderContents((CDOResourceFolder) listN.get(i), sCadena, projectList);
				} else if (listN.get(i).getName().endsWith(sCadena)) {
					projectList.add(listN.get(i));
					//System.out.println(listN.get(i).getPath());
				}
			}
		}
		return projectList;
	}
	
	protected void sourceProjectChanged() {
		int sourceindex=sourceProjectCombo.getSelectionIndex();
		CDOResourceNode selectedSource=projects.get(sourceindex);
		ResourceSet  baselineSourceResourceSet = new ResourceSetImpl();
		baselineSourceResourceSet.eSetDeliver(true);
		try {
			
			CDOResource artefactResource = null;
			CDOResource processResource = null;
			CDOResource argResource = null;
			
			resourceAPSource = transactionCDO.getResource(selectedSource.getPath());
			assuranceProjectSource = (AssuranceProject) resourceAPSource.getContents().get(0);
			if(targetProjectCombo.getSelectionIndex()>-1){
				reuseBtn.setEnabled(true);
			}						
			//Load Source Baselines
			EList<BaselineConfig> lstBaselineConfig = assuranceProjectSource.getBaselineConfig();
			Iterator<BaselineConfig> baselineConfig = lstBaselineConfig.iterator();
			
			BaselineConfig bconfig=null;
			
			boolean bFind = false;
			while (baselineConfig.hasNext() && !bFind) {
				bconfig = baselineConfig.next();
				if (bconfig.isIsActive()) {
					bFind = true;
					
					EList<BaseFramework> lstBaseFramework = bconfig.getRefFramework();										
					Iterator<BaseFramework> itbaseFramework = lstBaseFramework.iterator();
					
					while (itbaseFramework.hasNext()) {
															
						BaseFramework baseFramework = itbaseFramework.next();
						baselineResource = CDOUtil.getCDOObject(baseFramework).cdoResource();
						try {
							baselineSourceResourceSet.getResources().add(baselineResource);
						} catch (NullPointerException e) {
							//Ignore it 
						}
						baselineAllSelectedObjects.put(baselineResource, null);
					}
					//treeBaselineSource.setInput(baselineSourceResourceSet);
						
					CreateModelFromResourceSet(checktreeBaselineSource, baselineSourceResourceSet);
					baselineSourceResourceSet= null;
					//To search the nodes to be moved.
					checktreeBaselineSource.expandAll();
																			
					//To expand the moved nodes.
					//treeBaselineSource.expandAll();
				}
			}
			
			//Load the evidence models of the active assetsPackage of the selected assurance project 
			EList<AssetsPackage> lstAssetsPackage = assuranceProjectSource.getAssetsPackage();
			Iterator<AssetsPackage> assetsPackage = lstAssetsPackage.iterator();
			
			ResourceSet evidenceSourceResourceSet = new ResourceSetImpl();
			AssetsPackage aPackage=null;
			
			//Load the argumentation models of the active assetsPackage of the selected assurance project 												
			ResourceSet argSourceResourceSet = new ResourceSetImpl();
			
			//Load the process models of the active assetsPackage of the selected assurance project 												
			ResourceSet processSourceResourceSet = new ResourceSetImpl();
			
			bFind = false;
			while (assetsPackage.hasNext() && !bFind) {
				aPackage = assetsPackage.next();
				if (aPackage.isIsActive()) {
					bFind= true;
					EList<ArtefactModel> lstArtefactModel = aPackage.getArtefactsModel();
					Iterator<ArtefactModel> artefactModel = lstArtefactModel.iterator();
					while (artefactModel.hasNext()) {
						ArtefactModel artefact = artefactModel.next();
						artefactResource = CDOUtil.getCDOObject(artefact).cdoResource();
						try {
							evidenceSourceResourceSet.getResources().add(artefactResource);
						} catch (NullPointerException e) {
							// TODO Auto-generated catch block
							//e.printStackTrace();
						}
						evidenceAllSelectedObjects.put(artefactResource, null);
					}
					
					EList<Case> lstArgModel = aPackage.getArgumentationModel();
					Iterator<Case> argModel = lstArgModel.iterator();
					while (argModel.hasNext()) {
						Case argRoot = argModel.next();
						argResource = CDOUtil.getCDOObject(argRoot).cdoResource();
						try {
							argSourceResourceSet.getResources().add(argResource);
						} catch (NullPointerException e) {
							// TODO Auto-generated catch block
							//Do nothing
						}
						argAllSelectedObjects.put(argResource, null);
					}
					
					EList<ProcessModel> lstProcessModel = aPackage.getProcessModel();
					Iterator<ProcessModel> processModel = lstProcessModel.iterator();
					while (processModel.hasNext()) {
						ProcessModel processRoot = processModel.next();
						processResource = CDOUtil.getCDOObject(processRoot).cdoResource();
						try {
							processSourceResourceSet.getResources().add(processResource);
						} catch (NullPointerException e) {
							// TODO Auto-generated catch block
							//e.printStackTrace();
						}
						processAllSelectedObjects.put(processResource, null);
					}	
				}
				CreateModelFromResourceSet(checktreeEvidenceSource, evidenceSourceResourceSet);
				checktreeEvidenceSource.expandAll();
				
				CreateModelFromResourceSet(checktreeArgSource, argSourceResourceSet);
				checktreeArgSource.expandAll();	
				
				CreateModelFromResourceSet(checktreeProcessSource, processSourceResourceSet);
				checktreeProcessSource.expandAll();	
			}	
			
			
		} catch (Exception cdoe) {
			cdoe.printStackTrace();
			transactionCDO.rollback();
		}
	}
	
	protected void executeReuse() {
		boolean allOK=true;
		if(targetProjectCombo.getText().length()==0){
			MessageDialog.openError(new Shell(), "Selected Source Project",
					"You must select the source project of the reuse.");
			return;
		}
		
		if ( (checktreeEvidenceSource.getCheckedElements().length!=0) ||
				 (checktreeArgSource.getCheckedElements().length!=0) ||
				 (checktreeProcessSource.getCheckedElements().length!=0) ||
				 (checktreeBaselineSource.getCheckedElements().length!=0)) {
			//Load the evidence models of the active assetsPackage of the selected assurance project 
			EList<AssetsPackage> lstAssetsPackage = assuranceProjectTarget.getAssetsPackage();
			Iterator<AssetsPackage> assetsPackage = lstAssetsPackage.iterator();

			map = new HashMap<EObject, EObject>();
			options = new HashMap<Object, Object>();
			
			while (assetsPackage.hasNext()) {
				targetAssetsPackage = assetsPackage.next();
				if (targetAssetsPackage.isIsActive()) {
					break;				
				}
			}
		}
		else {
			MessageDialog.openError(new Shell(), "Selected Models",
					"You must select at least one model to be reused.");
			return;
		}

		
		try {
			if (checktreeBaselineSource.getCheckedElements().length!=0) {
				MessageDialog.openWarning(new Shell(), "Changes to target Assurance Project",
						"The model will be copied and related to a new Asset Package and Baseline Configuration that will be set up as the active ones.");
				projectPackage = AssuranceprojectPackage.eINSTANCE;
				projectFactory = projectPackage.getAssuranceprojectFactory();
				
				AssetsPackage newAP= projectFactory.createAssetsPackage();								
				newAP.setId("" + assuranceProjectTarget.getAssetsPackage().size());
				newAP.setName("AP_" + assuranceProjectTarget.getAssetsPackage().size());
				newAP.setDescription("Created automatically for Cross Project Reuse");			
				newAP.setIsActive(true);
				assuranceProjectTarget.getAssetsPackage().add(newAP);
				targetAssetsPackage.setIsActive(false);
				targetAssetsPackage=newAP;
				
				BaselineConfig newBC = projectFactory.createBaselineConfig();
				newBC.setId("" + assuranceProjectTarget.getBaselineConfig().size());
				newBC.setName("AP_" + assuranceProjectTarget.getBaselineConfig().size());
				newBC.setDescription("Created automatically for Cross Project Reuse");			
				
				//Load the evidence models of the active assetsPackage of the selected assurance project 
				EList<BaselineConfig> lstBC = assuranceProjectTarget.getBaselineConfig();
				Iterator<BaselineConfig> bcI = lstBC.iterator();
										
				//Inactive the active Baseline Config
				while (bcI.hasNext()) {
					BaselineConfig oneBC = bcI.next();
					if (oneBC.isIsActive()) {
						oneBC.setIsActive(false);				
					}
				}
				
				newBC.setIsActive(true);
				assuranceProjectTarget.getBaselineConfig().add(newBC);
				if (allOK) allOK=copyEvidences();
				if (allOK) allOK=copyArgumentations();
				if (allOK) allOK=copyProcesses();	
				if (allOK) allOK=copyMaps();
				//Baselines is the last because has mappings to the other models
				if (allOK) allOK=copyBaselines(newBC);
			}
			else {
				if (allOK) allOK=copyEvidences();
				if (allOK) allOK=copyArgumentations();
				if (allOK) allOK=copyProcesses();				
			}

			if(allOK){
				MessageDialog.openInformation(new Shell(), "Reuse success", "Selected models reused");
			}

		} catch (Exception cdoe) {
			cdoe.printStackTrace();
		}
		
	}
	
	@Override
	public void dispose() {		
		if (!transactionCDO.isClosed()) {
			transactionCDO.close();
		}
	}
	
    
    /**
     * Create toolbar.
     */
    private void createToolbar() {
            IToolBarManager mgr = getViewSite().getActionBars().getToolBarManager();
            mgr.add(addItemAction);
    }
    
    public void createActions() {
        addItemAction = new Action("Refresh") {
                public void run() { 
                	setProjectDataToCombo();
                   }
           };
        addItemAction.setImageDescriptor(getImageDescriptor("refresh.gif"));
//        
//        // Add selection listener.
//        viewer.addSelectionChangedListener(new ISelectionChangedListener() {
//                   public void selectionChanged(SelectionChangedEvent event) {
//                           updateActionEna55blement();
//                   }
//           });
   }
    
    /**
     * Returns the image descriptor with the given relative path.
     */
    private ImageDescriptor getImageDescriptor(String relativePath) {
           	String iconPath = "icons/";
           	ReusePlugin plugin = ReusePlugin.getDefault();
           	ImageDescriptor img = plugin.getImageDescriptor(iconPath + relativePath);
            return img;
    }
    
    private void setProjectDataToCombo() {
    	sourceProjectCombo.removeAll();
    	targetProjectCombo.removeAll();
    	checktreeBaselineSource.setInput(null);
    	checktreeProcessSource.setInput(null);
    	checktreeArgSource.setInput(null);
    	checktreeEvidenceSource.setInput(null);
    	reuseBtn.setEnabled(false);
		projects=findAssuranceProjects();
		Iterator<CDOResourceNode> iterator = projects.iterator();
		while(iterator.hasNext()){
			CDOResourceNode proj=iterator.next();
			String name=proj.getName();
			sourceProjectCombo.add(name);
			if(proj.cdoRevision().getPermission().isWritable()){
				targetProjectCombo.add(name);
			}
		}
    }
		
}

