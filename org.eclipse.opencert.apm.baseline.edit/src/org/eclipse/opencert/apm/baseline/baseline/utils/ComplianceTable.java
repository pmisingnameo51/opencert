/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.baseline.baseline.utils;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.JOptionPane;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.cdo.common.util.CDOException;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.edit.ui.util.EditUIUtil;
import org.eclipse.emf.eef.runtime.ui.notify.OpenWizardOnDoubleClick;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.net4j.util.ArrayUtil;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseApplicability;
import org.eclipse.opencert.apm.baseline.baseline.BaseApplicabilityLevel;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement;
import org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap;
import org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityApplicability;
import org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityLevel;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;
import org.eclipse.opencert.apm.baseline.baseline.BaseRole;
import org.eclipse.opencert.apm.baseline.baseline.BaseTechnique;
import org.eclipse.opencert.apm.baseline.baseline.util.ModelStorage;
import org.eclipse.opencert.infra.mappings.mapping.MapGroup;
import org.eclipse.opencert.infra.mappings.mapping.MapJustification;
import org.eclipse.opencert.infra.mappings.mapping.MapKind;
import org.eclipse.opencert.infra.mappings.mapping.MapModel;
import org.eclipse.opencert.pkm.refframework.refframework.provider.RefframeworkEditPlugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

public class ComplianceTable extends Dialog {

	private static final String ClassDiagram = "org.eclipse.opencert.apm.baseline.baseline.diagram.part.DawnBaselineDiagramEditor";
	protected IEditorPart editor;
	protected URI resourceURI;
	
	private CDOView viewCDO = null;
	private CDOTransaction transaction = null;
	private CDOSession sessionCDO = null;
	
	private CDOResource resourceFrom = null;

	protected Table tableHistory;
	
	protected Text txtJustify;
	
	protected ListViewer targetsList;
	
	protected org.eclipse.swt.widgets.Combo refList;
	protected ArrayList<String> refListDirMap;
	
	protected Combo cbMapGroup;
	
	String sElementFilter;
	protected Combo cbFilterElementMap;
	protected Button btnGap;
	protected Button btnFull;
	protected Button btnPartial;
	protected Button btnNoMap;
	
	//protected Button btnSearch;
	
	protected String qref;
	protected BaseFramework baseFrameworkFrom;
	protected Color green;
	protected Color orange;
	protected Color red;
	
	private static final String CRITICALITY_SELECTION = "Select the Criticality level:";
	private static final String APPLICABILITY_SELECTION = "Select the Applicability level:";
	protected CheckboxTreeViewerExt criticalityViewer;
	protected CheckboxTreeViewerExt aplicabilityViewer;
	//protected BaseCriticalityLevel[] critState;
	
	boolean writePermission=false;
	
	public ComplianceTable(Shell parentShell, boolean writePermission) {
		super(parentShell);

		this.writePermission=writePermission;
		
		editor = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage().getActiveEditor();
		
		if (editor.getClass().getName().contentEquals(ClassDiagram)) {
			
			ModelStorage ms=ModelStorage.getInstance();
			resourceURI = ms.getResourceURI();
				
		} else {
			resourceURI = EditUIUtil.getURI(editor.getEditorInput());
			
		}
		
		System.out.println("MODEL _used: " + resourceURI.toString());
		
		
		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
		CDOView [] views = sessionCDO.getViews();
		if (views.length!=0) {
			viewCDO = views[0];				
		}
		transaction = sessionCDO.openTransaction();

		setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX);
	}
	
	/*public ComplianceTable(Shell parentShell,URI resourceURI) {
		super(parentShell);

		
		this.resourceURI = resourceURI;
		
		//System.out.println("MODELO _usar: " + resourceURI.toString());
		
		
		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
		viewCDO = CDOConnectionUtil.instance.openView(sessionCDO);
		transaction = sessionCDO.openTransaction();

		setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX);
	}*/

	public ComplianceTable(IShellProvider parentShell) {
		super(parentShell);
	}

	@Override
	protected void constrainShellSize() {
		// TODO Auto-generated method stub
		super.constrainShellSize();
		//getShell().setMaximized(true);
	}
	
	@Override
	protected Control createDialogArea(final Composite parent) {						

		ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
				ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		
		final Composite contents = (Composite) super.createDialogArea(parent);
		
		GridLayout contentsGridLayout = (GridLayout) contents.getLayout();
		contentsGridLayout.numColumns = 2;
		
		GridData contentsGridData = (GridData) contents.getLayoutData();
		contentsGridData.horizontalAlignment = SWT.FILL;
		contentsGridData.heightHint =600;
		createSearchArea(contents);
		createComplianceTable(contents,adapterFactory);
		CreateTableContents(baseFrameworkFrom);
		
		return contents;
	}

	protected void createSearchArea(final Composite contents) {

		Composite controlSearch = new Composite(contents, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, false);
			data.horizontalAlignment = SWT.END;
			data.horizontalAlignment = SWT.FILL;
			data.verticalAlignment= SWT.FILL;
			data.horizontalSpan=2;
			controlSearch.setLayoutData(data);
			GridLayout layout = new GridLayout();
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 3; 
			controlSearch.setLayout(layout);
		}

		Composite controlMapping = new Composite(controlSearch, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, false);
			data.horizontalAlignment = SWT.END;
			data.horizontalAlignment = SWT.FILL;
			data.verticalAlignment= SWT.FILL;
			controlMapping.setLayoutData(data);
			GridLayout layout = new GridLayout();
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 1; 
			controlMapping.setLayout(layout);
		}
			
		Group groupFilter = new Group(controlMapping, SWT.NULL);
		
		groupFilter.setLayoutData(new GridData(SWT.FILL,SWT.NONE,false,true,1,1));
		groupFilter.setText("Filtering");
		GridLayout filterLayout= new GridLayout();
		filterLayout.numColumns = 2;
		groupFilter.setLayout(filterLayout);
		
		Group groupType = new Group(controlMapping, SWT.NULL);
		groupType.setLayoutData(new GridData(SWT.FILL,SWT.NONE,false,true));
		groupType.setText("Map Type");
		GridLayout typeLayout= new GridLayout();
		typeLayout.numColumns = 2;
		groupType.setLayout(typeLayout);
		
		Composite controlCrit = new Composite(controlSearch, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, false);
			data.horizontalAlignment = SWT.END;
			data.horizontalAlignment = SWT.FILL;
			data.verticalAlignment= SWT.FILL;
			data.horizontalSpan=2;
			controlCrit.setLayoutData(data);
			GridLayout layout = new GridLayout();
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 2; // antes ponia 3
			controlCrit.setLayout(layout);
		}
		
		Group groupCrit = new Group(controlCrit, SWT.NULL);
		groupCrit.setLayoutData(new GridData(SWT.FILL,SWT.NONE,true,true,2,1));
		groupCrit.setText("Criticality/Applicability Filter");
		groupCrit.setLayout(new GridLayout(2, false));

		Label mapping = new Label(groupFilter, SWT.NONE);
		mapping.setText("Map Model: ");
		refList = new org.eclipse.swt.widgets.Combo(groupFilter, SWT.READ_ONLY);	
		refListDirMap = new ArrayList<String>();
		/*En este caso solo incluimos el mapping del mapgroup relacionado con el baseFramework a traves del baselineconfig*/	
		
		String path="";
		for (int i = 0; i< resourceURI.segmentCount(); i++ ) {
			path = path + resourceURI.segment(i) + "/";
		}
		
		path = path.substring(0,path.length()-1);
		
		try {
			resourceFrom = transaction.getResource(path);	
		} catch (CDOException e) {

			e.printStackTrace();
		}
		
		
		baseFrameworkFrom = (BaseFramework) resourceFrom.getContents().get(0);						
		
		CDOResourceFolder assuranceprojectFolder = resourceFrom.getFolder();
		//CDOResourceFolder projectFolder = assuranceprojectFolder.getFolder();		
		EList<CDOResourceNode> lstResourceNodes = assuranceprojectFolder.getNodes();
		Iterator<CDOResourceNode> resourceNode = lstResourceNodes.iterator();
		
		//boolean bFind = false;
		String sName = "";	
		refList.add("All");
		refList.select(0);
		while (resourceNode.hasNext()) {
			CDOResourceNode node = resourceNode.next();
			sName = node.getName();
			System.out.println(sName);
			if (sName.endsWith(".mapping")) {
				refList.add(sName);
				refListDirMap.add(node.getPath());
				//bFind = true;
			}
		}
								
		qref = "";
		refList.addSelectionListener(new SelectionAdapter() {
		      public void widgetSelected(SelectionEvent e) {
		    	  if (refList.getSelectionIndex() != 0) {
			  		qref = refListDirMap.get(refList.getSelectionIndex()-1).toString();
			  		String pathModelMapping = resourceURI.segment(0)+ "/" + resourceURI.segment(1)+ "/" + refList.getItems()[refList.getSelectionIndex()];
					CDOResource resourceMapModel = transaction.getResource(pathModelMapping);				
					final MapModel mapModel = (MapModel) resourceMapModel.getContents().get(0);
			  		CreateModelFromFiletocombo(cbMapGroup,mapModel);
		    	  }
		    	  else{
		    		  qref="";
		    		  cbMapGroup.removeAll();
		    		  //refListDirMap= new ArrayList<String>();
		    	  }
		      }
		});
		
		Label mapLabel = new Label(groupFilter, SWT.NONE);
		mapLabel.setText("Map Group: ");						
		cbMapGroup = new Combo (groupFilter, SWT.READ_ONLY);
		cbMapGroup.add("All");
		cbMapGroup.select(0);
		
		Label filtermapLabel = new Label(groupFilter, SWT.NONE);
		filtermapLabel.setText("Map Element: ");
			
		cbFilterElementMap = new Combo (groupFilter, SWT.READ_ONLY);		
		cbFilterElementMap.add("All");
		cbFilterElementMap.add("Activity");
		cbFilterElementMap.add("Artefact");
		cbFilterElementMap.add("Requirement");
		cbFilterElementMap.add("Technique");
		cbFilterElementMap.add("Role");
		cbFilterElementMap.select(0);
		
		cbFilterElementMap.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {

				if (cbFilterElementMap.getSelectionIndex() != -1 ) {
					sElementFilter = cbFilterElementMap.getItem(cbFilterElementMap.getSelectionIndex());
					CreateTableContents(baseFrameworkFrom);
				}
			}
		});
		
//		Label filterMapType = new Label(groupFilter, SWT.NONE);
//		filterMapType.setText("   Map type: ");
			
		btnGap = new Button(groupType,SWT.CHECK);	
		btnGap.setText("Not Defined  ");
		btnGap.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
					CreateTableContents(baseFrameworkFrom);
			}
		});
		
		btnFull = new Button(groupType,SWT.CHECK);	
		btnFull.setText("Full  ");		
		btnFull.setSelection(true);
		btnFull.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				CreateTableContents(baseFrameworkFrom);
			}
		});
		
		btnPartial = new Button(groupType,SWT.CHECK);	
		btnPartial.setText("Partial  ");
		btnPartial.setSelection(true);
		btnPartial.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				CreateTableContents(baseFrameworkFrom);
			}
		});
		
		btnNoMap = new Button(groupType,SWT.CHECK);	
		btnNoMap.setText("No Map  ");
		btnNoMap.setSelection(true); 
		btnNoMap.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				CreateTableContents(baseFrameworkFrom);
			}
		});
		
		Label criticalityLabel = new Label(groupCrit, SWT.NONE);
		Label aplicabilityLabel = new Label(groupCrit, SWT.NONE);
		
		criticalityLabel.setText(CRITICALITY_SELECTION); 
	    criticalityViewer = new CheckboxTreeViewerExt(groupCrit, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		criticalityViewer.getTree().setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true,1,1));
	    criticalityViewer.expandAll();

		ComposedAdapterFactory critAdapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		criticalityViewer.setContentProvider(new AdapterFactoryContentProvider(critAdapterFactory));
		criticalityViewer.setLabelProvider(new AdapterFactoryLabelProvider(critAdapterFactory));
		
		CreateCriticalModelFromFile(criticalityViewer, resourceURI.path());
		ICheckStateListener checkCritStateListener = new ICheckStateListener(){
			public void checkStateChanged (CheckStateChangedEvent event){
				Object changedElement = event.getElement();
				boolean status = criticalityViewer.getChecked(changedElement);
				boolean isExpanded = criticalityViewer.getExpandedState(changedElement);
				criticalityViewer.expandToLevel(changedElement, AbstractTreeViewer.ALL_LEVELS);
				criticalityViewer.setSubtreeChecked(changedElement, status);
				if (!isExpanded){
					criticalityViewer.collapseToLevel(changedElement, AbstractTreeViewer.ALL_LEVELS);
				}
				if (status){
					criticalityViewer.setParentsChecked(changedElement,true);
				}
				//critState=(BaseCriticalityLevel[]) criticalityViewer.getCheckedElements();
				CreateTableContents(baseFrameworkFrom);
			}

		};
		
		
		criticalityViewer.addCheckStateListener(checkCritStateListener);
		
		aplicabilityLabel.setText(APPLICABILITY_SELECTION); 
		aplicabilityViewer = new CheckboxTreeViewerExt(groupCrit, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		aplicabilityViewer.getTree().setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,false,1,1));
		aplicabilityViewer.expandAll();

		ComposedAdapterFactory appliAdapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		aplicabilityViewer.setContentProvider(new AdapterFactoryContentProvider(appliAdapterFactory));
		aplicabilityViewer.setLabelProvider(new AdapterFactoryLabelProvider(appliAdapterFactory));
		CreateApplicabilityList (aplicabilityViewer, resourceURI.path());
		
		ICheckStateListener checkAppStateListener = new ICheckStateListener(){
			public void checkStateChanged (CheckStateChangedEvent event){
				Object changedElement = event.getElement();
				boolean status = aplicabilityViewer.getChecked(changedElement);
				boolean isExpanded = aplicabilityViewer.getExpandedState(changedElement);

				aplicabilityViewer.expandToLevel(changedElement, AbstractTreeViewer.ALL_LEVELS);
				aplicabilityViewer.setSubtreeChecked(changedElement, status);
				if (!isExpanded){
					aplicabilityViewer.collapseToLevel(changedElement, AbstractTreeViewer.ALL_LEVELS);
				}
				if (status){
					aplicabilityViewer.setParentsChecked(changedElement,true);
				}
				CreateTableContents(baseFrameworkFrom);
			}

		};
		
		aplicabilityViewer.addCheckStateListener(checkAppStateListener);
		
		/*btnSearch = new Button(contents, SWT.PUSH);
		btnSearch.setText("  SEARCH  ");
		btnSearch.setVisible(false);
		btnSearch.addSelectionListener(new SelectionAdapter() {
		
			public void widgetSelected(SelectionEvent e) {	
				CreateTableContents(baseFrameworkFrom);
			}
		});*/
		
	}

	/*//For Artefacts and for Requirements only, because are the Base elements related to Artefacts
		private boolean FindBaseFrameworkMapping(BaseFramework baseFramework) {
			boolean bFind = false;

			if (sElementFilter.contentEquals("Artefact")) {
				EList<BaseArtefact> lstBaseArtefact = baseFramework.getOwnedArtefact();
				Iterator<BaseArtefact> itbaseArtefact = lstBaseArtefact.iterator();

				while (itbaseArtefact.hasNext()) {
					BaseArtefact baseArtefact = itbaseArtefact.next();

					EList<BaseEquivalenceMap> lstBaseEquivalenceMap = baseArtefact.getEquivalenceMap();
					Iterator<BaseEquivalenceMap> itbaseEquivalenceMap = lstBaseEquivalenceMap.iterator();
					while (itbaseEquivalenceMap.hasNext()) {
						BaseEquivalenceMap baseEquivalenceMap = itbaseEquivalenceMap.next();
						MapModel mapModel = (MapModel) baseEquivalenceMap.getMapGroup().eContainer();
						mapResource = CDOUtil.getCDOObject(mapModel).cdoResource();

						// Antes de a�adir comprobar que no exista ya en la lista
						if (!VerifyMapResource(mapResource.getPath())) {
							refListMap.add(mapResource.getName());
							refListDirMap.add(mapResource.getPath());
							bFind = true;
						}
					}
				}
				
				//Requirementes of the baseFramework
				EList<BaseRequirement> lstBaseRequirement = baseFramework.getOwnedRequirement();
				bFind = searchBaseRequirementsEquivMap(bFind, lstBaseRequirement);
				
				//Requirementes of the BaseActivities
				EList<BaseActivity> lstBaseActivity = baseFramework.getOwnedActivities();
				bFind = searchActvityRequirements(bFind, lstBaseActivity);		

			return bFind;
		}

		private boolean searchActvityRequirements(boolean bFind,
				EList<BaseActivity> lstBaseActivity) {
			EList<BaseRequirement> lstBaseRequirement;
			Iterator<BaseActivity> itbaseActivity = lstBaseActivity.iterator();

			while (itbaseActivity.hasNext()) {
				BaseActivity baseActivity = itbaseActivity.next();

				lstBaseRequirement = baseActivity.getOwnedRequirement();
				bFind = searchBaseRequirementsEquivMap(bFind, lstBaseRequirement);
				
				if(baseActivity.getSubActivity().size()>0){
					EList<BaseActivity> lstSubActivities = baseActivity.getSubActivity();
					bFind = searchActvityRequirements(bFind, lstSubActivities);
				}
			
			}
			return bFind;
		}*/
	private void CreateModelFromFiletocombo(Combo combo,MapModel mapModel) {
					
		if (mapModel != null) {	
			
			try {
												
				combo.removeAll();
				EList<MapGroup> lstMapGroup = mapModel.getMapGroupModel();

				Iterator<MapGroup> itermapGroup = lstMapGroup.iterator();
//				combo.add("All");
				while (itermapGroup.hasNext()) {
					combo.add(itermapGroup.next().getName());
				}
				combo.select(0);
			}
			catch (Exception ex) {
				MessageDialog.openError(getShell(), "Not valid model file", "The provided model file couldn't be parsed as an EMF resource");
			}
		}
	}
	
	protected void createComplianceTable(final Composite parent, ComposedAdapterFactory adapterFactory) {
		
		Composite Tablecomposite = new Composite (parent , SWT.NONE);
		{
			GridData data = new GridData(SWT.NONE, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			data.grabExcessVerticalSpace=true;
			Tablecomposite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			data.horizontalAlignment = SWT.FILL;
			data.verticalAlignment= SWT.FILL;
			layout.marginHeight = 10;
			layout.marginWidth = 10;
			layout.numColumns = 3;
			Tablecomposite.setLayout(layout);
		}
		
		
		tableHistory = new Table(Tablecomposite, SWT.VIRTUAL| SWT.BORDER | SWT.MULTI | SWT.FULL_SELECTION);
				
		tableHistory.setLinesVisible (true);
		tableHistory.setHeaderVisible (true);
		
		
				
		GridData tableData = new GridData(GridData.FILL_BOTH);		
		tableHistory.setLayoutData(tableData);
		tableHistory.setItemCount (10);		
		tableData.horizontalSpan=2;
		
		TableColumn col1 = new TableColumn(tableHistory, SWT.NONE);
		col1.setWidth(20);
		col1.setResizable(false);
		col1.setText(""); //$NON-NLS-1$

		TableColumn col2 = new TableColumn(tableHistory, SWT.NONE);
		col2.setWidth(400);
		col2.setText("Base Element"); //$NON-NLS-1$
	
		
		TableColumn col3 = new TableColumn(tableHistory, SWT.NONE);
		col3.setWidth(100);
		col3.setText("Status");
		
		/*TableColumn col4 = new TableColumn(tableHistory, SWT.FILL);	
		col4.setText("Target");
		col4.setWidth(570);*/
		
		
		
		/*Listener paintListener = new Listener() {
			public void handleEvent(Event event) {
				switch (event.type) {
				case SWT.MeasureItem: {
					TableItem item = (TableItem) event.item;
					String text = getText(item, event.index);
					Point size = event.gc.textExtent(text);
					event.width = size.x;
					event.height =size.y;// Math.max(event.height, size.y);
					break;
				}
				case SWT.PaintItem: {
					TableItem item = (TableItem) event.item;
					if (event.index!=0){
						String text = getText(item, event.index);
						Point size = event.gc.textExtent(text);
						int offset2 = event.index == 0 ? Math.max(0, (event.height - size.y) / 2) : 0;
						event.gc.drawText(text, event.x, event.y + offset2, true);	    			
					}
					else{
						Image img = item.getImage();
						Point size = event.gc.textExtent("Img");
						int offset2 = event.index == 0 ? Math.max(0, (event.height - size.y) / 2) : 0;
						event.gc.drawImage(img, event.x, event.y + offset2);
					}
					break;
				}
				case SWT.EraseItem: {
					event.detail &= ~SWT.FOREGROUND;
					break;
				}
				}
			}

			String getText(TableItem item, int column) {
				String text = item.getText(column);
				if (column == 0) text="Img";				
				return text;
			}
		};
		 */
		
		Composite targetcomposite = new Composite (Tablecomposite , SWT.NONE);
		{
			GridLayout layout = new GridLayout();
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 1;			
			targetcomposite.setLayout(layout);
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			data.grabExcessVerticalSpace=true;
			data.horizontalAlignment = SWT.FILL;
			data.verticalAlignment= SWT.FILL;
			targetcomposite.setLayoutData(data);
		}
		
		Label lblJustify = new Label(targetcomposite, SWT.NONE);
		lblJustify.setText("Justification: ");

		txtJustify= new Text(targetcomposite, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		txtJustify.setText("");
		txtJustify.setEditable(false);
		GridData gridData =  new GridData( GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_FILL);
		//gridData.horizontalSpan = 3;
		gridData.grabExcessVerticalSpace = true;

		txtJustify.setLayoutData(gridData);
		
		
		Label filterMapType = new Label(targetcomposite, SWT.NONE);
		filterMapType.setText("Baseline Element Target List: ");
		
		targetsList = new  ListViewer(targetcomposite, SWT.FILL | SWT.SINGLE | SWT.BORDER | SWT.V_SCROLL| SWT.H_SCROLL);	
		targetsList.getList().setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
		targetsList.setContentProvider(new IStructuredContentProvider() {
		      public Object[] getElements(Object inputElement) {
		    	  EList<AssuranceAsset> targets= (EList<AssuranceAsset>)inputElement;
		          return targets.toArray();
		        }
		        		        
		      });

		targetsList.setLabelProvider(new LabelProvider() {		      			

			public String getText(Object element) {
				AssuranceAsset oneTG = (AssuranceAsset)element;	
				Object eFeature= oneTG.eGet(oneTG.eClass().getEStructuralFeature("name"));
				String name="NoNamed";
				if(eFeature!=null) name= eFeature.toString();

				return "{" + oneTG.eClass().getName() + "} " + name;
			}
		});
		
		BasicCommandStack commandStackET = new BasicCommandStack();
		EditingDomain editingDomainEvidences = new AdapterFactoryEditingDomain(adapterFactory, commandStackET, new HashMap<Resource, Boolean>());
		
		targetsList.addDoubleClickListener(new OpenWizardOnDoubleClick(editingDomainEvidences, adapterFactory));
		
		/*
		targetsList.addMouseListener(new MouseListener()
		{	
			private boolean doubleClick;
			private MouseEvent event;
			public void mouseDoubleClick(MouseEvent e)
			{
				doubleClick = true;
				
				Point p = new Point(e.x, e.y);

				TableItem column = tableHistory.getItem(p);
				if(column!= null){
					Object []dataList= (Object[]) column.getData();
					
					if(dataList.length>1){
					
						callComplianceMapEditor((BaseAssurableElement)dataList[0], (BaseComplianceMap)dataList[1],(String)dataList[2],(String)dataList[3]);
					}
					else{
						callComplianceMapEditor((BaseAssurableElement)dataList[0], null,null,null);
					}
				}
				else{
					MessageDialog.openInformation(getShell(), "Problem with the selected elment", "Not data found");
				}
				//int pp = 0;
			}

			public void mouseClick(MouseEvent e)
			{
				
				
			}
			@Override
			public void mouseDown(MouseEvent e) {
				 doubleClick = false;
				 event=e;
           
            Display.getDefault().timerExec(Display.getDefault().getDoubleClickTime(),
                    new Runnable() {
                        public void run() {
                            if (!doubleClick) {
                            	mouseClick(event);
                            }
                        }
                    });
			}

			@Override
			public void mouseUp(MouseEvent e) {
				// TODO Auto-generated method stub

			}

		});		
									
	}*/
		
		//refList.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
		/*targetsList.add("PAtata");
		targetsList.add("Tomate");*/
				
		
		//Listener paintListener = new Listener() {
		/*tableHistory.addListener(SWT.MeasureItem, paintListener);
		tableHistory.addListener(SWT.PaintItem, paintListener);
		tableHistory.addListener(SWT.EraseItem, paintListener);*/

		
		tableHistory.addMouseListener(new MouseListener()
		{	
			private boolean doubleClick;
			private MouseEvent event;
			public void mouseDoubleClick(MouseEvent e)
			{
				if(writePermission){
					doubleClick = true;
					
					Point p = new Point(e.x, e.y);
	
					TableItem column = tableHistory.getItem(p);
					if(column!= null){
						Object []dataList= (Object[]) column.getData();
						
						if(dataList.length>1){
						
							callComplianceMapEditor((BaseAssurableElement)dataList[0], (BaseComplianceMap)dataList[1],(String)dataList[2],(String)dataList[3]);
						}
						else{
							callComplianceMapEditor((BaseAssurableElement)dataList[0], null,null,null);
						}
					}
					else{
						MessageDialog.openInformation(getShell(), "Problem with the selected element", "Not data found");
					}
					//int pp = 0;
				}
			}

			public void mouseClick(MouseEvent e)
			{
				
				Point p = new Point(e.x, e.y);

				TableItem column = tableHistory.getItem(p);
				if(column!= null){
					Object []dataList= (Object[]) column.getData();
					
					if(dataList.length>1){
						targetsList.getList().removeAll();
						
						targetsList.setInput(dataList[4]);
						
						MapJustification mj= (MapJustification) dataList[5];
						if(mj!=null){
							txtJustify.setText(mj.getExplanation());
						}
						
						/*
						EList<AssuranceAsset> targets= (EList<AssuranceAsset>);
						Iterator<AssuranceAsset> itTargets = targets.iterator();						
																		
						while (itTargets.hasNext()) {
							AssuranceAsset oneTG = itTargets.next();	
							Object eFeature= oneTG.eGet(oneTG.eClass().getEStructuralFeature("name"));
							String name="NoNamed";
							if(eFeature!=null) name= eFeature.toString();
							
							
							targetsList.add("{" + oneTG.eClass().getName() + "} " + name);
							
							
							//text=text + fichero + "->" + containers + "{" + (oneTG.eClass().getName() + "} " + name +"\n");

						}*/
						//tableItem.setText(3,text);		
						//MessageDialog.openInformation(getShell(), "Title", "Rellenar el target List");
					}					
				}
				else{
					MessageDialog.openInformation(getShell(), "Problem with the selected element", "Not data found");
				}
				//int pp = 0;
			}
			@Override
			public void mouseDown(MouseEvent e) {
				 doubleClick = false;
				 event=e;
           
            Display.getDefault().timerExec(Display.getDefault().getDoubleClickTime(),
                    new Runnable() {
                        public void run() {
                            if (!doubleClick) {
                            	mouseClick(event);
                            }
                        }
                    });
			}

			@Override
			public void mouseUp(MouseEvent e) {
				// TODO Auto-generated method stub

			}

		});		
									
	}
	
	
	protected void callComplianceMapEditor(BaseAssurableElement tid, BaseComplianceMap dataList,String mapName, String targetModel) {
		// TODO Auto-generated method stub
		MappingSet complianceEditor = new MappingSet(this.getShell(),resourceURI);
		String type= tid.toString();
		type=(type.split("@"))[0];
		type= type.replace("Base", "");
		
		
		complianceEditor.create();
		
		complianceEditor.selectControlValues(type,tid,dataList,mapName,targetModel);
		complianceEditor.open();
		
		//Make a new the search to show possible change made in the Compliance Map Window 		
		//btnSearch.notifyListeners(SWT.Selection, new Event());
		CreateTableContents(baseFrameworkFrom);
		
	}

	private void CreateTableContents(BaseFramework baseFramework) {
		Map<BaseAssurableElement, Boolean> targets= getTargets(baseFramework);	
		try {
			// TODO Auto-generated method stub
			tableHistory.removeAll();
			targetsList.getList().removeAll();
			txtJustify.setText("");
			
			//getShell().pack(); No funciona
			if (cbFilterElementMap.getSelectionIndex() != -1 ) {
				sElementFilter = cbFilterElementMap.getItem(cbFilterElementMap.getSelectionIndex());
				//CreateTableContents(baseFrameworkFrom);
			}
			
			
			Display.getDefault().syncExec(new Runnable() {
			    public void run() {
			    	green = Display.getDefault().getSystemColor(SWT.COLOR_GREEN);
					orange = new Color(green.getDevice(),255,140,0);
					red = Display.getDefault().getSystemColor(SWT.COLOR_RED);
			    }
			});

			
			
			if(sElementFilter.contentEquals("All") || sElementFilter.contentEquals("Activity")){
				
				EList<BaseActivity> lst = baseFramework.getOwnedActivities();	
				Iterator<BaseActivity> it = lst.iterator();
				Bundle bundle = FrameworkUtil.getBundle(ComplianceTable.class);
				URL url = FileLocator.find(bundle, new Path("icons/full/obj16/BaseActivity.gif"), null);
			    ImageDescriptor imageAct = ImageDescriptor.createFromURL(url);
			    	        
				while (it.hasNext()) {
					BaseActivity baseActivity = it.next();
					if(baseActivity.isIsSelected()){
						boolean targetExist=targets.containsKey(baseActivity);
						if(targetExist){
							boolean b=targets.get(baseActivity);
							if(b)
								addComplianceToTable(imageAct, baseActivity);
						}else{
							addComplianceToTable(imageAct, baseActivity); //Not sure about this. 
						}
					}
					EList<BaseActivity> lstSubActivity = baseActivity.getSubActivity();
					searchSubActivities(lstSubActivity,imageAct);	
				}
			}
			
			if(sElementFilter.contentEquals("All") || sElementFilter.contentEquals("Requirement")){
				
				EList<BaseRequirement> lst = baseFramework.getOwnedRequirement();	
				Iterator<BaseRequirement> it = lst.iterator();
				Bundle bundle = FrameworkUtil.getBundle(ComplianceTable.class);
				URL url = FileLocator.find(bundle, new Path("icons/full/obj16/BaseRequirement.gif"), null);
			    ImageDescriptor image = ImageDescriptor.createFromURL(url);
			    	        
			    while (it.hasNext()) {
			    	BaseRequirement baseRequirement = it.next();
			    	if(baseRequirement.isIsSelected()){
						boolean targetExist=targets.containsKey(baseRequirement);
						if(targetExist){
							boolean b=targets.get(baseRequirement);
							if(b)
								addComplianceToTable(image, baseRequirement);
						}else{
							addComplianceToTable(image, baseRequirement); //Not sure about this. 
						}
					}
			    }
			    
			    EList<BaseActivity> lstBaseActivity = baseFramework.getOwnedActivities();
				searchActivityRequirements(lstBaseActivity,image,targets);	
			}

			if(sElementFilter.contentEquals("All") || sElementFilter.contentEquals("Artefact")){
				
				EList<BaseArtefact> lst = baseFramework.getOwnedArtefact();
				Iterator<BaseArtefact> it = lst.iterator();
				Bundle bundle = FrameworkUtil.getBundle(ComplianceTable.class);
				URL url = FileLocator.find(bundle, new Path("icons/full/obj16/BaseArtefact.gif"), null);
			    ImageDescriptor image = ImageDescriptor.createFromURL(url);
			    	        
				while (it.hasNext()) {
					BaseArtefact baseArtefact = it.next();				
					if(baseArtefact.isIsSelected()){
						boolean targetExist=targets.containsKey(baseArtefact);
						if(targetExist){
							boolean b=targets.get(baseArtefact);// .containsValue(baseArtefact);
							if(b)
								addComplianceToTable(image, baseArtefact);
						}else{
							addComplianceToTable(image, baseArtefact); //Not sure about this. 
						}
					}							
				}
			}
			
			
			
			if(sElementFilter.contentEquals("All") || sElementFilter.contentEquals("Technique")){
				
				EList<BaseTechnique> lst = baseFramework.getOwnedTechnique();
				Iterator<BaseTechnique> it = lst.iterator();
				Bundle bundle = FrameworkUtil.getBundle(ComplianceTable.class);
				URL url = FileLocator.find(bundle, new Path("icons/full/obj16/BaseTechnique.gif"), null);
			    ImageDescriptor image = ImageDescriptor.createFromURL(url);
			    	        
				while (it.hasNext()) {
					BaseTechnique baseTechnique = it.next();
					if(baseTechnique.isIsSelected()){
						boolean targetExist=targets.containsKey(baseTechnique);
						if(targetExist){
							boolean b=targets.get(baseTechnique);
							if(b)
								addComplianceToTable(image,baseTechnique);
						}else{
							addComplianceToTable(image,baseTechnique); //Not sure about this. 
						}
					}
				}
			}
			
			if(sElementFilter.contentEquals("All") || sElementFilter.contentEquals("Role")){
				
				EList<BaseRole> lst = baseFramework.getOwnedRole();
				Iterator<BaseRole> it = lst.iterator();
				Bundle bundle = FrameworkUtil.getBundle(ComplianceTable.class);
				URL url = FileLocator.find(bundle, new Path("icons/full/obj16/BaseRole.gif"), null);
			    ImageDescriptor image = ImageDescriptor.createFromURL(url);
			    	        
				while (it.hasNext()) {
					BaseRole baseRole = it.next();
					if(baseRole.isIsSelected()){
						boolean targetExist=targets.containsKey(baseRole);
						if(targetExist){
							boolean b=targets.get(baseRole);
							if(b)
								addComplianceToTable(image,baseRole);
						}else{
							addComplianceToTable(image,baseRole); //Not sure about this. 
						}						
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
				
	}

	private void addComplianceToTable(ImageDescriptor image, BaseAssurableElement element) {
		EList<BaseComplianceMap> lstBaseComplianceMap = element.getComplianceMap();
		Iterator<BaseComplianceMap> itbaseComplianceMap = lstBaseComplianceMap.iterator();
		if(itbaseComplianceMap.hasNext()){
			while (itbaseComplianceMap.hasNext()) {
												
				BaseComplianceMap bCM = itbaseComplianceMap.next();
				
				if((bCM.getType()==MapKind.FULL && btnFull.getSelection()) ||
					(bCM.getType()==MapKind.PARTIAL && btnPartial.getSelection())||
					(bCM.getType()==MapKind.NO_MAP && btnNoMap.getSelection())){
					
					if(cbMapGroup.getSelectionIndex()>0){
						String selectedMapGroup=cbMapGroup.getItem(cbMapGroup.getSelectionIndex());
						if(!selectedMapGroup.equals(bCM.getMapGroup().getName())){							
							break;
						}
					}
					
					TableItem tableItem = new TableItem(tableHistory,SWT.BORDER);	
					
					Object []dataList=new Object[6];
					
					dataList[0]=element;
					dataList[1]=bCM;
					tableItem.setData(dataList);
					tableItem.setImage(0,image.createImage());

					Object idFeature= element.eGet(element.eClass().getEStructuralFeature("id"));			
					String id="";
					if(idFeature!=null) id= idFeature.toString();

					String containers= getAllContainers(element);
					
					tableItem.setText(1, element.eGet(element.eClass().getEStructuralFeature("name")).toString());


					//Obtain the MapGroup and the mapping file
					//String compliancetext =  bCM.getMapGroup().eResource().getURI().path() + " ->" +  bCM.getMapGroup().getName() + "->" + bCM.getName(); 

					dataList[2]=bCM.getName();
					
					dataList[5]= bCM.getMapJustification();
					
					//tableItem.setText(2,compliancetext + " {" + bCM.getType().getName() + "}");
					
					tableItem.setText(2, bCM.getType().getName());
					
					if(bCM.getType()==MapKind.FULL ){								
						tableItem.setBackground(2,green);			
					}
					else if(bCM.getType()==MapKind.PARTIAL){
						tableItem.setBackground(2,orange);
					}
					else if(bCM.getType()==MapKind.NO_MAP){
						tableItem.setBackground(2,red);
					}

					EList<AssuranceAsset> targets= bCM.getTarget();
					Iterator<AssuranceAsset> itTargets = targets.iterator();
					String text="";
					
					
					
					if (itTargets.hasNext()) {
						AssuranceAsset oneTG = itTargets.next();	
						//Object eFeature= oneTG.eGet(oneTG.eClass().getEStructuralFeature("name"));
						//String name="NoNamed";
						//if(eFeature!=null) name= eFeature.toString();

						//containers= getAllContainers(oneTG);								
						String fichero = oneTG.eResource().getURI().path();
						dataList[3]=fichero;
						dataList[4]= targets;
						//text=text + fichero + "->" + containers + "{" + (oneTG.eClass().getName() + "} " + name +"\n");

					}
					//tableItem.setText(3,text);					
				}
								
			}
		}
		else if(btnGap.getSelection()){
			TableItem tableItem = new TableItem(tableHistory,SWT.BORDER);	
			
			tableItem.setImage(0,image.createImage());

			Object idFeature= element.eGet(element.eClass().getEStructuralFeature("id"));			
			String id="";
			if(idFeature!=null) id= idFeature.toString();

			String containers= getAllContainers(element);
			
			tableItem.setText(1, element.eGet(element.eClass().getEStructuralFeature("name")).toString());

			Object []dataList=new Object[1];
			
			dataList[0]=element;			
			
			tableItem.setData(dataList);

		}
	}


	private String getAllContainers(EObject oneTG) {
		EObject padre = oneTG.eContainer();
		String retorno = "";
		while(padre!=null){
			EStructuralFeature nameEF = padre.eClass().getEStructuralFeature("name");
			if(nameEF!=null){
				Object eFeature= padre.eGet(nameEF);			
				String name="NoNamed";
				if(eFeature!=null) name= eFeature.toString();
				
				retorno= name + "->" + retorno;
			}
			
			padre=padre.eContainer();
		}

		// TODO Auto-generated method stub

		return retorno;
	}

	private void searchSubActivities(EList<BaseActivity> lstBaseActivity,ImageDescriptor imageAct) {		
		Iterator<BaseActivity> itbaseActivity = lstBaseActivity.iterator();
		while (itbaseActivity.hasNext()) {
			BaseActivity baseActivity = itbaseActivity.next();

			if(baseActivity.isIsSelected()){
				addComplianceToTable(imageAct, baseActivity);

			}
			EList<BaseActivity> lstSubActivity = baseActivity.getSubActivity();
			searchSubActivities(lstSubActivity,  imageAct);		
		}		
	}

	private void searchSubRequirements(EList<BaseRequirement> lstBaseRequirement,ImageDescriptor imageReq,Map<BaseAssurableElement, Boolean> targets) {		
		
		Iterator<BaseRequirement> itBaseRequirement = lstBaseRequirement.iterator();
		while (itBaseRequirement.hasNext()) {
			BaseRequirement baseRequirement= itBaseRequirement.next();
			if(baseRequirement.isIsSelected()){
				//addComplianceToTable(imageReq, baseReq);					
				boolean targetExist=targets.containsKey(baseRequirement);
				if(targetExist){
					boolean b=targets.get(baseRequirement);
					if(b)
						addComplianceToTable(imageReq, baseRequirement);
				}else{
					addComplianceToTable(imageReq, baseRequirement); //Not sure about this. 
				}
			}
			searchSubRequirements(baseRequirement.getSubRequirement(),imageReq,targets);
		}		
	}
	
	private void searchActivityRequirements(EList<BaseActivity> lstBaseActivity, ImageDescriptor imageReq,Map<BaseAssurableElement, Boolean> targets){
		Iterator<BaseActivity> itbaseActivity = lstBaseActivity.iterator();
		while (itbaseActivity.hasNext()) {
			BaseActivity baseActivity = itbaseActivity.next();

			EList<BaseRequirement> lstBaseRequirement = baseActivity.getOwnedRequirement();
			Iterator<BaseRequirement> itBaseRequirement = lstBaseRequirement.iterator();
			while (itBaseRequirement.hasNext()) {
				//addComplianceToTable(imageReq, baseRequirement);
				BaseRequirement baseRequirement= itBaseRequirement.next();				
				if(baseRequirement.isIsSelected()){
					boolean targetExist=targets.containsKey(baseRequirement);
					if(targetExist){
						boolean b=targets.get(baseRequirement);
						if(b)
							addComplianceToTable(imageReq, baseRequirement);
					}else{
						addComplianceToTable(imageReq, baseRequirement); //Not sure about this. 
					}
				}
				searchSubRequirements(baseRequirement.getSubRequirement(),imageReq,targets);
				
			}		
			EList<BaseActivity> lstSubActivity = baseActivity.getSubActivity();
			searchActivityRequirements(lstSubActivity,imageReq,targets);
		}
	}
	
	/*private void readHistory() {
		//Begin ALC		
		if(location!= null && !useLocalRepository && validateInstance!=null){
			if(location.getText()!=null && !location.getText().equals("")){
				tableHistory.removeAll();
				ArrayList<String> entriesList=validateInstance.getFileHistory(location.getText());
				for (int i = 0; i < entriesList.size(); i++) {
					String allData = entriesList.get(i);
					TableItem tableItem = new TableItem(tableHistory,i);
					String []parts=allData.split("#");
					for (int j = 0; j < parts.length; j++) {
						tableItem.setText(j,parts[j]);
					}							
				}
			}	
			else{
				tableHistory.removeAll();
				tableHistory.setItemCount (10);
			}
		}
		//End ALC
	}*/
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Compliance Map table");
	}


	@Override
	protected void okPressed() {
		transaction.close();
		super.okPressed();
	}

	@Override
	public boolean close() {
		transaction.close();
		return super.close();
	}

	@Override
	protected Button createButton(Composite parent, int id,
	        String label, boolean defaultButton) {
	    if (id == IDialogConstants.CANCEL_ID) return null;
	    return super.createButton(parent, id, label, defaultButton);
	}
	
	public Boolean getResult() {
		return true;
	}

	public ResourceLocator getResourceLocator() {
		return RefframeworkEditPlugin.INSTANCE;
	}
	public void CreateCriticalModelFromFile(CheckboxTreeViewerExt tviewer, String model){
		if (model != null) {	
		   Resource resource = null;
			try {
				resource = viewCDO.getResource(model);
				tviewer.setInput(null);
				tviewer.setInput(resource);
				
			    final ViewerFilter modelFilter = new ViewerFilter() {
			        public boolean select(
			          Viewer viewer,
			          Object parentElement,
			          Object element) {
			        	if (element instanceof BaseFramework)
			        		return true;
			        	if (element instanceof BaseCriticalityLevel)
			        		return true;
		        		else
		        			return false;
			        }
			    };
			    tviewer.addFilter(modelFilter);
			    tviewer.expandAll();
			    tviewer.setAllChecked(true);
			}
			catch (Exception ex) {
				MessageDialog.openError(getShell(), "Not valid model file", "The provided model file couldn't be parsed as an EMF resource");
				tviewer.setInput(null);
			}
		}
	}
	private void CreateApplicabilityList( CheckboxTreeViewerExt viewer, String model) {
		if (model != null) {	
			   Resource resource = null;
			   try {
					resource = viewCDO.getResource(model);
					viewer.setInput(null);
					viewer.setInput(resource);
					
				    final ViewerFilter modelFilter = new ViewerFilter() {
				        public boolean select(
				          Viewer viewer,
				          Object parentElement,
				          Object element) {
				        	if (element instanceof BaseFramework)
				        		return true;
				        	if (element instanceof BaseApplicabilityLevel)
				        		return true;
			        		else
			        			return false;
				        }
				    };
				    viewer.addFilter(modelFilter);
				    viewer.expandAll();
				    viewer.setAllChecked(true);
				   // viewCDO.getSession().close();
			   }catch (Exception ex) {
					MessageDialog.openError(getShell(), "Not valid model file", "The provided model file couldn't be parsed as an EMF resource");
					viewer.setInput(null);
				}
		}
	}
	private Map<BaseAssurableElement, Boolean> getTargets(BaseFramework baseFramework){
		Map<BaseAssurableElement, Boolean> targets=new HashMap();
		EList<BaseActivity> activities = ((BaseFramework) baseFramework).getOwnedActivities();
		targets=getActivityAppli(activities, targets);
		return targets;
	}
	public Map<BaseAssurableElement, Boolean> getActivityAppli(EList<BaseActivity> activities, Map<BaseAssurableElement, Boolean> targets){
		Iterator<BaseActivity> actIterator = activities.iterator();
		while (actIterator.hasNext()){
			BaseActivity act=actIterator.next();
			EList<BaseActivity> subActivities = act.getSubActivity();
			if(subActivities.size()>0){
				targets=getActivityAppli(subActivities,targets);
			}
			EList<BaseRequirement> requirements = act.getOwnedRequirement();
			EList<BaseApplicability> applicabilities = act.getApplicability();
			targets=getCriticalityTarget(applicabilities, targets);
			targets=getRequirementsAppli(requirements,targets);
			
		}
		return targets;
	}
	
	public Map<BaseAssurableElement, Boolean> getRequirementsAppli(EList<BaseRequirement> requirements, Map<BaseAssurableElement, Boolean> targets){
		Iterator<BaseRequirement> reqIterator = requirements.iterator();
		while(reqIterator.hasNext()){
			BaseRequirement req = reqIterator.next();
			EList<BaseRequirement> subReq = req.getSubRequirement();
			if(subReq.size()>0){
				targets=getRequirementsAppli(subReq, targets);
			}
			EList<BaseApplicability> reqApplicabilities = req.getApplicability();
			targets=getCriticalityTarget(reqApplicabilities,targets);
		}
		return targets;
	
	}
	public Map<BaseAssurableElement, Boolean> getCriticalityTarget(EList<BaseApplicability> applicabilities,Map<BaseAssurableElement, Boolean> targets){	
		Iterator<BaseApplicability> applIterator = applicabilities.iterator();		
		while(applIterator.hasNext()){
			BaseApplicability appli = applIterator.next();
			BaseAssurableElement target = appli.getApplicTarget();
			if (target!=null) {	
				Boolean b=false;
				EList<BaseCriticalityApplicability> critAppli = appli.getApplicCritic();
				Iterator<BaseCriticalityApplicability> critAppliIterator = critAppli.iterator();
				while(critAppliIterator.hasNext()){
					BaseCriticalityApplicability critApp = critAppliIterator.next();
					BaseCriticalityLevel criticalityLevel = critApp.getCriticLevel();
					BaseApplicabilityLevel applicabilityLevel = critApp.getApplicLevel();
					
					
					
					Object[] selectedCrit =  criticalityViewer.getCheckedElements();
					
					boolean statusCrit = false;
					
					for(Object oneCrit:selectedCrit){
						if(oneCrit.toString().equals(criticalityLevel.toString())){
							statusCrit=true;
							break;
						}
					}
					
					
					Object[] selectedApli =  aplicabilityViewer.getCheckedElements();
					
					boolean statusApp = false;
					
					for(Object oneApli:selectedApli){
						if(oneApli.toString().equals(applicabilityLevel.toString())){
							statusApp=true;
							break;
						}
					}
					
					//boolean statusCrit = Arrays.asList(criticalityViewer.getCheckedElements()).contains(criticalityLevel);
					//boolean statusApp =  aplicabilityViewer.getChecked(applicabilityLevel);
					
					
					if(statusCrit && statusApp ){
						b=true;						
					}
					targets.put(target,b);
				}	
			}
			
	    }
		return targets;
	}
	public boolean checkCritApply(BaseAssurableElement target, EList<BaseApplicability> applicabilities){
		boolean b=false;
		Iterator<BaseApplicability> applIterator = applicabilities.iterator();		
		while(applIterator.hasNext()){
			BaseApplicability appli = applIterator.next();
			if (target!=null) {			
				EList<BaseCriticalityApplicability> critAppli = appli.getApplicCritic();
				Iterator<BaseCriticalityApplicability> critAppliIterator = critAppli.iterator();
				while(critAppliIterator.hasNext()){
					BaseCriticalityApplicability critApp = critAppliIterator.next();
					BaseCriticalityLevel criticalityLevel = critApp.getCriticLevel();
					BaseApplicabilityLevel applicabilityLevel = critApp.getApplicLevel();
					boolean statusCrit = criticalityViewer.getChecked(criticalityLevel);
					boolean statusApp =  aplicabilityViewer.getChecked(applicabilityLevel);
					if(statusCrit && statusApp ){
						b=true;
					}
				}	
			}

	    }
		return b;

	}

		

}
