/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.components;

// Start of user code for imports

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.resource.ResourceSet;

import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;

import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;

import org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent;

import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;

import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;

import org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactComplianceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactEquivalenceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseArtefactSelectionPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;

// End of user code

/**
 * 
 * 
 */
public class BaseArtefactPropertiesEditionComponent extends ComposedPropertiesEditionComponent {

	/**
	 * The Base part
	 * 
	 */
	private BaseArtefactPropertiesEditionPart basePart;

	/**
	 * The BaseArtefactBasePropertiesEditionComponent sub component
	 * 
	 */
	protected BaseArtefactBasePropertiesEditionComponent baseArtefactBasePropertiesEditionComponent;

	/**
	 * The BaseArtefactSelection part
	 * 
	 */
	private BaseArtefactSelectionPropertiesEditionPart baseArtefactSelectionPart;

	/**
	 * The BaseArtefactBaseArtefactSelectionPropertiesEditionComponent sub component
	 * 
	 */
	protected BaseArtefactBaseArtefactSelectionPropertiesEditionComponent baseArtefactBaseArtefactSelectionPropertiesEditionComponent;

	/**
	 * The BaseArtefactEquivalenceMap part
	 * 
	 */
	private BaseArtefactEquivalenceMapPropertiesEditionPart baseArtefactEquivalenceMapPart;

	/**
	 * The BaseArtefactBaseArtefactEquivalenceMapPropertiesEditionComponent sub component
	 * 
	 */
	protected BaseArtefactBaseArtefactEquivalenceMapPropertiesEditionComponent baseArtefactBaseArtefactEquivalenceMapPropertiesEditionComponent;

	/**
	 * The BaseArtefactComplianceMap part
	 * 
	 */
	private BaseArtefactComplianceMapPropertiesEditionPart baseArtefactComplianceMapPart;

	/**
	 * The BaseArtefactBaseArtefactComplianceMapPropertiesEditionComponent sub component
	 * 
	 */
	protected BaseArtefactBaseArtefactComplianceMapPropertiesEditionComponent baseArtefactBaseArtefactComplianceMapPropertiesEditionComponent;

	/**
	 * Parameterized constructor
	 * 
	 * @param baseArtefact the EObject to edit
	 * 
	 */
	public BaseArtefactPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject baseArtefact, String editing_mode) {
		super(editingContext, editing_mode);
		if (baseArtefact instanceof BaseArtefact) {
			PropertiesEditingProvider provider = null;
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseArtefact, PropertiesEditingProvider.class);
			baseArtefactBasePropertiesEditionComponent = (BaseArtefactBasePropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseArtefactBasePropertiesEditionComponent.BASE_PART, BaseArtefactBasePropertiesEditionComponent.class);
			addSubComponent(baseArtefactBasePropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseArtefact, PropertiesEditingProvider.class);
			baseArtefactBaseArtefactSelectionPropertiesEditionComponent = (BaseArtefactBaseArtefactSelectionPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseArtefactBaseArtefactSelectionPropertiesEditionComponent.BASEARTEFACTSELECTION_PART, BaseArtefactBaseArtefactSelectionPropertiesEditionComponent.class);
			addSubComponent(baseArtefactBaseArtefactSelectionPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseArtefact, PropertiesEditingProvider.class);
			baseArtefactBaseArtefactEquivalenceMapPropertiesEditionComponent = (BaseArtefactBaseArtefactEquivalenceMapPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseArtefactBaseArtefactEquivalenceMapPropertiesEditionComponent.BASEARTEFACTEQUIVALENCEMAP_PART, BaseArtefactBaseArtefactEquivalenceMapPropertiesEditionComponent.class);
			addSubComponent(baseArtefactBaseArtefactEquivalenceMapPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(baseArtefact, PropertiesEditingProvider.class);
			baseArtefactBaseArtefactComplianceMapPropertiesEditionComponent = (BaseArtefactBaseArtefactComplianceMapPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, BaseArtefactBaseArtefactComplianceMapPropertiesEditionComponent.BASEARTEFACTCOMPLIANCEMAP_PART, BaseArtefactBaseArtefactComplianceMapPropertiesEditionComponent.class);
			addSubComponent(baseArtefactBaseArtefactComplianceMapPropertiesEditionComponent);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      getPropertiesEditionPart(int, java.lang.String)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(int kind, String key) {
		if (BaseArtefactBasePropertiesEditionComponent.BASE_PART.equals(key)) {
			basePart = (BaseArtefactPropertiesEditionPart)baseArtefactBasePropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)basePart;
		}
		if (BaseArtefactBaseArtefactSelectionPropertiesEditionComponent.BASEARTEFACTSELECTION_PART.equals(key)) {
			baseArtefactSelectionPart = (BaseArtefactSelectionPropertiesEditionPart)baseArtefactBaseArtefactSelectionPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)baseArtefactSelectionPart;
		}
		if (BaseArtefactBaseArtefactEquivalenceMapPropertiesEditionComponent.BASEARTEFACTEQUIVALENCEMAP_PART.equals(key)) {
			baseArtefactEquivalenceMapPart = (BaseArtefactEquivalenceMapPropertiesEditionPart)baseArtefactBaseArtefactEquivalenceMapPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)baseArtefactEquivalenceMapPart;
		}
		if (BaseArtefactBaseArtefactComplianceMapPropertiesEditionComponent.BASEARTEFACTCOMPLIANCEMAP_PART.equals(key)) {
			baseArtefactComplianceMapPart = (BaseArtefactComplianceMapPropertiesEditionPart)baseArtefactBaseArtefactComplianceMapPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)baseArtefactComplianceMapPart;
		}
		return super.getPropertiesEditionPart(kind, key);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      setPropertiesEditionPart(java.lang.Object, int,
	 *      org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart)
	 * 
	 */
	public void setPropertiesEditionPart(java.lang.Object key, int kind, IPropertiesEditionPart propertiesEditionPart) {
		if (BaselineViewsRepository.BaseArtefact.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			basePart = (BaseArtefactPropertiesEditionPart)propertiesEditionPart;
		}
		if (BaselineViewsRepository.BaseArtefactSelection.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			baseArtefactSelectionPart = (BaseArtefactSelectionPropertiesEditionPart)propertiesEditionPart;
		}
		if (BaselineViewsRepository.BaseArtefactEquivalenceMap.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			baseArtefactEquivalenceMapPart = (BaseArtefactEquivalenceMapPropertiesEditionPart)propertiesEditionPart;
		}
		if (BaselineViewsRepository.BaseArtefactComplianceMap.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			baseArtefactComplianceMapPart = (BaseArtefactComplianceMapPropertiesEditionPart)propertiesEditionPart;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(java.lang.Object key, int kind, EObject element, ResourceSet allResource) {
		if (key == BaselineViewsRepository.BaseArtefact.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == BaselineViewsRepository.BaseArtefactSelection.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == BaselineViewsRepository.BaseArtefactEquivalenceMap.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == BaselineViewsRepository.BaseArtefactComplianceMap.class) {
			super.initPart(key, kind, element, allResource);
		}
	}
}
