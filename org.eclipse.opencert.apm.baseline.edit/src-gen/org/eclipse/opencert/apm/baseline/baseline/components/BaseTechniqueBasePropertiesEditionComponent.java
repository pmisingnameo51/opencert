/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.filters.EObjectFilter;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.apm.baseline.baseline.BaseCriticalityApplicability;
import org.eclipse.opencert.apm.baseline.baseline.BaseTechnique;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseTechniquePropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;


// End of user code

/**
 * 
 * 
 */
public class BaseTechniqueBasePropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for criticApplic ReferencesTable
	 */
	private ReferencesTableSettings criticApplicSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public BaseTechniqueBasePropertiesEditionComponent(PropertiesEditingContext editingContext, EObject baseTechnique, String editing_mode) {
		super(editingContext, baseTechnique, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = BaselineViewsRepository.class;
		partKey = BaselineViewsRepository.BaseTechnique.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final BaseTechnique baseTechnique = (BaseTechnique)elt;
			final BaseTechniquePropertiesEditionPart basePart = (BaseTechniquePropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(BaselineViewsRepository.BaseTechnique.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseTechnique.getId()));
			
			if (isAccessible(BaselineViewsRepository.BaseTechnique.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseTechnique.getName()));
			
			if (isAccessible(BaselineViewsRepository.BaseTechnique.Properties.description))
				basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, baseTechnique.getDescription()));
			if (isAccessible(BaselineViewsRepository.BaseTechnique.Properties.criticApplic)) {
				criticApplicSettings = new ReferencesTableSettings(baseTechnique, BaselinePackage.eINSTANCE.getBaseTechnique_CriticApplic());
				basePart.initCriticApplic(criticApplicSettings);
			}
			if (isAccessible(BaselineViewsRepository.BaseTechnique.Properties.aim))
				basePart.setAim(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseTechnique.getAim()));
			
			// init filters
			
			
			
			if (isAccessible(BaselineViewsRepository.BaseTechnique.Properties.criticApplic)) {
				basePart.addFilterToCriticApplic(new EObjectFilter(BaselinePackage.Literals.BASE_CRITICALITY_APPLICABILITY));
				// Start of user code for additional businessfilters for criticApplic
				// End of user code
			}
			
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}








	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == BaselineViewsRepository.BaseTechnique.Properties.id) {
			return GeneralPackage.eINSTANCE.getNamedElement_Id();
		}
		if (editorKey == BaselineViewsRepository.BaseTechnique.Properties.name) {
			return GeneralPackage.eINSTANCE.getNamedElement_Name();
		}
		if (editorKey == BaselineViewsRepository.BaseTechnique.Properties.description) {
			return GeneralPackage.eINSTANCE.getDescribableElement_Description();
		}
		if (editorKey == BaselineViewsRepository.BaseTechnique.Properties.criticApplic) {
			return BaselinePackage.eINSTANCE.getBaseTechnique_CriticApplic();
		}
		if (editorKey == BaselineViewsRepository.BaseTechnique.Properties.aim) {
			return BaselinePackage.eINSTANCE.getBaseTechnique_Aim();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		BaseTechnique baseTechnique = (BaseTechnique)semanticObject;
		if (BaselineViewsRepository.BaseTechnique.Properties.id == event.getAffectedEditor()) {
			baseTechnique.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseTechnique.Properties.name == event.getAffectedEditor()) {
			baseTechnique.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseTechnique.Properties.description == event.getAffectedEditor()) {
			baseTechnique.setDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseTechnique.Properties.criticApplic == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof BaseCriticalityApplicability) {
					criticApplicSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				criticApplicSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				criticApplicSettings.move(event.getNewIndex(), (BaseCriticalityApplicability) event.getNewValue());
			}
		}
		if (BaselineViewsRepository.BaseTechnique.Properties.aim == event.getAffectedEditor()) {
			baseTechnique.setAim((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			BaseTechniquePropertiesEditionPart basePart = (BaseTechniquePropertiesEditionPart)editingPart;
			if (GeneralPackage.eINSTANCE.getNamedElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseTechnique.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (GeneralPackage.eINSTANCE.getNamedElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseTechnique.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (GeneralPackage.eINSTANCE.getDescribableElement_Description().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseTechnique.Properties.description)){
				if (msg.getNewValue() != null) {
					basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setDescription("");
				}
			}
			if (BaselinePackage.eINSTANCE.getBaseTechnique_CriticApplic().equals(msg.getFeature())  && isAccessible(BaselineViewsRepository.BaseTechnique.Properties.criticApplic))
				basePart.updateCriticApplic();
			if (BaselinePackage.eINSTANCE.getBaseTechnique_Aim().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseTechnique.Properties.aim)) {
				if (msg.getNewValue() != null) {
					basePart.setAim(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setAim("");
				}
			}
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			GeneralPackage.eINSTANCE.getNamedElement_Id(),
			GeneralPackage.eINSTANCE.getNamedElement_Name(),
			GeneralPackage.eINSTANCE.getDescribableElement_Description(),
			BaselinePackage.eINSTANCE.getBaseTechnique_CriticApplic(),
			BaselinePackage.eINSTANCE.getBaseTechnique_Aim()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (BaselineViewsRepository.BaseTechnique.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseTechnique.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseTechnique.Properties.description == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseTechnique.Properties.aim == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(BaselinePackage.eINSTANCE.getBaseTechnique_Aim().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(BaselinePackage.eINSTANCE.getBaseTechnique_Aim().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
