/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.parts;

// Start of user code for imports
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;

import org.eclipse.jface.viewers.ViewerFilter;


// End of user code

/**
 * 
 * 
 */
public interface BaseRequirementPropertiesEditionPart {

	/**
	 * @return the id
	 * 
	 */
	public String getId();

	/**
	 * Defines a new id
	 * @param newValue the new id to set
	 * 
	 */
	public void setId(String newValue);


	/**
	 * @return the name
	 * 
	 */
	public String getName();

	/**
	 * Defines a new name
	 * @param newValue the new name to set
	 * 
	 */
	public void setName(String newValue);


	/**
	 * @return the description
	 * 
	 */
	public String getDescription();

	/**
	 * Defines a new description
	 * @param newValue the new description to set
	 * 
	 */
	public void setDescription(String newValue);


	/**
	 * @return the reference
	 * 
	 */
	public String getReference();

	/**
	 * Defines a new reference
	 * @param newValue the new reference to set
	 * 
	 */
	public void setReference(String newValue);


	/**
	 * @return the assumptions
	 * 
	 */
	public String getAssumptions();

	/**
	 * Defines a new assumptions
	 * @param newValue the new assumptions to set
	 * 
	 */
	public void setAssumptions(String newValue);


	/**
	 * @return the rationale
	 * 
	 */
	public String getRationale();

	/**
	 * Defines a new rationale
	 * @param newValue the new rationale to set
	 * 
	 */
	public void setRationale(String newValue);


	/**
	 * @return the image
	 * 
	 */
	public String getImage();

	/**
	 * Defines a new image
	 * @param newValue the new image to set
	 * 
	 */
	public void setImage(String newValue);


	/**
	 * @return the annotations
	 * 
	 */
	public String getAnnotations();

	/**
	 * Defines a new annotations
	 * @param newValue the new annotations to set
	 * 
	 */
	public void setAnnotations(String newValue);




	/**
	 * Init the ownedRel
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initOwnedRel(ReferencesTableSettings settings);

	/**
	 * Update the ownedRel
	 * @param newValue the ownedRel to update
	 * 
	 */
	public void updateOwnedRel();

	/**
	 * Adds the given filter to the ownedRel edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToOwnedRel(ViewerFilter filter);

	/**
	 * Adds the given filter to the ownedRel edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToOwnedRel(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the ownedRel table
	 * 
	 */
	public boolean isContainedInOwnedRelTable(EObject element);




	/**
	 * Init the subRequirement
	 * @param current the current value
	 * @param containgFeature the feature where to navigate if necessary
	 * @param feature the feature to manage
	 */
	public void initSubRequirement(ReferencesTableSettings settings);

	/**
	 * Update the subRequirement
	 * @param newValue the subRequirement to update
	 * 
	 */
	public void updateSubRequirement();

	/**
	 * Adds the given filter to the subRequirement edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addFilterToSubRequirement(ViewerFilter filter);

	/**
	 * Adds the given filter to the subRequirement edition editor.
	 * 
	 * @param filter
	 *            a viewer filter
	 * @see org.eclipse.jface.viewers.StructuredViewer#addFilter(ViewerFilter)
	 * 
	 */
	public void addBusinessFilterToSubRequirement(ViewerFilter filter);

	/**
	 * @return true if the given element is contained inside the subRequirement table
	 * 
	 */
	public boolean isContainedInSubRequirementTable(EObject element);





	/**
	 * Returns the internationalized title text.
	 * 
	 * @return the internationalized title text.
	 * 
	 */
	public String getTitle();

	// Start of user code for additional methods
	
	// End of user code

}
