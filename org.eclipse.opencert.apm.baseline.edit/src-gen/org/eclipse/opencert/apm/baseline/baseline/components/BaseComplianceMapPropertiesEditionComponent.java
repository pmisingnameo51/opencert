/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.baseline.baseline.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.context.impl.EObjectPropertiesEditionContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.filters.EObjectFilter;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.impl.utils.EEFUtils;
import org.eclipse.emf.eef.runtime.policies.PropertiesEditingPolicy;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.ButtonsModeEnum;
import org.eclipse.emf.eef.runtime.ui.widgets.eobjflatcombo.EObjectFlatComboSettings;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaseComplianceMapPropertiesEditionPart;
import org.eclipse.opencert.apm.baseline.baseline.parts.BaselineViewsRepository;
import org.eclipse.opencert.infra.mappings.mapping.MapGroup;
import org.eclipse.opencert.infra.mappings.mapping.MapJustification;
import org.eclipse.opencert.infra.mappings.mapping.MapKind;
import org.eclipse.opencert.infra.mappings.mapping.MappingFactory;
import org.eclipse.opencert.infra.mappings.mapping.MappingPackage;


// End of user code

/**
 * 
 * 
 */
public class BaseComplianceMapPropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for mapGroup EObjectFlatComboViewer
	 */
	private EObjectFlatComboSettings mapGroupSettings;
	
	/**
	 * Settings for target ReferencesTable
	 */
	private ReferencesTableSettings targetSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public BaseComplianceMapPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject baseComplianceMap, String editing_mode) {
		super(editingContext, baseComplianceMap, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = BaselineViewsRepository.class;
		partKey = BaselineViewsRepository.BaseComplianceMap.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final BaseComplianceMap baseComplianceMap = (BaseComplianceMap)elt;
			final BaseComplianceMapPropertiesEditionPart basePart = (BaseComplianceMapPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(BaselineViewsRepository.BaseComplianceMap.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseComplianceMap.getId()));
			
			if (isAccessible(BaselineViewsRepository.BaseComplianceMap.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, baseComplianceMap.getName()));
			
			if (isAccessible(BaselineViewsRepository.BaseComplianceMap.Properties.mapGroup)) {
				// init part
				mapGroupSettings = new EObjectFlatComboSettings(baseComplianceMap, MappingPackage.eINSTANCE.getMap_MapGroup());
				basePart.initMapGroup(mapGroupSettings);
				// set the button mode
				basePart.setMapGroupButtonMode(ButtonsModeEnum.BROWSE);
			}
			if (isAccessible(BaselineViewsRepository.BaseComplianceMap.Properties.mapGroupCombo)) {
				basePart.initMapGroupCombo(EEFUtils.choiceOfValues(baseComplianceMap, MappingPackage.eINSTANCE.getMap_MapGroup()), baseComplianceMap.getMapGroup());
			}
			if (isAccessible(BaselineViewsRepository.BaseComplianceMap.Properties.type)) {
				basePart.initType(EEFUtils.choiceOfValues(baseComplianceMap, MappingPackage.eINSTANCE.getMap_Type()), baseComplianceMap.getType());
			}
			// FIXME NO VALID CASE INTO template public updater(editionElement : PropertiesEditionElement, view : View, pec : PropertiesEditionComponent) in widgetControl.mtl module, with the values : justification, BaseComplianceMap, BaseComplianceMap.
			// Start IRR
			if (isAccessible(BaselineViewsRepository.BaseComplianceMap.Properties.justification)) {
				MapJustification mapJustification = MappingFactory.eINSTANCE.createMapJustification();
				mapJustification = baseComplianceMap.getMapJustification();
				if (mapJustification != null)
				basePart.setJustification(mapJustification.getExplanation());
			}
			// End IRR
			if (isAccessible(BaselineViewsRepository.BaseComplianceMap.Properties.target)) {
				targetSettings = new ReferencesTableSettings(baseComplianceMap, BaselinePackage.eINSTANCE.getBaseComplianceMap_Target());
				basePart.initTarget(targetSettings);
			}
			// init filters
			
			
			if (isAccessible(BaselineViewsRepository.BaseComplianceMap.Properties.mapGroup)) {
				basePart.addFilterToMapGroup(new ViewerFilter() {
				
					/**
					 * {@inheritDoc}
					 * 
					 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
					 */
					public boolean select(Viewer viewer, Object parentElement, Object element) {
						return (element instanceof String && element.equals("")) || (element instanceof MapGroup); //$NON-NLS-1$ 
					}
					
				});
				// Start of user code for additional businessfilters for mapGroup
				// End of user code
			}
			// Start of user code for additional businessfilters for mapGroupCombo
			// End of user code
			
			
			// FIXME NO VALID CASE INTO template public filterUpdater(editionElement : PropertiesEditionElement, view : View, pec : PropertiesEditionComponent) in widgetControl.mtl module, with the values : justification, BaseComplianceMap, BaseComplianceMap.
			if (isAccessible(BaselineViewsRepository.BaseComplianceMap.Properties.target)) {
				basePart.addFilterToTarget(new EObjectFilter(AssuranceassetPackage.Literals.ASSURANCE_ASSET));
				// Start of user code for additional businessfilters for target
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}










	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == BaselineViewsRepository.BaseComplianceMap.Properties.id) {
			return GeneralPackage.eINSTANCE.getNamedElement_Id();
		}
		if (editorKey == BaselineViewsRepository.BaseComplianceMap.Properties.name) {
			return GeneralPackage.eINSTANCE.getNamedElement_Name();
		}
		if (editorKey == BaselineViewsRepository.BaseComplianceMap.Properties.mapGroup) {
			return MappingPackage.eINSTANCE.getMap_MapGroup();
		}
		if (editorKey == BaselineViewsRepository.BaseComplianceMap.Properties.mapGroupCombo) {
			return MappingPackage.eINSTANCE.getMap_MapGroup();
		}
		if (editorKey == BaselineViewsRepository.BaseComplianceMap.Properties.type) {
			return MappingPackage.eINSTANCE.getMap_Type();
		}
		if (editorKey == BaselineViewsRepository.BaseComplianceMap.Properties.justification) {
			return MappingPackage.eINSTANCE.getMap_MapJustification();
		}
		if (editorKey == BaselineViewsRepository.BaseComplianceMap.Properties.target) {
			return BaselinePackage.eINSTANCE.getBaseComplianceMap_Target();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		BaseComplianceMap baseComplianceMap = (BaseComplianceMap)semanticObject;
		if (BaselineViewsRepository.BaseComplianceMap.Properties.id == event.getAffectedEditor()) {
			baseComplianceMap.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseComplianceMap.Properties.name == event.getAffectedEditor()) {
			baseComplianceMap.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (BaselineViewsRepository.BaseComplianceMap.Properties.mapGroup == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.SET) {
				mapGroupSettings.setToReference((MapGroup)event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.ADD) {
				MapGroup eObject = MappingFactory.eINSTANCE.createMapGroup();
				EObjectPropertiesEditionContext context = new EObjectPropertiesEditionContext(editingContext, this, eObject, editingContext.getAdapterFactory());
				PropertiesEditingProvider provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(eObject, PropertiesEditingProvider.class);
				if (provider != null) {
					PropertiesEditingPolicy policy = provider.getPolicy(context);
					if (policy != null) {
						policy.execute();
					}
				}
				mapGroupSettings.setToReference(eObject);
			}
		}
		if (BaselineViewsRepository.BaseComplianceMap.Properties.mapGroupCombo == event.getAffectedEditor()) {
			baseComplianceMap.setMapGroup(!"".equals(event.getNewValue()) ? (MapGroup) event.getNewValue() : null);
		}
		if (BaselineViewsRepository.BaseComplianceMap.Properties.type == event.getAffectedEditor()) {
			baseComplianceMap.setType((MapKind)event.getNewValue());
		}
		
		
		
		if (BaselineViewsRepository.BaseComplianceMap.Properties.justification == event.getAffectedEditor()) {
			// FIXME INVALID CASE you must override the template 'declareEObjectUpdater' for the case : justification, BaseComplianceMap, BaseComplianceMap.
			// Start IRR
			MapJustification mapJustification = MappingFactory.eINSTANCE.createMapJustification();
			mapJustification.setExplanation((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
			baseComplianceMap.setMapJustification(mapJustification);
			// End IRR
		}
		
		if (BaselineViewsRepository.BaseComplianceMap.Properties.target == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof AssuranceAsset) {
					targetSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				targetSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				targetSettings.move(event.getNewIndex(), (AssuranceAsset) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			BaseComplianceMapPropertiesEditionPart basePart = (BaseComplianceMapPropertiesEditionPart)editingPart;
			if (GeneralPackage.eINSTANCE.getNamedElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseComplianceMap.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (GeneralPackage.eINSTANCE.getNamedElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseComplianceMap.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (MappingPackage.eINSTANCE.getMap_MapGroup().equals(msg.getFeature()) && basePart != null && isAccessible(BaselineViewsRepository.BaseComplianceMap.Properties.mapGroup))
				basePart.setMapGroup((EObject)msg.getNewValue());
			if (MappingPackage.eINSTANCE.getMap_MapGroup().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(BaselineViewsRepository.BaseComplianceMap.Properties.mapGroupCombo))
				basePart.setMapGroupCombo((Object)msg.getNewValue());
			if (MappingPackage.eINSTANCE.getMap_Type().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && isAccessible(BaselineViewsRepository.BaseComplianceMap.Properties.type))
				basePart.setType((MapKind)msg.getNewValue());
			
			// FIXME INVALID CASE INTO template public liveUpdater(editionElement : PropertiesEditionElement, view : View, pec : PropertiesEditionComponent) in widgetControl.mtl module, with the values : justification, BaseComplianceMap, BaseComplianceMap.
			// Start IRR
			if (MappingPackage.eINSTANCE.getMapJustification_Explanation()
					.equals(msg.getFeature())
					&& basePart != null
					&& isAccessible(BaselineViewsRepository.BaseComplianceMap.Properties.justification)) {
				if (msg.getNewValue() != null) {
					basePart.setJustification(EcoreUtil.convertToString(
							EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setJustification("");
				}
			}
			// End IRR
			
			
			if (BaselinePackage.eINSTANCE.getBaseComplianceMap_Target().equals(msg.getFeature())  && isAccessible(BaselineViewsRepository.BaseComplianceMap.Properties.target))
				basePart.updateTarget();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			GeneralPackage.eINSTANCE.getNamedElement_Id(),
			GeneralPackage.eINSTANCE.getNamedElement_Name(),
			MappingPackage.eINSTANCE.getMap_MapGroup(),
			MappingPackage.eINSTANCE.getMap_MapGroup(),
			MappingPackage.eINSTANCE.getMap_Type(),
			MappingPackage.eINSTANCE.getMap_MapJustification(),
			BaselinePackage.eINSTANCE.getBaseComplianceMap_Target()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (BaselineViewsRepository.BaseComplianceMap.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseComplianceMap.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), newValue);
				}
				if (BaselineViewsRepository.BaseComplianceMap.Properties.type == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(MappingPackage.eINSTANCE.getMap_Type().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(MappingPackage.eINSTANCE.getMap_Type().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
