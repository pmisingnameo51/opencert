/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.evm.evidspec.evidence;

import java.util.Date;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.ManageableAssuranceAsset;
import org.eclipse.opencert.infra.general.general.DescribableElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Artefact</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#getPropertyValue <em>Property Value</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#getArtefactPart <em>Artefact Part</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#getOwnedRel <em>Owned Rel</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#getVersionID <em>Version ID</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#getDate <em>Date</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#getChanges <em>Changes</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#isIsLastVersion <em>Is Last Version</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#getResource <em>Resource</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#getPrecedentVersion <em>Precedent Version</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#isIsTemplate <em>Is Template</em>}</li>
 *   <li>{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#isIsConfigurable <em>Is Configurable</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefact()
 * @model
 * @generated
 */
public interface Artefact extends ManageableAssuranceAsset, DescribableElement {
	/**
	 * Returns the value of the '<em><b>Property Value</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.evm.evidspec.evidence.Value}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Value</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Value</em>' containment reference list.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefact_PropertyValue()
	 * @model containment="true"
	 * @generated
	 */
	EList<Value> getPropertyValue();

	/**
	 * Returns the value of the '<em><b>Artefact Part</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.evm.evidspec.evidence.Artefact}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Artefact Part</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Artefact Part</em>' containment reference list.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefact_ArtefactPart()
	 * @model containment="true"
	 * @generated
	 */
	EList<Artefact> getArtefactPart();

	/**
	 * Returns the value of the '<em><b>Owned Rel</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Rel</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Rel</em>' containment reference list.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefact_OwnedRel()
	 * @model containment="true"
	 * @generated
	 */
	EList<ArtefactRel> getOwnedRel();

	/**
	 * Returns the value of the '<em><b>Version ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version ID</em>' attribute.
	 * @see #setVersionID(String)
	 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefact_VersionID()
	 * @model required="true"
	 * @generated
	 */
	String getVersionID();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#getVersionID <em>Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version ID</em>' attribute.
	 * @see #getVersionID()
	 * @generated
	 */
	void setVersionID(String value);

	/**
	 * Returns the value of the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date</em>' attribute.
	 * @see #setDate(Date)
	 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefact_Date()
	 * @model
	 * @generated
	 */
	Date getDate();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#getDate <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date</em>' attribute.
	 * @see #getDate()
	 * @generated
	 */
	void setDate(Date value);

	/**
	 * Returns the value of the '<em><b>Changes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Changes</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Changes</em>' attribute.
	 * @see #setChanges(String)
	 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefact_Changes()
	 * @model
	 * @generated
	 */
	String getChanges();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#getChanges <em>Changes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Changes</em>' attribute.
	 * @see #getChanges()
	 * @generated
	 */
	void setChanges(String value);

	/**
	 * Returns the value of the '<em><b>Is Last Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Last Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Last Version</em>' attribute.
	 * @see #setIsLastVersion(boolean)
	 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefact_IsLastVersion()
	 * @model
	 * @generated
	 */
	boolean isIsLastVersion();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#isIsLastVersion <em>Is Last Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Last Version</em>' attribute.
	 * @see #isIsLastVersion()
	 * @generated
	 */
	void setIsLastVersion(boolean value);

	/**
	 * Returns the value of the '<em><b>Resource</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.evm.evidspec.evidence.Resource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource</em>' containment reference list.
	 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefact_Resource()
	 * @model containment="true"
	 * @generated
	 */
	EList<Resource> getResource();

	/**
	 * Returns the value of the '<em><b>Precedent Version</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Precedent Version</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Precedent Version</em>' reference.
	 * @see #setPrecedentVersion(Artefact)
	 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefact_PrecedentVersion()
	 * @model
	 * @generated
	 */
	Artefact getPrecedentVersion();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#getPrecedentVersion <em>Precedent Version</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Precedent Version</em>' reference.
	 * @see #getPrecedentVersion()
	 * @generated
	 */
	void setPrecedentVersion(Artefact value);

	/**
	 * Returns the value of the '<em><b>Is Template</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Template</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Template</em>' attribute.
	 * @see #setIsTemplate(boolean)
	 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefact_IsTemplate()
	 * @model
	 * @generated
	 */
	boolean isIsTemplate();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#isIsTemplate <em>Is Template</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Template</em>' attribute.
	 * @see #isIsTemplate()
	 * @generated
	 */
	void setIsTemplate(boolean value);

	/**
	 * Returns the value of the '<em><b>Is Configurable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Configurable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Configurable</em>' attribute.
	 * @see #setIsConfigurable(boolean)
	 * @see org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage#getArtefact_IsConfigurable()
	 * @model
	 * @generated
	 */
	boolean isIsConfigurable();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.evm.evidspec.evidence.Artefact#isIsConfigurable <em>Is Configurable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Configurable</em>' attribute.
	 * @see #isIsConfigurable()
	 * @generated
	 */
	void setIsConfigurable(boolean value);

} // Artefact
