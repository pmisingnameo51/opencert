/*******************************************************************************
 * Copyright (C) 2018 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic.cdo;

import org.eclipse.emf.cdo.CDOObject;
import org.eclipse.emf.cdo.common.id.CDOID;
import org.eclipse.emf.cdo.common.id.CDOIDUtil;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.URI;
import org.eclipse.opencert.elastic.search.ObjectResolver;

public class CDOObjectResolver implements ObjectResolver<CDOObject> {

	@Override
	public CDOObject resolve(String objectId, Object context) {
		// the only context we can handle at the moment is a CDOView
		if (context instanceof CDOView) {
			try {
				URI uri = URI.createURI(objectId);
				CDOID id = CDOIDUtil.read(uri.fragment());
				CDOView view = (CDOView) context;
				CDOObject object = view.getObject(id, true);
				return object;
			} catch (Exception e) {
				// ignore
			}
		}

		return null;
	}
}
