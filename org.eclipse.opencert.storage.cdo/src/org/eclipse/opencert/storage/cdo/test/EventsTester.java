/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.storage.cdo.test;

import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOQuery;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel;
import org.eclipse.opencert.evm.evidspec.evidence.EvidenceFactory;
import org.eclipse.opencert.storage.cdo.StandaloneCDOAccessor;
import org.eclipse.opencert.storage.cdo.executors.SimpleInTransactionExecutor;

public class EventsTester
{
    public static void main(String[] args)
    {
        int id = 1;
        
        createArtefact(id);
        
        updateArtefact(id);
        
        deleteArtefact(id);
        
        createArtefactRel(id);
        
        updateArtefactRel(id);
        
        deleteArtefactRel(id);
    }


    private static void createArtefact(final int artefactId)
    {
        new SimpleInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                System.out.print("scenario: creating artefact...");
                
                Resource resource = StandaloneCDOAccessor.getDefaultResource(cdoTransaction);
                
                Artefact artefact = EvidenceFactory.eINSTANCE
                        .createArtefact();
                artefact.setDescription("description_" + artefactId);
                artefact.setId(String.valueOf(artefactId));
                artefact.setName("name_" + artefactId);
                artefact.setVersionID(String.valueOf(1));
                artefact.setChanges("First_artefact_version");
                artefact.setIsLastVersion(true);
                
                resource.getContents().add(artefact);
                
                cdoTransaction.setCommitComment("events test");
                
                System.out.println("done");
                
            }
        }.executeReadWriteOperation();
    }
    
    
    private static void updateArtefact(final int artefactId)
    {
        new SimpleInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                System.out.print("scenario: updating artefact...");
                
                CDOQuery cqo = cdoTransaction
                        .createQuery("sql", createArtefactQuery(artefactId));
                
                Artefact artefact = cqo.getResultValue(Artefact.class);
                
                artefact.setName(artefact.getName() + "_updated_" + 2);
                
                cdoTransaction.setCommitComment("Changing name of artefact " + artefactId);
                
                System.out.println("done");
            }
        }.executeReadWriteOperation();
    }
    
    
    private static void deleteArtefact(final int artefactId)
    {
        new SimpleInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                System.out.print("scenario: deleting artefact...");
                
                Resource resource = StandaloneCDOAccessor.getDefaultResource(cdoTransaction);
                
                CDOQuery cqo = cdoTransaction
                        .createQuery("sql", createArtefactQuery(artefactId));
                
                Artefact artefact = cqo.getResultValue(Artefact.class);
                
                resource.getContents().remove(artefact);
                
                cdoTransaction.setCommitComment("Removing artefact " + artefactId);
                
                System.out.println("done");
            }
        }.executeReadWriteOperation();
        
    }

    
    private static void createArtefactRel(final int artefactRelId)
    {
        new SimpleInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                System.out.print("scenario: Creating artefactrel...");
                
                Resource resource = StandaloneCDOAccessor.getDefaultResource(cdoTransaction);
                
                ArtefactRel artefactRel = EvidenceFactory.eINSTANCE
                        .createArtefactRel();
                artefactRel.setDescription("description_" + artefactRelId);
                artefactRel.setId(String.valueOf(artefactRelId));
                artefactRel.setName("name_" + artefactRelId);
                
                resource.getContents().add(artefactRel);
                
                cdoTransaction.setCommitComment("events test");
                
                System.out.println("done");
                
            }
        }.executeReadWriteOperation();
    }
    
    
    private static void updateArtefactRel(final int artefactRelId)
    {
        new SimpleInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                System.out.print("scenario: updating artefactRel...");
                
                CDOQuery cqo = cdoTransaction
                        .createQuery("sql", createArtefactRelQuery(artefactRelId));
                
                ArtefactRel artefactRel = cqo.getResultValue(ArtefactRel.class);
                
                artefactRel.setName(artefactRel.getName() + "_updated_" + 2);
                
                cdoTransaction.setCommitComment("Changing name of artefactRel " + artefactRelId);
                
                System.out.println("done");
            }
        }.executeReadWriteOperation();
    }
    
    
    private static void deleteArtefactRel(final int artefactRelId)
    {
        new SimpleInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                System.out.print("scenario: deleting artefactRel...");
                
                Resource resource = StandaloneCDOAccessor.getDefaultResource(cdoTransaction);
                
                CDOQuery cqo = cdoTransaction
                        .createQuery("sql", createArtefactRelQuery(artefactRelId));
                
                ArtefactRel artefactRel = cqo.getResultValue(ArtefactRel.class);
                
                resource.getContents().remove(artefactRel);
                
                cdoTransaction.setCommitComment("Removing artefactRel " + artefactRelId);
                
                System.out.println("done");
            }
        }.executeReadWriteOperation();
        
    }
    
    

    private static String createArtefactQuery(int artefactId)
    {
        return "select * from evidence_Artefact where id ilike '" + artefactId + "' and cdo_revised = 0 and cdo_version >= 0";
    }
    
    
    private static String createArtefactRelQuery(int artefactRelId)
    {
        return "select * from evidence_ArtefactRel where id ilike '" + artefactRelId + "' and cdo_revised = 0 and cdo_version >= 0";
    }
}