/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.storage.cdo.test;

import java.util.Date;

import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetFactory;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind;
import org.eclipse.opencert.infra.general.general.ChangeEffectKind;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel;
import org.eclipse.opencert.apm.baseline.baseline.BaselineFactory;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel;
import org.eclipse.opencert.evm.evidspec.evidence.EvidenceFactory;
import org.eclipse.opencert.pam.procspec.process.Activity;
import org.eclipse.opencert.pam.procspec.process.ProcessFactory;

public abstract class DataCreationRoutines
{
    public static Artefact createArtefact(String name, int artefactId) 
    {
        Artefact artefact = EvidenceFactory.eINSTANCE
                .createArtefact();
        artefact.setId("id_" + artefactId);
        artefact.setName(name);
        artefact.setDescription("description of " + artefact.getName());
        
        artefact.setVersionID(String.valueOf(1));
        artefact.setChanges("First artefact version");
        artefact.setIsLastVersion(true);
                
        AssuranceAssetEvaluation assuranceAssetEvaluation = createEvaluation(artefactId);
        artefact.getEvaluation().add(assuranceAssetEvaluation);
        AssuranceAssetEvent assuranceAssetEvent = createEvaluationEvent(artefactId, assuranceAssetEvaluation);
        artefact.getLifecycleEvent().add(assuranceAssetEvent);

        return artefact;
    }
    
    
    public static Activity createActivity(String name, int activityId) 
    {
        Activity activity = ProcessFactory.eINSTANCE
                .createActivity();
        activity.setId("id_" + activityId);
        activity.setName(name);
        activity.setDescription("description of " + activity.getName());
        
        AssuranceAssetEvaluation assuranceAssetEvaluation = createEvaluation(activityId);
        activity.getEvaluation().add(assuranceAssetEvaluation);
        AssuranceAssetEvent assuranceAssetEvent = createEvaluationEvent(activityId, assuranceAssetEvaluation);
        activity.getLifecycleEvent().add(assuranceAssetEvent);

        return activity;
    }

    
    public static ArtefactRel createArtefactRel(String name,
            Artefact sourceArtefact, Artefact targetArtefact, ChangeEffectKind modificationEffect, ChangeEffectKind revocationEffect)
    {
        ArtefactRel artefactRel = EvidenceFactory.eINSTANCE.createArtefactRel();
        artefactRel.setId("id_" + name);
        artefactRel.setName(name);
        artefactRel.setModificationEffect(modificationEffect);
        artefactRel.setRevocationEffect(revocationEffect);
        artefactRel.setSource(sourceArtefact);
        artefactRel.setTarget(targetArtefact);
        
        return artefactRel;
    }
    
    
    private static AssuranceAssetEvaluation createEvaluation(int elementId) 
    {
        AssuranceAssetEvaluation assuranceAssetEvaluation = AssuranceassetFactory.eINSTANCE.createAssuranceAssetEvaluation();
        assuranceAssetEvaluation.setCriterion("Criterion: " + elementId);
        assuranceAssetEvaluation.setCriterionDescription("Criterion description: " + elementId);
        assuranceAssetEvaluation.setEvaluationResult("Evaluation Result " + elementId);
        assuranceAssetEvaluation.setId("Id " + elementId);
        assuranceAssetEvaluation.setName("Name " + elementId);
        assuranceAssetEvaluation.setRationale("Rationale " + elementId);
        
        return assuranceAssetEvaluation;
    }
    
    private static AssuranceAssetEvent createEvaluationEvent(int elementId, AssuranceAssetEvaluation assuranceAssetEvaluation) 
    {
        AssuranceAssetEvent assuranceAssetEvent = AssuranceassetFactory.eINSTANCE.createAssuranceAssetEvent();
        assuranceAssetEvent.setId("Id " + elementId);
        assuranceAssetEvent.setName("AssuranceAssetEvent " + elementId);
        assuranceAssetEvent.setDescription("Generated automatically during sample data creation");
        assuranceAssetEvent.setResultingEvaluation(assuranceAssetEvaluation);
        assuranceAssetEvent.setType(EventKind.EVALUATION);
        assuranceAssetEvent.setTime(new Date());
        
        return assuranceAssetEvent;
    }


    public static BaseArtefact createBaseArtefact(String name, int artefactId)
    {
        BaseArtefact artefact = BaselineFactory.eINSTANCE
                .createBaseArtefact();
        artefact.setId("id_" + artefactId);
        artefact.setName(name);
        artefact.setDescription("description of " + artefact.getName());
        artefact.setIsSelected(true);
        
        return artefact;
    }
    
    
    public static BaseArtefactRel createBaseArtefactRel(String name,
            BaseArtefact sourceArtefact, BaseArtefact targetArtefact, ChangeEffectKind modificationEffect, ChangeEffectKind revocationEffect)
    {
        BaseArtefactRel artefactRel = BaselineFactory.eINSTANCE.createBaseArtefactRel();
        artefactRel.setId("id_" + name);
        artefactRel.setName(name);
        artefactRel.setModificationEffect(modificationEffect);
        artefactRel.setRevocationEffect(revocationEffect);
        artefactRel.setSource(sourceArtefact);
        artefactRel.setTarget(targetArtefact);
        
        return artefactRel;
    }
}
