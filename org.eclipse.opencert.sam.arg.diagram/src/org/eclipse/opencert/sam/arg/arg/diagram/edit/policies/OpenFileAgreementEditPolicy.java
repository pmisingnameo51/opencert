/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.edit.policies;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;


import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.cdo.common.security.CDOPermission;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.ui.DawnEditorInput;
import org.eclipse.emf.cdo.dawn.ui.helper.EditorDescriptionHelper;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.core.preferences.PreferencesHint;
import org.eclipse.gmf.runtime.diagram.core.services.ViewService;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.OpenEditPolicy;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.HintedDiagramLinkStyle;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.eclipse.gmf.runtime.notation.Style;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.impl.ShapeImpl;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.CaseEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgDiagramEditor;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgDiagramEditorPlugin;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgDiagramEditorUtil;
import org.eclipse.opencert.sam.arg.arg.diagram.part.Messages;
import org.eclipse.opencert.sam.arg.arg.impl.AgreementImpl;
import org.eclipse.opencert.sam.arg.arg.impl.ArgumentationImpl;
import org.eclipse.opencert.sam.arg.ui.util.UtilConstants;
import org.eclipse.emf.common.util.URI;



/**
 * @generated NOT
 */
/**
 * 
 * @author M� Carmen Palacios
 */
public class OpenFileAgreementEditPolicy extends OpenEditPolicy {


	/**
	 * @generated NOT
	 */
	protected Command getOpenCommand(Request request) {
		EditPart targetEditPart = getTargetEditPart(request);
		if (false == targetEditPart.getModel() instanceof View) {
			return null;
		}
		View view = (View) targetEditPart.getModel();
		AgreementImpl agreeImpl = (AgreementImpl)((View)targetEditPart.getModel()).getElement();
		if(agreeImpl == null || agreeImpl.getId() == null)
		{
			return null;			
		}
		String location = agreeImpl.getLocation();
		return new ICommandProxy(new OpenFileAgreementCommand(targetEditPart, location));
	}

	/**
	 * @generated NOT
	 */
	private static class OpenFileAgreementCommand extends
			AbstractTransactionalCommand {

		private final EditPart targetEditPart;
		private String diagramLocation;

		/**
		 * @generated NOT
		 */
		OpenFileAgreementCommand(EditPart targetEditPart,String loca) {
			// editing domain is taken for original diagram, 
			// if we open diagram from another file, we should use another editing domain
			super(TransactionUtil.getEditingDomain(targetEditPart.getModel()),
					Messages.CommandName_OpenDiagram, null);
			this.targetEditPart = targetEditPart;
			this.diagramLocation=loca;
		}

		// FIXME canExecute if  !(readOnly && getDiagramToOpen == null), i.e. open works on ro diagrams only when there's associated diagram already

		/**
		 * @generated NOT
		 */
		protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
				IAdaptable info) throws ExecutionException {
			try {
				//Start changes from Alejandra
				Diagram diagram=null;
				URI uri=null;
				if(diagramLocation==null){
					diagram=(Diagram)initializeNewAgreement();
				}else{
					diagram=getAgreementToOpen();
				}
				String editorID =  EditorDescriptionHelper.getEditorIdForDawnEditor(diagram.getName());
		        uri=URI.createURI(diagramLocation);
		          if (editorID != null && !editorID.equals("")){
		        	  DawnEditorInput editorInput = new DawnEditorInput(uri);
		              IWorkbenchPage page = PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow().getActivePage();		              
		              page.openEditor(editorInput, editorID);
		          }
		            
//				IFile agreement = getAgreementToOpen();
//				if (agreement == null) {
//					agreement = initializeNewAgreement();
//				}
//
//				AgreementImpl agreeImpl = (AgreementImpl)((View)targetEditPart.getModel()).getElement();
//				if(agreeImpl == null && (agreement.getName() == null || agreement.getName().compareTo("") == 0)) // si agreement relacionado a una View cuyo modelo ha sido borrado
//				{
//					//+ agreement.getName());					
//					throw new ExecutionException("Can't open agreement '");
//
//				}
//				IEditorDescriptor desc = PlatformUI.getWorkbench().
//				        getEditorRegistry().getDefaultEditor(agreement.getName());
//				IWorkbenchPage page = PlatformUI.getWorkbench()
//						.getActiveWorkbenchWindow().getActivePage();
//				page.openEditor(new FileEditorInput(agreement), desc.getId());
		          // end of changes from Alejandra
				return CommandResult.newOKCommandResult();
			} catch (Exception ex) {
				throw new ExecutionException("Can't open agreement", ex);
			}
		}
		
		/**
		 * @generated NOT
		 */
		protected Diagram getAgreementToOpen() {
			AgreementImpl agreeImpl = (AgreementImpl)((View)targetEditPart.getModel()).getElement();
			if(agreeImpl == null || agreeImpl.getId() == null) // si diagram relacionado a una View cuyo modelo ha sido borrado
			{
				return null;			
			}
			//Alejandra start changes
//			IFile locationFile  = null;
//			String name = agreeImpl.getId();
//			String location = agreeImpl.getLocation(); // location contiene el nombre completo del fichero
//    		if(location != null)
//    		{
			Diagram d=null;
    		if (diagramLocation.startsWith("/")){
    			//we are in CDO
    			CDOConnectionUtil.instance.init(
    					PreferenceConstants.getRepositoryName(),
    					PreferenceConstants.getProtocol(),
    					PreferenceConstants.getServerName());
    			CDOSession sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
    			CDOView viewCDO = CDOConnectionUtil.instance.openView(sessionCDO);	
    			URI uri=null;
    			CDOResourceNode[] listR=  viewCDO.getElements();	
    			
    			for (int i=0; i<listR.length; i++){
    				String aux=listR[i].getPath();
    				Object o=listR[i];
    				if(o instanceof CDOResourceFolder){
    					CDOResourceNode r=checkFolderContents((CDOResourceFolder)o,diagramLocation);
    					if (r!=null){
    						uri=r.getURI();
    						diagramLocation=uri.toString();
    						EList<EObject> nodeContent = r.eContents();
    						Iterator<EObject> it=nodeContent.iterator();
    						while(it.hasNext()){
    							Object obj2=it.next();
    							if(obj2 instanceof Diagram){
    								d=(Diagram) obj2;
    							}
    						}				
    					}
    				}else{
    					if(aux.contains(diagramLocation)){
    						uri=listR[i].getURI();
    						diagramLocation=uri.toString();
    						EList<EObject> nodeContent = listR[i].eContents();
    						Iterator<EObject> it=nodeContent.iterator();
    						while(it.hasNext()){
    							Object obj2=it.next();
    							if(obj2 instanceof Diagram){
    								d=(Diagram) obj2;
    							}
    						}
    					}
    				}
    			}
    			if (d == null) {
    				return null;
    			}
    		       
    		}else{
    			//End of Alejandra changes
    			// we are not in cdo, we are working with files. this part does not work for the moment
    			URI agreementURI = URI.createFileURI(diagramLocation);
    			ResourceSet resourceSet = getEditingDomain().getResourceSet();
    			System.out.println("Loaded " + agreementURI);
    			Resource agreementResource = resourceSet.getResource(agreementURI, true);
    			System.out.println("IsLoaded?: "+agreementResource.isLoaded());
    			/* MCP: No funciona
    			File lFile = new File(location);
    			locationFile = fileToIFile(lFile);
    			 */
    			/* MCP: funciona pero cuando la propiedad esta en formato fichero SO
				locationFile = WorkspaceSynchronizer.getFile(agreementResource);
    			 */
    			IPath path = new Path(diagramLocation);
    			IFile locationFile = ResourcesPlugin.getWorkspace().getRoot().getFile(path);
    			/*
    			IResource resource = locationFile;
    			if (resource.exists())
    			{
    				int kk = 0;
    			}
    			 */
    		}
//    		}//location=null
//    		if(location == null || location.isEmpty() || locationFile == null || !locationFile.exists())
//    		{
//    			return null;
//    		}
//    		
//    		return locationFile;
    		return d;
		}
		
		public static boolean hasReadPermission(CDOResourceNode node) {
			// force to get the last revision
			node.cdoReload();
			CDOPermission permission = node.cdoRevision().getPermission();
			return permission.isReadable();
		}
		
		/**
		 * @generated NOT
		 */
		private CDOResourceNode checkFolderContents(CDOResourceFolder cdoResourceFolder, String searchDir) {
			if (hasReadPermission(cdoResourceFolder)) {
				EList<CDOResourceNode> listN=  cdoResourceFolder.getNodes();
				for (int i=0; i<listN.size(); i++){
					String aux=listN.get(i).getPath();
					if(aux.contains(searchDir)){
						//we find it
						CDOResourceNode node=listN.get(i);
						return node;
					}
					else{
						if(listN.get(i) instanceof CDOResourceFolder){
							checkFolderContents((CDOResourceFolder)listN.get(i), searchDir);
						}
					}
				}
			}
			return null;
		}

		/**
		 * @generated NOT
		 */
		protected Diagram initializeNewAgreement() throws ExecutionException {
			String finalDiagramLocation="";
			//String folder0 = getPath2(((View)targetEditPart.getModel()).eResource().getURI().toFileString());
			// Start MCP
			/* Dawn cdo */
			//String diagramFullFilename = ((View)targetEditPart.getModel()).eResource().getURI().toPlatformString(false);
			String folder = null;
			Resource aux = ((View)targetEditPart.getModel()).eResource();
			if (false == ((View)targetEditPart.getModel()).eResource() instanceof CDOResource)
			{
				// we are in a file diagram, so the contract will be stored in files
				String diagramFullFilename = ((View)targetEditPart.getModel()).eResource().getURI().toPlatformString(false);
				String folder0 = getPath2(diagramFullFilename.substring(diagramFullFilename.lastIndexOf("/")+1, diagramFullFilename.length()));
				System.out.println("folder0="+ folder0);
				String uri= ((View)targetEditPart.getModel()).eResource().getURI().toPlatformString(false);
				int beginIndex = uri.toString().lastIndexOf("/");
				//String folder = getPath2(uri.toString().substring(beginIndex+1, uri.toString().length()));
				folder = uri.toString().substring(0, beginIndex+1);				
			}
			else
			{	
				//we are in cdo diagram, so the contract will also be stored in cdo 
//				This preference does not exist. We will include the contract in the same place as the argumentation diagram
//				String diagramFullFilename = Platform.getPreferencesService().
//				  getString("org.eclipse.opencert.sam.preferences", UtilConstants.Agreement_PATH, "Agreements Dir wrong", null);
				View view = (View) targetEditPart.getModel();
				Style link = view.getStyle(NotationPackage.eINSTANCE
						.getHintedDiagramLinkStyle());
				CDOResource cdoaux=(CDOResource)aux;
				CDOResourceFolder cdofolder = cdoaux.getFolder();
				String contractDiagramLocation=cdofolder.getURI().toString();
				String cdoFolderLocation=cdofolder.getURI().toString();
				URI cdoFolderURI = URI.createPlatformResourceURI(cdoFolderLocation, false);
				IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(cdoFolderURI.lastSegment()); ; //retrieve the ModulesDir IProject
				// create an empty diagram and model
				ShapeImpl sh = (ShapeImpl) link.eContainer();
				AgreementImpl agreeImpl= (AgreementImpl) sh.basicGetElement();
//				ArgumentationImpl argImpl = (ArgumentationImpl)sh.basicGetElement();
				if(agreeImpl == null || agreeImpl.getId() == null) // si diagram relacionado a una View cuyo modelo ha sido borrado
				{
					throw new ExecutionException("Can't create diagram null.arg_diagram");				
				}
				String name = agreeImpl.getId();
				Diagram d = null;
				String diagramName = name + CaseEditPart.FILE_DIAGRAM_ID;
				String modelName =  name + CaseEditPart.FILE_MODEL_ID;
				URI projectPluginDiagramURI = URI.createPlatformResourceURI(ArgDiagramEditorPlugin.ID, false);
				IProject projectPluginDiagram = ResourcesPlugin.getWorkspace().getRoot().getProject(projectPluginDiagramURI.lastSegment());
				String emptyDiagram = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
									+ "<notation:Diagram xmi:version=\"2.0\" xmlns:xmi=\"http://www.omg.org/XMI\" xmlns:arg=\"arg\" xmlns:notation=\"http://www.eclipse.org/gmf/runtime/1.0.2/notation\" xmi:id=\"_n3jlgPBwEeKfGOouoiya7g\" type=\"Arg\" name=\"empty.arg_diagram\" measurementUnit=\"Pixel\"> "
									+ "<styles xmi:type=\"notation:DiagramStyle\" xmi:id=\"_n3jlgfBwEeKfGOouoiya7g\"/>"
									+ "<element xmi:type=\"arg:Case\" href=\"empty.arg#/\"/> "
									+ "</notation:Diagram>";
				String emptyModel = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
							        + "<arg:Case xmi:version=\"2.0\" xmlns:xmi=\"http://www.omg.org/XMI\" xmlns:arg=\"arg\"/>";
	    		File file2 = new File(contractDiagramLocation+"\\"+ diagramName);
	    		File file2M = new File(contractDiagramLocation+"\\"+ modelName);
	    		try {
	    			//diagram 
	    			copyToFile (emptyDiagram, file2);
	    			// model
	    			copyToFile(emptyModel, file2M);
	    		}
	    		 catch (IOException exception) {
	    			  exception.printStackTrace();
	    		}
	    		
	    		String diagramFName = project.getFile(diagramName).getFullPath().toString();
	    		finalDiagramLocation = diagramFName;
	    		String modelFName =  project.getFile(modelName).getFullPath().toString();
	    				
				URI diagramURI = URI.createPlatformResourceURI(diagramFName, false);
				URI modelURI = URI.createPlatformResourceURI(modelFName, false);
				Resource diagramResource = ArgDiagramEditorUtil.createDiagram(diagramURI, modelURI, new NullProgressMonitor());
				
				d = (Diagram) diagramResource.getContents().get(0);
	    		
//				int beginIndex = diagramFullFilename.toString().lastIndexOf("\\");
//				folder = diagramFullFilename.toString().substring(beginIndex, diagramFullFilename.length());	
//				folder += "\\";
				
			}
			// End MCP			

			System.out.println("folder="+ folder);			
			URI folderURI = URI.createPlatformResourceURI(folder, false);
			IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(folderURI.segment(1)); //retrieve the IProject
			// create an empty agreement
			AgreementImpl argImpl = (AgreementImpl)((View)targetEditPart.getModel()).getElement();
			if(argImpl == null || argImpl.getId() == null) // si diagram relacionado a una View cuyo modelo ha sido borrado
			{
				throw new ExecutionException("Can't create agreement file null.agree");				
			}
			String name = argImpl.getId();
			String location = argImpl.getLocation(); // location contiene el nombre completo del fichero
    		IFile locationFile = null; 
    		if(location != null)
    		{
    			//locationFile = new File(location);
				IPath path = new Path(location);
				locationFile = ResourcesPlugin.getWorkspace().getRoot().getFile(path);
				IResource iresource = locationFile;
				finalDiagramLocation = iresource.getLocation().toOSString();
    		}
    		if(location == null || location.isEmpty() || locationFile == null || !locationFile.exists())
    		{
				String agreementName = name + UtilConstants.ORG_OPENCERT_SAM_AGREE_MODEL_ID;
				
				finalDiagramLocation = folder+agreementName;
				IFile ifile = null;
				IPath path = new Path(finalDiagramLocation);
				ifile = ResourcesPlugin.getWorkspace().getRoot().getFile(path);
				String content="Agreement "+ name +"\n";
				byte[] bytes = content.getBytes();
				InputStream source = new ByteArrayInputStream(bytes);
				try {
					ifile.create(source, IResource.NONE, null);
				} catch (CoreException e) {
					e.printStackTrace();
				}

				ResourceSet rSet = new ResourceSetImpl();
				URI diagramfullFileNameURI = getFileURI(ifile, rSet);
				//MCP: No funciona finalDiagramLocation = diagramfullFileNameURI.toFileString();
				/* MCP: funciona pero cuando la propiedad esta en formato fichero SO
				IResource iresource = ifile;
				finalDiagramLocation = iresource.getLocation().toOSString();
				*/
				finalDiagramLocation = diagramfullFileNameURI.toString();				
				System.out.println("finalDiagramLocation="+ finalDiagramLocation);	
				//locationFile = new File(diagramfullFileName);
				locationFile = ifile;
    		}
    		
    		// inicializar location property
    		String locationprop = (String)argImpl.eGet(ArgPackage.Literals.ARGUMENTATION__LOCATION);
    		/* MCP: funciona pero cuando la propiedad esta en formato fichero SO
			argImpl.eSet(ArgPackage.Literals.ARGUMENTATION__LOCATION, finalDiagramLocation);
			*/
			String nombre = finalDiagramLocation.substring(finalDiagramLocation.lastIndexOf("/")+1, finalDiagramLocation.length());
			String finalDiagramLocation2 = folder + nombre; 
			argImpl.eSet(ArgPackage.Literals.ARGUMENTATION__LOCATION, finalDiagramLocation2);

			EObject container = ((View)targetEditPart.getModel()).eContainer();
			while (container instanceof View) {
				((View) container).persist();
				container = container.eContainer();
			}
			try {
				new WorkspaceModifyOperation() {
					protected void execute(IProgressMonitor monitor)
							throws CoreException, InvocationTargetException,
							InterruptedException {
						try {
							//diagramFacet.eResource()???
							for (Iterator it = ((View)targetEditPart.getModel()).eResource()
									.getResourceSet().getResources().iterator(); it
									.hasNext();) {
								Resource nextResource = (Resource) it.next();
								if (nextResource.isLoaded()
										&& !getEditingDomain().isReadOnly(
												nextResource)) {
									nextResource.save(ArgDiagramEditorUtil
											.getSaveOptions());
								}
							}
						} catch (IOException ex) {
							throw new InvocationTargetException(ex,
									"Save operation failed");
						}
					}
				}.run(null);
			} catch (InvocationTargetException e) {
				throw new ExecutionException("Can't create agreement file: ", e);
			} catch (InterruptedException e) {
				throw new ExecutionException("Can't create agreement file: ", e);
			}
			
//			return locationFile;
			Diagram d=null;
			return d;
		}
		

		private boolean copyToFile(String contentSourceFile, File destFile) throws IOException {
			BufferedWriter writer = new BufferedWriter(new FileWriter(destFile));
		    writer.write(contentSourceFile);
		    return true;
		}
		 
		  /**
		   * @generated NOT
		   */
		   private List<IFile> getDiagramFiles(IContainer folder) {
		       final List<IFile> ret = new ArrayList<IFile>();
		       try {
		            final IResource[] members = folder.members();
		            for (final IResource resource : members) {
		                 if (resource instanceof IContainer) {
		                     ret.addAll(getDiagramFiles((IContainer) resource));
		                 } else if (resource instanceof IFile) {
		                     final IFile file = (IFile) resource;
		                     if (file.getName().endsWith(UtilConstants.ARGFILE_DIAGRAM_ID)) {
		                          ret.add(file);
		                     }
		                 }
		            }
		       } catch (final CoreException e) {
		                e.printStackTrace();
		       }
		       return ret;
		    }
		
	    /**
	     * @generated NOT
	     */
	    private String getPath2(String fileName)
	    {
	    	String path = "";
	    	IProject[] projects = ResourcesPlugin.getWorkspace().getRoot().getProjects();
	    	for(int i=0;i<projects.length;i++)
	    	{
	    		IProject curr = projects[i];
	    		List<IFile> list = getDiagramFiles(curr);
	    		for(IFile element : list)
	    		{
	    			if(element.getName().contains(fileName))
	    			{
	    				path = element.getFullPath().toString();
	    				int lind = path.lastIndexOf(fileName);
	    				String path2 = path.substring(0, lind);
	    				return path2;
	    			}
	    		}
	    	}
	    
	    	return path;
	    }
		
	    /**
	     * @generated NOT
	     */
	    private URI getFileURI(IFile file, ResourceSet resourceSet) {
	        final String pathName = file.getFullPath().toString();
	        URI resourceURI = URI.createFileURI(pathName);
	        resourceURI = resourceSet.getURIConverter().normalize(resourceURI);
	        return resourceURI;
	     }

		
	    /**
	     * @generated NOT
	     */
		private IFile fileToIFile(File f) {
			String p = f.getPath();
			IPath path = new Path(p);
			return ResourcesPlugin.getWorkspace().getRoot().getFile(path);
			}

	}

}
