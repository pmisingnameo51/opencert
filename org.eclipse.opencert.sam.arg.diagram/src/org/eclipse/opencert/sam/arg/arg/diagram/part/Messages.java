/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

	/**
	 * @generated
	 */
	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Messages() {
	}

	/**
	 * @generated
	 */
	public static String ArgCreationWizardTitle;

	/**
	 * @generated
	 */
	public static String ArgCreationWizard_DiagramModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String ArgCreationWizard_DiagramModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String ArgCreationWizard_DomainModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String ArgCreationWizard_DomainModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String ArgCreationWizardOpenEditorError;

	/**
	 * @generated
	 */
	public static String ArgCreationWizardCreationError;

	/**
	 * @generated
	 */
	public static String ArgCreationWizardPageExtensionError;

	/**
	 * @generated
	 */
	public static String ArgDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String ArgDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String ArgDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	 * @generated
	 */
	public static String ArgDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	 * @generated
	 */
	public static String ArgDocumentProvider_isModifiable;

	/**
	 * @generated
	 */
	public static String ArgDocumentProvider_handleElementContentChanged;

	/**
	 * @generated
	 */
	public static String ArgDocumentProvider_IncorrectInputError;

	/**
	 * @generated
	 */
	public static String ArgDocumentProvider_NoDiagramInResourceError;

	/**
	 * @generated
	 */
	public static String ArgDocumentProvider_DiagramLoadingError;

	/**
	 * @generated
	 */
	public static String ArgDocumentProvider_UnsynchronizedFileSaveError;

	/**
	 * @generated
	 */
	public static String ArgDocumentProvider_SaveDiagramTask;

	/**
	 * @generated
	 */
	public static String ArgDocumentProvider_SaveNextResourceTask;

	/**
	 * @generated
	 */
	public static String ArgDocumentProvider_SaveAsOperation;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_WizardTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_OpenModelFileDialogTitle;

	/**
	 * @generated
	 */
	public static String ArgNewDiagramFileWizard_CreationPageName;

	/**
	 * @generated
	 */
	public static String ArgNewDiagramFileWizard_CreationPageTitle;

	/**
	 * @generated
	 */
	public static String ArgNewDiagramFileWizard_CreationPageDescription;

	/**
	 * @generated
	 */
	public static String ArgNewDiagramFileWizard_RootSelectionPageName;

	/**
	 * @generated
	 */
	public static String ArgNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	 * @generated
	 */
	public static String ArgNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	 * @generated
	 */
	public static String ArgNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	 * @generated
	 */
	public static String ArgNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	 * @generated
	 */
	public static String ArgNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	 * @generated
	 */
	public static String ArgNewDiagramFileWizard_InitDiagramCommand;

	/**
	 * @generated
	 */
	public static String ArgNewDiagramFileWizard_IncorrectRootError;

	/**
	 * @generated
	 */
	public static String ArgDiagramEditor_SavingDeletedFile;

	/**
	 * @generated
	 */
	public static String ArgDiagramEditor_SaveAsErrorTitle;

	/**
	 * @generated
	 */
	public static String ArgDiagramEditor_SaveAsErrorMessage;

	/**
	 * @generated
	 */
	public static String ArgDiagramEditor_SaveErrorTitle;

	/**
	 * @generated
	 */
	public static String ArgDiagramEditor_SaveErrorMessage;

	/**
	 * @generated
	 */
	public static String ArgElementChooserDialog_SelectModelElementTitle;

	/**
	 * @generated
	 */
	public static String ModelElementSelectionPageMessage;

	/**
	 * @generated
	 */
	public static String ValidateActionMessage;

	/**
	 * @generated
	 */
	public static String Argumentationcore1Group_title;

	/**
	 * @generated
	 */
	public static String Argumentationrelationships2Group_title;

	/**
	 * @generated
	 */
	public static String Argumentationmodularextensions3Group_title;

	/**
	 * @generated
	 */
	public static String ArgumentReasoning1CreationTool_title;

	/**
	 * @generated
	 */
	public static String ArgumentReasoning1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Argumentation2CreationTool_title;

	/**
	 * @generated
	 */
	public static String Argumentation2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Claim3CreationTool_title;

	/**
	 * @generated
	 */
	public static String Claim3CreationTool_desc;
	/**
	 * ARL changes
	 * Includes Assumption
	 */
	public static String Assumption3CreationTool_title;
	public static String Assumption3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String InformationElementCitation4CreationTool_title;

	/**
	 * @generated
	 */
	public static String InformationElementCitation4CreationTool_desc;
	/**
	 * ARL 
	 * Includes context, justification and solution
	 */
	public static String Context4CreationTool_title;
	public static String Context4CreationTool_desc;
	public static String Justification4CreationTool_title;
	public static String Justification4CreationTool_desc;
	public static String Solution4CreationTool_title;
	public static String Solution4CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AssertedChallenge1CreationTool_title;

	/**
	 * @generated
	 */
	public static String AssertedChallenge1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AssertedContext2CreationTool_title;

	/**
	 * @generated
	 */
	public static String AssertedContext2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AssertedCounterEvidence3CreationTool_title;

	/**
	 * @generated
	 */
	public static String AssertedCounterEvidence3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AssertedEvidence4CreationTool_title;

	/**
	 * @generated
	 */
	public static String AssertedEvidence4CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AssertedInference5CreationTool_title;

	/**
	 * @generated
	 */
	public static String AssertedInference5CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Choice6CreationTool_title;

	/**
	 * @generated
	 */
	public static String Choice6CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ArgumentElementCitation1CreationTool_title;

	/**
	 * @generated
	 */
	public static String ArgumentElementCitation1CreationTool_desc;
	/**
	 * ARL 
	 * Includes AwayGoal, AwayContext and Away Solution
	 */
	public static String AwayGoal1CreationTool_title;
	public static String AwayGoal1CreationTool_desc;
	public static String AwayContext1CreationTool_title;
	public static String AwayContext1CreationTool_desc;	
	public static String AwaySolution1CreationTool_title;
	public static String AwaySolution1CreationTool_desc;	
	/**
	 * @generated
	 */
	public static String Agreement2CreationTool_title;

	/**
	 * @generated
	 */
	public static String Agreement2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String CommandName_OpenDiagram;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Case_1000_links;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Claim_2001_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Claim_2001_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Agreement_2002_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Agreement_2002_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ArgumentReasoning_2003_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ArgumentReasoning_2003_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Argumentation_2004_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Argumentation_2004_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_InformationElementCitation_2005_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_InformationElementCitation_2005_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ArgumentElementCitation_2006_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ArgumentElementCitation_2006_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Choice_2007_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Choice_2007_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Choice_3001_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Choice_3001_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AssertedInference_4001_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AssertedInference_4001_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AssertedInference_4001_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AssertedInference_4001_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AssertedEvidence_4002_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AssertedEvidence_4002_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AssertedEvidence_4002_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AssertedEvidence_4002_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AssertedContext_4003_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AssertedContext_4003_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AssertedContext_4003_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AssertedContext_4003_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AssertedChallenge_4004_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AssertedChallenge_4004_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AssertedChallenge_4004_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AssertedCounterEvidence_4005_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AssertedCounterEvidence_4005_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AssertedCounterEvidence_4005_source;

	/**
	 * @generated
	 */
	public static String NavigatorActionProvider_OpenDiagramActionName;

	/**
	 * @generated
	 */
	public static String MessageFormatParser_InvalidInputError;

	/**
	 * @generated
	 */
	public static String ArgModelingAssistantProviderTitle;

	/**
	 * @generated
	 */
	public static String ArgModelingAssistantProviderMessage;

	//TODO: put accessor fields manually	
}
