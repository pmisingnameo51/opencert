package org.eclipse.opencert.sam.arg.arg.diagram.instantiation;

import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgDiagramEditor;

public class InstantiateVariableHandler extends AbstractHandler {

	@Override
	@SuppressWarnings("unchecked")
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Map<String, String> parameters = event.getParameters();

		String itemRawString = parameters.get(ParameterNames.itemBody);
		String replacement = parameters.get(ParameterNames.replacement);
		String commandName = parameters.get(ParameterNames.commandName);
		String editorHash = parameters.get(ParameterNames.editorHash);
		ArgDiagramEditor editor = getArgEditors(editorHash);

		InstantiationEMFCommand command = new InstantiationEMFCommand(
				commandName, itemRawString, replacement, editor);
		editor.getEditingDomain().getCommandStack().execute(command);

		return null;
	}

	private ArgDiagramEditor getArgEditors(String editorHash) {
		ArgDiagramEditor result = null;

		IEditorPart editor = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		if (editor instanceof ArgDiagramEditor) {
			if (editor.hashCode() == Integer.parseInt(editorHash)){
				result = (ArgDiagramEditor) editor;
			}
		}

		return result;
	}
}
