/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.edit.parts;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;
import org.eclipse.opencert.gsn.figures.GSNArgumentModule;
import org.eclipse.opencert.gsn.figures.LayoutUtil;
import org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation;
import org.eclipse.opencert.sam.arg.arg.CitationElementType;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.policies.ArgumentElementCitationItemSemanticEditPolicy;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.policies.OpenDiagramEditPolicy;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgVisualIDRegistry;
import org.eclipse.opencert.userguidance.label.WrappingLabel2;

/**
 * @generated
 */
public class ArgumentElementCitationEditPart extends ShapeNodeEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 2006;

	/**
	 * @generated
	 */
	protected IFigure contentPane;

	/**
	 * @generated
	 */
	protected IFigure primaryShape;

	/**
	 * @generated
	 */
	public ArgumentElementCitationEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new ArgumentElementCitationItemSemanticEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		installEditPolicy(EditPolicyRoles.OPEN_ROLE,
				new OpenDiagramEditPolicy());
		// XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated
	 */
	protected LayoutEditPolicy createLayoutEditPolicy() {
		org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy lep = new org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy() {

			protected EditPolicy createChildEditPolicy(EditPart child) {
				EditPolicy result = child
						.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null) {
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request) {
				return null;
			}

			protected Command getCreateCommand(CreateRequest request) {
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated NOT
	 */
	protected IFigure createNodeShape() {
		// Start MCP
		//return primaryShape = new ArgumentElementCitationFigure();
		if (!(((View) this.getModel()).getElement() instanceof ArgumentElementCitation)) {
			System.out.println("createNodeShape(): Error model not allowed");
			return primaryShape = new ArgumentElementCitationFigure();
		}

		ArgumentElementCitation newElem = (ArgumentElementCitation) ((Node) this
				.getModel()).getElement();
		primaryShape = new ArgumentElementCitationFigure();
		if (newElem.getCitedType() == CitationElementType.CLAIM) {
			((ArgumentElementCitationFigure) (primaryShape)).setShape(1);
		} else if (newElem.getCitedType() == CitationElementType.CONTEXT) {
			((ArgumentElementCitationFigure) (primaryShape)).setShape(2);
		} else // Similar to SOLUTION
		{
			((ArgumentElementCitationFigure) (primaryShape)).setShape(3);
		}

		return primaryShape;
		// End MCP
	}

	/**
	 * @generated
	 */
	public ArgumentElementCitationFigure getPrimaryShape() {
		return (ArgumentElementCitationFigure) primaryShape;
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof ArgumentElementCitationIdEditPart) {
			((ArgumentElementCitationIdEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureArgumentElementCitationLabelFigure());
			return true;
		}
		if (childEditPart instanceof ArgumentElementCitationDescriptionEditPart) {
			((ArgumentElementCitationDescriptionEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureArgumentationElementDescriptionLabelFigure());
			return true;
		}
		if (childEditPart instanceof ArgumentElementCitationArgumentationReferenceEditPart) {
			((ArgumentElementCitationArgumentationReferenceEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureArgumentElementCitationArgumentationReferenceLabelFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof ArgumentElementCitationIdEditPart) {
			return true;
		}
		if (childEditPart instanceof ArgumentElementCitationDescriptionEditPart) {
			return true;
		}
		if (childEditPart instanceof ArgumentElementCitationArgumentationReferenceEditPart) {
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart) {
		return getContentPane();
	}

	/**
	 * @generated
	 */
	protected NodeFigure createNodePlate() {
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(120, 80);
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createNodeFigure() {
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane.
	 * Respects layout one may have set for generated figure.
	 * @param nodeShape instance of generated figure class
	 * @generated
	 */
	protected IFigure setupContentPane(IFigure nodeShape) {
		if (nodeShape.getLayoutManager() == null) {
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			layout.setSpacing(5);
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane() {
		if (contentPane != null) {
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	protected void setForegroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setForegroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setBackgroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setBackgroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineWidth(int width) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineWidth(width);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineType(int style) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineStyle(style);
		}
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart() {
		return getChildBySemanticHint(ArgVisualIDRegistry
				.getType(ArgumentElementCitationIdEditPart.VISUAL_ID));
	}

	/**
	 * @generated NOT
	 */
	protected void handleNotificationEvent(Notification event) {
		// Start MCP
		/*
		if (event.getNotifier() == getModel()
				&& EcorePackage.eINSTANCE.getEModelElement_EAnnotations()
						.equals(event.getFeature())) {
			handleMajorSemanticChange();
		} else {
			super.handleNotificationEvent(event);
		}
		 */
		if (event.getNotifier() == getModel()
				&& EcorePackage.eINSTANCE.getEModelElement_EAnnotations()
						.equals(event.getFeature())) {
			handleMajorSemanticChange();
		} else {

			if (event.getNotifier() instanceof ArgumentElementCitation) {

				ArgumentElementCitation elem = (ArgumentElementCitation) ((Node) this
						.getModel()).getElement();
				ArgumentElementCitationFigure figure = (ArgumentElementCitationFigure) this
						.getPrimaryShape();
				if (elem.getCitedType() == CitationElementType.CLAIM) {
					figure.setShape(1);
				} else if (elem.getCitedType() == CitationElementType.CONTEXT) {
					figure.setShape(2);
				} else // Similar to SOLUTION
				{
					figure.setShape(3);
				}
			}

			super.handleNotificationEvent(event);
		}
		// End MCP
	}

	/**
	 * @generated NOT
	 */
	public class ArgumentElementCitationFigure extends GSNArgumentModule {
		// Start MCP
		/**
		 * @generated NOT
		 */
		int shape = 1;

		/**
		 * @generated NOT
		 */
		public int getShape() {
			return shape;
		}

		/**
		 * @generated NOT
		 */
		public void setShape(int sh) {
			shape = sh;
		}

		/**
		 * @generated NOT
		 */
		@Override
		public void paint(Graphics graphics) {
			if (getShape() == 1) { //CitationElementType.CLAIM
				paintGSNAwayGoal(graphics);
			} else if (getShape() == 2) { //CitationElementType.CONTEXT
				paintGSNAwayContext(graphics);
			} else // Similar to SOLUTION
			{
				paintGSNAwaySolution(graphics);
			}
		}

		public void paintGSNAwayGoal(Graphics graphics) {
			Rectangle r = getBounds();

			// Define the points of a parallelogram
			Point p1 = new Point(r.x, r.y);
			Point p2 = new Point(r.x + r.width, r.y);
			Point p3 = new Point(r.x, r.y + r.height);
			Point p4 = new Point(r.x + r.width, r.y + r.height);
			Point p5 = new Point(r.x, r.y + 3 * r.height / 4);
			Point p6 = new Point(r.x + r.width, r.y + 3 * r.height / 4);

			Point p7 = new Point(r.x + 2 * r.width / 18, r.y + 14 * r.height
					/ 18);
			Point p8 = new Point(r.x + 3 * r.width / 18, r.y + 14 * r.height
					/ 18);
			Point p9 = new Point(r.x + 3 * r.width / 18, r.y + 15 * r.height
					/ 18);
			Point p10 = new Point(r.x + 2 * r.width / 18, r.y + 15 * r.height
					/ 18);
			Point p11 = new Point(r.x + 5 * r.width / 18, r.y + 15 * r.height
					/ 18);
			Point p12 = new Point(r.x + 2 * r.width / 18, r.y + 17 * r.height
					/ 18);
			Point p13 = new Point(r.x + 5 * r.width / 18, r.y + 17 * r.height
					/ 18);

			PointList pointList = new PointList();
			pointList.addPoint(p1);
			pointList.addPoint(p2);
			pointList.addPoint(p3);
			pointList.addPoint(p4);
			pointList.addPoint(p5);
			pointList.addPoint(p6);
			pointList.addPoint(p7);
			pointList.addPoint(p8);
			pointList.addPoint(p9);
			pointList.addPoint(p10);
			pointList.addPoint(p11);
			pointList.addPoint(p12);
			pointList.addPoint(p13);
			// Fill the shape
			graphics.fillPolygon(pointList);

			// Draw the outline
			graphics.drawLine(p1, p2);
			graphics.drawLine(p1, p3);
			graphics.drawLine(p3, p4);
			graphics.drawLine(p2, p4);
			graphics.drawLine(p5, p6);

			graphics.drawLine(p7, p8);
			graphics.drawLine(p8, p9);
			graphics.drawLine(p7, p10);
			graphics.drawLine(p10, p11);
			graphics.drawLine(p11, p13);
			graphics.drawLine(p12, p13);
			graphics.drawLine(p12, p10);
			// Move the first label to the center of the parallelogram
			WrappingLabel label = (WrappingLabel) getChildren().get(0);
			LayoutUtil.moveToCenterAndUp(label, this, graphics);
			label.paint(graphics);
			// Move the second label to the center of the parallelogram
			label = (WrappingLabel) getChildren().get(1);
			LayoutUtil.moveToCenterAndDown(label, this, graphics);
			label.paint(graphics);

			//Move the third label to the bottom of the shape
			label = (WrappingLabel) getChildren().get(2);
			LayoutUtil.moveToCenterAndBottom(label, this, graphics);
			label.paint(graphics);
		}

		public void paintGSNAwayContext(Graphics graphics) {
			Rectangle r = getBounds();

			// Define the points of a parallelogram
			Point p5 = new Point(r.x, r.y + 3 * r.height / 4);
			Point p6 = new Point(r.x + r.width, r.y + 3 * r.height / 4);

			Point p7 = new Point(r.x + 2 * r.width / 18, r.y + 14 * r.height
					/ 18);
			Point p8 = new Point(r.x + 3 * r.width / 18, r.y + 14 * r.height
					/ 18);
			Point p9 = new Point(r.x + 3 * r.width / 18, r.y + 15 * r.height
					/ 18);
			Point p10 = new Point(r.x + 2 * r.width / 18, r.y + 15 * r.height
					/ 18);
			Point p11 = new Point(r.x + 5 * r.width / 18, r.y + 15 * r.height
					/ 18);
			Point p12 = new Point(r.x + 2 * r.width / 18, r.y + 17 * r.height
					/ 18);
			Point p13 = new Point(r.x + 5 * r.width / 18, r.y + 17 * r.height
					/ 18);

			Point p14 = new Point(r.x, r.y + r.height);
			Point p15 = new Point(r.x + r.width, r.y + r.height);

			PointList pointList = new PointList();
			pointList.addPoint(p5);
			pointList.addPoint(p6);
			pointList.addPoint(p7);
			pointList.addPoint(p8);
			pointList.addPoint(p9);
			pointList.addPoint(p10);
			pointList.addPoint(p11);
			pointList.addPoint(p12);
			pointList.addPoint(p13);
			pointList.addPoint(p14);
			pointList.addPoint(p15);

			// Fill the shape
			graphics.fillPolygon(pointList);

			// Draw the outline
			Rectangle r2 = new Rectangle(r.x, r.y, r.width, 7 * r.height / 4);
			graphics.drawRoundRectangle(r2, r2.width / 4, r2.height / 4);

			graphics.drawLine(p5, p6);

			graphics.drawLine(p7, p8);
			graphics.drawLine(p8, p9);
			graphics.drawLine(p7, p10);
			graphics.drawLine(p10, p11);
			graphics.drawLine(p11, p13);
			graphics.drawLine(p12, p13);
			graphics.drawLine(p12, p10);
			graphics.drawLine(p14, p15);
			graphics.drawLine(p5, p14);
			graphics.drawLine(p6, p15);

			// Move the first label to the center of the parallelogram
			WrappingLabel label = (WrappingLabel) getChildren().get(0);
			LayoutUtil.moveToCenterAndUp(label, this, graphics);
			label.paint(graphics);
			// Move the second label to the center of the parallelogram
			label = (WrappingLabel) getChildren().get(1);
			LayoutUtil.moveToCenterAndDown(label, this, graphics);
			label.paint(graphics);

			//Move the third label to the bottom of the shape
			label = (WrappingLabel) getChildren().get(2);
			LayoutUtil.moveToCenterAndBottom(label, this, graphics);
			label.paint(graphics);
		}

		public void paintGSNAwaySolution(Graphics graphics) {
			Rectangle r = getBounds();

			// Define the points of a parallelogram
			Point p5 = new Point(r.x, r.y + 3 * r.height / 4);
			Point p6 = new Point(r.x + r.width, r.y + 3 * r.height / 4);

			Point p7 = new Point(r.x + 2 * r.width / 18, r.y + 14 * r.height
					/ 18);
			Point p8 = new Point(r.x + 3 * r.width / 18, r.y + 14 * r.height
					/ 18);
			Point p9 = new Point(r.x + 3 * r.width / 18, r.y + 15 * r.height
					/ 18);
			Point p10 = new Point(r.x + 2 * r.width / 18, r.y + 15 * r.height
					/ 18);
			Point p11 = new Point(r.x + 5 * r.width / 18, r.y + 15 * r.height
					/ 18);
			Point p12 = new Point(r.x + 2 * r.width / 18, r.y + 17 * r.height
					/ 18);
			Point p13 = new Point(r.x + 5 * r.width / 18, r.y + 17 * r.height
					/ 18);

			Point p14 = new Point(r.x, r.y + r.height);
			Point p15 = new Point(r.x + r.width, r.y + r.height);

			PointList pointList = new PointList();
			pointList.addPoint(p5);
			pointList.addPoint(p6);
			pointList.addPoint(p7);
			pointList.addPoint(p8);
			pointList.addPoint(p9);
			pointList.addPoint(p10);
			pointList.addPoint(p11);
			pointList.addPoint(p12);
			pointList.addPoint(p13);
			pointList.addPoint(p14);
			pointList.addPoint(p15);

			// Fill the shape
			graphics.fillPolygon(pointList);

			// Draw the outline
			graphics.drawOval(r.x, r.y, r.width, 7 * r.height / 4);

			graphics.drawLine(p5, p6);

			graphics.drawLine(p7, p8);
			graphics.drawLine(p8, p9);
			graphics.drawLine(p7, p10);
			graphics.drawLine(p10, p11);
			graphics.drawLine(p11, p13);
			graphics.drawLine(p12, p13);
			graphics.drawLine(p12, p10);
			graphics.drawLine(p14, p15);
			graphics.drawLine(p5, p14);
			graphics.drawLine(p6, p15);

			// Move the first label to the center of the parallelogram
			WrappingLabel label = (WrappingLabel) getChildren().get(0);
			LayoutUtil.moveToCenterAndUp(label, this, graphics);
			label.paint(graphics);
			// Move the second label to the center of the parallelogram
			label = (WrappingLabel) getChildren().get(1);
			LayoutUtil.moveToCenterAndDown(label, this, graphics);
			label.paint(graphics);
			//Move the third label to the bottom of the shape
			label = (WrappingLabel) getChildren().get(2);
			LayoutUtil.moveToCenterAndBottom(label, this, graphics);
			label.paint(graphics);

		}

		// End MCP

		/**
		 * @generated
		 */
		private WrappingLabel fFigureArgumentElementCitationLabelFigure;
		/**
		 * @generated
		 */
		private WrappingLabel fFigureArgumentationElementDescriptionLabelFigure;
		/**
		 * @generated
		 */
		private WrappingLabel fFigureArgumentElementCitationArgumentationReferenceLabelFigure;

		/**
		 * @generated
		 */
		public ArgumentElementCitationFigure() {
			this.setPreferredSize(new Dimension(getMapMode().DPtoLP(120),
					getMapMode().DPtoLP(80)));
			createContents();
		}

		/**
		 * @generated
		 */
		private void createContents() {

			fFigureArgumentElementCitationLabelFigure = new WrappingLabel();

			fFigureArgumentElementCitationLabelFigure
					.setText("ArgumentElementCitation");

			fFigureArgumentElementCitationLabelFigure
					.setFont(FFIGUREARGUMENTELEMENTCITATIONLABELFIGURE_FONT);

			this.add(fFigureArgumentElementCitationLabelFigure);

			fFigureArgumentationElementDescriptionLabelFigure = new WrappingLabel();

			fFigureArgumentationElementDescriptionLabelFigure.setText("");

			fFigureArgumentationElementDescriptionLabelFigure
					.setFont(FFIGUREARGUMENTATIONELEMENTDESCRIPTIONLABELFIGURE_FONT);

			this.add(fFigureArgumentationElementDescriptionLabelFigure);

			fFigureArgumentElementCitationArgumentationReferenceLabelFigure = new WrappingLabel2();

			fFigureArgumentElementCitationArgumentationReferenceLabelFigure
					.setText("");

			fFigureArgumentElementCitationArgumentationReferenceLabelFigure
					.setFont(FFIGUREARGUMENTELEMENTCITATIONARGUMENTATIONREFERENCELABELFIGURE_FONT);

			this.add(fFigureArgumentElementCitationArgumentationReferenceLabelFigure);

		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureArgumentElementCitationLabelFigure() {
			return fFigureArgumentElementCitationLabelFigure;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureArgumentationElementDescriptionLabelFigure() {
			return fFigureArgumentationElementDescriptionLabelFigure;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureArgumentElementCitationArgumentationReferenceLabelFigure() {
			return fFigureArgumentElementCitationArgumentationReferenceLabelFigure;
		}

	}

	/**
	 * @generated
	 */
	static final Font FFIGUREARGUMENTELEMENTCITATIONLABELFIGURE_FONT = new Font(
			Display.getCurrent(), Display.getDefault().getSystemFont()
					.getFontData()[0].getName(), 9, SWT.BOLD);

	/**
	 * @generated
	 */
	static final Font FFIGUREARGUMENTATIONELEMENTDESCRIPTIONLABELFIGURE_FONT = new Font(
			Display.getCurrent(), Display.getDefault().getSystemFont()
					.getFontData()[0].getName(), 9, SWT.BOLD);

	/**
	 * @generated
	 */
	static final Font FFIGUREARGUMENTELEMENTCITATIONARGUMENTATIONREFERENCELABELFIGURE_FONT = new Font(
			Display.getCurrent(), Display.getDefault().getSystemFont()
					.getFontData()[0].getName(), 9, SWT.BOLD);

}
