/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.edit.parts;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.PrecisionPoint;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.handles.MoveHandle;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.diagram.ui.editparts.BorderedBorderItemEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IBorderItemEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.BorderItemSelectionEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.figures.BorderItemLocator;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;
import org.eclipse.opencert.sam.arg.arg.AssertedByMultiplicityExtension;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.policies.Choice2ItemSemanticEditPolicy;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgVisualIDRegistry;
import org.eclipse.opencert.sam.arg.arg.Choice;
import org.eclipse.opencert.sam.arg.ui.figures.ChoiceDefaultSizeNodeFigureWithFixedAnchors;
import org.eclipse.opencert.sam.arg.ui.figures.ChoiceFigure;
import org.eclipse.opencert.sam.arg.ui.figures.DefaultSizeNodeFigureWithFixedAnchors;

/**
 * @generated
 */
public class Choice2EditPart extends BorderedBorderItemEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 3001;

	/**
	 * @generated
	 */
	protected IFigure contentPane;

	/**
	 * @generated NOT
	 */
	// Start MCP
	//protected IFigure primaryShape;
	protected ChoiceFigure2 primaryShape;

	// End MCP

	/**
	 * @generated
	 */
	public Choice2EditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE,
				getPrimaryDragEditPolicy());
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new Choice2ItemSemanticEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		// XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated
	 */
	protected LayoutEditPolicy createLayoutEditPolicy() {
		org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy lep = new org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy() {

			protected EditPolicy createChildEditPolicy(EditPart child) {
				View childView = (View) child.getModel();
				switch (ArgVisualIDRegistry.getVisualID(childView)) {
				case ChoiceOptionality2EditPart.VISUAL_ID:
					return new BorderItemSelectionEditPolicy() {

						protected List createSelectionHandles() {
							MoveHandle mh = new MoveHandle(
									(GraphicalEditPart) getHost());
							mh.setBorder(null);
							return Collections.singletonList(mh);
						}
					};
				}
				EditPolicy result = child
						.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null) {
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request) {
				return null;
			}

			protected Command getCreateCommand(CreateRequest request) {
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated NOT
	 */
	protected IFigure createNodeShape() {
		// Start MCP		
		//return primaryShape = new ChoiceFigure();
		if (!(((View) this.getModel()).getElement() instanceof Choice)) {
			System.out.println("createNodeShape(): Error model not allowed");
			return primaryShape = new ChoiceFigure2();
		}

		Choice model = (Choice) ((View) this.getModel()).getElement();
		primaryShape = new ChoiceFigure2();
		if (model.getSourceMultiextension() == AssertedByMultiplicityExtension.OPTIONAL) {
			primaryShape.setShape(1);
		} else if (model.getSourceMultiextension() == AssertedByMultiplicityExtension.MULTI) {
			primaryShape.setShape(2);
		} else {
			primaryShape.setShape(0);
		}
		return primaryShape;
		// End MCP		
	}

	/**
	 * @generated NOT
	 */
	// Start MCP
	/*
	public ChoiceFigure getPrimaryShape() {
		return (ChoiceFigure) primaryShape;
	}
	 */
	public ChoiceFigure2 getPrimaryShape() {
		return (ChoiceFigure2) primaryShape;
	}

	// End MCP	

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof ChoiceDescription2EditPart) {
			((ChoiceDescription2EditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureArgumentationElementDescriptionLabelFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof ChoiceDescription2EditPart) {
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart) {
		if (editPart instanceof IBorderItemEditPart) {
			return getBorderedFigure().getBorderItemContainer();
		}
		return getContentPane();
	}

	/**
	 * @generated
	 */
	protected void addBorderItem(IFigure borderItemContainer,
			IBorderItemEditPart borderItemEditPart) {
		if (borderItemEditPart instanceof ChoiceOptionality2EditPart) {
			BorderItemLocator locator = new BorderItemLocator(getMainFigure(),
					PositionConstants.SOUTH);
			locator.setBorderItemOffset(new Dimension(-20, -20));
			borderItemContainer.add(borderItemEditPart.getFigure(), locator);
		} else {
			super.addBorderItem(borderItemContainer, borderItemEditPart);
		}
	}

	/**
	 * @generated NOT
	 */
	protected NodeFigure createNodePlate() {
		// Start MCP
		//DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(40, 40);

		HashMap<String, PrecisionPoint> anchorLocations = new HashMap<String, PrecisionPoint>();
		/* No importa el orden ya que no es una lista ordenada */
		anchorLocations.put("NORTH", new PrecisionPoint(0.5d, 0));
		anchorLocations.put("SOUTH", new PrecisionPoint(0.5d, 1d));
		/* 
		DefaultSizeNodeFigureWithFixedAnchors result = new DefaultSizeNodeFigureWithFixedAnchors(
				40, 40, anchorLocations);
		 */
		ChoiceDefaultSizeNodeFigureWithFixedAnchors result = new ChoiceDefaultSizeNodeFigureWithFixedAnchors(
				40, 40, anchorLocations);
		// End MCP

		//FIXME: workaround for #154536
		result.getBounds().setSize(result.getPreferredSize());
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createMainFigure() {
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane.
	 * Respects layout one may have set for generated figure.
	 * @param nodeShape instance of generated figure class
	 * @generated
	 */
	protected IFigure setupContentPane(IFigure nodeShape) {
		if (nodeShape.getLayoutManager() == null) {
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			layout.setSpacing(5);
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane() {
		if (contentPane != null) {
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	protected void setForegroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setForegroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setBackgroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setBackgroundColor(color);
		}
	}

	/**
	 * @generated NOT
	 */
	// Start MCP	
	protected void setLineWidth(int width) {
		/*
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineWidth(width);
		}
		 */
	}

	// End MCP	

	/**
	 * @generated NOT
	 */
	// Start MCP	
	protected void setLineType(int style) {
		/*		
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineStyle(style);
		}
		 */
	}

	// End MCP	

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart() {
		return getChildBySemanticHint(ArgVisualIDRegistry
				.getType(ChoiceOptionality2EditPart.VISUAL_ID));
	}

	/**
	 * @generated
	 */
	public class ChoiceFigure extends org.eclipse.opencert.sam.arg.ui.figures.Choice {

		/**
		 * @generated
		 */
		private WrappingLabel fFigureArgumentationElementDescriptionLabelFigure;

		/**
		 * @generated
		 */
		public ChoiceFigure() {
			this.setPreferredSize(new Dimension(getMapMode().DPtoLP(40),
					getMapMode().DPtoLP(40)));
			createContents();
		}

		/**
		 * @generated
		 */
		private void createContents() {

			fFigureArgumentationElementDescriptionLabelFigure = new WrappingLabel();

			fFigureArgumentationElementDescriptionLabelFigure.setText("");

			fFigureArgumentationElementDescriptionLabelFigure
					.setFont(FFIGUREARGUMENTATIONELEMENTDESCRIPTIONLABELFIGURE_FONT);

			this.add(fFigureArgumentationElementDescriptionLabelFigure);

		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureArgumentationElementDescriptionLabelFigure() {
			return fFigureArgumentationElementDescriptionLabelFigure;
		}

	}

	/**
	 * @generated NOT
	 */
	// Start MCP
	protected void handleNotificationEvent(Notification event) {
		if (event.getNotifier() == getModel()
				&& EcorePackage.eINSTANCE.getEModelElement_EAnnotations()
						.equals(event.getFeature())) {
			handleMajorSemanticChange();
		} else {

			if (event.getNotifier() instanceof Choice) {

				Choice model = (Choice) ((View) this.getModel()).getElement();
				ChoiceFigure2 figure = (ChoiceFigure2) this.getPrimaryShape();
				if (model.getSourceMultiextension() == AssertedByMultiplicityExtension.OPTIONAL) {
					figure.setShape(1);
					model.setSourceCardinality("");
				} else if (model.getSourceMultiextension() == AssertedByMultiplicityExtension.MULTI) {
					figure.setShape(2);
				} else {
					figure.setShape(0);
					model.setSourceCardinality("");
				}
			}
			super.handleNotificationEvent(event);
		}
	}

	// End MCP

	// Start MCP
	/**
	 * @generated NOT
	 */
	public class ChoiceFigure2 extends ChoiceFigure {

	}

	/**
	 * @generated
	 */
	static final Font FFIGUREARGUMENTATIONELEMENTDESCRIPTIONLABELFIGURE_FONT = new Font(
			Display.getCurrent(), Display.getDefault().getSystemFont()
					.getFontData()[0].getName(), 9, SWT.BOLD);

}
