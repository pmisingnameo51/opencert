/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.GetParserOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserProvider;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserService;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ParserHintAdapter;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AgreementDescriptionEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AgreementIdEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentElementCitationArgumentationReferenceEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentElementCitationDescriptionEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentElementCitationIdEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentReasoningDescriptionEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentReasoningIdEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentationDescriptionEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ArgumentationIdEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedContextCardinalityEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedEvidenceCardinalityEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.AssertedInferenceCardinalityEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ChoiceDescription2EditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ChoiceDescriptionEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ChoiceOptionality2EditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ChoiceOptionalityEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ClaimDescriptionEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.ClaimIdEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.InformationElementCitationDescriptionEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.edit.parts.InformationElementCitationIdEditPart;
import org.eclipse.opencert.sam.arg.arg.diagram.parsers.MessageFormatParser;
import org.eclipse.opencert.sam.arg.arg.diagram.part.ArgVisualIDRegistry;

/**
 * @generated
 */
public class ArgParserProvider extends AbstractProvider implements
		IParserProvider {

	/**
	 * @generated
	 */
	private IParser claimId_5003Parser;

	/**
	 * @generated
	 */
	private IParser getClaimId_5003Parser() {
		if (claimId_5003Parser == null) {
			EAttribute[] features = new EAttribute[] { ArgPackage.eINSTANCE
					.getModelElement_Id() };
			MessageFormatParser parser = new MessageFormatParser(features);
			claimId_5003Parser = parser;
		}
		return claimId_5003Parser;
	}

	/**
	 * @generated
	 */
	private IParser claimDescription_5004Parser;

	/**
	 * @generated
	 */
	private IParser getClaimDescription_5004Parser() {
		if (claimDescription_5004Parser == null) {
			EAttribute[] features = new EAttribute[] { ArgPackage.eINSTANCE
					.getArgumentationElement_Description() };
			MessageFormatParser parser = new MessageFormatParser(features);
			claimDescription_5004Parser = parser;
		}
		return claimDescription_5004Parser;
	}

	/**
	 * @generated
	 */
	private IParser agreementId_5005Parser;

	/**
	 * @generated
	 */
	private IParser getAgreementId_5005Parser() {
		if (agreementId_5005Parser == null) {
			EAttribute[] features = new EAttribute[] { ArgPackage.eINSTANCE
					.getModelElement_Id() };
			MessageFormatParser parser = new MessageFormatParser(features);
			agreementId_5005Parser = parser;
		}
		return agreementId_5005Parser;
	}

	/**
	 * @generated
	 */
	private IParser agreementDescription_5006Parser;

	/**
	 * @generated
	 */
	private IParser getAgreementDescription_5006Parser() {
		if (agreementDescription_5006Parser == null) {
			EAttribute[] features = new EAttribute[] { ArgPackage.eINSTANCE
					.getArgumentationElement_Description() };
			MessageFormatParser parser = new MessageFormatParser(features);
			agreementDescription_5006Parser = parser;
		}
		return agreementDescription_5006Parser;
	}

	/**
	 * @generated
	 */
	private IParser argumentReasoningId_5007Parser;

	/**
	 * @generated
	 */
	private IParser getArgumentReasoningId_5007Parser() {
		if (argumentReasoningId_5007Parser == null) {
			EAttribute[] features = new EAttribute[] { ArgPackage.eINSTANCE
					.getModelElement_Id() };
			MessageFormatParser parser = new MessageFormatParser(features);
			argumentReasoningId_5007Parser = parser;
		}
		return argumentReasoningId_5007Parser;
	}

	/**
	 * @generated
	 */
	private IParser argumentReasoningDescription_5008Parser;

	/**
	 * @generated
	 */
	private IParser getArgumentReasoningDescription_5008Parser() {
		if (argumentReasoningDescription_5008Parser == null) {
			EAttribute[] features = new EAttribute[] { ArgPackage.eINSTANCE
					.getArgumentationElement_Description() };
			MessageFormatParser parser = new MessageFormatParser(features);
			argumentReasoningDescription_5008Parser = parser;
		}
		return argumentReasoningDescription_5008Parser;
	}

	/**
	 * @generated
	 */
	private IParser argumentationId_5009Parser;

	/**
	 * @generated
	 */
	private IParser getArgumentationId_5009Parser() {
		if (argumentationId_5009Parser == null) {
			EAttribute[] features = new EAttribute[] { ArgPackage.eINSTANCE
					.getModelElement_Id() };
			MessageFormatParser parser = new MessageFormatParser(features);
			argumentationId_5009Parser = parser;
		}
		return argumentationId_5009Parser;
	}

	/**
	 * @generated
	 */
	private IParser argumentationDescription_5010Parser;

	/**
	 * @generated
	 */
	private IParser getArgumentationDescription_5010Parser() {
		if (argumentationDescription_5010Parser == null) {
			EAttribute[] features = new EAttribute[] { ArgPackage.eINSTANCE
					.getArgumentationElement_Description() };
			MessageFormatParser parser = new MessageFormatParser(features);
			argumentationDescription_5010Parser = parser;
		}
		return argumentationDescription_5010Parser;
	}

	/**
	 * @generated
	 */
	private IParser informationElementCitationId_5011Parser;

	/**
	 * @generated
	 */
	private IParser getInformationElementCitationId_5011Parser() {
		if (informationElementCitationId_5011Parser == null) {
			EAttribute[] features = new EAttribute[] { ArgPackage.eINSTANCE
					.getModelElement_Id() };
			MessageFormatParser parser = new MessageFormatParser(features);
			informationElementCitationId_5011Parser = parser;
		}
		return informationElementCitationId_5011Parser;
	}

	/**
	 * @generated
	 */
	private IParser informationElementCitationDescription_5012Parser;

	/**
	 * @generated
	 */
	private IParser getInformationElementCitationDescription_5012Parser() {
		if (informationElementCitationDescription_5012Parser == null) {
			EAttribute[] features = new EAttribute[] { ArgPackage.eINSTANCE
					.getArgumentationElement_Description() };
			MessageFormatParser parser = new MessageFormatParser(features);
			informationElementCitationDescription_5012Parser = parser;
		}
		return informationElementCitationDescription_5012Parser;
	}

	/**
	 * @generated
	 */
	private IParser argumentElementCitationId_5013Parser;

	/**
	 * @generated
	 */
	private IParser getArgumentElementCitationId_5013Parser() {
		if (argumentElementCitationId_5013Parser == null) {
			EAttribute[] features = new EAttribute[] { ArgPackage.eINSTANCE
					.getModelElement_Id() };
			MessageFormatParser parser = new MessageFormatParser(features);
			argumentElementCitationId_5013Parser = parser;
		}
		return argumentElementCitationId_5013Parser;
	}

	/**
	 * @generated
	 */
	private IParser argumentElementCitationDescription_5014Parser;

	/**
	 * @generated
	 */
	private IParser getArgumentElementCitationDescription_5014Parser() {
		if (argumentElementCitationDescription_5014Parser == null) {
			EAttribute[] features = new EAttribute[] { ArgPackage.eINSTANCE
					.getArgumentationElement_Description() };
			MessageFormatParser parser = new MessageFormatParser(features);
			argumentElementCitationDescription_5014Parser = parser;
		}
		return argumentElementCitationDescription_5014Parser;
	}

	/**
	 * @generated
	 */
	private IParser argumentElementCitationArgumentationReference_5015Parser;

	/**
	 * @generated
	 */
	private IParser getArgumentElementCitationArgumentationReference_5015Parser() {
		if (argumentElementCitationArgumentationReference_5015Parser == null) {
			EAttribute[] features = new EAttribute[] { ArgPackage.eINSTANCE
					.getArgumentElementCitation_ArgumentationReference() };
			MessageFormatParser parser = new MessageFormatParser(features);
			argumentElementCitationArgumentationReference_5015Parser = parser;
		}
		return argumentElementCitationArgumentationReference_5015Parser;
	}

	/**
	 * @generated
	 */
	private IParser choiceOptionality_5016Parser;

	/**
	 * @generated
	 */
	private IParser getChoiceOptionality_5016Parser() {
		if (choiceOptionality_5016Parser == null) {
			EAttribute[] features = new EAttribute[] { ArgPackage.eINSTANCE
					.getChoice_Optionality() };
			MessageFormatParser parser = new MessageFormatParser(features);
			choiceOptionality_5016Parser = parser;
		}
		return choiceOptionality_5016Parser;
	}

	/**
	 * @generated
	 */
	private IParser choiceDescription_5017Parser;

	/**
	 * @generated
	 */
	private IParser getChoiceDescription_5017Parser() {
		if (choiceDescription_5017Parser == null) {
			EAttribute[] features = new EAttribute[] { ArgPackage.eINSTANCE
					.getArgumentationElement_Description() };
			MessageFormatParser parser = new MessageFormatParser(features);
			choiceDescription_5017Parser = parser;
		}
		return choiceDescription_5017Parser;
	}

	/**
	 * @generated
	 */
	private IParser choiceOptionality_5001Parser;

	/**
	 * @generated
	 */
	private IParser getChoiceOptionality_5001Parser() {
		if (choiceOptionality_5001Parser == null) {
			EAttribute[] features = new EAttribute[] { ArgPackage.eINSTANCE
					.getChoice_Optionality() };
			MessageFormatParser parser = new MessageFormatParser(features);
			choiceOptionality_5001Parser = parser;
		}
		return choiceOptionality_5001Parser;
	}

	/**
	 * @generated
	 */
	private IParser choiceDescription_5002Parser;

	/**
	 * @generated
	 */
	private IParser getChoiceDescription_5002Parser() {
		if (choiceDescription_5002Parser == null) {
			EAttribute[] features = new EAttribute[] { ArgPackage.eINSTANCE
					.getArgumentationElement_Description() };
			MessageFormatParser parser = new MessageFormatParser(features);
			choiceDescription_5002Parser = parser;
		}
		return choiceDescription_5002Parser;
	}

	/**
	 * @generated
	 */
	private IParser assertedInferenceCardinality_6001Parser;

	/**
	 * @generated
	 */
	private IParser getAssertedInferenceCardinality_6001Parser() {
		if (assertedInferenceCardinality_6001Parser == null) {
			EAttribute[] features = new EAttribute[] { ArgPackage.eINSTANCE
					.getAssertedInference_Cardinality() };
			MessageFormatParser parser = new MessageFormatParser(features);
			assertedInferenceCardinality_6001Parser = parser;
		}
		return assertedInferenceCardinality_6001Parser;
	}

	/**
	 * @generated
	 */
	private IParser assertedEvidenceCardinality_6002Parser;

	/**
	 * @generated
	 */
	private IParser getAssertedEvidenceCardinality_6002Parser() {
		if (assertedEvidenceCardinality_6002Parser == null) {
			EAttribute[] features = new EAttribute[] { ArgPackage.eINSTANCE
					.getAssertedEvidence_Cardinality() };
			MessageFormatParser parser = new MessageFormatParser(features);
			assertedEvidenceCardinality_6002Parser = parser;
		}
		return assertedEvidenceCardinality_6002Parser;
	}

	/**
	 * @generated
	 */
	private IParser assertedContextCardinality_6003Parser;

	/**
	 * @generated
	 */
	private IParser getAssertedContextCardinality_6003Parser() {
		if (assertedContextCardinality_6003Parser == null) {
			EAttribute[] features = new EAttribute[] { ArgPackage.eINSTANCE
					.getAssertedContext_Cardinality() };
			MessageFormatParser parser = new MessageFormatParser(features);
			assertedContextCardinality_6003Parser = parser;
		}
		return assertedContextCardinality_6003Parser;
	}

	/**
	 * @generated
	 */
	protected IParser getParser(int visualID) {
		switch (visualID) {
		case ClaimIdEditPart.VISUAL_ID:
			return getClaimId_5003Parser();
		case ClaimDescriptionEditPart.VISUAL_ID:
			return getClaimDescription_5004Parser();
		case AgreementIdEditPart.VISUAL_ID:
			return getAgreementId_5005Parser();
		case AgreementDescriptionEditPart.VISUAL_ID:
			return getAgreementDescription_5006Parser();
		case ArgumentReasoningIdEditPart.VISUAL_ID:
			return getArgumentReasoningId_5007Parser();
		case ArgumentReasoningDescriptionEditPart.VISUAL_ID:
			return getArgumentReasoningDescription_5008Parser();
		case ArgumentationIdEditPart.VISUAL_ID:
			return getArgumentationId_5009Parser();
		case ArgumentationDescriptionEditPart.VISUAL_ID:
			return getArgumentationDescription_5010Parser();
		case InformationElementCitationIdEditPart.VISUAL_ID:
			return getInformationElementCitationId_5011Parser();
		case InformationElementCitationDescriptionEditPart.VISUAL_ID:
			return getInformationElementCitationDescription_5012Parser();
		case ArgumentElementCitationIdEditPart.VISUAL_ID:
			return getArgumentElementCitationId_5013Parser();
		case ArgumentElementCitationDescriptionEditPart.VISUAL_ID:
			return getArgumentElementCitationDescription_5014Parser();
		case ArgumentElementCitationArgumentationReferenceEditPart.VISUAL_ID:
			return getArgumentElementCitationArgumentationReference_5015Parser();
		case ChoiceOptionalityEditPart.VISUAL_ID:
			return getChoiceOptionality_5016Parser();
		case ChoiceDescriptionEditPart.VISUAL_ID:
			return getChoiceDescription_5017Parser();
		case ChoiceOptionality2EditPart.VISUAL_ID:
			return getChoiceOptionality_5001Parser();
		case ChoiceDescription2EditPart.VISUAL_ID:
			return getChoiceDescription_5002Parser();
		case AssertedInferenceCardinalityEditPart.VISUAL_ID:
			return getAssertedInferenceCardinality_6001Parser();
		case AssertedEvidenceCardinalityEditPart.VISUAL_ID:
			return getAssertedEvidenceCardinality_6002Parser();
		case AssertedContextCardinalityEditPart.VISUAL_ID:
			return getAssertedContextCardinality_6003Parser();
		}
		return null;
	}

	/**
	 * Utility method that consults ParserService
	 * @generated
	 */
	public static IParser getParser(IElementType type, EObject object,
			String parserHint) {
		return ParserService.getInstance().getParser(
				new HintAdapter(type, object, parserHint));
	}

	/**
	 * @generated
	 */
	public IParser getParser(IAdaptable hint) {
		String vid = (String) hint.getAdapter(String.class);
		if (vid != null) {
			return getParser(ArgVisualIDRegistry.getVisualID(vid));
		}
		View view = (View) hint.getAdapter(View.class);
		if (view != null) {
			return getParser(ArgVisualIDRegistry.getVisualID(view));
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean provides(IOperation operation) {
		if (operation instanceof GetParserOperation) {
			IAdaptable hint = ((GetParserOperation) operation).getHint();
			if (ArgElementTypes.getElement(hint) == null) {
				return false;
			}
			return getParser(hint) != null;
		}
		return false;
	}

	/**
	 * @generated
	 */
	private static class HintAdapter extends ParserHintAdapter {

		/**
		 * @generated
		 */
		private final IElementType elementType;

		/**
		 * @generated
		 */
		public HintAdapter(IElementType type, EObject object, String parserHint) {
			super(object, parserHint);
			assert type != null;
			elementType = type;
		}

		/**
		 * @generated
		 */
		public Object getAdapter(Class adapter) {
			if (IElementType.class.equals(adapter)) {
				return elementType;
			}
			return super.getAdapter(adapter);
		}
	}

}
