/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.part;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeConnectionTool;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeCreationTool;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.opencert.sam.arg.arg.diagram.providers.ArgElementTypes;

/**
 * @generated
 */
public class ArgPaletteFactory {

	/**
	 * @generated
	 */
	public void fillPalette(PaletteRoot paletteRoot) {
		paletteRoot.add(createArgumentationcore1Group());
		paletteRoot.add(createArgumentationrelationships2Group());
		paletteRoot.add(createArgumentationmodularextensions3Group());
	}

	/**
	 * Creates "Argumentation core" palette tool group
	 * ARL perform changes to generated code to follow GSN namin
	 */
	private PaletteContainer createArgumentationcore1Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				Messages.Argumentationcore1Group_title);
		paletteContainer.setId("createArgumentationcore1Group"); //$NON-NLS-1$
		paletteContainer.add(createArgumentReasoning1CreationTool());
		//paletteContainer.add(createArgumentation2CreationTool());
		paletteContainer.add(createClaim3CreationTool());
		paletteContainer.add(createAssumption3CreationTool());
//		paletteContainer.add(createInformationElementCitation4CreationTool());
		paletteContainer.add(createSolution4CreationTool());
		paletteContainer.add(createContext4CreationTool());
		paletteContainer.add(createJustification4CreationTool());
		return paletteContainer;
	}

	/**
	 * Creates "Argumentation relationships" palette tool group
	 * @generated
	 */
	private PaletteContainer createArgumentationrelationships2Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				Messages.Argumentationrelationships2Group_title);
		paletteContainer.setId("createArgumentationrelationships2Group"); //$NON-NLS-1$
		paletteContainer.add(createAssertedChallenge1CreationTool());
		paletteContainer.add(createAssertedContext2CreationTool());
		paletteContainer.add(createAssertedCounterEvidence3CreationTool());
		paletteContainer.add(createAssertedEvidence4CreationTool());
		paletteContainer.add(createAssertedInference5CreationTool());
		paletteContainer.add(createChoice6CreationTool());
		return paletteContainer;
	}

	/**
	 * Creates "Argumentation modular extensions" palette tool group
	 * @generated
	 */
	private PaletteContainer createArgumentationmodularextensions3Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				Messages.Argumentationmodularextensions3Group_title);
		paletteContainer.setId("createArgumentationmodularextensions3Group"); //$NON-NLS-1$
		paletteContainer.add(createArgumentation2CreationTool());
//		paletteContainer.add(createArgumentElementCitation1CreationTool());
		paletteContainer.add(createAwayGoal1CreationTool());
		paletteContainer.add(createAwayContext1CreationTool());
		paletteContainer.add(createAwaySolution1CreationTool());
		paletteContainer.add(createAgreement2CreationTool());
		return paletteContainer;
	}

	/**
	 * @generated
	 */
	private ToolEntry createArgumentReasoning1CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.ArgumentReasoning1CreationTool_title,
				Messages.ArgumentReasoning1CreationTool_desc,
				Collections
						.singletonList(ArgElementTypes.ArgumentReasoning_2003));
		entry.setId("createArgumentReasoning1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/Strategy.gif")); //$NON-NLS-1$
		entry.setLargeIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/Strategy.gif")); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createArgumentation2CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Argumentation2CreationTool_title,
				Messages.Argumentation2CreationTool_desc,
				Collections.singletonList(ArgElementTypes.Argumentation_2004));
		entry.setId("createArgumentation2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/ArgumentModule.gif")); //$NON-NLS-1$
		entry.setLargeIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/ArgumentModule.gif")); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createClaim3CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Claim3CreationTool_title,
				Messages.Claim3CreationTool_desc,
				Collections.singletonList(ArgElementTypes.Claim_2001));
		entry.setId("createClaim3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/Goal.gif")); //$NON-NLS-1$
		entry.setLargeIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/Goal.gif")); //$NON-NLS-1$
		return entry;
	}
	/**
	 * ARL changes
	 * Includes assumption
	 * @return
	 */
	private ToolEntry createAssumption3CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Assumption3CreationTool_title,
				Messages.Assumption3CreationTool_desc,
				//ARL 
				Collections.singletonList(ArgElementTypes.Assumption_2000));
		entry.setId("createAssumption3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/Assumption.gif")); //$NON-NLS-1$
		entry.setLargeIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/Assumption.gif")); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
//	private ToolEntry createInformationElementCitation4CreationTool() {
//		NodeToolEntry entry = new NodeToolEntry(
//				Messages.InformationElementCitation4CreationTool_title,
//				Messages.InformationElementCitation4CreationTool_desc,
//				Collections
//						.singletonList(ArgElementTypes.InformationElementCitation_2005));
//		entry.setId("createInformationElementCitation4CreationTool"); //$NON-NLS-1$
//		entry.setSmallIcon(ArgDiagramEditorPlugin
//				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/Solution.gif")); //$NON-NLS-1$
//		entry.setLargeIcon(ArgDiagramEditorPlugin
//				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/Solution.gif")); //$NON-NLS-1$
//		return entry;
//	}
	private ToolEntry createSolution4CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Solution4CreationTool_title,
				Messages.Solution4CreationTool_desc,
				Collections
						.singletonList(ArgElementTypes.Solution_3004));
		entry.setId("createInformationElementCitation4CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/Solution.gif")); //$NON-NLS-1$
		entry.setLargeIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/Solution.gif")); //$NON-NLS-1$
		return entry;
	}
	/**
	 * Creates a context
	 */
	private ToolEntry createContext4CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Context4CreationTool_title,
				Messages.Context4CreationTool_desc,
				Collections
						.singletonList(ArgElementTypes.Context_3003));
		entry.setId("createContext4CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/Context.gif")); //$NON-NLS-1$
		entry.setLargeIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/Context.gif")); //$NON-NLS-1$
		return entry;
	}
	/**
	 * Creates a Justification
	 */
	private ToolEntry createJustification4CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Justification4CreationTool_title,
				Messages.Justification4CreationTool_desc,
				Collections
						.singletonList(ArgElementTypes.Justification_3002));
		entry.setId("createJustification4CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/Justification.gif")); //$NON-NLS-1$
		entry.setLargeIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/Justification.gif")); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAssertedChallenge1CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.AssertedChallenge1CreationTool_title,
				Messages.AssertedChallenge1CreationTool_desc,
				Collections
						.singletonList(ArgElementTypes.AssertedChallenge_4004));
		entry.setId("createAssertedChallenge1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/SACEM_tooling_icons/Challenge.gif")); //$NON-NLS-1$
		entry.setLargeIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/SACEM_tooling_icons/Challenge.gif")); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAssertedContext2CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.AssertedContext2CreationTool_title,
				Messages.AssertedContext2CreationTool_desc,
				Collections.singletonList(ArgElementTypes.AssertedContext_4003));
		entry.setId("createAssertedContext2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/inTheContextOf.gif")); //$NON-NLS-1$
		entry.setLargeIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/inTheContextOf.gif")); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAssertedCounterEvidence3CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.AssertedCounterEvidence3CreationTool_title,
				Messages.AssertedCounterEvidence3CreationTool_desc,
				Collections
						.singletonList(ArgElementTypes.AssertedCounterEvidence_4005));
		entry.setId("createAssertedCounterEvidence3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/SACEM_tooling_icons/CounterEvidence.gif")); //$NON-NLS-1$
		entry.setLargeIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/SACEM_tooling_icons/CounterEvidence.gif")); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAssertedEvidence4CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.AssertedEvidence4CreationTool_title,
				Messages.AssertedEvidence4CreationTool_desc,
				Collections
						.singletonList(ArgElementTypes.AssertedEvidence_4002));
		entry.setId("createAssertedEvidence4CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/solvedBy.gif")); //$NON-NLS-1$
		entry.setLargeIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/solvedBy.gif")); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAssertedInference5CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.AssertedInference5CreationTool_title,
				Messages.AssertedInference5CreationTool_desc,
				Collections
						.singletonList(ArgElementTypes.AssertedInference_4001));
		entry.setId("createAssertedInference5CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/solvedBy.gif")); //$NON-NLS-1$
		entry.setLargeIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/solvedBy.gif")); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createChoice6CreationTool() {
		ArrayList<IElementType> types = new ArrayList<IElementType>(2);
		types.add(ArgElementTypes.Choice_3001);
		types.add(ArgElementTypes.Choice_2007);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Choice6CreationTool_title,
				Messages.Choice6CreationTool_desc, types);
		entry.setId("createChoice6CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/Optionality.gif")); //$NON-NLS-1$
		entry.setLargeIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/Optionality.gif")); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
//	private ToolEntry createArgumentElementCitation1CreationTool() {
//		NodeToolEntry entry = new NodeToolEntry(
//				Messages.ArgumentElementCitation1CreationTool_title,
//				Messages.ArgumentElementCitation1CreationTool_desc,
//				Collections
//						.singletonList(ArgElementTypes.ArgumentElementCitation_2006));
//		entry.setId("createArgumentElementCitation1CreationTool"); //$NON-NLS-1$
//		entry.setSmallIcon(ArgDiagramEditorPlugin
//				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/ArgumentModule.gif")); //$NON-NLS-1$
//		entry.setLargeIcon(ArgDiagramEditorPlugin
//				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/ArgumentModule.gif")); //$NON-NLS-1$
//		return entry;
//	}
	/**
	 * ARL changes
	 * Includes createAway*1CreationTool methods
	 * @return
	 */
	private ToolEntry createAwayGoal1CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AwayGoal1CreationTool_title,
				Messages.AwayGoal1CreationTool_desc,
				Collections
						.singletonList(ArgElementTypes.AwayClaim_5001));
		entry.setId("createAwayGoal1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/AwayGoal.gif")); //$NON-NLS-1$
		entry.setLargeIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/AwayGoal.gif")); //$NON-NLS-1$
		return entry;
	}
	private ToolEntry createAwayContext1CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AwayContext1CreationTool_title,
				Messages.AwayContext1CreationTool_desc,
				Collections
						.singletonList(ArgElementTypes.AwayContext_5002));
		entry.setId("createAwayGoal1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/AwayContext.gif")); //$NON-NLS-1$
		entry.setLargeIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/AwayContext.gif")); //$NON-NLS-1$
		return entry;
	}
	private ToolEntry createAwaySolution1CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AwaySolution1CreationTool_title,
				Messages.AwaySolution1CreationTool_desc,
				Collections
						.singletonList(ArgElementTypes.AwaySolution_5003));
		entry.setId("createAwayGoal1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/AwaySolution.gif")); //$NON-NLS-1$
		entry.setLargeIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/AwaySolution.gif")); //$NON-NLS-1$
		return entry;
	}
	/**
	 * @generated
	 */
	private ToolEntry createAgreement2CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Agreement2CreationTool_title,
				Messages.Agreement2CreationTool_desc,
				Collections.singletonList(ArgElementTypes.Agreement_2002));
		entry.setId("createAgreement2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/Contract.gif")); //$NON-NLS-1$
		entry.setLargeIcon(ArgDiagramEditorPlugin
				.findImageDescriptor("/org.eclipse.opencert.sam.arg/GSN_tooling_icons/Contract.gif")); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private static class NodeToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> elementTypes;

		/**
		 * @generated
		 */
		private NodeToolEntry(String title, String description,
				List<IElementType> elementTypes) {
			super(title, description, null, null);
			this.elementTypes = elementTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeCreationTool(elementTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}

	/**
	 * @generated
	 */
	private static class LinkToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> relationshipTypes;

		/**
		 * @generated
		 */
		private LinkToolEntry(String title, String description,
				List<IElementType> relationshipTypes) {
			super(title, description, null, null);
			this.relationshipTypes = relationshipTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeConnectionTool(relationshipTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}
}
