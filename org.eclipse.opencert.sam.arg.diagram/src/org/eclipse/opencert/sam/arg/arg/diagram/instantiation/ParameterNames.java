package org.eclipse.opencert.sam.arg.arg.diagram.instantiation;

public interface ParameterNames {
	public static final String commandID = "org.eclipse.opencert.arg.diagram.contextmenu.command.instantiate";

	public static final String itemBody = "org.eclipse.opencert.sam.arg.diagram.instantiante.command.itemBody";
	public static final String replacement = "org.eclipse.opencert.sam.arg.diagram.instantiante.command.replacement";
	public static final String commandName = "org.eclipse.opencert.sam.arg.diagram.instantiante.command.commandName";
	public static final String editorHash = "org.eclipse.opencert.sam.arg.diagram.instantiante.command.editorHash";
}
