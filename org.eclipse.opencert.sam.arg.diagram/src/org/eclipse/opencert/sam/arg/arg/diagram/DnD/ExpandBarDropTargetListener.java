/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.DnD;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.edit.ui.dnd.LocalTransfer;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.Request;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramDropTargetListener;
import org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.TransferData;

/**
 * 
 * @author M� Carmen Palacios
 */
public class ExpandBarDropTargetListener extends DiagramDropTargetListener {

	public ExpandBarDropTargetListener(EditPartViewer viewer) {
		super(viewer, LocalTransfer.getInstance());
	}

    protected Object getJavaObject(TransferData data) {
        return LocalTransfer.getInstance().nativeToJava(data);
    }

    @Override
    protected List getObjectsBeingDropped() {
        /*  Get the selection from the transfer agent */
        List fileList = new ArrayList();
        
        //fileList.add(LocalSelectionTransfer.getTransfer().getSelection());
        TransferData data = getCurrentEvent().currentDataType;
        Object transferedObject = getJavaObject(data);

				Object nextSelectedObject = transferedObject;
				System.out.println("getObjectsBeingDropped nextSelectedObject ="+ nextSelectedObject.toString());
				if(nextSelectedObject instanceof String)
				{
					if(((String) nextSelectedObject).compareTo("") != 0) fileList.add(nextSelectedObject.toString());
			    }


       
        //System.out.println("ObjectsBeingDropped:"+fileList);
       
        if (fileList.size() > 0)
            return fileList;
       
        return null;
    }
/*
	@Override
	protected void handleDrop() {
		DiagramEditor editor = (DiagramEditor) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		
		CreateViewRequest createRequest = CreateViewRequestFactory.getCreateShapeRequest(MolicElementTypes.Sketch_3001,editor.getDiagramEditPart().getDiagramPreferencesHint());
		//System.out.println(createRequest.getNewObject());
		
		createRequest.setLocation(getDropLocation());
		
		
		Command c = getTargetEditPart().getCommand(createRequest);
		//getViewer().getEditDomain().getCommandStack().execute(c);
		
		

		super.handleDrop();
	}
*/
	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.gmf.runtime.diagram.ui.parts.DiagramDropTargetListener#
	 * createTargetRequest()
	 */
	/**
	 * Overrides the default request creation by adding it a MOVE operation in
	 * case that the selection is valid. FIXME if only I knew why this is needed
	 * !!!!!
	 */
	@Override
	protected Request createTargetRequest() {
		DropObjectsRequest req = (DropObjectsRequest) super
				.createTargetRequest();
		req.setRequiredDetail(DND.DROP_COPY);
		
		return req;
	}
}