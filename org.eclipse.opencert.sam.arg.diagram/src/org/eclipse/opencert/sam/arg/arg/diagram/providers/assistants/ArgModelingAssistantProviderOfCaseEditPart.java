/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.sam.arg.arg.diagram.providers.assistants;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.opencert.sam.arg.arg.diagram.providers.ArgElementTypes;
import org.eclipse.opencert.sam.arg.arg.diagram.providers.ArgModelingAssistantProvider;

/**
 * @generated
 */
public class ArgModelingAssistantProviderOfCaseEditPart extends
		ArgModelingAssistantProvider {

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForPopupBar(IAdaptable host) {
		List<IElementType> types = new ArrayList<IElementType>(7);
		types.add(ArgElementTypes.Claim_2001);
		types.add(ArgElementTypes.Agreement_2002);
		types.add(ArgElementTypes.ArgumentReasoning_2003);
		types.add(ArgElementTypes.Argumentation_2004);
		types.add(ArgElementTypes.InformationElementCitation_2005);
		types.add(ArgElementTypes.ArgumentElementCitation_2006);
		types.add(ArgElementTypes.Choice_2007);
		return types;
	}

}
