/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.evm.evidspec.presentation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.cdo.CDOObject;
import org.eclipse.emf.cdo.common.id.CDOID;
import org.eclipse.emf.cdo.dawn.editors.IDawnEditor;
import org.eclipse.emf.cdo.dawn.editors.IDawnEditorSupport;
import org.eclipse.emf.cdo.dawn.emf.editors.impl.DawnEMFEditorSupport;
import org.eclipse.emf.cdo.dawn.ui.DawnEditorInput;
import org.eclipse.emf.cdo.dawn.ui.DawnLabelProvider;
import org.eclipse.emf.cdo.dawn.ui.DawnSelectionViewerAdapterFactoryContentProvider;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CDOUtil;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.ui.ViewerPane;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind;
import org.eclipse.opencert.apm.assurproj.utils.widget.CheckboxTreeViewerExt;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.dialogs.CheckedTreeSelectionDialog;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.presentation.EvidenceEditor;
import org.eclipse.opencert.evm.evidspec.evidence.util.ArtefactModificationController;
import org.eclipse.opencert.impactanalysis.ImpactAnalyser;
import org.eclipse.opencert.impactanalysis.ImpactSource;
import org.eclipse.opencert.impactanalysis.relations.IArtefactRelationImpact;

public class DawnEvidenceEditor extends EvidenceEditor implements IDawnEditor {
	private static final String SELECT_THE_NODES_FROM_THE_TREE_TO_CONFIRM_THE_CHANGES = "Select the nodes from the tree to confirm the changes.";

	private static final String IMPACT_TREE_SELECTION = "IMPACT ANALYSER Confirmation";

	private static final String CONFIRM = "Confirm";

	private IDawnEditorSupport dawnEditorSupport;
	
	public static String ID = "org.eclipse.opencert.evm.evidspec.presentation.DawnEvidenceEditorID";


	public DawnEvidenceEditor() {
		super();
		dawnEditorSupport = new DawnEMFEditorSupport(this);
	}

	@Override
	protected void setInput(IEditorInput input) {
		super.setInput(input);
		if (input instanceof DawnEditorInput) {
			dawnEditorSupport.setView(((DawnEditorInput) input).getView());
			dawnEditorSupport.registerListeners();
		}
	}

	@Override
	protected void setInputWithNotify(IEditorInput input) {
		super.setInput(input);
		if (input instanceof DawnEditorInput) {
			CDOResource resource = ((DawnEditorInput) input).getResource();
			URI uri = URI.createURI(((DawnEditorInput) input).getURI()
					.toString());

			if (resource == null || resource.cdoView() == null) {
				ResourceSet resourceSet = editingDomain.getResourceSet();
				CDOTransaction transaction = CDOConnectionUtil.instance
						.openCurrentTransaction(resourceSet, uri.toString());

				resource = (CDOResource) resourceSet.getResource(uri, true);

				if (resource == null || resource.cdoView() == null) {
					resource = transaction.getOrCreateResource(uri.toString());
				}
			}

			((DawnEditorInput) input).setResource(resource);
			dawnEditorSupport.setView(((DawnEditorInput) input).getView());
			dawnEditorSupport.registerListeners();
		}
	}

	@Override
	public void createPages() {
		super.createPages();
		
		if (!(getEditorInput() instanceof DawnEditorInput)) {
			return;
		}

		selectionViewer
				.setContentProvider(new DawnSelectionViewerAdapterFactoryContentProvider(
						adapterFactory, ((DawnEditorInput) getEditorInput())
								.getResource()));
		selectionViewer.setLabelProvider(new DawnLabelProvider(adapterFactory,
				dawnEditorSupport.getView(), selectionViewer));
		parentViewer.setLabelProvider(new DawnLabelProvider(adapterFactory,
				dawnEditorSupport.getView(), selectionViewer));
		listViewer.setLabelProvider(new DawnLabelProvider(adapterFactory,
				dawnEditorSupport.getView(), selectionViewer));
		treeViewer.setLabelProvider(new DawnLabelProvider(adapterFactory,
				dawnEditorSupport.getView(), selectionViewer));
		tableViewer.setLabelProvider(new DawnLabelProvider(adapterFactory,
				dawnEditorSupport.getView(), selectionViewer));
		treeViewerWithCheck.setLabelProvider(new DawnLabelProvider(
				adapterFactory, dawnEditorSupport.getView(), selectionViewer));

		CDOResource resource = ((DawnEditorInput) getEditorInput())
				.getResource();

		selectionViewer.setInput(resource.getResourceSet());
		selectionViewer.setSelection(new StructuredSelection(resource), true);

		parentViewer
				.setContentProvider(new ReverseAdapterFactoryContentProvider(
						adapterFactory));
	}

	@Override
	public void doSave(IProgressMonitor progressMonitor) {
		CDOView view = dawnEditorSupport.getView();					
		
		if (view instanceof CDOTransaction) {
			if (view.hasConflict()) {
				MessageDialog.openError(Display.getDefault().getActiveShell(),
						"conflict",
						"Your Resource is in conflict and cannot be committed");
			} else {
				super.doSave(progressMonitor);				
				callImpactAnalyser();							
			}
		} else {
			super.doSave(progressMonitor);
			callImpactAnalyser();
		}
	}

	private void callImpactAnalyser() {
		ArtefactModificationController amc = ArtefactModificationController.getInstance();
		ImpactAnalyser impactAnalyser = new ImpactAnalyser(new EclipseOpencertClientConfigurationAdapter());
		List<ImpactSource> impactSources = new ArrayList<ImpactSource>();		
		
		try {
		    
		    for (Iterator<Artefact> iter=amc.getModifiedArtefacts().iterator();iter.hasNext();) {     
                Artefact modArtefact = iter.next();         
                 if(modArtefact instanceof EObject){
                                                                            
                    EObject elobjeto = (EObject)modArtefact;
                    CDOObject cdo=CDOUtil.getCDOObject(elobjeto);
                    CDOID id = cdo.cdoID();;
                    Long  initialArtefactCDOId = new Long(id.toURIFragment());                                              
                    
                    //add this artefact to impact analysis later on
                    ImpactSource impactSource = new ImpactSource(initialArtefactCDOId, EventKind.MODIFICATION);
                    impactSources.add(impactSource);
                 }   
                //Remove modified artefact from the list
                iter.remove();
            }
		    
            List<IArtefactRelationImpact> artefactsRelationImpacts =
	                impactAnalyser.listArtefactsRelationImpacts(impactSources);
            
            List<IArtefactRelationImpact> artefactsRelationImpactsExecute= new ArrayList<IArtefactRelationImpact>();
	        
	        if (artefactsRelationImpacts.size()!=0){
		        CheckedTreeSelectionDialog dialog2 = new CheckedTreeSelectionDialog(Display.getDefault().getActiveShell(),new ArtefactsRelationImpactLabelProvider(),new ArtefactsRelationImpactContentProvider(artefactsRelationImpacts)){
		        		  @Override
					public Button getOkButton() {
						// TODO Auto-generated method stub
		        			  super.getOkButton().setText(CONFIRM);	  
						return super.getOkButton();
					}

						@Override
			        	  protected Point getInitialSize() {
		        			  return new Point(1000, 700);
		        		  }
	        	  };
		        
		        dialog2.setTitle(IMPACT_TREE_SELECTION);

		        dialog2.setMessage(SELECT_THE_NODES_FROM_THE_TREE_TO_CONFIRM_THE_CHANGES);
		        
		        	       
		        
		        
		        dialog2.setExpandedElements(artefactsRelationImpacts.toArray());
		        
		        dialog2.setInput(artefactsRelationImpacts);
		        
		        
		        int response = dialog2.open();
		        		        		        
		        if(response==0){
		        	Object [] sele = dialog2.getResult();
		        	
		        	for(Object oneImpact:sele){
		        		if(oneImpact instanceof IArtefactRelationImpact){
		        			artefactsRelationImpactsExecute.add((IArtefactRelationImpact)oneImpact);
		        		}
		        	}
		        	
		        	impactAnalyser.executeArtefactsRelationImpacts(artefactsRelationImpactsExecute);			        				        	
		        } 
		        				       
	        }	        			
		} finally {
		    impactAnalyser.close();
        }  

		//Clear the list evaluated Artefacts
		amc.getEvaluatedArtefacts().clear();
	}

	public String getContributorID() {
		return "org.eclipse.opencert.evm.evidspec.evidence.properties";
	}

	public CDOView getView() {
		return dawnEditorSupport.getView();
	}

	public void setDirty() {
		dawnEditorSupport.setDirty(true);
	}

	@Override
	public void dispose() {
		try {
			super.dispose();
		} finally {
			dawnEditorSupport.close();
		}
	}

	public String getContributorId() {
		return "org.eclipse.opencert.evm.evidspec.evidence.properties";
	}

	public IDawnEditorSupport getDawnEditorSupport() {
		return dawnEditorSupport;
	}
}
