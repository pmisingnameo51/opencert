/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.assuranceproject.etl;


import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.cdo.common.model.EMFUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.common.EMFPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.core.GMFEditingDomainFactory;
import org.eclipse.opencert.apm.assurproj.assuranceproject.provider.AssuranceprojectEditPlugin;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel;
import org.eclipse.opencert.evm.evidspec.evidence.EvidenceFactory;
import org.eclipse.opencert.evm.evidspec.evidence.impl.ArtefactModelImpl;
import org.eclipse.opencert.pam.procspec.process.ProcessFactory;
import org.eclipse.opencert.pam.procspec.process.ProcessModel;
import org.eclipse.opencert.pam.procspec.process.impl.ProcessModelImpl;
import org.eclipse.opencert.sam.arg.arg.ArgFactory;
import org.eclipse.opencert.sam.arg.arg.Case;
import org.eclipse.opencert.sam.arg.arg.impl.CaseImpl;


public class CDOTransformFileHandler {
	// configuration params
	// coming from class AssurProjInitializationHelper 
//	private static final String UTF_8 = "UTF-8";

	
	private final String eviModelURI = "eviModel.evidence";	
	private final String procModelURI = "procModel.process";
	private final String argModelURI = "argModel.arg";
	private String targetModelURI = "";
	
	private final String eviMetamodelFileURI = "model/evidence.ecore";
	private final String procMetamodelFileURI = "model/process.ecore";
	private final String argMetamodelFileURI = "model/arg.ecore";
	private String targetMetamodelFileURI = "";
	
	private static String pluginInWorkSpace = "";
	private static String pluginPath = "";
//	private static String epfMetamodelFile = "";
	
//	private static String eviMetamodelFile = "";
//	private static String procMetamodelFile = "";
//	private static String argMetamodelFile = "";
	protected String targetMetamodelFile = "";

//	private CDOResource eviModel;
//	private CDOResource processModel;
//	private CDOResource argModel;
	private CDOResource targetModel;
	
	protected CDOResourceFolder project;
	protected EMFPlugin pluginProject;
	private CDOTransaction transaction;	
	
	String modelFilePath="";
	String configurationFilePath="";
	
	// files
//	private String eviModelFile;
//	private String procModelFile;
//	private String argModelFile;
	protected String targetModelFile;
		
//	private Resource eviModelResource = null;
//	private Resource procModelResource = null;
//	private Resource argModelResource = null;
	private Resource targetModelResource = null;
	protected String modelExtension="";
	
	public CDOTransformFileHandler(CDOResourceFolder project, String modelFilePath, CDOResource targetModel, CDOTransaction transaction, String modelExtension) {
		
		try {
			this.project = project; //User created project
			this.pluginProject = AssuranceprojectEditPlugin.INSTANCE;
			this.modelFilePath = modelFilePath;
			this.targetModel=targetModel;
			this.modelExtension=modelExtension;
			if(modelExtension.equals(".arg")){
				this.targetModelURI=this.argModelURI;
				this.targetMetamodelFileURI=this.argMetamodelFileURI;
			}
			if(modelExtension.equals(".evidence")){
				this.targetModelURI=this.eviModelURI;
				this.targetMetamodelFileURI=this.eviMetamodelFileURI;
			}
			if(modelExtension.equals(".process")){
				this.targetModelURI=this.procModelURI;
				this.targetMetamodelFileURI=this.procMetamodelFileURI;
			}

			this.transaction = transaction;

			URL entry = null;
			URL fileURL = null;
			String file  ="";
			try {
				// init config data
				String spluginPath = AssuranceprojectEditPlugin.getPlugin().getBundle().getLocation();
				RedirectSystemErr.out("spluginPath:= ." + spluginPath + ".");
				if(spluginPath.contains("plugins")) // si estoy en modo jar
				{
					entry = AssuranceprojectEditPlugin.getPlugin().getBundle().getEntry(targetMetamodelFileURI);
					fileURL = FileLocator.resolve(entry);
					file = fileURL.getFile();
					RedirectSystemErr.out("file:= ." + file + ".");
					pluginPath = file.substring("file:/".length(), file.indexOf("plugins"))+ "configuration";
					RedirectSystemErr.out("pluginPath:= ." + pluginPath + ".");
					//String sname = Platform.getBundle(Activator.PLUGIN_ID).getSymbolicName();
					String sname = AssuranceprojectEditPlugin.getPlugin().getBundle().getSymbolicName();
					RedirectSystemErr.out("sname:= ." + sname + ".");
					pluginInWorkSpace = "";
				}	
				else
				{
					pluginPath = spluginPath.substring("reference:file:/".length(), spluginPath.length()-1); // eliminar ultimo caracter "/"
					RedirectSystemErr.out("pluginPath:= ." + pluginPath + ".");
					//String sname = Platform.getBundle(Activator.PLUGIN_ID).getSymbolicName();
					String sname = AssuranceprojectEditPlugin.getPlugin().getBundle().getSymbolicName();
					int index0 = pluginPath.lastIndexOf("/", pluginPath.length()-(sname.length()+2));
					pluginInWorkSpace = pluginPath.substring(index0+1, pluginPath.lastIndexOf("/"));
				}
				RedirectSystemErr.out("pluginInWorkSpace:= ." + pluginInWorkSpace + ".");		
				targetMetamodelFile = pluginPath + "/" + targetMetamodelFileURI;

				// path ficheros temporales de modelo input y modelo output
	            String modelsPath = System.getProperty("user.home");
	            RedirectSystemErr.out("modelsPath:= ." + modelsPath + ".");
				
	            targetModelFile = modelsPath  + IPath.SEPARATOR + targetModelURI;
				URI targetModelLocalURI = URI.createFileURI(targetModelFile);

				if(targetModelFile.endsWith(".arg")){
					targetModelResource = createArgumentModel(targetModelLocalURI, null);
				}
				if(targetModelFile.endsWith(".evidence")){
//					eviModelResource = createEviModel(targetModelLocalURI, null);
					targetModelResource = createEviModel(targetModelLocalURI, null);
				}
				if(targetModelFile.endsWith(".process")){
//					procModelResource = createProcModel(targetModelLocalURI, null);
					targetModelResource = createProcModel(targetModelLocalURI, null);
				}
				
			} catch (IOException e) {
				e.printStackTrace();
				RedirectSystemErr.out("CDOTransformHandler Exception 1 ." + e.toString());
			} 
		}
		catch (Exception e)
		{
			e.printStackTrace();
			RedirectSystemErr.out("CDOTransformHandler Exception 3 ." + e.toString());
		}
		
	}
	
	/* copied from BaselineDiagramEditorUtil */
	private void attachModelToResource(EObject model,
			Resource resource) {
		resource.getContents().add(model);
	}
	
	/* copied from BaselineDiagramEditorUtil */	
	private void setCharset(IFile file) {
		if (file == null) {
			return;
		}
		try {
			file.setCharset("UTF-8", new NullProgressMonitor()); //$NON-NLS-1$
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}


	private ArtefactModel createEviInitialModel() {
		return EvidenceFactory.eINSTANCE.createArtefactModel();
	}

	private Resource createEviModel(URI modelURI, final CDOResource modelCDO) {
		TransactionalEditingDomain editingDomain = GMFEditingDomainFactory.INSTANCE
				.createEditingDomain();
		final Resource modelResource = editingDomain.getResourceSet()
				.createResource(modelURI);

		AbstractTransactionalCommand command = new AbstractTransactionalCommand(
				editingDomain,
				"Create Evidence Model File",
				Collections.EMPTY_LIST) {
			protected CommandResult doExecuteWithResult(
					IProgressMonitor monitor, IAdaptable info)
					throws ExecutionException {
				ArtefactModelImpl model = (ArtefactModelImpl)createEviInitialModel();
				if(modelCDO != null)
				{
					model = (ArtefactModelImpl) modelCDO.getContents().get(0);
				}
				attachModelToResource(model, modelResource);

				try {
					modelResource.save(null);
				} catch (IOException e) {

					e.printStackTrace();
				}
				return CommandResult.newOKCommandResult();
			}
		};
		try {
			OperationHistoryFactory.getOperationHistory().execute(command, null, null);
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		setCharset(WorkspaceSynchronizer.getFile(modelResource));
		return modelResource;
	}
	
	
	private ProcessModel createProcInitialModel() {
		return ProcessFactory.eINSTANCE.createProcessModel();
	}

	private Resource createProcModel(URI modelURI, final CDOResource modelCDO) {
		TransactionalEditingDomain editingDomain = GMFEditingDomainFactory.INSTANCE
				.createEditingDomain();
		final Resource modelResource = editingDomain.getResourceSet()
				.createResource(modelURI);

		AbstractTransactionalCommand command = new AbstractTransactionalCommand(
				editingDomain,
				"Create Process Model File",
				Collections.EMPTY_LIST) {
			protected CommandResult doExecuteWithResult(
					IProgressMonitor monitor, IAdaptable info)
					throws ExecutionException {
				ProcessModelImpl model = (ProcessModelImpl)createProcInitialModel();
				if(modelCDO != null)
				{
					model = (ProcessModelImpl) modelCDO.getContents().get(0);
				}
				attachModelToResource(model, modelResource);

				try {
					modelResource.save(null);
				} catch (IOException e) {

					e.printStackTrace();
				}
				return CommandResult.newOKCommandResult();
			}
		};
		try {
			OperationHistoryFactory.getOperationHistory().execute(command, null, null);
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		setCharset(WorkspaceSynchronizer.getFile(modelResource));
		return modelResource;
	}
	private Case createArgumentInitialModel() {
		return ArgFactory.eINSTANCE.createCase();
	}

	private Resource createArgumentModel(URI modelURI, final CDOResource modelCDO) {
		TransactionalEditingDomain editingDomain = GMFEditingDomainFactory.INSTANCE
				.createEditingDomain();
		final Resource modelResource = editingDomain.getResourceSet()
				.createResource(modelURI);

		AbstractTransactionalCommand command = new AbstractTransactionalCommand(
				editingDomain,
				"Create Argument Model File",
				Collections.EMPTY_LIST) {
			protected CommandResult doExecuteWithResult(
					IProgressMonitor monitor, IAdaptable info)
					throws ExecutionException {
				CaseImpl model = (CaseImpl)createArgumentInitialModel();
				if(modelCDO != null)
				{
					model = (CaseImpl) modelCDO.getContents().get(0);
				}
				attachModelToResource(model, modelResource);

				try {
					modelResource.save(null);
				} catch (IOException e) {

					e.printStackTrace();
				}
				return CommandResult.newOKCommandResult();
			}
		};
		try {
			OperationHistoryFactory.getOperationHistory().execute(command, null, null);
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		setCharset(WorkspaceSynchronizer.getFile(modelResource));
		return modelResource;
	}


	// MCP
	public Object execute() throws ExecutionException, IOException {
		try {
		    RedirectSystemErr.out(" before refreshing file content ");
		    // guardar el modelo transformado y disponible en fichero en CDO
		    // primero refrescar contenido creado por rules
//		    if(eviModelResource!=null){
//			if (eviModelResource.isLoaded()) {
//				eviModelResource.unload();
//				try {
//					eviModelResource.load(Collections.EMPTY_MAP);
//				}
//				catch (IOException exception) {
//					exception.printStackTrace();
//				}
//			}}
//
//		    // guardar el modelo transformado y disponible en fichero en CDO
//		    // primero refrescar contenido creado por rules
//		    if(procModelResource!=null){
//			if (procModelResource.isLoaded()) {
//				procModelResource.unload();
//				try {
//					procModelResource.load(Collections.EMPTY_MAP);
//				}
//				catch (IOException exception) {
//					exception.printStackTrace();
//				}
//			}}
		    if (targetModelResource.isLoaded()) {
				targetModelResource.unload();
				try {
					targetModelResource.load(Collections.EMPTY_MAP);
				}
				catch (IOException exception) {
					exception.printStackTrace();
				}
			}
			
		    
		    RedirectSystemErr.out(" before creating CDO Argumentation, Evidence or Process models. ");	    
		    		    
		    URI targetXmiUri = URI.createFileURI(modelFilePath);
		    URI targetCdoURI = targetModel.getURI();
		    
		    
			ResourceSet resourceSet = new ResourceSetImpl();
			resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
			
			Resource targetXmiResource = resourceSet.createResource(targetXmiUri);
			
			FileInputStream sourceFileInStream = new FileInputStream(targetXmiUri.path());
			targetXmiResource.load(sourceFileInStream, Collections.EMPTY_MAP);
			
			targetXmiResource.setURI(targetXmiUri);
			
			EMFUtil.safeResolveAll(resourceSet);
           
			CDOResource targetCdoResource = transaction.getOrCreateResource(targetCdoURI.path());
			targetCdoResource.getContents().addAll(targetXmiResource.getContents());
						
			EMFUtil.safeResolveAll(resourceSet);

			targetCdoResource.save(Collections.EMPTY_MAP);
			
			transaction.commit();
    
		} catch (Exception e) {
			e.printStackTrace();
		}
	    RedirectSystemErr.end();		
//		return res;
	    return null; //ARL no se muy bien qu� hay que retornar.
	}

		
//	// MCP
//	public EpfTransformParameters createTransformParameters(EpfTransformParameters epfTransformParameters) 
//	{
//		//etlTransformParameters.setPluginID(Activator.PLUGIN_ID);
//		
//		// reglas
//		epfTransformParameters.setEtlTransform(etlFile);
//		
//		//input
//		epfTransformParameters.setEPFMetaModelFilePath(epfMetamodelFile);
//		epfTransformParameters.setConfModelFilePath(configurationFilePath);
//		epfTransformParameters.setLibModelFilePath(modelFilePath);
//		epfTransformParameters.setConfName("CONF");
//		epfTransformParameters.setLibName("LIB");
//		epfTransformParameters.setConfReadOnLoad(true);
//		epfTransformParameters.setLibReadOnLoad(true);
//		epfTransformParameters.setConfStoreOnDisposal(true);
//		epfTransformParameters.setLibStoreOnDisposal(true);
//
//		//output
//		epfTransformParameters.setArtMetaModelFilePath(eviMetamodelFile);
//		epfTransformParameters.setProcessMetaModelFilePath(procMetamodelFile);
//		
//		epfTransformParameters.setArtMetamodelURI(EvidencePackage.eINSTANCE.getNsURI());
//		epfTransformParameters.setProcessMetaModelURI(ProcessPackage.eINSTANCE.getNsURI());
//		epfTransformParameters.setArtModelFilePath(eviModelFile);
//		epfTransformParameters.setProcessModelFilePath(procModelFile);
//		epfTransformParameters.setArtName("ART");
//		epfTransformParameters.setProcessName("PRO");
//		epfTransformParameters.setArtReadOnLoad(false);
//		epfTransformParameters.setProcessReadOnLoad(false);
//		epfTransformParameters.setArtStoreOnDisposal(true);
//		epfTransformParameters.setProcessStoreOnDisposal(true);
//
//		return epfTransformParameters;
//	}
}
