/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
  * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.etl;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.epsilon.common.parse.problem.ParseProblem;
import org.eclipse.epsilon.common.util.StringProperties;
import org.eclipse.epsilon.emc.emf.EmfModel;
import org.eclipse.epsilon.emc.emf.xml.XmlModel;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.eol.exceptions.models.EolModelLoadingException;
import org.eclipse.epsilon.eol.models.IRelativePathResolver;
import org.eclipse.epsilon.etl.EtlModule;
import org.eclipse.epsilon.etl.IEtlModule;
import org.eclipse.epsilon.etl.dom.TransformationRule;
import org.eclipse.epsilon.etl.trace.Transformation;
import org.eclipse.opencert.apm.assurproj.assuranceproject.provider.AssuranceprojectEditPlugin;


public class EpfTransformation {
	
	private static final String RESOURCE_PATH_PREFIX = "/";
	
	private static final String UTF_8 = "UTF-8";
	private String pluginID;
	private String etlTransform;

	//MCP
	private Object result;
	// Transformation params
	private String confName;
	private String confModelFilePath;
	private Boolean confReadOnLoad;
	private Boolean confStoreOnDisposal;
	//MCP
	private String libName;
	private String libModelFilePath;
	private boolean libReadOnLoad;
	private boolean libStoreOnDisposal;
	
	private String epfMetamodelURI;
	private String epfMetamodelFilePath;
	
	// Paremeters for ouput models
	// Process
	private String processName;
	private String processMetamodelURI;
	//CRF problem	
	private String processMetaModelFilePath;
	//CRF problem	
	private String processModelFilePath;
	private boolean processReadOnLoad;
	private boolean processStoreOnDisposal;
	// Artefact
	private String artName;
	private String artMetamodelURI;
	//CRF problem	
	private String artMetaModelFilePath;
	//CRF problem	
	private String artModelFilePath;
	private boolean artReadOnLoad;
	private boolean artStoreOnDisposal;
	
	
	public void execute() throws Exception {
		
		URL resolvedTransformationURL = null;
		URI transformURI = null;
		String file = null;

		try{
			//String spluginPath = Platform.getBundle(Activator.PLUGIN_ID).getLocation();
			String spluginPath = AssuranceprojectEditPlugin.getPlugin().getBundle().getLocation();
			RedirectSystemErr.out("spluginPath:= ." + spluginPath + ".");
			if(spluginPath.contains("plugins")) // si estoy en modo jar
			{
				String pluginPath = "configuration";

				URL transformationEntry = AssuranceprojectEditPlugin.getPlugin().getBundle().getEntry(
						etlTransform);

				resolvedTransformationURL = FileLocator
						.resolve(transformationEntry);
				// MCP
				file = resolvedTransformationURL.getFile();
				file = file.replace(" ", "%20");// reemplazar caracter blanco

				String wrong = file.substring(file.indexOf("plugins"), file.indexOf("/"+etlTransform));
				String file2 = file.replace(wrong, pluginPath);
				file = file2;
			}
			else
			{	
				URL transformationEntry = AssuranceprojectEditPlugin.getPlugin().getBundle().getEntry(
						etlTransform);
				resolvedTransformationURL = FileLocator
						.resolve(transformationEntry);
				// MCP
				file = "file:" + resolvedTransformationURL.getFile();
				file = file.replace(" ", "%20");// reemplazar caracter blanco
			}
			RedirectSystemErr.out("rules file:= ." + file + ".");
			//transformURI = resolvedTransformationURL.toURI();
			transformURI = new URI(file);
			// MCP
		}catch(IOException e){
			e.printStackTrace();RedirectSystemErr.out("execute exception 1: "+e.toString());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			RedirectSystemErr.out("execute exception 2: "+e.toString());
		}
		// MCP
		//IEolExecutableModule etlModule = new EtlModule();
		IEtlModule etlModule = new EtlModule();
		// MCP
		try {
			etlModule.parse(transformURI);
		} catch (Exception e) {
			e.printStackTrace();
			RedirectSystemErr.out("execute exception 3: "+e.toString());
		}

		if (etlModule.getParseProblems().size() > 0) {
			System.err.println("Parse errors occured...");
			for (ParseProblem problem : etlModule.getParseProblems()) {
				System.err.println(problem.toString());
				RedirectSystemErr.out("encontrado "+problem.toString());
			}
		}
		for ( TransformationRule rule: etlModule.getTransformationRules())
		{
			RedirectSystemErr.out("rule = ."+ rule.toString() + ".");

		}
		// Loading of the models used in the transformation.
		// Configuration
		XmlModel confModel=createSourceXmlModel(confName, epfMetamodelFilePath, confModelFilePath, confReadOnLoad, confStoreOnDisposal);
		// Library
		XmlModel libModel=createSourceXmlModel(libName, epfMetamodelFilePath, libModelFilePath, libReadOnLoad, libStoreOnDisposal);
		// Process
		EmfModel processModel=createTargetEmFModel(processName, processMetamodelURI, processMetaModelFilePath, processModelFilePath, processReadOnLoad, processStoreOnDisposal);
		// Evidspec -> Artefact model
		EmfModel artModel=createTargetEmFModel(artName, artMetamodelURI, artMetaModelFilePath, artModelFilePath, artReadOnLoad, artStoreOnDisposal);
		
		if(confModel == null)
		{
			RedirectSystemErr.out("confModel is null");
		}
		if(libModel == null)
		{
			RedirectSystemErr.out("libModel is null");
		}
		if(processModel == null)
		{
			RedirectSystemErr.out("processModel is null");
		}
		if(artModel == null)
		{
			RedirectSystemErr.out("artModel is null");
		}
		etlModule.getContext().getModelRepository().addModel(confModel);
		etlModule.getContext().getModelRepository().addModel(libModel);
		etlModule.getContext().getModelRepository().addModel(processModel);
		etlModule.getContext().getModelRepository().addModel(artModel);
		// Running the transformation
		try {
			result = etlModule.execute();
		} catch (EolRuntimeException e) {
			e.printStackTrace();
			RedirectSystemErr.out("execute exception 4: "+e.toString());
		}

		RedirectSystemErr.out("execute voy a leer reglas disparadas ");
		for(Transformation elem : etlModule.getContext().getTransformationTrace().getTransformations())
		{
			TransformationRule rule = elem.getRule();
			Object source = elem.getSource();
			Collection targets = elem.getTargets();
			
			System.out.println("Fired rule =" + rule.getName() + ": source =[" + source.toString() + "] & tragets =[" + targets.toString() + "]");
		}
		confModel.dispose();
		libModel.dispose();
		processModel.dispose();
		artModel.dispose();
		etlModule.getContext().getModelRepository().dispose();
	}
	
	private org.eclipse.emf.common.util.URI getResourceEmfURI(String resourcePath) {
		org.eclipse.emf.common.util.URI res=org.eclipse.emf.common.util.URI.createURI(getClass().getResource(
				RESOURCE_PATH_PREFIX + resourcePath).toExternalForm());
		System.out.println(res.toFileString());
		
		return res;
	}
	
	protected XmlModel createSourceXmlModel(String name, String sourceMetaModelFilePath,
			String sourceModelFilePath, Boolean sourceReadOnLoad, Boolean sourceStoreOnDisposal){
		
		XmlModel xmlModel=new XmlModel();
		
		try{
			StringProperties sp = new StringProperties();
			sp.put(XmlModel.PROPERTY_NAME, name);
			sp.put(XmlModel.PROPERTY_READONLOAD, sourceReadOnLoad);
			sp.put(XmlModel.PROPERTY_STOREONDISPOSAL, sourceStoreOnDisposal);
			sp.put(XmlModel.PROPERTY_XSD_FILE, sourceMetaModelFilePath);
			sp.put(XmlModel.PROPERTY_MODEL_FILE,sourceModelFilePath);
			xmlModel.load(sp, new IRelativePathResolver() {
			    @Override
			    public String resolve(String relativePath) {
			        return relativePath;
			    }
			});
		//return model;
		}catch(Exception e){
			e.printStackTrace();
		}
		

		return xmlModel;
		
	}
	
	protected EmfModel createTargetEmFModel(String name, String targetMetaModelURI, String targetMetaModelFilePath,
			String targetModelFilePath, Boolean targetReadOnLoad, Boolean targetStoreOnDisposal){
		EmfModel emfModel= new EmfModel();
		emfModel.setName(name);

		if(targetMetaModelURI != null && !targetMetaModelURI.isEmpty()) 
		{ 
			emfModel.setMetamodelFileBased(false); 
			if(targetMetaModelURI.contains(","))
			{
				String[] metaModelURIs = targetMetaModelURI.split(",");
				List<String> uris =new ArrayList<String>(metaModelURIs.length);
				for(int i=0;i<metaModelURIs.length; i++)
				{
					uris.add(metaModelURIs[i].trim());
				};
				RedirectSystemErr.out("target uris:= ." + uris.toString() + ".");	
				emfModel.setMetamodelUris(uris);
			}
			else
			{
				RedirectSystemErr.out("targetMetaModelURI:= ." + targetMetaModelURI + ".");
				emfModel.setMetamodelUri(targetMetaModelURI);
			}
		}
		if(targetMetaModelFilePath != null && !targetMetaModelFilePath.isEmpty()) 
		{ 
			emfModel.setMetamodelFileBased(true); 
			if(targetMetaModelFilePath.contains(","))
			{
				String[] metaModelURIs = targetMetaModelFilePath.split(",");
				List<String> files = new ArrayList<String>(metaModelURIs.length);
				List<org.eclipse.emf.common.util.URI> fileUris =new ArrayList<org.eclipse.emf.common.util.URI>(metaModelURIs.length);
				for(int i=0;i<metaModelURIs.length; i++)
				{
					files.add(metaModelURIs[i].trim());
				};

				RedirectSystemErr.out("target files:= ." + files.toString() + ".");
				emfModel.setMetamodelFiles(files);
			}
			else
			{
				RedirectSystemErr.out("targetMetaModelFilePath:= ." + targetMetaModelFilePath + ".");
				emfModel.setMetamodelFile(targetMetaModelFilePath);
			}
		}
		//CRF problem		
		RedirectSystemErr.out("targetModelFilePath:= ." + targetModelFilePath + ".");		
		emfModel.setModelFile(targetModelFilePath);
		emfModel.setReadOnLoad(targetReadOnLoad);
		emfModel.setStoredOnDisposal(targetStoreOnDisposal);
		// MCP
		emfModel.setCachingEnabled(true);
		emfModel.setExpand(true);
		// MCP
		try {
			emfModel.load();
		} catch (EolModelLoadingException e) {
			e.printStackTrace();
		} 
		return emfModel;
	}
	
	public void createJob(EpfTransformParameters parameters) {
		this.pluginID = parameters.getPluginID();
		this.etlTransform = parameters.getEtlTransform();
		
		// Configuration model 
		confName=parameters.getConfName();
		confModelFilePath=parameters.getConfModelFilePath();
		confReadOnLoad=parameters.isConfReadOnLoad();
		confStoreOnDisposal=parameters.isConfStoreOnDisposal();
		
		//Library model
		libName=parameters.getLibName();
		libModelFilePath=parameters.getLibModelFilePath();
		libReadOnLoad=parameters.isLibReadOnLoad();
		libStoreOnDisposal=parameters.isLibStoreOnDisposal();
		
		// EPF metamodel (common to library and configuration)
		epfMetamodelURI=parameters.getEPFMetaModelURI();
		epfMetamodelFilePath=parameters.getEPFMetaModelFilePath();
		
		// Process model
		processName=parameters.getProcessName();
		processMetamodelURI=parameters.getProcessMetamodelURI();
		processMetaModelFilePath=parameters.getProcessMetamodelFilePath();
		processModelFilePath=parameters.getProcessModelFilePath();
		processReadOnLoad=parameters.isProcessReadOnLoad();
		processStoreOnDisposal=parameters.isProcessStoreOnDisposal();
		
		// Artefact model
		artName=parameters.getArtName();
		artMetamodelURI=parameters.getArtMetamodelURI();
		artMetaModelFilePath=parameters.getArtMetamodelFilePath();
		artModelFilePath=parameters.getArtModelFilePath();
		artReadOnLoad=parameters.isArtReadOnLoad();
		artStoreOnDisposal=parameters.isArtStoreOnDisposal();
	}
	
	public Object getResult()
	{
		return result;
	}

}
