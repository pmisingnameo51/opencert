/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.apm.assurproj.assuranceproject.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.JOptionPane;

import org.eclipse.emf.cdo.CDOObject;
import org.eclipse.emf.cdo.common.util.CDOException;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.ui.views.DawnExplorer;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CDOUtil;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.edit.ui.util.EditUIUtil;
import org.eclipse.emf.eef.runtime.ui.notify.OpenWizardOnDoubleClick;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssetsPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.assurproj.assuranceproject.BaselineConfig;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.PlatformUI;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivity;
import org.eclipse.opencert.apm.baseline.baseline.BaseActivityRel;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefact;
import org.eclipse.opencert.apm.baseline.baseline.BaseArtefactRel;
import org.eclipse.opencert.apm.baseline.baseline.BaseAssurableElement;
import org.eclipse.opencert.apm.baseline.baseline.BaseComplianceMap;
import org.eclipse.opencert.apm.baseline.baseline.BaseEquivalenceMap;
import org.eclipse.opencert.apm.baseline.baseline.BaseFramework;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirement;
import org.eclipse.opencert.apm.baseline.baseline.BaseRequirementRel;
import org.eclipse.opencert.apm.baseline.baseline.BaseRole;
import org.eclipse.opencert.apm.baseline.baseline.BaseTechnique;
import org.eclipse.opencert.apm.baseline.baseline.BaselineElement;
import org.eclipse.opencert.apm.baseline.baseline.BaselineFactory;
import org.eclipse.opencert.apm.baseline.baseline.BaselinePackage;
import org.eclipse.opencert.apm.baseline.baseline.utils.MappingSet;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel;
import org.eclipse.opencert.evm.evidspec.evidence.EvidenceFactory;
import org.eclipse.opencert.evm.evidspec.evidence.EvidencePackage;
import org.eclipse.opencert.infra.mappings.mapping.MapGroup;
import org.eclipse.opencert.infra.mappings.mapping.MapJustification;
import org.eclipse.opencert.infra.mappings.mapping.MapKind;
import org.eclipse.opencert.infra.mappings.mapping.MapModel;
import org.eclipse.opencert.infra.mappings.mapping.MappingFactory;
import org.eclipse.opencert.infra.mappings.mapping.MappingPackage;
import org.eclipse.opencert.infra.svnkit.MainClass;
import org.eclipse.opencert.pam.procspec.process.Activity;
import org.eclipse.opencert.pam.procspec.process.Participant;
import org.eclipse.opencert.pam.procspec.process.Technique;
import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;
import org.eclipse.opencert.sam.arg.arg.Claim;



public class CrossDomainUI extends Dialog {
	
	

	protected ArrayList<String> sResult = null;
	

	private IEditorPart editor;
	private URI resourceURI;
	private CDOTransaction transaction = null;
	private CDOSession sessionCDO = null;

	private CDOResource resourceFrom = null; 		// Resource Source Assurance project
	private CDOResource resourceTarget = null; 		// Resource Target Assurance project
	private AssuranceProject assuranceProjectFrom;	//model Source Assurance project
	private AssuranceProject assuranceProjectTarget;//model Traget Assurance project
	private CDOResource artefactResource = null;
	private CDOResource mapResource = null;
	
	private final String ARTEFACTMODEL_CLASS = "ArtefactModel";
	private final String ARTEFACTDEFINITION_CLASS = "ArtefactDefinition";
	private final String ARTEFACT_CLASS = "Artefact";
	
	private EvidencePackage evidencePackage = EvidencePackage.eINSTANCE;
	private EvidenceFactory evidenceFactory = evidencePackage.getEvidenceFactory();
	
	private BaselinePackage baselinePackage = BaselinePackage.eINSTANCE;
	private BaselineFactory baselineFactory = baselinePackage.getBaselineFactory();
	
	private MappingPackage mappingPackage = MappingPackage.eINSTANCE;
	private MappingFactory mappingFactory=mappingPackage.getMappingFactory();
	
	private org.eclipse.swt.widgets.List refList;
	
	private ArrayList<String> refListMap;
	private ArrayList<String> refListDirMap;
	private CDOView viewCDO = null;


	private TreeViewer treeBaselineTarget;
	private TreeViewer treeBaselineSource;
	private ResourceSet baselineSourceResourceSet;
	private ResourceSet baselineTargetResourceSet;
	
	private TreeViewer treeEvidenceTarget;
	private ResourceSet evidenceSourceResourceSet;
	private ResourceSet evidenceTargetResourceSet;
	private Text projectsourceText;
	private Label sourceLabel;
	private CheckboxTreeViewerExt checktreeEvidenceSource;
	
	private Combo cbMapGroup;
	//private Combo cbComplianceMap;
	//private Combo cbFilterElementMap;
	private Text txtJustification;
	private Text idText;
	private Text nameText;
	
	private Combo cbType;
	private Label targetLabel;
	private Text projecttargetText;
		
	boolean bnewEquivalenceMap = false;
	boolean bEqualMapGroup = false;
		
	EObject object;

	// List Equivalence Map of the selected element tree
	EList<BaseComplianceMap> lstrefCompMapFrom;

	EList<TreeItem> listGreenItem = new BasicEList<TreeItem>();
	
	Map<Object, Object> options = new HashMap<Object, Object>();

	EList<BaseComplianceMap> lstComplianceMap;
	EList<MapGroup> lstMapGroup;
	EList<Activity> lstrefActivity;
	EList<Artefact> lstrefArtefact;
	EList<Claim> lstrefRequirement;
	EList<Participant> lstrefRole;
	EList<Technique> lstrefTechnique;

	
	//Source Compliance Map controls
	protected Text SCMidText;
	protected Text SCMnameText;
	protected Combo SCMcbType;
	protected Text SCMtxtJustification;
	
	
	//Target Compliance Map controls
	protected Text TCMidText;
	protected Text TCMnameText;
	protected Combo TCMcbType;
	protected Text TCMtxtJustification;
	
	//Postconditions controls
	protected TableViewer listPostConditions;
	protected Text PCidText;
	protected Text PCnameText;
	protected Text PCtxtdescription;
	
	protected AssuranceProject assuranceProject;
	
	protected MapGroup mapGroupActive;
	protected MapGroup mapGroup;
	protected MapKind mapKind;
	protected MapJustification mapJustification;
	protected MapModel mapModel;
	protected BaseComplianceMap baseComplianceMap; 

	//String sElementFilter;
	
	TextCellEditor cellEditor = null;		
	
	private MapGroup targetComplianceGroup;
	public CrossDomainUI(Shell parentShell) {
		
		super(parentShell);

		
		editor = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage().getActiveEditor();
		resourceURI = EditUIUtil.getURI(editor.getEditorInput());

		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		sessionCDO = CDOConnectionUtil.instance.getCurrentSession();
		viewCDO = CDOConnectionUtil.instance.openView(sessionCDO);
		transaction = sessionCDO.openTransaction();
		

		setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX);		
		
	}

	public CrossDomainUI(IShellProvider parentShell) {
		super(parentShell);
	}

	@Override
	protected void constrainShellSize() {
		// TODO Auto-generated method stub
		super.constrainShellSize();
		getShell().setMaximized(true);
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
	    super.createButtonsForButtonBar(parent);

	    Button ok = getButton(IDialogConstants.OK_ID);
	    ok.setText("Reuse");
	    setButtonLayoutData(ok);

	    Button cancel = getButton(IDialogConstants.CANCEL_ID);
	    cancel.setText("Close");
	    setButtonLayoutData(cancel);
	 }
	
	@Override
	protected Control createDialogArea(final Composite parent) {

		refListDirMap = new ArrayList<String>();
		
		refListMap= new ArrayList<String>();
	
		ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
				ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

		final Composite contents = (Composite) super.createDialogArea(parent);

		GridLayout contentsGridLayout = (GridLayout) contents.getLayout();
		contentsGridLayout.numColumns = 3;

		GridData contentsGridData = (GridData) contents.getLayoutData();
		contentsGridData.horizontalAlignment = SWT.FILL;
		contentsGridData.verticalAlignment = SWT.FILL;

		// Default filter
	//	sElementFilter = "Artefact";

		
		createTargetDialogArea(contents,adapterFactory);
		createMiddleDialogArea(contents,adapterFactory);
		createSourceDialogArea(contents, adapterFactory);

		return contents;
	}

	protected void createSourceDialogArea(final Composite contents,
			ComposedAdapterFactory adapterFactory) {

		
		final Composite sourceComposite = new Composite(contents, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, false, true);
			data.horizontalAlignment = SWT.END;
			data.horizontalAlignment = SWT.FILL;
			sourceComposite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			layout.marginHeight = 0;
			layout.marginWidth = 10;
			layout.numColumns = 1;
			
			sourceComposite.setLayout(layout);
		}

		

		final Composite compositeSourceProject = new Composite(sourceComposite, SWT.NONE);
		{
			GridData dataNew = new GridData(SWT.FILL, SWT.FILL, false, true);
			dataNew.horizontalAlignment = SWT.END;
			compositeSourceProject.setLayoutData(dataNew);

			GridLayout layoutNew = new GridLayout();
			dataNew.horizontalAlignment = SWT.FILL;
			layoutNew.marginHeight = 0;
			layoutNew.marginWidth = 0;
			layoutNew.numColumns = 3;
			compositeSourceProject.setLayout(layoutNew);
		}
		
		// Label
		sourceLabel = new Label(compositeSourceProject, SWT.NONE);
		sourceLabel.setText("Source Project:");
		GridData sourceLabelGridData = new GridData();
		sourceLabelGridData.horizontalAlignment = SWT.FILL;
		sourceLabelGridData.verticalAlignment = SWT.FILL;
		sourceLabel.setLayoutData(sourceLabelGridData);
		// Text
		projectsourceText = new Text(compositeSourceProject, SWT.NONE);
		GridData nameTextGridData = new GridData();
		nameTextGridData.grabExcessHorizontalSpace = true;
		nameTextGridData.minimumWidth = 50;
		nameTextGridData.horizontalAlignment = SWT.FILL;
		nameTextGridData.verticalAlignment = SWT.FILL;
		projectsourceText.setLayoutData(nameTextGridData);
		
		//Button
		Button selectSourceProject = new Button(compositeSourceProject, SWT.NONE);
		selectSourceProject.setText("Search...");
		GridData NewGroupMapGridData = new GridData();
		NewGroupMapGridData.verticalAlignment = SWT.BEGINNING;
		NewGroupMapGridData.horizontalAlignment = SWT.FILL;
		selectSourceProject.setLayoutData(NewGroupMapGridData);

		selectSourceProject.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				
				ListAssuranceProject listAssuranceProject = new ListAssuranceProject(compositeSourceProject.getShell(), sessionCDO);
				if (listAssuranceProject.open() == Window.OK) {
					sResult = new ArrayList<String>();
					sResult.add(listAssuranceProject.getResult().get(0));
					sResult.add(listAssuranceProject.getResult().get(1));
					
					projectsourceText.setText(listAssuranceProject.getResult().get(1));
					
					try {	
						resourceFrom = viewCDO.getResource(sResult.get(1), true);
						assuranceProjectFrom = (AssuranceProject) resourceFrom.getContents().get(0);
						
						
						//Load Source Baselines
						EList<BaselineConfig> lstBaselineConfig = assuranceProjectFrom.getBaselineConfig();
						Iterator<BaselineConfig> baselineConfig = lstBaselineConfig.iterator();
						
						baselineSourceResourceSet = new ResourceSetImpl();
						
						boolean bFind = false;
						while (baselineConfig.hasNext() && !bFind) {
							BaselineConfig bconfig = baselineConfig.next();
							if (bconfig.isIsActive()) {
								bFind = true;
								
								EList<BaseFramework> lstBaseFramework = bconfig.getRefFramework();										
								Iterator<BaseFramework> itbaseFramework = lstBaseFramework.iterator();
																
								while (itbaseFramework.hasNext()) {
																		
									BaseFramework baseFramework = itbaseFramework.next();									
									baselineSourceResourceSet.getResources().add(baseFramework.eResource());
								}
								//treeBaselineSource.setInput(baselineSourceResourceSet);
								
								CreateBaselineModelFromResourceSet(treeBaselineSource,baselineSourceResourceSet);
								
								//To search the nodes to be moved.
								treeBaselineSource.expandAll();
								
								
								moveBaseRequirementsAboveBaseArtefacts(treeBaselineSource);
								//To expand the moved nodes.
								//treeBaselineSource.expandAll();
								
								
							}
						}
						
						//Load the evidence models of the active assetsPackage of the selected assurance project 
						EList<AssetsPackage> lstAssetsPackage = assuranceProjectFrom.getAssetsPackage();
						Iterator<AssetsPackage> assetsPackage = lstAssetsPackage.iterator();
						
						evidenceSourceResourceSet = new ResourceSetImpl();
							
						 bFind = false;
						while (assetsPackage.hasNext() && !bFind) {
							AssetsPackage aPackage = assetsPackage.next();
							if (aPackage.isIsActive()) {
								bFind= true;
								EList<ArtefactModel> lstArtefactModel = aPackage.getArtefactsModel();
								Iterator<ArtefactModel> artefactModel = lstArtefactModel.iterator();
								while (artefactModel.hasNext()) {
									ArtefactModel artefact = artefactModel.next();
									artefactResource = CDOUtil.getCDOObject(artefact).cdoResource();
									evidenceSourceResourceSet.getResources().add(artefactResource);
								}							
								CreateModelFromResourceSet(checktreeEvidenceSource,evidenceSourceResourceSet);
								checktreeEvidenceSource.expandAll();
							}
						}						
					} catch (CDOException cdoe) {
						cdoe.printStackTrace();
					}	
				}
			}
		});
		
		
		//Load the baseline
		GridData modelViewerGridData1 = new GridData();
		modelViewerGridData1.verticalAlignment = SWT.FILL;
		modelViewerGridData1.horizontalAlignment = SWT.FILL;
		modelViewerGridData1.widthHint = (Display.getCurrent().getBounds().width / 3);
		modelViewerGridData1.heightHint = (Display.getCurrent().getBounds().height / 3) - 20;
		modelViewerGridData1.grabExcessHorizontalSpace = true;
		modelViewerGridData1.grabExcessVerticalSpace = true;
		
		treeBaselineSource = new TreeViewer(sourceComposite,
				SWT.NONE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		
		treeBaselineSource.getTree().setLayoutData(modelViewerGridData1);
		treeBaselineSource.setContentProvider(new AdapterFactoryContentProvider(
				adapterFactory));
		treeBaselineSource.setLabelProvider(new AdapterFactoryLabelProvider(
				adapterFactory));
		treeBaselineSource.expandAll();
		
		BasicCommandStack commandStackBL = new BasicCommandStack();
		EditingDomain editingDomainBL = new AdapterFactoryEditingDomain(adapterFactory, commandStackBL, new HashMap<Resource, Boolean>());
		treeBaselineSource.addDoubleClickListener(new OpenWizardOnDoubleClick(editingDomainBL, adapterFactory));
		
		//Source compliance controls
		//Compliance map controls
		Group groupSourceCM = new Group(sourceComposite, SWT.NULL);
		groupSourceCM.setLayout(new GridLayout(1,true));
		groupSourceCM.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true,1,1));
		groupSourceCM.setText("Target Compliance Map");

		
		final Composite controlSCM = new Composite(groupSourceCM, SWT.NONE);
		{
			GridData dataNew = new GridData(SWT.FILL, SWT.FILL, true, true);
			dataNew.horizontalAlignment = SWT.END;
			controlSCM.setLayoutData(dataNew);

			GridLayout layoutNew = new GridLayout();
			dataNew.horizontalAlignment = SWT.FILL;
			dataNew.verticalAlignment = SWT.FILL;
			layoutNew.marginHeight = 0;
			layoutNew.marginWidth = 0;
			layoutNew.numColumns = 2;
			controlSCM.setLayout(layoutNew);
		}
		final Composite controlSCMCol1 = new Composite(controlSCM, SWT.NONE);
		{
			GridData SCMCol1 = new GridData(SWT.FILL, SWT.FILL, true, true);
			SCMCol1.horizontalAlignment = SWT.END;
			controlSCMCol1.setLayoutData(SCMCol1);
			
			GridLayout layoutNew1 = new GridLayout();
			SCMCol1.horizontalAlignment = SWT.FILL;
			SCMCol1.verticalAlignment = SWT.FILL;
			layoutNew1.marginHeight = 0;
			layoutNew1.marginWidth = 0;
			layoutNew1.numColumns = 2;
			controlSCMCol1.setLayout(layoutNew1);
			
		}
		final Composite controlSCMCol2 = new Composite(controlSCM, SWT.NONE);
		{
			
			GridData SCMCol2 = new GridData(SWT.FILL, SWT.FILL, true, true);
			SCMCol2.horizontalAlignment = SWT.END;
			controlSCMCol2.setLayoutData(SCMCol2);
						
			GridLayout layoutNew2 = new GridLayout();
			SCMCol2.horizontalAlignment = SWT.FILL;
			SCMCol2.verticalAlignment = SWT.FILL;
			layoutNew2.marginHeight = 0;
			layoutNew2.marginWidth = 0;
			layoutNew2.numColumns = 1;
			controlSCMCol2.setLayout(layoutNew2);
		}
		
		/*cbComplianceMap = new Combo (controlEquivMap, SWT.READ_ONLY);
		GridData cbComplianceMapData = new GridData();
		cbComplianceMapData.verticalAlignment = SWT.FILL;
		cbComplianceMapData.horizontalAlignment = SWT.FILL;
		cbComplianceMapData.grabExcessHorizontalSpace = true;
		cbComplianceMapData.grabExcessVerticalSpace = true;
		cbComplianceMap.setLayoutData(cbComplianceMapData);*/
		
		
				
		
		// Label
		Label SCMidLabel = new Label(controlSCMCol1, SWT.NONE);
		SCMidLabel.setText("ID");
		GridData SCMidLabelGridData = new GridData();
		SCMidLabelGridData.horizontalAlignment = SWT.FILL;
		SCMidLabelGridData.verticalAlignment = SWT.FILL;
		SCMidLabel.setLayoutData(SCMidLabelGridData);

		// Text
		SCMidText = new Text(controlSCMCol1, SWT.NONE);
		GridData SCMidTextGridData = new GridData();
		SCMidTextGridData.horizontalAlignment = SWT.FILL;
		SCMidTextGridData.verticalAlignment = SWT.FILL;
		SCMidText.setLayoutData(SCMidTextGridData);
		
		
		// Label
		Label SCMnameLabel = new Label(controlSCMCol1, SWT.NONE);
		SCMnameLabel.setText("Name");
		GridData SCMnameLabelGridData = new GridData();
		SCMnameLabelGridData.grabExcessHorizontalSpace = true;
		SCMnameLabelGridData.minimumWidth = 20;
		SCMnameLabelGridData.horizontalAlignment = SWT.FILL;
		SCMnameLabelGridData.verticalAlignment = SWT.FILL;
		SCMnameLabel.setLayoutData(SCMnameLabelGridData);

		// Text
		SCMnameText = new Text(controlSCMCol1, SWT.NONE);
		GridData SCMnameTextGridData = new GridData();
		SCMnameTextGridData.grabExcessHorizontalSpace = true;
		SCMnameTextGridData.minimumWidth = 30;
		SCMnameTextGridData.horizontalAlignment = SWT.FILL;
		SCMnameTextGridData.verticalAlignment = SWT.FILL;
		SCMnameText.setLayoutData(nameTextGridData);
		
		
		// Text
		Label SCMtypeLabel = new Label(controlSCMCol1, SWT.NONE);
		SCMtypeLabel.setText("Type");
		GridData SCMtypeLabelGridData = new GridData();
		SCMtypeLabelGridData.grabExcessHorizontalSpace = true;
		
		SCMtypeLabelGridData.horizontalAlignment = SWT.FILL;
		SCMtypeLabelGridData.verticalAlignment = SWT.FILL;
		SCMtypeLabel.setLayoutData(SCMtypeLabelGridData);
		
		SCMcbType = new Combo (controlSCMCol1, SWT.READ_ONLY);
		GridData SCMcbTypeGridData = new GridData();
		SCMcbTypeGridData.verticalAlignment = SWT.FILL;
		SCMcbTypeGridData.horizontalAlignment = SWT.FILL;
		//SCMcbTypeGridData.widthHint = Display.getCurrent().getBounds().width / 7;
		//SCMcbTypeGridData.heightHint = Display.getCurrent().getBounds().height / 7;
		SCMcbTypeGridData.grabExcessHorizontalSpace = true;
		SCMcbTypeGridData.grabExcessVerticalSpace = true;
		SCMcbType.setLayoutData(SCMcbTypeGridData);
		
		EEnum enumMapKind = MappingFactory.eINSTANCE.getMappingPackage().getMapKind();
		EList<EEnumLiteral> mapkindList= enumMapKind.getELiterals();
		
		Iterator<EEnumLiteral> iterTo = mapkindList.iterator();
		while (iterTo.hasNext()) {
			SCMcbType.add(iterTo.next().getName());
		}
		SCMcbType.select(0);
		
		Label SCMjustificationLabel = new Label(controlSCMCol2, SWT.NONE);
		SCMjustificationLabel.setText("Justification");
		GridData SCMjustificationLabelGridData = new GridData();
		SCMjustificationLabelGridData.horizontalSpan=1;
		SCMjustificationLabelGridData.horizontalAlignment = SWT.FILL;
		SCMjustificationLabelGridData.verticalAlignment = SWT.FILL;
		SCMjustificationLabel.setLayoutData(SCMjustificationLabelGridData);
		
		SCMtxtJustification = new Text(controlSCMCol2, SWT.MULTI);
		SCMtxtJustification.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
		/*GridData SCMtxtJustificationGridData = new GridData();
		SCMtxtJustificationGridData.horizontalSpan = 1;		
		SCMtxtJustificationGridData.horizontalAlignment = SWT.FILL;
		SCMtxtJustificationGridData.verticalAlignment = SWT.FILL;
		SCMtxtJustificationGridData.widthHint = Display.getCurrent().getBounds().width / 4;
		SCMtxtJustificationGridData.heightHint = 33;
		SCMtxtJustification.setLayoutData(SCMtxtJustificationGridData);**/

		
		
		// TreeViewer for Artefact models
		checktreeEvidenceSource = new CheckboxTreeViewerExt(sourceComposite, SWT.SINGLE
				| SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		
		final GridData modelViewerGridData = new GridData();
		modelViewerGridData.verticalAlignment = SWT.FILL;
		modelViewerGridData.horizontalAlignment = SWT.FILL;
		modelViewerGridData.widthHint = Display.getCurrent().getBounds().width / 3;
		modelViewerGridData.heightHint = (Display.getCurrent().getBounds().height / 3) - 20;
		modelViewerGridData.grabExcessHorizontalSpace = false;
		modelViewerGridData.grabExcessVerticalSpace = true;

		checktreeEvidenceSource.getTree().setLayoutData(modelViewerGridData);
		checktreeEvidenceSource.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
		checktreeEvidenceSource.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		
		BasicCommandStack commandStack = new BasicCommandStack();
		EditingDomain editingDomain = new AdapterFactoryEditingDomain(adapterFactory, commandStack, new HashMap<Resource, Boolean>());
		checktreeEvidenceSource.addDoubleClickListener(new OpenWizardOnDoubleClick(editingDomain, adapterFactory));
						
	}

	//To put the Requirement above the correspondent BaseArtefacts
	protected void moveBaseRequirementsAboveBaseArtefacts(TreeViewer treeBaseline) {
		// TODO Auto-generated method stub
		Tree modifyTree =treeBaseline.getTree();
		TreeItem[] rootNodes = modifyTree.getItems();
		for(TreeItem root:rootNodes){
			for(int x=0; x <root.getItems()[0].getItems().length;x++){
				TreeItem  oneItem = root.getItems()[0].getItems()[x];
				Object dataObject = oneItem.getData();
				if(dataObject instanceof BaseArtefact){
					//Buscar sus BaseRequirementes y moverlos
					BaseArtefact oneArtefact= (BaseArtefact)dataObject;
					EList <BaseRequirement>listRequirement = oneArtefact.getConstrainingRequirement();
					for(BaseRequirement onReq:listRequirement){
						TreeItem nodeToMove= searchRequirement(onReq,root.getItems()[0]);
						if(nodeToMove!=null){
							//oneItem.getItems()
							TreeItem newNode = new TreeItem(oneItem,SWT.NONE,oneItem.getItems().length);
							newNode.setData(nodeToMove.getData());
							newNode.setText(nodeToMove.getText());
							newNode.setImage(nodeToMove.getImage());							
						}
						oneItem.setExpanded(true);
					}
					
				}
			}
		}
		
	}
	
	private TreeItem searchRequirement(BaseRequirement equivRequirement, TreeItem root) {
		for(int x=0; x <root.getItems().length;x++){
			TreeItem  oneItem = root.getItems()[x];			
			Object dataObject = oneItem.getData();
			if(dataObject instanceof BaseRequirement){
				if(((BaseRequirement)dataObject).cdoID()==equivRequirement.cdoID()){					
					return oneItem;
				}
			}	
			if(oneItem.getItems().length>0){
				TreeItem found=searchRequirement( equivRequirement,  oneItem);
				if (found!=null) return found;
			}
		}
		return null;
	}						

	protected void createMiddleDialogArea(final Composite contents,
			ComposedAdapterFactory adapterFactory) {
		
		Composite controlMapping = new Composite(contents, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			controlMapping.setLayoutData(data);

			GridLayout layout = new GridLayout();
			data.horizontalAlignment = SWT.FILL;
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 1;
			controlMapping.setLayout(layout);
		}

		Group groupFilter = new Group(controlMapping, SWT.NULL);
		groupFilter.setLayout(new GridLayout(1,false));
		groupFilter.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true,2,1));
		groupFilter.setText("Filtering");
		
		refList = new org.eclipse.swt.widgets.List(groupFilter, SWT.FILL | SWT.SINGLE | SWT.BORDER | SWT.V_SCROLL| SWT.H_SCROLL);
		refList.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
		for (String one:refListMap){
			refList.add(one);
		}
		//refListDirMap = new ArrayList<String>();
		
		refList.addSelectionListener(new SelectionAdapter() {
		      public void widgetSelected(SelectionEvent e) {
			  		CreateModelFromFiletocombo(cbMapGroup, refListDirMap.get(refList.getSelectionIndex()).toString());
		      }
		});
		
		
		
		
		Label mapLabel = new Label(groupFilter, SWT.NONE);
		mapLabel.setText("Map Group");
		GridData mapLabelGridData = new GridData();
		mapLabelGridData.horizontalSpan = 2;
		mapLabelGridData.horizontalAlignment = SWT.FILL;
		mapLabelGridData.verticalAlignment = SWT.FILL;
		mapLabel.setLayoutData(mapLabelGridData);

		final Composite controlMappingNew = new Composite(groupFilter, SWT.NONE);
		{
			GridData dataNew = new GridData(SWT.FILL, SWT.FILL, true, true);
			dataNew.horizontalAlignment = SWT.END;
			controlMappingNew.setLayoutData(dataNew);

			GridLayout layoutNew = new GridLayout();
			dataNew.horizontalAlignment = SWT.FILL;
			layoutNew.marginHeight = 0;
			layoutNew.marginWidth = 0;
			layoutNew.numColumns = 1;
			controlMappingNew.setLayout(layoutNew);
		}
		
		
		cbMapGroup = new Combo (controlMappingNew, SWT.READ_ONLY);
		GridData cbMapGroupGridData = new GridData();
		cbMapGroupGridData.verticalAlignment = SWT.FILL;
		cbMapGroupGridData.horizontalAlignment = SWT.FILL;
		cbMapGroupGridData.grabExcessHorizontalSpace = true;
		cbMapGroupGridData.grabExcessVerticalSpace = true;
		cbMapGroup.setLayoutData(cbMapGroupGridData);
		
		// if we select a map group, verify selected element from sourcetree and cbtarget
		// show the equivalence from the element selected
		
		cbMapGroup.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {		
				
				
				txtJustification.setText("");
				idText.setText("");
				nameText.setText("");
				cbType.select(-1);		
				// mapGroup selected
				mapGroup = lstMapGroup.get(cbMapGroup.getSelectionIndex());
				
				if (CDOUtil.getCDOObject(mapGroup).cdoID().equals(CDOUtil.getCDOObject(mapGroupActive).cdoID())){
					//if (mapGroup.getName().contentEquals(mapGroupActive.getName())) {
				} else {
					MessageDialog.openError(getShell(), "Selected Map Group",
							"The seleted map group is not the map group of the active base line.");
				}
			}
		});

		
		/*Label filtermapLabel = new Label(groupFilter, SWT.NONE);
		filtermapLabel.setText("Filter Map Element");
		GridData filtermapLabelGridData = new GridData();
		filtermapLabelGridData.horizontalSpan = 2;
		filtermapLabelGridData.horizontalAlignment = SWT.FILL;
		filtermapLabelGridData.verticalAlignment = SWT.FILL;
		filtermapLabel.setLayoutData(filtermapLabelGridData);
		
		cbFilterElementMap = new Combo (groupFilter, SWT.READ_ONLY);
		GridData cbFilterElementMapGridData = new GridData();
		cbFilterElementMapGridData.verticalAlignment = SWT.FILL;
		cbFilterElementMapGridData.horizontalAlignment = SWT.FILL;
		cbFilterElementMapGridData.widthHint = Display.getCurrent().getBounds().width / 7;
		cbFilterElementMapGridData.heightHint = Display.getCurrent().getBounds().height / 7;
		cbFilterElementMapGridData.grabExcessHorizontalSpace = true;
		cbFilterElementMapGridData.grabExcessVerticalSpace = true;
		cbFilterElementMap.setLayoutData(cbFilterElementMapGridData);
		
		cbFilterElementMap.add("Activity");
		cbFilterElementMap.add("Artefact");
		cbFilterElementMap.add("Requirement");
		cbFilterElementMap.add("Technique");
		cbFilterElementMap.add("Role");
		cbFilterElementMap.select(1);
		
		*/
	
		Group groupEquMap = new Group(controlMapping, SWT.NULL);
		groupEquMap.setLayout(new GridLayout(1,true));
		groupEquMap.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true,1,1));
		groupEquMap.setText("Existing Equivalence Map");

		
		final Composite controlEquivMap = new Composite(groupEquMap, SWT.NONE);
		{
			GridData dataNew = new GridData(SWT.FILL, SWT.FILL, true, true);
			dataNew.horizontalAlignment = SWT.END;
			controlEquivMap.setLayoutData(dataNew);

			GridLayout layoutNew = new GridLayout();
			dataNew.horizontalAlignment = SWT.FILL;
			dataNew.verticalAlignment = SWT.FILL;
			layoutNew.marginHeight = 0;
			layoutNew.marginWidth = 0;
			layoutNew.numColumns = 2;
			controlEquivMap.setLayout(layoutNew);
		}
		
		
		/*cbComplianceMap = new Combo (controlEquivMap, SWT.READ_ONLY);
		GridData cbComplianceMapData = new GridData();
		cbComplianceMapData.verticalAlignment = SWT.FILL;
		cbComplianceMapData.horizontalAlignment = SWT.FILL;
		cbComplianceMapData.grabExcessHorizontalSpace = true;
		cbComplianceMapData.grabExcessVerticalSpace = true;
		cbComplianceMap.setLayoutData(cbComplianceMapData);*/
		
		
				
		
		// Label
		Label idLabel = new Label(controlEquivMap, SWT.NONE);
		idLabel.setText("ID");
		GridData idLabelGridData = new GridData();
		idLabelGridData.horizontalAlignment = SWT.FILL;
		idLabelGridData.verticalAlignment = SWT.FILL;
		idLabel.setLayoutData(idLabelGridData);

		// Text
		idText = new Text(controlEquivMap, SWT.NONE);
		GridData idTextGridData = new GridData();
		idTextGridData.horizontalAlignment = SWT.FILL;
		idTextGridData.verticalAlignment = SWT.FILL;
		idText.setLayoutData(idTextGridData);
		
		
		// Label
		Label nameLabel = new Label(controlEquivMap, SWT.NONE);
		nameLabel.setText("Name");
		GridData nameLabelGridData = new GridData();
		nameLabelGridData.grabExcessHorizontalSpace = true;
		nameLabelGridData.minimumWidth = 20;
		nameLabelGridData.horizontalAlignment = SWT.FILL;
		nameLabelGridData.verticalAlignment = SWT.FILL;
		nameLabel.setLayoutData(nameLabelGridData);

		// Text
		nameText = new Text(controlEquivMap, SWT.NONE);
		GridData nameTextGridData = new GridData();
		nameTextGridData.grabExcessHorizontalSpace = true;
		nameTextGridData.minimumWidth = 20;
		nameTextGridData.horizontalAlignment = SWT.FILL;
		nameTextGridData.verticalAlignment = SWT.FILL;
		nameText.setLayoutData(nameTextGridData);
		
		
		// Text
		Label typeLabel = new Label(controlEquivMap, SWT.NONE);
		typeLabel.setText("Type");
		GridData typeLabelGridData = new GridData();
		typeLabelGridData.grabExcessHorizontalSpace = true;
		typeLabelGridData.minimumWidth = 20;
		typeLabelGridData.horizontalAlignment = SWT.FILL;
		typeLabelGridData.verticalAlignment = SWT.FILL;
		typeLabel.setLayoutData(typeLabelGridData);
		
		cbType = new Combo (controlEquivMap, SWT.READ_ONLY);
		GridData cbTypeGridData = new GridData();
		cbTypeGridData.verticalAlignment = SWT.FILL;
		cbTypeGridData.horizontalAlignment = SWT.FILL;
		cbTypeGridData.widthHint = Display.getCurrent().getBounds().width / 7;
		cbTypeGridData.heightHint = Display.getCurrent().getBounds().height / 7;
		cbTypeGridData.grabExcessHorizontalSpace = true;
		cbTypeGridData.grabExcessVerticalSpace = true;
		cbType.setLayoutData(cbTypeGridData);
		
		EEnum enumMapKind = MappingFactory.eINSTANCE.getMappingPackage().getMapKind();
		EList<EEnumLiteral> mapkindList= enumMapKind.getELiterals();
		
		Iterator<EEnumLiteral> iterTo = mapkindList.iterator();
		while (iterTo.hasNext()) {
			cbType.add(iterTo.next().getName());
		}
		cbType.select(0);
		
		Label justificationLabel = new Label(groupEquMap, SWT.NONE);
		justificationLabel.setText("Justification");
		GridData justificationLabelGridData = new GridData();
		justificationLabelGridData.horizontalAlignment = SWT.FILL;
		justificationLabelGridData.verticalAlignment = SWT.FILL;
		justificationLabel.setLayoutData(justificationLabelGridData);
		
		txtJustification = new Text(groupEquMap, SWT.MULTI);
		GridData txtJustificationGridData = new GridData();
		txtJustificationGridData.horizontalSpan = 2;
		txtJustificationGridData.horizontalAlignment = SWT.FILL;
		txtJustificationGridData.verticalAlignment = SWT.FILL;
		txtJustificationGridData.heightHint = Display.getCurrent().getBounds().height / 9;
		txtJustification.setLayoutData(txtJustificationGridData);
		
		
		//POSTCONDITIONS controls
		Group groupPostConditions = new Group(controlMapping, SWT.NULL);
		groupPostConditions.setLayout(new GridLayout(1,true));
		groupPostConditions.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true,1,1));
		groupPostConditions.setText("PostConditions");

		
		final Composite controlPostConditions = new Composite(groupPostConditions, SWT.NONE);
		{
			GridData dataNew = new GridData(SWT.FILL, SWT.FILL, true, true);
			dataNew.horizontalAlignment = SWT.END;
			dataNew.heightHint=Display.getCurrent().getBounds().height /3 -20;
			controlPostConditions.setLayoutData(dataNew);

			GridLayout layoutNew = new GridLayout();
			dataNew.horizontalAlignment = SWT.FILL;
			dataNew.verticalAlignment = SWT.FILL;
			layoutNew.marginHeight = 0;
			layoutNew.marginWidth = 0;
			layoutNew.numColumns = 2;
			controlPostConditions.setLayout(layoutNew);
		}
		final Composite controlPCCol1 = new Composite(controlPostConditions, SWT.NONE);
		{
			GridData PCCol1 = new GridData(SWT.FILL, SWT.FILL, true, true);
			PCCol1.horizontalAlignment = SWT.END;
			controlPCCol1.setLayoutData(PCCol1);
			
			GridLayout layoutNew1 = new GridLayout();
			PCCol1.horizontalAlignment = SWT.FILL;
			PCCol1.verticalAlignment = SWT.FILL;
			layoutNew1.marginHeight = 0;
			layoutNew1.marginWidth = 0;
			layoutNew1.numColumns = 1;
			controlPCCol1.setLayout(layoutNew1);
			
		}
		final Composite controlPCCol2 = new Composite(controlPostConditions, SWT.NONE);
		{
			
			GridData PCCol2 = new GridData(SWT.FILL, SWT.FILL, true, true);
			PCCol2.horizontalAlignment = SWT.END;
			controlPCCol2.setLayoutData(PCCol2);
						
			GridLayout layoutNew2 = new GridLayout();
			PCCol2.horizontalAlignment = SWT.FILL;
			PCCol2.verticalAlignment = SWT.FILL;
			layoutNew2.marginHeight = 0;
			layoutNew2.marginWidth = 0;
			layoutNew2.numColumns = 1;
			controlPCCol2.setLayout(layoutNew2);
		}
	
		/*cbComplianceMap = new Combo (controlEquivMap, SWT.READ_ONLY);
		GridData cbComplianceMapData = new GridData();
		cbComplianceMapData.verticalAlignment = SWT.FILL;
		cbComplianceMapData.horizontalAlignment = SWT.FILL;
		cbComplianceMapData.grabExcessHorizontalSpace = true;
		cbComplianceMapData.grabExcessVerticalSpace = true;
		cbComplianceMap.setLayoutData(cbComplianceMapData);*/
		
		// Label
		Label PCObligationsLabel = new Label(controlPCCol1, SWT.NONE);
		PCObligationsLabel.setText("New Obligations");
		GridData PCObligationsGridData = new GridData();
		PCObligationsGridData.horizontalAlignment = SWT.LEFT;
		PCObligationsGridData.verticalAlignment = SWT.FILL;
		PCObligationsLabel.setLayoutData(PCObligationsGridData);
		listPostConditions = new TableViewer(controlPCCol1, SWT.FILL | SWT.SINGLE | SWT.BORDER | SWT.V_SCROLL| SWT.H_SCROLL);

		listPostConditions.setContentProvider(new AdapterFactoryContentProvider(
				adapterFactory));
		listPostConditions.setLabelProvider(new AdapterFactoryLabelProvider(
				adapterFactory));
		
		listPostConditions.getTable().setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
				
		listPostConditions.addSelectionChangedListener(new ISelectionChangedListener() {
		      public void selectionChanged(SelectionChangedEvent event) {
		        IStructuredSelection selection = (IStructuredSelection)event.getSelection();
		        EObject objectPostCondition = (EObject) selection.getFirstElement();
		        if(objectPostCondition!=null){
		        	Object idFeature= objectPostCondition.eGet(objectPostCondition.eClass().getEStructuralFeature("id"));
		        	if(idFeature!=null) PCidText.setText(idFeature.toString());
		        	Object nameFeature= objectPostCondition.eGet(objectPostCondition.eClass().getEStructuralFeature("name"));
		        	if(nameFeature!=null) PCnameText.setText(nameFeature.toString());
		        	Object descFeature= objectPostCondition.eGet(objectPostCondition.eClass().getEStructuralFeature("description"));
		        	if(descFeature!=null) PCtxtdescription.setText(descFeature.toString());
		        }
		      }
		    });
		 
		//Button
		/*Button ObligationsButton = new Button(controlPCCol2, SWT.NONE);
		ObligationsButton.setText("New...");
		GridData NewObligationsGridData = new GridData();
		NewObligationsGridData.verticalAlignment = SWT.END;
		NewObligationsGridData.horizontalAlignment = SWT.NONE;
		ObligationsButton.setLayoutData(NewObligationsGridData);*/
		
					
		// Label
		Label PCidLabel = new Label(controlPCCol2, SWT.NONE);
		PCidLabel.setText("ID");
		GridData PCidLabelGridData = new GridData();
		PCidLabelGridData.horizontalAlignment = SWT.FILL;
		PCidLabelGridData.verticalAlignment = SWT.FILL;
		PCidLabel.setLayoutData(PCidLabelGridData);

		// Text
		PCidText = new Text(controlPCCol2, SWT.NONE);
		GridData PCidTextGridData = new GridData();
		PCidTextGridData.horizontalAlignment = SWT.FILL;
		PCidTextGridData.verticalAlignment = SWT.FILL;
		PCidText.setLayoutData(PCidTextGridData);
		
		
		// Label
		Label PCnameLabel = new Label(controlPCCol2, SWT.NONE);
		PCnameLabel.setText("Name");
		GridData PCnameLabelGridData = new GridData();
		PCnameLabelGridData.grabExcessHorizontalSpace = true;
		PCnameLabelGridData.minimumWidth = 20;
		PCnameLabelGridData.horizontalAlignment = SWT.FILL;
		PCnameLabelGridData.verticalAlignment = SWT.FILL;
		PCnameLabel.setLayoutData(PCnameLabelGridData);

		// Text
		PCnameText = new Text(controlPCCol2, SWT.NONE);
		GridData PCnameTextGridData = new GridData();
		PCnameTextGridData.grabExcessHorizontalSpace = true;
		PCnameTextGridData.minimumWidth = 30;
		PCnameTextGridData.horizontalAlignment = SWT.FILL;
		PCnameTextGridData.verticalAlignment = SWT.FILL;
		PCnameText.setLayoutData(nameTextGridData);
							
		
		Label PCdescriptionLabel = new Label(controlPCCol2, SWT.NONE);
		PCdescriptionLabel.setText("Description");
		GridData PCdescriptionLabelGridData = new GridData();
		PCdescriptionLabelGridData.horizontalSpan=1;
		PCdescriptionLabelGridData.horizontalAlignment = SWT.FILL;
		PCdescriptionLabelGridData.verticalAlignment = SWT.FILL;
		PCdescriptionLabel.setLayoutData(PCdescriptionLabelGridData);
		
		PCtxtdescription = new Text(controlPCCol2, SWT.MULTI);
		PCtxtdescription.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
		/*GridData PCtxtdescriptionGridData = new GridData();
		PCtxtdescriptionGridData.horizontalSpan = 1;		
		PCtxtdescriptionGridData.horizontalAlignment = SWT.FILL;
		PCtxtdescriptionGridData.verticalAlignment = SWT.FILL;
		//PCtxtdescriptionGridData.widthHint = Display.getCurrent().getBounds().width / 4;
		PCtxtdescriptionGridData.heightHint = 33;
		PCtxtdescription.setLayoutData(PCtxtdescriptionGridData);*/
	}

	protected void createTargetDialogArea(final Composite contents,
			ComposedAdapterFactory adapterFactory) {

		Composite targetComposite = new Composite(contents, SWT.NONE);
		{
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.horizontalAlignment = SWT.END;
			targetComposite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			data.horizontalAlignment = SWT.FILL;
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 1;
			targetComposite.setLayout(layout);
		}

		
		
		final Composite compositeTargetProject = new Composite(targetComposite, SWT.NONE);
		{
			GridData dataNew = new GridData(SWT.FILL, SWT.FILL, true, true);
			dataNew.horizontalAlignment = SWT.END;
			compositeTargetProject.setLayoutData(dataNew);

			GridLayout layoutNew = new GridLayout();
			dataNew.horizontalAlignment = SWT.FILL;
			layoutNew.marginHeight = 0;
			layoutNew.marginWidth = 0;
			layoutNew.numColumns = 2;
			compositeTargetProject.setLayout(layoutNew);
		}
		
	
		// Label
		targetLabel = new Label(compositeTargetProject, SWT.NONE);
		targetLabel.setText("Target Project:");
		GridData targetLabelGridData = new GridData();		
		targetLabelGridData.horizontalAlignment = SWT.FILL;
		targetLabelGridData.verticalAlignment = SWT.FILL;
		targetLabel.setLayoutData(targetLabelGridData);
		// Text
		projecttargetText = new Text(compositeTargetProject, SWT.NONE);
		
		GridData nameTextGridData = new GridData();
		nameTextGridData.grabExcessHorizontalSpace = true;
		nameTextGridData.minimumWidth = 50;
		nameTextGridData.horizontalAlignment = SWT.FILL;
		nameTextGridData.verticalAlignment = SWT.FILL;
		projecttargetText.setLayoutData(nameTextGridData);
		//projecttargetText.setText(resourceURI.toString());
		
				
		String path = "";
		for (int i = 0; i< resourceURI.segmentCount(); i++ ) {
			path = path + resourceURI.segment(i) + "/";
		}
		
		path = path.substring(0,path.length()-1);
		projecttargetText.setText(path);
		
		projecttargetText.setEditable(false);
		
		
		/* IRR model tree
		IRR: Create evidence model at the end of the process */
		try {
			//Almacenar mapgroup del target project
			String mappingPath = path.replace("assuranceproject", "mapping");
			CDOResource targetMappingModel= transaction.getResource(mappingPath);
			MapModel mm=(MapModel) targetMappingModel.getContents().get(0);
			
			if(mm.getMapGroupModel().size()>0){
				targetComplianceGroup= mm.getMapGroupModel().get(0);
			}
			
			
			resourceTarget = transaction.getResource(path);
			assuranceProjectTarget = (AssuranceProject) resourceTarget.getContents().get(0);
			String pathAssuranceProjectFolder= path.substring(0,path.lastIndexOf("/"));
			CDOResourceFolder evidenceFolder = transaction.getResourceFolder(pathAssuranceProjectFolder.substring(0,pathAssuranceProjectFolder.lastIndexOf("/")) + "/EVIDENCE");
			CreateModelEvidenceFromBaseLine(assuranceProjectTarget,evidenceFolder );			
		} catch (CDOException e) {

			e.printStackTrace();
		}
		
		
		
		//Button
		/*Button selectSourceProject = new Button(compositeTargetProject, SWT.NONE);
		selectSourceProject.setText("Search...");
		GridData NewGroupMapGridData = new GridData();
		NewGroupMapGridData.verticalAlignment = SWT.BEGINNING;
		NewGroupMapGridData.horizontalAlignment = SWT.FILL;
		selectSourceProject.setLayoutData(NewGroupMapGridData);*/
		
		
		//Load the baseline
		GridData modelViewerGridData1 = new GridData();
		modelViewerGridData1.verticalAlignment = SWT.FILL;
		modelViewerGridData1.horizontalAlignment = SWT.FILL;
		modelViewerGridData1.widthHint = Display.getCurrent().getBounds().width / 3;
		modelViewerGridData1.heightHint = (Display.getCurrent().getBounds().height / 3)-20;
		modelViewerGridData1.grabExcessHorizontalSpace = true;
		modelViewerGridData1.grabExcessVerticalSpace = true;
		
		treeBaselineTarget = new TreeViewer(targetComposite,
				SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		
		treeBaselineTarget.getTree().setLayoutData(modelViewerGridData1);
		treeBaselineTarget.setContentProvider(new AdapterFactoryContentProvider(
				adapterFactory));
		treeBaselineTarget.setLabelProvider(new AdapterFactoryLabelProvider(
				adapterFactory));

		//treeBaselineTarget.setInput(baselineTargetResourceSet);
		CreateBaselineModelFromResourceSet(treeBaselineTarget,baselineTargetResourceSet);
		
		
		
		//To search the nodes to be moved.
		treeBaselineTarget.expandAll();

		moveBaseRequirementsAboveBaseArtefacts(treeBaselineTarget);
		//To expand the moved nodes.
		//treeBaselineTarget.expandAll();
		
		BasicCommandStack commandStackBL = new BasicCommandStack();
		EditingDomain editingDomainBL = new AdapterFactoryEditingDomain(adapterFactory, commandStackBL, new HashMap<Resource, Boolean>());
		treeBaselineTarget.addDoubleClickListener(new OpenWizardOnDoubleClick(editingDomainBL, adapterFactory));
		
		
		//Compliance map controls
		Group groupTargetCM = new Group(targetComposite, SWT.NULL);
		groupTargetCM.setLayout(new GridLayout(1,true));
		groupTargetCM.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true,1,1));
		groupTargetCM.setText("Target Compliance Map");

		
		final Composite controlTCM = new Composite(groupTargetCM, SWT.NONE);
		{
			GridData dataNew = new GridData(SWT.FILL, SWT.FILL, true, true);
			dataNew.horizontalAlignment = SWT.END;
			controlTCM.setLayoutData(dataNew);

			GridLayout layoutNew = new GridLayout();
			dataNew.horizontalAlignment = SWT.FILL;
			dataNew.verticalAlignment = SWT.FILL;
			layoutNew.marginHeight = 0;
			layoutNew.marginWidth = 0;
			layoutNew.numColumns = 2;
			controlTCM.setLayout(layoutNew);
		}
		final Composite controlTCMCol1 = new Composite(controlTCM, SWT.NONE);
		{
			GridData TCMCol1 = new GridData(SWT.FILL, SWT.FILL, true, true);
			TCMCol1.horizontalAlignment = SWT.END;
			controlTCMCol1.setLayoutData(TCMCol1);
			
			GridLayout layoutNew1 = new GridLayout();
			TCMCol1.horizontalAlignment = SWT.FILL;
			TCMCol1.verticalAlignment = SWT.FILL;
			layoutNew1.marginHeight = 0;
			layoutNew1.marginWidth = 0;
			layoutNew1.numColumns = 2;
			controlTCMCol1.setLayout(layoutNew1);
			
		}
		final Composite controlTCMCol2 = new Composite(controlTCM, SWT.NONE);
		{
			
			GridData TCMCol2 = new GridData(SWT.FILL, SWT.FILL, true, true);
			TCMCol2.horizontalAlignment = SWT.END;
			controlTCMCol2.setLayoutData(TCMCol2);
						
			GridLayout layoutNew2 = new GridLayout();
			TCMCol2.horizontalAlignment = SWT.FILL;
			TCMCol2.verticalAlignment = SWT.FILL;
			layoutNew2.marginHeight = 0;
			layoutNew2.marginWidth = 0;
			layoutNew2.numColumns = 2;
			controlTCMCol2.setLayout(layoutNew2);
		}
		
		/*cbComplianceMap = new Combo (controlEquivMap, SWT.READ_ONLY);
		GridData cbComplianceMapData = new GridData();
		cbComplianceMapData.verticalAlignment = SWT.FILL;
		cbComplianceMapData.horizontalAlignment = SWT.FILL;
		cbComplianceMapData.grabExcessHorizontalSpace = true;
		cbComplianceMapData.grabExcessVerticalSpace = true;
		cbComplianceMap.setLayoutData(cbComplianceMapData);*/
		
		
				
		
		// Label
		Label TCMidLabel = new Label(controlTCMCol1, SWT.NONE);
		TCMidLabel.setText("ID");
		GridData TCMidLabelGridData = new GridData();
		TCMidLabelGridData.horizontalAlignment = SWT.FILL;
		TCMidLabelGridData.verticalAlignment = SWT.FILL;
		TCMidLabel.setLayoutData(TCMidLabelGridData);

		// Text
		TCMidText = new Text(controlTCMCol1, SWT.NONE);
		GridData TCMidTextGridData = new GridData();
		TCMidTextGridData.horizontalAlignment = SWT.FILL;
		TCMidTextGridData.verticalAlignment = SWT.FILL;
		TCMidText.setLayoutData(TCMidTextGridData);
		
		
		// Label
		Label TCMnameLabel = new Label(controlTCMCol1, SWT.NONE);
		TCMnameLabel.setText("Name");
		GridData TCMnameLabelGridData = new GridData();
		TCMnameLabelGridData.grabExcessHorizontalSpace = true;
		TCMnameLabelGridData.minimumWidth = 20;
		TCMnameLabelGridData.horizontalAlignment = SWT.FILL;
		TCMnameLabelGridData.verticalAlignment = SWT.FILL;
		TCMnameLabel.setLayoutData(TCMnameLabelGridData);

		// Text
		TCMnameText = new Text(controlTCMCol1, SWT.NONE);
		GridData TCMnameTextGridData = new GridData();
		TCMnameTextGridData.grabExcessHorizontalSpace = true;
		TCMnameTextGridData.minimumWidth = 30;
		TCMnameTextGridData.horizontalAlignment = SWT.FILL;
		TCMnameTextGridData.verticalAlignment = SWT.FILL;
		TCMnameText.setLayoutData(nameTextGridData);
		
		
		// Text
		Label TCMtypeLabel = new Label(controlTCMCol1, SWT.NONE);
		TCMtypeLabel.setText("Type");
		GridData TCMtypeLabelGridData = new GridData();
		TCMtypeLabelGridData.grabExcessHorizontalSpace = true;
		
		TCMtypeLabelGridData.horizontalAlignment = SWT.FILL;
		TCMtypeLabelGridData.verticalAlignment = SWT.FILL;
		TCMtypeLabel.setLayoutData(TCMtypeLabelGridData);
		
		TCMcbType = new Combo (controlTCMCol1, SWT.READ_ONLY);
		GridData TCMcbTypeGridData = new GridData();
		TCMcbTypeGridData.verticalAlignment = SWT.FILL;
		TCMcbTypeGridData.horizontalAlignment = SWT.FILL;
		//TCMcbTypeGridData.widthHint = Display.getCurrent().getBounds().width / 7;
		//TCMcbTypeGridData.heightHint = Display.getCurrent().getBounds().height / 7;
		TCMcbTypeGridData.grabExcessHorizontalSpace = true;
		TCMcbTypeGridData.grabExcessVerticalSpace = true;
		TCMcbType.setLayoutData(TCMcbTypeGridData);
		
		EEnum enumMapKind = MappingFactory.eINSTANCE.getMappingPackage().getMapKind();
		EList<EEnumLiteral> mapkindList= enumMapKind.getELiterals();
		
		Iterator<EEnumLiteral> iterTo = mapkindList.iterator();
		while (iterTo.hasNext()) {
			TCMcbType.add(iterTo.next().getName());
		}
		TCMcbType.select(0);						
		
		Label TCMjustificationLabel = new Label(controlTCMCol2, SWT.NONE);
		TCMjustificationLabel.setText("Justification");
		GridData TCMjustificationLabelGridData = new GridData();		
		TCMjustificationLabelGridData.horizontalAlignment = SWT.FILL;
		TCMjustificationLabelGridData.verticalAlignment = SWT.FILL;
		TCMjustificationLabel.setLayoutData(TCMjustificationLabelGridData);
		
		//Button
		Button newCMButton = new Button(controlTCMCol2, SWT.NONE);
		newCMButton.setText("New CM");
		GridData newCMGridData = new GridData();
		newCMGridData.verticalAlignment = SWT.END;
		newCMGridData.horizontalAlignment =SWT.END;
		newCMButton.setLayoutData(newCMGridData);
		
		
		newCMButton.addSelectionListener(new SelectionAdapter() {

			/**
			 * {@inheritDoc}
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			public void widgetSelected(SelectionEvent e) {

				PlatformUI.getWorkbench().saveAllEditors(true);
				MappingSet ComplianceMappingSet = new MappingSet(getShell(), baselineTargetResourceSet.getResources().get(0).getURI());
				ComplianceMappingSet.open();
			}
		});
	
		
		
		TCMtxtJustification = new Text(controlTCMCol2, SWT.MULTI);
		TCMtxtJustification.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true,2,1));
		/*GridData TCMtxtJustificationGridData = new GridData();
		TCMtxtJustificationGridData.horizontalSpan = 2;		
		TCMtxtJustificationGridData.horizontalAlignment = SWT.FILL;
		TCMtxtJustificationGridData.verticalAlignment = SWT.FILL;
		
		TCMtxtJustification.setLayoutData(TCMtxtJustificationGridData);*/
		
		
		
		//Load the evidence models
		GridData modelViewerGridData2 = new GridData();
		modelViewerGridData2.verticalAlignment = SWT.FILL;
		modelViewerGridData2.horizontalAlignment = SWT.FILL;
		modelViewerGridData2.widthHint = Display.getCurrent().getBounds().width / 3;
		modelViewerGridData2.heightHint = (Display.getCurrent().getBounds().height / 3) - 20;
		modelViewerGridData2.grabExcessHorizontalSpace = true;
		modelViewerGridData2.grabExcessVerticalSpace = true;
		
		treeEvidenceTarget = new TreeViewer(targetComposite,
				SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		
		treeEvidenceTarget.getTree().setLayoutData(modelViewerGridData2);
		treeEvidenceTarget.setContentProvider(new AdapterFactoryContentProvider(
				adapterFactory));
		treeEvidenceTarget.setLabelProvider(new AdapterFactoryLabelProvider(
				adapterFactory));

		CreateModelFromResourceSet(treeEvidenceTarget,evidenceTargetResourceSet);
						
		treeEvidenceTarget.expandAll();			
		
		
		
		BasicCommandStack commandStackET = new BasicCommandStack();
		EditingDomain editingDomainET = new AdapterFactoryEditingDomain(adapterFactory, commandStackET, new HashMap<Resource, Boolean>());
		treeEvidenceTarget.addDoubleClickListener(new OpenWizardOnDoubleClick(editingDomainET, adapterFactory));
		
		
		treeBaselineTarget.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				
				if(event.getSelection().isEmpty()) {
					System.out.println("Empty");
					return;
				}
				
				
				object = null;
				if(event.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection sel = (IStructuredSelection) event.getSelection();
					object = (EObject) sel.getFirstElement();
					
					
					
					clearAllControls();
					
					//Clean the source and target baseline and evidence tree green nodes
					
					if(checktreeEvidenceSource.getTree().getItemCount()>0){
												
																																				
					}
					else{
						if(projectsourceText.getText().length()==0){
							MessageDialog.openError(getShell(), "Selected Source Project",
								"You must select the source project of the reuse.");
						}
						else{
							MessageDialog.openError(getShell(), "Wrong Source Project",
									"The selected source project doesn't have evidence model.");
						}
						return;
					}
					
					// 
					if ((object instanceof BaseFramework) || 
							(object instanceof CDOResource)) {						
						return;
					} 
					
					if (object instanceof EObject) {
						//if (sElementFilter.contentEquals("Artefact")) {

						if( cbMapGroup.getSelectionIndex()==-1){
							MessageDialog.openError(getShell(), "Selected Map Group",
									"You must select the equivalence map group.");
							return;
						}

						
						EObject selectedObject= object;////He de mirar de quien es target

						
						if(selectedObject instanceof BaseAssurableElement){
							BaseAssurableElement targetBaseAssurableElement=(BaseAssurableElement)selectedObject;

							//Read the compliance compliance  map of the selected target baseArtefact 
							EList<BaseComplianceMap> lstTargetComplianceMap = targetBaseAssurableElement.getComplianceMap();
							Iterator<BaseComplianceMap> itTargetComplianceMap = lstTargetComplianceMap.iterator();
							while (itTargetComplianceMap.hasNext()) {

								//Fill the target the compliance controls
								BaseComplianceMap targetComplianceMap = itTargetComplianceMap.next();
								
								//Estos son los target que me interesan
								if(targetComplianceMap.getId()!=null){
									TCMidText.setText(targetComplianceMap.getId());
								}
								else{
									TCMidText.setText("");
								}
								
								if(targetComplianceMap.getName()!=null){
									TCMnameText.setText(targetComplianceMap.getName());		
								}
								else{
									TCMnameText.setText("");
								}
								if(targetComplianceMap.getMapJustification()!=null){
									if(targetComplianceMap.getMapJustification().getExplanation()!=null){
										TCMtxtJustification.setText(targetComplianceMap.getMapJustification().getExplanation());
									}
								}	
								else{
									TCMtxtJustification.setText("");
								}
								
																															
								if(targetComplianceMap.getType()!=null){
									EEnum enumMapKind = MappingFactory.eINSTANCE.getMappingPackage().getMapKind();
									EList<EEnumLiteral> mapkindList= enumMapKind.getELiterals();

									Iterator<EEnumLiteral> iterTo = mapkindList.iterator();
									int index=0;
									while (iterTo.hasNext()) {
										if(iterTo.next().getName().equals(targetComplianceMap.getType().getLiteral())){
											break;
										}
										index++;
									}																				

									TCMcbType.select(index);
								}

								if(targetComplianceMap.getTarget().size()>0){
									Tree modifyTree =treeEvidenceTarget.getTree();
									TreeItem[] rootNodes = modifyTree.getItems();
									boolean found =false;
									//For each target in compliance search in evidence tree
									for(AssuranceAsset targetEvidence:targetComplianceMap.getTarget()){
										for(TreeItem root:rootNodes){
											for(int x=0; x <root.getItems().length;x++){
												TreeItem  oneItem = root.getItems()[x];
												found = searchInTreeNodes(targetEvidence,oneItem);				
												if (found) break;
											}

											if (found) break;
										}
									}
								}
							}



							//Read the  equivalence map of the selected target baseArtefact 
							EList <RefAssurableElement> equivalenceRefAssurableElement = searchRefAssurableElements(targetBaseAssurableElement);																																													

							//Search the postConditions. The source model is the same than the equivalence map
							for(RefAssurableElement ra:equivalenceRefAssurableElement){
								CDOResource target = ra.cdoResource();
								if(target.equals((targetBaseAssurableElement.getRefAssurableElement()).cdoResource())){
									//Search it in the target Baseline tree

									Tree modifyTreeSB =treeBaselineTarget.getTree();
									TreeItem[] rootNodesSB = modifyTreeSB.getItems();
									boolean foundSB =false;
									for(TreeItem root:rootNodesSB){
										for(int x=0; x <root.getItems().length;x++){
											TreeItem  oneItem = root.getItems()[x];
											foundSB = searchPostConditionsTBTree(ra,oneItem);				
											if (foundSB) break;
										}

										if (foundSB) break;											

									}
								}
							}


							//Load the evidence models of the active assetsPackage of the selected source assurance project 
							EList<BaselineConfig> lstBaselineConfig = assuranceProjectFrom.getBaselineConfig();
							Iterator<BaselineConfig> baselineConfig = lstBaselineConfig.iterator();
							boolean bFind=false;
							while (baselineConfig.hasNext() && !bFind) {
								BaselineConfig bconfig = baselineConfig.next();
								if (bconfig.isIsActive()) {
									bFind = true;

									EList<BaseFramework> lstBaseFramework = bconfig.getRefFramework();
									Iterator<BaseFramework> itbaseFramework = lstBaseFramework.iterator();

									while (itbaseFramework.hasNext()) {
										BaseFramework sourceFramework = itbaseFramework.next();

										//EList<BaseArtefact> lstBaseArtefact =sourceFramework.getOwnedArtefact();

										EList<BaseAssurableElement> lstBaseAssurableElement = getAllBaseAssurableElements(sourceFramework);

										Iterator<BaseAssurableElement> itbaseBAE = lstBaseAssurableElement.iterator();

										//Search the baseArtefacts of the source project according to the equivalence map
										while (itbaseBAE.hasNext()){
											BaseAssurableElement sourceBAE = itbaseBAE.next();

											//if (sourceBaseArtefact.isIsSelected()) {
											RefAssurableElement sourceRAE= (RefAssurableElement)sourceBAE.getRefAssurableElement();

											for(RefAssurableElement ra:equivalenceRefAssurableElement){

												//Rellenar la parte central:																													
												//RefEquivalenceMap rm = sourceRefArtefact.getEquivalence().get(0);
												//txtJustification.setText(rm.getMapJustification().getExplanation());
												//Read the compliance map of the equivalence source BaseArtefacts of the surce BaseArtefacts to look for the Artefacts
												if (ra.cdoID()==sourceRAE.cdoID()){		


													Tree modifyTreeSB =treeBaselineSource.getTree();
													TreeItem[] rootNodesSB = modifyTreeSB.getItems();
													boolean foundSB =false;
													for(TreeItem root:rootNodesSB){
														for(int x=0; x <root.getItems().length;x++){
															TreeItem  oneItem = root.getItems()[x];
															foundSB = searchInTreeNodes(sourceBAE,oneItem);				
															if (foundSB) break;
														}

														if (foundSB) break;
													}

													EList<BaseComplianceMap> lstBaseComplianceMap = sourceBAE.getComplianceMap();
													Iterator<BaseComplianceMap> itbaseComplianceMap = lstBaseComplianceMap.iterator();
													while (itbaseComplianceMap.hasNext()) {
														BaseComplianceMap baseComplianceMap = itbaseComplianceMap.next();
														//Estos son los target que me interesan
														if(baseComplianceMap.getId()!=null){
															SCMidText.setText(baseComplianceMap.getId());
														}
														else{
															SCMidText.setText("");
														}
														
														if(baseComplianceMap.getName()!=null){
															SCMnameText.setText(baseComplianceMap.getName());	
														}
														else{
															SCMnameText.setText("");
														}
														if(baseComplianceMap.getMapJustification()!=null){
															if(baseComplianceMap.getMapJustification().getExplanation()!=null){
																SCMtxtJustification.setText(baseComplianceMap.getMapJustification().getExplanation());
															}
														}	
														else{
															SCMtxtJustification.setText("");
														}
														
																																																								
														if(baseComplianceMap.getType()!=null){
															EEnum enumMapKind = MappingFactory.eINSTANCE.getMappingPackage().getMapKind();
															EList<EEnumLiteral> mapkindList= enumMapKind.getELiterals();

															Iterator<EEnumLiteral> iterTo = mapkindList.iterator();
															int index=0;
															while (iterTo.hasNext()) {
																if(iterTo.next().getName().equals(baseComplianceMap.getType().getLiteral())){
																	break;
																}
																index++;
															}																				

															SCMcbType.select(index);
														}

														EList<AssuranceAsset> lstArtefacts = baseComplianceMap.getTarget();
														Iterator<AssuranceAsset> itArtefacts = lstArtefacts.iterator();

														Tree modifyTree =checktreeEvidenceSource.getTree();
														TreeItem[] rootNodes = modifyTree.getItems();
														boolean found =false;

														while (itArtefacts.hasNext()){
															Artefact equivArtefact = (Artefact)itArtefacts.next();																	
															//Search the equivalence Artefacts in the treeview showing the  evidence model of the source project																																																																	
															for(TreeItem root:rootNodes){
																for(int x=0; x <root.getItems().length;x++){
																	TreeItem  oneItem = root.getItems()[x];
																	found = searchInTreeNodes(equivArtefact,oneItem);				
																	if (found) break;
																}

																if (found) break;
															}																																		
														}
													}
													break;
												}
											}																																														
										}
									}																																																
								}
							}																								
						}																				 	
					}	
				}
			}												

		});				
		
		//Button
		Button saveTargetEvidences = new Button(targetComposite, SWT.NONE);
		saveTargetEvidences.setText("Save target evidences");
		GridData NewGroupMapGridData = new GridData();
		NewGroupMapGridData.verticalAlignment = SWT.BEGINNING;
		NewGroupMapGridData.horizontalAlignment = SWT.END;
		saveTargetEvidences.setLayoutData(NewGroupMapGridData);

		saveTargetEvidences.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				for(Resource evRes :evidenceTargetResourceSet.getResources()){
					CDOResource savRes= (CDOResource)evRes;
					try {
						savRes.save(options);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}				
				}
				MessageDialog.openInformation(getShell(), "Evidences saved", "Target changes evidences saved.");
			}							
			});
		
	}
		
	
	protected EList<BaseAssurableElement> getAllBaseAssurableElements(BaseFramework sourceFramework) {
		EList<BaseAssurableElement> listResult = new BasicEList<BaseAssurableElement>();
		CDOResource baselineSourceRes= sourceFramework.cdoResource();		
		for (Iterator<EObject> iter=baselineSourceRes.getAllContents();iter.hasNext();) {					
			EObject aEObject= iter.next();				
			if(aEObject instanceof BaseAssurableElement){
				listResult.add((BaseAssurableElement)aEObject);
			}											
		}		
		return listResult;
	}
			
	
	private boolean searchInTreeNodes(CDOObject searchObject, TreeItem root) {
		for(int x=0; x <root.getItems().length;x++){
			TreeItem  oneItem = root.getItems()[x];
			
			Object dataObject = oneItem.getData();
			if(dataObject instanceof CDOObject){
				if(((CDOObject)dataObject).cdoID()==searchObject.cdoID()){
					oneItem.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_GREEN));
					oneItem.setChecked(true);
					listGreenItem.add(oneItem);
					return true;
				}
			}
			if(oneItem.getItems().length>0){
				boolean found=searchInTreeNodes( searchObject,  oneItem);
				if (found) return true;
			}					
		}
		return false;
	}	
		
	
	private boolean searchPostConditionsTBTree(RefAssurableElement searchObject, TreeItem root) {
		for(int x=0; x <root.getItems().length;x++){
			TreeItem  oneItem = root.getItems()[x];
			
			Object dataObject = oneItem.getData();
			if(dataObject instanceof BaseAssurableElement){
				BaseAssurableElement treeObject= (BaseAssurableElement)dataObject;
				
				if((treeObject.getRefAssurableElement()).cdoID()==searchObject.cdoID()){
					//Object nameFeature= treeObject.eGet(treeObject.eClass().getEStructuralFeature("name"));	
					//String className= treeObject.toString().split("@")[0];
					listPostConditions.add(treeObject);
					//refListPost.getItem(refList.getItemCount()).
					return true;
				}
			}
			if(oneItem.getItems().length>0){
				boolean found=searchPostConditionsTBTree( searchObject,  oneItem);
				if (found) return true;
			}					
		}
		return false;
	}
	
	//Obtain all the equivalence RefAssurableElements of the the selected target BaseAssurableElement
	private EList <RefAssurableElement> searchRefAssurableElements(BaseAssurableElement targetBaseAssurableElement){
		
		EList<BaseEquivalenceMap> lstBaseEquivalenceMap = targetBaseAssurableElement.getEquivalenceMap();
		Iterator<BaseEquivalenceMap> itbaseEquivalenceMap = lstBaseEquivalenceMap.iterator();
		EList <RefAssurableElement> equivalenceRefArtefacts= new BasicEList<RefAssurableElement>();
		while (itbaseEquivalenceMap.hasNext()) {
			BaseEquivalenceMap baseEquivalenceMap = itbaseEquivalenceMap.next();
			MapGroup mapGroup = (MapGroup) baseEquivalenceMap.getMapGroup();
			if(mapGroup.cdoID()==(lstMapGroup.get(cbMapGroup.getSelectionIndex())).cdoID()){
				//Estos son los target que me interesan
				if(baseEquivalenceMap.getId()!=null){
					idText.setText(baseEquivalenceMap.getId());
				}
				else{
					idText.setText("");
				}
				
				if(baseEquivalenceMap.getName()!=null){
					nameText.setText(baseEquivalenceMap.getName());
				}
				else{
					nameText.setText("");
				}
				if(baseEquivalenceMap.getMapJustification()!=null){
					if(baseEquivalenceMap.getMapJustification().getExplanation()!=null){
						txtJustification.setText(baseEquivalenceMap.getMapJustification().getExplanation());
					}
				}
				else{
					txtJustification.setText("");
				}
				
				if(baseEquivalenceMap.getType()!=null){
					EEnum enumMapKind = MappingFactory.eINSTANCE.getMappingPackage().getMapKind();
					EList<EEnumLiteral> mapkindList= enumMapKind.getELiterals();
					
					Iterator<EEnumLiteral> iterTo = mapkindList.iterator();
					int index=0;
					while (iterTo.hasNext()) {
						if(iterTo.next().getName().equals(baseEquivalenceMap.getType().getLiteral())){
							break;
						}
						index++;
					}																				
					
					cbType.select(index);
				}
				equivalenceRefArtefacts.addAll(baseEquivalenceMap.getTarget());	
				//Only search one equivalence map, if found, not read more
				break;
														
			}										
		}
		return equivalenceRefArtefacts;
	}
	private int isEquivalenceArtefact(Object equivArtefact, TreeItem root) {
		for(int x=0; x <root.getItems().length;x++){
			TreeItem  oneItem = root.getItems()[x];					
			Object dataObject = oneItem.getData();
			if(dataObject.equals(equivArtefact)){						
				if (oneItem.getBackground().equals(Display.getCurrent().getSystemColor(SWT.COLOR_GREEN))){
					return 1;
				}
				else{
					return 2;
				}													
			}
			if(oneItem.getItems().length>0){
				int found=isEquivalenceArtefact( equivArtefact,  oneItem);
				if (found !=0 ) return found;
			}					
		}
		return 0;
	}	



	

	public void CreateModelFromFiletocombo(Combo combo,String spath) {
		
		
		if (spath != null) {	
			CDOResource resource = null;
			try {
				
				resource = viewCDO.getResource(spath, true);
				mapModel = null;
				mapModel = (MapModel) resource.getContents().get(0);
				
				combo.removeAll();
				lstMapGroup = mapModel.getMapGroupModel();

				Iterator<MapGroup> itermapGroup = lstMapGroup.iterator();
				while (itermapGroup.hasNext()) {
					combo.add(itermapGroup.next().getName());
				}
			}
			catch (Exception ex) {
				MessageDialog.openError(getShell(), "Not valid model file", "The provided model file couldn't be parsed as an EMF resource");
			}
		}
	}
	
	
	
	

	
	public void CreateModelFromFile(CDOResource resource) {

		try {
			checktreeEvidenceSource.setInput(resource);
			final ViewerFilter modelFilter = new ViewerFilter() {
				public boolean select(Viewer viewer, Object parentElement,
						Object element) {
					//if (sElementFilter.contentEquals("Artefact")) {
						if (element instanceof ArtefactModel)
							return true;
						else if (element instanceof ArtefactDefinition)
							return true;
						else if (element instanceof Artefact)
							return true;
						else if (element instanceof org.eclipse.opencert.evm.evidspec.evidence.Resource)
							return true;
						else
							return false;
				//	} else
				//		return false;
				}
			};
			checktreeEvidenceSource.addFilter(modelFilter);
		} catch (Exception ex) {
			MessageDialog
					.openError(getShell(), "Not valid model file",
							"The provided model file couldn't be parsed as an EMF resource");
			checktreeEvidenceSource.setInput(null);
		}
	}

	
	public void CreateModelFromResourceSet(TreeViewer tree ,ResourceSet resourceSet) {

		try {
			tree.setInput(resourceSet);
			
			final ViewerFilter modelFilter = new ViewerFilter() {
				public boolean select(Viewer viewer, Object parentElement,
						Object element) {
					
					//if (sElementFilter.contentEquals("Artefact")) {
						if (element instanceof CDOResource) 
							return true;
						else if (element instanceof ArtefactModel)
							return true;
						else if (element instanceof ArtefactDefinition)
							return true;
						else if (element instanceof Artefact)
							return true;
						/*else if (element instanceof org.eclipse.opencert.evm.evidspec.evidence.Resource)
							return true;*/
						else
							return false;
					//} else
					//	return false;
				}
			};
			tree.addFilter(modelFilter);
		} catch (Exception ex) {
			MessageDialog
					.openError(getShell(), "Not valid model file",
							"The provided model file couldn't be parsed as an EMF resource");
			tree.setInput(null);
		}
	}
	
	
	public void CreateBaselineModelFromResourceSet(TreeViewer tree, ResourceSet resourceSet) {

		try {
			tree.setInput(resourceSet);
			
			final ViewerFilter modelFilter = new ViewerFilter() {
				public boolean select(Viewer viewer, Object parentElement,
						Object element) {
										
						if (element instanceof CDOResource) 
							return true;
						else if (element instanceof BaseFramework)
							return true;
						else if (element instanceof BaseActivity|| element instanceof BaseActivityRel)
							return true;
						else if (element instanceof BaseArtefact || element instanceof BaseArtefactRel)
							return true;
						else if (element instanceof BaseRequirement || element instanceof BaseRequirementRel)
							return true;
						else if (element instanceof BaseRole)
							return true;
						else if (element instanceof BaseTechnique)
							return true;
						/*else if (element instanceof org.eclipse.opencert.evm.evidspec.evidence.Resource)
							return true;*/
						else
							return false;					
				}
			};
			tree.addFilter(modelFilter);
		} catch (Exception ex) {
			MessageDialog
					.openError(getShell(), "Not valid model file",
							"The provided model file couldn't be parsed as an EMF resource");
			tree.setInput(null);
		}
	}
	
    //Tambien deberia crear un compliance mapp en memoria
	public void CreateModelEvidenceFromBaseLine(AssuranceProject assuranceProject,CDOResourceFolder evidenceFolder) {
		
		AssetsPackage aPackage = null; //Active Assets Package 
		boolean bFind = false;
		boolean bEquivalenceMap = false;
		
		boolean reuse=false;
		
		
		evidenceTargetResourceSet = new ResourceSetImpl();
		baselineTargetResourceSet = new ResourceSetImpl();
		
		EList<AssetsPackage> lstAssetsPackage = assuranceProject.getAssetsPackage();
		Iterator<AssetsPackage> assetsPackage = lstAssetsPackage.iterator();
		
		while (assetsPackage.hasNext() && !bFind) {
			aPackage = assetsPackage.next();
			if (aPackage.isIsActive()) {
				bFind=true;
				if(aPackage.getArtefactsModel().size()>0){
										
					MessageDialog dialog = new MessageDialog(getShell(), "Reuse evidence models", null,
						    "The target project already has evidence model/s. Do you want to reuse it/them?", MessageDialog.CONFIRM, new String[] { "YES",
						  "NO" }, 0);
						int result = dialog.open();
					
					if (result==0){
						reuse=true;
						for(ArtefactModel exitingTargetAM:aPackage.getArtefactsModel()){
							evidenceTargetResourceSet.getResources().add(exitingTargetAM.eResource());
						}
						
					}					
				}
				if(reuse==false){
					MessageDialog dialog2 = new MessageDialog(getShell(), "New evidence model will be created", null,
						    "A new evidence model assigned to the target project will be created in the Repository. Do you agree?", MessageDialog.CONFIRM, new String[] { "YES",
						  "NO" }, 0);
						int result = dialog2.open();
						//If doesn't want close the crossdomain window
						if (result==1){
							this.close();
							return;
						}
				}
			}
		}
		
		if (bFind) {
			EList<BaselineConfig> lstBaselineConfig = assuranceProjectTarget.getBaselineConfig();
			Iterator<BaselineConfig> baselineConfig = lstBaselineConfig.iterator();

			bFind = false;
			while (baselineConfig.hasNext() && !bFind) {
				BaselineConfig bconfig = baselineConfig.next();
				if (bconfig.isIsActive()) {
					bFind = true;
					
					EList<BaseFramework> lstBaseFramework = bconfig.getRefFramework();										
					Iterator<BaseFramework> itbaseFramework = lstBaseFramework.iterator();
					
					int i = 1;
					while (itbaseFramework.hasNext()) {
						
						
						BaseFramework baseFramework = itbaseFramework.next();
						
						baselineTargetResourceSet.getResources().add(baseFramework.eResource());
						
						if(!reuse){
						//Check resourceContents
							CDOResource targetevidenceResource=null;

							String existingPath="";
							boolean newNameFound=false;
							EObject rootObjectArtefactModel =null;
							for (CDOResourceNode oneResource:evidenceFolder.getNodes()){
								//The model exists create a new one
								if(oneResource.getPath().endsWith("Evidence_" + i + ".evidence")){								
									existingPath=oneResource.getPath();																

									//Looking for a not existing evidence model name to create a new one
									int suma= i;
									while (true){

										suma=suma +1;
										int cut= existingPath.lastIndexOf("_");
										existingPath=existingPath.substring(0,cut+1) + suma + ".evidence";
										try{
											//To check if exists
											viewCDO.getResource(existingPath);
										}
										catch(Exception e){
											targetevidenceResource = evidenceFolder.addResource("Evidence_" + suma + ".evidence");
											rootObjectArtefactModel = createArtefactModel(suma, baseFramework); 
											newNameFound=true;
											break;
										}															
									}	
									if(newNameFound) break;
								}
							}
							if(!newNameFound){
								targetevidenceResource = evidenceFolder.addResource("Evidence_" + i + ".evidence"); 
								rootObjectArtefactModel = createArtefactModel(i, baseFramework); 							
							}

							targetevidenceResource.getContents().add(rootObjectArtefactModel); 
							aPackage.getArtefactsModel().add((ArtefactModel) rootObjectArtefactModel);

							evidenceTargetResourceSet.getResources().add(targetevidenceResource);
							
							try {
								//Save the target evidencence model
								targetevidenceResource.save(options);
								
								//Save the assurance projec
								aPackage.cdoResource().save(options);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
								
							
						}
						
						
						//For the BaseFramework find the BaseEquivalenceMap
						
						bEquivalenceMap = FindBaseFrameworkMapping(baseFramework);
						
						i++;
					}
					
					if (!bEquivalenceMap) {
						MessageDialog
						.openError(getShell(), "Not Equivalence Map",
								"The Assurance Project " + assuranceProject.getName() + " doesn't have equivalence map.");
					}
					
				}
			}
			if (! bFind) {
				MessageDialog
				.openError(getShell(), "Not Active BaseLine",
						"The Assurance Project " + assuranceProject.getName() + " doesn't have an active Base Line.");
			}
			
		} else {
			MessageDialog
			.openError(getShell(), "Not Active AssetsPackage",
					"The Assurance Project " + assuranceProject.getName() + " doesn't have an active assets package.");
		}
				
		//return resourceSetEvidence;
		
	}
	
	
	protected EObject createArtefactModel(int i,BaseFramework baseFramework ) {
		
		EClass eClass = (EClass)evidencePackage.getEClassifier(ARTEFACTMODEL_CLASS);
		EObject rootObject = evidenceFactory.create(eClass);
		((org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel)rootObject).setId("" + i);
		((org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel)rootObject).setName("ArtefactModel_" + i);
		
		EList<BaseArtefact> lstBaseArtefact =baseFramework.getOwnedArtefact();
		Iterator<BaseArtefact> itbaseArtefact = lstBaseArtefact.iterator();
		int j = 1;
		while (itbaseArtefact.hasNext()){
			BaseArtefact targetBaseArtefact = itbaseArtefact.next();
						
			if (targetBaseArtefact.isIsSelected()) {
				EClass eArtefactDefinition = (EClass)evidencePackage.getEClassifier(ARTEFACTDEFINITION_CLASS);
				EObject artefactDefinitionObject = evidenceFactory.create(eArtefactDefinition);
				((org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition)artefactDefinitionObject).setId(targetBaseArtefact.getId());
				//((org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition)artefactDefinitionObject).setName("ArtefactDefinition_" + j);
				((org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition)artefactDefinitionObject).setName(targetBaseArtefact.getName());
				((org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel)rootObject).getArtefact().add((org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition)artefactDefinitionObject);
				 
				EClass eArtefact = (EClass)evidencePackage.getEClassifier(ARTEFACT_CLASS);
				EObject artefactObject = evidenceFactory.create(eArtefact);
				((org.eclipse.opencert.evm.evidspec.evidence.Artefact)artefactObject).setId("1");
				((org.eclipse.opencert.evm.evidspec.evidence.Artefact)artefactObject).setName("Artefact version 1");
				((org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition)artefactDefinitionObject).getArtefact().add((org.eclipse.opencert.evm.evidspec.evidence.Artefact)artefactObject);							
				((org.eclipse.opencert.evm.evidspec.evidence.Artefact)artefactObject).setVersionID("1");
				((org.eclipse.opencert.evm.evidspec.evidence.Artefact)artefactObject).setIsLastVersion(true);
				///Create a compliance map between the target baseline and the evidence model to generate
				BaseComplianceMap targetProjectCM= baselineFactory.createBaseComplianceMap();
				targetProjectCM.setId(""+j);
				targetProjectCM.setName("Cross Standard compliance "+j);
				MapJustification mj= mappingFactory.createMapJustification();
				mj.setExplanation("Compliance generated automatically by Cross Standard");
				targetProjectCM.setMapJustification(mj);
				targetProjectCM.getTarget().add((org.eclipse.opencert.evm.evidspec.evidence.Artefact)artefactObject);
				targetProjectCM.setMapGroup(targetComplianceGroup);
				
				
				//Obtener el mapgroup del compliance del assurance project								
				targetBaseArtefact.getComplianceMap().add(targetProjectCM);				
				
				j++;		 
			}
		}
		return rootObject;
	}
	
	

	//For Artefacts and for Requirements only, because are the Base elements related to Artefacts
	private boolean FindBaseFrameworkMapping(BaseFramework baseFramework) {
		boolean bFind = false;

		//if (sElementFilter.contentEquals("Artefact")) {
			EList<BaseArtefact> lstBaseArtefact = baseFramework.getOwnedArtefact();
			Iterator<BaseArtefact> itbaseArtefact = lstBaseArtefact.iterator();

			while (itbaseArtefact.hasNext()) {
				BaseArtefact baseArtefact = itbaseArtefact.next();

				EList<BaseEquivalenceMap> lstBaseEquivalenceMap = baseArtefact.getEquivalenceMap();
				Iterator<BaseEquivalenceMap> itbaseEquivalenceMap = lstBaseEquivalenceMap.iterator();
				while (itbaseEquivalenceMap.hasNext()) {
					BaseEquivalenceMap baseEquivalenceMap = itbaseEquivalenceMap.next();
					MapModel mapModel = (MapModel) baseEquivalenceMap.getMapGroup().eContainer();
					mapResource = CDOUtil.getCDOObject(mapModel).cdoResource();

					// Antes de a�adir comprobar que no exista ya en la lista
					if (!VerifyMapResource(mapResource.getPath())) {
						refListMap.add(mapResource.getName());
						refListDirMap.add(mapResource.getPath());
						bFind = true;
					}
				}
			}
			
			//Requirementes of the baseFramework
			EList<BaseRequirement> lstBaseRequirement = baseFramework.getOwnedRequirement();
			bFind = searchBaseRequirementsEquivMap(bFind, lstBaseRequirement);
			
			//Requirementes of the BaseActivities
			EList<BaseActivity> lstBaseActivity = baseFramework.getOwnedActivities();
			bFind = searchActvityRequirements(bFind, lstBaseActivity);		

		return bFind;
	}

	private boolean searchActvityRequirements(boolean bFind,
			EList<BaseActivity> lstBaseActivity) {
		EList<BaseRequirement> lstBaseRequirement;
		Iterator<BaseActivity> itbaseActivity = lstBaseActivity.iterator();

		while (itbaseActivity.hasNext()) {
			BaseActivity baseActivity = itbaseActivity.next();

			lstBaseRequirement = baseActivity.getOwnedRequirement();
			bFind = searchBaseRequirementsEquivMap(bFind, lstBaseRequirement);
			
			if(baseActivity.getSubActivity().size()>0){
				EList<BaseActivity> lstSubActivities = baseActivity.getSubActivity();
				bFind = searchActvityRequirements(bFind, lstSubActivities);
			}
		
		}
		return bFind;
	}

	private boolean searchBaseRequirementsEquivMap(boolean bFind,
			EList<BaseRequirement> lstBaseRequirement) {
		Iterator<BaseRequirement> itbaseRequirement = lstBaseRequirement.iterator();

		while (itbaseRequirement.hasNext()) {
			BaseRequirement baseRequirement = itbaseRequirement.next();

			EList<BaseEquivalenceMap> lstBaseEquivalenceMap = baseRequirement.getEquivalenceMap();
			Iterator<BaseEquivalenceMap> itbaseEquivalenceMap = lstBaseEquivalenceMap.iterator();
			while (itbaseEquivalenceMap.hasNext()) {
				BaseEquivalenceMap baseEquivalenceMap = itbaseEquivalenceMap.next();
				MapModel mapModel = (MapModel) baseEquivalenceMap.getMapGroup().eContainer();
				mapResource = CDOUtil.getCDOObject(mapModel).cdoResource();

				// Antes de a�adir comprobar que no exista ya en la lista
				if (!VerifyMapResource(mapResource.getPath())) {
					refListMap.add(mapResource.getName());
					refListDirMap.add(mapResource.getPath());
					bFind = true;
				}
			}
		}
		return bFind;
	}
	
	private boolean VerifyMapResource (String nameMapModel) {
		boolean bFind = false;
		int i = 0;
		
		while (i< refListDirMap.size() && !bFind) {
			if (refListDirMap.get(0).contentEquals(nameMapModel)){
				bFind = true;
			}
			i++;
		}
		return bFind;
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Cross Standard");
	}
	
	@Override
	protected void okPressed() {
		
		
		//usar el targetResourceSet y comparar con el arbol  modelViewerTarget
		//modelViewerTarget.getCheckedElements();
		
		
		/*org.eclipse.swt.graphics.Cursor cursor;*/
		//this.getContents().setCursor(new Cursor(new Display(), SWT.CURSOR_WAIT));
		
		//Resource firstevModel =targetResourceSet.getResources().get(0);
		
			
		
		
		
		
		ArtefactModel amF =null;
		
		
		
		if(projectsourceText.getText().length()==0){
			MessageDialog.openError(getShell(), "Selected Source Project",
				"You must select the source project of the reuse.");
			return;
		}
		
		if(checktreeEvidenceSource.getTree().getItemCount()==0){
			MessageDialog.openError(getShell(), "Wrong Source Project",
					"The selected source project doesn't have evidence model.");
			return;
		}
		
		if(treeEvidenceTarget.getSelection().isEmpty()){
			MessageDialog.openError(getShell(), "Selected Target Artefacts",
					"You must select the Target Artefact of the reuse.");
			return;			
		}
		else{
			for(TreeItem oneSelected: treeEvidenceTarget.getTree().getSelection()){
				if (!oneSelected.getBackground().equals(Display.getCurrent().getSystemColor(SWT.COLOR_GREEN))){
					int ax = JOptionPane.showConfirmDialog(null, "The selected Target Artefact has not equivalence with the checked Source Artefact/s. Do you want to continue the reuse anyway?");
					if(ax == JOptionPane.OK_OPTION){
						break;				
					}
					else{
						return;
					}
				}
			}
					
			treeEvidenceTarget.getTree().getSelection();
		}
		
		Object[] sourceArtefacts =  checktreeEvidenceSource.getCheckedElements();
		
		if(sourceArtefacts.length==0){
			MessageDialog.openError(getShell(), "Selected Source Artefacts",
					"You must select the Source Project Artefacts for the reuse.");
				return;
		}
				
		for(Object oneArtefact:sourceArtefacts){
			int found = isEquivalenceArtefact(oneArtefact,checktreeEvidenceSource.getTree().getItem(0));
			if(found == 2){				
				int ax = JOptionPane.showConfirmDialog(null, "You have selected source Artefacts(not highlighted in green) without equivalence with the selected target Artefact. Do you want to reuse them anyway?");
				if(ax == JOptionPane.OK_OPTION){
					break;				
				}
				else{
					return;
				}
			}
		}
		
		String resume ="You are going to copy the Source Artefacts:\r\n";
		for(Object oneArtefact:sourceArtefacts){
			if(oneArtefact instanceof Artefact){
				Artefact sa= (Artefact)oneArtefact;
				resume+="  " + sa.getId() + " - " + sa.getName() + " in model " + sa.cdoResource().getName() + "\r\n";
			}			
		}
		resume+= "and their related Resources an files to the target Artefact:\r\n";
		IStructuredSelection selT = (IStructuredSelection) treeEvidenceTarget.getSelection();
		EObject objectTarget = (EObject) selT.getFirstElement();	
		Artefact artTo;
		if (objectTarget instanceof Artefact){
			artTo = (Artefact) objectTarget;
			resume+="  " + artTo.getId() + "-" + artTo.getName() +  " in model " + artTo.cdoResource().getName() + "\r\n";
		}
		
		int ax = JOptionPane.showConfirmDialog(null, resume + "Do you want to continue?");
		if(ax == JOptionPane.OK_OPTION){			
			MessageDialog.openInformation(getShell(), "Long Process",
					"The reuse process could take several minutes. Please, be patient.");
		}
		else{
			return;
		}
		for (TableItem postCondItem:listPostConditions.getTable().getItems()){
			Object onePCObject= postCondItem.getData();
			if(onePCObject instanceof BaselineElement){
				BaselineElement postcond= (BaselineElement)onePCObject;
				postcond.setIsSelected(true);
				postcond.setSelectionJustification("Automatically selected for being postCondition of a Cross Standard Reusing");
			}
		}
		
		for(Object oneArtefact:sourceArtefacts){			
			
			if (oneArtefact instanceof Artefact){
				Artefact artFrom = (Artefact) oneArtefact;
				ArtefactDefinition adF =(ArtefactDefinition)artFrom.eContainer();
				amF = (ArtefactModel) adF.eContainer();
				
				ArtefactModel amT =null;							
				if (objectTarget instanceof Artefact){
					artTo = (Artefact) objectTarget;
					ArtefactDefinition adT =(ArtefactDefinition)artTo.eContainer();
					amT = (ArtefactModel) adT.eContainer();
					
					//Copy the ArtefactModelData to destination model If it's empty
					
					if(amT.getRepoUrl()==null || amT.getRepoUrl().equals("")){
						if(amF.getId()!=null){
							amT.setId(amF.getId());
						}
						if(amF.getName()!=null){
							amT.setName(amF.getName());							
						}
						else if (amT.getName()==null){
							amT.setName ("NoNameSpecified");
						}
						
						
						amT.setRepoLocalPath(amF.getRepoLocalPath());
						amT.setRepoPassword(amF.getRepoPassword());
						amT.setRepoUrl(amF.getRepoUrl());
						amT.setRepoUser(amF.getRepoUser());
						amT.setRepoUsesLocal(amF.isRepoUsesLocal());
					}
				}
				else{
					return;
				}
				
				if (amT.isRepoUsesLocal()) {
					String localRepo = amF.getRepoLocalPath();
					File projectFolder=new File(localRepo+"/" + assuranceProjectTarget.getName());
					if(!projectFolder.exists()){	           			
						projectFolder.mkdir();
			        }
					
					File artefactModelFolderPath=new File(projectFolder.getAbsolutePath() +"/" + amT.getName());
					if(!artefactModelFolderPath.exists()){	           		
						artefactModelFolderPath.mkdir();
			       }
					
					if(amT.getArtefact().get(0).getName()==null){
						amT.getArtefact().get(0).setName ("NoNameSpecified");
					}
					
					File artefactDefinitionFolderPath=new File(artefactModelFolderPath.getAbsolutePath() +"/" + amT.getArtefact().get(0).getName());
					if(!artefactDefinitionFolderPath.exists()){	           		
						artefactDefinitionFolderPath.mkdir();
			       }
						
					File artefactFolderPath=new File(artefactDefinitionFolderPath.getAbsolutePath() +"/" + artTo.getName());
					if(!artefactFolderPath.exists()){			     
						artefactFolderPath.mkdir();
			        }							
						
					for (org.eclipse.opencert.evm.evidspec.evidence.Resource fromFile : artFrom.getResource()){
						//Copy the resource to target artefact
						org.eclipse.opencert.evm.evidspec.evidence.Resource copy = evidenceFactory.createResource();
						copy.setId(fromFile.getId());
						if (fromFile.getName()==null){
							fromFile.setName ("NoNameSpecified");
						}
						else{
							copy.setName(fromFile.getName());
						}
						
						copy.setDescription(fromFile.getDescription());
						copy.setFormat(fromFile.getFormat());
						
						File fromResourceFile = new File(fromFile.getLocation());
						
						//Obtain only the name and extension
						
						
						File destinationResourceFile = new File(artefactFolderPath + "/" + fromResourceFile.getName());
						
						if(destinationResourceFile.exists()){
							ax = JOptionPane.showConfirmDialog(null, "Resource file  " + fromFile.getLocation() +" exists in the destination project. Do you want to update it?");
							if(ax == JOptionPane.YES_OPTION){
								copyFileUsingStream(fromResourceFile, destinationResourceFile);
								copy.setLocation(destinationResourceFile.getAbsolutePath());							
							}  
							else{
								copy.setLocation(destinationResourceFile.getAbsolutePath());
							}
						}
						else{
							copyFileUsingStream(fromResourceFile, destinationResourceFile);
							copy.setLocation(destinationResourceFile.getAbsolutePath());	
				        }													
						artTo.getResource().add(copy);								
					}																				 
				}
				else{
					MainClass repoLibrary = new MainClass();
					repoLibrary.connectRepository(amT.getRepoUrl(), amT.getRepoUser(), amT.getRepoPassword());
					
					String projectFolderPath ="";
					if(!repoLibrary.existsFolderRepo(assuranceProjectTarget.getName())){									
						projectFolderPath = repoLibrary.addFolderToRepo("",assuranceProjectTarget.getName(), "");
					}
					else{
						projectFolderPath =assuranceProjectTarget.getName();
					}
					
					String artefactModelFolderPath = "";
					if(!repoLibrary.existsFolderRepo(projectFolderPath+"/" + amT.getName())){
						artefactModelFolderPath = repoLibrary.addFolderToRepo(projectFolderPath,amT.getName(), "");						
					}
					else{
						artefactModelFolderPath =projectFolderPath+"/"+amT.getName();
					}
							
					
					String artefactDefinitionFolderPath = "";
					if(amT.getArtefact().get(0).getName()==null){
						amT.getArtefact().get(0).setName ("NoNameSpecified");
					}
					
					if(!repoLibrary.existsFolderRepo(artefactModelFolderPath + "/" + amT.getArtefact().get(0).getName())){
						artefactDefinitionFolderPath = repoLibrary.addFolderToRepo(artefactModelFolderPath,amT.getArtefact().get(0).getName(), "");						
					}
					else{
						artefactDefinitionFolderPath =artefactModelFolderPath+"/"+amT.getArtefact().get(0).getName();
					}
					
						
					String artefactFolderPath="";
					if(!repoLibrary.existsFolderRepo(artefactDefinitionFolderPath+"/" + artTo.getName())){
						artefactFolderPath = repoLibrary.addFolderToRepo(artefactDefinitionFolderPath,artTo.getName(), "");
						
					}
					else{
						artefactFolderPath =artefactDefinitionFolderPath+"/" + artTo.getName();
					}								
					
					for (org.eclipse.opencert.evm.evidspec.evidence.Resource fromFile : artFrom.getResource()){
						//Copy the resource to target artefact
						org.eclipse.opencert.evm.evidspec.evidence.Resource copy = evidenceFactory.createResource();
						copy.setId(fromFile.getId());
												
						if (fromFile.getName()==null){
							fromFile.setName ("NoNameSpecified");
						}
						else{
							copy.setName(fromFile.getName());
						}
						
						copy.setDescription(fromFile.getDescription());
						copy.setFormat(fromFile.getFormat());
						
						String resourcePath= repoLibrary.copyFileFromRepo(fromFile.getLocation(), artefactFolderPath,"");
						if(!resourcePath.equals("exists")){
							copy.setLocation(resourcePath);
						}
						else{
							ax = JOptionPane.showConfirmDialog(null, "Resource file  " + fromFile.getLocation() +" exists in the destination project. Do you want to update it?");
							if(ax == JOptionPane.YES_OPTION){
								String result=repoLibrary.updateFileFromRepo(fromFile.getLocation(), artefactFolderPath,"");
								if(!result.contains("error")){
									copy.setLocation(result);									
								}								
							}
							else{
								copy.setLocation(fromFile.getLocation());
							}
						}
						artTo.getResource().add(copy);								
					}
																					 
				}
			}
		}
													
		try {
			for(Resource evRes :evidenceTargetResourceSet.getResources()){
				CDOResource savRes= (CDOResource)evRes;
				savRes.save(options);				
			}
			for(Resource blRes :baselineTargetResourceSet.getResources()){
				CDOResource savBLRes= (CDOResource)blRes;
				savBLRes.save(options);				
			}
			
			MessageDialog.openInformation(getShell(), "Reuse success", "Selected artefacts reused");
			return;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//modelViewerTarget.getCheckedElements()
		//this.getContents().setCursor(new Cursor(new Display(), SWT.ARROW));
		//super.okPressed();
	}

	@Override
	public boolean close() {		
		//Refresh the Repository explorer view
		DawnExplorer repoView =null;
		IViewReference viewReferences[] = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getViewReferences();
		for (int i = 0; i < viewReferences.length; i++) {
			if ("org.eclipse.emf.cdo.dawn.ui.views.DawnExplorer".equals(viewReferences[i].getId())) {
				repoView = (DawnExplorer)viewReferences[i].getView(false);
				repoView.refreshViewer(false);
				break;
			}
		}
		try {
			if (transaction!= null && !transaction.isClosed()) transaction.close();
		} catch (NullPointerException e) {
			// Ignore NullPointerException while deactivating CDOTransactionImpl
			//e.printStackTrace();
		}
		return super.close();
	}

	private void clearAllControls() {
		txtJustification.setText("");
		idText.setText("");
		nameText.setText("");
		cbType.select(0);
		
		listPostConditions.setInput(null);
		
		PCidText.setText("");
		PCnameText.setText("");
		PCtxtdescription.setText("");
		
		SCMidText.setText("");
		SCMnameText.setText("");
		SCMcbType.select(0);
		SCMtxtJustification.setText("");
		
		TCMidText.setText("");
		TCMnameText.setText("");
		TCMcbType.select(0);
		TCMtxtJustification.setText("");
		
		for(TreeItem greenNode:listGreenItem){							
			greenNode.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
			greenNode.setExpanded(false);
			greenNode.setChecked(false);
			greenNode.setExpanded(true);
		}
		listGreenItem.clear();
	}

	/*public Boolean getResult() {
		return true;
	}*/			
	private static void copyFileUsingStream(File source, File dest) {
	    InputStream is = null;
	    OutputStream os = null;
	    try {
	        is = new FileInputStream(source);
	        os = new FileOutputStream(dest);
	        byte[] buffer = new byte[1024];
	        int length;
	        while ((length = is.read(buffer)) > 0) {
	            os.write(buffer, 0, length);
	        }
	        is.close();
	        os.close();
	    }catch(Exception e){
	    	
	    }	    	    
	}
			
	
}

