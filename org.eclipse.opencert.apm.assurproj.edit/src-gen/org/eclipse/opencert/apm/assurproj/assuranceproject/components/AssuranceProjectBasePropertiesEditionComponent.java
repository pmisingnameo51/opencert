/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.components;

// Start of user code for imports
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.eef.runtime.api.notify.EStructuralFeatureNotificationFilter;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.notify.NotificationFilter;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.impl.components.SinglePartPropertiesEditingComponent;
import org.eclipse.emf.eef.runtime.impl.filters.EObjectFilter;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.impl.utils.EEFConverterUtil;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceprojectPackage;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceprojectViewsRepository;
import org.eclipse.opencert.infra.general.general.GeneralPackage;


// End of user code

/**
 * 
 * 
 */
public class AssuranceProjectBasePropertiesEditionComponent extends SinglePartPropertiesEditingComponent {

	
	public static String BASE_PART = "Base"; //$NON-NLS-1$

	
	/**
	 * Settings for subProject ReferencesTable
	 */
	private ReferencesTableSettings subProjectSettings;
	
	
	/**
	 * Default constructor
	 * 
	 */
	public AssuranceProjectBasePropertiesEditionComponent(PropertiesEditingContext editingContext, EObject assuranceProject, String editing_mode) {
		super(editingContext, assuranceProject, editing_mode);
		parts = new String[] { BASE_PART };
		repositoryKey = AssuranceprojectViewsRepository.class;
		partKey = AssuranceprojectViewsRepository.AssuranceProject_.class;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject, 
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(Object key, int kind, EObject elt, ResourceSet allResource) {
		setInitializing(true);
		if (editingPart != null && key == partKey) {
			editingPart.setContext(elt, allResource);
			
			final AssuranceProject assuranceProject = (AssuranceProject)elt;
			final AssuranceProjectPropertiesEditionPart basePart = (AssuranceProjectPropertiesEditionPart)editingPart;
			// init values
			if (isAccessible(AssuranceprojectViewsRepository.AssuranceProject_.Properties.id))
				basePart.setId(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, assuranceProject.getId()));
			
			if (isAccessible(AssuranceprojectViewsRepository.AssuranceProject_.Properties.name))
				basePart.setName(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, assuranceProject.getName()));
			
			if (isAccessible(AssuranceprojectViewsRepository.AssuranceProject_.Properties.description))
				basePart.setDescription(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, assuranceProject.getDescription()));
			
			if (isAccessible(AssuranceprojectViewsRepository.AssuranceProject_.Properties.createdBy))
				basePart.setCreatedBy(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, assuranceProject.getCreatedBy()));
			
			if (isAccessible(AssuranceprojectViewsRepository.AssuranceProject_.Properties.responsible))
				basePart.setResponsible(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, assuranceProject.getResponsible()));
			
			if (isAccessible(AssuranceprojectViewsRepository.AssuranceProject_.Properties.date))
				basePart.setDate(EEFConverterUtil.convertToString(EcorePackage.Literals.EDATE, assuranceProject.getDate()));
			
			if (isAccessible(AssuranceprojectViewsRepository.AssuranceProject_.Properties.version))
				basePart.setVersion(EEFConverterUtil.convertToString(EcorePackage.Literals.ESTRING, assuranceProject.getVersion()));
			
			if (isAccessible(AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject)) {
				subProjectSettings = new ReferencesTableSettings(assuranceProject, AssuranceprojectPackage.eINSTANCE.getAssuranceProject_SubProject());
				basePart.initSubProject(subProjectSettings);
			}
			// init filters
			
			
			
			
			
			
			
			if (isAccessible(AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject)) {
				basePart.addFilterToSubProject(new EObjectFilter(AssuranceprojectPackage.Literals.ASSURANCE_PROJECT));
				// Start of user code for additional businessfilters for subProject
				// End of user code
			}
			// init values for referenced views
			
			// init filters for referenced views
			
		}
		setInitializing(false);
	}











	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#associatedFeature(java.lang.Object)
	 */
	public EStructuralFeature associatedFeature(Object editorKey) {
		if (editorKey == AssuranceprojectViewsRepository.AssuranceProject_.Properties.id) {
			return GeneralPackage.eINSTANCE.getNamedElement_Id();
		}
		if (editorKey == AssuranceprojectViewsRepository.AssuranceProject_.Properties.name) {
			return GeneralPackage.eINSTANCE.getNamedElement_Name();
		}
		if (editorKey == AssuranceprojectViewsRepository.AssuranceProject_.Properties.description) {
			return GeneralPackage.eINSTANCE.getDescribableElement_Description();
		}
		if (editorKey == AssuranceprojectViewsRepository.AssuranceProject_.Properties.createdBy) {
			return AssuranceprojectPackage.eINSTANCE.getAssuranceProject_CreatedBy();
		}
		if (editorKey == AssuranceprojectViewsRepository.AssuranceProject_.Properties.responsible) {
			return AssuranceprojectPackage.eINSTANCE.getAssuranceProject_Responsible();
		}
		if (editorKey == AssuranceprojectViewsRepository.AssuranceProject_.Properties.date) {
			return AssuranceprojectPackage.eINSTANCE.getAssuranceProject_Date();
		}
		if (editorKey == AssuranceprojectViewsRepository.AssuranceProject_.Properties.version) {
			return AssuranceprojectPackage.eINSTANCE.getAssuranceProject_Version();
		}
		if (editorKey == AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject) {
			return AssuranceprojectPackage.eINSTANCE.getAssuranceProject_SubProject();
		}
		return super.associatedFeature(editorKey);
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updateSemanticModel(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void updateSemanticModel(final IPropertiesEditionEvent event) {
		AssuranceProject assuranceProject = (AssuranceProject)semanticObject;
		if (AssuranceprojectViewsRepository.AssuranceProject_.Properties.id == event.getAffectedEditor()) {
			assuranceProject.setId((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (AssuranceprojectViewsRepository.AssuranceProject_.Properties.name == event.getAffectedEditor()) {
			assuranceProject.setName((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (AssuranceprojectViewsRepository.AssuranceProject_.Properties.description == event.getAffectedEditor()) {
			assuranceProject.setDescription((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (AssuranceprojectViewsRepository.AssuranceProject_.Properties.createdBy == event.getAffectedEditor()) {
			assuranceProject.setCreatedBy((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (AssuranceprojectViewsRepository.AssuranceProject_.Properties.responsible == event.getAffectedEditor()) {
			assuranceProject.setResponsible((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (AssuranceprojectViewsRepository.AssuranceProject_.Properties.date == event.getAffectedEditor()) {
			assuranceProject.setDate((java.util.Date)EEFConverterUtil.createFromString(EcorePackage.Literals.EDATE, (String)event.getNewValue()));
		}
		if (AssuranceprojectViewsRepository.AssuranceProject_.Properties.version == event.getAffectedEditor()) {
			assuranceProject.setVersion((java.lang.String)EEFConverterUtil.createFromString(EcorePackage.Literals.ESTRING, (String)event.getNewValue()));
		}
		if (AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject == event.getAffectedEditor()) {
			if (event.getKind() == PropertiesEditionEvent.ADD) {
				if (event.getNewValue() instanceof AssuranceProject) {
					subProjectSettings.addToReference((EObject) event.getNewValue());
				}
			} else if (event.getKind() == PropertiesEditionEvent.REMOVE) {
				subProjectSettings.removeFromReference((EObject) event.getNewValue());
			} else if (event.getKind() == PropertiesEditionEvent.MOVE) {
				subProjectSettings.move(event.getNewIndex(), (AssuranceProject) event.getNewValue());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#updatePart(org.eclipse.emf.common.notify.Notification)
	 */
	public void updatePart(Notification msg) {
		super.updatePart(msg);
		if (editingPart.isVisible()) {
			AssuranceProjectPropertiesEditionPart basePart = (AssuranceProjectPropertiesEditionPart)editingPart;
			if (GeneralPackage.eINSTANCE.getNamedElement_Id().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceprojectViewsRepository.AssuranceProject_.Properties.id)) {
				if (msg.getNewValue() != null) {
					basePart.setId(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setId("");
				}
			}
			if (GeneralPackage.eINSTANCE.getNamedElement_Name().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceprojectViewsRepository.AssuranceProject_.Properties.name)) {
				if (msg.getNewValue() != null) {
					basePart.setName(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setName("");
				}
			}
			if (GeneralPackage.eINSTANCE.getDescribableElement_Description().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceprojectViewsRepository.AssuranceProject_.Properties.description)) {
				if (msg.getNewValue() != null) {
					basePart.setDescription(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setDescription("");
				}
			}
			if (AssuranceprojectPackage.eINSTANCE.getAssuranceProject_CreatedBy().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceprojectViewsRepository.AssuranceProject_.Properties.createdBy)) {
				if (msg.getNewValue() != null) {
					basePart.setCreatedBy(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setCreatedBy("");
				}
			}
			if (AssuranceprojectPackage.eINSTANCE.getAssuranceProject_Responsible().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceprojectViewsRepository.AssuranceProject_.Properties.responsible)) {
				if (msg.getNewValue() != null) {
					basePart.setResponsible(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setResponsible("");
				}
			}
			if (AssuranceprojectPackage.eINSTANCE.getAssuranceProject_Date().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceprojectViewsRepository.AssuranceProject_.Properties.date)) {
				if (msg.getNewValue() != null) {
					basePart.setDate(EcoreUtil.convertToString(EcorePackage.Literals.EDATE, msg.getNewValue()));
				} else {
					basePart.setDate("");
				}
			}
			if (AssuranceprojectPackage.eINSTANCE.getAssuranceProject_Version().equals(msg.getFeature()) && msg.getNotifier().equals(semanticObject) && basePart != null && isAccessible(AssuranceprojectViewsRepository.AssuranceProject_.Properties.version)) {
				if (msg.getNewValue() != null) {
					basePart.setVersion(EcoreUtil.convertToString(EcorePackage.Literals.ESTRING, msg.getNewValue()));
				} else {
					basePart.setVersion("");
				}
			}
			if (AssuranceprojectPackage.eINSTANCE.getAssuranceProject_SubProject().equals(msg.getFeature())  && isAccessible(AssuranceprojectViewsRepository.AssuranceProject_.Properties.subProject))
				basePart.updateSubProject();
			
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.StandardPropertiesEditionComponent#getNotificationFilters()
	 */
	@Override
	protected NotificationFilter[] getNotificationFilters() {
		NotificationFilter filter = new EStructuralFeatureNotificationFilter(
			GeneralPackage.eINSTANCE.getNamedElement_Id(),
			GeneralPackage.eINSTANCE.getNamedElement_Name(),
			GeneralPackage.eINSTANCE.getDescribableElement_Description(),
			AssuranceprojectPackage.eINSTANCE.getAssuranceProject_CreatedBy(),
			AssuranceprojectPackage.eINSTANCE.getAssuranceProject_Responsible(),
			AssuranceprojectPackage.eINSTANCE.getAssuranceProject_Date(),
			AssuranceprojectPackage.eINSTANCE.getAssuranceProject_Version(),
			AssuranceprojectPackage.eINSTANCE.getAssuranceProject_SubProject()		);
		return new NotificationFilter[] {filter,};
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent#validateValue(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public Diagnostic validateValue(IPropertiesEditionEvent event) {
		Diagnostic ret = Diagnostic.OK_INSTANCE;
		if (event.getNewValue() != null) {
			try {
				if (AssuranceprojectViewsRepository.AssuranceProject_.Properties.id == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Id().getEAttributeType(), newValue);
				}
				if (AssuranceprojectViewsRepository.AssuranceProject_.Properties.name == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getNamedElement_Name().getEAttributeType(), newValue);
				}
				if (AssuranceprojectViewsRepository.AssuranceProject_.Properties.description == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(GeneralPackage.eINSTANCE.getDescribableElement_Description().getEAttributeType(), newValue);
				}
				if (AssuranceprojectViewsRepository.AssuranceProject_.Properties.createdBy == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(AssuranceprojectPackage.eINSTANCE.getAssuranceProject_CreatedBy().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(AssuranceprojectPackage.eINSTANCE.getAssuranceProject_CreatedBy().getEAttributeType(), newValue);
				}
				if (AssuranceprojectViewsRepository.AssuranceProject_.Properties.responsible == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(AssuranceprojectPackage.eINSTANCE.getAssuranceProject_Responsible().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(AssuranceprojectPackage.eINSTANCE.getAssuranceProject_Responsible().getEAttributeType(), newValue);
				}
				if (AssuranceprojectViewsRepository.AssuranceProject_.Properties.date == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(AssuranceprojectPackage.eINSTANCE.getAssuranceProject_Date().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(AssuranceprojectPackage.eINSTANCE.getAssuranceProject_Date().getEAttributeType(), newValue);
				}
				if (AssuranceprojectViewsRepository.AssuranceProject_.Properties.version == event.getAffectedEditor()) {
					Object newValue = event.getNewValue();
					if (newValue instanceof String) {
						newValue = EEFConverterUtil.createFromString(AssuranceprojectPackage.eINSTANCE.getAssuranceProject_Version().getEAttributeType(), (String)newValue);
					}
					ret = Diagnostician.INSTANCE.validate(AssuranceprojectPackage.eINSTANCE.getAssuranceProject_Version().getEAttributeType(), newValue);
				}
			} catch (IllegalArgumentException iae) {
				ret = BasicDiagnostic.toDiagnostic(iae);
			} catch (WrappedException we) {
				ret = BasicDiagnostic.toDiagnostic(we);
			}
		}
		return ret;
	}


	

}
