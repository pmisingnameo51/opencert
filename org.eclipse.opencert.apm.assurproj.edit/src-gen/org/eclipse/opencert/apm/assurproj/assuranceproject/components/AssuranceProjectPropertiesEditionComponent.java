/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assurproj.assuranceproject.components;

// Start of user code for imports

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.context.PropertiesEditingContext;
import org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.providers.PropertiesEditingProvider;
import org.eclipse.opencert.apm.assurproj.assuranceproject.AssuranceProject;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceAssetsPropertiesEditionPart;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceProjectPropertiesEditionPart;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.AssuranceprojectViewsRepository;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.PermissionConfigurationsPropertiesEditionPart;
import org.eclipse.opencert.apm.assurproj.assuranceproject.parts.ProjectBaselinesPropertiesEditionPart;

// End of user code

/**
 * 
 * 
 */
public class AssuranceProjectPropertiesEditionComponent extends ComposedPropertiesEditionComponent {

	/**
	 * The Base part
	 * 
	 */
	private AssuranceProjectPropertiesEditionPart basePart;

	/**
	 * The AssuranceProjectBasePropertiesEditionComponent sub component
	 * 
	 */
	protected AssuranceProjectBasePropertiesEditionComponent assuranceProjectBasePropertiesEditionComponent;

	/**
	 * The ProjectBaselines part
	 * 
	 */
	private ProjectBaselinesPropertiesEditionPart projectBaselinesPart;

	/**
	 * The AssuranceProjectProjectBaselinesPropertiesEditionComponent sub component
	 * 
	 */
	protected AssuranceProjectProjectBaselinesPropertiesEditionComponent assuranceProjectProjectBaselinesPropertiesEditionComponent;

	/**
	 * The PermissionConfigurations part
	 * 
	 */
	private PermissionConfigurationsPropertiesEditionPart permissionConfigurationsPart;

	/**
	 * The AssuranceProjectPermissionConfigurationsPropertiesEditionComponent sub component
	 * 
	 */
	protected AssuranceProjectPermissionConfigurationsPropertiesEditionComponent assuranceProjectPermissionConfigurationsPropertiesEditionComponent;

	/**
	 * The AssuranceAssets part
	 * 
	 */
	private AssuranceAssetsPropertiesEditionPart assuranceAssetsPart;

	/**
	 * The AssuranceProjectAssuranceAssetsPropertiesEditionComponent sub component
	 * 
	 */
	protected AssuranceProjectAssuranceAssetsPropertiesEditionComponent assuranceProjectAssuranceAssetsPropertiesEditionComponent;

	/**
	 * Parameterized constructor
	 * 
	 * @param assuranceProject the EObject to edit
	 * 
	 */
	public AssuranceProjectPropertiesEditionComponent(PropertiesEditingContext editingContext, EObject assuranceProject, String editing_mode) {
		super(editingContext, editing_mode);
		if (assuranceProject instanceof AssuranceProject) {
			PropertiesEditingProvider provider = null;
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(assuranceProject, PropertiesEditingProvider.class);
			assuranceProjectBasePropertiesEditionComponent = (AssuranceProjectBasePropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, AssuranceProjectBasePropertiesEditionComponent.BASE_PART, AssuranceProjectBasePropertiesEditionComponent.class);
			addSubComponent(assuranceProjectBasePropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(assuranceProject, PropertiesEditingProvider.class);
			assuranceProjectProjectBaselinesPropertiesEditionComponent = (AssuranceProjectProjectBaselinesPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, AssuranceProjectProjectBaselinesPropertiesEditionComponent.PROJECTBASELINES_PART, AssuranceProjectProjectBaselinesPropertiesEditionComponent.class);
			addSubComponent(assuranceProjectProjectBaselinesPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(assuranceProject, PropertiesEditingProvider.class);
			assuranceProjectPermissionConfigurationsPropertiesEditionComponent = (AssuranceProjectPermissionConfigurationsPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, AssuranceProjectPermissionConfigurationsPropertiesEditionComponent.PERMISSIONCONFIGURATIONS_PART, AssuranceProjectPermissionConfigurationsPropertiesEditionComponent.class);
			addSubComponent(assuranceProjectPermissionConfigurationsPropertiesEditionComponent);
			provider = (PropertiesEditingProvider)editingContext.getAdapterFactory().adapt(assuranceProject, PropertiesEditingProvider.class);
			assuranceProjectAssuranceAssetsPropertiesEditionComponent = (AssuranceProjectAssuranceAssetsPropertiesEditionComponent)provider.getPropertiesEditingComponent(editingContext, editing_mode, AssuranceProjectAssuranceAssetsPropertiesEditionComponent.ASSURANCEASSETS_PART, AssuranceProjectAssuranceAssetsPropertiesEditionComponent.class);
			addSubComponent(assuranceProjectAssuranceAssetsPropertiesEditionComponent);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      getPropertiesEditionPart(int, java.lang.String)
	 * 
	 */
	public IPropertiesEditionPart getPropertiesEditionPart(int kind, String key) {
		if (AssuranceProjectBasePropertiesEditionComponent.BASE_PART.equals(key)) {
			basePart = (AssuranceProjectPropertiesEditionPart)assuranceProjectBasePropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)basePart;
		}
		if (AssuranceProjectProjectBaselinesPropertiesEditionComponent.PROJECTBASELINES_PART.equals(key)) {
			projectBaselinesPart = (ProjectBaselinesPropertiesEditionPart)assuranceProjectProjectBaselinesPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)projectBaselinesPart;
		}
		if (AssuranceProjectPermissionConfigurationsPropertiesEditionComponent.PERMISSIONCONFIGURATIONS_PART.equals(key)) {
			permissionConfigurationsPart = (PermissionConfigurationsPropertiesEditionPart)assuranceProjectPermissionConfigurationsPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)permissionConfigurationsPart;
		}
		if (AssuranceProjectAssuranceAssetsPropertiesEditionComponent.ASSURANCEASSETS_PART.equals(key)) {
			assuranceAssetsPart = (AssuranceAssetsPropertiesEditionPart)assuranceProjectAssuranceAssetsPropertiesEditionComponent.getPropertiesEditionPart(kind, key);
			return (IPropertiesEditionPart)assuranceAssetsPart;
		}
		return super.getPropertiesEditionPart(kind, key);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      setPropertiesEditionPart(java.lang.Object, int,
	 *      org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart)
	 * 
	 */
	public void setPropertiesEditionPart(java.lang.Object key, int kind, IPropertiesEditionPart propertiesEditionPart) {
		if (AssuranceprojectViewsRepository.AssuranceProject_.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			basePart = (AssuranceProjectPropertiesEditionPart)propertiesEditionPart;
		}
		if (AssuranceprojectViewsRepository.ProjectBaselines.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			projectBaselinesPart = (ProjectBaselinesPropertiesEditionPart)propertiesEditionPart;
		}
		if (AssuranceprojectViewsRepository.PermissionConfigurations.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			permissionConfigurationsPart = (PermissionConfigurationsPropertiesEditionPart)propertiesEditionPart;
		}
		if (AssuranceprojectViewsRepository.AssuranceAssets.class == key) {
			super.setPropertiesEditionPart(key, kind, propertiesEditionPart);
			assuranceAssetsPart = (AssuranceAssetsPropertiesEditionPart)propertiesEditionPart;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.impl.components.ComposedPropertiesEditionComponent#
	 *      initPart(java.lang.Object, int, org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.resource.ResourceSet)
	 * 
	 */
	public void initPart(java.lang.Object key, int kind, EObject element, ResourceSet allResource) {
		if (key == AssuranceprojectViewsRepository.AssuranceProject_.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == AssuranceprojectViewsRepository.ProjectBaselines.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == AssuranceprojectViewsRepository.PermissionConfigurations.class) {
			super.initPart(key, kind, element, allResource);
		}
		if (key == AssuranceprojectViewsRepository.AssuranceAssets.class) {
			super.initPart(key, kind, element, allResource);
		}
	}
}
