/*******************************************************************************
 * Copyright (c) 2017 Intecs SpA 
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 *
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.chess.traceability.internal.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.capra.GenericTraceMetaModel.impl.GenericTraceModelImpl;
import org.eclipse.capra.core.CapraException;
import org.eclipse.capra.core.adapters.PersistenceAdapter;
import org.eclipse.capra.core.util.ExtensionPointUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.AnalysisContextArtefactLinkImpl;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.ComponentArgumentationElementLinkImpl;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.ContractAgreementLinkImpl;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.ContractArtefactLinkImpl;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.ContractClaimLinkImpl;
import org.eclipse.opencert.chess.tracemodel.OpenCertTraceLinkMetaModel.impl.FormalPropertyClaimLinkImpl;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.sam.arg.arg.Agreement;
import org.eclipse.opencert.sam.arg.arg.ArgumentationElement;
import org.eclipse.opencert.sam.arg.arg.Claim;
import org.eclipse.uml2.uml.Class;
import org.polarsys.chess.contracts.profile.chesscontract.Contract;
import org.polarsys.chess.contracts.profile.chesscontract.FormalProperty;

public class Utils {

	// TODO get methods should be refactored...

	public static List<Claim> getLinkedClaim(Contract contract) throws CapraException {
		List<Claim> list = new ArrayList<Claim>();

		PersistenceAdapter persistenceAdapter = ExtensionPointUtil.getTracePersistenceAdapter();
		Object traceModel = persistenceAdapter.getTraceModel(new ResourceSetImpl());
		if (traceModel instanceof GenericTraceModelImpl) {
			GenericTraceModelImpl model = (GenericTraceModelImpl) traceModel;
			ContractClaimLinkImpl traceLink = null;
			for (EObject trace : model.getTraces()) {
				if (trace instanceof ContractClaimLinkImpl) {
					traceLink = (ContractClaimLinkImpl) trace;
					if (EcoreUtil.getURI(traceLink.getSources()).equals(EcoreUtil.getURI(contract))) {
						for (EObject eobj : traceLink.getTargets()) {
							if (eobj instanceof Claim) {
								list.add((Claim) eobj);
							}
						}
					}
				}

			}
		}

		return list;
	}

	public static List<Artefact> getLinkedArtefact(Contract contract) throws CapraException {
		List<Artefact> list = new ArrayList<Artefact>();
		PersistenceAdapter persistenceAdapter = ExtensionPointUtil.getTracePersistenceAdapter();
		Object traceModel = persistenceAdapter.getTraceModel(new ResourceSetImpl());
		if (traceModel instanceof GenericTraceModelImpl) {
			GenericTraceModelImpl model = (GenericTraceModelImpl) traceModel;
			ContractArtefactLinkImpl traceLink = null;
			for (EObject trace : model.getTraces()) {
				if (trace instanceof ContractArtefactLinkImpl) {
					traceLink = (ContractArtefactLinkImpl) trace;
					if (EcoreUtil.getURI(traceLink.getSources()).equals(EcoreUtil.getURI(contract))) {
						for (EObject eobj : traceLink.getTargets()) {
							if (eobj instanceof Artefact) {
								list.add((Artefact) eobj);
							}
						}
					}
				}

			}
		}

		return list;
	}

	public static List<Agreement> getLinkedAgreement(Contract contract) throws CapraException {
		List<Agreement> list = new ArrayList<Agreement>();
		PersistenceAdapter persistenceAdapter = ExtensionPointUtil.getTracePersistenceAdapter();
		Object traceModel = persistenceAdapter.getTraceModel(new ResourceSetImpl());
		if (traceModel instanceof GenericTraceModelImpl) {
			GenericTraceModelImpl model = (GenericTraceModelImpl) traceModel;
			ContractAgreementLinkImpl traceLink = null;
			for (EObject trace : model.getTraces()) {
				if (trace instanceof ContractAgreementLinkImpl) {
					traceLink = (ContractAgreementLinkImpl) trace;
					if (EcoreUtil.getURI(traceLink.getSources()).equals(EcoreUtil.getURI(contract))) {
						for (EObject eobj : traceLink.getTargets()) {

							if (eobj instanceof Agreement) {
								list.add((Agreement) eobj);
							}
						}
					}
				}

			}
		}

		return list;
	}

	public static List<Claim> getLinkedClaim(FormalProperty property) throws CapraException {
		List<Claim> list = new ArrayList<Claim>();
		PersistenceAdapter persistenceAdapter = ExtensionPointUtil.getTracePersistenceAdapter();
		Object traceModel = persistenceAdapter.getTraceModel(new ResourceSetImpl());
		if (traceModel instanceof GenericTraceModelImpl) {
			GenericTraceModelImpl model = (GenericTraceModelImpl) traceModel;
			FormalPropertyClaimLinkImpl traceLink = null;
			for (EObject trace : model.getTraces()) {
				if (trace instanceof FormalPropertyClaimLinkImpl) {
					traceLink = (FormalPropertyClaimLinkImpl) trace;
					if (EcoreUtil.getURI(traceLink.getSources()).equals(EcoreUtil.getURI(property))) {
						for (EObject eobj : traceLink.getTargets()) {
							if (eobj instanceof Claim) {
								list.add((Claim) eobj);
							}
						}
					}
				}

			}
		}

		return list;
	}

	public static List<Artefact> getArtefactLinkedtoAnalysisContext(
			org.eclipse.uml2.uml.Class analysisContext_baseClass) throws CapraException {
		List<Artefact> list = new ArrayList<Artefact>();
		PersistenceAdapter persistenceAdapter = ExtensionPointUtil.getTracePersistenceAdapter();
		Object traceModel = persistenceAdapter.getTraceModel(new ResourceSetImpl());
		if (traceModel instanceof GenericTraceModelImpl) {
			GenericTraceModelImpl model = (GenericTraceModelImpl) traceModel;
			AnalysisContextArtefactLinkImpl traceLink = null;
			for (EObject trace : model.getTraces()) {
				if (trace instanceof AnalysisContextArtefactLinkImpl) {
					traceLink = (AnalysisContextArtefactLinkImpl) trace;
					if (EcoreUtil.getURI(traceLink.getSources()).equals(EcoreUtil.getURI(analysisContext_baseClass))) {
						for (EObject eobj : traceLink.getTargets()) {
							if (eobj instanceof Artefact) {
								list.add((Artefact) eobj);
							}
						}
					}
				}

			}
		}

		return list;
	}

	public static List<ArgumentationElement> getLinkedArgumentationElement(Class component) throws CapraException {
		List<ArgumentationElement> list = new ArrayList<ArgumentationElement>();
		PersistenceAdapter persistenceAdapter = ExtensionPointUtil.getTracePersistenceAdapter();
		Object traceModel = persistenceAdapter.getTraceModel(new ResourceSetImpl());
		if (traceModel instanceof GenericTraceModelImpl) {
			GenericTraceModelImpl model = (GenericTraceModelImpl) traceModel;
			ComponentArgumentationElementLinkImpl traceLink = null;
			for (EObject trace : model.getTraces()) {
				if (trace instanceof ComponentArgumentationElementLinkImpl) {
					traceLink = (ComponentArgumentationElementLinkImpl) trace;
					if (EcoreUtil.getURI(traceLink.getSources()).equals(EcoreUtil.getURI(component))) {
						for (EObject eobj : traceLink.getTargets()) {

							if (eobj instanceof ArgumentationElement) {
								list.add((ArgumentationElement) eobj);
							}
						}
					}
				}

			}
		}

		return list;
	}

	public static void removeLink(Contract contract, Claim claim) throws CapraException {
		ArrayList<ContractClaimLinkImpl> linksToRemove = new ArrayList<ContractClaimLinkImpl>();
		GenericTraceModelImpl model = null;
		PersistenceAdapter persistenceAdapter = ExtensionPointUtil.getTracePersistenceAdapter();
		Object traceModel = persistenceAdapter.getTraceModel(new ResourceSetImpl());
		if (traceModel instanceof GenericTraceModelImpl) {
			model = (GenericTraceModelImpl) traceModel;
			ContractClaimLinkImpl traceLink = null;
			for (EObject trace : model.getTraces()) {
				if (trace instanceof ContractClaimLinkImpl) {
					traceLink = (ContractClaimLinkImpl) trace;
					if (EcoreUtil.getURI(traceLink.getSources()).equals(EcoreUtil.getURI(contract))) {
						for (EObject eobj : traceLink.getTargets()) {
							if (EcoreUtil.getURI(eobj).equals(EcoreUtil.getURI(claim))) {
								linksToRemove.add(traceLink);
							}
							// TODO 1:n connection to be considered
							break;
						}
					}
				}
			}
		}

		for (ContractClaimLinkImpl link : linksToRemove) {
			model.getTraces().remove(link);
		}

		persistenceAdapter.saveTracesAndArtifacts(model, null);

		return;

	}

	public static void removeLink(Contract contract, Artefact artefact) throws CapraException {
		ArrayList<ContractArtefactLinkImpl> linksToRemove = new ArrayList<ContractArtefactLinkImpl>();
		GenericTraceModelImpl model = null;
		PersistenceAdapter persistenceAdapter = ExtensionPointUtil.getTracePersistenceAdapter();
		Object traceModel = persistenceAdapter.getTraceModel(new ResourceSetImpl());
		if (traceModel instanceof GenericTraceModelImpl) {
			model = (GenericTraceModelImpl) traceModel;
			ContractArtefactLinkImpl traceLink = null;
			for (EObject trace : model.getTraces()) {
				if (trace instanceof ContractArtefactLinkImpl) {
					traceLink = (ContractArtefactLinkImpl) trace;
					if (EcoreUtil.getURI(traceLink.getSources()).equals(EcoreUtil.getURI(contract))) {
						for (EObject eobj : traceLink.getTargets()) {
							if (EcoreUtil.getURI(eobj).equals(EcoreUtil.getURI(artefact))) {
								linksToRemove.add(traceLink);
							}
							// TODO 1:n connection to be considered
							break;
						}
					}
				}
			}
		}

		for (ContractArtefactLinkImpl link : linksToRemove) {
			model.getTraces().remove(link);
		}

		persistenceAdapter.saveTracesAndArtifacts(model, null);

		return;

	}

	public static void removeLink(Contract contract, Agreement agree) throws CapraException {
		ArrayList<ContractAgreementLinkImpl> linksToRemove = new ArrayList<ContractAgreementLinkImpl>();
		GenericTraceModelImpl model = null;
		PersistenceAdapter persistenceAdapter = ExtensionPointUtil.getTracePersistenceAdapter();
		Object traceModel = persistenceAdapter.getTraceModel(new ResourceSetImpl());
		if (traceModel instanceof GenericTraceModelImpl) {
			model = (GenericTraceModelImpl) traceModel;
			ContractAgreementLinkImpl traceLink = null;
			for (EObject trace : model.getTraces()) {
				if (trace instanceof ContractAgreementLinkImpl) {
					traceLink = (ContractAgreementLinkImpl) trace;
					if (EcoreUtil.getURI(traceLink.getSources()).equals(EcoreUtil.getURI(contract))) {
						for (EObject eobj : traceLink.getTargets()) {
							if (EcoreUtil.getURI(eobj).equals(EcoreUtil.getURI(agree))) {
								linksToRemove.add(traceLink);
							}
							// TODO 1:n connection to be considered
							break;
						}
					}
				}
			}
		}

		for (ContractAgreementLinkImpl link : linksToRemove) {
			model.getTraces().remove(link);
		}
		persistenceAdapter.saveTracesAndArtifacts(model, null);

		return;

	}

	public static void removeLink(FormalProperty formalProperty, Claim claim) throws CapraException {
		ArrayList<FormalPropertyClaimLinkImpl> linksToRemove = new ArrayList<FormalPropertyClaimLinkImpl>();
		GenericTraceModelImpl model = null;
		PersistenceAdapter persistenceAdapter = ExtensionPointUtil.getTracePersistenceAdapter();
		Object traceModel = persistenceAdapter.getTraceModel(new ResourceSetImpl());
		if (traceModel instanceof GenericTraceModelImpl) {
			model = (GenericTraceModelImpl) traceModel;
			FormalPropertyClaimLinkImpl traceLink = null;
			for (EObject trace : model.getTraces()) {
				if (trace instanceof FormalPropertyClaimLinkImpl) {
					traceLink = (FormalPropertyClaimLinkImpl) trace;
					if (EcoreUtil.getURI(traceLink.getSources()).equals(EcoreUtil.getURI(formalProperty))) {
						for (EObject eobj : traceLink.getTargets()) {
							if (EcoreUtil.getURI(eobj).equals(EcoreUtil.getURI(claim))) {
								linksToRemove.add(traceLink);
							}
							// TODO 1:n connection to be considered
							break;
						}
					}
				}
			}
		}

		for (FormalPropertyClaimLinkImpl link : linksToRemove) {
			model.getTraces().remove(link);
		}
		persistenceAdapter.saveTracesAndArtifacts(model, null);

		return;
	}

	public static void removeAnalysisContextArtefactLink(Class analysisContext_baseClass, Artefact artefact)
			throws CapraException {
		ArrayList<AnalysisContextArtefactLinkImpl> linksToRemove = new ArrayList<AnalysisContextArtefactLinkImpl>();
		GenericTraceModelImpl model = null;
		PersistenceAdapter persistenceAdapter = ExtensionPointUtil.getTracePersistenceAdapter();
		Object traceModel = persistenceAdapter.getTraceModel(new ResourceSetImpl());
		if (traceModel instanceof GenericTraceModelImpl) {
			model = (GenericTraceModelImpl) traceModel;
			AnalysisContextArtefactLinkImpl traceLink = null;
			for (EObject trace : model.getTraces()) {
				if (trace instanceof AnalysisContextArtefactLinkImpl) {
					traceLink = (AnalysisContextArtefactLinkImpl) trace;
					if (EcoreUtil.getURI(traceLink.getSources()).equals(EcoreUtil.getURI(analysisContext_baseClass))) {
						for (EObject eobj : traceLink.getTargets()) {
							if (EcoreUtil.getURI(eobj).equals(EcoreUtil.getURI(artefact))) {
								linksToRemove.add(traceLink);
							}
							// TODO 1:n connection to be considered
							break;
						}
					}
				}
			}
		}

		for (AnalysisContextArtefactLinkImpl link : linksToRemove) {
			model.getTraces().remove(link);
		}

		persistenceAdapter.saveTracesAndArtifacts(model, null);

		return;

	}

	public static void removeLink(Class component, ArgumentationElement arg) throws CapraException {
		ArrayList<ComponentArgumentationElementLinkImpl> linksToRemove = new ArrayList<ComponentArgumentationElementLinkImpl>();
		GenericTraceModelImpl model = null;
		PersistenceAdapter persistenceAdapter = ExtensionPointUtil.getTracePersistenceAdapter();
		Object traceModel = persistenceAdapter.getTraceModel(new ResourceSetImpl());
		if (traceModel instanceof GenericTraceModelImpl) {
			model = (GenericTraceModelImpl) traceModel;
			ComponentArgumentationElementLinkImpl traceLink = null;
			for (EObject trace : model.getTraces()) {
				if (trace instanceof ComponentArgumentationElementLinkImpl) {
					traceLink = (ComponentArgumentationElementLinkImpl) trace;
					if (EcoreUtil.getURI(traceLink.getSources()).equals(EcoreUtil.getURI(component))) {
						for (EObject eobj : traceLink.getTargets()) {
							if (EcoreUtil.getURI(eobj).equals(EcoreUtil.getURI(arg))) {
								linksToRemove.add(traceLink);
							}
							// TODO 1:n connection to be considered
							break;
						}
					}
				}
			}
		}

		for (ComponentArgumentationElementLinkImpl link : linksToRemove) {
			model.getTraces().remove(link);
		}

		persistenceAdapter.saveTracesAndArtifacts(model, null);

		return;

	}

}
