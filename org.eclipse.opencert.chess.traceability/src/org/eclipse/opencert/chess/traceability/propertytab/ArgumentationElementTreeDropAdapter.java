/*******************************************************************************
 * Copyright (c) 2017 Intecs SpA 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.chess.traceability.propertytab;

import org.eclipse.capra.core.CapraException;
import org.eclipse.emf.cdo.transfer.ui.TransferDropAdapter;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.opencert.chess.traceability.util.Utils;
import org.eclipse.opencert.sam.arg.arg.ArgumentationElement;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.uml2.uml.Class;

/**
 * Supports dropping gadgets into a tree viewer.
 */
public class ArgumentationElementTreeDropAdapter extends TransferDropAdapter {

	private Class component;

	public ArgumentationElementTreeDropAdapter(TableViewer viewer) {
		super(viewer);
	}

	public void setComponent(Class component) {
		this.component = component;
	}

	protected boolean performDrop(Object data, Object target) {
		super.performDrop(data, target);
		return false;

	}

	/**
	 * Method declared on ViewerDropAdapter
	 */
	public boolean performDrop(Object data) {

		Object selection = ((TreeSelection) data).getPaths()[0].getLastSegment();

		if (!(selection instanceof ArgumentationElement))
			return false;
		ArgumentationElement arg = (ArgumentationElement) selection;

		if (component != null) {
			Utils.trace(component, arg);
		}

		((TableViewer) this.getViewer()).add(arg);
		return true;
	}

	/**
	 * Method declared on ViewerDropAdapter
	 */
	public boolean validateDrop(Object target, int op, TransferData type) {
		return true;
	}
}