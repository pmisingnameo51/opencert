/*******************************************************************************
 * Copyright (c) 2017 Intecs SpA 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.chess.traceability.propertytab;

import java.util.Collection;

import org.eclipse.jface.viewers.IStructuredContentProvider;

public class ClaimTreeContentProvider implements IStructuredContentProvider {
	
	private static ClaimTreeContentProvider instance = null;

	@Override
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof Object[]) {
			return (Object[]) inputElement;
		}
        if (inputElement instanceof Collection) {
			return ((Collection) inputElement).toArray();
		}
        return new Object[0];
	}
	
	public static ClaimTreeContentProvider getInstance(){
		if (instance== null){
			instance = new ClaimTreeContentProvider();
		}
		return instance;
	}

}
