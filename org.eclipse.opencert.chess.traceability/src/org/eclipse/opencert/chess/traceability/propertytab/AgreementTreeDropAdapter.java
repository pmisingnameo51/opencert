/*******************************************************************************
 * Copyright (c) 2017 Intecs SpA 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.chess.traceability.propertytab;

import org.eclipse.capra.core.CapraException;
import org.eclipse.emf.cdo.transfer.ui.TransferDropAdapter;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.opencert.chess.traceability.util.Utils;
import org.eclipse.opencert.sam.arg.arg.Agreement;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.uml2.uml.Class;
import org.polarsys.chess.contracts.profile.chesscontract.Contract;

/**
 * Supports dropping gadgets into a tree viewer.
 */
public class AgreementTreeDropAdapter extends TransferDropAdapter {

	private Contract contract;
	private TreeSelection myselection;

	public AgreementTreeDropAdapter(TableViewer claimViewer) {
		super(claimViewer);
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	protected boolean performDrop(Object data, Object target) {
		super.performDrop(data, target);
		return false;

	}

	public TreeSelection getMySelection() {
		return myselection;
	}

	/**
	 * Method declared on ViewerDropAdapter
	 */
	public boolean performDrop(Object data) {

		Object selection = ((TreeSelection) data).getPaths()[0].getLastSegment();
		myselection = (TreeSelection) data;

		if (!(selection instanceof Agreement))
			return false;
		final Agreement agree = (Agreement) selection;

		Utils.trace(contract, agree);

		((TableViewer) this.getViewer()).add(agree);
		return true;
	}

	/**
	 * Method declared on ViewerDropAdapter
	 */
	public boolean validateDrop(Object target, int op, TransferData type) {
		return true;
	}

}