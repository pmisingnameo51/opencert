/*******************************************************************************
 * Copyright (c) 2018, MDH 
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *   Faiz Ul Muram
 *   Initial API and implementation and/or initial documentation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.epf.detectFallacies.actions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.net.URI;

import org.eclipse.epf.library.LibraryService;
import org.eclipse.epf.library.edit.util.TngUtil;
import org.eclipse.epf.uma.Activity;
import org.eclipse.epf.uma.BreakdownElement;
import org.eclipse.epf.uma.CapabilityPattern;
import org.eclipse.epf.uma.ContentDescription;
import org.eclipse.epf.uma.ContentElement;
import org.eclipse.epf.uma.ContentPackage;
import org.eclipse.epf.uma.MethodElement;
import org.eclipse.epf.uma.MethodLibrary;
import org.eclipse.epf.uma.MethodPackage;
import org.eclipse.epf.uma.MethodPlugin;
import org.eclipse.epf.uma.Phase;
import org.eclipse.epf.uma.Practice;
import org.eclipse.epf.uma.ProcessElement;
import org.eclipse.epf.uma.ProcessPackage;
import org.eclipse.epf.uma.Role;
import org.eclipse.epf.uma.RoleDescription;
import org.eclipse.epf.uma.RoleDescriptor;
import org.eclipse.epf.uma.Task;
import org.eclipse.epf.uma.TaskDescriptor;
import org.eclipse.epf.uma.Tool;
import org.eclipse.epf.uma.ToolMentor;
import org.eclipse.epf.uma.impl.ProcessComponentImpl;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

public class FallacyDetection implements IObjectActionDelegate {

	private List<Practice> listPracticesRoles = new ArrayList<Practice>(); 
	private List<ContentElement> listReferencesRoles = new ArrayList<ContentElement>(); 
	private List<Practice> listPracticesTools = new ArrayList<Practice>();
	private List<ContentElement> listReferencesTools = new ArrayList<ContentElement>();
	private List<String> visitedElements = new ArrayList<String>();
	private List<Boolean> resultsValidationTools = new ArrayList<Boolean>(); //false if there are an omission of key evidence, true if not
	private List<Boolean> resultsValidationStaff = new ArrayList<Boolean>(); //false if there are an omission of key evidence, true if not
	private BufferedWriter bwStaff;
	private BufferedWriter bwTool;
	private String path;
	private IWorkbenchPage page;
	private Shell shell;
	private MessageConsoleStream out;
	
	private List<String> arrayRoleMessageNoOk= new ArrayList<String>(); //list to store the message when the evidence is insufficient
	private List<String> arrayRoleNameNoOk= new ArrayList<String>(); //list to store the name of the role when the evidence is insufficient
	private List<String> arrayRoleNameOk= new ArrayList<String>(); //list to store the name of the role when the evidence is sufficient
	private int numRole;
	private List<String> arrayToolMessageNoOk= new ArrayList<String>();
	private List<String> arrayToolNameNoOk= new ArrayList<String>();
	private List<String> arrayToolNameOk= new ArrayList<String>();
	private int numTool;
	
	private String prepositions = " about , after , at , before , between , but , for , from , in , into , like , of , on , through "
								+ " to , under , up , with , within , without , the , and , a , an , is , are ";
	
	private String rationales = "because,due to,since";
	
	
	/**
	 * Constructor for Action1.
	 */
	public FallacyDetection() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		page = targetPart.getSite().getPage();
		shell = targetPart.getSite().getShell();
	}

	
	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		
		 MessageConsole pluginConsole = getConsole("Fallacy Detection Console");
		 out = pluginConsole.newMessageStream();
		
		//Clear variables
		visitedElements.clear();
		arrayRoleMessageNoOk.clear();
		arrayRoleNameNoOk.clear();
		arrayRoleNameOk.clear();
		arrayToolMessageNoOk.clear();
		arrayToolNameNoOk.clear();
		arrayToolNameOk.clear();
		resultsValidationStaff.clear();
		resultsValidationTools.clear();
		numRole=0;
		numTool=0;
		
		DialogManager dialogManager = new DialogManager(shell);
		
		try {
			Object[] arrayBuffWrit = dialogManager.createDialog();
			bwStaff=(BufferedWriter) arrayBuffWrit[0];
			bwTool=(BufferedWriter) arrayBuffWrit[1];
			path=(String) arrayBuffWrit[2];
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//ProgressMonitorDialog dialog = new ProgressMonitorDialog(shell);
		//dialog.open();
		//IProgressMonitor monitor = dialog.getProgressMonitor();
		//monitor.beginTask("Detecting fallacies. Please wait... ", monitor.UNKNOWN);
		
		if(bwStaff!=null){
			
			MethodLibrary src = LibraryService.getInstance().getCurrentMethodLibrary();
			Boolean flag = false;
			//Get the Method Plugins
			List<MethodPlugin> listPlugins = src.getMethodPlugins();
			for(MethodPlugin plugin: listPlugins){
				if(flag){break;}
				//Get the System Packages
				List systemPackages = TngUtil.getAllSystemPackages(plugin);
				for(Object sp: systemPackages){
					if(flag){break;}
					if(sp instanceof ContentPackage){
						//Get the Content Packages
						ContentPackage systemPackage = (ContentPackage) sp;
						if (systemPackage.getName().equals("CoreContent")){
							List<MethodPackage> methodPackages = systemPackage.getChildPackages();
							for(MethodPackage methodPackage: methodPackages){
								if(flag){break;}	
								ContentPackage contentPackage = (ContentPackage)methodPackage;
								List<ContentElement> contentElements = contentPackage.getContentElements();
								for(ContentElement contentElement: contentElements){
									//Get the Practice
									if(contentElement instanceof Practice){
										Practice practice = (Practice) contentElement;
										//Get the Sub Practices
										List<Practice> subPractices = practice.getSubPractices();
										for(Practice subPractice: subPractices){
											//Get the References
											List<ContentElement> references = subPractice.getContentReferences();
											for(ContentElement reference: references){
												//If the Reference is a Role, that sub practice belongs to the Staffing Plan Practice
												if(reference instanceof Role){
													listPracticesRoles.add(subPractice);
													listReferencesRoles.add(references.get(0));
													flag = true;
												}
												//If the Reference is a Tool, that sub practice belongs to the Tool Qualification Plan Practice
												if(reference instanceof Tool){
													listPracticesTools.add(subPractice);
													listReferencesTools.add(references.get(0));
													flag = true;
												}
											}
										}	
									}
								}
								break;
							}	
						}
					}
				}
			}		
			
			ISelection sel = page.getSelection();
			TreeSelection tree = (TreeSelection) sel;
			ProcessComponentImpl process = (ProcessComponentImpl) tree.getFirstElement();
			
			for (Iterator it = process.eContents().iterator(); it.hasNext();) {
				Object nextObj = it.next();			
				ProcessPackage child = nextObj instanceof ProcessPackage ?
						(ProcessPackage) nextObj : null;	
				if (child == null) {
					continue;
				}
				
				//Get the selected element (Delivery Process / Capability Pattern)
				if (child instanceof ProcessPackage){
					ProcessPackage proc = (ProcessPackage) child;
					
					//Get the Process Elements (Capability Patterns for Delivery Process / Phases for Capability Pattern)
					List<ProcessElement> listProcessElements = proc.getProcessElements();
					for (ProcessElement p: listProcessElements) {
						//If the selected element is a Delivery Process, for each Capability Pattern
						if (p instanceof CapabilityPattern){
							//get the Phases
							List<BreakdownElement> listPhases =((CapabilityPattern) p).getBreakdownElements();
							for (ProcessElement phase: listPhases){
								if (phase instanceof Phase){
									//get the Activities
									List<BreakdownElement> listActivities = ((Phase) phase).getBreakdownElements();
									//and get the rest of the elements
									getElements(listActivities);
								}
							}
						}
						
						//If the selected element is a Capability Pattern, for each Phase
						if (p instanceof Phase){
							//get the Activities
							List<BreakdownElement> listActivities = ((Phase) p).getBreakdownElements();
							//and get the rest of the elements
							getElements(listActivities);
						}
					}
				}
			}
			
			//monitor.done();
			//dialog.close();
			
    		try {
				createReport(bwStaff);
				createReport(bwTool);
				
				bwStaff.close();
				bwTool.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
	
			if(resultsValidationTools.contains(false)&&resultsValidationStaff.contains(false)){
				String rol ="";
				for (String r: arrayRoleNameNoOk){
					rol = rol+"   - "+r+" role\n";
				}
				
				String tol ="";
				for (String t: arrayToolNameNoOk){
					tol = tol+"   - "+t+" tool\n";
				}
				
				String m = "Validation completed successfully.\n\nRESULT: The presented evidence is insufficient for the following elements:\n"+rol+tol+"\n"+
							"Please look at the validation reports or console for further details and provide the omitted key evidences before doing the transformation.\n\n";
	    		
	    		File folder = new File(path);
	    		URI uriFolder = folder.toURI();
	    		
				dialogManager.showValidationResults(m, MessageDialog.ERROR, "Open Folder", uriFolder);

			}else{
				
				if(resultsValidationTools.contains(false)){
					String tol ="";
					for (String t: arrayToolNameNoOk){
						tol = tol+"   - "+t+"\n";
					}
					String m = "Validation completed successfully.\n\nRESULT: The presented evidence is insufficient for the following tools:\n"+tol+"\n"+
							"Please look at the Tool Qualification Plan report or console for further details and provide the omitted key evidences before doing the transformation.\n\n";
					
					File file = new File(path+"/Tool Qualification Plan Report.txt");  
		    		URI uriFile = file.toURI();
					
					dialogManager.showValidationResults(m, MessageDialog.ERROR, "Open File", uriFile);

				}else if(resultsValidationStaff.contains(false)){
					String rol ="";
					for (String r: arrayRoleNameNoOk){
						rol = rol+"   - "+r+"\n";
					}
					
					String m = "Validation completed successfully.\n\nRESULT: The presented evidence is insufficient for the following roles:\n"+rol+"\n"+
							"Please look at the Staffing Plan report or console for further details and provide the omitted key evidences before doing the transformation.\n\n";
					
					File file = new File(path+"/Staffing Plan Report.txt");  
		    		URI uriFile = file.toURI();
					
					dialogManager.showValidationResults(m, MessageDialog.ERROR, "Open File", uriFile);	

				}else{
					String m = "Validation completed successfully.\n\nRESULT: The presented key evidences are sufficient.\n\n"+
							"Please look at the validation reports or console for further details.\n\n";
					
		    		File folder = new File(path);
		    		URI uriFolder = folder.toURI();
					
					dialogManager.showValidationResults(m, MessageDialog.INFORMATION, "Open Folder", uriFolder);
				}
			}
		}
	}
		
	public void getElements(List<BreakdownElement> listActivities){
		//For each Activity
		for (BreakdownElement bde: listActivities){
			if (bde instanceof Activity){
				//Get the Tasks
				List<BreakdownElement> listTasks = ((Activity) bde).getBreakdownElements();
				for (BreakdownElement bde2: listTasks){
					if(bde2 instanceof TaskDescriptor){
						//Get the Roles that perform that Task
						TaskDescriptor taskDescriptor = (TaskDescriptor) bde2;
						List<RoleDescriptor> listRoles = taskDescriptor.getPerformedPrimarilyBy();
						for(RoleDescriptor r: taskDescriptor.getAdditionallyPerformedBy()){
							listRoles.add(r);
						}
						for(RoleDescriptor r: taskDescriptor.getAssistedBy()){
							listRoles.add(r);
						}
						for (RoleDescriptor rol: listRoles){
							Role role = rol.getRole();
							if(visitedElements.contains(role.getGuid())){
								continue;
							}
							visitedElements.add(role.getGuid());
							
							for (Iterator iter = role.eContents().iterator(); iter.hasNext();) {
								Object nextObject = iter.next();			
								MethodElement child2 = nextObject instanceof MethodElement ?
													(MethodElement) nextObject : null;		
								if (child2 == null) {
									continue;
								}
								
								//Get the Skills
								if (child2 instanceof RoleDescription){
									String skills = ((RoleDescription) child2).getSkills();
									
									//If the Role has any Practice that references to it, get the requirements and verify its skills
									for (ContentElement ref: listReferencesRoles){
										if (ref.getGuid().equals(role.getGuid())){
											int index = listReferencesRoles.indexOf(ref);
											String requirements = listPracticesRoles.get(index).getBriefDescription();
											try {
												validationRequirements(role.getPresentationName(), requirements, skills, role.getType().getName());
											} catch (IOException e) {
												e.printStackTrace();
											}
											break;
										}
									}
								}
							}
						}
				
						//Get the Tool Mentors of the task
						Task task = taskDescriptor.getTask();
						List<ToolMentor> listToolMentorsTask = task.getToolMentors();
						if(!listToolMentorsTask.isEmpty()){
							//For each Tool of the requirements, get the Tool Mentors
							for (ContentElement refTool: listReferencesTools){
								String qual="";
								Tool tool = (Tool) refTool;
								List<ToolMentor> listToolMentorsTool = tool.getToolMentors();
								//and check if the associated Tool of the Tool Mentor of the task has any requirements
								for(ToolMentor tm: listToolMentorsTool){
									for(ToolMentor tm2: listToolMentorsTask){
										//if it has
										if(tm.getGuid().equals(tm2.getGuid())){
											for (Iterator iter = tool.eContents().iterator(); iter.hasNext();) {
												Object nextObject = iter.next();			
												ContentDescription toolDescription = nextObject instanceof ContentDescription ?
																			(ContentDescription) nextObject : null;			
												if (toolDescription == null) {
													continue;
												}
												//get the qualification
												qual = toolDescription.getKeyConsiderations();
											}
											int index = listReferencesTools.indexOf(tool);
											//and the requirement
											String req = listPracticesTools.get(index).getBriefDescription();
											
											//and validate them
											if(!visitedElements.contains(tool.getGuid())){
												try {
													validationRequirements(tool.getPresentationName(), req, qual, tool.getType().getName());
													visitedElements.add(tool.getGuid());
												} catch (IOException e) {
													e.printStackTrace();
												}
											}
										}
									}
								}
							}
						}	
					}	
				}
			}	
		}
	}


	private void validationRequirements (String name, String requirements, String evidence, String type) throws IOException {
	switch(type){
	
		case("Role"):
			
			String arraySkills[];
			String arraySkillsNoPrep[]=null;
			String arrayRequirements[];
			String arrayReqNoPrep[]=null;
			
			arrayRequirements = requirements.split("; ");
			arrayReqNoPrep = requirements.split("; "); 
			//Remove the introduction sentence, e.g. "Shall be competent and have relevant experience:, Shall be competent in"
			StringBuffer stringbuff = new StringBuffer();	
			for (int i=0; i<arrayRequirements[0].length(); i++){
				char c = arrayRequirements[0].charAt(i);
				String subStr = stringbuff.append(c).toString();
				if (subStr.contains(":")||subStr.contains("competent in")||subStr.contains("shall be able to")){
					String req0 = arrayRequirements[0].substring(i+1, arrayRequirements[0].length());
					arrayRequirements[0] = req0;
					arrayReqNoPrep[0] = req0;
					stringbuff.delete(0, stringbuff.length());
					i=0;
				}
			}
	
			if (evidence.isEmpty()) {
				resultsValidationStaff.add(false);
				List<String> listRequirements = new ArrayList<String>();
				for(int i=0; i<arrayRequirements.length; i++){
					listRequirements.add(arrayRequirements[i]);
				}
				storeMissingEvidence(name, listRequirements, type);
				return;
				
			} else {	
				//Remove the HTML tags and special characters
				String skills5 = evidence.replaceAll("\\<[^>]*>","");
				String skills4 = skills5.replaceAll("\r\n    ", " ");
				String skills3 = skills4.replaceAll("\r\n", "");
				String skills2 = skills3.replaceAll("&amp;", "&");
				String skills1 = skills2.replaceAll("&nbsp;", " ");
				arraySkills = skills1.split("; ");
				
				//Remove the introduction sentence, e.g. "Certification against qualifications and relevant experience:"
				StringBuffer sb = new StringBuffer();			
				for (int i=0; i<arraySkills[0].length(); i++){
					char c = arraySkills[0].charAt(i);
					String subStr = sb.append(c).toString();
					if (subStr.contains("against")||subStr.contains(":")||subStr.contains("over")){
						String skill0 = arraySkills[0].substring(i+2, arraySkills[0].length());
						arraySkills[0] = skill0;
						sb.delete(0, sb.length());
						i=0;
					}
				}
				arraySkillsNoPrep = arraySkills;
			}
			
			List<String> omittedReq = new ArrayList<String>();
			List<Integer> usedSkills = new ArrayList<Integer>();
			String[] arrayPrepositions = prepositions.split(",");
			
			//For each requirement, check all the skills
			if (arrayRequirements!=null){
				for(int i=0; i<arrayRequirements.length; i++){
					for (int j=0; j<arraySkills.length; j++){
						if(usedSkills.contains(j)){
							continue;
						}
						
						String s = arrayReqNoPrep[i].trim();
						arrayReqNoPrep[i] = s;
						
						//Remove the prepositions and other non-main words
						for(String p: arrayPrepositions){
							if(arraySkills[j].contains(p)){
								arraySkillsNoPrep[j] = arraySkills[j].replace(p," ");
							}
							
							if(arrayRequirements[i].contains(p)){
								arrayReqNoPrep[i] = arrayReqNoPrep[i].replace(p," ");
							}
						}
						
						String[] arrayWordsRequirement;
						arrayWordsRequirement = arrayReqNoPrep[i].split(" ");
						double numWordsReq = arrayWordsRequirement.length;
						String[] arrayWordsSkill = arraySkillsNoPrep[j].split(" ");
						double numWordsMatch = 0;
						//Count how many skill words are in the requirement
						for (String w: arrayWordsSkill){
							if (arrayRequirements[i].toLowerCase().contains(w.toLowerCase())){
								numWordsMatch ++;
							}
						}
						
						//Get the match percentage between the total number of main words of the requirement
						//and the number of words that match the skill
						double percentMatch = numWordsMatch / numWordsReq;
						
						String[] words = rationales.split(",");
						Boolean isRationale = false;
						
						for (String word: words) {
							//Look for rationales
							if (arraySkillsNoPrep[j].contains(word)&&(percentMatch>=((double)1/3))) {
								usedSkills.add(j);
								resultsValidationStaff.add(true);
								isRationale = true;
								break;
							}
						}
						
						if (isRationale){
							break;
						}else{
							//If the match percentage is greater than 2/3, the meaning is the same, so the evidence is provided
							if (percentMatch >= ((double)2/3)){
								usedSkills.add(j);
								resultsValidationStaff.add(true);
								j=arraySkillsNoPrep.length;
							}else{
								if (j==arraySkills.length-1){
									resultsValidationStaff.add(false);
									omittedReq.add(arrayRequirements[i]);
								}
							}
						}
					}	
				}
				
				if(omittedReq.isEmpty()){
					arrayRoleNameOk.add(name.toUpperCase());
				}else{
					storeMissingEvidence(name, omittedReq, type);
				}
			}
			break;
			
		case("Tool"):
		
			if(evidence.isEmpty()){
				List<String> misReqTool = new ArrayList<String>();
				misReqTool.add(requirements);
				storeMissingEvidence(name, misReqTool, type);
				resultsValidationTools.add(false);
				
			}else{
				Boolean toolQual = false;
				String qualifications = "qualified by, previously been used";
				String[] qualificationsArray = qualifications.split(",");
				
				String[] rationalesArray = rationales.split(",");
				
				for(String q: qualificationsArray){
					if (evidence.toLowerCase().contains(q.toLowerCase())){
						toolQual = true;
					}
				}
				
				for(String r: rationalesArray){
					if (evidence.toLowerCase().contains(r.toLowerCase())){
						toolQual = true;
					}
				}
				
				if(toolQual){
					resultsValidationTools.add(true);
					arrayToolNameOk.add(name);			
				}else{	
					resultsValidationTools.add(false);
					List<String> misReqTool = new ArrayList<String>();
					misReqTool.add(requirements);
					storeMissingEvidence(name, misReqTool, type);	
				}		
			}
			break;
		}
	}

	public void storeMissingEvidence (String name, List<String> omittedReq, String type) {	
		String m ="";
		if(type.equals("Role")){
			arrayRoleNameNoOk.add(name);
			for(String o:omittedReq){
				if(!o.isEmpty()){
					m=m+o+";";
				}
			}
			arrayRoleMessageNoOk.add(m);
		}
		
		if(type.equals("Tool")){
			arrayToolNameNoOk.add(name);
			for(String o:omittedReq){
				if(!o.isEmpty()){
					m=m+o+";";
				}
			}
			arrayToolMessageNoOk.add(m);	
		}
    }
	
	
	public void createReport(BufferedWriter bw) throws IOException{
		if (bw.equals(bwStaff)){
			if(!arrayRoleMessageNoOk.isEmpty()){
				bw.write("Certification against following roles are INSUFFICIENT: ");
				out.println("Certification against following ROLES are INSUFFICIENT: ");
				for (String s: arrayRoleNameNoOk){
					bw.newLine();
					numRole++;
					bw.write("   "+numRole+". "+s.toUpperCase());
					out.println("   "+numRole+". "+s.toUpperCase());
					bw.newLine();
					bw.write("\tDETECTED FALLACIES: Certifications against following competencies/requirements are omitted:");
					out.println("\tDETECTED FALLACIES: Certifications against following competencies/requirements are omitted:");
					for(String st: arrayRoleMessageNoOk){
						if (arrayRoleMessageNoOk.indexOf(st)==arrayRoleNameNoOk.indexOf(s)){
							for (String str: st.split(";")){
								bw.newLine();
								String stri = str.trim();
								bw.write("\t"+"- "+stri);
								out.println("\t"+"- "+stri);
							}
							bw.newLine();
							bw.write("\tRECOMMENDATION: Add skill certifications against above omitted evidence for the "+s+" role\r\n\tto achieve sufficiency or provide rationale for its omission.");
							out.println("\tRECOMMENDATION: Add skill certifications against above omitted evidence for the "+s+" role\r\n\tto achieve sufficiency or provide rationale for its omission.");
							bw.newLine();
						}
					}
				}
			}
			
			if(!arrayRoleMessageNoOk.isEmpty()){
				bw.newLine();
				bw.newLine();
				out.println();
			}
			bw.write("Certification against following roles are SUFFICIENT: ");
			out.println("Certification against following ROLES are SUFFICIENT: ");
	
			for (String s: arrayRoleNameOk){
				bw.newLine();
				numRole++;
				bw.write("   "+numRole+". "+s.toUpperCase());
				out.println("   "+numRole+". "+s.toUpperCase());
			}
		}
		
		if(bw.equals(bwTool)){
			if(!arrayToolMessageNoOk.isEmpty()){
				out.println("\n");
				bw.write("Qualification against following tools are INSUFFICIENT: ");
				out.println("Qualification against following TOOLS are INSUFFICIENT: ");
				for (String s: arrayToolNameNoOk){
					bw.newLine();
					numTool++;
					bw.write("   "+numTool+". "+s.toUpperCase()+". Certifications against following requirements are omitted:");
					out.println("   "+numTool+". "+s.toUpperCase()+". Certifications against following requirements are omitted:");
					for(String st: arrayToolMessageNoOk){
						if (arrayToolMessageNoOk.indexOf(st)==arrayToolNameNoOk.indexOf(s)){
							for (String str: st.split(";")){
								bw.newLine();
								String stri = str.trim();
								bw.write("\t"+"- "+stri);
								out.println("\t"+"- "+stri);
							}
							bw.newLine();
							bw.write("\tRECOMMENDATION: Add tool certifications against above omitted evidence for the "+s+" tool\r\n\tto achieve sufficiency or provide rationale for its omission.");
							out.println("\tRECOMMENDATION: Add tool certifications against above omitted evidence for the "+s+" tool\r\n\tto achieve sufficiency or provide rationale for its omission.");
							bw.newLine();
						}
					}
				}
			}

			if(!arrayToolMessageNoOk.isEmpty()){
				bw.newLine();
				bw.newLine();
				out.println();
			}
			bw.write("Certification against following tool qualifications are SUFFICIENT: ");
			out.println();
			out.println("Certification against following TOOL qualifications are SUFFICIENT: ");
			for (String s: arrayToolNameOk){
				bw.newLine();
				numTool++;
				bw.write("   "+numTool+". "+s.toUpperCase());
				out.println("   "+numTool+". "+s.toUpperCase());
			}
			out.println("\n");
		}
	}
	
	
	private MessageConsole getConsole(String name) {
		ConsolePlugin consolePlugin = ConsolePlugin.getDefault();
		IConsoleManager conMan = consolePlugin.getConsoleManager();
		IConsole[] existing = conMan.getConsoles();
		for (int i = 0; i < existing.length; i++){
			if (name.equals(existing[i].getName())){
		    	return (MessageConsole) existing[i];
		    }
		}
		MessageConsole plugConsole = new MessageConsole(name, null);
		conMan.addConsoles(new IConsole[]{plugConsole});
		return plugConsole;
	}
	
	
	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		System.out.println(selection);	
	}
}