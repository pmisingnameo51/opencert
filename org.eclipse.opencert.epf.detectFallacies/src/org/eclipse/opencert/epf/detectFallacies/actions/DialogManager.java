/*******************************************************************************
 * Copyright (c) 2018, MDH 
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *   Faiz Ul Muram
 *   Initial API and implementation and/or initial documentation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.epf.detectFallacies.actions;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Shell;

public class DialogManager {
	
	private Shell shell;
	private Object[] arrayBuffWrit = new Object[3];
	
	public DialogManager(Shell shell) {
		this.shell = shell;
	}
	

	public Object[] createDialog() throws IOException{
		for (int i=0; i<arrayBuffWrit.length; i++){
			arrayBuffWrit[i]=null;
		}
		MessageDialog dg = new MessageDialog(
	            shell,
	            "Select directory",
	            null,
	            "Please specify a target directory to store the result of the validation",
	            MessageDialog.INFORMATION,
	            new String[]{
	                "Browse...", 
	                IDialogConstants.CANCEL_LABEL},
	            -1//SWT.CLOSE
	            );
	    
		switch(dg.open()) {
	    case 0: 
	    	createDirectoryChooser();
	        break;
	    case 1:
	    	confirmExit(1);
	    	break;
	    	
		case -1:
			confirmExit(1);
	        break;
		}
		
		return arrayBuffWrit;
	}                 
	
	
	public void createDirectoryChooser() throws IOException{
		for (int i=0; i<arrayBuffWrit.length; i++){
			arrayBuffWrit[i]=null;
		}
        DirectoryDialog dialog = new DirectoryDialog(shell, SWT.Close);
	    dialog.setMessage("Please specify a directory to store the result of the validation.");
	    dialog.setText("Browse For Folder");

	    String path = dialog.open();
	    if (path != null) {
	    	arrayBuffWrit[2]=path;
	    	FileWriter filewriterStaff = new FileWriter(path+"/Staffing Plan Report.txt");
	    	BufferedWriter bwStaff = new BufferedWriter(filewriterStaff);
			arrayBuffWrit[0]=bwStaff;
			FileWriter filewriterTool = new FileWriter(path+"/Tool Qualification Plan Report.txt");
			BufferedWriter bwTool = new BufferedWriter(filewriterTool);
			arrayBuffWrit[1]=bwTool;
	    }else{
	    	confirmExit(2);
	    }
	}
	
	
	public void confirmExit(int i) throws IOException {
		MessageDialog dg = new MessageDialog(
	            shell,
	            "Confirm",
	            null,
				"Selecting a target directory is mandatory, by closing the window the validation will be cancelled.\nDo you really want to cancel the process?",
	            MessageDialog.INFORMATION,
	            new String[]{
	                IDialogConstants.YES_LABEL, 
	                IDialogConstants.NO_LABEL},
	            -1//SWT.CLOSE
	            );
	    
		switch(dg.open()) {
	    case 0: 
	    	dg.close();
	        break;
	    case 1:
	    	if (i==1){
	    		createDialog();
	    	}
	    	if(i==2){
	    		createDirectoryChooser();
	    	}
	    	break;
		case -1:
			dg.close();
			break;
		}
	}

	
	public void showValidationResults(String message, int type, String labelButton, URI uri){	
		MessageDialog mdialog = new MessageDialog(
	            shell,
	            "Validation Completion",
	            null,
	            message,
	            type,
	            new String[]{
	                IDialogConstants.OK_LABEL, 
	                labelButton},
	            SWT.CLOSE //0 
	            );
	    
		switch(mdialog.open()) {
	    case 0: 
	    	mdialog.close();
	        break;
	    case 1:
	        try{
	    		Runtime r = Runtime.getRuntime();
	        	Process p = r.exec("cmd /c start "+uri);
	    	}catch (IOException e1){
	            e1.printStackTrace();
	        }
	        break;
	    }
	}
}
