/*******************************************************************************
 * Copyright (c) 2017, MDH 
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *   Irfan Sljivo <irfan.sljivo@mdh.se || irfan.sljivo@gmail.com>
 *   Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.chess.argumentGenerator.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.window.Window;
import org.eclipse.opencert.chess.argumentGenerator.dialogs.ArgumentationGenerationDialog;
import org.eclipse.opencert.chess.argumentGenerator.dialogs.SelectOcraAnalysisCtxDialog;
import org.eclipse.papyrus.editor.PapyrusMultiDiagramEditor;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.uml2.uml.Model;
import org.polarsys.chess.core.util.uml.ResourceUtils;

public class ArgumentationGenerationHandler extends AbstractHandler {
	
	private String systemQN;
	private Shell activeShell;
	private String argumentationFolder;
	private Boolean checkWeakContracts;
	private PapyrusMultiDiagramEditor editor;
	
	public ArgumentationGenerationHandler() {
	}

	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		IEditorPart editorPart = HandlerUtil.getActiveEditor(event);
//		if (!(CHESSEditorUtils.isCHESSProject(editorPart)))
//			return null;
		
		PapyrusMultiDiagramEditor editor = (PapyrusMultiDiagramEditor)editorPart;
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		Shell activeShell = window.getShell();
		Model model;
		String modelname;
		try {
			//First get the analysis context to know the source system and whether validated contracts should be used or the selected ones
			Resource res = ResourceUtils.getUMLResource(editor.getServicesRegistry());
			model = ResourceUtils.getModel(res);
			modelname = model.getName();

			SelectOcraAnalysisCtxDialog dialog = new SelectOcraAnalysisCtxDialog(activeShell, model);
			dialog.create();
			if (dialog.open() == Window.OK) {
				systemQN = dialog.getSystem();
				checkWeakContracts=dialog.getCheckWeakContracts();
				if(systemQN == null || systemQN.isEmpty()){
					return null;
				}
			}else{
				return null;
			}

		String systemName = systemQN.substring(systemQN.lastIndexOf("::")+2);
		
		//Get the destination Argumentation folder from the assurance cases on the CDO and start the transformation
		ArgumentationGenerationDialog changeDialog = new ArgumentationGenerationDialog(activeShell,editor,model,systemQN,checkWeakContracts);
		changeDialog.open();
		
		} catch (ServiceException e) {
			e.printStackTrace();
			return null;
		}
		
		return null;
	}
	
	
}
