/*******************************************************************************
 * Copyright (c) 2017, MDH 
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *   Irfan Sljivo <irfan.sljivo@mdh.se || irfan.sljivo@gmail.com>
 *   Initial API and implementation and/or initial documentation
 *******************************************************************************/
package org.eclipse.opencert.chess.argumentGenerator.dialogs;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.cdo.common.security.CDOPermission;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.opencert.chess.argumentGenerator.argumentation.CDOResourceListLabelProvider;
import org.eclipse.opencert.chess.argumentGenerator.argumentation.CHESSContract2OpencertArgumentGenerator;
import org.eclipse.papyrus.editor.PapyrusMultiDiagramEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Model;

public class ArgumentationGenerationDialog extends Dialog {
	
	private CDOView view;
	private ArrayList refList = new ArrayList();
	private ArrayList<String> refListDir = new ArrayList();
	private ComboViewer comboViewerSelectArgModel;
	private static final String ARGUMENTATION_NsPrefix = ".arg";
	HashMap<Object, Object> options = new HashMap<Object, Object>();
	private static final String UTF_8 = "UTF-8";
	private Model model;
	private String systemQN;
	private Boolean checkWeakContracts;
	public ArgumentationGenerationDialog(Shell parentShell,PapyrusMultiDiagramEditor editor,Model inputModel, String systemQN, Boolean weakContracts) {
		super(parentShell);
		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		CDOSession session = CDOConnectionUtil.instance.getCurrentSession();
		view = CDOConnectionUtil.instance.openView(session);
		options.put(XMLResource.OPTION_ENCODING, UTF_8);
		this.model=inputModel;
		this.systemQN=systemQN;
		this.checkWeakContracts=weakContracts;
	}

	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(1, false));
		
		Label lblSelectASystem = new Label(container, SWT.NONE);
		lblSelectASystem.setText("Select a system integration argumentation model");
		
		comboViewerSelectArgModel = new ComboViewer(container, SWT.NONE);
		Combo combo = comboViewerSelectArgModel.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		comboViewerSelectArgModel.setContentProvider(new ArrayContentProvider());
		comboViewerSelectArgModel.setLabelProvider(new CDOResourceListLabelProvider());
		ProgressMonitorDialog dialog = new ProgressMonitorDialog(getShell());
		dialog.open();
		IProgressMonitor monitor = dialog.getProgressMonitor();
		monitor.beginTask("Searching for argumentation folders", 20);

		try {			
			populateArgModels();
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		monitor.worked(1);
		dialog.close();
		return container;
	}

	protected void createButtonsForButtonBar(Composite parent) {
		Button button = createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {

				MessageDialog dialog = new MessageDialog(ArgumentationGenerationDialog.this.getParentShell(), "Argumentation Generation", null,
						"Argument-fragments Generated", MessageDialog.INFORMATION, new String[] { "OK",}, 0);
				ProgressMonitorDialog monitorDialog = new ProgressMonitorDialog(getShell());
				monitorDialog.open();
				IProgressMonitor monitor = monitorDialog.getProgressMonitor();
				monitor.beginTask("Beginning argument-fragement generation", 20);
				
				CHESSContract2OpencertArgumentGenerator argumentGenerator= new CHESSContract2OpencertArgumentGenerator(monitor,  getDestinationFolder(), model, systemQN,checkWeakContracts);
				argumentGenerator.generateArgumentationFragments();
				//generateArgumentationFragments(monitor,getDestinationFolder(),model);
				int result = dialog.open();
				ArgumentationGenerationDialog.this.close();
						}
		});
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	protected Point getInitialSize() {
		return new Point(450, 300);
	}
	
	public CDOResourceFolder getDestinationFolder()
	{
		return (CDOResourceFolder)((IStructuredSelection)comboViewerSelectArgModel.getSelection()).getFirstElement();
	}
	

	public void populateArgModels()
	{
		addFilesOfDir();
		setArgModelListInput(refList.toArray());
	}
	
	public int addFilesOfDir()
	{      
		  int sumStd=0;
		  CDOResourceNode[] listR=  view.getElements();   
	      for (int i=0; i<listR.length; i++){
	            if(listR[i] instanceof CDOResourceFolder){
	            	//System.out.println(((CDOResourceFolder)listR[i]).getName());
	                if(((CDOResourceFolder)listR[i]).getName().equalsIgnoreCase("argumentation"))  
	                {
	                	refList.add(listR[i]);
		                refListDir.add(listR[i].getPath());
		                sumStd++;
	                }
	                else
	                	checkFolderContents((CDOResourceFolder)listR[i],sumStd);
	            	
	            }
                     
	      }
	      return sumStd;
	}
	
	public static boolean hasWritePermission(CDOResourceNode node) {
		// force to get the last revision
		node.cdoReload();
		CDOPermission permission = node.cdoRevision().getPermission();
		return permission.isWritable();
	}
	
	private void checkFolderContents(CDOResourceFolder cdoResourceFolder, int sumStd) {
		if (hasWritePermission(cdoResourceFolder)) {
			EList<CDOResourceNode> listN=  cdoResourceFolder.getNodes();
			for (int i=0; i<listN.size(); i++){
				if(listN.get(i) instanceof CDOResourceFolder){
					if(((CDOResourceFolder)listN.get(i)).getName().equalsIgnoreCase("argumentation"))  
	                {
						//System.out.println(listN.get(i).getPath());
						refList.add(listN.get(i));
			            refListDir.add(listN.get(i).getPath());
			            sumStd++;
	                }
	                else
	                	checkFolderContents((CDOResourceFolder)listN.get(i),sumStd);
		        }
		   }
		}
	}
	
	public void setArgModelListInput(Object[] cdoResource)
	{
		comboViewerSelectArgModel.setInput(cdoResource);
		comboViewerSelectArgModel.setSorter(new ViewerSorter() {
			public int compare(
					Viewer viewer, Object c1, Object c2) {
						return ((CDOResourceFolder) c1).getPath().compareToIgnoreCase(((CDOResourceFolder) c2).getPath());
					}
			});
	}
	
}
