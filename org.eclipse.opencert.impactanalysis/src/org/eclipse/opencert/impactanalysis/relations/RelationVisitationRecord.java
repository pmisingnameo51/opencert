/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.impactanalysis.relations;

import org.eclipse.opencert.infra.general.general.ChangeEffectKind;

public class RelationVisitationRecord
{
    private final String impactingArtefactRelationUniqueId;
    private final ChangeEffectKind impactedChangeEffectKind;
    private final ArtefactRelationType artefactRelationType;
    
    public RelationVisitationRecord(IArtefactRelationImpact artefactRelationImpact)
    {
        this.impactingArtefactRelationUniqueId = artefactRelationImpact.getImpactingArtefactRelationUniqueId();
        this.impactedChangeEffectKind = artefactRelationImpact.getImpactedChangeEffectKind();
        this.artefactRelationType = artefactRelationImpact.getArtefactRelationType();
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((impactingArtefactRelationUniqueId == null) ? 0 : impactingArtefactRelationUniqueId.hashCode());
        result = prime
                * result
                + ((artefactRelationType == null) ? 0 : artefactRelationType
                        .hashCode());
        result = prime
                * result
                + ((impactedChangeEffectKind == null)
                        ? 0
                        : impactedChangeEffectKind.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RelationVisitationRecord other = (RelationVisitationRecord) obj;
        if (impactingArtefactRelationUniqueId == null) {
            if (other.impactingArtefactRelationUniqueId != null)
                return false;
        } else if (!impactingArtefactRelationUniqueId.equals(other.impactingArtefactRelationUniqueId))
            return false;
        if (artefactRelationType != other.artefactRelationType)
            return false;
        if (impactedChangeEffectKind != other.impactedChangeEffectKind)
            return false;
        return true;
    }
    
    
}
