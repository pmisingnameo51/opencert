/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.impactanalysis.relations;

import org.eclipse.opencert.apm.assuranceassets.assuranceasset.EventKind;
import org.eclipse.opencert.infra.general.general.ChangeEffectKind;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel;
import org.eclipse.opencert.storage.cdo.CDOStorageUtil;

class ArtefactsRelationImpact
    extends AbstractArtefactsRelationImpact
{
    private ChangeEffectKind impactingArtefactRelModificationEffect;
    private ChangeEffectKind impactingArtefactRelRevocationEffect;
    
    private String userFriendlyImpactedArtefactName;
    private String userFriendlyImpactingArtefactName;
    private String relationName;
    
    protected ArtefactsRelationImpact(int recursionDepth, ArtefactRel impactingArtefactRel, ChangeEffectKind impactedChangeEffectKind, EventKind impactingEventKind)
    {
        super(recursionDepth, ArtefactRelationType.DIRECT_EVIDENCE_RELATION,
                impactedChangeEffectKind, impactingEventKind,
                String.valueOf(CDOStorageUtil.getCDOId(impactingArtefactRel)),
                CDOStorageUtil.getCDOId(impactingArtefactRel.getTarget()),
                CDOStorageUtil.getCDOId(impactingArtefactRel.getSource()));
        
        impactingArtefactRelModificationEffect = impactingArtefactRel.getModificationEffect();
        impactingArtefactRelRevocationEffect = impactingArtefactRel.getRevocationEffect();
        
        userFriendlyImpactedArtefactName = impactingArtefactRel.getSource().getName();
        userFriendlyImpactingArtefactName = impactingArtefactRel.getTarget().getName();
        relationName = impactingArtefactRel.getName();
    }

    @Override
    public String getUserFriendlyRelationName()
    {
        return relationName;
    }
    
    
    @Override
    public String getUserFriendlyImpactedArtefactName()
    {
        return userFriendlyImpactedArtefactName;
    }

    
    @Override
    public String getUserFriendlyImpactingArtefactName()
    {
        return userFriendlyImpactingArtefactName;
    }

    
    public ChangeEffectKind getImpactingArtefactRelModificationEffect()
    {
        return impactingArtefactRelModificationEffect;
    }
    
    
    public ChangeEffectKind getImpactingArtefactRelRevocationEffect()
    {
        return impactingArtefactRelRevocationEffect;
    }
}
