/*******************************************************************************
 * Copyright (c) 2016 Parasoft.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Janusz Studzizba - initial API and implementation
 *   Dariusz Oszczedlowski - initial API and implementation
 *   Magdalena Gniewek - initial API and implementation
 *   Michal Wlodarczyk - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.impactanalysis.test;

import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.opencert.infra.general.general.ChangeEffectKind;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactDefinition;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactModel;
import org.eclipse.opencert.evm.evidspec.evidence.ArtefactRel;
import org.eclipse.opencert.evm.evidspec.evidence.EvidenceFactory;
import org.eclipse.opencert.storage.cdo.StandaloneCDOAccessor;
import org.eclipse.opencert.storage.cdo.executors.SimpleInTransactionExecutor;
import org.eclipse.opencert.storage.cdo.test.DataCreationRoutines;

public class ArtefactsImpactStructureCreator
{
    
    public static void main(String[] args)
    {
        createComplexImpactStructure();
    }

    
    /*
     * 
     * ArtefactA --MODIFY, REVOKE--> ArtefactB --REVOKE, REVOKE--> ArtefactC (...)
     * 
     * 
     * (...) ArtefactC --MODIFY, VALIDATE--> ArtefactD --MODIFY, VALIDATE--> ArtefactE 
     * 
     */
    private static void createBasicImpactStructure()
    {
        new SimpleInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                Resource resource = StandaloneCDOAccessor.getDefaultResource(cdoTransaction);
                
                Artefact notImpactingArtefact = DataCreationRoutines.createArtefact("NotImpactingArtefact", 1);
                
                Artefact artefactA = DataCreationRoutines.createArtefact("ArtefactA", 1);
                Artefact artefactB = DataCreationRoutines.createArtefact("ArtefactB", 2);
                Artefact artefactC = DataCreationRoutines.createArtefact("ArtefactC", 3);
                Artefact artefactD = DataCreationRoutines.createArtefact("ArtefactD", 4);
                Artefact artefactE = DataCreationRoutines.createArtefact("ArtefactE", 5);
                
                /*
                 * The current semantics is "source artefact depends on target artefact", so
                 * the changes in the target may impact the source.
                 * 
                 */
                
                ArtefactRel artefactRelA = DataCreationRoutines.createArtefactRel(
                        "ArtefactRelA", artefactB, artefactA, ChangeEffectKind.MODIFY, ChangeEffectKind.REVOKE);
                ArtefactRel artefactRelB = DataCreationRoutines.createArtefactRel(
                        "ArtefactRelB", artefactC, artefactB, ChangeEffectKind.REVOKE, ChangeEffectKind.REVOKE);
                ArtefactRel artefactRelC = DataCreationRoutines.createArtefactRel(
                        "ArtefactRelC", artefactD, artefactC, ChangeEffectKind.MODIFY, ChangeEffectKind.VALIDATE);
                ArtefactRel artefactRelD = DataCreationRoutines.createArtefactRel(
                        "ArtefactRelD", artefactE, artefactD, ChangeEffectKind.MODIFY, ChangeEffectKind.VALIDATE);
                
                
                resource.getContents().add(notImpactingArtefact);
                resource.getContents().add(artefactA);
                resource.getContents().add(artefactB);
                resource.getContents().add(artefactC);
                resource.getContents().add(artefactD);
                resource.getContents().add(artefactE);
                
                resource.getContents().add(artefactRelA);
                resource.getContents().add(artefactRelB);
                resource.getContents().add(artefactRelC);
                resource.getContents().add(artefactRelD);
                
            }
        }.executeReadWriteOperation();
    }
    
    
    /*
     * ArtefactAA -- impacts via ArtefactRelAA (MODIFY) --> ArtefactXXX  -- impacts via ArtefactRelXXX (MODIFY) --> ArtefactZZZ
       ArtefactBB -- impacts via ArtefactRelBB (MODIFY) --> ArtefactXXX  -- impacts via ArtefactRelXXX (MODIFY) --> ArtefactZZZ
       ArtefactCC -- impacts via ArtefactRelCC (MODIFY) --> ArtefactXXX  -- impacts via ArtefactRelXXX (MODIFY) --> ArtefactZZZ

       this should be minimized to: 

       ArtefactAA -- impacts via ArtefactRelAA (MODIFY) --> ArtefactXXX  -- impacts via ArtefactRelXXX (MODIFY) --> ArtefactZZZ
       ArtefactBB -- impacts via ArtefactRelBB (MODIFY) --> ArtefactXXX
       ArtefactCC -- impacts via ArtefactRelCC (MODIFY) --> ArtefactXXX
     * 
     * 
    */
    private static void createImpactStructureThatWillHavePropagationGraphMinimized()
    {
        new SimpleInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                Resource resource = StandaloneCDOAccessor.getDefaultResource(cdoTransaction);
                
                Artefact artefactAA = DataCreationRoutines.createArtefact("ArtefactAAA", 1001);
                Artefact artefactBB = DataCreationRoutines.createArtefact("ArtefactBBB", 1002);
                Artefact artefactCC = DataCreationRoutines.createArtefact("ArtefactCCC", 1003);
                
                Artefact artefactXXX = DataCreationRoutines.createArtefact("ArtefactXXX", 1004);
                Artefact artefactZZZ = DataCreationRoutines.createArtefact("ArtefactZZZ", 1005);
                
                /*
                 * The current semantics is "source artefact depends on target artefact", so
                 * the changes in the target may impact the source.
                 * 
                 */
                
                ArtefactRel artefactRelAA = DataCreationRoutines.createArtefactRel(
                        "ArtefactRelAAA", artefactXXX, artefactAA, ChangeEffectKind.MODIFY, ChangeEffectKind.MODIFY);
                ArtefactRel artefactRelBB = DataCreationRoutines.createArtefactRel(
                        "ArtefactRelBBB", artefactXXX, artefactBB, ChangeEffectKind.MODIFY, ChangeEffectKind.MODIFY);
                ArtefactRel artefactRelCC = DataCreationRoutines.createArtefactRel(
                        "ArtefactRelCCC", artefactXXX, artefactCC, ChangeEffectKind.MODIFY, ChangeEffectKind.MODIFY);
                
                ArtefactRel artefactRelXXX = DataCreationRoutines.createArtefactRel(
                        "ArtefactRelDDD", artefactZZZ, artefactXXX, ChangeEffectKind.MODIFY, ChangeEffectKind.MODIFY);
                
                
                resource.getContents().add(artefactAA);
                resource.getContents().add(artefactBB);
                resource.getContents().add(artefactCC);
                resource.getContents().add(artefactXXX);
                resource.getContents().add(artefactZZZ);
                
                resource.getContents().add(artefactRelAA);
                resource.getContents().add(artefactRelBB);
                resource.getContents().add(artefactRelCC);
                resource.getContents().add(artefactRelXXX);
                
            }
        }.executeReadWriteOperation();
    }
    
    
    
    /*
     * This can be a very deep structure where every 2nd (0, 2, 4...) level (marked as (...) below) has similar structure - like this:
     * 
     * 
     * Artefact0_1 --MODIFY, REVOKE  --> Artefact1_1 -- REVOKE, REVOKE --> Artefact2_1 (...)
     *            --MODIFY, REVOKE  --> Artefact1_2 -- MODIFY, MODIFY --> Artefact2_2 (...)
     *            --MODIFY, NONE    --> Artefact1_3 -- MODIFY, REVOKE --> Artefact2_3 (...)
     *            --NONE, REVOKE    --> Artefact1_4 -- MODIFY, REVOKE --> Artefact2_4 (...) 
     *            --MODIFY, VALIDATE--> Artefact1_5 -- MODIFY, REVOKE --> Artefact2_5 (...)
     *            --VALIDATE, REVOKE--> Artefact1_6 -- MODIFY, REVOKE --> Artefact2_6 (...) 
     * 
     * Artefact2_1 --MODIFY, REVOKE --> Artefact3_1 -- REVOKE, REVOKE --> Artefact4_1 (...)
     *            --MODIFY, REVOKE  --> Artefact3_2 -- MODIFY, MODIFY --> Artefact4_2 (...)
     *            --MODIFY, NONE    --> Artefact3_3 -- MODIFY, REVOKE --> Artefact4_3 (...)
     *            --NONE, REVOKE    --> Artefact3_4 -- MODIFY, REVOKE --> Artefact4_4 (...) 
     *            --MODIFY, VALIDATE--> Artefact3_5 -- MODIFY, REVOKE --> Artefact4_5 (...)
     *            --VALIDATE, REVOKE--> Artefact3_6 -- MODIFY, REVOKE --> Artefact4_6 (...)
     * 
     */
    private static void createComplexImpactStructure()
    {
        new SimpleInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                Resource resource = StandaloneCDOAccessor.getDefaultResource(cdoTransaction);
                
                Artefact rootArtefact = DataCreationRoutines.createArtefact("RootArtefact_test_single", 1);
                resource.getContents().add(rootArtefact);
                
                createResursively(resource, 1, rootArtefact, 3, "ts");
            }
        }.executeReadWriteOperation();
    }
    
    
    private static void createResursively(Resource resource, int level, Artefact localRootArtefact, int maxLevel, String variantPrefix)
    {
        if (level > maxLevel) {
            return;
        }
        
        System.out.println("Level: " + level);
        
        
        Artefact artefact1 = DataCreationRoutines.createArtefact(variantPrefix + "_Artefact" + level + "_1", 1);
        Artefact artefact2 = DataCreationRoutines.createArtefact(variantPrefix + "_Artefact" + level + "_2", 2);
        Artefact artefact3 = DataCreationRoutines.createArtefact(variantPrefix + "_Artefact" + level + "_3", 3);
        Artefact artefact4 = DataCreationRoutines.createArtefact(variantPrefix + "_Artefact" + level + "_4", 4);
        Artefact artefact5 = DataCreationRoutines.createArtefact(variantPrefix + "_Artefact" + level + "_5", 5);
        Artefact artefact6 = DataCreationRoutines.createArtefact(variantPrefix + "_Artefact" + level + "_6", 6);
        
        Artefact artefact21 = DataCreationRoutines.createArtefact(variantPrefix + "_Artefact" + (level+1) + "_1", 7);
        Artefact artefact22 = DataCreationRoutines.createArtefact(variantPrefix + "_Artefact" + (level+1) + "_2", 8);
        Artefact artefact23 = DataCreationRoutines.createArtefact(variantPrefix + "_Artefact" + (level+1) + "_3", 9);
        Artefact artefact24 = DataCreationRoutines.createArtefact(variantPrefix + "_Artefact" + (level+1) + "_4", 10);
        Artefact artefact25 = DataCreationRoutines.createArtefact(variantPrefix + "_Artefact" + (level+1) + "_5", 11);
        Artefact artefact26 = DataCreationRoutines.createArtefact(variantPrefix + "_Artefact" + (level+1) + "_6", 12);
        
        
        
        /*
         * The current semantics is "source artefact depends on target artefact", so
         * the changes in the target may impact the source.
         * 
         */
        
        ArtefactRel artefactRelA = DataCreationRoutines.createArtefactRel(
                variantPrefix + "_ArtefactRelA_" + level, artefact1, localRootArtefact, ChangeEffectKind.MODIFY, ChangeEffectKind.REVOKE);
        ArtefactRel artefactRelB = DataCreationRoutines.createArtefactRel(
                variantPrefix + "_ArtefactRelB_" + level, artefact2, localRootArtefact, ChangeEffectKind.MODIFY, ChangeEffectKind.REVOKE);
        ArtefactRel artefactRelC = DataCreationRoutines.createArtefactRel(
                variantPrefix + "_ArtefactRelC_" + level, artefact3, localRootArtefact, ChangeEffectKind.MODIFY, ChangeEffectKind.NONE);
        ArtefactRel artefactRelD = DataCreationRoutines.createArtefactRel(
                variantPrefix + "_ArtefactRelD_" + level, artefact4, localRootArtefact, ChangeEffectKind.NONE, ChangeEffectKind.REVOKE);
        ArtefactRel artefactRelE = DataCreationRoutines.createArtefactRel(
                variantPrefix + "_ArtefactRelE_" + level, artefact5, localRootArtefact, ChangeEffectKind.MODIFY, ChangeEffectKind.VALIDATE);
        ArtefactRel artefactRelF = DataCreationRoutines.createArtefactRel(
                variantPrefix + "_ArtefactRelF_" + level, artefact6, localRootArtefact, ChangeEffectKind.VALIDATE, ChangeEffectKind.REVOKE);
        
        
        ArtefactRel artefactRelG = DataCreationRoutines.createArtefactRel(
                variantPrefix + "_ArtefactRelG_" + (level +1), artefact21, artefact1, ChangeEffectKind.REVOKE, ChangeEffectKind.REVOKE);
        ArtefactRel artefactRelH = DataCreationRoutines.createArtefactRel(
                variantPrefix + "_ArtefactRelH_" + (level +1), artefact22, artefact2, ChangeEffectKind.MODIFY, ChangeEffectKind.MODIFY);
        ArtefactRel artefactRelI = DataCreationRoutines.createArtefactRel(
                variantPrefix + "_ArtefactRelI_" + (level +1), artefact23, artefact3, ChangeEffectKind.MODIFY, ChangeEffectKind.REVOKE);
        ArtefactRel artefactRelJ = DataCreationRoutines.createArtefactRel(
                variantPrefix + "_ArtefactRelJ_" + (level +1), artefact24, artefact4, ChangeEffectKind.MODIFY, ChangeEffectKind.REVOKE);
        ArtefactRel artefactRelK = DataCreationRoutines.createArtefactRel(
                variantPrefix + "_ArtefactRelK_" + (level +1), artefact25, artefact5, ChangeEffectKind.MODIFY, ChangeEffectKind.REVOKE);
        ArtefactRel artefactRelL = DataCreationRoutines.createArtefactRel(
                variantPrefix + "_ArtefactRelL_" + (level +1), artefact26, artefact6, ChangeEffectKind.MODIFY, ChangeEffectKind.REVOKE);
        
        
        resource.getContents().add(artefact1);
        resource.getContents().add(artefact2);
        resource.getContents().add(artefact3);
        resource.getContents().add(artefact4);
        resource.getContents().add(artefact5);
        resource.getContents().add(artefact6);
        resource.getContents().add(artefact21);
        resource.getContents().add(artefact22);
        resource.getContents().add(artefact23);
        resource.getContents().add(artefact24);
        resource.getContents().add(artefact25);
        resource.getContents().add(artefact26);
        
        resource.getContents().add(artefactRelA);
        resource.getContents().add(artefactRelB);
        resource.getContents().add(artefactRelC);
        resource.getContents().add(artefactRelD);
        resource.getContents().add(artefactRelE);
        resource.getContents().add(artefactRelF);
        resource.getContents().add(artefactRelG);
        resource.getContents().add(artefactRelH);
        resource.getContents().add(artefactRelI);
        resource.getContents().add(artefactRelJ);
        resource.getContents().add(artefactRelK);
        resource.getContents().add(artefactRelL);
        
        
        createResursively(resource, level + 2, artefact21, maxLevel, variantPrefix);
        createResursively(resource, level + 2, artefact22, maxLevel, variantPrefix);
        createResursively(resource, level + 2, artefact23, maxLevel, variantPrefix);
        createResursively(resource, level + 2, artefact24, maxLevel, variantPrefix);
        createResursively(resource, level + 2, artefact25, maxLevel, variantPrefix);
        createResursively(resource, level + 2, artefact26, maxLevel, variantPrefix);
    }
    
    
    /*
     * 
     * ArtefactA --MODIFY, REVOKE--> ArtefactB --MODIFY, REVOKE--> ArtefactC (...)
     * 
     * (...) ArtefactC --MODIFY, REVOKE --> ArtefactD -- REVOKE, MODIFY --> ArtefactA (...)
     * 
     * This should cycle 2 times (ArtefactD-->ArtefactA changes impact) and then anti-cycle code
     * 
     * should jump in not allowing to move from ArtefactA to ArtefactB
     *
     */
    private static void createImpactStructureWithCycle()
    {
        new SimpleInTransactionExecutor()
        {
            @Override
            public void executeInTransaction(CDOTransaction cdoTransaction)
            {
                Resource resource = StandaloneCDOAccessor.getDefaultResource(cdoTransaction);
                
                ArtefactModel am =  EvidenceFactory.eINSTANCE.createArtefactModel();
                am.setName("ArtefactModel");
                
                ArtefactDefinition ad = EvidenceFactory.eINSTANCE.createArtefactDefinition();
                ad.setName("ArtefactDef");
                                                
                String prefix = "1_Artefact";
                
                Artefact artefactA = DataCreationRoutines.createArtefact(prefix + "A_cycle", 1);
                Artefact artefactB = DataCreationRoutines.createArtefact(prefix + "B_cycle", 2);
                Artefact artefactC = DataCreationRoutines.createArtefact(prefix + "C_cycle", 3);
                Artefact artefactD = DataCreationRoutines.createArtefact(prefix + "D_cycle", 4);
                
                /*
                 * The current semantics is "source artefact depends on target artefact", so
                 * the changes in the target may impact the source.
                 * 
                 */
                
                ArtefactRel artefactRelA = DataCreationRoutines.createArtefactRel(
                        prefix + "RelA_cycle", artefactB, artefactA, ChangeEffectKind.MODIFY, ChangeEffectKind.REVOKE);
                ArtefactRel artefactRelB = DataCreationRoutines.createArtefactRel(
                        prefix + "RelB_cycle", artefactC, artefactB, ChangeEffectKind.MODIFY, ChangeEffectKind.REVOKE);
                ArtefactRel artefactRelC = DataCreationRoutines.createArtefactRel(
                        prefix + "RelC_cycle", artefactD, artefactC, ChangeEffectKind.MODIFY, ChangeEffectKind.REVOKE);
                ArtefactRel artefactRelD = DataCreationRoutines.createArtefactRel(
                        prefix + "RelD_cycle", artefactA, artefactD, ChangeEffectKind.REVOKE, ChangeEffectKind.MODIFY);
                
                artefactB.getOwnedRel().add(artefactRelA);
                artefactC.getOwnedRel().add(artefactRelB);
                artefactD.getOwnedRel().add(artefactRelC);                
                artefactA.getOwnedRel().add(artefactRelD);
                
                ad.getArtefact().add(artefactA);
                ad.getArtefact().add(artefactB);
                ad.getArtefact().add(artefactC);
                ad.getArtefact().add(artefactD);
                
                am.getArtefact().add(ad);
                resource.getContents().add(am);
                
            }
        }.executeReadWriteOperation();
    }
}
