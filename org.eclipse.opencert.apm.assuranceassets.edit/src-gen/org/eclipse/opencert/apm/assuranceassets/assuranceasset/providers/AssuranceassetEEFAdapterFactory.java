/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assuranceassets.assuranceasset.providers;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.util.AssuranceassetAdapterFactory;

/**
 * 
 * 
 */
public class AssuranceassetEEFAdapterFactory extends AssuranceassetAdapterFactory {

	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.util.AssuranceassetAdapterFactory#createAssuranceAssetsModelAdapter()
	 * 
	 */
	public Adapter createAssuranceAssetsModelAdapter() {
		return new AssuranceAssetsModelPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.util.AssuranceassetAdapterFactory#createManageableAssuranceAssetAdapter()
	 * 
	 */
	public Adapter createManageableAssuranceAssetAdapter() {
		return new ManageableAssuranceAssetPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.util.AssuranceassetAdapterFactory#createAssuranceAssetEventAdapter()
	 * 
	 */
	public Adapter createAssuranceAssetEventAdapter() {
		return new AssuranceAssetEventPropertiesEditionProvider();
	}
	/**
	 * {@inheritDoc}
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.util.AssuranceassetAdapterFactory#createAssuranceAssetEvaluationAdapter()
	 * 
	 */
	public Adapter createAssuranceAssetEvaluationAdapter() {
		return new AssuranceAssetEvaluationPropertiesEditionProvider();
	}

}
