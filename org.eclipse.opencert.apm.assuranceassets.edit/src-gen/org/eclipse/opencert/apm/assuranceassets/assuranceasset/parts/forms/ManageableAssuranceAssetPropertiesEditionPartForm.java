/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 * Generated with Acceleo
 */
package org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.forms;

// Start of user code for imports
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.eef.runtime.api.component.IPropertiesEditionComponent;
import org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart;
import org.eclipse.emf.eef.runtime.impl.notify.PropertiesEditionEvent;
import org.eclipse.emf.eef.runtime.part.impl.SectionPropertiesEditingPart;
import org.eclipse.emf.eef.runtime.ui.parts.PartComposer;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.BindingCompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionSequence;
import org.eclipse.emf.eef.runtime.ui.parts.sequence.CompositionStep;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable;
import org.eclipse.emf.eef.runtime.ui.widgets.ReferencesTable.ReferencesTableListener;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableContentProvider;
import org.eclipse.emf.eef.runtime.ui.widgets.referencestable.ReferencesTableSettings;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.AssuranceassetViewsRepository;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.ManageableAssuranceAssetPropertiesEditionPart;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.providers.AssuranceassetMessages;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

// End of user code

/**
 * 
 * 
 */
public class ManageableAssuranceAssetPropertiesEditionPartForm extends SectionPropertiesEditingPart implements IFormPropertiesEditionPart, ManageableAssuranceAssetPropertiesEditionPart {

	protected ReferencesTable evaluation;
	protected List<ViewerFilter> evaluationBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> evaluationFilters = new ArrayList<ViewerFilter>();
	protected ReferencesTable lifecycleEvent;
	protected List<ViewerFilter> lifecycleEventBusinessFilters = new ArrayList<ViewerFilter>();
	protected List<ViewerFilter> lifecycleEventFilters = new ArrayList<ViewerFilter>();



	/**
	 * For {@link ISection} use only.
	 */
	public ManageableAssuranceAssetPropertiesEditionPartForm() { super(); }

	/**
	 * Default constructor
	 * @param editionComponent the {@link IPropertiesEditionComponent} that manage this part
	 * 
	 */
	public ManageableAssuranceAssetPropertiesEditionPartForm(IPropertiesEditionComponent editionComponent) {
		super(editionComponent);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createFigure(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
	 * 
	 */
	public Composite createFigure(final Composite parent, final FormToolkit widgetFactory) {
		ScrolledForm scrolledForm = widgetFactory.createScrolledForm(parent);
		Form form = scrolledForm.getForm();
		view = form.getBody();
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		view.setLayout(layout);
		createControls(widgetFactory, view);
		return scrolledForm;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.parts.IFormPropertiesEditionPart#
	 *  createControls(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 * 
	 */
	public void createControls(final FormToolkit widgetFactory, Composite view) {
		CompositionSequence manageableAssuranceAssetStep = new BindingCompositionSequence(propertiesEditionComponent);
		CompositionStep propertiesStep = manageableAssuranceAssetStep.addStep(AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.class);
		propertiesStep.addStep(AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.evaluation);
		propertiesStep.addStep(AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.lifecycleEvent);
		
		
		composer = new PartComposer(manageableAssuranceAssetStep) {

			@Override
			public Composite addToPart(Composite parent, Object key) {
				if (key == AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.class) {
					return createPropertiesGroup(widgetFactory, parent);
				}
				if (key == AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.evaluation) {
					return createEvaluationTableComposition(widgetFactory, parent);
				}
				if (key == AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.lifecycleEvent) {
					return createLifecycleEventTableComposition(widgetFactory, parent);
				}
				return parent;
			}
		};
		composer.compose(view);
	}
	/**
	 * 
	 */
	protected Composite createPropertiesGroup(FormToolkit widgetFactory, final Composite parent) {
		Section propertiesSection = widgetFactory.createSection(parent, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		propertiesSection.setText(AssuranceassetMessages.ManageableAssuranceAssetPropertiesEditionPart_PropertiesGroupLabel);
		GridData propertiesSectionData = new GridData(GridData.FILL_HORIZONTAL);
		propertiesSectionData.horizontalSpan = 3;
		propertiesSection.setLayoutData(propertiesSectionData);
		Composite propertiesGroup = widgetFactory.createComposite(propertiesSection);
		GridLayout propertiesGroupLayout = new GridLayout();
		propertiesGroupLayout.numColumns = 3;
		propertiesGroup.setLayout(propertiesGroupLayout);
		propertiesSection.setClient(propertiesGroup);
		return propertiesGroup;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createEvaluationTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.evaluation = new ReferencesTable(getDescription(AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.evaluation, AssuranceassetMessages.ManageableAssuranceAssetPropertiesEditionPart_EvaluationLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ManageableAssuranceAssetPropertiesEditionPartForm.this, AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.evaluation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				evaluation.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ManageableAssuranceAssetPropertiesEditionPartForm.this, AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.evaluation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				evaluation.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ManageableAssuranceAssetPropertiesEditionPartForm.this, AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.evaluation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				evaluation.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ManageableAssuranceAssetPropertiesEditionPartForm.this, AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.evaluation, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				evaluation.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.evaluationFilters) {
			this.evaluation.addFilter(filter);
		}
		this.evaluation.setHelpText(propertiesEditionComponent.getHelpContent(AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.evaluation, AssuranceassetViewsRepository.FORM_KIND));
		this.evaluation.createControls(parent, widgetFactory);
		this.evaluation.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ManageableAssuranceAssetPropertiesEditionPartForm.this, AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.evaluation, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData evaluationData = new GridData(GridData.FILL_HORIZONTAL);
		evaluationData.horizontalSpan = 3;
		this.evaluation.setLayoutData(evaluationData);
		this.evaluation.setLowerBound(0);
		this.evaluation.setUpperBound(-1);
		evaluation.setID(AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.evaluation);
		evaluation.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createEvaluationTableComposition

		// End of user code
		return parent;
	}

	/**
	 * @param container
	 * 
	 */
	protected Composite createLifecycleEventTableComposition(FormToolkit widgetFactory, Composite parent) {
		this.lifecycleEvent = new ReferencesTable(getDescription(AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.lifecycleEvent, AssuranceassetMessages.ManageableAssuranceAssetPropertiesEditionPart_LifecycleEventLabel), new ReferencesTableListener() {
			public void handleAdd() {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ManageableAssuranceAssetPropertiesEditionPartForm.this, AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.ADD, null, null));
				lifecycleEvent.refresh();
			}
			public void handleEdit(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ManageableAssuranceAssetPropertiesEditionPartForm.this, AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.EDIT, null, element));
				lifecycleEvent.refresh();
			}
			public void handleMove(EObject element, int oldIndex, int newIndex) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ManageableAssuranceAssetPropertiesEditionPartForm.this, AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.MOVE, element, newIndex));
				lifecycleEvent.refresh();
			}
			public void handleRemove(EObject element) {
				propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ManageableAssuranceAssetPropertiesEditionPartForm.this, AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.lifecycleEvent, PropertiesEditionEvent.COMMIT, PropertiesEditionEvent.REMOVE, null, element));
				lifecycleEvent.refresh();
			}
			public void navigateTo(EObject element) { }
		});
		for (ViewerFilter filter : this.lifecycleEventFilters) {
			this.lifecycleEvent.addFilter(filter);
		}
		this.lifecycleEvent.setHelpText(propertiesEditionComponent.getHelpContent(AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.lifecycleEvent, AssuranceassetViewsRepository.FORM_KIND));
		this.lifecycleEvent.createControls(parent, widgetFactory);
		this.lifecycleEvent.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				if (e.item != null && e.item.getData() instanceof EObject) {
					propertiesEditionComponent.firePropertiesChanged(new PropertiesEditionEvent(ManageableAssuranceAssetPropertiesEditionPartForm.this, AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.lifecycleEvent, PropertiesEditionEvent.CHANGE, PropertiesEditionEvent.SELECTION_CHANGED, null, e.item.getData()));
				}
			}
			
		});
		GridData lifecycleEventData = new GridData(GridData.FILL_HORIZONTAL);
		lifecycleEventData.horizontalSpan = 3;
		this.lifecycleEvent.setLayoutData(lifecycleEventData);
		this.lifecycleEvent.setLowerBound(0);
		this.lifecycleEvent.setUpperBound(-1);
		lifecycleEvent.setID(AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.lifecycleEvent);
		lifecycleEvent.setEEFType("eef::AdvancedTableComposition"); //$NON-NLS-1$
		// Start of user code for createLifecycleEventTableComposition

		// End of user code
		return parent;
	}


	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionListener#firePropertiesChanged(org.eclipse.emf.eef.runtime.api.notify.IPropertiesEditionEvent)
	 * 
	 */
	public void firePropertiesChanged(IPropertiesEditionEvent event) {
		// Start of user code for tab synchronization
		
		// End of user code
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.ManageableAssuranceAssetPropertiesEditionPart#initEvaluation(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initEvaluation(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		evaluation.setContentProvider(contentProvider);
		evaluation.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.evaluation);
		if (eefElementEditorReadOnlyState && evaluation.isEnabled()) {
			evaluation.setEnabled(false);
			evaluation.setToolTipText(AssuranceassetMessages.ManageableAssuranceAsset_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !evaluation.isEnabled()) {
			evaluation.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.ManageableAssuranceAssetPropertiesEditionPart#updateEvaluation()
	 * 
	 */
	public void updateEvaluation() {
	evaluation.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.ManageableAssuranceAssetPropertiesEditionPart#addFilterEvaluation(ViewerFilter filter)
	 * 
	 */
	public void addFilterToEvaluation(ViewerFilter filter) {
		evaluationFilters.add(filter);
		if (this.evaluation != null) {
			this.evaluation.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.ManageableAssuranceAssetPropertiesEditionPart#addBusinessFilterEvaluation(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToEvaluation(ViewerFilter filter) {
		evaluationBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.ManageableAssuranceAssetPropertiesEditionPart#isContainedInEvaluationTable(EObject element)
	 * 
	 */
	public boolean isContainedInEvaluationTable(EObject element) {
		return ((ReferencesTableSettings)evaluation.getInput()).contains(element);
	}



	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.ManageableAssuranceAssetPropertiesEditionPart#initLifecycleEvent(EObject current, EReference containingFeature, EReference feature)
	 */
	public void initLifecycleEvent(ReferencesTableSettings settings) {
		if (current.eResource() != null && current.eResource().getResourceSet() != null)
			this.resourceSet = current.eResource().getResourceSet();
		ReferencesTableContentProvider contentProvider = new ReferencesTableContentProvider();
		lifecycleEvent.setContentProvider(contentProvider);
		lifecycleEvent.setInput(settings);
		boolean eefElementEditorReadOnlyState = isReadOnly(AssuranceassetViewsRepository.ManageableAssuranceAsset.Properties.lifecycleEvent);
		if (eefElementEditorReadOnlyState && lifecycleEvent.isEnabled()) {
			lifecycleEvent.setEnabled(false);
			lifecycleEvent.setToolTipText(AssuranceassetMessages.ManageableAssuranceAsset_ReadOnly);
		} else if (!eefElementEditorReadOnlyState && !lifecycleEvent.isEnabled()) {
			lifecycleEvent.setEnabled(true);
		}	
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.ManageableAssuranceAssetPropertiesEditionPart#updateLifecycleEvent()
	 * 
	 */
	public void updateLifecycleEvent() {
	lifecycleEvent.refresh();
}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.ManageableAssuranceAssetPropertiesEditionPart#addFilterLifecycleEvent(ViewerFilter filter)
	 * 
	 */
	public void addFilterToLifecycleEvent(ViewerFilter filter) {
		lifecycleEventFilters.add(filter);
		if (this.lifecycleEvent != null) {
			this.lifecycleEvent.addFilter(filter);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.ManageableAssuranceAssetPropertiesEditionPart#addBusinessFilterLifecycleEvent(ViewerFilter filter)
	 * 
	 */
	public void addBusinessFilterToLifecycleEvent(ViewerFilter filter) {
		lifecycleEventBusinessFilters.add(filter);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.opencert.apm.assuranceassets.assuranceasset.parts.ManageableAssuranceAssetPropertiesEditionPart#isContainedInLifecycleEventTable(EObject element)
	 * 
	 */
	public boolean isContainedInLifecycleEventTable(EObject element) {
		return ((ReferencesTableSettings)lifecycleEvent.getInput()).contains(element);
	}






	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.eef.runtime.api.parts.IPropertiesEditionPart#getTitle()
	 * 
	 */
	public String getTitle() {
		return AssuranceassetMessages.ManageableAssuranceAsset_Part_Title;
	}

	// Start of user code additional methods
	
	// End of user code


}
