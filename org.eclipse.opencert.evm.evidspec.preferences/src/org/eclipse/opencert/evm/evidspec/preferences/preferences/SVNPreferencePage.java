/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.evm.evidspec.preferences.preferences;

import org.eclipse.jface.preference.*;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.opencert.evm.evidspec.preferences.Activator;

/**
 * This class represents a preference page that
 * is contributed to the Preferences dialog. By 
 * subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows
 * us to create a page that is small and knows how to 
 * save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They
 * are stored in the preference store that belongs to
 * the main plug-in class. That way, preferences can
 * be accessed directly via the preference store.
 */

public class SVNPreferencePage
	extends FieldEditorPreferencePage
	implements IWorkbenchPreferencePage {

	public SVNPreferencePage() {
		super(GRID);
		setPreferenceStore(PlatformUI.getPreferenceStore());
		setDescription("Preferences for the SVN to manage Artefacts");
	}

	/**
	 * Creates the field editors. Field editors are abstractions of
	 * the common GUI blocks needed to manipulate various types
	 * of preferences. Each field editor knows how to save and
	 * restore itself.
	 */
	public void createFieldEditors() {
		addField(new BooleanFieldEditor(PreferenceConstants.SVNInfo_REPOSITORY_TYPE, 
				"&Use Local Repository:", getFieldEditorParent()));
		addField(new DirectoryFieldEditor(PreferenceConstants.SVNInfo_LOCAL_REPOSITORY_URL, 
				"&SVN Local Repository Location:", getFieldEditorParent()));
		addField(new StringFieldEditor(PreferenceConstants.SVNInfo_REMOTE_REPOSITORY_URL, 
				"&SVN Remote Repository Location:", getFieldEditorParent()));
		addField(new StringFieldEditor(PreferenceConstants.SVNInfo_USER, 
				"&SVN Repository User:", getFieldEditorParent()));
		addField(new PasswordFieldEditor(PreferenceConstants.SVNInfo_PASS, 
				"&SVN Repository Password:", getFieldEditorParent()));
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
	}
	
}