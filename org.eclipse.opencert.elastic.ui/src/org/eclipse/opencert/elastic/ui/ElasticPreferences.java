/*******************************************************************************
 * Copyright (C) 2017 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic.ui;

/**
 * Bundle preferences.
 */
public interface ElasticPreferences {

	/**
	 * Property for server address to elastic server.
	 */
	String SERVER_ADDRESS = "server.address"; //$NON-NLS-1$

	/**
	 * Property for elastic index name.
	 */
	String INDEX_NAME = "index.name"; //$NON-NLS-1$

}
