/*******************************************************************************
 * Copyright (C) 2018 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic.ui;

import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbenchPart;

/**
 * We use this observer to spy on any active part (editor or view).
 */
public class ElasticPartObserver implements IPartListener {

	@Override
	public void partActivated(IWorkbenchPart part) {
		// TODO Handle
	}

	@Override
	public void partBroughtToTop(IWorkbenchPart part) {
		// TODO Handle
	}

	@Override
	public void partClosed(IWorkbenchPart part) {
		// TODO Handle
	}

	@Override
	public void partDeactivated(IWorkbenchPart part) {
		// TODO Handle
	}

	@Override
	public void partOpened(IWorkbenchPart part) {
		// TODO Handle
	}
}
