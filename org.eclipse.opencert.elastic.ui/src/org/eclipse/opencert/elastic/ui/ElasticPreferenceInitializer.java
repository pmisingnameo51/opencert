/*******************************************************************************
 * Copyright (C) 2017 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic.ui;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

/**
 * Bundle preference initializer, no specials.
 */
public class ElasticPreferenceInitializer extends AbstractPreferenceInitializer implements ElasticPreferences {

	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore node = Activator.getDefault().getPreferenceStore();
		node.setDefault(SERVER_ADDRESS, "http://localhost:9200"); //$NON-NLS-1$
		node.setDefault(INDEX_NAME, "amass"); //$NON-NLS-1$
	}

}
