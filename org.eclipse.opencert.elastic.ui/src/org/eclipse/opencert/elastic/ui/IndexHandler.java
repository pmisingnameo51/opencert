/*******************************************************************************
 * Copyright (C) 2017 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic.ui;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.opencert.elastic.ElasticClient;
import org.eclipse.opencert.elastic.ElasticClientImpl;
import org.eclipse.opencert.elastic.ElasticConsole;
import org.eclipse.opencert.elastic.ElasticIndexer;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * A generic handler to index the selected object, its containment tree or its
 * resource. All data currently hard coded, no preferences page or other
 * configuration.
 * 
 * @author mauersberger
 */
public class IndexHandler extends AbstractHandler {

	/*
	 * We use this console.
	 */
	ElasticConsole console = new ElasticConsoleImpl();

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection currentSelection = HandlerUtil.getCurrentSelection(event);
		if (currentSelection.isEmpty()) {
			HandlerUtil.getActiveShell(event).getDisplay().beep();
			return null;
		}

		IPreferenceStore node = Activator.getDefault().getPreferenceStore();
		URL url = null;
		try {
			String spec = node.getString(ElasticPreferences.SERVER_ADDRESS);
			url = new URL(spec);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			return null;
		}

		String indexName = node.getString(ElasticPreferences.INDEX_NAME);
		ElasticClient client = ElasticClientImpl.on(url.getHost(), url.getPort(), url.getProtocol());
		ElasticIndexer indexer = ElasticIndexer.on(client).onIndex(indexName).using(console);

		// scope as command parameter
		Object firstElement = ((StructuredSelection) currentSelection).getFirstElement();
		if (!(firstElement instanceof EObject) && (firstElement instanceof IAdaptable)) {
			firstElement = ((IAdaptable) firstElement).getAdapter(EObject.class);
		}
		// don't index anything we cannot handle
		if (firstElement == null) {
			HandlerUtil.getActiveShell(event).getDisplay().beep();
			return null;
		}

		Object scope = event.getParameter("scope"); //$NON-NLS-1$
		if ("object".equals(scope)) { //$NON-NLS-1$
			indexer.indexObject(firstElement);
		} else if ("tree".equals(scope)) { //$NON-NLS-1$
			indexer.indexTree(firstElement);
		} else if ("resource".equals(scope)) { //$NON-NLS-1$
			indexer.indexResource(firstElement);
		} else {
			HandlerUtil.getActiveShell(event).getDisplay().beep();
		}

		return null;
	}

}
