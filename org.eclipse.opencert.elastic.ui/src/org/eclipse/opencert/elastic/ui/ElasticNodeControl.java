/*******************************************************************************
 * Copyright (C) 2017 ANSYS medini Technologies AG
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 	ANSYS medini Technologies AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.opencert.elastic.ui;

import java.net.URL;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.opencert.elastic.ElasticClient;
import org.eclipse.opencert.elastic.ElasticClientImpl;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.ui.menus.WorkbenchWindowControlContribution;
import org.eclipse.ui.progress.UIJob;

@SuppressWarnings({ "nls", "javadoc" })
public class ElasticNodeControl extends WorkbenchWindowControlContribution {

	// job to update the controls
	UIJob updater = null;

	enum ElasticStatus {
		UNKNOWN,
		NOT_CONFIGURED,
		NOT_REACHABLE,
		REACHABLE
	}

	ElasticStatus serverStatus = ElasticStatus.UNKNOWN, indexStatus = ElasticStatus.UNKNOWN;

	Image unstable, stable, disabled;

	String serverHint, indexHint;

	private CLabel serverStatusLabel, indexStatusLabel;

	@Override
	protected Control createControl(Composite parent) {
		Composite control = new Composite(parent, SWT.NONE);
		Layout layout = new FillLayout();
		control.setLayout(layout);

		// load images (we have a display)
		stable = Activator.getDefault().getImage("/icons/passed-status.png");
		unstable = Activator.getDefault().getImage("/icons/unstable-status.png");
		disabled = Activator.getDefault().getImage("/icons/disabled-status.png");

		CLabel elasticLabel = new CLabel(control, SWT.NONE);
		elasticLabel.setImage(Activator.getDefault().getImage("/icons/etools16/elastic.png"));
		elasticLabel.setToolTipText("This is the Elasticsearch toolbar");

		serverStatusLabel = new CLabel(control, SWT.NONE);
		serverStatusLabel.setImage(disabled);
		serverStatusLabel.setToolTipText(serverHint);
		indexStatusLabel = new CLabel(control, SWT.NONE);
		indexStatusLabel.setImage(disabled);
		indexStatusLabel.setToolTipText(indexHint);

		control.pack();
		// enforce at least 16px height
		Point size = control.getSize();
		if (size.y < 16) {
			size.y = 16;
			control.setSize(size);
		}

		updater = new UIJob("Update Elastic Controls") { //$NON-NLS-1$

			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				updateLabels();
				return Status.OK_STATUS;
			}
		};

		Job job = new Job("Check Elastic Server") { //$NON-NLS-1$
			@Override
			public IStatus run(IProgressMonitor monitor) {
				IPreferenceStore node = Activator.getDefault().getPreferenceStore();
				String server = node.getString(ElasticPreferences.SERVER_ADDRESS);
				if (server == null || server.trim().isEmpty()) {
					serverStatus = ElasticStatus.NOT_CONFIGURED;
					serverHint = "No server configured. Do it in the preferences";
					indexStatus = ElasticStatus.NOT_CONFIGURED;
					indexHint = "No server configured. Do it in the preferences";
				} else {
					pingServer(node, server);
				}

				updater.schedule();
				return Status.OK_STATUS;
			}

			private void pingServer(IPreferenceStore node, String server) {
				try {
					URL url = new URL(server);
					ElasticClient client = ElasticClientImpl.on(url.getHost(), url.getPort(), url.getProtocol());
					String version = client.ping().version();
					serverStatus = ElasticStatus.REACHABLE;
					serverHint = "Server reachable, detected v" + version;

					try {
						String index = node.getString(ElasticPreferences.INDEX_NAME);
						if (index == null || index.trim().isEmpty()) {
							indexStatus = ElasticStatus.NOT_CONFIGURED;
							indexHint = "No index configured";
						} else {
							String status = client.indexStatus(index);
							indexStatus = ElasticStatus.REACHABLE;
							indexHint = "Index reachable: " + status;
						}
					} catch (Exception exception) {
						exception.printStackTrace();
						indexStatus = ElasticStatus.NOT_REACHABLE;
						indexHint = "Index not reachable";
					}

				} catch (Throwable e) {
					serverStatus = ElasticStatus.NOT_REACHABLE;
					serverHint = "Server not reachable: " + e;
					indexStatus = ElasticStatus.NOT_REACHABLE;
					indexHint = "Index not reachable";
				}
			}
		};
		job.schedule(2500);

		// register for preference changes
		IPreferenceStore node = Activator.getDefault().getPreferenceStore();
		node.addPropertyChangeListener(new IPropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent event) {
				// we don't care WHAT has changed, we just refresh
				reset();
				updater.schedule();
				job.schedule(500);
			}
		});

		// register for explicit user update by double click
		serverStatusLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				reset();
				updater.schedule();
				job.schedule(500);
			}
		});
		indexStatusLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				reset();
				updater.schedule();
				job.schedule(500);
			}
		});

		return control;
	}

	private void reset() {
		serverStatus = ElasticStatus.UNKNOWN;
		serverHint = "Server status is being obtained...";
		indexStatus = ElasticStatus.UNKNOWN;
		indexHint = "Index status is being obtained...";
	}

	/*
	 * Update the view. Assumed to be executed in the UI thread.
	 */
	private void updateLabels() {
		if (!serverStatusLabel.isDisposed()) {
			switch (serverStatus) {
				case REACHABLE:
					serverStatusLabel.setImage(stable);
					break;
				case NOT_REACHABLE:
					serverStatusLabel.setImage(unstable);
					break;
				default:
					serverStatusLabel.setImage(disabled);
					break;
			}
			serverStatusLabel.setToolTipText(serverHint);
		}
		if (!indexStatusLabel.isDisposed()) {
			switch (indexStatus) {
				case REACHABLE:
					indexStatusLabel.setImage(stable);
					break;
				case NOT_REACHABLE:
					indexStatusLabel.setImage(unstable);
					break;
				default:
					indexStatusLabel.setImage(disabled);
					break;
			}
			indexStatusLabel.setToolTipText(indexHint);
		}
	}
}
