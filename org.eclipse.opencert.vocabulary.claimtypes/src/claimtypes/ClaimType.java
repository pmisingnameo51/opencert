/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/**
 */
package claimtypes;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.infra.general.general.DescribableElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Claim Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link claimtypes.ClaimType#getSentenceStructures <em>Sentence Structures</em>}</li>
 *   <li>{@link claimtypes.ClaimType#getColor <em>Color</em>}</li>
 * </ul>
 * </p>
 *
 * @see claimtypes.ClaimtypesPackage#getClaimType()
 * @model
 * @generated
 */
public interface ClaimType extends DescribableElement {
	/**
	 * Returns the value of the '<em><b>Sentence Structures</b></em>' containment reference list.
	 * The list contents are of type {@link claimtypes.SentenceStructure}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sentence Structures</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sentence Structures</em>' containment reference list.
	 * @see claimtypes.ClaimtypesPackage#getClaimType_SentenceStructures()
	 * @model containment="true"
	 * @generated
	 */
	EList<SentenceStructure> getSentenceStructures();

	/**
	 * Returns the value of the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Color</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Color</em>' attribute.
	 * @see #setColor(String)
	 * @see claimtypes.ClaimtypesPackage#getClaimType_Color()
	 * @model
	 * @generated
	 */
	String getColor();

	/**
	 * Sets the value of the '{@link claimtypes.ClaimType#getColor <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Color</em>' attribute.
	 * @see #getColor()
	 * @generated
	 */
	void setColor(String value);

} // ClaimType
