/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.vocabulary.diagram.providers.assistants;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.opencert.vocabulary.diagram.providers.VocabularyElementTypes;
import org.eclipse.opencert.vocabulary.diagram.providers.VocabularyModelingAssistantProvider;

/**
 * @generated
 */
public class VocabularyModelingAssistantProviderOfVocabularyEditPart extends
		VocabularyModelingAssistantProvider {

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForPopupBar(IAdaptable host) {
		List<IElementType> types = new ArrayList<IElementType>(3);
		types.add(VocabularyElementTypes.Term_2001);
		types.add(VocabularyElementTypes.Category_2002);
		types.add(VocabularyElementTypes.SourceOfDefinition_2003);
		return types;
	}

}
