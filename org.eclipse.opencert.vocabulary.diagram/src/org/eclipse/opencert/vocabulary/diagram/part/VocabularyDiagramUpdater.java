/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.vocabulary.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.update.DiagramUpdater;
import org.eclipse.opencert.vocabulary.Category;
import org.eclipse.opencert.vocabulary.SourceOfDefinition;
import org.eclipse.opencert.vocabulary.Term;
import org.eclipse.opencert.vocabulary.Vocabulary;
import org.eclipse.opencert.vocabulary.VocabularyPackage;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.CategoryEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.CategorySubCategoriesEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.CategoryTermsEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.SourceOfDefinitionEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermDefinedByEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermHasAEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermIsAEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.TermRefersToEditPart;
import org.eclipse.opencert.vocabulary.diagram.edit.parts.VocabularyEditPart;
import org.eclipse.opencert.vocabulary.diagram.providers.VocabularyElementTypes;

/**
 * @generated
 */
public class VocabularyDiagramUpdater {

	/**
	 * @generated
	 */
	public static boolean isShortcutOrphaned(View view) {
		return !view.isSetElement() || view.getElement() == null
				|| view.getElement().eIsProxy();
	}

	/**
	 * @generated
	 */
	public static List<VocabularyNodeDescriptor> getSemanticChildren(View view) {
		switch (VocabularyVisualIDRegistry.getVisualID(view)) {
		case VocabularyEditPart.VISUAL_ID:
			return getVocabulary_1000SemanticChildren(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<VocabularyNodeDescriptor> getVocabulary_1000SemanticChildren(
			View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		Vocabulary modelElement = (Vocabulary) view.getElement();
		LinkedList<VocabularyNodeDescriptor> result = new LinkedList<VocabularyNodeDescriptor>();
		for (Iterator<?> it = modelElement.getTerms().iterator(); it.hasNext();) {
			Term childElement = (Term) it.next();
			int visualID = VocabularyVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == TermEditPart.VISUAL_ID) {
				result.add(new VocabularyNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator<?> it = modelElement.getCategories().iterator(); it
				.hasNext();) {
			Category childElement = (Category) it.next();
			int visualID = VocabularyVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == CategoryEditPart.VISUAL_ID) {
				result.add(new VocabularyNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator<?> it = modelElement.getSourcesOfDefinition().iterator(); it
				.hasNext();) {
			SourceOfDefinition childElement = (SourceOfDefinition) it.next();
			int visualID = VocabularyVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == SourceOfDefinitionEditPart.VISUAL_ID) {
				result.add(new VocabularyNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<VocabularyLinkDescriptor> getContainedLinks(View view) {
		switch (VocabularyVisualIDRegistry.getVisualID(view)) {
		case VocabularyEditPart.VISUAL_ID:
			return getVocabulary_1000ContainedLinks(view);
		case TermEditPart.VISUAL_ID:
			return getTerm_2001ContainedLinks(view);
		case CategoryEditPart.VISUAL_ID:
			return getCategory_2002ContainedLinks(view);
		case SourceOfDefinitionEditPart.VISUAL_ID:
			return getSourceOfDefinition_2003ContainedLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<VocabularyLinkDescriptor> getIncomingLinks(View view) {
		switch (VocabularyVisualIDRegistry.getVisualID(view)) {
		case TermEditPart.VISUAL_ID:
			return getTerm_2001IncomingLinks(view);
		case CategoryEditPart.VISUAL_ID:
			return getCategory_2002IncomingLinks(view);
		case SourceOfDefinitionEditPart.VISUAL_ID:
			return getSourceOfDefinition_2003IncomingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<VocabularyLinkDescriptor> getOutgoingLinks(View view) {
		switch (VocabularyVisualIDRegistry.getVisualID(view)) {
		case TermEditPart.VISUAL_ID:
			return getTerm_2001OutgoingLinks(view);
		case CategoryEditPart.VISUAL_ID:
			return getCategory_2002OutgoingLinks(view);
		case SourceOfDefinitionEditPart.VISUAL_ID:
			return getSourceOfDefinition_2003OutgoingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<VocabularyLinkDescriptor> getVocabulary_1000ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<VocabularyLinkDescriptor> getTerm_2001ContainedLinks(
			View view) {
		Term modelElement = (Term) view.getElement();
		LinkedList<VocabularyLinkDescriptor> result = new LinkedList<VocabularyLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_Term_DefinedBy_4003(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_Term_IsA_4004(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_Term_HasA_4005(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_Term_RefersTo_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<VocabularyLinkDescriptor> getCategory_2002ContainedLinks(
			View view) {
		Category modelElement = (Category) view.getElement();
		LinkedList<VocabularyLinkDescriptor> result = new LinkedList<VocabularyLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_Category_Terms_4001(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_Category_SubCategories_4002(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<VocabularyLinkDescriptor> getSourceOfDefinition_2003ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<VocabularyLinkDescriptor> getTerm_2001IncomingLinks(
			View view) {
		Term modelElement = (Term) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<VocabularyLinkDescriptor> result = new LinkedList<VocabularyLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_Category_Terms_4001(
				modelElement, crossReferences));
		result.addAll(getIncomingFeatureModelFacetLinks_Term_IsA_4004(
				modelElement, crossReferences));
		result.addAll(getIncomingFeatureModelFacetLinks_Term_HasA_4005(
				modelElement, crossReferences));
		result.addAll(getIncomingFeatureModelFacetLinks_Term_RefersTo_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<VocabularyLinkDescriptor> getCategory_2002IncomingLinks(
			View view) {
		Category modelElement = (Category) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<VocabularyLinkDescriptor> result = new LinkedList<VocabularyLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_Category_SubCategories_4002(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<VocabularyLinkDescriptor> getSourceOfDefinition_2003IncomingLinks(
			View view) {
		SourceOfDefinition modelElement = (SourceOfDefinition) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<VocabularyLinkDescriptor> result = new LinkedList<VocabularyLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_Term_DefinedBy_4003(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<VocabularyLinkDescriptor> getTerm_2001OutgoingLinks(
			View view) {
		Term modelElement = (Term) view.getElement();
		LinkedList<VocabularyLinkDescriptor> result = new LinkedList<VocabularyLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_Term_DefinedBy_4003(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_Term_IsA_4004(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_Term_HasA_4005(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_Term_RefersTo_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<VocabularyLinkDescriptor> getCategory_2002OutgoingLinks(
			View view) {
		Category modelElement = (Category) view.getElement();
		LinkedList<VocabularyLinkDescriptor> result = new LinkedList<VocabularyLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_Category_Terms_4001(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_Category_SubCategories_4002(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<VocabularyLinkDescriptor> getSourceOfDefinition_2003OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	private static Collection<VocabularyLinkDescriptor> getIncomingFeatureModelFacetLinks_Category_Terms_4001(
			Term target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<VocabularyLinkDescriptor> result = new LinkedList<VocabularyLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == VocabularyPackage.eINSTANCE
					.getCategory_Terms()) {
				result.add(new VocabularyLinkDescriptor(setting.getEObject(),
						target, VocabularyElementTypes.CategoryTerms_4001,
						CategoryTermsEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<VocabularyLinkDescriptor> getIncomingFeatureModelFacetLinks_Category_SubCategories_4002(
			Category target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<VocabularyLinkDescriptor> result = new LinkedList<VocabularyLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == VocabularyPackage.eINSTANCE
					.getCategory_SubCategories()) {
				result.add(new VocabularyLinkDescriptor(setting.getEObject(),
						target,
						VocabularyElementTypes.CategorySubCategories_4002,
						CategorySubCategoriesEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<VocabularyLinkDescriptor> getIncomingFeatureModelFacetLinks_Term_DefinedBy_4003(
			SourceOfDefinition target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<VocabularyLinkDescriptor> result = new LinkedList<VocabularyLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == VocabularyPackage.eINSTANCE
					.getTerm_DefinedBy()) {
				result.add(new VocabularyLinkDescriptor(setting.getEObject(),
						target, VocabularyElementTypes.TermDefinedBy_4003,
						TermDefinedByEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<VocabularyLinkDescriptor> getIncomingFeatureModelFacetLinks_Term_IsA_4004(
			Term target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<VocabularyLinkDescriptor> result = new LinkedList<VocabularyLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == VocabularyPackage.eINSTANCE
					.getTerm_IsA()) {
				result.add(new VocabularyLinkDescriptor(setting.getEObject(),
						target, VocabularyElementTypes.TermIsA_4004,
						TermIsAEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<VocabularyLinkDescriptor> getIncomingFeatureModelFacetLinks_Term_HasA_4005(
			Term target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<VocabularyLinkDescriptor> result = new LinkedList<VocabularyLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == VocabularyPackage.eINSTANCE
					.getTerm_HasA()) {
				result.add(new VocabularyLinkDescriptor(setting.getEObject(),
						target, VocabularyElementTypes.TermHasA_4005,
						TermHasAEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<VocabularyLinkDescriptor> getIncomingFeatureModelFacetLinks_Term_RefersTo_4006(
			Term target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<VocabularyLinkDescriptor> result = new LinkedList<VocabularyLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == VocabularyPackage.eINSTANCE
					.getTerm_RefersTo()) {
				result.add(new VocabularyLinkDescriptor(setting.getEObject(),
						target, VocabularyElementTypes.TermRefersTo_4006,
						TermRefersToEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<VocabularyLinkDescriptor> getOutgoingFeatureModelFacetLinks_Category_Terms_4001(
			Category source) {
		LinkedList<VocabularyLinkDescriptor> result = new LinkedList<VocabularyLinkDescriptor>();
		for (Iterator<?> destinations = source.getTerms().iterator(); destinations
				.hasNext();) {
			Term destination = (Term) destinations.next();
			result.add(new VocabularyLinkDescriptor(source, destination,
					VocabularyElementTypes.CategoryTerms_4001,
					CategoryTermsEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<VocabularyLinkDescriptor> getOutgoingFeatureModelFacetLinks_Category_SubCategories_4002(
			Category source) {
		LinkedList<VocabularyLinkDescriptor> result = new LinkedList<VocabularyLinkDescriptor>();
		for (Iterator<?> destinations = source.getSubCategories().iterator(); destinations
				.hasNext();) {
			Category destination = (Category) destinations.next();
			result.add(new VocabularyLinkDescriptor(source, destination,
					VocabularyElementTypes.CategorySubCategories_4002,
					CategorySubCategoriesEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<VocabularyLinkDescriptor> getOutgoingFeatureModelFacetLinks_Term_DefinedBy_4003(
			Term source) {
		LinkedList<VocabularyLinkDescriptor> result = new LinkedList<VocabularyLinkDescriptor>();
		SourceOfDefinition destination = source.getDefinedBy();
		if (destination == null) {
			return result;
		}
		result.add(new VocabularyLinkDescriptor(source, destination,
				VocabularyElementTypes.TermDefinedBy_4003,
				TermDefinedByEditPart.VISUAL_ID));
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<VocabularyLinkDescriptor> getOutgoingFeatureModelFacetLinks_Term_IsA_4004(
			Term source) {
		LinkedList<VocabularyLinkDescriptor> result = new LinkedList<VocabularyLinkDescriptor>();
		for (Iterator<?> destinations = source.getIsA().iterator(); destinations
				.hasNext();) {
			Term destination = (Term) destinations.next();
			result.add(new VocabularyLinkDescriptor(source, destination,
					VocabularyElementTypes.TermIsA_4004,
					TermIsAEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<VocabularyLinkDescriptor> getOutgoingFeatureModelFacetLinks_Term_HasA_4005(
			Term source) {
		LinkedList<VocabularyLinkDescriptor> result = new LinkedList<VocabularyLinkDescriptor>();
		for (Iterator<?> destinations = source.getHasA().iterator(); destinations
				.hasNext();) {
			Term destination = (Term) destinations.next();
			result.add(new VocabularyLinkDescriptor(source, destination,
					VocabularyElementTypes.TermHasA_4005,
					TermHasAEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<VocabularyLinkDescriptor> getOutgoingFeatureModelFacetLinks_Term_RefersTo_4006(
			Term source) {
		LinkedList<VocabularyLinkDescriptor> result = new LinkedList<VocabularyLinkDescriptor>();
		for (Iterator<?> destinations = source.getRefersTo().iterator(); destinations
				.hasNext();) {
			Term destination = (Term) destinations.next();
			result.add(new VocabularyLinkDescriptor(source, destination,
					VocabularyElementTypes.TermRefersTo_4006,
					TermRefersToEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static final DiagramUpdater TYPED_INSTANCE = new DiagramUpdater() {
		/**
		 * @generated
		 */
		@Override
		public List<VocabularyNodeDescriptor> getSemanticChildren(View view) {
			return VocabularyDiagramUpdater.getSemanticChildren(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<VocabularyLinkDescriptor> getContainedLinks(View view) {
			return VocabularyDiagramUpdater.getContainedLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<VocabularyLinkDescriptor> getIncomingLinks(View view) {
			return VocabularyDiagramUpdater.getIncomingLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<VocabularyLinkDescriptor> getOutgoingLinks(View view) {
			return VocabularyDiagramUpdater.getOutgoingLinks(view);
		}
	};

}
