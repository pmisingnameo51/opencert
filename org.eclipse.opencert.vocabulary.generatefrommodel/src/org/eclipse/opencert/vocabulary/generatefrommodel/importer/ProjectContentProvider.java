package org.eclipse.opencert.vocabulary.generatefrommodel.importer;

/*******************************************************************************
 * Copyright (c) 2016 KPIT Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jan Mauersberger- initial API and implementation
 *   Sascha Baumgart- initial API and implementation
 *******************************************************************************/

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

/**
 * Provides content for a tree viewer that shows only containers.
 */
@SuppressWarnings("all")
public class ProjectContentProvider implements ITreeContentProvider {
	public void dispose() {
	}

	public Object[] getChildren(Object element) {
		Object[] result = null;
		if (element instanceof IProject) {
			IProject project = (IProject) element;
			if (project.isOpen()) {
				try {
					result = project.members();
				} catch (CoreException e) {
					e.printStackTrace();
				}
			} else {
				result = new Object[] {};
			}
		} else if (element instanceof IFolder) {
			try {
				result = ((IFolder) element).members();
			} catch (CoreException e) {
			}
		} else if (element instanceof IFile) {
			result = new Object[] {};
		}

		return result;
	}

	/**
	 * Requires an array of IProject instances.
	 */
	public Object[] getElements(Object element) {
		return (Object[]) element;
	}

	public Object getParent(Object element) {
		Object result = null;
		if (element instanceof IResource) {
			result = ((IFolder) element).getParent();
		}

		return result;
	}

	public boolean hasChildren(Object element) {
		return getChildren(element).length > 0;
	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}
}
