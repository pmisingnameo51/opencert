/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.policies;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.commands.DuplicateEObjectsCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.commands.RefActivityCreateCommand;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.commands.RefArtefactCreateCommand;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.commands.RefRoleCreateCommand;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.providers.RefframeworkElementTypes;

/**
 * @generated
 */
public class RefFrameworkItemSemanticEditPolicy extends
		RefframeworkBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public RefFrameworkItemSemanticEditPolicy() {
		super(RefframeworkElementTypes.RefFramework_1000);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (RefframeworkElementTypes.RefActivity_2001 == req.getElementType()) {
			return getGEFWrapper(new RefActivityCreateCommand(req));
		}
		if (RefframeworkElementTypes.RefArtefact_2002 == req.getElementType()) {
			return getGEFWrapper(new RefArtefactCreateCommand(req));
		}
		if (RefframeworkElementTypes.RefRole_2003 == req.getElementType()) {
			return getGEFWrapper(new RefRoleCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getDuplicateCommand(DuplicateElementsRequest req) {
		TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost())
				.getEditingDomain();
		return getGEFWrapper(new DuplicateAnythingCommand(editingDomain, req));
	}

	/**
	 * @generated
	 */
	private static class DuplicateAnythingCommand extends
			DuplicateEObjectsCommand {

		/**
		 * @generated
		 */
		public DuplicateAnythingCommand(
				TransactionalEditingDomain editingDomain,
				DuplicateElementsRequest req) {
			super(editingDomain, req.getLabel(), req
					.getElementsToBeDuplicated(), req
					.getAllDuplicatedElementsMap());
		}

	}

}
