/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.pkm.refframework.refframework.diagram.part;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;


import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gef.Disposable;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
//import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.commands.UnexecutableCommand;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.ui.action.AbstractActionHandler;
import org.eclipse.gmf.runtime.diagram.core.preferences.PreferencesHint;
import org.eclipse.gmf.runtime.diagram.core.services.ViewService;
import org.eclipse.gmf.runtime.diagram.ui.actions.ActionIds;
import org.eclipse.gmf.runtime.diagram.ui.actions.DiagramAction;
import org.eclipse.gmf.runtime.diagram.ui.commands.CommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.l10n.DiagramUIMessages;
import org.eclipse.gmf.runtime.diagram.ui.parts.IDiagramWorkbenchPart;
import org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.EditCommandRequestWrapper;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.CompositeTransactionalCommand;
import org.eclipse.gmf.runtime.emf.core.resources.GMFResourceFactory;
import org.eclipse.gmf.runtime.emf.type.core.EditHelperContext;
import org.eclipse.gmf.runtime.emf.type.core.IClientContext;
import org.eclipse.gmf.runtime.emf.type.core.requests.AbstractEditCommandRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.HintedDiagramLinkStyle;
import org.eclipse.gmf.runtime.notation.NotationFactory;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.eclipse.gmf.runtime.notation.Style;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.impl.NodeImpl;
import org.eclipse.gmf.runtime.notation.impl.ShapeImpl;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.DND;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.WorkspaceModifyDelegatingOperation;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivity2EditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefArtefactEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefFrameworkEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefRoleEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkDiagramEditor;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkDiagramEditorPlugin;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkDiagramEditorUtil;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.part.Messages;



import org.eclipse.emf.common.util.URI;


/**
 * This class (of Action type) is used to open an associated diagram file of the Node element
 * 
 * @author M� Carmen Palacios
 */
/**
 * @generated NOT
 */
public class OpenAssociatedDiagramAction extends Action 
{
	private static int cont = 0;
	public static final String ACTION_OPEN = "OP";
	
	private static final String OPEN_REQUEST = "Open";
    private String actionId; 
    private String diagramfullFileName="";
	private EditPart editPart;
	private ISelection selection;
	
	/**
	 * @generated NOT
	 */
	public OpenAssociatedDiagramAction(String diagramFile, ISelection selection, String actionId) {
		//super(part);
		// AHORA PASO EL FICHERO COMO UN RESOURCE!!!
		//diagramfullFileName = diagramFile;
		String kk = "platform:/resource/";
		int lastInd = diagramFile.length();  
		diagramfullFileName = diagramFile.substring(kk.length(), lastInd);
		this.actionId = actionId;
		this.selection = selection;
		editPart = null;
	}


	
	
	/**
	 * @generated NOT
	 */
	public void init() {
		//super.init();
	
		if(!diagramfullFileName.contains(RefFrameworkEditPart.FILE_DIAGRAM_ID)) return;
		int beginInd = diagramfullFileName.lastIndexOf("\\"); 
		if (beginInd == -1) beginInd = diagramfullFileName.lastIndexOf("/"); 
		if (actionId.equals(ACTION_OPEN)) 
		{ 
			cont++;
			setId("openAssociatedDiagram" + cont);
			setText("Open associated diagram " + diagramfullFileName.substring(beginInd+1));
			setToolTipText("Open diagram......");

		} 
	}

	/**
	 * @generated NOT
	 */
	protected String getCommandLabel() {
		return "MCP Action"; //????
	}

	/**
	 * @generated NOT
	 */
	/*
	// OJO: No se me invoka
	public void selectionChanged(IAction action, ISelection selection)
	{
		IStructuredSelection s = (IStructuredSelection)selection;
		if(!s.isEmpty() && s.getFirstElement() instanceof RefActivityEditPart)
		{
			editPart = (RefActivityEditPart)s.getFirstElement();
		}
	}
	*/
	
	/**
	 * @generated NOT
	 */
	public void run()
	{
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
	    IWorkbenchPage page = window.getActivePage();
	    ISelection selection = page.getSelection();
		
		IStructuredSelection s = (IStructuredSelection)selection;
		s = (IStructuredSelection)this.selection;
		if(s!= null && !s.isEmpty() && (s.getFirstElement() instanceof RefActivityEditPart  || s.getFirstElement() instanceof RefActivity2EditPart || s.getFirstElement() instanceof RefArtefactEditPart || s.getFirstElement() instanceof RefRoleEditPart ))
		//if(s!= null && !s.isEmpty() && s.getFirstElement() instanceof NamedElementImpl)
		{
			editPart = (EditPart)s.getFirstElement();
		}

		if(editPart != null)
		{
			View view = (View) editPart.getModel();
			EObject element = view.getElement();
			//final String diagramName = "kk";
			TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(element);
			OpenAssociatedDiagramCommand cmd = new OpenAssociatedDiagramCommand(domain, "test associate MCP", null);
			cmd.init(view, diagramfullFileName);
			try {
			    cmd.execute(new NullProgressMonitor(),  null);
			} catch (ExecutionException ex) {
				ex.printStackTrace();
			}
		}
	}

	
	
	/**
	 * @generated NOT
	 */
	protected HintedDiagramLinkStyle getHintedDiagramLink(View view, String diagramName) {
		HintedDiagramLinkStyle diagramFacet = null;
		boolean found = false;
		for( Object element : view.getStyles())
		{
			if (true == element instanceof HintedDiagramLinkStyle)
			{
				HintedDiagramLinkStyle elementHD = (HintedDiagramLinkStyle)element;
				if(elementHD.getHint() != null && elementHD.getHint().compareTo("Sub"+diagramName) == 0)
				{
					found = true;
					diagramFacet = elementHD;
				}
			}
		}
		if(!found)
		{
			System.out.println("getHintedDiagramLink: Error HintedDiagramLink not found ()" + diagramName);
			
		}
		
		return diagramFacet;
	}
	
	
	/**
	 * @generated NOT
	 */
	public class OpenAssociatedDiagramCommand extends AbstractTransactionalCommand{
		TransactionalEditingDomain myEditingDomain;
		View view;
		String diagramfullFileName;
		
		/**
		 * @generated NOT
		 */
		public OpenAssociatedDiagramCommand (TransactionalEditingDomain domain, String label, List affectedFiles)
		{
			super(domain, label, affectedFiles);
			myEditingDomain = domain;
		}
		
		/**
		 * @generated NOT
		 */
		public void init(View view, String diagramfullFileName)
		{
			this.view = view;
			this.diagramfullFileName = diagramfullFileName;
		}
		
		/**
		 * @generated NOT
		 */
		@Override
		protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
				IAdaptable info) throws ExecutionException {
			try {
				/*
				Diagram diagram = ((HintedDiagramLinkStyle) view.getStyle(NotationPackage.eINSTANCE
						.getHintedDiagramLinkStyle())).getDiagramLink();
				*/
				HintedDiagramLinkStyle hinted = getHintedDiagramLink(view, diagramfullFileName);
				if(hinted == null)
				{
					throw new ExecutionException("Can't open diagram'");
				}
				Diagram diagram = hinted.getDiagramLink();
				if(diagram != null && diagramfullFileName.contains(diagram.getName()))
				{
					URI uri = EcoreUtil.getURI(diagram);
					Resource diagramResource ;			
					diagramResource = myEditingDomain.getResourceSet().getResource(uri.trimFragment(), false);
					if (!diagramResource.isLoaded()) {
						try {
							Map options = new HashMap(GMFResourceFactory.getDefaultLoadOptions());
							// @see 171060 
							// options.put(org.eclipse.emf.ecore.xmi.XMLResource.OPTION_RECORD_UNKNOWN_FEATURE, Boolean.TRUE);
							diagramResource.load(options);
						} catch (IOException e) {
							diagramResource.unload();
							//throw e;
							throw new ExecutionException("Can't load diagram'");
						}
					}
					/*
					String editorName = uri.toFileString();				
					if(editorName == null && (diagram.getName() == null || diagram.getName().compareTo("") == 0)) // si diagram relacionado a una View cuyo modelo ha sido borrado
					{
						//+ diagram.getName());					
						throw new ExecutionException("Can't open diagram '");
					}
					IEditorInput editorInput = new URIEditorInput(uri, editorName);
					IWorkbenchPage page = PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getActivePage();
					page.openEditor(editorInput, getEditorID());
					*/
					RefframeworkDiagramEditorUtil.openDiagram(diagramResource);
				}
				return CommandResult.newOKCommandResult();

			} catch (Exception ex) {
				throw new ExecutionException("Can't associate the diagram", ex);
			}
		}
		
		
		/**
		 * @generated NOT
		 */
		protected String getEditorID() {
			return RefframeworkDiagramEditor.ID;
		}
		
		
	}; // new AbstractTransactionalCommand
	
}

