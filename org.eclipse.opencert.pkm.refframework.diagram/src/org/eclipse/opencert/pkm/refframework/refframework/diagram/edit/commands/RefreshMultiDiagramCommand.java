/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.commands;

import java.util.Iterator;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CanonicalEditPolicy;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.opencert.infra.general.general.NamedElement;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.policies.RefFrameworkSemiCanonicalEditPolicy;
import org.eclipse.opencert.pkm.refframework.refframework.impl.RefFrameworkImpl;



/**
 * The Class RefreshMultiDiagramCommand.
 * 
 * @author  M� Carmen Palacios
 */
/**
 * @generated NOT
 */
//Start MCP: test several diagrams from one model
public class RefreshMultiDiagramCommand extends AbstractTransactionalCommand {
	    private List<EObject> listRootObjects;
        private View parentView;
        private EditPart host;

        /**
         * @generated NOT
         */
        public RefreshMultiDiagramCommand (
                        List<EObject> listObjects,
                        TransactionalEditingDomain editingDomain, View parentView,
                        EditPart host) {
                super(editingDomain, "Refresh element view", getWorkspaceFiles(parentView)); 
                this.listRootObjects = listObjects;
                this.parentView = parentView;
                this.host = host;
        }
        
        
        /**
         * @generated NOT
         */
        protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
                        IAdaptable info) throws ExecutionException {

        	if (listRootObjects == null || listRootObjects.isEmpty()) return  CommandResult.newErrorCommandResult("Objetos no disponibles");
        	
        	List<EObject> sublist = listRootObjects;
        	for( EObject element : sublist)
        	{
        	    if(false == element instanceof NamedElement)
        	    	listRootObjects.remove(element);
        	}
        	if (listRootObjects == null || listRootObjects.isEmpty()) return  CommandResult.newErrorCommandResult("Objetos no disponibles");        	

        	/* MCP!!!
            List editPolicies = CanonicalEditPolicy
                    .getRegisteredEditPolicies(rootObject);
            for (Iterator it = editPolicies.iterator(); it.hasNext(); ) {
                    CanonicalEditPolicy nextEditPolicy = (CanonicalEditPolicy) it.next();
                    nextEditPolicy.refresh();
            }
             */
            
    	    RefFrameworkSemiCanonicalEditPolicy kk = new RefFrameworkSemiCanonicalEditPolicy();
            kk.setObjects(listRootObjects);
            kk.setHost(this.host);
            kk.refresh();
            kk.deactivate(); // para que no invoque refreshSemantic() al eliminar del diagrama uno de los objetos
            
            return CommandResult.newOKCommandResult();
        }
}
//End MCP
