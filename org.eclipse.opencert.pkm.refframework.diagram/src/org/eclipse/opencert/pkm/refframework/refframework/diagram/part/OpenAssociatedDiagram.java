/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
package org.eclipse.opencert.pkm.refframework.refframework.diagram.part;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.eclipse.jface.action.ContributionItem;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.CompoundContributionItem;
//import org.eclipse.ui.internal.ActivateEditorHandler;
import org.eclipse.ui.menus.CommandContributionItem;
import org.eclipse.ui.menus.CommandContributionItemParameter;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.HintedDiagramLinkStyle;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivity2EditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefActivityEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefArtefactEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefFrameworkEditPart;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts.RefRoleEditPart;

/**
 * This class is used to generate the items of the OpenAssociatedDiagram menu
 * 
 * @author M� Carmen Palacios
 */
/**
 * @generated NOT
 */
public class OpenAssociatedDiagram extends CompoundContributionItem {
	private static ISelection selection;
	private static ArrayList fileList = new ArrayList();


	/**
	 * @generated NOT
	 */
	private static class NobodyHereContribution extends ContributionItem {
		/* (non-Javadoc)
		 * @see org.eclipse.jface.action.ContributionItem#fill(org.eclipse.swt.widgets.Menu, int)
		 */
		public void fill(Menu menu, int index) {
			MenuItem item = new MenuItem(menu, SWT.NONE, index);
			item.setText("Any file here"); // a mostrar si no hay subitems...
			item.setEnabled(false);
		}
	}

	
	/**
	 * @generated NOT
	 */
	private List<IFile> getHintedDiagrams(View view, List<IFile> lf) {
		List<IFile> ret = new ArrayList<IFile>();
		for( Object element : view.getStyles())
		{
			if (true == element instanceof HintedDiagramLinkStyle)
			{
				HintedDiagramLinkStyle elementHD = (HintedDiagramLinkStyle)element;
				if(elementHD.getHint() != null)
				{
					Diagram d = elementHD.getDiagramLink();
					for(IFile file : lf)
					{
						if(d.getName().contains(file.getName()))
						{
							ret.add(file);
							break;
						}
					}
				}
			}
		}
		
		return ret;
	}
	
	
	/**
	 * @generated NOT
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.actions.CompoundContributionItem#getContributionItems()
	 */
	protected IContributionItem[] getContributionItems() {
		List<IFile> listf = new ArrayList<IFile>();;		
		EditPart editPart = null;
		
		IWorkbenchWindow window = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow();
		IWorkbenchPart  part = window.getActivePage().getActivePart();
		selection = window.getActivePage().getSelection();
		IStructuredSelection s = (IStructuredSelection)this.selection;
		if(s!= null && !s.isEmpty() && 
		   (s.getFirstElement() instanceof RefActivityEditPart || s.getFirstElement() instanceof RefActivity2EditPart || s.getFirstElement() instanceof RefArtefactEditPart  || s.getFirstElement() instanceof RefRoleEditPart ))
		{
			editPart = (EditPart)s.getFirstElement();
		}

		//IEditorReference[] editors = window.getActivePage().getEditorReferences();
		String diagramfullFileName="";
		String path = getPath2(part.getTitle());		
		List<IFile> listfIni = getDiagramFiles(getProject(part.getTitle()));
		if(editPart != null)
		{
			View view = (View) editPart.getModel();
			listf = getHintedDiagrams(view, listfIni);
		}
		ResourceSet rSet = new ResourceSetImpl();
		fileList.clear();
		int cont = 1;
		ArrayList menuList = new ArrayList();		
		for(IFile element : listf)
		{
			//diagramfullFileName = path + element.getName();
			URI diagramfullFileNameURI = getFileURI(element, rSet);
			//String kk2 = diagramfullFileNameURI.toFileString();
			diagramfullFileName = diagramfullFileNameURI.toString();
			if(!diagramfullFileName.contains(part.getTitle())) // si no es el current diagram
			{
				try {
				menuList.add(createItem(cont++, diagramfullFileName));
				fileList.add(diagramfullFileName);
				}
				catch (PartInitException e) {
				}
			}
		}

		if (menuList.isEmpty()) {
			menuList.add(new NobodyHereContribution());
		}
		return (IContributionItem[]) menuList
				.toArray(new IContributionItem[menuList.size()]);
	}

	/**
	 * @generated NOT
	 */
	private IContributionItem createItem(int i, String file)
			throws PartInitException {
		CommandContributionItemParameter p = new CommandContributionItemParameter(
				PlatformUI.getWorkbench(), null, OpenAssociatedDiagramHandler.ID,
				CommandContributionItem.STYLE_PUSH);
		p.parameters = new HashMap();
		p.parameters.put(OpenAssociatedDiagramHandler.PARM_EDITOR, new Integer(i));
		String menuNum = Integer.toString(i);
		p.label = menuNum + " " + file;
		p.mnemonic = menuNum;
		return new CommandContributionItem(p);
	}
	
	

	/**
	 * @generated NOT
	 */
	  private Collection<Diagram> getDiagrams(IProject p) {
	       final List<IFile> files = getDiagramFiles(p);
	       final List<Diagram> diagramList = new ArrayList<Diagram>();
	       final ResourceSet rSet = new ResourceSetImpl();
	       for (final IFile file : files) {
	            final Diagram diagram = getDiagramFromFile(file, rSet);
	            if (diagram != null) {
	                diagramList.add(diagram);
	            }
	       }
	       return diagramList;
	    }

	 
		/**
		 * @generated NOT
		 */
	   private List<IFile> getDiagramFiles(IContainer folder) {
	       final List<IFile> ret = new ArrayList<IFile>();
	       try {
	            final IResource[] members = folder.members();
	            for (final IResource resource : members) {
	                 if (resource instanceof IContainer) {
	                     ret.addAll(getDiagramFiles((IContainer) resource));
	                 } else if (resource instanceof IFile) {
	                     final IFile file = (IFile) resource;
	                     if (file.getName().endsWith(RefFrameworkEditPart.FILE_DIAGRAM_ID)) {
	                          ret.add(file);
	                     }
	                 }
	            }
	       } catch (final CoreException e) {
	                e.printStackTrace();
	       }
	       return ret;
	    }
	 
	 
		/**
		 * @generated NOT
		 */
	    private Diagram getDiagramFromFile(IFile file, 
                ResourceSet resourceSet) {
			// Get the URI of the model file.
			final URI resourceURI = getFileURI(file, resourceSet);
			// Demand load the resource for this file.
			Resource resource;
			try {
				resource = resourceSet.getResource(resourceURI, true);
				if (resource != null) {
					// does resource contain a diagram as root object?
					final EList<EObject> contents = resource.getContents();
					for (final EObject object : contents) {
						if (object instanceof Diagram) {
						return (Diagram) object;
						}
					}
				}
			} catch (final WrappedException e) {
			e.printStackTrace();
			}
			return null;
		}
	 
		/**
		 * @generated NOT
		 */
	    private URI getFileURI(IFile file, ResourceSet resourceSet) {
	        final String pathName = file.getFullPath().toString();
	        URI resourceURI = URI.createFileURI(pathName);
	        resourceURI = resourceSet.getURIConverter().normalize(resourceURI);
	        return resourceURI;
	     }
	    
	
    

		/**
		 * @generated NOT
		 */
	    private String getPath2(String fileName)
	    {
	    	String path = "";
	    	IProject[] projects = ResourcesPlugin.getWorkspace().getRoot().getProjects();
	    	for(int i=0;i<projects.length;i++)
	    	{
	    		IProject curr = projects[i];
	    		List<IFile> list = getDiagramFiles(curr);
	    		for(IFile element : list)
	    		{
	    			if(element.getName().contains(fileName))
	    			{
	    				path = element.getFullPath().toString();
	    				int lind = path.lastIndexOf(fileName);
	    				String path2 = path.substring(0, lind);
	    				return path2;
	    			}
	    		}
	    	}
	    
	    	return path;
	    }

		/**
		 * @generated NOT
		 */
	    private IProject getProject(String fileName)
	    {
	    	IProject curr = null;
	    	IProject[] projects = ResourcesPlugin.getWorkspace().getRoot().getProjects();
	    	for(int i=0;i<projects.length;i++)
	    	{
	    		curr = projects[i];
	    		List<IFile> list = getDiagramFiles(curr);
	    		for(IFile element : list)
	    		{
	    			if(element.getName().contains(fileName))
	    			{
	    				return curr;
	    			}
	    		}
	    	}
	    
	    	return curr;
	    }
	    
	
		/**
		 * @generated NOT
		 */
	    public static String getFile(int ind)
	    {
	    	String fileName = (String) fileList.get(ind-1);
	    	return fileName;
	    }

	    
		/**
		 * @generated NOT
		 */
	    public static ISelection getSelection()
	    {
	    	return selection;
	    }
    
}
