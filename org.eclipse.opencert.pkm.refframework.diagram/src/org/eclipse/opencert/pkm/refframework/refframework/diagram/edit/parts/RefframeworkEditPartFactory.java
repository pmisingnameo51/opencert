/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/*
 * 
 */
package org.eclipse.opencert.pkm.refframework.refframework.diagram.edit.parts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.directedit.locator.CellEditorLocatorAccess;
import org.eclipse.opencert.pkm.refframework.refframework.diagram.part.RefframeworkVisualIDRegistry;

/**
 * @generated
 */
public class RefframeworkEditPartFactory implements EditPartFactory {

	/**
	 * @generated
	 */
	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof View) {
			View view = (View) model;
			switch (RefframeworkVisualIDRegistry.getVisualID(view)) {

			case RefFrameworkEditPart.VISUAL_ID:
				return new RefFrameworkEditPart(view);

			case RefActivityEditPart.VISUAL_ID:
				return new RefActivityEditPart(view);

			case RefActivityNameEditPart.VISUAL_ID:
				return new RefActivityNameEditPart(view);

			case RefArtefactEditPart.VISUAL_ID:
				return new RefArtefactEditPart(view);

			case RefArtefactNameEditPart.VISUAL_ID:
				return new RefArtefactNameEditPart(view);

			case RefRoleEditPart.VISUAL_ID:
				return new RefRoleEditPart(view);

			case RefRoleNameEditPart.VISUAL_ID:
				return new RefRoleNameEditPart(view);

			case RefActivity2EditPart.VISUAL_ID:
				return new RefActivity2EditPart(view);

			case RefActivityName2EditPart.VISUAL_ID:
				return new RefActivityName2EditPart(view);

			case RefActivityRefActivitySubActivityCompartmentEditPart.VISUAL_ID:
				return new RefActivityRefActivitySubActivityCompartmentEditPart(
						view);

			case RefActivityRefActivitySubActivityCompartment2EditPart.VISUAL_ID:
				return new RefActivityRefActivitySubActivityCompartment2EditPart(
						view);

			case RefActivityRequiredArtefactEditPart.VISUAL_ID:
				return new RefActivityRequiredArtefactEditPart(view);

			case WrappingLabelEditPart.VISUAL_ID:
				return new WrappingLabelEditPart(view);

			case RefActivityProducedArtefactEditPart.VISUAL_ID:
				return new RefActivityProducedArtefactEditPart(view);

			case WrappingLabel2EditPart.VISUAL_ID:
				return new WrappingLabel2EditPart(view);

			case RefActivityPrecedingActivityEditPart.VISUAL_ID:
				return new RefActivityPrecedingActivityEditPart(view);

			case WrappingLabel3EditPart.VISUAL_ID:
				return new WrappingLabel3EditPart(view);

			case RefActivityRoleEditPart.VISUAL_ID:
				return new RefActivityRoleEditPart(view);

			case WrappingLabel4EditPart.VISUAL_ID:
				return new WrappingLabel4EditPart(view);

			}
		}
		return createUnrecognizedEditPart(context, model);
	}

	/**
	 * @generated
	 */
	private EditPart createUnrecognizedEditPart(EditPart context, Object model) {
		// Handle creation of unrecognized child node EditParts here
		return null;
	}

	/**
	 * @generated
	 */
	public static CellEditorLocator getTextCellEditorLocator(
			ITextAwareEditPart source) {
		return CellEditorLocatorAccess.INSTANCE
				.getTextCellEditorLocator(source);
	}

}
