/*******************************************************************************
 *
 * Copyright (c) 2017 Intecs SpA 
*
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 * Stefano Puri stefano.puri@intecs.it
 * Initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.opencert.chess.contracts.validation;

import org.eclipse.emf.common.command.Command;
import org.eclipse.opencert.chess.contracts.validation.command.CHESSContractValidateCommand;
import org.eclipse.papyrus.commands.wrappers.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.services.validation.handler.AbstractCommandHandler;

public class CHESSContractValidationHandler extends AbstractCommandHandler {

	private CHESSContractValidateCommand foreverValidateCommand;
	
	@Override
	public Command getCommand() {
		String label = "Validate model for NuSMV3-OCRA analysis tool";
		foreverValidateCommand = new CHESSContractValidateCommand(label, Activator.PLUGIN_ID, getSelectedElement());
		return new GMFtoEMFCommandWrapper(foreverValidateCommand);
	}

}
