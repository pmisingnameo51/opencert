/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pam.procspec.process;

import java.util.Date;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.ManageableAssuranceAsset;
import org.eclipse.opencert.infra.general.general.DescribableElement;
import org.eclipse.opencert.evm.evidspec.evidence.Artefact;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Activity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.Activity#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.Activity#getEndTime <em>End Time</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.Activity#getSubActivity <em>Sub Activity</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.Activity#getPrecedingActivity <em>Preceding Activity</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.Activity#getParticipant <em>Participant</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.Activity#getTechnique <em>Technique</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.Activity#getOwnedRel <em>Owned Rel</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.Activity#getRequiredArtefact <em>Required Artefact</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.Activity#getProducedArtefact <em>Produced Artefact</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.Activity#getAssetEvent <em>Asset Event</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.pam.procspec.process.ProcessPackage#getActivity()
 * @model
 * @generated
 */
public interface Activity extends ManageableAssuranceAsset, DescribableElement {
	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' attribute.
	 * @see #setStartTime(Date)
	 * @see org.eclipse.opencert.pam.procspec.process.ProcessPackage#getActivity_StartTime()
	 * @model
	 * @generated
	 */
	Date getStartTime();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.pam.procspec.process.Activity#getStartTime <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' attribute.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(Date value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' attribute.
	 * @see #setEndTime(Date)
	 * @see org.eclipse.opencert.pam.procspec.process.ProcessPackage#getActivity_EndTime()
	 * @model
	 * @generated
	 */
	Date getEndTime();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.pam.procspec.process.Activity#getEndTime <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' attribute.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(Date value);

	/**
	 * Returns the value of the '<em><b>Sub Activity</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pam.procspec.process.Activity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Activity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Activity</em>' containment reference list.
	 * @see org.eclipse.opencert.pam.procspec.process.ProcessPackage#getActivity_SubActivity()
	 * @model containment="true"
	 * @generated
	 */
	EList<Activity> getSubActivity();

	/**
	 * Returns the value of the '<em><b>Preceding Activity</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pam.procspec.process.Activity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preceding Activity</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preceding Activity</em>' reference list.
	 * @see org.eclipse.opencert.pam.procspec.process.ProcessPackage#getActivity_PrecedingActivity()
	 * @model
	 * @generated
	 */
	EList<Activity> getPrecedingActivity();

	/**
	 * Returns the value of the '<em><b>Participant</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pam.procspec.process.Participant}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Participant</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Participant</em>' reference list.
	 * @see org.eclipse.opencert.pam.procspec.process.ProcessPackage#getActivity_Participant()
	 * @model
	 * @generated
	 */
	EList<Participant> getParticipant();

	/**
	 * Returns the value of the '<em><b>Technique</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pam.procspec.process.Technique}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Technique</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Technique</em>' reference list.
	 * @see org.eclipse.opencert.pam.procspec.process.ProcessPackage#getActivity_Technique()
	 * @model
	 * @generated
	 */
	EList<Technique> getTechnique();

	/**
	 * Returns the value of the '<em><b>Owned Rel</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pam.procspec.process.ActivityRel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Rel</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Rel</em>' containment reference list.
	 * @see org.eclipse.opencert.pam.procspec.process.ProcessPackage#getActivity_OwnedRel()
	 * @model containment="true"
	 * @generated
	 */
	EList<ActivityRel> getOwnedRel();

	/**
	 * Returns the value of the '<em><b>Required Artefact</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.evm.evidspec.evidence.Artefact}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Artefact</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Artefact</em>' reference list.
	 * @see org.eclipse.opencert.pam.procspec.process.ProcessPackage#getActivity_RequiredArtefact()
	 * @model
	 * @generated
	 */
	EList<Artefact> getRequiredArtefact();

	/**
	 * Returns the value of the '<em><b>Produced Artefact</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.evm.evidspec.evidence.Artefact}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Produced Artefact</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Produced Artefact</em>' reference list.
	 * @see org.eclipse.opencert.pam.procspec.process.ProcessPackage#getActivity_ProducedArtefact()
	 * @model
	 * @generated
	 */
	EList<Artefact> getProducedArtefact();

	/**
	 * Returns the value of the '<em><b>Asset Event</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asset Event</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asset Event</em>' reference list.
	 * @see org.eclipse.opencert.pam.procspec.process.ProcessPackage#getActivity_AssetEvent()
	 * @model
	 * @generated
	 */
	EList<AssuranceAssetEvent> getAssetEvent();

} // Activity
