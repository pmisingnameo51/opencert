/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pam.procspec.process.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.opencert.infra.general.general.impl.DescribableElementImpl;
import org.eclipse.opencert.pam.procspec.process.Activity;
import org.eclipse.opencert.pam.procspec.process.Participant;
import org.eclipse.opencert.pam.procspec.process.ProcessModel;
import org.eclipse.opencert.pam.procspec.process.ProcessPackage;
import org.eclipse.opencert.pam.procspec.process.Technique;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.ProcessModelImpl#getOwnedActivity <em>Owned Activity</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.ProcessModelImpl#getOwnedParticipant <em>Owned Participant</em>}</li>
 *   <li>{@link org.eclipse.opencert.pam.procspec.process.impl.ProcessModelImpl#getOwnedTechnique <em>Owned Technique</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ProcessModelImpl extends DescribableElementImpl implements ProcessModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProcessModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ProcessPackage.Literals.PROCESS_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Activity> getOwnedActivity() {
		return (EList<Activity>)eDynamicGet(ProcessPackage.PROCESS_MODEL__OWNED_ACTIVITY, ProcessPackage.Literals.PROCESS_MODEL__OWNED_ACTIVITY, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Participant> getOwnedParticipant() {
		return (EList<Participant>)eDynamicGet(ProcessPackage.PROCESS_MODEL__OWNED_PARTICIPANT, ProcessPackage.Literals.PROCESS_MODEL__OWNED_PARTICIPANT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Technique> getOwnedTechnique() {
		return (EList<Technique>)eDynamicGet(ProcessPackage.PROCESS_MODEL__OWNED_TECHNIQUE, ProcessPackage.Literals.PROCESS_MODEL__OWNED_TECHNIQUE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ProcessPackage.PROCESS_MODEL__OWNED_ACTIVITY:
				return ((InternalEList<?>)getOwnedActivity()).basicRemove(otherEnd, msgs);
			case ProcessPackage.PROCESS_MODEL__OWNED_PARTICIPANT:
				return ((InternalEList<?>)getOwnedParticipant()).basicRemove(otherEnd, msgs);
			case ProcessPackage.PROCESS_MODEL__OWNED_TECHNIQUE:
				return ((InternalEList<?>)getOwnedTechnique()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ProcessPackage.PROCESS_MODEL__OWNED_ACTIVITY:
				return getOwnedActivity();
			case ProcessPackage.PROCESS_MODEL__OWNED_PARTICIPANT:
				return getOwnedParticipant();
			case ProcessPackage.PROCESS_MODEL__OWNED_TECHNIQUE:
				return getOwnedTechnique();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ProcessPackage.PROCESS_MODEL__OWNED_ACTIVITY:
				getOwnedActivity().clear();
				getOwnedActivity().addAll((Collection<? extends Activity>)newValue);
				return;
			case ProcessPackage.PROCESS_MODEL__OWNED_PARTICIPANT:
				getOwnedParticipant().clear();
				getOwnedParticipant().addAll((Collection<? extends Participant>)newValue);
				return;
			case ProcessPackage.PROCESS_MODEL__OWNED_TECHNIQUE:
				getOwnedTechnique().clear();
				getOwnedTechnique().addAll((Collection<? extends Technique>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ProcessPackage.PROCESS_MODEL__OWNED_ACTIVITY:
				getOwnedActivity().clear();
				return;
			case ProcessPackage.PROCESS_MODEL__OWNED_PARTICIPANT:
				getOwnedParticipant().clear();
				return;
			case ProcessPackage.PROCESS_MODEL__OWNED_TECHNIQUE:
				getOwnedTechnique().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ProcessPackage.PROCESS_MODEL__OWNED_ACTIVITY:
				return !getOwnedActivity().isEmpty();
			case ProcessPackage.PROCESS_MODEL__OWNED_PARTICIPANT:
				return !getOwnedParticipant().isEmpty();
			case ProcessPackage.PROCESS_MODEL__OWNED_TECHNIQUE:
				return !getOwnedTechnique().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ProcessModelImpl
