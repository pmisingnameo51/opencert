/*
 * Copyright (c) 2010-2012 Eike Stepper (Berlin, Germany) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Martin Fluegge - initial API and implementation
 *         Angel L�pez (Tecnalia)- Modified to solve bug https://www.eclipse.org/forums/index.php/m/1355009/#msg_1355009
 */
package org.eclipse.emf.cdo.dawn.ui.views;

import java.util.ArrayList;

import org.eclipse.emf.cdo.common.security.CDOPermission;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.ui.CDOItemProvider;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.net4j.util.container.IContainer;
import org.eclipse.net4j.util.ui.views.IElementFilter;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;

/**
 * @author Martin Fluegge
 * @param <CONTAINER>
 * * Modified by Tecnalia
 */
public class DawnWizardPageItemProvider<CONTAINER extends IContainer<Object>> extends CDOItemProvider
{

  public DawnWizardPageItemProvider(IElementFilter iElementFilter)
  {
    super(null, iElementFilter);
  }

  /*@Override
  public Object[] getChildren(Object element)
  {
    if (element instanceof CDOResourceFolder)
    {
      return ((CDOResourceFolder)element).getNodes().toArray();
    }

    if (element instanceof CDOSession)
    {
      CDOSession session = (CDOSession)element;
      Object[] child = new Object[session.getViews().length];
      for(int x=0;x<session.getViews().length;x++){
    	  child[x] = session.getViews()[x];// .getView(dawnExplorer.getView().getViewID());
      }
      return child;
    }

    return super.getChildren(element);
  }*/
  
  //Modified by Tecnalia
  @Override
  public Object[] getChildren(Object element)
  {
    Object[] result = super.getChildren(element);
    
    if(element instanceof CDOView){
    	ArrayList<Object> readableElements = new ArrayList<Object>();
    	for (Object oneCDOResource : result) {
			if(oneCDOResource instanceof CDOResourceNode){
				CDOResourceNode node = (CDOResourceNode)oneCDOResource;
				node.cdoReload();
				CDOPermission permission = node.cdoRevision().getPermission();
				if(permission.isWritable()){
					readableElements.add(oneCDOResource);
				}
			}
		}
    	result=readableElements.toArray();
    }
    
    if (result.length > 0 && result[0] instanceof CDOView)
    {
	    IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
	    IViewPart view = page.findView("org.eclipse.emf.cdo.dawn.ui.views.DawnExplorer");
	    // TODO This might be null! handle!
	    DawnExplorer dawnExplorer = (DawnExplorer)view;
	    
	    for (int i = 0; i < result.length; i++)
	    {
	      if (result[i] == dawnExplorer.getView())
	      {
	        //ourView = (CDOView)result[i];
	        result = new Object[] { result[i] };    
	        return result;
	      }
	    }
    }
   /* if (result.length > 0 && result[0] instanceof CDOView)
    {      
        result = new Object[] { result[0] };    
      return result;
    }*/

    if (element instanceof CDOResourceFolder)
    {
    	
    	Object[] interResult =  ((CDOResourceFolder)element).getNodes().toArray();
    	
    	ArrayList<Object> readableElements = new ArrayList<Object>();
    	for (Object oneCDOResource : result) {
			if(oneCDOResource instanceof CDOResourceNode){
				CDOResourceNode node = (CDOResourceNode)oneCDOResource;
				node.cdoReload();
				CDOPermission permission = node.cdoRevision().getPermission();
				if(permission.isReadable()){
					readableElements.add(oneCDOResource);
				}
			}
		}
    	result=readableElements.toArray();
    }
    return result;
  }

  @Override
  public boolean hasChildren(Object element)
  {
    if (element instanceof CDOView)
    {
      return ((CDOView)element).getRootResource().getContents().size() > 0;
    }

    if (element instanceof CDOResourceFolder)
    {
    	CDOResourceNode node = (CDOResourceFolder)element;
    	node.cdoReload();
		CDOPermission permission = node.cdoRevision().getPermission();
		
    	if(permission.isWritable()){
    		return ((CDOResourceFolder)element).getNodes().size() > 0;
    	}
    }

    return super.hasChildren(element);
  }  
  
  @Override
  public Object getParent(Object element)
  {
    if (element instanceof CDOResourceNode)
    {
      CDOResourceNode node = (CDOResourceNode)element;
      CDOResourceNode parent = (CDOResourceNode)node.eContainer();
      if (parent.isRoot())
      {
        return parent.cdoView();
      }

      return parent;
    }

    return super.getParent(element);
  }
}
