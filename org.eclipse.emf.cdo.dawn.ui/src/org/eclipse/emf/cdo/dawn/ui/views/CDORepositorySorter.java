package org.eclipse.emf.cdo.dawn.ui.views;

import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;

public class CDORepositorySorter extends ViewerSorter{

	@Override
	public int category(Object element) {
		if(element instanceof CDOResourceFolder) return 1;
	    if(element instanceof CDOResource) return 2;
	    return 3;
	}

	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {
		// TODO Auto-generated method stub
		return super.compare(viewer, e1, e2);
	}

}
