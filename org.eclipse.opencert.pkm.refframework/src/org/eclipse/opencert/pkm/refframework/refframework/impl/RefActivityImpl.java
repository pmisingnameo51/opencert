/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pkm.refframework.refframework.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.opencert.infra.general.general.impl.DescribableElementImpl;
import org.eclipse.opencert.pkm.refframework.refframework.RefActivity;
import org.eclipse.opencert.pkm.refframework.refframework.RefActivityRel;
import org.eclipse.opencert.pkm.refframework.refframework.RefApplicability;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefact;
import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;
import org.eclipse.opencert.pkm.refframework.refframework.RefEquivalenceMap;
import org.eclipse.opencert.pkm.refframework.refframework.RefRequirement;
import org.eclipse.opencert.pkm.refframework.refframework.RefRole;
import org.eclipse.opencert.pkm.refframework.refframework.RefTechnique;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ref Activity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefActivityImpl#getEquivalence <em>Equivalence</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefActivityImpl#getObjective <em>Objective</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefActivityImpl#getScope <em>Scope</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefActivityImpl#getRequiredArtefact <em>Required Artefact</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefActivityImpl#getProducedArtefact <em>Produced Artefact</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefActivityImpl#getSubActivity <em>Sub Activity</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefActivityImpl#getPrecedingActivity <em>Preceding Activity</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefActivityImpl#getOwnedRequirement <em>Owned Requirement</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefActivityImpl#getRole <em>Role</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefActivityImpl#getApplicableTechnique <em>Applicable Technique</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefActivityImpl#getOwnedRel <em>Owned Rel</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefActivityImpl#getApplicability <em>Applicability</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RefActivityImpl extends DescribableElementImpl implements RefActivity {
	/**
	 * The default value of the '{@link #getObjective() <em>Objective</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjective()
	 * @generated
	 * @ordered
	 */
	protected static final String OBJECTIVE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getScope() <em>Scope</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScope()
	 * @generated
	 * @ordered
	 */
	protected static final String SCOPE_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RefActivityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RefframeworkPackage.Literals.REF_ACTIVITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefEquivalenceMap> getEquivalence() {
		return (EList<RefEquivalenceMap>)eDynamicGet(RefframeworkPackage.REF_ACTIVITY__EQUIVALENCE, RefframeworkPackage.Literals.REF_ASSURABLE_ELEMENT__EQUIVALENCE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getObjective() {
		return (String)eDynamicGet(RefframeworkPackage.REF_ACTIVITY__OBJECTIVE, RefframeworkPackage.Literals.REF_ACTIVITY__OBJECTIVE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjective(String newObjective) {
		eDynamicSet(RefframeworkPackage.REF_ACTIVITY__OBJECTIVE, RefframeworkPackage.Literals.REF_ACTIVITY__OBJECTIVE, newObjective);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getScope() {
		return (String)eDynamicGet(RefframeworkPackage.REF_ACTIVITY__SCOPE, RefframeworkPackage.Literals.REF_ACTIVITY__SCOPE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScope(String newScope) {
		eDynamicSet(RefframeworkPackage.REF_ACTIVITY__SCOPE, RefframeworkPackage.Literals.REF_ACTIVITY__SCOPE, newScope);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefArtefact> getRequiredArtefact() {
		return (EList<RefArtefact>)eDynamicGet(RefframeworkPackage.REF_ACTIVITY__REQUIRED_ARTEFACT, RefframeworkPackage.Literals.REF_ACTIVITY__REQUIRED_ARTEFACT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefArtefact> getProducedArtefact() {
		return (EList<RefArtefact>)eDynamicGet(RefframeworkPackage.REF_ACTIVITY__PRODUCED_ARTEFACT, RefframeworkPackage.Literals.REF_ACTIVITY__PRODUCED_ARTEFACT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefActivity> getSubActivity() {
		return (EList<RefActivity>)eDynamicGet(RefframeworkPackage.REF_ACTIVITY__SUB_ACTIVITY, RefframeworkPackage.Literals.REF_ACTIVITY__SUB_ACTIVITY, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefActivity> getPrecedingActivity() {
		return (EList<RefActivity>)eDynamicGet(RefframeworkPackage.REF_ACTIVITY__PRECEDING_ACTIVITY, RefframeworkPackage.Literals.REF_ACTIVITY__PRECEDING_ACTIVITY, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefRequirement> getOwnedRequirement() {
		return (EList<RefRequirement>)eDynamicGet(RefframeworkPackage.REF_ACTIVITY__OWNED_REQUIREMENT, RefframeworkPackage.Literals.REF_ACTIVITY__OWNED_REQUIREMENT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefRole> getRole() {
		return (EList<RefRole>)eDynamicGet(RefframeworkPackage.REF_ACTIVITY__ROLE, RefframeworkPackage.Literals.REF_ACTIVITY__ROLE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefTechnique getApplicableTechnique() {
		return (RefTechnique)eDynamicGet(RefframeworkPackage.REF_ACTIVITY__APPLICABLE_TECHNIQUE, RefframeworkPackage.Literals.REF_ACTIVITY__APPLICABLE_TECHNIQUE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefTechnique basicGetApplicableTechnique() {
		return (RefTechnique)eDynamicGet(RefframeworkPackage.REF_ACTIVITY__APPLICABLE_TECHNIQUE, RefframeworkPackage.Literals.REF_ACTIVITY__APPLICABLE_TECHNIQUE, false, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApplicableTechnique(RefTechnique newApplicableTechnique) {
		eDynamicSet(RefframeworkPackage.REF_ACTIVITY__APPLICABLE_TECHNIQUE, RefframeworkPackage.Literals.REF_ACTIVITY__APPLICABLE_TECHNIQUE, newApplicableTechnique);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefActivityRel> getOwnedRel() {
		return (EList<RefActivityRel>)eDynamicGet(RefframeworkPackage.REF_ACTIVITY__OWNED_REL, RefframeworkPackage.Literals.REF_ACTIVITY__OWNED_REL, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefApplicability> getApplicability() {
		return (EList<RefApplicability>)eDynamicGet(RefframeworkPackage.REF_ACTIVITY__APPLICABILITY, RefframeworkPackage.Literals.REF_ACTIVITY__APPLICABILITY, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RefframeworkPackage.REF_ACTIVITY__EQUIVALENCE:
				return ((InternalEList<?>)getEquivalence()).basicRemove(otherEnd, msgs);
			case RefframeworkPackage.REF_ACTIVITY__SUB_ACTIVITY:
				return ((InternalEList<?>)getSubActivity()).basicRemove(otherEnd, msgs);
			case RefframeworkPackage.REF_ACTIVITY__OWNED_REQUIREMENT:
				return ((InternalEList<?>)getOwnedRequirement()).basicRemove(otherEnd, msgs);
			case RefframeworkPackage.REF_ACTIVITY__OWNED_REL:
				return ((InternalEList<?>)getOwnedRel()).basicRemove(otherEnd, msgs);
			case RefframeworkPackage.REF_ACTIVITY__APPLICABILITY:
				return ((InternalEList<?>)getApplicability()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RefframeworkPackage.REF_ACTIVITY__EQUIVALENCE:
				return getEquivalence();
			case RefframeworkPackage.REF_ACTIVITY__OBJECTIVE:
				return getObjective();
			case RefframeworkPackage.REF_ACTIVITY__SCOPE:
				return getScope();
			case RefframeworkPackage.REF_ACTIVITY__REQUIRED_ARTEFACT:
				return getRequiredArtefact();
			case RefframeworkPackage.REF_ACTIVITY__PRODUCED_ARTEFACT:
				return getProducedArtefact();
			case RefframeworkPackage.REF_ACTIVITY__SUB_ACTIVITY:
				return getSubActivity();
			case RefframeworkPackage.REF_ACTIVITY__PRECEDING_ACTIVITY:
				return getPrecedingActivity();
			case RefframeworkPackage.REF_ACTIVITY__OWNED_REQUIREMENT:
				return getOwnedRequirement();
			case RefframeworkPackage.REF_ACTIVITY__ROLE:
				return getRole();
			case RefframeworkPackage.REF_ACTIVITY__APPLICABLE_TECHNIQUE:
				if (resolve) return getApplicableTechnique();
				return basicGetApplicableTechnique();
			case RefframeworkPackage.REF_ACTIVITY__OWNED_REL:
				return getOwnedRel();
			case RefframeworkPackage.REF_ACTIVITY__APPLICABILITY:
				return getApplicability();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RefframeworkPackage.REF_ACTIVITY__EQUIVALENCE:
				getEquivalence().clear();
				getEquivalence().addAll((Collection<? extends RefEquivalenceMap>)newValue);
				return;
			case RefframeworkPackage.REF_ACTIVITY__OBJECTIVE:
				setObjective((String)newValue);
				return;
			case RefframeworkPackage.REF_ACTIVITY__SCOPE:
				setScope((String)newValue);
				return;
			case RefframeworkPackage.REF_ACTIVITY__REQUIRED_ARTEFACT:
				getRequiredArtefact().clear();
				getRequiredArtefact().addAll((Collection<? extends RefArtefact>)newValue);
				return;
			case RefframeworkPackage.REF_ACTIVITY__PRODUCED_ARTEFACT:
				getProducedArtefact().clear();
				getProducedArtefact().addAll((Collection<? extends RefArtefact>)newValue);
				return;
			case RefframeworkPackage.REF_ACTIVITY__SUB_ACTIVITY:
				getSubActivity().clear();
				getSubActivity().addAll((Collection<? extends RefActivity>)newValue);
				return;
			case RefframeworkPackage.REF_ACTIVITY__PRECEDING_ACTIVITY:
				getPrecedingActivity().clear();
				getPrecedingActivity().addAll((Collection<? extends RefActivity>)newValue);
				return;
			case RefframeworkPackage.REF_ACTIVITY__OWNED_REQUIREMENT:
				getOwnedRequirement().clear();
				getOwnedRequirement().addAll((Collection<? extends RefRequirement>)newValue);
				return;
			case RefframeworkPackage.REF_ACTIVITY__ROLE:
				getRole().clear();
				getRole().addAll((Collection<? extends RefRole>)newValue);
				return;
			case RefframeworkPackage.REF_ACTIVITY__APPLICABLE_TECHNIQUE:
				setApplicableTechnique((RefTechnique)newValue);
				return;
			case RefframeworkPackage.REF_ACTIVITY__OWNED_REL:
				getOwnedRel().clear();
				getOwnedRel().addAll((Collection<? extends RefActivityRel>)newValue);
				return;
			case RefframeworkPackage.REF_ACTIVITY__APPLICABILITY:
				getApplicability().clear();
				getApplicability().addAll((Collection<? extends RefApplicability>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RefframeworkPackage.REF_ACTIVITY__EQUIVALENCE:
				getEquivalence().clear();
				return;
			case RefframeworkPackage.REF_ACTIVITY__OBJECTIVE:
				setObjective(OBJECTIVE_EDEFAULT);
				return;
			case RefframeworkPackage.REF_ACTIVITY__SCOPE:
				setScope(SCOPE_EDEFAULT);
				return;
			case RefframeworkPackage.REF_ACTIVITY__REQUIRED_ARTEFACT:
				getRequiredArtefact().clear();
				return;
			case RefframeworkPackage.REF_ACTIVITY__PRODUCED_ARTEFACT:
				getProducedArtefact().clear();
				return;
			case RefframeworkPackage.REF_ACTIVITY__SUB_ACTIVITY:
				getSubActivity().clear();
				return;
			case RefframeworkPackage.REF_ACTIVITY__PRECEDING_ACTIVITY:
				getPrecedingActivity().clear();
				return;
			case RefframeworkPackage.REF_ACTIVITY__OWNED_REQUIREMENT:
				getOwnedRequirement().clear();
				return;
			case RefframeworkPackage.REF_ACTIVITY__ROLE:
				getRole().clear();
				return;
			case RefframeworkPackage.REF_ACTIVITY__APPLICABLE_TECHNIQUE:
				setApplicableTechnique((RefTechnique)null);
				return;
			case RefframeworkPackage.REF_ACTIVITY__OWNED_REL:
				getOwnedRel().clear();
				return;
			case RefframeworkPackage.REF_ACTIVITY__APPLICABILITY:
				getApplicability().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RefframeworkPackage.REF_ACTIVITY__EQUIVALENCE:
				return !getEquivalence().isEmpty();
			case RefframeworkPackage.REF_ACTIVITY__OBJECTIVE:
				return OBJECTIVE_EDEFAULT == null ? getObjective() != null : !OBJECTIVE_EDEFAULT.equals(getObjective());
			case RefframeworkPackage.REF_ACTIVITY__SCOPE:
				return SCOPE_EDEFAULT == null ? getScope() != null : !SCOPE_EDEFAULT.equals(getScope());
			case RefframeworkPackage.REF_ACTIVITY__REQUIRED_ARTEFACT:
				return !getRequiredArtefact().isEmpty();
			case RefframeworkPackage.REF_ACTIVITY__PRODUCED_ARTEFACT:
				return !getProducedArtefact().isEmpty();
			case RefframeworkPackage.REF_ACTIVITY__SUB_ACTIVITY:
				return !getSubActivity().isEmpty();
			case RefframeworkPackage.REF_ACTIVITY__PRECEDING_ACTIVITY:
				return !getPrecedingActivity().isEmpty();
			case RefframeworkPackage.REF_ACTIVITY__OWNED_REQUIREMENT:
				return !getOwnedRequirement().isEmpty();
			case RefframeworkPackage.REF_ACTIVITY__ROLE:
				return !getRole().isEmpty();
			case RefframeworkPackage.REF_ACTIVITY__APPLICABLE_TECHNIQUE:
				return basicGetApplicableTechnique() != null;
			case RefframeworkPackage.REF_ACTIVITY__OWNED_REL:
				return !getOwnedRel().isEmpty();
			case RefframeworkPackage.REF_ACTIVITY__APPLICABILITY:
				return !getApplicability().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == RefAssurableElement.class) {
			switch (derivedFeatureID) {
				case RefframeworkPackage.REF_ACTIVITY__EQUIVALENCE: return RefframeworkPackage.REF_ASSURABLE_ELEMENT__EQUIVALENCE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == RefAssurableElement.class) {
			switch (baseFeatureID) {
				case RefframeworkPackage.REF_ASSURABLE_ELEMENT__EQUIVALENCE: return RefframeworkPackage.REF_ACTIVITY__EQUIVALENCE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //RefActivityImpl
