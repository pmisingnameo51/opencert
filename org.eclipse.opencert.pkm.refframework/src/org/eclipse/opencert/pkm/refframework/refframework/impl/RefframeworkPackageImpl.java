/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pkm.refframework.refframework.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.infra.properties.property.PropertyPackage;
import org.eclipse.opencert.infra.mappings.mapping.MappingPackage;
import org.eclipse.opencert.pkm.refframework.refframework.RefActivity;
import org.eclipse.opencert.pkm.refframework.refframework.RefActivityRel;
import org.eclipse.opencert.pkm.refframework.refframework.RefApplicability;
import org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityLevel;
import org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityRel;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefact;
import org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel;
import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;
import org.eclipse.opencert.pkm.refframework.refframework.RefControlCategory;
import org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityApplicability;
import org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityLevel;
import org.eclipse.opencert.pkm.refframework.refframework.RefEquivalenceMap;
import org.eclipse.opencert.pkm.refframework.refframework.RefFramework;
import org.eclipse.opencert.pkm.refframework.refframework.RefIndependencyLevel;
import org.eclipse.opencert.pkm.refframework.refframework.RefRecommendationLevel;
import org.eclipse.opencert.pkm.refframework.refframework.RefRequirement;
import org.eclipse.opencert.pkm.refframework.refframework.RefRequirementRel;
import org.eclipse.opencert.pkm.refframework.refframework.RefRole;
import org.eclipse.opencert.pkm.refframework.refframework.RefTechnique;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkFactory;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RefframeworkPackageImpl extends EPackageImpl implements RefframeworkPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass refFrameworkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass refRequirementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass refArtefactEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass refActivityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass refRequirementRelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass refRoleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass refApplicabilityLevelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass refCriticalityLevelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass refTechniqueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass refArtefactRelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass refCriticalityApplicabilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass refActivityRelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass refAssurableElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass refIndependencyLevelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass refRecommendationLevelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass refControlCategoryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass refApplicabilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass refApplicabilityRelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass refEquivalenceMapEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private RefframeworkPackageImpl() {
		super(eNS_URI, RefframeworkFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link RefframeworkPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static RefframeworkPackage init() {
		if (isInited) return (RefframeworkPackage)EPackage.Registry.INSTANCE.getEPackage(RefframeworkPackage.eNS_URI);

		// Obtain or create and register package
		RefframeworkPackageImpl theRefframeworkPackage = (RefframeworkPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof RefframeworkPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new RefframeworkPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		MappingPackage.eINSTANCE.eClass();
		PropertyPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theRefframeworkPackage.createPackageContents();

		// Initialize created meta-data
		theRefframeworkPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRefframeworkPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(RefframeworkPackage.eNS_URI, theRefframeworkPackage);
		return theRefframeworkPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRefFramework() {
		return refFrameworkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefFramework_Scope() {
		return (EAttribute)refFrameworkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefFramework_Rev() {
		return (EAttribute)refFrameworkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefFramework_Purpose() {
		return (EAttribute)refFrameworkEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefFramework_Publisher() {
		return (EAttribute)refFrameworkEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefFramework_Issued() {
		return (EAttribute)refFrameworkEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefFramework_OwnedActivities() {
		return (EReference)refFrameworkEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefFramework_OwnedArtefact() {
		return (EReference)refFrameworkEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefFramework_OwnedRequirement() {
		return (EReference)refFrameworkEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefFramework_OwnedApplicLevel() {
		return (EReference)refFrameworkEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefFramework_OwnedCriticLevel() {
		return (EReference)refFrameworkEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefFramework_OwnedRole() {
		return (EReference)refFrameworkEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefFramework_OwnedTechnique() {
		return (EReference)refFrameworkEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRefRequirement() {
		return refRequirementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefRequirement_Reference() {
		return (EAttribute)refRequirementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefRequirement_Assumptions() {
		return (EAttribute)refRequirementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefRequirement_Rationale() {
		return (EAttribute)refRequirementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefRequirement_Image() {
		return (EAttribute)refRequirementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefRequirement_Annotations() {
		return (EAttribute)refRequirementEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefRequirement_OwnedRel() {
		return (EReference)refRequirementEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefRequirement_Applicability() {
		return (EReference)refRequirementEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefRequirement_SubRequirement() {
		return (EReference)refRequirementEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRefArtefact() {
		return refArtefactEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefArtefact_Reference() {
		return (EAttribute)refArtefactEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefArtefact_ConstrainingRequirement() {
		return (EReference)refArtefactEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefArtefact_ApplicableTechnique() {
		return (EReference)refArtefactEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefArtefact_OwnedRel() {
		return (EReference)refArtefactEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefArtefact_Property() {
		return (EReference)refArtefactEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRefActivity() {
		return refActivityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefActivity_Objective() {
		return (EAttribute)refActivityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefActivity_Scope() {
		return (EAttribute)refActivityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefActivity_RequiredArtefact() {
		return (EReference)refActivityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefActivity_ProducedArtefact() {
		return (EReference)refActivityEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefActivity_SubActivity() {
		return (EReference)refActivityEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefActivity_PrecedingActivity() {
		return (EReference)refActivityEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefActivity_OwnedRequirement() {
		return (EReference)refActivityEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefActivity_Role() {
		return (EReference)refActivityEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefActivity_ApplicableTechnique() {
		return (EReference)refActivityEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefActivity_OwnedRel() {
		return (EReference)refActivityEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefActivity_Applicability() {
		return (EReference)refActivityEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRefRequirementRel() {
		return refRequirementRelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefRequirementRel_Target() {
		return (EReference)refRequirementRelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefRequirementRel_Source() {
		return (EReference)refRequirementRelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefRequirementRel_Type() {
		return (EAttribute)refRequirementRelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRefRole() {
		return refRoleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRefApplicabilityLevel() {
		return refApplicabilityLevelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRefCriticalityLevel() {
		return refCriticalityLevelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRefTechnique() {
		return refTechniqueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefTechnique_CriticApplic() {
		return (EReference)refTechniqueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefTechnique_Aim() {
		return (EAttribute)refTechniqueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRefArtefactRel() {
		return refArtefactRelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefArtefactRel_MaxMultiplicitySource() {
		return (EAttribute)refArtefactRelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefArtefactRel_MinMultiplicitySource() {
		return (EAttribute)refArtefactRelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefArtefactRel_MaxMultiplicityTarget() {
		return (EAttribute)refArtefactRelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefArtefactRel_MinMultiplicityTarget() {
		return (EAttribute)refArtefactRelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefArtefactRel_ModificationEffect() {
		return (EAttribute)refArtefactRelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefArtefactRel_RevocationEffect() {
		return (EAttribute)refArtefactRelEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefArtefactRel_Source() {
		return (EReference)refArtefactRelEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefArtefactRel_Target() {
		return (EReference)refArtefactRelEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRefCriticalityApplicability() {
		return refCriticalityApplicabilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefCriticalityApplicability_ApplicLevel() {
		return (EReference)refCriticalityApplicabilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefCriticalityApplicability_CriticLevel() {
		return (EReference)refCriticalityApplicabilityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefCriticalityApplicability_Comment() {
		return (EAttribute)refCriticalityApplicabilityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRefActivityRel() {
		return refActivityRelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefActivityRel_Type() {
		return (EAttribute)refActivityRelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefActivityRel_Source() {
		return (EReference)refActivityRelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefActivityRel_Target() {
		return (EReference)refActivityRelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRefAssurableElement() {
		return refAssurableElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefAssurableElement_Equivalence() {
		return (EReference)refAssurableElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRefIndependencyLevel() {
		return refIndependencyLevelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRefRecommendationLevel() {
		return refRecommendationLevelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRefControlCategory() {
		return refControlCategoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRefApplicability() {
		return refApplicabilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefApplicability_ApplicCritic() {
		return (EReference)refApplicabilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefApplicability_Comments() {
		return (EAttribute)refApplicabilityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefApplicability_ApplicTarget() {
		return (EReference)refApplicabilityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefApplicability_OwnedRel() {
		return (EReference)refApplicabilityEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRefApplicabilityRel() {
		return refApplicabilityRelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRefApplicabilityRel_Type() {
		return (EAttribute)refApplicabilityRelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefApplicabilityRel_Source() {
		return (EReference)refApplicabilityRelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefApplicabilityRel_Target() {
		return (EReference)refApplicabilityRelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRefEquivalenceMap() {
		return refEquivalenceMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRefEquivalenceMap_Target() {
		return (EReference)refEquivalenceMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefframeworkFactory getRefframeworkFactory() {
		return (RefframeworkFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		refFrameworkEClass = createEClass(REF_FRAMEWORK);
		createEAttribute(refFrameworkEClass, REF_FRAMEWORK__SCOPE);
		createEAttribute(refFrameworkEClass, REF_FRAMEWORK__REV);
		createEAttribute(refFrameworkEClass, REF_FRAMEWORK__PURPOSE);
		createEAttribute(refFrameworkEClass, REF_FRAMEWORK__PUBLISHER);
		createEAttribute(refFrameworkEClass, REF_FRAMEWORK__ISSUED);
		createEReference(refFrameworkEClass, REF_FRAMEWORK__OWNED_ACTIVITIES);
		createEReference(refFrameworkEClass, REF_FRAMEWORK__OWNED_ARTEFACT);
		createEReference(refFrameworkEClass, REF_FRAMEWORK__OWNED_REQUIREMENT);
		createEReference(refFrameworkEClass, REF_FRAMEWORK__OWNED_APPLIC_LEVEL);
		createEReference(refFrameworkEClass, REF_FRAMEWORK__OWNED_CRITIC_LEVEL);
		createEReference(refFrameworkEClass, REF_FRAMEWORK__OWNED_ROLE);
		createEReference(refFrameworkEClass, REF_FRAMEWORK__OWNED_TECHNIQUE);

		refRequirementEClass = createEClass(REF_REQUIREMENT);
		createEAttribute(refRequirementEClass, REF_REQUIREMENT__REFERENCE);
		createEAttribute(refRequirementEClass, REF_REQUIREMENT__ASSUMPTIONS);
		createEAttribute(refRequirementEClass, REF_REQUIREMENT__RATIONALE);
		createEAttribute(refRequirementEClass, REF_REQUIREMENT__IMAGE);
		createEAttribute(refRequirementEClass, REF_REQUIREMENT__ANNOTATIONS);
		createEReference(refRequirementEClass, REF_REQUIREMENT__OWNED_REL);
		createEReference(refRequirementEClass, REF_REQUIREMENT__APPLICABILITY);
		createEReference(refRequirementEClass, REF_REQUIREMENT__SUB_REQUIREMENT);

		refArtefactEClass = createEClass(REF_ARTEFACT);
		createEAttribute(refArtefactEClass, REF_ARTEFACT__REFERENCE);
		createEReference(refArtefactEClass, REF_ARTEFACT__CONSTRAINING_REQUIREMENT);
		createEReference(refArtefactEClass, REF_ARTEFACT__APPLICABLE_TECHNIQUE);
		createEReference(refArtefactEClass, REF_ARTEFACT__OWNED_REL);
		createEReference(refArtefactEClass, REF_ARTEFACT__PROPERTY);

		refActivityEClass = createEClass(REF_ACTIVITY);
		createEAttribute(refActivityEClass, REF_ACTIVITY__OBJECTIVE);
		createEAttribute(refActivityEClass, REF_ACTIVITY__SCOPE);
		createEReference(refActivityEClass, REF_ACTIVITY__REQUIRED_ARTEFACT);
		createEReference(refActivityEClass, REF_ACTIVITY__PRODUCED_ARTEFACT);
		createEReference(refActivityEClass, REF_ACTIVITY__SUB_ACTIVITY);
		createEReference(refActivityEClass, REF_ACTIVITY__PRECEDING_ACTIVITY);
		createEReference(refActivityEClass, REF_ACTIVITY__OWNED_REQUIREMENT);
		createEReference(refActivityEClass, REF_ACTIVITY__ROLE);
		createEReference(refActivityEClass, REF_ACTIVITY__APPLICABLE_TECHNIQUE);
		createEReference(refActivityEClass, REF_ACTIVITY__OWNED_REL);
		createEReference(refActivityEClass, REF_ACTIVITY__APPLICABILITY);

		refRequirementRelEClass = createEClass(REF_REQUIREMENT_REL);
		createEReference(refRequirementRelEClass, REF_REQUIREMENT_REL__TARGET);
		createEReference(refRequirementRelEClass, REF_REQUIREMENT_REL__SOURCE);
		createEAttribute(refRequirementRelEClass, REF_REQUIREMENT_REL__TYPE);

		refRoleEClass = createEClass(REF_ROLE);

		refApplicabilityLevelEClass = createEClass(REF_APPLICABILITY_LEVEL);

		refCriticalityLevelEClass = createEClass(REF_CRITICALITY_LEVEL);

		refTechniqueEClass = createEClass(REF_TECHNIQUE);
		createEReference(refTechniqueEClass, REF_TECHNIQUE__CRITIC_APPLIC);
		createEAttribute(refTechniqueEClass, REF_TECHNIQUE__AIM);

		refArtefactRelEClass = createEClass(REF_ARTEFACT_REL);
		createEAttribute(refArtefactRelEClass, REF_ARTEFACT_REL__MAX_MULTIPLICITY_SOURCE);
		createEAttribute(refArtefactRelEClass, REF_ARTEFACT_REL__MIN_MULTIPLICITY_SOURCE);
		createEAttribute(refArtefactRelEClass, REF_ARTEFACT_REL__MAX_MULTIPLICITY_TARGET);
		createEAttribute(refArtefactRelEClass, REF_ARTEFACT_REL__MIN_MULTIPLICITY_TARGET);
		createEAttribute(refArtefactRelEClass, REF_ARTEFACT_REL__MODIFICATION_EFFECT);
		createEAttribute(refArtefactRelEClass, REF_ARTEFACT_REL__REVOCATION_EFFECT);
		createEReference(refArtefactRelEClass, REF_ARTEFACT_REL__SOURCE);
		createEReference(refArtefactRelEClass, REF_ARTEFACT_REL__TARGET);

		refCriticalityApplicabilityEClass = createEClass(REF_CRITICALITY_APPLICABILITY);
		createEReference(refCriticalityApplicabilityEClass, REF_CRITICALITY_APPLICABILITY__APPLIC_LEVEL);
		createEReference(refCriticalityApplicabilityEClass, REF_CRITICALITY_APPLICABILITY__CRITIC_LEVEL);
		createEAttribute(refCriticalityApplicabilityEClass, REF_CRITICALITY_APPLICABILITY__COMMENT);

		refActivityRelEClass = createEClass(REF_ACTIVITY_REL);
		createEAttribute(refActivityRelEClass, REF_ACTIVITY_REL__TYPE);
		createEReference(refActivityRelEClass, REF_ACTIVITY_REL__SOURCE);
		createEReference(refActivityRelEClass, REF_ACTIVITY_REL__TARGET);

		refAssurableElementEClass = createEClass(REF_ASSURABLE_ELEMENT);
		createEReference(refAssurableElementEClass, REF_ASSURABLE_ELEMENT__EQUIVALENCE);

		refIndependencyLevelEClass = createEClass(REF_INDEPENDENCY_LEVEL);

		refRecommendationLevelEClass = createEClass(REF_RECOMMENDATION_LEVEL);

		refControlCategoryEClass = createEClass(REF_CONTROL_CATEGORY);

		refApplicabilityEClass = createEClass(REF_APPLICABILITY);
		createEReference(refApplicabilityEClass, REF_APPLICABILITY__APPLIC_CRITIC);
		createEAttribute(refApplicabilityEClass, REF_APPLICABILITY__COMMENTS);
		createEReference(refApplicabilityEClass, REF_APPLICABILITY__APPLIC_TARGET);
		createEReference(refApplicabilityEClass, REF_APPLICABILITY__OWNED_REL);

		refApplicabilityRelEClass = createEClass(REF_APPLICABILITY_REL);
		createEAttribute(refApplicabilityRelEClass, REF_APPLICABILITY_REL__TYPE);
		createEReference(refApplicabilityRelEClass, REF_APPLICABILITY_REL__SOURCE);
		createEReference(refApplicabilityRelEClass, REF_APPLICABILITY_REL__TARGET);

		refEquivalenceMapEClass = createEClass(REF_EQUIVALENCE_MAP);
		createEReference(refEquivalenceMapEClass, REF_EQUIVALENCE_MAP__TARGET);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		GeneralPackage theGeneralPackage = (GeneralPackage)EPackage.Registry.INSTANCE.getEPackage(GeneralPackage.eNS_URI);
		PropertyPackage thePropertyPackage = (PropertyPackage)EPackage.Registry.INSTANCE.getEPackage(PropertyPackage.eNS_URI);
		MappingPackage theMappingPackage = (MappingPackage)EPackage.Registry.INSTANCE.getEPackage(MappingPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		refFrameworkEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		refRequirementEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		refRequirementEClass.getESuperTypes().add(this.getRefAssurableElement());
		refArtefactEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		refArtefactEClass.getESuperTypes().add(this.getRefAssurableElement());
		refActivityEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		refActivityEClass.getESuperTypes().add(this.getRefAssurableElement());
		refRoleEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		refRoleEClass.getESuperTypes().add(this.getRefAssurableElement());
		refApplicabilityLevelEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		refCriticalityLevelEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		refTechniqueEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		refTechniqueEClass.getESuperTypes().add(this.getRefAssurableElement());
		refArtefactRelEClass.getESuperTypes().add(theGeneralPackage.getDescribableElement());
		refIndependencyLevelEClass.getESuperTypes().add(this.getRefApplicabilityLevel());
		refRecommendationLevelEClass.getESuperTypes().add(this.getRefApplicabilityLevel());
		refControlCategoryEClass.getESuperTypes().add(this.getRefApplicabilityLevel());
		refApplicabilityEClass.getESuperTypes().add(theGeneralPackage.getNamedElement());
		refEquivalenceMapEClass.getESuperTypes().add(theMappingPackage.getEquivalenceMap());

		// Initialize classes, features, and operations; add parameters
		initEClass(refFrameworkEClass, RefFramework.class, "RefFramework", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRefFramework_Scope(), ecorePackage.getEString(), "scope", null, 0, 1, RefFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRefFramework_Rev(), ecorePackage.getEString(), "rev", null, 0, 1, RefFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRefFramework_Purpose(), ecorePackage.getEString(), "purpose", null, 0, 1, RefFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRefFramework_Publisher(), ecorePackage.getEString(), "publisher", null, 0, 1, RefFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRefFramework_Issued(), ecorePackage.getEDate(), "issued", null, 0, 1, RefFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefFramework_OwnedActivities(), this.getRefActivity(), null, "ownedActivities", null, 0, -1, RefFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefFramework_OwnedArtefact(), this.getRefArtefact(), null, "ownedArtefact", null, 0, -1, RefFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefFramework_OwnedRequirement(), this.getRefRequirement(), null, "ownedRequirement", null, 0, -1, RefFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefFramework_OwnedApplicLevel(), this.getRefApplicabilityLevel(), null, "ownedApplicLevel", null, 0, -1, RefFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefFramework_OwnedCriticLevel(), this.getRefCriticalityLevel(), null, "ownedCriticLevel", null, 0, -1, RefFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefFramework_OwnedRole(), this.getRefRole(), null, "ownedRole", null, 0, -1, RefFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefFramework_OwnedTechnique(), this.getRefTechnique(), null, "ownedTechnique", null, 0, -1, RefFramework.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(refRequirementEClass, RefRequirement.class, "RefRequirement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRefRequirement_Reference(), ecorePackage.getEString(), "reference", null, 0, 1, RefRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRefRequirement_Assumptions(), ecorePackage.getEString(), "assumptions", null, 0, 1, RefRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRefRequirement_Rationale(), ecorePackage.getEString(), "rationale", null, 0, 1, RefRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRefRequirement_Image(), ecorePackage.getEString(), "image", null, 0, 1, RefRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRefRequirement_Annotations(), ecorePackage.getEString(), "annotations", null, 0, 1, RefRequirement.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefRequirement_OwnedRel(), this.getRefRequirementRel(), null, "ownedRel", null, 0, -1, RefRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefRequirement_Applicability(), this.getRefApplicability(), null, "applicability", null, 0, -1, RefRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefRequirement_SubRequirement(), this.getRefRequirement(), null, "subRequirement", null, 0, -1, RefRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(refArtefactEClass, RefArtefact.class, "RefArtefact", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRefArtefact_Reference(), ecorePackage.getEString(), "reference", null, 0, 1, RefArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefArtefact_ConstrainingRequirement(), this.getRefRequirement(), null, "constrainingRequirement", null, 0, -1, RefArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefArtefact_ApplicableTechnique(), this.getRefTechnique(), null, "applicableTechnique", null, 0, -1, RefArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefArtefact_OwnedRel(), this.getRefArtefactRel(), null, "ownedRel", null, 0, -1, RefArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefArtefact_Property(), thePropertyPackage.getProperty(), null, "property", null, 0, -1, RefArtefact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(refActivityEClass, RefActivity.class, "RefActivity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRefActivity_Objective(), ecorePackage.getEString(), "objective", null, 0, 1, RefActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRefActivity_Scope(), ecorePackage.getEString(), "scope", null, 0, 1, RefActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefActivity_RequiredArtefact(), this.getRefArtefact(), null, "requiredArtefact", null, 0, -1, RefActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefActivity_ProducedArtefact(), this.getRefArtefact(), null, "producedArtefact", null, 0, -1, RefActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefActivity_SubActivity(), this.getRefActivity(), null, "subActivity", null, 0, -1, RefActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefActivity_PrecedingActivity(), this.getRefActivity(), null, "precedingActivity", null, 0, -1, RefActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefActivity_OwnedRequirement(), this.getRefRequirement(), null, "ownedRequirement", null, 0, -1, RefActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefActivity_Role(), this.getRefRole(), null, "role", null, 0, -1, RefActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefActivity_ApplicableTechnique(), this.getRefTechnique(), null, "applicableTechnique", null, 0, 1, RefActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefActivity_OwnedRel(), this.getRefActivityRel(), null, "ownedRel", null, 0, -1, RefActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefActivity_Applicability(), this.getRefApplicability(), null, "applicability", null, 0, -1, RefActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(refRequirementRelEClass, RefRequirementRel.class, "RefRequirementRel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRefRequirementRel_Target(), this.getRefRequirement(), null, "target", null, 1, 1, RefRequirementRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefRequirementRel_Source(), this.getRefRequirement(), null, "source", null, 1, 1, RefRequirementRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRefRequirementRel_Type(), theGeneralPackage.getRequirementRelKind(), "type", null, 0, 1, RefRequirementRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(refRoleEClass, RefRole.class, "RefRole", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(refApplicabilityLevelEClass, RefApplicabilityLevel.class, "RefApplicabilityLevel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(refCriticalityLevelEClass, RefCriticalityLevel.class, "RefCriticalityLevel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(refTechniqueEClass, RefTechnique.class, "RefTechnique", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRefTechnique_CriticApplic(), this.getRefCriticalityApplicability(), null, "criticApplic", null, 0, -1, RefTechnique.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRefTechnique_Aim(), ecorePackage.getEString(), "aim", null, 0, 1, RefTechnique.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(refArtefactRelEClass, RefArtefactRel.class, "RefArtefactRel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRefArtefactRel_MaxMultiplicitySource(), ecorePackage.getEInt(), "maxMultiplicitySource", null, 0, 1, RefArtefactRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRefArtefactRel_MinMultiplicitySource(), ecorePackage.getEInt(), "minMultiplicitySource", null, 0, 1, RefArtefactRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRefArtefactRel_MaxMultiplicityTarget(), ecorePackage.getEInt(), "maxMultiplicityTarget", null, 0, 1, RefArtefactRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRefArtefactRel_MinMultiplicityTarget(), ecorePackage.getEInt(), "minMultiplicityTarget", null, 0, 1, RefArtefactRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRefArtefactRel_ModificationEffect(), theGeneralPackage.getChangeEffectKind(), "modificationEffect", null, 0, 1, RefArtefactRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRefArtefactRel_RevocationEffect(), theGeneralPackage.getChangeEffectKind(), "revocationEffect", null, 0, 1, RefArtefactRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefArtefactRel_Source(), this.getRefArtefact(), null, "source", null, 1, 1, RefArtefactRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefArtefactRel_Target(), this.getRefArtefact(), null, "target", null, 1, 1, RefArtefactRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(refCriticalityApplicabilityEClass, RefCriticalityApplicability.class, "RefCriticalityApplicability", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRefCriticalityApplicability_ApplicLevel(), this.getRefApplicabilityLevel(), null, "applicLevel", null, 1, 1, RefCriticalityApplicability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefCriticalityApplicability_CriticLevel(), this.getRefCriticalityLevel(), null, "criticLevel", null, 1, 1, RefCriticalityApplicability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRefCriticalityApplicability_Comment(), ecorePackage.getEString(), "comment", null, 0, 1, RefCriticalityApplicability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(refActivityRelEClass, RefActivityRel.class, "RefActivityRel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRefActivityRel_Type(), theGeneralPackage.getActivityRelKind(), "type", null, 0, 1, RefActivityRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefActivityRel_Source(), this.getRefActivity(), null, "source", null, 1, 1, RefActivityRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefActivityRel_Target(), this.getRefActivity(), null, "target", null, 1, 1, RefActivityRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(refAssurableElementEClass, RefAssurableElement.class, "RefAssurableElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRefAssurableElement_Equivalence(), this.getRefEquivalenceMap(), null, "equivalence", null, 0, -1, RefAssurableElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(refIndependencyLevelEClass, RefIndependencyLevel.class, "RefIndependencyLevel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(refRecommendationLevelEClass, RefRecommendationLevel.class, "RefRecommendationLevel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(refControlCategoryEClass, RefControlCategory.class, "RefControlCategory", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(refApplicabilityEClass, RefApplicability.class, "RefApplicability", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRefApplicability_ApplicCritic(), this.getRefCriticalityApplicability(), null, "applicCritic", null, 0, -1, RefApplicability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRefApplicability_Comments(), ecorePackage.getEString(), "comments", null, 0, 1, RefApplicability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefApplicability_ApplicTarget(), this.getRefAssurableElement(), null, "applicTarget", null, 0, 1, RefApplicability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefApplicability_OwnedRel(), this.getRefApplicabilityRel(), null, "ownedRel", null, 0, -1, RefApplicability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(refApplicabilityRelEClass, RefApplicabilityRel.class, "RefApplicabilityRel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRefApplicabilityRel_Type(), theGeneralPackage.getApplicabilityKind(), "type", null, 0, 1, RefApplicabilityRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefApplicabilityRel_Source(), this.getRefApplicability(), null, "source", null, 1, 1, RefApplicabilityRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRefApplicabilityRel_Target(), this.getRefApplicability(), null, "target", null, 1, 1, RefApplicabilityRel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(refEquivalenceMapEClass, RefEquivalenceMap.class, "RefEquivalenceMap", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRefEquivalenceMap_Target(), this.getRefAssurableElement(), null, "target", null, 0, -1, RefEquivalenceMap.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// gmf
		createGmfAnnotations();
		// gmf.diagram
		createGmf_1Annotations();
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
		// gmf.node
		createGmf_2Annotations();
		// gmf.link
		createGmf_3Annotations();
		// gmf.compartment
		createGmf_4Annotations();
	}

	/**
	 * Initializes the annotations for <b>gmf</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGmfAnnotations() {
		String source = "gmf";		
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
		   });										
	}

	/**
	 * Initializes the annotations for <b>gmf.diagram</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGmf_1Annotations() {
		String source = "gmf.diagram";			
		addAnnotation
		  (refFrameworkEClass, 
		   source, 
		   new String[] {
		   });									
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";				
		addAnnotation
		  (getRefRequirement_Annotations(), 
		   source, 
		   new String[] {
		   });								
	}

	/**
	 * Initializes the annotations for <b>gmf.node</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGmf_2Annotations() {
		String source = "gmf.node";					
		addAnnotation
		  (refArtefactEClass, 
		   source, 
		   new String[] {
			 "label", "name",
			 "figure", "figures.ArtefactFigure",
			 "label.icon", "false",
			 "label.placement", "external"
		   });		
		addAnnotation
		  (refActivityEClass, 
		   source, 
		   new String[] {
			 "label", "name",
			 "border.color", "0,0,0",
			 "border.width", "2"
		   });							
		addAnnotation
		  (refRoleEClass, 
		   source, 
		   new String[] {
			 "label", "name",
			 "figure", "figures.RefRoleFigure",
			 "label.icon", "false",
			 "label.placement", "external"
		   });
	}

	/**
	 * Initializes the annotations for <b>gmf.link</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGmf_3Annotations() {
		String source = "gmf.link";							
		addAnnotation
		  (getRefActivity_RequiredArtefact(), 
		   source, 
		   new String[] {
			 "color", "255,0,0",
			 "source.decoration", "closedarrow",
			 "style", "dash",
			 "tool.small.bundle", "org.eclipse.opencert.pkm.refframework",
			 "tool.small.path", "icons/Require.gif"
		   });		
		addAnnotation
		  (getRefActivity_ProducedArtefact(), 
		   source, 
		   new String[] {
			 "color", "0,255,0",
			 "target.decoration", "filledclosedarrow",
			 "style", "solid",
			 "tool.small.bundle", "org.eclipse.opencert.pkm.refframework",
			 "tool.small.path", "icons/Produce.gif"
		   });			
		addAnnotation
		  (getRefActivity_PrecedingActivity(), 
		   source, 
		   new String[] {
			 "source.decoration", "arrow",
			 "tool.small.bundle", "org.eclipse.opencert.pkm.refframework",
			 "tool.small.path", "icons/Precedence.gif"
		   });		
		addAnnotation
		  (getRefActivity_Role(), 
		   source, 
		   new String[] {
			 "color", "0,0,255",
			 "source.decoration", "closedarrow",
			 "style", "dash",
			 "tool.small.bundle", "org.eclipse.opencert.pkm.refframework",
			 "tool.small.path", "icons/Executing.gif"
		   });	
	}

	/**
	 * Initializes the annotations for <b>gmf.compartment</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGmf_4Annotations() {
		String source = "gmf.compartment";									
		addAnnotation
		  (getRefActivity_SubActivity(), 
		   source, 
		   new String[] {
		   });			
	}

} //RefframeworkPackageImpl
