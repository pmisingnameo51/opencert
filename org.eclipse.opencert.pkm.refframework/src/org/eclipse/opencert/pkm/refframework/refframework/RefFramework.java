/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pkm.refframework.refframework;

import java.util.Date;

import org.eclipse.emf.common.util.EList;
import org.eclipse.opencert.infra.general.general.DescribableElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ref Framework</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getScope <em>Scope</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getRev <em>Rev</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getPurpose <em>Purpose</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getPublisher <em>Publisher</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getIssued <em>Issued</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getOwnedActivities <em>Owned Activities</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getOwnedArtefact <em>Owned Artefact</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getOwnedRequirement <em>Owned Requirement</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getOwnedApplicLevel <em>Owned Applic Level</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getOwnedCriticLevel <em>Owned Critic Level</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getOwnedRole <em>Owned Role</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getOwnedTechnique <em>Owned Technique</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefFramework()
 * @model
 * @generated
 */
public interface RefFramework extends DescribableElement {
	/**
	 * Returns the value of the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope</em>' attribute.
	 * @see #setScope(String)
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefFramework_Scope()
	 * @model
	 * @generated
	 */
	String getScope();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getScope <em>Scope</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scope</em>' attribute.
	 * @see #getScope()
	 * @generated
	 */
	void setScope(String value);

	/**
	 * Returns the value of the '<em><b>Rev</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rev</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rev</em>' attribute.
	 * @see #setRev(String)
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefFramework_Rev()
	 * @model
	 * @generated
	 */
	String getRev();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getRev <em>Rev</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rev</em>' attribute.
	 * @see #getRev()
	 * @generated
	 */
	void setRev(String value);

	/**
	 * Returns the value of the '<em><b>Purpose</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Purpose</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Purpose</em>' attribute.
	 * @see #setPurpose(String)
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefFramework_Purpose()
	 * @model
	 * @generated
	 */
	String getPurpose();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getPurpose <em>Purpose</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Purpose</em>' attribute.
	 * @see #getPurpose()
	 * @generated
	 */
	void setPurpose(String value);

	/**
	 * Returns the value of the '<em><b>Publisher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Publisher</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Publisher</em>' attribute.
	 * @see #setPublisher(String)
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefFramework_Publisher()
	 * @model
	 * @generated
	 */
	String getPublisher();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getPublisher <em>Publisher</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Publisher</em>' attribute.
	 * @see #getPublisher()
	 * @generated
	 */
	void setPublisher(String value);

	/**
	 * Returns the value of the '<em><b>Issued</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Issued</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Issued</em>' attribute.
	 * @see #setIssued(Date)
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefFramework_Issued()
	 * @model
	 * @generated
	 */
	Date getIssued();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getIssued <em>Issued</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Issued</em>' attribute.
	 * @see #getIssued()
	 * @generated
	 */
	void setIssued(Date value);

	/**
	 * Returns the value of the '<em><b>Owned Activities</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Activities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Activities</em>' containment reference list.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefFramework_OwnedActivities()
	 * @model containment="true"
	 * @generated
	 */
	EList<RefActivity> getOwnedActivities();

	/**
	 * Returns the value of the '<em><b>Owned Artefact</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pkm.refframework.refframework.RefArtefact}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Artefact</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Artefact</em>' containment reference list.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefFramework_OwnedArtefact()
	 * @model containment="true"
	 * @generated
	 */
	EList<RefArtefact> getOwnedArtefact();

	/**
	 * Returns the value of the '<em><b>Owned Requirement</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pkm.refframework.refframework.RefRequirement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Requirement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Requirement</em>' containment reference list.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefFramework_OwnedRequirement()
	 * @model containment="true"
	 * @generated
	 */
	EList<RefRequirement> getOwnedRequirement();

	/**
	 * Returns the value of the '<em><b>Owned Applic Level</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityLevel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Applic Level</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Applic Level</em>' containment reference list.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefFramework_OwnedApplicLevel()
	 * @model containment="true"
	 * @generated
	 */
	EList<RefApplicabilityLevel> getOwnedApplicLevel();

	/**
	 * Returns the value of the '<em><b>Owned Critic Level</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityLevel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Critic Level</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Critic Level</em>' containment reference list.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefFramework_OwnedCriticLevel()
	 * @model containment="true"
	 * @generated
	 */
	EList<RefCriticalityLevel> getOwnedCriticLevel();

	/**
	 * Returns the value of the '<em><b>Owned Role</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pkm.refframework.refframework.RefRole}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Role</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Role</em>' containment reference list.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefFramework_OwnedRole()
	 * @model containment="true"
	 * @generated
	 */
	EList<RefRole> getOwnedRole();

	/**
	 * Returns the value of the '<em><b>Owned Technique</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.opencert.pkm.refframework.refframework.RefTechnique}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Technique</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Technique</em>' containment reference list.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage#getRefFramework_OwnedTechnique()
	 * @model containment="true"
	 * @generated
	 */
	EList<RefTechnique> getOwnedTechnique();

} // RefFramework
