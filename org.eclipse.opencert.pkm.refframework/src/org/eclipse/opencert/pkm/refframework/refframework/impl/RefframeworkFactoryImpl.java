/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pkm.refframework.refframework.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.opencert.pkm.refframework.refframework.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RefframeworkFactoryImpl extends EFactoryImpl implements RefframeworkFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static RefframeworkFactory init() {
		try {
			RefframeworkFactory theRefframeworkFactory = (RefframeworkFactory)EPackage.Registry.INSTANCE.getEFactory(RefframeworkPackage.eNS_URI);
			if (theRefframeworkFactory != null) {
				return theRefframeworkFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new RefframeworkFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefframeworkFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case RefframeworkPackage.REF_FRAMEWORK: return (EObject)createRefFramework();
			case RefframeworkPackage.REF_REQUIREMENT: return (EObject)createRefRequirement();
			case RefframeworkPackage.REF_ARTEFACT: return (EObject)createRefArtefact();
			case RefframeworkPackage.REF_ACTIVITY: return (EObject)createRefActivity();
			case RefframeworkPackage.REF_REQUIREMENT_REL: return (EObject)createRefRequirementRel();
			case RefframeworkPackage.REF_ROLE: return (EObject)createRefRole();
			case RefframeworkPackage.REF_APPLICABILITY_LEVEL: return (EObject)createRefApplicabilityLevel();
			case RefframeworkPackage.REF_CRITICALITY_LEVEL: return (EObject)createRefCriticalityLevel();
			case RefframeworkPackage.REF_TECHNIQUE: return (EObject)createRefTechnique();
			case RefframeworkPackage.REF_ARTEFACT_REL: return (EObject)createRefArtefactRel();
			case RefframeworkPackage.REF_CRITICALITY_APPLICABILITY: return (EObject)createRefCriticalityApplicability();
			case RefframeworkPackage.REF_ACTIVITY_REL: return (EObject)createRefActivityRel();
			case RefframeworkPackage.REF_INDEPENDENCY_LEVEL: return (EObject)createRefIndependencyLevel();
			case RefframeworkPackage.REF_RECOMMENDATION_LEVEL: return (EObject)createRefRecommendationLevel();
			case RefframeworkPackage.REF_CONTROL_CATEGORY: return (EObject)createRefControlCategory();
			case RefframeworkPackage.REF_APPLICABILITY: return (EObject)createRefApplicability();
			case RefframeworkPackage.REF_APPLICABILITY_REL: return (EObject)createRefApplicabilityRel();
			case RefframeworkPackage.REF_EQUIVALENCE_MAP: return (EObject)createRefEquivalenceMap();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefFramework createRefFramework() {
		RefFrameworkImpl refFramework = new RefFrameworkImpl();
		return refFramework;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefRequirement createRefRequirement() {
		RefRequirementImpl refRequirement = new RefRequirementImpl();
		return refRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefArtefact createRefArtefact() {
		RefArtefactImpl refArtefact = new RefArtefactImpl();
		return refArtefact;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefActivity createRefActivity() {
		RefActivityImpl refActivity = new RefActivityImpl();
		return refActivity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefRequirementRel createRefRequirementRel() {
		RefRequirementRelImpl refRequirementRel = new RefRequirementRelImpl();
		return refRequirementRel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefRole createRefRole() {
		RefRoleImpl refRole = new RefRoleImpl();
		return refRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefApplicabilityLevel createRefApplicabilityLevel() {
		RefApplicabilityLevelImpl refApplicabilityLevel = new RefApplicabilityLevelImpl();
		return refApplicabilityLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefCriticalityLevel createRefCriticalityLevel() {
		RefCriticalityLevelImpl refCriticalityLevel = new RefCriticalityLevelImpl();
		return refCriticalityLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefTechnique createRefTechnique() {
		RefTechniqueImpl refTechnique = new RefTechniqueImpl();
		return refTechnique;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefArtefactRel createRefArtefactRel() {
		RefArtefactRelImpl refArtefactRel = new RefArtefactRelImpl();
		return refArtefactRel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefCriticalityApplicability createRefCriticalityApplicability() {
		RefCriticalityApplicabilityImpl refCriticalityApplicability = new RefCriticalityApplicabilityImpl();
		return refCriticalityApplicability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefActivityRel createRefActivityRel() {
		RefActivityRelImpl refActivityRel = new RefActivityRelImpl();
		return refActivityRel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefIndependencyLevel createRefIndependencyLevel() {
		RefIndependencyLevelImpl refIndependencyLevel = new RefIndependencyLevelImpl();
		return refIndependencyLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefRecommendationLevel createRefRecommendationLevel() {
		RefRecommendationLevelImpl refRecommendationLevel = new RefRecommendationLevelImpl();
		return refRecommendationLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefControlCategory createRefControlCategory() {
		RefControlCategoryImpl refControlCategory = new RefControlCategoryImpl();
		return refControlCategory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefApplicability createRefApplicability() {
		RefApplicabilityImpl refApplicability = new RefApplicabilityImpl();
		return refApplicability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefApplicabilityRel createRefApplicabilityRel() {
		RefApplicabilityRelImpl refApplicabilityRel = new RefApplicabilityRelImpl();
		return refApplicabilityRel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefEquivalenceMap createRefEquivalenceMap() {
		RefEquivalenceMapImpl refEquivalenceMap = new RefEquivalenceMapImpl();
		return refEquivalenceMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefframeworkPackage getRefframeworkPackage() {
		return (RefframeworkPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static RefframeworkPackage getPackage() {
		return RefframeworkPackage.eINSTANCE;
	}

} //RefframeworkFactoryImpl
