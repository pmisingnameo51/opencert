/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pkm.refframework.refframework.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.opencert.infra.general.general.impl.DescribableElementImpl;
import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;
import org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityApplicability;
import org.eclipse.opencert.pkm.refframework.refframework.RefEquivalenceMap;
import org.eclipse.opencert.pkm.refframework.refframework.RefTechnique;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ref Technique</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefTechniqueImpl#getEquivalence <em>Equivalence</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefTechniqueImpl#getCriticApplic <em>Critic Applic</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefTechniqueImpl#getAim <em>Aim</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RefTechniqueImpl extends DescribableElementImpl implements RefTechnique {
	/**
	 * The default value of the '{@link #getAim() <em>Aim</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAim()
	 * @generated
	 * @ordered
	 */
	protected static final String AIM_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RefTechniqueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RefframeworkPackage.Literals.REF_TECHNIQUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefEquivalenceMap> getEquivalence() {
		return (EList<RefEquivalenceMap>)eDynamicGet(RefframeworkPackage.REF_TECHNIQUE__EQUIVALENCE, RefframeworkPackage.Literals.REF_ASSURABLE_ELEMENT__EQUIVALENCE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefCriticalityApplicability> getCriticApplic() {
		return (EList<RefCriticalityApplicability>)eDynamicGet(RefframeworkPackage.REF_TECHNIQUE__CRITIC_APPLIC, RefframeworkPackage.Literals.REF_TECHNIQUE__CRITIC_APPLIC, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAim() {
		return (String)eDynamicGet(RefframeworkPackage.REF_TECHNIQUE__AIM, RefframeworkPackage.Literals.REF_TECHNIQUE__AIM, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAim(String newAim) {
		eDynamicSet(RefframeworkPackage.REF_TECHNIQUE__AIM, RefframeworkPackage.Literals.REF_TECHNIQUE__AIM, newAim);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RefframeworkPackage.REF_TECHNIQUE__EQUIVALENCE:
				return ((InternalEList<?>)getEquivalence()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RefframeworkPackage.REF_TECHNIQUE__EQUIVALENCE:
				return getEquivalence();
			case RefframeworkPackage.REF_TECHNIQUE__CRITIC_APPLIC:
				return getCriticApplic();
			case RefframeworkPackage.REF_TECHNIQUE__AIM:
				return getAim();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RefframeworkPackage.REF_TECHNIQUE__EQUIVALENCE:
				getEquivalence().clear();
				getEquivalence().addAll((Collection<? extends RefEquivalenceMap>)newValue);
				return;
			case RefframeworkPackage.REF_TECHNIQUE__CRITIC_APPLIC:
				getCriticApplic().clear();
				getCriticApplic().addAll((Collection<? extends RefCriticalityApplicability>)newValue);
				return;
			case RefframeworkPackage.REF_TECHNIQUE__AIM:
				setAim((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RefframeworkPackage.REF_TECHNIQUE__EQUIVALENCE:
				getEquivalence().clear();
				return;
			case RefframeworkPackage.REF_TECHNIQUE__CRITIC_APPLIC:
				getCriticApplic().clear();
				return;
			case RefframeworkPackage.REF_TECHNIQUE__AIM:
				setAim(AIM_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RefframeworkPackage.REF_TECHNIQUE__EQUIVALENCE:
				return !getEquivalence().isEmpty();
			case RefframeworkPackage.REF_TECHNIQUE__CRITIC_APPLIC:
				return !getCriticApplic().isEmpty();
			case RefframeworkPackage.REF_TECHNIQUE__AIM:
				return AIM_EDEFAULT == null ? getAim() != null : !AIM_EDEFAULT.equals(getAim());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == RefAssurableElement.class) {
			switch (derivedFeatureID) {
				case RefframeworkPackage.REF_TECHNIQUE__EQUIVALENCE: return RefframeworkPackage.REF_ASSURABLE_ELEMENT__EQUIVALENCE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == RefAssurableElement.class) {
			switch (baseFeatureID) {
				case RefframeworkPackage.REF_ASSURABLE_ELEMENT__EQUIVALENCE: return RefframeworkPackage.REF_TECHNIQUE__EQUIVALENCE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //RefTechniqueImpl
