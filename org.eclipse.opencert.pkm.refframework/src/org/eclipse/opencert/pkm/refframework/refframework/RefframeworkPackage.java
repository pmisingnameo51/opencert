/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pkm.refframework.refframework;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.opencert.infra.general.general.GeneralPackage;
import org.eclipse.opencert.infra.mappings.mapping.MappingPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.opencert.pkm.refframework.refframework.RefframeworkFactory
 * @model kind="package"
 * @generated
 */
public interface RefframeworkPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "refframework";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://refframework/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "refframework";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RefframeworkPackage eINSTANCE = org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefFrameworkImpl <em>Ref Framework</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefFrameworkImpl
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefFramework()
	 * @generated
	 */
	int REF_FRAMEWORK = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_FRAMEWORK__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_FRAMEWORK__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_FRAMEWORK__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_FRAMEWORK__SCOPE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Rev</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_FRAMEWORK__REV = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Purpose</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_FRAMEWORK__PURPOSE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Publisher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_FRAMEWORK__PUBLISHER = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Issued</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_FRAMEWORK__ISSUED = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Owned Activities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_FRAMEWORK__OWNED_ACTIVITIES = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Owned Artefact</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_FRAMEWORK__OWNED_ARTEFACT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Owned Requirement</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_FRAMEWORK__OWNED_REQUIREMENT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Owned Applic Level</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_FRAMEWORK__OWNED_APPLIC_LEVEL = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Owned Critic Level</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_FRAMEWORK__OWNED_CRITIC_LEVEL = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Owned Role</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_FRAMEWORK__OWNED_ROLE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Owned Technique</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_FRAMEWORK__OWNED_TECHNIQUE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The number of structural features of the '<em>Ref Framework</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_FRAMEWORK_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 12;

	/**
	 * The number of operations of the '<em>Ref Framework</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_FRAMEWORK_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefRequirementImpl <em>Ref Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefRequirementImpl
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefRequirement()
	 * @generated
	 */
	int REF_REQUIREMENT = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_REQUIREMENT__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_REQUIREMENT__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_REQUIREMENT__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Equivalence</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_REQUIREMENT__EQUIVALENCE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_REQUIREMENT__REFERENCE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Assumptions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_REQUIREMENT__ASSUMPTIONS = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Rationale</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_REQUIREMENT__RATIONALE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_REQUIREMENT__IMAGE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_REQUIREMENT__ANNOTATIONS = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Owned Rel</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_REQUIREMENT__OWNED_REL = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Applicability</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_REQUIREMENT__APPLICABILITY = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Sub Requirement</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_REQUIREMENT__SUB_REQUIREMENT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>Ref Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_REQUIREMENT_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The number of operations of the '<em>Ref Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_REQUIREMENT_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefArtefactImpl <em>Ref Artefact</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefArtefactImpl
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefArtefact()
	 * @generated
	 */
	int REF_ARTEFACT = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Equivalence</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT__EQUIVALENCE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT__REFERENCE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Constraining Requirement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT__CONSTRAINING_REQUIREMENT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Applicable Technique</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT__APPLICABLE_TECHNIQUE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Owned Rel</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT__OWNED_REL = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT__PROPERTY = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Ref Artefact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Ref Artefact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefActivityImpl <em>Ref Activity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefActivityImpl
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefActivity()
	 * @generated
	 */
	int REF_ACTIVITY = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ACTIVITY__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ACTIVITY__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ACTIVITY__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Equivalence</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ACTIVITY__EQUIVALENCE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Objective</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ACTIVITY__OBJECTIVE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ACTIVITY__SCOPE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Required Artefact</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ACTIVITY__REQUIRED_ARTEFACT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Produced Artefact</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ACTIVITY__PRODUCED_ARTEFACT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Sub Activity</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ACTIVITY__SUB_ACTIVITY = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Preceding Activity</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ACTIVITY__PRECEDING_ACTIVITY = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Owned Requirement</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ACTIVITY__OWNED_REQUIREMENT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ACTIVITY__ROLE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Applicable Technique</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ACTIVITY__APPLICABLE_TECHNIQUE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Owned Rel</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ACTIVITY__OWNED_REL = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Applicability</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ACTIVITY__APPLICABILITY = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The number of structural features of the '<em>Ref Activity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ACTIVITY_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 12;

	/**
	 * The number of operations of the '<em>Ref Activity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ACTIVITY_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefRequirementRelImpl <em>Ref Requirement Rel</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefRequirementRelImpl
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefRequirementRel()
	 * @generated
	 */
	int REF_REQUIREMENT_REL = 4;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_REQUIREMENT_REL__TARGET = 0;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_REQUIREMENT_REL__SOURCE = 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_REQUIREMENT_REL__TYPE = 2;

	/**
	 * The number of structural features of the '<em>Ref Requirement Rel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_REQUIREMENT_REL_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Ref Requirement Rel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_REQUIREMENT_REL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefRoleImpl <em>Ref Role</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefRoleImpl
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefRole()
	 * @generated
	 */
	int REF_ROLE = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ROLE__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ROLE__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ROLE__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Equivalence</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ROLE__EQUIVALENCE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ref Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ROLE_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Ref Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ROLE_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefApplicabilityLevelImpl <em>Ref Applicability Level</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefApplicabilityLevelImpl
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefApplicabilityLevel()
	 * @generated
	 */
	int REF_APPLICABILITY_LEVEL = 6;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_APPLICABILITY_LEVEL__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_APPLICABILITY_LEVEL__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_APPLICABILITY_LEVEL__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The number of structural features of the '<em>Ref Applicability Level</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_APPLICABILITY_LEVEL_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ref Applicability Level</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_APPLICABILITY_LEVEL_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefCriticalityLevelImpl <em>Ref Criticality Level</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefCriticalityLevelImpl
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefCriticalityLevel()
	 * @generated
	 */
	int REF_CRITICALITY_LEVEL = 7;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_CRITICALITY_LEVEL__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_CRITICALITY_LEVEL__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_CRITICALITY_LEVEL__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The number of structural features of the '<em>Ref Criticality Level</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_CRITICALITY_LEVEL_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ref Criticality Level</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_CRITICALITY_LEVEL_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefTechniqueImpl <em>Ref Technique</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefTechniqueImpl
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefTechnique()
	 * @generated
	 */
	int REF_TECHNIQUE = 8;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_TECHNIQUE__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_TECHNIQUE__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_TECHNIQUE__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Equivalence</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_TECHNIQUE__EQUIVALENCE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Critic Applic</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_TECHNIQUE__CRITIC_APPLIC = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Aim</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_TECHNIQUE__AIM = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Ref Technique</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_TECHNIQUE_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Ref Technique</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_TECHNIQUE_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefArtefactRelImpl <em>Ref Artefact Rel</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefArtefactRelImpl
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefArtefactRel()
	 * @generated
	 */
	int REF_ARTEFACT_REL = 9;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT_REL__ID = GeneralPackage.DESCRIBABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT_REL__NAME = GeneralPackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT_REL__DESCRIPTION = GeneralPackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Max Multiplicity Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT_REL__MAX_MULTIPLICITY_SOURCE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Min Multiplicity Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT_REL__MIN_MULTIPLICITY_SOURCE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Max Multiplicity Target</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT_REL__MAX_MULTIPLICITY_TARGET = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Min Multiplicity Target</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT_REL__MIN_MULTIPLICITY_TARGET = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Modification Effect</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT_REL__MODIFICATION_EFFECT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Revocation Effect</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT_REL__REVOCATION_EFFECT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT_REL__SOURCE = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT_REL__TARGET = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Ref Artefact Rel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT_REL_FEATURE_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>Ref Artefact Rel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ARTEFACT_REL_OPERATION_COUNT = GeneralPackage.DESCRIBABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefCriticalityApplicabilityImpl <em>Ref Criticality Applicability</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefCriticalityApplicabilityImpl
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefCriticalityApplicability()
	 * @generated
	 */
	int REF_CRITICALITY_APPLICABILITY = 10;

	/**
	 * The feature id for the '<em><b>Applic Level</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_CRITICALITY_APPLICABILITY__APPLIC_LEVEL = 0;

	/**
	 * The feature id for the '<em><b>Critic Level</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_CRITICALITY_APPLICABILITY__CRITIC_LEVEL = 1;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_CRITICALITY_APPLICABILITY__COMMENT = 2;

	/**
	 * The number of structural features of the '<em>Ref Criticality Applicability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_CRITICALITY_APPLICABILITY_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Ref Criticality Applicability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_CRITICALITY_APPLICABILITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefActivityRelImpl <em>Ref Activity Rel</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefActivityRelImpl
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefActivityRel()
	 * @generated
	 */
	int REF_ACTIVITY_REL = 11;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ACTIVITY_REL__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ACTIVITY_REL__SOURCE = 1;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ACTIVITY_REL__TARGET = 2;

	/**
	 * The number of structural features of the '<em>Ref Activity Rel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ACTIVITY_REL_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Ref Activity Rel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ACTIVITY_REL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefAssurableElementImpl <em>Ref Assurable Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefAssurableElementImpl
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefAssurableElement()
	 * @generated
	 */
	int REF_ASSURABLE_ELEMENT = 12;

	/**
	 * The feature id for the '<em><b>Equivalence</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ASSURABLE_ELEMENT__EQUIVALENCE = 0;

	/**
	 * The number of structural features of the '<em>Ref Assurable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ASSURABLE_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Ref Assurable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_ASSURABLE_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefIndependencyLevelImpl <em>Ref Independency Level</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefIndependencyLevelImpl
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefIndependencyLevel()
	 * @generated
	 */
	int REF_INDEPENDENCY_LEVEL = 13;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_INDEPENDENCY_LEVEL__ID = REF_APPLICABILITY_LEVEL__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_INDEPENDENCY_LEVEL__NAME = REF_APPLICABILITY_LEVEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_INDEPENDENCY_LEVEL__DESCRIPTION = REF_APPLICABILITY_LEVEL__DESCRIPTION;

	/**
	 * The number of structural features of the '<em>Ref Independency Level</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_INDEPENDENCY_LEVEL_FEATURE_COUNT = REF_APPLICABILITY_LEVEL_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ref Independency Level</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_INDEPENDENCY_LEVEL_OPERATION_COUNT = REF_APPLICABILITY_LEVEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefRecommendationLevelImpl <em>Ref Recommendation Level</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefRecommendationLevelImpl
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefRecommendationLevel()
	 * @generated
	 */
	int REF_RECOMMENDATION_LEVEL = 14;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_RECOMMENDATION_LEVEL__ID = REF_APPLICABILITY_LEVEL__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_RECOMMENDATION_LEVEL__NAME = REF_APPLICABILITY_LEVEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_RECOMMENDATION_LEVEL__DESCRIPTION = REF_APPLICABILITY_LEVEL__DESCRIPTION;

	/**
	 * The number of structural features of the '<em>Ref Recommendation Level</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_RECOMMENDATION_LEVEL_FEATURE_COUNT = REF_APPLICABILITY_LEVEL_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ref Recommendation Level</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_RECOMMENDATION_LEVEL_OPERATION_COUNT = REF_APPLICABILITY_LEVEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefControlCategoryImpl <em>Ref Control Category</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefControlCategoryImpl
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefControlCategory()
	 * @generated
	 */
	int REF_CONTROL_CATEGORY = 15;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_CONTROL_CATEGORY__ID = REF_APPLICABILITY_LEVEL__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_CONTROL_CATEGORY__NAME = REF_APPLICABILITY_LEVEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_CONTROL_CATEGORY__DESCRIPTION = REF_APPLICABILITY_LEVEL__DESCRIPTION;

	/**
	 * The number of structural features of the '<em>Ref Control Category</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_CONTROL_CATEGORY_FEATURE_COUNT = REF_APPLICABILITY_LEVEL_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ref Control Category</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_CONTROL_CATEGORY_OPERATION_COUNT = REF_APPLICABILITY_LEVEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefApplicabilityImpl <em>Ref Applicability</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefApplicabilityImpl
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefApplicability()
	 * @generated
	 */
	int REF_APPLICABILITY = 16;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_APPLICABILITY__ID = GeneralPackage.NAMED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_APPLICABILITY__NAME = GeneralPackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Applic Critic</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_APPLICABILITY__APPLIC_CRITIC = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Comments</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_APPLICABILITY__COMMENTS = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Applic Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_APPLICABILITY__APPLIC_TARGET = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Owned Rel</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_APPLICABILITY__OWNED_REL = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Ref Applicability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_APPLICABILITY_FEATURE_COUNT = GeneralPackage.NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Ref Applicability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_APPLICABILITY_OPERATION_COUNT = GeneralPackage.NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefApplicabilityRelImpl <em>Ref Applicability Rel</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefApplicabilityRelImpl
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefApplicabilityRel()
	 * @generated
	 */
	int REF_APPLICABILITY_REL = 17;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_APPLICABILITY_REL__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_APPLICABILITY_REL__SOURCE = 1;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_APPLICABILITY_REL__TARGET = 2;

	/**
	 * The number of structural features of the '<em>Ref Applicability Rel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_APPLICABILITY_REL_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Ref Applicability Rel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_APPLICABILITY_REL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefEquivalenceMapImpl <em>Ref Equivalence Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefEquivalenceMapImpl
	 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefEquivalenceMap()
	 * @generated
	 */
	int REF_EQUIVALENCE_MAP = 18;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_EQUIVALENCE_MAP__ID = MappingPackage.EQUIVALENCE_MAP__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_EQUIVALENCE_MAP__NAME = MappingPackage.EQUIVALENCE_MAP__NAME;

	/**
	 * The feature id for the '<em><b>Map Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_EQUIVALENCE_MAP__MAP_GROUP = MappingPackage.EQUIVALENCE_MAP__MAP_GROUP;

	/**
	 * The feature id for the '<em><b>Map Justification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_EQUIVALENCE_MAP__MAP_JUSTIFICATION = MappingPackage.EQUIVALENCE_MAP__MAP_JUSTIFICATION;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_EQUIVALENCE_MAP__TYPE = MappingPackage.EQUIVALENCE_MAP__TYPE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_EQUIVALENCE_MAP__TARGET = MappingPackage.EQUIVALENCE_MAP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ref Equivalence Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_EQUIVALENCE_MAP_FEATURE_COUNT = MappingPackage.EQUIVALENCE_MAP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Ref Equivalence Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REF_EQUIVALENCE_MAP_OPERATION_COUNT = MappingPackage.EQUIVALENCE_MAP_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework <em>Ref Framework</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ref Framework</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefFramework
	 * @generated
	 */
	EClass getRefFramework();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getScope <em>Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Scope</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getScope()
	 * @see #getRefFramework()
	 * @generated
	 */
	EAttribute getRefFramework_Scope();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getRev <em>Rev</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rev</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getRev()
	 * @see #getRefFramework()
	 * @generated
	 */
	EAttribute getRefFramework_Rev();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getPurpose <em>Purpose</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Purpose</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getPurpose()
	 * @see #getRefFramework()
	 * @generated
	 */
	EAttribute getRefFramework_Purpose();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getPublisher <em>Publisher</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Publisher</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getPublisher()
	 * @see #getRefFramework()
	 * @generated
	 */
	EAttribute getRefFramework_Publisher();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getIssued <em>Issued</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Issued</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getIssued()
	 * @see #getRefFramework()
	 * @generated
	 */
	EAttribute getRefFramework_Issued();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getOwnedActivities <em>Owned Activities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Activities</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getOwnedActivities()
	 * @see #getRefFramework()
	 * @generated
	 */
	EReference getRefFramework_OwnedActivities();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getOwnedArtefact <em>Owned Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Artefact</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getOwnedArtefact()
	 * @see #getRefFramework()
	 * @generated
	 */
	EReference getRefFramework_OwnedArtefact();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getOwnedRequirement <em>Owned Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Requirement</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getOwnedRequirement()
	 * @see #getRefFramework()
	 * @generated
	 */
	EReference getRefFramework_OwnedRequirement();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getOwnedApplicLevel <em>Owned Applic Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Applic Level</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getOwnedApplicLevel()
	 * @see #getRefFramework()
	 * @generated
	 */
	EReference getRefFramework_OwnedApplicLevel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getOwnedCriticLevel <em>Owned Critic Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Critic Level</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getOwnedCriticLevel()
	 * @see #getRefFramework()
	 * @generated
	 */
	EReference getRefFramework_OwnedCriticLevel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getOwnedRole <em>Owned Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Role</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getOwnedRole()
	 * @see #getRefFramework()
	 * @generated
	 */
	EReference getRefFramework_OwnedRole();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getOwnedTechnique <em>Owned Technique</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Technique</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefFramework#getOwnedTechnique()
	 * @see #getRefFramework()
	 * @generated
	 */
	EReference getRefFramework_OwnedTechnique();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefRequirement <em>Ref Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ref Requirement</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefRequirement
	 * @generated
	 */
	EClass getRefRequirement();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefRequirement#getReference <em>Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reference</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefRequirement#getReference()
	 * @see #getRefRequirement()
	 * @generated
	 */
	EAttribute getRefRequirement_Reference();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefRequirement#getAssumptions <em>Assumptions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assumptions</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefRequirement#getAssumptions()
	 * @see #getRefRequirement()
	 * @generated
	 */
	EAttribute getRefRequirement_Assumptions();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefRequirement#getRationale <em>Rationale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rationale</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefRequirement#getRationale()
	 * @see #getRefRequirement()
	 * @generated
	 */
	EAttribute getRefRequirement_Rationale();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefRequirement#getImage <em>Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Image</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefRequirement#getImage()
	 * @see #getRefRequirement()
	 * @generated
	 */
	EAttribute getRefRequirement_Image();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefRequirement#getAnnotations <em>Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Annotations</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefRequirement#getAnnotations()
	 * @see #getRefRequirement()
	 * @generated
	 */
	EAttribute getRefRequirement_Annotations();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefRequirement#getOwnedRel <em>Owned Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Rel</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefRequirement#getOwnedRel()
	 * @see #getRefRequirement()
	 * @generated
	 */
	EReference getRefRequirement_OwnedRel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefRequirement#getApplicability <em>Applicability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Applicability</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefRequirement#getApplicability()
	 * @see #getRefRequirement()
	 * @generated
	 */
	EReference getRefRequirement_Applicability();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefRequirement#getSubRequirement <em>Sub Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Requirement</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefRequirement#getSubRequirement()
	 * @see #getRefRequirement()
	 * @generated
	 */
	EReference getRefRequirement_SubRequirement();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefArtefact <em>Ref Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ref Artefact</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefArtefact
	 * @generated
	 */
	EClass getRefArtefact();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefArtefact#getReference <em>Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reference</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefArtefact#getReference()
	 * @see #getRefArtefact()
	 * @generated
	 */
	EAttribute getRefArtefact_Reference();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefArtefact#getConstrainingRequirement <em>Constraining Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Constraining Requirement</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefArtefact#getConstrainingRequirement()
	 * @see #getRefArtefact()
	 * @generated
	 */
	EReference getRefArtefact_ConstrainingRequirement();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefArtefact#getApplicableTechnique <em>Applicable Technique</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Applicable Technique</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefArtefact#getApplicableTechnique()
	 * @see #getRefArtefact()
	 * @generated
	 */
	EReference getRefArtefact_ApplicableTechnique();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefArtefact#getOwnedRel <em>Owned Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Rel</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefArtefact#getOwnedRel()
	 * @see #getRefArtefact()
	 * @generated
	 */
	EReference getRefArtefact_OwnedRel();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefArtefact#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Property</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefArtefact#getProperty()
	 * @see #getRefArtefact()
	 * @generated
	 */
	EReference getRefArtefact_Property();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity <em>Ref Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ref Activity</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefActivity
	 * @generated
	 */
	EClass getRefActivity();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getObjective <em>Objective</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Objective</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getObjective()
	 * @see #getRefActivity()
	 * @generated
	 */
	EAttribute getRefActivity_Objective();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getScope <em>Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Scope</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getScope()
	 * @see #getRefActivity()
	 * @generated
	 */
	EAttribute getRefActivity_Scope();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getRequiredArtefact <em>Required Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Required Artefact</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getRequiredArtefact()
	 * @see #getRefActivity()
	 * @generated
	 */
	EReference getRefActivity_RequiredArtefact();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getProducedArtefact <em>Produced Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Produced Artefact</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getProducedArtefact()
	 * @see #getRefActivity()
	 * @generated
	 */
	EReference getRefActivity_ProducedArtefact();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getSubActivity <em>Sub Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Activity</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getSubActivity()
	 * @see #getRefActivity()
	 * @generated
	 */
	EReference getRefActivity_SubActivity();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getPrecedingActivity <em>Preceding Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Preceding Activity</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getPrecedingActivity()
	 * @see #getRefActivity()
	 * @generated
	 */
	EReference getRefActivity_PrecedingActivity();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getOwnedRequirement <em>Owned Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Requirement</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getOwnedRequirement()
	 * @see #getRefActivity()
	 * @generated
	 */
	EReference getRefActivity_OwnedRequirement();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Role</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getRole()
	 * @see #getRefActivity()
	 * @generated
	 */
	EReference getRefActivity_Role();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getApplicableTechnique <em>Applicable Technique</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Applicable Technique</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getApplicableTechnique()
	 * @see #getRefActivity()
	 * @generated
	 */
	EReference getRefActivity_ApplicableTechnique();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getOwnedRel <em>Owned Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Rel</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getOwnedRel()
	 * @see #getRefActivity()
	 * @generated
	 */
	EReference getRefActivity_OwnedRel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getApplicability <em>Applicability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Applicability</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefActivity#getApplicability()
	 * @see #getRefActivity()
	 * @generated
	 */
	EReference getRefActivity_Applicability();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefRequirementRel <em>Ref Requirement Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ref Requirement Rel</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefRequirementRel
	 * @generated
	 */
	EClass getRefRequirementRel();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.pkm.refframework.refframework.RefRequirementRel#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefRequirementRel#getTarget()
	 * @see #getRefRequirementRel()
	 * @generated
	 */
	EReference getRefRequirementRel_Target();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.pkm.refframework.refframework.RefRequirementRel#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefRequirementRel#getSource()
	 * @see #getRefRequirementRel()
	 * @generated
	 */
	EReference getRefRequirementRel_Source();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefRequirementRel#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefRequirementRel#getType()
	 * @see #getRefRequirementRel()
	 * @generated
	 */
	EAttribute getRefRequirementRel_Type();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefRole <em>Ref Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ref Role</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefRole
	 * @generated
	 */
	EClass getRefRole();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityLevel <em>Ref Applicability Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ref Applicability Level</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityLevel
	 * @generated
	 */
	EClass getRefApplicabilityLevel();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityLevel <em>Ref Criticality Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ref Criticality Level</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityLevel
	 * @generated
	 */
	EClass getRefCriticalityLevel();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefTechnique <em>Ref Technique</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ref Technique</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefTechnique
	 * @generated
	 */
	EClass getRefTechnique();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefTechnique#getCriticApplic <em>Critic Applic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Critic Applic</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefTechnique#getCriticApplic()
	 * @see #getRefTechnique()
	 * @generated
	 */
	EReference getRefTechnique_CriticApplic();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefTechnique#getAim <em>Aim</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Aim</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefTechnique#getAim()
	 * @see #getRefTechnique()
	 * @generated
	 */
	EAttribute getRefTechnique_Aim();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel <em>Ref Artefact Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ref Artefact Rel</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel
	 * @generated
	 */
	EClass getRefArtefactRel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel#getMaxMultiplicitySource <em>Max Multiplicity Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Multiplicity Source</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel#getMaxMultiplicitySource()
	 * @see #getRefArtefactRel()
	 * @generated
	 */
	EAttribute getRefArtefactRel_MaxMultiplicitySource();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel#getMinMultiplicitySource <em>Min Multiplicity Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Multiplicity Source</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel#getMinMultiplicitySource()
	 * @see #getRefArtefactRel()
	 * @generated
	 */
	EAttribute getRefArtefactRel_MinMultiplicitySource();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel#getMaxMultiplicityTarget <em>Max Multiplicity Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Multiplicity Target</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel#getMaxMultiplicityTarget()
	 * @see #getRefArtefactRel()
	 * @generated
	 */
	EAttribute getRefArtefactRel_MaxMultiplicityTarget();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel#getMinMultiplicityTarget <em>Min Multiplicity Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Multiplicity Target</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel#getMinMultiplicityTarget()
	 * @see #getRefArtefactRel()
	 * @generated
	 */
	EAttribute getRefArtefactRel_MinMultiplicityTarget();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel#getModificationEffect <em>Modification Effect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Modification Effect</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel#getModificationEffect()
	 * @see #getRefArtefactRel()
	 * @generated
	 */
	EAttribute getRefArtefactRel_ModificationEffect();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel#getRevocationEffect <em>Revocation Effect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Revocation Effect</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel#getRevocationEffect()
	 * @see #getRefArtefactRel()
	 * @generated
	 */
	EAttribute getRefArtefactRel_RevocationEffect();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel#getSource()
	 * @see #getRefArtefactRel()
	 * @generated
	 */
	EReference getRefArtefactRel_Source();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefArtefactRel#getTarget()
	 * @see #getRefArtefactRel()
	 * @generated
	 */
	EReference getRefArtefactRel_Target();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityApplicability <em>Ref Criticality Applicability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ref Criticality Applicability</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityApplicability
	 * @generated
	 */
	EClass getRefCriticalityApplicability();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityApplicability#getApplicLevel <em>Applic Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Applic Level</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityApplicability#getApplicLevel()
	 * @see #getRefCriticalityApplicability()
	 * @generated
	 */
	EReference getRefCriticalityApplicability_ApplicLevel();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityApplicability#getCriticLevel <em>Critic Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Critic Level</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityApplicability#getCriticLevel()
	 * @see #getRefCriticalityApplicability()
	 * @generated
	 */
	EReference getRefCriticalityApplicability_CriticLevel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityApplicability#getComment <em>Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Comment</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityApplicability#getComment()
	 * @see #getRefCriticalityApplicability()
	 * @generated
	 */
	EAttribute getRefCriticalityApplicability_Comment();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivityRel <em>Ref Activity Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ref Activity Rel</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefActivityRel
	 * @generated
	 */
	EClass getRefActivityRel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivityRel#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefActivityRel#getType()
	 * @see #getRefActivityRel()
	 * @generated
	 */
	EAttribute getRefActivityRel_Type();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivityRel#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefActivityRel#getSource()
	 * @see #getRefActivityRel()
	 * @generated
	 */
	EReference getRefActivityRel_Source();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.pkm.refframework.refframework.RefActivityRel#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefActivityRel#getTarget()
	 * @see #getRefActivityRel()
	 * @generated
	 */
	EReference getRefActivityRel_Target();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement <em>Ref Assurable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ref Assurable Element</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement
	 * @generated
	 */
	EClass getRefAssurableElement();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement#getEquivalence <em>Equivalence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Equivalence</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement#getEquivalence()
	 * @see #getRefAssurableElement()
	 * @generated
	 */
	EReference getRefAssurableElement_Equivalence();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefIndependencyLevel <em>Ref Independency Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ref Independency Level</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefIndependencyLevel
	 * @generated
	 */
	EClass getRefIndependencyLevel();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefRecommendationLevel <em>Ref Recommendation Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ref Recommendation Level</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefRecommendationLevel
	 * @generated
	 */
	EClass getRefRecommendationLevel();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefControlCategory <em>Ref Control Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ref Control Category</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefControlCategory
	 * @generated
	 */
	EClass getRefControlCategory();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefApplicability <em>Ref Applicability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ref Applicability</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefApplicability
	 * @generated
	 */
	EClass getRefApplicability();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefApplicability#getApplicCritic <em>Applic Critic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Applic Critic</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefApplicability#getApplicCritic()
	 * @see #getRefApplicability()
	 * @generated
	 */
	EReference getRefApplicability_ApplicCritic();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefApplicability#getComments <em>Comments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Comments</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefApplicability#getComments()
	 * @see #getRefApplicability()
	 * @generated
	 */
	EAttribute getRefApplicability_Comments();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.pkm.refframework.refframework.RefApplicability#getApplicTarget <em>Applic Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Applic Target</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefApplicability#getApplicTarget()
	 * @see #getRefApplicability()
	 * @generated
	 */
	EReference getRefApplicability_ApplicTarget();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefApplicability#getOwnedRel <em>Owned Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Rel</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefApplicability#getOwnedRel()
	 * @see #getRefApplicability()
	 * @generated
	 */
	EReference getRefApplicability_OwnedRel();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityRel <em>Ref Applicability Rel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ref Applicability Rel</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityRel
	 * @generated
	 */
	EClass getRefApplicabilityRel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityRel#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityRel#getType()
	 * @see #getRefApplicabilityRel()
	 * @generated
	 */
	EAttribute getRefApplicabilityRel_Type();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityRel#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityRel#getSource()
	 * @see #getRefApplicabilityRel()
	 * @generated
	 */
	EReference getRefApplicabilityRel_Source();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityRel#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityRel#getTarget()
	 * @see #getRefApplicabilityRel()
	 * @generated
	 */
	EReference getRefApplicabilityRel_Target();

	/**
	 * Returns the meta object for class '{@link org.eclipse.opencert.pkm.refframework.refframework.RefEquivalenceMap <em>Ref Equivalence Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ref Equivalence Map</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefEquivalenceMap
	 * @generated
	 */
	EClass getRefEquivalenceMap();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.opencert.pkm.refframework.refframework.RefEquivalenceMap#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Target</em>'.
	 * @see org.eclipse.opencert.pkm.refframework.refframework.RefEquivalenceMap#getTarget()
	 * @see #getRefEquivalenceMap()
	 * @generated
	 */
	EReference getRefEquivalenceMap_Target();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RefframeworkFactory getRefframeworkFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefFrameworkImpl <em>Ref Framework</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefFrameworkImpl
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefFramework()
		 * @generated
		 */
		EClass REF_FRAMEWORK = eINSTANCE.getRefFramework();

		/**
		 * The meta object literal for the '<em><b>Scope</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_FRAMEWORK__SCOPE = eINSTANCE.getRefFramework_Scope();

		/**
		 * The meta object literal for the '<em><b>Rev</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_FRAMEWORK__REV = eINSTANCE.getRefFramework_Rev();

		/**
		 * The meta object literal for the '<em><b>Purpose</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_FRAMEWORK__PURPOSE = eINSTANCE.getRefFramework_Purpose();

		/**
		 * The meta object literal for the '<em><b>Publisher</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_FRAMEWORK__PUBLISHER = eINSTANCE.getRefFramework_Publisher();

		/**
		 * The meta object literal for the '<em><b>Issued</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_FRAMEWORK__ISSUED = eINSTANCE.getRefFramework_Issued();

		/**
		 * The meta object literal for the '<em><b>Owned Activities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_FRAMEWORK__OWNED_ACTIVITIES = eINSTANCE.getRefFramework_OwnedActivities();

		/**
		 * The meta object literal for the '<em><b>Owned Artefact</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_FRAMEWORK__OWNED_ARTEFACT = eINSTANCE.getRefFramework_OwnedArtefact();

		/**
		 * The meta object literal for the '<em><b>Owned Requirement</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_FRAMEWORK__OWNED_REQUIREMENT = eINSTANCE.getRefFramework_OwnedRequirement();

		/**
		 * The meta object literal for the '<em><b>Owned Applic Level</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_FRAMEWORK__OWNED_APPLIC_LEVEL = eINSTANCE.getRefFramework_OwnedApplicLevel();

		/**
		 * The meta object literal for the '<em><b>Owned Critic Level</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_FRAMEWORK__OWNED_CRITIC_LEVEL = eINSTANCE.getRefFramework_OwnedCriticLevel();

		/**
		 * The meta object literal for the '<em><b>Owned Role</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_FRAMEWORK__OWNED_ROLE = eINSTANCE.getRefFramework_OwnedRole();

		/**
		 * The meta object literal for the '<em><b>Owned Technique</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_FRAMEWORK__OWNED_TECHNIQUE = eINSTANCE.getRefFramework_OwnedTechnique();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefRequirementImpl <em>Ref Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefRequirementImpl
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefRequirement()
		 * @generated
		 */
		EClass REF_REQUIREMENT = eINSTANCE.getRefRequirement();

		/**
		 * The meta object literal for the '<em><b>Reference</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_REQUIREMENT__REFERENCE = eINSTANCE.getRefRequirement_Reference();

		/**
		 * The meta object literal for the '<em><b>Assumptions</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_REQUIREMENT__ASSUMPTIONS = eINSTANCE.getRefRequirement_Assumptions();

		/**
		 * The meta object literal for the '<em><b>Rationale</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_REQUIREMENT__RATIONALE = eINSTANCE.getRefRequirement_Rationale();

		/**
		 * The meta object literal for the '<em><b>Image</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_REQUIREMENT__IMAGE = eINSTANCE.getRefRequirement_Image();

		/**
		 * The meta object literal for the '<em><b>Annotations</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_REQUIREMENT__ANNOTATIONS = eINSTANCE.getRefRequirement_Annotations();

		/**
		 * The meta object literal for the '<em><b>Owned Rel</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_REQUIREMENT__OWNED_REL = eINSTANCE.getRefRequirement_OwnedRel();

		/**
		 * The meta object literal for the '<em><b>Applicability</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_REQUIREMENT__APPLICABILITY = eINSTANCE.getRefRequirement_Applicability();

		/**
		 * The meta object literal for the '<em><b>Sub Requirement</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_REQUIREMENT__SUB_REQUIREMENT = eINSTANCE.getRefRequirement_SubRequirement();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefArtefactImpl <em>Ref Artefact</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefArtefactImpl
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefArtefact()
		 * @generated
		 */
		EClass REF_ARTEFACT = eINSTANCE.getRefArtefact();

		/**
		 * The meta object literal for the '<em><b>Reference</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_ARTEFACT__REFERENCE = eINSTANCE.getRefArtefact_Reference();

		/**
		 * The meta object literal for the '<em><b>Constraining Requirement</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_ARTEFACT__CONSTRAINING_REQUIREMENT = eINSTANCE.getRefArtefact_ConstrainingRequirement();

		/**
		 * The meta object literal for the '<em><b>Applicable Technique</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_ARTEFACT__APPLICABLE_TECHNIQUE = eINSTANCE.getRefArtefact_ApplicableTechnique();

		/**
		 * The meta object literal for the '<em><b>Owned Rel</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_ARTEFACT__OWNED_REL = eINSTANCE.getRefArtefact_OwnedRel();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_ARTEFACT__PROPERTY = eINSTANCE.getRefArtefact_Property();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefActivityImpl <em>Ref Activity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefActivityImpl
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefActivity()
		 * @generated
		 */
		EClass REF_ACTIVITY = eINSTANCE.getRefActivity();

		/**
		 * The meta object literal for the '<em><b>Objective</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_ACTIVITY__OBJECTIVE = eINSTANCE.getRefActivity_Objective();

		/**
		 * The meta object literal for the '<em><b>Scope</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_ACTIVITY__SCOPE = eINSTANCE.getRefActivity_Scope();

		/**
		 * The meta object literal for the '<em><b>Required Artefact</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_ACTIVITY__REQUIRED_ARTEFACT = eINSTANCE.getRefActivity_RequiredArtefact();

		/**
		 * The meta object literal for the '<em><b>Produced Artefact</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_ACTIVITY__PRODUCED_ARTEFACT = eINSTANCE.getRefActivity_ProducedArtefact();

		/**
		 * The meta object literal for the '<em><b>Sub Activity</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_ACTIVITY__SUB_ACTIVITY = eINSTANCE.getRefActivity_SubActivity();

		/**
		 * The meta object literal for the '<em><b>Preceding Activity</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_ACTIVITY__PRECEDING_ACTIVITY = eINSTANCE.getRefActivity_PrecedingActivity();

		/**
		 * The meta object literal for the '<em><b>Owned Requirement</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_ACTIVITY__OWNED_REQUIREMENT = eINSTANCE.getRefActivity_OwnedRequirement();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_ACTIVITY__ROLE = eINSTANCE.getRefActivity_Role();

		/**
		 * The meta object literal for the '<em><b>Applicable Technique</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_ACTIVITY__APPLICABLE_TECHNIQUE = eINSTANCE.getRefActivity_ApplicableTechnique();

		/**
		 * The meta object literal for the '<em><b>Owned Rel</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_ACTIVITY__OWNED_REL = eINSTANCE.getRefActivity_OwnedRel();

		/**
		 * The meta object literal for the '<em><b>Applicability</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_ACTIVITY__APPLICABILITY = eINSTANCE.getRefActivity_Applicability();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefRequirementRelImpl <em>Ref Requirement Rel</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefRequirementRelImpl
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefRequirementRel()
		 * @generated
		 */
		EClass REF_REQUIREMENT_REL = eINSTANCE.getRefRequirementRel();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_REQUIREMENT_REL__TARGET = eINSTANCE.getRefRequirementRel_Target();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_REQUIREMENT_REL__SOURCE = eINSTANCE.getRefRequirementRel_Source();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_REQUIREMENT_REL__TYPE = eINSTANCE.getRefRequirementRel_Type();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefRoleImpl <em>Ref Role</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefRoleImpl
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefRole()
		 * @generated
		 */
		EClass REF_ROLE = eINSTANCE.getRefRole();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefApplicabilityLevelImpl <em>Ref Applicability Level</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefApplicabilityLevelImpl
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefApplicabilityLevel()
		 * @generated
		 */
		EClass REF_APPLICABILITY_LEVEL = eINSTANCE.getRefApplicabilityLevel();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefCriticalityLevelImpl <em>Ref Criticality Level</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefCriticalityLevelImpl
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefCriticalityLevel()
		 * @generated
		 */
		EClass REF_CRITICALITY_LEVEL = eINSTANCE.getRefCriticalityLevel();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefTechniqueImpl <em>Ref Technique</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefTechniqueImpl
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefTechnique()
		 * @generated
		 */
		EClass REF_TECHNIQUE = eINSTANCE.getRefTechnique();

		/**
		 * The meta object literal for the '<em><b>Critic Applic</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_TECHNIQUE__CRITIC_APPLIC = eINSTANCE.getRefTechnique_CriticApplic();

		/**
		 * The meta object literal for the '<em><b>Aim</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_TECHNIQUE__AIM = eINSTANCE.getRefTechnique_Aim();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefArtefactRelImpl <em>Ref Artefact Rel</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefArtefactRelImpl
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefArtefactRel()
		 * @generated
		 */
		EClass REF_ARTEFACT_REL = eINSTANCE.getRefArtefactRel();

		/**
		 * The meta object literal for the '<em><b>Max Multiplicity Source</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_ARTEFACT_REL__MAX_MULTIPLICITY_SOURCE = eINSTANCE.getRefArtefactRel_MaxMultiplicitySource();

		/**
		 * The meta object literal for the '<em><b>Min Multiplicity Source</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_ARTEFACT_REL__MIN_MULTIPLICITY_SOURCE = eINSTANCE.getRefArtefactRel_MinMultiplicitySource();

		/**
		 * The meta object literal for the '<em><b>Max Multiplicity Target</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_ARTEFACT_REL__MAX_MULTIPLICITY_TARGET = eINSTANCE.getRefArtefactRel_MaxMultiplicityTarget();

		/**
		 * The meta object literal for the '<em><b>Min Multiplicity Target</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_ARTEFACT_REL__MIN_MULTIPLICITY_TARGET = eINSTANCE.getRefArtefactRel_MinMultiplicityTarget();

		/**
		 * The meta object literal for the '<em><b>Modification Effect</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_ARTEFACT_REL__MODIFICATION_EFFECT = eINSTANCE.getRefArtefactRel_ModificationEffect();

		/**
		 * The meta object literal for the '<em><b>Revocation Effect</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_ARTEFACT_REL__REVOCATION_EFFECT = eINSTANCE.getRefArtefactRel_RevocationEffect();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_ARTEFACT_REL__SOURCE = eINSTANCE.getRefArtefactRel_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_ARTEFACT_REL__TARGET = eINSTANCE.getRefArtefactRel_Target();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefCriticalityApplicabilityImpl <em>Ref Criticality Applicability</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefCriticalityApplicabilityImpl
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefCriticalityApplicability()
		 * @generated
		 */
		EClass REF_CRITICALITY_APPLICABILITY = eINSTANCE.getRefCriticalityApplicability();

		/**
		 * The meta object literal for the '<em><b>Applic Level</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_CRITICALITY_APPLICABILITY__APPLIC_LEVEL = eINSTANCE.getRefCriticalityApplicability_ApplicLevel();

		/**
		 * The meta object literal for the '<em><b>Critic Level</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_CRITICALITY_APPLICABILITY__CRITIC_LEVEL = eINSTANCE.getRefCriticalityApplicability_CriticLevel();

		/**
		 * The meta object literal for the '<em><b>Comment</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_CRITICALITY_APPLICABILITY__COMMENT = eINSTANCE.getRefCriticalityApplicability_Comment();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefActivityRelImpl <em>Ref Activity Rel</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefActivityRelImpl
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefActivityRel()
		 * @generated
		 */
		EClass REF_ACTIVITY_REL = eINSTANCE.getRefActivityRel();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_ACTIVITY_REL__TYPE = eINSTANCE.getRefActivityRel_Type();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_ACTIVITY_REL__SOURCE = eINSTANCE.getRefActivityRel_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_ACTIVITY_REL__TARGET = eINSTANCE.getRefActivityRel_Target();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefAssurableElementImpl <em>Ref Assurable Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefAssurableElementImpl
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefAssurableElement()
		 * @generated
		 */
		EClass REF_ASSURABLE_ELEMENT = eINSTANCE.getRefAssurableElement();

		/**
		 * The meta object literal for the '<em><b>Equivalence</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_ASSURABLE_ELEMENT__EQUIVALENCE = eINSTANCE.getRefAssurableElement_Equivalence();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefIndependencyLevelImpl <em>Ref Independency Level</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefIndependencyLevelImpl
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefIndependencyLevel()
		 * @generated
		 */
		EClass REF_INDEPENDENCY_LEVEL = eINSTANCE.getRefIndependencyLevel();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefRecommendationLevelImpl <em>Ref Recommendation Level</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefRecommendationLevelImpl
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefRecommendationLevel()
		 * @generated
		 */
		EClass REF_RECOMMENDATION_LEVEL = eINSTANCE.getRefRecommendationLevel();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefControlCategoryImpl <em>Ref Control Category</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefControlCategoryImpl
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefControlCategory()
		 * @generated
		 */
		EClass REF_CONTROL_CATEGORY = eINSTANCE.getRefControlCategory();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefApplicabilityImpl <em>Ref Applicability</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefApplicabilityImpl
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefApplicability()
		 * @generated
		 */
		EClass REF_APPLICABILITY = eINSTANCE.getRefApplicability();

		/**
		 * The meta object literal for the '<em><b>Applic Critic</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_APPLICABILITY__APPLIC_CRITIC = eINSTANCE.getRefApplicability_ApplicCritic();

		/**
		 * The meta object literal for the '<em><b>Comments</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_APPLICABILITY__COMMENTS = eINSTANCE.getRefApplicability_Comments();

		/**
		 * The meta object literal for the '<em><b>Applic Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_APPLICABILITY__APPLIC_TARGET = eINSTANCE.getRefApplicability_ApplicTarget();

		/**
		 * The meta object literal for the '<em><b>Owned Rel</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_APPLICABILITY__OWNED_REL = eINSTANCE.getRefApplicability_OwnedRel();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefApplicabilityRelImpl <em>Ref Applicability Rel</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefApplicabilityRelImpl
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefApplicabilityRel()
		 * @generated
		 */
		EClass REF_APPLICABILITY_REL = eINSTANCE.getRefApplicabilityRel();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REF_APPLICABILITY_REL__TYPE = eINSTANCE.getRefApplicabilityRel_Type();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_APPLICABILITY_REL__SOURCE = eINSTANCE.getRefApplicabilityRel_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_APPLICABILITY_REL__TARGET = eINSTANCE.getRefApplicabilityRel_Target();

		/**
		 * The meta object literal for the '{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefEquivalenceMapImpl <em>Ref Equivalence Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefEquivalenceMapImpl
		 * @see org.eclipse.opencert.pkm.refframework.refframework.impl.RefframeworkPackageImpl#getRefEquivalenceMap()
		 * @generated
		 */
		EClass REF_EQUIVALENCE_MAP = eINSTANCE.getRefEquivalenceMap();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REF_EQUIVALENCE_MAP__TARGET = eINSTANCE.getRefEquivalenceMap_Target();

	}

} //RefframeworkPackage
