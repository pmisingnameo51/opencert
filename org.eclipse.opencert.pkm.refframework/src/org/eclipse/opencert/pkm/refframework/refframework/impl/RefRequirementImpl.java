/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pkm.refframework.refframework.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.opencert.infra.general.general.impl.DescribableElementImpl;
import org.eclipse.opencert.pkm.refframework.refframework.RefApplicability;
import org.eclipse.opencert.pkm.refframework.refframework.RefAssurableElement;
import org.eclipse.opencert.pkm.refframework.refframework.RefEquivalenceMap;
import org.eclipse.opencert.pkm.refframework.refframework.RefRequirement;
import org.eclipse.opencert.pkm.refframework.refframework.RefRequirementRel;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ref Requirement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefRequirementImpl#getEquivalence <em>Equivalence</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefRequirementImpl#getReference <em>Reference</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefRequirementImpl#getAssumptions <em>Assumptions</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefRequirementImpl#getRationale <em>Rationale</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefRequirementImpl#getImage <em>Image</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefRequirementImpl#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefRequirementImpl#getOwnedRel <em>Owned Rel</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefRequirementImpl#getApplicability <em>Applicability</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefRequirementImpl#getSubRequirement <em>Sub Requirement</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RefRequirementImpl extends DescribableElementImpl implements RefRequirement {
	/**
	 * The default value of the '{@link #getReference() <em>Reference</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReference()
	 * @generated
	 * @ordered
	 */
	protected static final String REFERENCE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAssumptions() <em>Assumptions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssumptions()
	 * @generated
	 * @ordered
	 */
	protected static final String ASSUMPTIONS_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getRationale() <em>Rationale</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRationale()
	 * @generated
	 * @ordered
	 */
	protected static final String RATIONALE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getImage() <em>Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImage()
	 * @generated
	 * @ordered
	 */
	protected static final String IMAGE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAnnotations() <em>Annotations</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotations()
	 * @generated
	 * @ordered
	 */
	protected static final String ANNOTATIONS_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RefRequirementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RefframeworkPackage.Literals.REF_REQUIREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefEquivalenceMap> getEquivalence() {
		return (EList<RefEquivalenceMap>)eDynamicGet(RefframeworkPackage.REF_REQUIREMENT__EQUIVALENCE, RefframeworkPackage.Literals.REF_ASSURABLE_ELEMENT__EQUIVALENCE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getReference() {
		return (String)eDynamicGet(RefframeworkPackage.REF_REQUIREMENT__REFERENCE, RefframeworkPackage.Literals.REF_REQUIREMENT__REFERENCE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReference(String newReference) {
		eDynamicSet(RefframeworkPackage.REF_REQUIREMENT__REFERENCE, RefframeworkPackage.Literals.REF_REQUIREMENT__REFERENCE, newReference);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAssumptions() {
		return (String)eDynamicGet(RefframeworkPackage.REF_REQUIREMENT__ASSUMPTIONS, RefframeworkPackage.Literals.REF_REQUIREMENT__ASSUMPTIONS, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssumptions(String newAssumptions) {
		eDynamicSet(RefframeworkPackage.REF_REQUIREMENT__ASSUMPTIONS, RefframeworkPackage.Literals.REF_REQUIREMENT__ASSUMPTIONS, newAssumptions);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRationale() {
		return (String)eDynamicGet(RefframeworkPackage.REF_REQUIREMENT__RATIONALE, RefframeworkPackage.Literals.REF_REQUIREMENT__RATIONALE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRationale(String newRationale) {
		eDynamicSet(RefframeworkPackage.REF_REQUIREMENT__RATIONALE, RefframeworkPackage.Literals.REF_REQUIREMENT__RATIONALE, newRationale);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getImage() {
		return (String)eDynamicGet(RefframeworkPackage.REF_REQUIREMENT__IMAGE, RefframeworkPackage.Literals.REF_REQUIREMENT__IMAGE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImage(String newImage) {
		eDynamicSet(RefframeworkPackage.REF_REQUIREMENT__IMAGE, RefframeworkPackage.Literals.REF_REQUIREMENT__IMAGE, newImage);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAnnotations() {
		return (String)eDynamicGet(RefframeworkPackage.REF_REQUIREMENT__ANNOTATIONS, RefframeworkPackage.Literals.REF_REQUIREMENT__ANNOTATIONS, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotations(String newAnnotations) {
		eDynamicSet(RefframeworkPackage.REF_REQUIREMENT__ANNOTATIONS, RefframeworkPackage.Literals.REF_REQUIREMENT__ANNOTATIONS, newAnnotations);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefRequirementRel> getOwnedRel() {
		return (EList<RefRequirementRel>)eDynamicGet(RefframeworkPackage.REF_REQUIREMENT__OWNED_REL, RefframeworkPackage.Literals.REF_REQUIREMENT__OWNED_REL, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefApplicability> getApplicability() {
		return (EList<RefApplicability>)eDynamicGet(RefframeworkPackage.REF_REQUIREMENT__APPLICABILITY, RefframeworkPackage.Literals.REF_REQUIREMENT__APPLICABILITY, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RefRequirement> getSubRequirement() {
		return (EList<RefRequirement>)eDynamicGet(RefframeworkPackage.REF_REQUIREMENT__SUB_REQUIREMENT, RefframeworkPackage.Literals.REF_REQUIREMENT__SUB_REQUIREMENT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RefframeworkPackage.REF_REQUIREMENT__EQUIVALENCE:
				return ((InternalEList<?>)getEquivalence()).basicRemove(otherEnd, msgs);
			case RefframeworkPackage.REF_REQUIREMENT__OWNED_REL:
				return ((InternalEList<?>)getOwnedRel()).basicRemove(otherEnd, msgs);
			case RefframeworkPackage.REF_REQUIREMENT__APPLICABILITY:
				return ((InternalEList<?>)getApplicability()).basicRemove(otherEnd, msgs);
			case RefframeworkPackage.REF_REQUIREMENT__SUB_REQUIREMENT:
				return ((InternalEList<?>)getSubRequirement()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RefframeworkPackage.REF_REQUIREMENT__EQUIVALENCE:
				return getEquivalence();
			case RefframeworkPackage.REF_REQUIREMENT__REFERENCE:
				return getReference();
			case RefframeworkPackage.REF_REQUIREMENT__ASSUMPTIONS:
				return getAssumptions();
			case RefframeworkPackage.REF_REQUIREMENT__RATIONALE:
				return getRationale();
			case RefframeworkPackage.REF_REQUIREMENT__IMAGE:
				return getImage();
			case RefframeworkPackage.REF_REQUIREMENT__ANNOTATIONS:
				return getAnnotations();
			case RefframeworkPackage.REF_REQUIREMENT__OWNED_REL:
				return getOwnedRel();
			case RefframeworkPackage.REF_REQUIREMENT__APPLICABILITY:
				return getApplicability();
			case RefframeworkPackage.REF_REQUIREMENT__SUB_REQUIREMENT:
				return getSubRequirement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RefframeworkPackage.REF_REQUIREMENT__EQUIVALENCE:
				getEquivalence().clear();
				getEquivalence().addAll((Collection<? extends RefEquivalenceMap>)newValue);
				return;
			case RefframeworkPackage.REF_REQUIREMENT__REFERENCE:
				setReference((String)newValue);
				return;
			case RefframeworkPackage.REF_REQUIREMENT__ASSUMPTIONS:
				setAssumptions((String)newValue);
				return;
			case RefframeworkPackage.REF_REQUIREMENT__RATIONALE:
				setRationale((String)newValue);
				return;
			case RefframeworkPackage.REF_REQUIREMENT__IMAGE:
				setImage((String)newValue);
				return;
			case RefframeworkPackage.REF_REQUIREMENT__ANNOTATIONS:
				setAnnotations((String)newValue);
				return;
			case RefframeworkPackage.REF_REQUIREMENT__OWNED_REL:
				getOwnedRel().clear();
				getOwnedRel().addAll((Collection<? extends RefRequirementRel>)newValue);
				return;
			case RefframeworkPackage.REF_REQUIREMENT__APPLICABILITY:
				getApplicability().clear();
				getApplicability().addAll((Collection<? extends RefApplicability>)newValue);
				return;
			case RefframeworkPackage.REF_REQUIREMENT__SUB_REQUIREMENT:
				getSubRequirement().clear();
				getSubRequirement().addAll((Collection<? extends RefRequirement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RefframeworkPackage.REF_REQUIREMENT__EQUIVALENCE:
				getEquivalence().clear();
				return;
			case RefframeworkPackage.REF_REQUIREMENT__REFERENCE:
				setReference(REFERENCE_EDEFAULT);
				return;
			case RefframeworkPackage.REF_REQUIREMENT__ASSUMPTIONS:
				setAssumptions(ASSUMPTIONS_EDEFAULT);
				return;
			case RefframeworkPackage.REF_REQUIREMENT__RATIONALE:
				setRationale(RATIONALE_EDEFAULT);
				return;
			case RefframeworkPackage.REF_REQUIREMENT__IMAGE:
				setImage(IMAGE_EDEFAULT);
				return;
			case RefframeworkPackage.REF_REQUIREMENT__ANNOTATIONS:
				setAnnotations(ANNOTATIONS_EDEFAULT);
				return;
			case RefframeworkPackage.REF_REQUIREMENT__OWNED_REL:
				getOwnedRel().clear();
				return;
			case RefframeworkPackage.REF_REQUIREMENT__APPLICABILITY:
				getApplicability().clear();
				return;
			case RefframeworkPackage.REF_REQUIREMENT__SUB_REQUIREMENT:
				getSubRequirement().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RefframeworkPackage.REF_REQUIREMENT__EQUIVALENCE:
				return !getEquivalence().isEmpty();
			case RefframeworkPackage.REF_REQUIREMENT__REFERENCE:
				return REFERENCE_EDEFAULT == null ? getReference() != null : !REFERENCE_EDEFAULT.equals(getReference());
			case RefframeworkPackage.REF_REQUIREMENT__ASSUMPTIONS:
				return ASSUMPTIONS_EDEFAULT == null ? getAssumptions() != null : !ASSUMPTIONS_EDEFAULT.equals(getAssumptions());
			case RefframeworkPackage.REF_REQUIREMENT__RATIONALE:
				return RATIONALE_EDEFAULT == null ? getRationale() != null : !RATIONALE_EDEFAULT.equals(getRationale());
			case RefframeworkPackage.REF_REQUIREMENT__IMAGE:
				return IMAGE_EDEFAULT == null ? getImage() != null : !IMAGE_EDEFAULT.equals(getImage());
			case RefframeworkPackage.REF_REQUIREMENT__ANNOTATIONS:
				return ANNOTATIONS_EDEFAULT == null ? getAnnotations() != null : !ANNOTATIONS_EDEFAULT.equals(getAnnotations());
			case RefframeworkPackage.REF_REQUIREMENT__OWNED_REL:
				return !getOwnedRel().isEmpty();
			case RefframeworkPackage.REF_REQUIREMENT__APPLICABILITY:
				return !getApplicability().isEmpty();
			case RefframeworkPackage.REF_REQUIREMENT__SUB_REQUIREMENT:
				return !getSubRequirement().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == RefAssurableElement.class) {
			switch (derivedFeatureID) {
				case RefframeworkPackage.REF_REQUIREMENT__EQUIVALENCE: return RefframeworkPackage.REF_ASSURABLE_ELEMENT__EQUIVALENCE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == RefAssurableElement.class) {
			switch (baseFeatureID) {
				case RefframeworkPackage.REF_ASSURABLE_ELEMENT__EQUIVALENCE: return RefframeworkPackage.REF_REQUIREMENT__EQUIVALENCE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //RefRequirementImpl
