/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.pkm.refframework.refframework.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.internal.cdo.CDOObjectImpl;

import org.eclipse.opencert.pkm.refframework.refframework.RefApplicabilityLevel;
import org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityApplicability;
import org.eclipse.opencert.pkm.refframework.refframework.RefCriticalityLevel;
import org.eclipse.opencert.pkm.refframework.refframework.RefframeworkPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ref Criticality Applicability</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefCriticalityApplicabilityImpl#getApplicLevel <em>Applic Level</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefCriticalityApplicabilityImpl#getCriticLevel <em>Critic Level</em>}</li>
 *   <li>{@link org.eclipse.opencert.pkm.refframework.refframework.impl.RefCriticalityApplicabilityImpl#getComment <em>Comment</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RefCriticalityApplicabilityImpl extends CDOObjectImpl implements RefCriticalityApplicability {
	/**
	 * The default value of the '{@link #getComment() <em>Comment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected static final String COMMENT_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RefCriticalityApplicabilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RefframeworkPackage.Literals.REF_CRITICALITY_APPLICABILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefApplicabilityLevel getApplicLevel() {
		return (RefApplicabilityLevel)eDynamicGet(RefframeworkPackage.REF_CRITICALITY_APPLICABILITY__APPLIC_LEVEL, RefframeworkPackage.Literals.REF_CRITICALITY_APPLICABILITY__APPLIC_LEVEL, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefApplicabilityLevel basicGetApplicLevel() {
		return (RefApplicabilityLevel)eDynamicGet(RefframeworkPackage.REF_CRITICALITY_APPLICABILITY__APPLIC_LEVEL, RefframeworkPackage.Literals.REF_CRITICALITY_APPLICABILITY__APPLIC_LEVEL, false, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApplicLevel(RefApplicabilityLevel newApplicLevel) {
		eDynamicSet(RefframeworkPackage.REF_CRITICALITY_APPLICABILITY__APPLIC_LEVEL, RefframeworkPackage.Literals.REF_CRITICALITY_APPLICABILITY__APPLIC_LEVEL, newApplicLevel);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefCriticalityLevel getCriticLevel() {
		return (RefCriticalityLevel)eDynamicGet(RefframeworkPackage.REF_CRITICALITY_APPLICABILITY__CRITIC_LEVEL, RefframeworkPackage.Literals.REF_CRITICALITY_APPLICABILITY__CRITIC_LEVEL, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefCriticalityLevel basicGetCriticLevel() {
		return (RefCriticalityLevel)eDynamicGet(RefframeworkPackage.REF_CRITICALITY_APPLICABILITY__CRITIC_LEVEL, RefframeworkPackage.Literals.REF_CRITICALITY_APPLICABILITY__CRITIC_LEVEL, false, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCriticLevel(RefCriticalityLevel newCriticLevel) {
		eDynamicSet(RefframeworkPackage.REF_CRITICALITY_APPLICABILITY__CRITIC_LEVEL, RefframeworkPackage.Literals.REF_CRITICALITY_APPLICABILITY__CRITIC_LEVEL, newCriticLevel);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getComment() {
		return (String)eDynamicGet(RefframeworkPackage.REF_CRITICALITY_APPLICABILITY__COMMENT, RefframeworkPackage.Literals.REF_CRITICALITY_APPLICABILITY__COMMENT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComment(String newComment) {
		eDynamicSet(RefframeworkPackage.REF_CRITICALITY_APPLICABILITY__COMMENT, RefframeworkPackage.Literals.REF_CRITICALITY_APPLICABILITY__COMMENT, newComment);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RefframeworkPackage.REF_CRITICALITY_APPLICABILITY__APPLIC_LEVEL:
				if (resolve) return getApplicLevel();
				return basicGetApplicLevel();
			case RefframeworkPackage.REF_CRITICALITY_APPLICABILITY__CRITIC_LEVEL:
				if (resolve) return getCriticLevel();
				return basicGetCriticLevel();
			case RefframeworkPackage.REF_CRITICALITY_APPLICABILITY__COMMENT:
				return getComment();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RefframeworkPackage.REF_CRITICALITY_APPLICABILITY__APPLIC_LEVEL:
				setApplicLevel((RefApplicabilityLevel)newValue);
				return;
			case RefframeworkPackage.REF_CRITICALITY_APPLICABILITY__CRITIC_LEVEL:
				setCriticLevel((RefCriticalityLevel)newValue);
				return;
			case RefframeworkPackage.REF_CRITICALITY_APPLICABILITY__COMMENT:
				setComment((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RefframeworkPackage.REF_CRITICALITY_APPLICABILITY__APPLIC_LEVEL:
				setApplicLevel((RefApplicabilityLevel)null);
				return;
			case RefframeworkPackage.REF_CRITICALITY_APPLICABILITY__CRITIC_LEVEL:
				setCriticLevel((RefCriticalityLevel)null);
				return;
			case RefframeworkPackage.REF_CRITICALITY_APPLICABILITY__COMMENT:
				setComment(COMMENT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RefframeworkPackage.REF_CRITICALITY_APPLICABILITY__APPLIC_LEVEL:
				return basicGetApplicLevel() != null;
			case RefframeworkPackage.REF_CRITICALITY_APPLICABILITY__CRITIC_LEVEL:
				return basicGetCriticLevel() != null;
			case RefframeworkPackage.REF_CRITICALITY_APPLICABILITY__COMMENT:
				return COMMENT_EDEFAULT == null ? getComment() != null : !COMMENT_EDEFAULT.equals(getComment());
		}
		return super.eIsSet(featureID);
	}

} //RefCriticalityApplicabilityImpl
