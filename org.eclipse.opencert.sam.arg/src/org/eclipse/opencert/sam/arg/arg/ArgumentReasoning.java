/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Argument Reasoning</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.ArgumentReasoning#getToBeSupported <em>To Be Supported</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.ArgumentReasoning#getToBeInstantiated <em>To Be Instantiated</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.ArgumentReasoning#getHasStructure <em>Has Structure</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getArgumentReasoning()
 * @model annotation="gmf.node label='id' label.icon='false' figure='gsnfigures.GSNStrategy' tool.small.path='GSN_tooling_icons/Strategy.gif' tool.large.path='GSN_tooling_icons/Strategy.gif' tool.small.bundle='org.eclipse.opencert.sam.arg' tool.large.bundle='org.eclipse.opencert.sam.arg' size='120,80'"
 * @generated
 */
public interface ArgumentReasoning extends ReasoningElement {
	/**
	 * Returns the value of the '<em><b>To Be Supported</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Be Supported</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Be Supported</em>' attribute.
	 * @see #setToBeSupported(Boolean)
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getArgumentReasoning_ToBeSupported()
	 * @model default="false" required="true"
	 * @generated
	 */
	Boolean getToBeSupported();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.sam.arg.arg.ArgumentReasoning#getToBeSupported <em>To Be Supported</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To Be Supported</em>' attribute.
	 * @see #getToBeSupported()
	 * @generated
	 */
	void setToBeSupported(Boolean value);

	/**
	 * Returns the value of the '<em><b>To Be Instantiated</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Be Instantiated</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Be Instantiated</em>' attribute.
	 * @see #setToBeInstantiated(Boolean)
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getArgumentReasoning_ToBeInstantiated()
	 * @model default="false" required="true"
	 * @generated
	 */
	Boolean getToBeInstantiated();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.sam.arg.arg.ArgumentReasoning#getToBeInstantiated <em>To Be Instantiated</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To Be Instantiated</em>' attribute.
	 * @see #getToBeInstantiated()
	 * @generated
	 */
	void setToBeInstantiated(Boolean value);

	/**
	 * Returns the value of the '<em><b>Has Structure</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.sam.arg.arg.Argumentation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Structure</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Structure</em>' reference list.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getArgumentReasoning_HasStructure()
	 * @model
	 * @generated
	 */
	EList<Argumentation> getHasStructure();

} // ArgumentReasoning
