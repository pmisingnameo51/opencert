/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAsset;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvaluation;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceAssetEvent;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.AssuranceassetPackage;
import org.eclipse.opencert.apm.assuranceassets.assuranceasset.ManageableAssuranceAsset;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.ArgumentElement;
import org.eclipse.opencert.sam.arg.arg.Argumentation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Argumentation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.ArgumentationImpl#getEvaluation <em>Evaluation</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.ArgumentationImpl#getLifecycleEvent <em>Lifecycle Event</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.ArgumentationImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.ArgumentationImpl#getArgumentation <em>Argumentation</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.ArgumentationImpl#getConsistOf <em>Consist Of</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ArgumentationImpl extends ArgumentationElementImpl implements Argumentation {
	/**
	 * The default value of the '{@link #getLocation() <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected static final String LOCATION_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArgumentationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArgPackage.Literals.ARGUMENTATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<AssuranceAssetEvaluation> getEvaluation() {
		return (EList<AssuranceAssetEvaluation>)eDynamicGet(ArgPackage.ARGUMENTATION__EVALUATION, AssuranceassetPackage.Literals.MANAGEABLE_ASSURANCE_ASSET__EVALUATION, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<AssuranceAssetEvent> getLifecycleEvent() {
		return (EList<AssuranceAssetEvent>)eDynamicGet(ArgPackage.ARGUMENTATION__LIFECYCLE_EVENT, AssuranceassetPackage.Literals.MANAGEABLE_ASSURANCE_ASSET__LIFECYCLE_EVENT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLocation() {
		return (String)eDynamicGet(ArgPackage.ARGUMENTATION__LOCATION, ArgPackage.Literals.ARGUMENTATION__LOCATION, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(String newLocation) {
		eDynamicSet(ArgPackage.ARGUMENTATION__LOCATION, ArgPackage.Literals.ARGUMENTATION__LOCATION, newLocation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Argumentation> getArgumentation() {
		return (EList<Argumentation>)eDynamicGet(ArgPackage.ARGUMENTATION__ARGUMENTATION, ArgPackage.Literals.ARGUMENTATION__ARGUMENTATION, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ArgumentElement> getConsistOf() {
		return (EList<ArgumentElement>)eDynamicGet(ArgPackage.ARGUMENTATION__CONSIST_OF, ArgPackage.Literals.ARGUMENTATION__CONSIST_OF, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ArgPackage.ARGUMENTATION__EVALUATION:
				return ((InternalEList<?>)getEvaluation()).basicRemove(otherEnd, msgs);
			case ArgPackage.ARGUMENTATION__LIFECYCLE_EVENT:
				return ((InternalEList<?>)getLifecycleEvent()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ArgPackage.ARGUMENTATION__EVALUATION:
				return getEvaluation();
			case ArgPackage.ARGUMENTATION__LIFECYCLE_EVENT:
				return getLifecycleEvent();
			case ArgPackage.ARGUMENTATION__LOCATION:
				return getLocation();
			case ArgPackage.ARGUMENTATION__ARGUMENTATION:
				return getArgumentation();
			case ArgPackage.ARGUMENTATION__CONSIST_OF:
				return getConsistOf();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ArgPackage.ARGUMENTATION__EVALUATION:
				getEvaluation().clear();
				getEvaluation().addAll((Collection<? extends AssuranceAssetEvaluation>)newValue);
				return;
			case ArgPackage.ARGUMENTATION__LIFECYCLE_EVENT:
				getLifecycleEvent().clear();
				getLifecycleEvent().addAll((Collection<? extends AssuranceAssetEvent>)newValue);
				return;
			case ArgPackage.ARGUMENTATION__LOCATION:
				setLocation((String)newValue);
				return;
			case ArgPackage.ARGUMENTATION__ARGUMENTATION:
				getArgumentation().clear();
				getArgumentation().addAll((Collection<? extends Argumentation>)newValue);
				return;
			case ArgPackage.ARGUMENTATION__CONSIST_OF:
				getConsistOf().clear();
				getConsistOf().addAll((Collection<? extends ArgumentElement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ArgPackage.ARGUMENTATION__EVALUATION:
				getEvaluation().clear();
				return;
			case ArgPackage.ARGUMENTATION__LIFECYCLE_EVENT:
				getLifecycleEvent().clear();
				return;
			case ArgPackage.ARGUMENTATION__LOCATION:
				setLocation(LOCATION_EDEFAULT);
				return;
			case ArgPackage.ARGUMENTATION__ARGUMENTATION:
				getArgumentation().clear();
				return;
			case ArgPackage.ARGUMENTATION__CONSIST_OF:
				getConsistOf().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ArgPackage.ARGUMENTATION__EVALUATION:
				return !getEvaluation().isEmpty();
			case ArgPackage.ARGUMENTATION__LIFECYCLE_EVENT:
				return !getLifecycleEvent().isEmpty();
			case ArgPackage.ARGUMENTATION__LOCATION:
				return LOCATION_EDEFAULT == null ? getLocation() != null : !LOCATION_EDEFAULT.equals(getLocation());
			case ArgPackage.ARGUMENTATION__ARGUMENTATION:
				return !getArgumentation().isEmpty();
			case ArgPackage.ARGUMENTATION__CONSIST_OF:
				return !getConsistOf().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == AssuranceAsset.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == ManageableAssuranceAsset.class) {
			switch (derivedFeatureID) {
				case ArgPackage.ARGUMENTATION__EVALUATION: return AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET__EVALUATION;
				case ArgPackage.ARGUMENTATION__LIFECYCLE_EVENT: return AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET__LIFECYCLE_EVENT;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == AssuranceAsset.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == ManageableAssuranceAsset.class) {
			switch (baseFeatureID) {
				case AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET__EVALUATION: return ArgPackage.ARGUMENTATION__EVALUATION;
				case AssuranceassetPackage.MANAGEABLE_ASSURANCE_ASSET__LIFECYCLE_EVENT: return ArgPackage.ARGUMENTATION__LIFECYCLE_EVENT;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ArgumentationImpl
