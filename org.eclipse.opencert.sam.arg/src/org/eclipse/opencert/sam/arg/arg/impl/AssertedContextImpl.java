/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.ArgumentationElement;
import org.eclipse.opencert.sam.arg.arg.AssertedByMultiplicityExtension;
import org.eclipse.opencert.sam.arg.arg.AssertedContext;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Asserted Context</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.AssertedContextImpl#getMultiextension <em>Multiextension</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.AssertedContextImpl#getCardinality <em>Cardinality</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.AssertedContextImpl#getSource <em>Source</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.AssertedContextImpl#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AssertedContextImpl extends AssertedRelationshipImpl implements AssertedContext {
	/**
	 * The default value of the '{@link #getMultiextension() <em>Multiextension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMultiextension()
	 * @generated
	 * @ordered
	 */
	protected static final AssertedByMultiplicityExtension MULTIEXTENSION_EDEFAULT = AssertedByMultiplicityExtension.NORMAL;

	/**
	 * The default value of the '{@link #getCardinality() <em>Cardinality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCardinality()
	 * @generated
	 * @ordered
	 */
	protected static final String CARDINALITY_EDEFAULT = "";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssertedContextImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArgPackage.Literals.ASSERTED_CONTEXT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertedByMultiplicityExtension getMultiextension() {
		return (AssertedByMultiplicityExtension)eDynamicGet(ArgPackage.ASSERTED_CONTEXT__MULTIEXTENSION, ArgPackage.Literals.ASSERTED_CONTEXT__MULTIEXTENSION, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMultiextension(AssertedByMultiplicityExtension newMultiextension) {
		eDynamicSet(ArgPackage.ASSERTED_CONTEXT__MULTIEXTENSION, ArgPackage.Literals.ASSERTED_CONTEXT__MULTIEXTENSION, newMultiextension);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCardinality() {
		return (String)eDynamicGet(ArgPackage.ASSERTED_CONTEXT__CARDINALITY, ArgPackage.Literals.ASSERTED_CONTEXT__CARDINALITY, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCardinality(String newCardinality) {
		eDynamicSet(ArgPackage.ASSERTED_CONTEXT__CARDINALITY, ArgPackage.Literals.ASSERTED_CONTEXT__CARDINALITY, newCardinality);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ArgumentationElement> getSource() {
		return (EList<ArgumentationElement>)eDynamicGet(ArgPackage.ASSERTED_CONTEXT__SOURCE, ArgPackage.Literals.ASSERTED_CONTEXT__SOURCE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ArgumentationElement> getTarget() {
		return (EList<ArgumentationElement>)eDynamicGet(ArgPackage.ASSERTED_CONTEXT__TARGET, ArgPackage.Literals.ASSERTED_CONTEXT__TARGET, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ArgPackage.ASSERTED_CONTEXT__MULTIEXTENSION:
				return getMultiextension();
			case ArgPackage.ASSERTED_CONTEXT__CARDINALITY:
				return getCardinality();
			case ArgPackage.ASSERTED_CONTEXT__SOURCE:
				return getSource();
			case ArgPackage.ASSERTED_CONTEXT__TARGET:
				return getTarget();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ArgPackage.ASSERTED_CONTEXT__MULTIEXTENSION:
				setMultiextension((AssertedByMultiplicityExtension)newValue);
				return;
			case ArgPackage.ASSERTED_CONTEXT__CARDINALITY:
				setCardinality((String)newValue);
				return;
			case ArgPackage.ASSERTED_CONTEXT__SOURCE:
				getSource().clear();
				getSource().addAll((Collection<? extends ArgumentationElement>)newValue);
				return;
			case ArgPackage.ASSERTED_CONTEXT__TARGET:
				getTarget().clear();
				getTarget().addAll((Collection<? extends ArgumentationElement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ArgPackage.ASSERTED_CONTEXT__MULTIEXTENSION:
				setMultiextension(MULTIEXTENSION_EDEFAULT);
				return;
			case ArgPackage.ASSERTED_CONTEXT__CARDINALITY:
				setCardinality(CARDINALITY_EDEFAULT);
				return;
			case ArgPackage.ASSERTED_CONTEXT__SOURCE:
				getSource().clear();
				return;
			case ArgPackage.ASSERTED_CONTEXT__TARGET:
				getTarget().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ArgPackage.ASSERTED_CONTEXT__MULTIEXTENSION:
				return getMultiextension() != MULTIEXTENSION_EDEFAULT;
			case ArgPackage.ASSERTED_CONTEXT__CARDINALITY:
				return CARDINALITY_EDEFAULT == null ? getCardinality() != null : !CARDINALITY_EDEFAULT.equals(getCardinality());
			case ArgPackage.ASSERTED_CONTEXT__SOURCE:
				return !getSource().isEmpty();
			case ArgPackage.ASSERTED_CONTEXT__TARGET:
				return !getTarget().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AssertedContextImpl
