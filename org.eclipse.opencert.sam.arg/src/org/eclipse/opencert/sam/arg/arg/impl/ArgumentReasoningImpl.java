/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.ArgumentReasoning;
import org.eclipse.opencert.sam.arg.arg.Argumentation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Argument Reasoning</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.ArgumentReasoningImpl#getToBeSupported <em>To Be Supported</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.ArgumentReasoningImpl#getToBeInstantiated <em>To Be Instantiated</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.ArgumentReasoningImpl#getHasStructure <em>Has Structure</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ArgumentReasoningImpl extends ReasoningElementImpl implements ArgumentReasoning {
	/**
	 * The default value of the '{@link #getToBeSupported() <em>To Be Supported</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToBeSupported()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean TO_BE_SUPPORTED_EDEFAULT = Boolean.FALSE;

	/**
	 * The default value of the '{@link #getToBeInstantiated() <em>To Be Instantiated</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToBeInstantiated()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean TO_BE_INSTANTIATED_EDEFAULT = Boolean.FALSE;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArgumentReasoningImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArgPackage.Literals.ARGUMENT_REASONING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getToBeSupported() {
		return (Boolean)eDynamicGet(ArgPackage.ARGUMENT_REASONING__TO_BE_SUPPORTED, ArgPackage.Literals.ARGUMENT_REASONING__TO_BE_SUPPORTED, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToBeSupported(Boolean newToBeSupported) {
		eDynamicSet(ArgPackage.ARGUMENT_REASONING__TO_BE_SUPPORTED, ArgPackage.Literals.ARGUMENT_REASONING__TO_BE_SUPPORTED, newToBeSupported);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getToBeInstantiated() {
		return (Boolean)eDynamicGet(ArgPackage.ARGUMENT_REASONING__TO_BE_INSTANTIATED, ArgPackage.Literals.ARGUMENT_REASONING__TO_BE_INSTANTIATED, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToBeInstantiated(Boolean newToBeInstantiated) {
		eDynamicSet(ArgPackage.ARGUMENT_REASONING__TO_BE_INSTANTIATED, ArgPackage.Literals.ARGUMENT_REASONING__TO_BE_INSTANTIATED, newToBeInstantiated);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Argumentation> getHasStructure() {
		return (EList<Argumentation>)eDynamicGet(ArgPackage.ARGUMENT_REASONING__HAS_STRUCTURE, ArgPackage.Literals.ARGUMENT_REASONING__HAS_STRUCTURE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ArgPackage.ARGUMENT_REASONING__TO_BE_SUPPORTED:
				return getToBeSupported();
			case ArgPackage.ARGUMENT_REASONING__TO_BE_INSTANTIATED:
				return getToBeInstantiated();
			case ArgPackage.ARGUMENT_REASONING__HAS_STRUCTURE:
				return getHasStructure();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ArgPackage.ARGUMENT_REASONING__TO_BE_SUPPORTED:
				setToBeSupported((Boolean)newValue);
				return;
			case ArgPackage.ARGUMENT_REASONING__TO_BE_INSTANTIATED:
				setToBeInstantiated((Boolean)newValue);
				return;
			case ArgPackage.ARGUMENT_REASONING__HAS_STRUCTURE:
				getHasStructure().clear();
				getHasStructure().addAll((Collection<? extends Argumentation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ArgPackage.ARGUMENT_REASONING__TO_BE_SUPPORTED:
				setToBeSupported(TO_BE_SUPPORTED_EDEFAULT);
				return;
			case ArgPackage.ARGUMENT_REASONING__TO_BE_INSTANTIATED:
				setToBeInstantiated(TO_BE_INSTANTIATED_EDEFAULT);
				return;
			case ArgPackage.ARGUMENT_REASONING__HAS_STRUCTURE:
				getHasStructure().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ArgPackage.ARGUMENT_REASONING__TO_BE_SUPPORTED:
				return TO_BE_SUPPORTED_EDEFAULT == null ? getToBeSupported() != null : !TO_BE_SUPPORTED_EDEFAULT.equals(getToBeSupported());
			case ArgPackage.ARGUMENT_REASONING__TO_BE_INSTANTIATED:
				return TO_BE_INSTANTIATED_EDEFAULT == null ? getToBeInstantiated() != null : !TO_BE_INSTANTIATED_EDEFAULT.equals(getToBeInstantiated());
			case ArgPackage.ARGUMENT_REASONING__HAS_STRUCTURE:
				return !getHasStructure().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ArgumentReasoningImpl
