/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.ArgumentElementCitation;
import org.eclipse.opencert.sam.arg.arg.CitationElementType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Argument Element Citation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.ArgumentElementCitationImpl#getCitedType <em>Cited Type</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.ArgumentElementCitationImpl#getArgumentationReference <em>Argumentation Reference</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ArgumentElementCitationImpl extends ArgumentElementImpl implements ArgumentElementCitation {
	/**
	 * The default value of the '{@link #getCitedType() <em>Cited Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCitedType()
	 * @generated
	 * @ordered
	 */
	protected static final CitationElementType CITED_TYPE_EDEFAULT = CitationElementType.CLAIM;

	/**
	 * The default value of the '{@link #getArgumentationReference() <em>Argumentation Reference</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArgumentationReference()
	 * @generated
	 * @ordered
	 */
	protected static final String ARGUMENTATION_REFERENCE_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArgumentElementCitationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArgPackage.Literals.ARGUMENT_ELEMENT_CITATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CitationElementType getCitedType() {
		return (CitationElementType)eDynamicGet(ArgPackage.ARGUMENT_ELEMENT_CITATION__CITED_TYPE, ArgPackage.Literals.ARGUMENT_ELEMENT_CITATION__CITED_TYPE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCitedType(CitationElementType newCitedType) {
		eDynamicSet(ArgPackage.ARGUMENT_ELEMENT_CITATION__CITED_TYPE, ArgPackage.Literals.ARGUMENT_ELEMENT_CITATION__CITED_TYPE, newCitedType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getArgumentationReference() {
		return (String)eDynamicGet(ArgPackage.ARGUMENT_ELEMENT_CITATION__ARGUMENTATION_REFERENCE, ArgPackage.Literals.ARGUMENT_ELEMENT_CITATION__ARGUMENTATION_REFERENCE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setArgumentationReference(String newArgumentationReference) {
		eDynamicSet(ArgPackage.ARGUMENT_ELEMENT_CITATION__ARGUMENTATION_REFERENCE, ArgPackage.Literals.ARGUMENT_ELEMENT_CITATION__ARGUMENTATION_REFERENCE, newArgumentationReference);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ArgPackage.ARGUMENT_ELEMENT_CITATION__CITED_TYPE:
				return getCitedType();
			case ArgPackage.ARGUMENT_ELEMENT_CITATION__ARGUMENTATION_REFERENCE:
				return getArgumentationReference();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ArgPackage.ARGUMENT_ELEMENT_CITATION__CITED_TYPE:
				setCitedType((CitationElementType)newValue);
				return;
			case ArgPackage.ARGUMENT_ELEMENT_CITATION__ARGUMENTATION_REFERENCE:
				setArgumentationReference((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ArgPackage.ARGUMENT_ELEMENT_CITATION__CITED_TYPE:
				setCitedType(CITED_TYPE_EDEFAULT);
				return;
			case ArgPackage.ARGUMENT_ELEMENT_CITATION__ARGUMENTATION_REFERENCE:
				setArgumentationReference(ARGUMENTATION_REFERENCE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ArgPackage.ARGUMENT_ELEMENT_CITATION__CITED_TYPE:
				return getCitedType() != CITED_TYPE_EDEFAULT;
			case ArgPackage.ARGUMENT_ELEMENT_CITATION__ARGUMENTATION_REFERENCE:
				return ARGUMENTATION_REFERENCE_EDEFAULT == null ? getArgumentationReference() != null : !ARGUMENTATION_REFERENCE_EDEFAULT.equals(getArgumentationReference());
		}
		return super.eIsSet(featureID);
	}

} //ArgumentElementCitationImpl
