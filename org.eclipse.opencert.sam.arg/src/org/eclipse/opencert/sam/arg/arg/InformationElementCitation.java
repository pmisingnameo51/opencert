/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg;

import org.eclipse.emf.common.util.EList;

import org.eclipse.opencert.evm.evidspec.evidence.Artefact;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Information Element Citation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.InformationElementCitation#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.InformationElementCitation#getToBeInstantiated <em>To Be Instantiated</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.InformationElementCitation#getUrl <em>Url</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.InformationElementCitation#getArtefact <em>Artefact</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getInformationElementCitation()
 * @model annotation="gmf.node label='id' label.icon='false' figure='gsnfigures.GSNSolution' tool.small.path='GSN_tooling_icons/Solution.gif' tool.large.path='GSN_tooling_icons/Solution.gif' tool.small.bundle='org.eclipse.opencert.sam.arg' tool.large.bundle='org.eclipse.opencert.sam.arg' size='100,100'"
 * @generated
 */
public interface InformationElementCitation extends ArgumentElement {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.opencert.sam.arg.arg.InformationElementType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see org.eclipse.opencert.sam.arg.arg.InformationElementType
	 * @see #setType(InformationElementType)
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getInformationElementCitation_Type()
	 * @model
	 * @generated
	 */
	InformationElementType getType();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.sam.arg.arg.InformationElementCitation#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see org.eclipse.opencert.sam.arg.arg.InformationElementType
	 * @see #getType()
	 * @generated
	 */
	void setType(InformationElementType value);

	/**
	 * Returns the value of the '<em><b>To Be Instantiated</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Be Instantiated</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Be Instantiated</em>' attribute.
	 * @see #setToBeInstantiated(Boolean)
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getInformationElementCitation_ToBeInstantiated()
	 * @model default="false" required="true"
	 * @generated
	 */
	Boolean getToBeInstantiated();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.sam.arg.arg.InformationElementCitation#getToBeInstantiated <em>To Be Instantiated</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To Be Instantiated</em>' attribute.
	 * @see #getToBeInstantiated()
	 * @generated
	 */
	void setToBeInstantiated(Boolean value);

	/**
	 * Returns the value of the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Url</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Url</em>' attribute.
	 * @see #setUrl(String)
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getInformationElementCitation_Url()
	 * @model
	 * @generated
	 */
	String getUrl();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.sam.arg.arg.InformationElementCitation#getUrl <em>Url</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Url</em>' attribute.
	 * @see #getUrl()
	 * @generated
	 */
	void setUrl(String value);

	/**
	 * Returns the value of the '<em><b>Artefact</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.evm.evidspec.evidence.Artefact}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Artefact</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Artefact</em>' reference list.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getInformationElementCitation_Artefact()
	 * @model
	 * @generated
	 */
	EList<Artefact> getArtefact();

} // InformationElementCitation
