/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.opencert.evm.evidspec.evidence.Artefact;

import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.Argumentation;
import org.eclipse.opencert.sam.arg.arg.AssuranceCase;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Assurance Case</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.AssuranceCaseImpl#getHasArgument <em>Has Argument</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.AssuranceCaseImpl#getHasEvidence <em>Has Evidence</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.AssuranceCaseImpl#getComposedOf <em>Composed Of</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AssuranceCaseImpl extends ModelElementImpl implements AssuranceCase {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssuranceCaseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArgPackage.Literals.ASSURANCE_CASE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Argumentation> getHasArgument() {
		return (EList<Argumentation>)eDynamicGet(ArgPackage.ASSURANCE_CASE__HAS_ARGUMENT, ArgPackage.Literals.ASSURANCE_CASE__HAS_ARGUMENT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Artefact> getHasEvidence() {
		return (EList<Artefact>)eDynamicGet(ArgPackage.ASSURANCE_CASE__HAS_EVIDENCE, ArgPackage.Literals.ASSURANCE_CASE__HAS_EVIDENCE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<AssuranceCase> getComposedOf() {
		return (EList<AssuranceCase>)eDynamicGet(ArgPackage.ASSURANCE_CASE__COMPOSED_OF, ArgPackage.Literals.ASSURANCE_CASE__COMPOSED_OF, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ArgPackage.ASSURANCE_CASE__HAS_ARGUMENT:
				return ((InternalEList<?>)getHasArgument()).basicRemove(otherEnd, msgs);
			case ArgPackage.ASSURANCE_CASE__HAS_EVIDENCE:
				return ((InternalEList<?>)getHasEvidence()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ArgPackage.ASSURANCE_CASE__HAS_ARGUMENT:
				return getHasArgument();
			case ArgPackage.ASSURANCE_CASE__HAS_EVIDENCE:
				return getHasEvidence();
			case ArgPackage.ASSURANCE_CASE__COMPOSED_OF:
				return getComposedOf();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ArgPackage.ASSURANCE_CASE__HAS_ARGUMENT:
				getHasArgument().clear();
				getHasArgument().addAll((Collection<? extends Argumentation>)newValue);
				return;
			case ArgPackage.ASSURANCE_CASE__HAS_EVIDENCE:
				getHasEvidence().clear();
				getHasEvidence().addAll((Collection<? extends Artefact>)newValue);
				return;
			case ArgPackage.ASSURANCE_CASE__COMPOSED_OF:
				getComposedOf().clear();
				getComposedOf().addAll((Collection<? extends AssuranceCase>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ArgPackage.ASSURANCE_CASE__HAS_ARGUMENT:
				getHasArgument().clear();
				return;
			case ArgPackage.ASSURANCE_CASE__HAS_EVIDENCE:
				getHasEvidence().clear();
				return;
			case ArgPackage.ASSURANCE_CASE__COMPOSED_OF:
				getComposedOf().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ArgPackage.ASSURANCE_CASE__HAS_ARGUMENT:
				return !getHasArgument().isEmpty();
			case ArgPackage.ASSURANCE_CASE__HAS_EVIDENCE:
				return !getHasEvidence().isEmpty();
			case ArgPackage.ASSURANCE_CASE__COMPOSED_OF:
				return !getComposedOf().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AssuranceCaseImpl
