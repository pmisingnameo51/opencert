/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.AssertedByMultiplicityExtension;
import org.eclipse.opencert.sam.arg.arg.Choice;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Choice</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.ChoiceImpl#getSourceMultiextension <em>Source Multiextension</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.ChoiceImpl#getSourceCardinality <em>Source Cardinality</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.impl.ChoiceImpl#getOptionality <em>Optionality</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ChoiceImpl extends ArgumentElementImpl implements Choice {
	/**
	 * The default value of the '{@link #getSourceMultiextension() <em>Source Multiextension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceMultiextension()
	 * @generated
	 * @ordered
	 */
	protected static final AssertedByMultiplicityExtension SOURCE_MULTIEXTENSION_EDEFAULT = AssertedByMultiplicityExtension.NORMAL;

	/**
	 * The default value of the '{@link #getSourceCardinality() <em>Source Cardinality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceCardinality()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_CARDINALITY_EDEFAULT = "";

	/**
	 * The default value of the '{@link #getOptionality() <em>Optionality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOptionality()
	 * @generated
	 * @ordered
	 */
	protected static final String OPTIONALITY_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ChoiceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArgPackage.Literals.CHOICE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertedByMultiplicityExtension getSourceMultiextension() {
		return (AssertedByMultiplicityExtension)eDynamicGet(ArgPackage.CHOICE__SOURCE_MULTIEXTENSION, ArgPackage.Literals.CHOICE__SOURCE_MULTIEXTENSION, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceMultiextension(AssertedByMultiplicityExtension newSourceMultiextension) {
		eDynamicSet(ArgPackage.CHOICE__SOURCE_MULTIEXTENSION, ArgPackage.Literals.CHOICE__SOURCE_MULTIEXTENSION, newSourceMultiextension);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSourceCardinality() {
		return (String)eDynamicGet(ArgPackage.CHOICE__SOURCE_CARDINALITY, ArgPackage.Literals.CHOICE__SOURCE_CARDINALITY, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceCardinality(String newSourceCardinality) {
		eDynamicSet(ArgPackage.CHOICE__SOURCE_CARDINALITY, ArgPackage.Literals.CHOICE__SOURCE_CARDINALITY, newSourceCardinality);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOptionality() {
		return (String)eDynamicGet(ArgPackage.CHOICE__OPTIONALITY, ArgPackage.Literals.CHOICE__OPTIONALITY, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOptionality(String newOptionality) {
		eDynamicSet(ArgPackage.CHOICE__OPTIONALITY, ArgPackage.Literals.CHOICE__OPTIONALITY, newOptionality);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ArgPackage.CHOICE__SOURCE_MULTIEXTENSION:
				return getSourceMultiextension();
			case ArgPackage.CHOICE__SOURCE_CARDINALITY:
				return getSourceCardinality();
			case ArgPackage.CHOICE__OPTIONALITY:
				return getOptionality();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ArgPackage.CHOICE__SOURCE_MULTIEXTENSION:
				setSourceMultiextension((AssertedByMultiplicityExtension)newValue);
				return;
			case ArgPackage.CHOICE__SOURCE_CARDINALITY:
				setSourceCardinality((String)newValue);
				return;
			case ArgPackage.CHOICE__OPTIONALITY:
				setOptionality((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ArgPackage.CHOICE__SOURCE_MULTIEXTENSION:
				setSourceMultiextension(SOURCE_MULTIEXTENSION_EDEFAULT);
				return;
			case ArgPackage.CHOICE__SOURCE_CARDINALITY:
				setSourceCardinality(SOURCE_CARDINALITY_EDEFAULT);
				return;
			case ArgPackage.CHOICE__OPTIONALITY:
				setOptionality(OPTIONALITY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ArgPackage.CHOICE__SOURCE_MULTIEXTENSION:
				return getSourceMultiextension() != SOURCE_MULTIEXTENSION_EDEFAULT;
			case ArgPackage.CHOICE__SOURCE_CARDINALITY:
				return SOURCE_CARDINALITY_EDEFAULT == null ? getSourceCardinality() != null : !SOURCE_CARDINALITY_EDEFAULT.equals(getSourceCardinality());
			case ArgPackage.CHOICE__OPTIONALITY:
				return OPTIONALITY_EDEFAULT == null ? getOptionality() != null : !OPTIONALITY_EDEFAULT.equals(getOptionality());
		}
		return super.eIsSet(featureID);
	}

} //ChoiceImpl
