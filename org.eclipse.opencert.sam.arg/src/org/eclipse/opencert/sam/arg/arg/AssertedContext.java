/*******************************************************************************
 * Copyright (c) 2016 Fundaci�n Tecnalia Research & Innovation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Huascar Espinoza - initial API and implementation
 *   Alejandra Ru�z - initial API and implementation
 *   Idoya Del R�o - initial API and implementation
 *   Mari Carmen Palacios - initial API and implementation
 *   Angel L�pez - initial API and implementation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.sam.arg.arg;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Asserted Context</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.AssertedContext#getMultiextension <em>Multiextension</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.AssertedContext#getCardinality <em>Cardinality</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.AssertedContext#getSource <em>Source</em>}</li>
 *   <li>{@link org.eclipse.opencert.sam.arg.arg.AssertedContext#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getAssertedContext()
 * @model annotation="gmf.link color='0,0,0' source='source' source.constraint='self.oclIsTypeOf(ArgumentReasoning) or self.oclIsTypeOf(Claim) or self.oclIsTypeOf(Argumentation)' target='target' target.constraint='self.oclIsTypeOf(ArgumentElementCitation) or self.oclIsTypeOf(InformationElementCitation)' style='solid' width='1' target.decoration='closedarrow' tool.small.path='GSN_tooling_icons/inTheContextOf.gif' tool.large.path='GSN_tooling_icons/inTheContextOf.gif' tool.small.bundle='org.eclipse.opencert.sam.arg' tool.large.bundle='org.eclipse.opencert.sam.arg' label='cardinality'"
 * @generated
 */
public interface AssertedContext extends AssertedRelationship {
	/**
	 * Returns the value of the '<em><b>Multiextension</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.opencert.sam.arg.arg.AssertedByMultiplicityExtension}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multiextension</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiextension</em>' attribute.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedByMultiplicityExtension
	 * @see #setMultiextension(AssertedByMultiplicityExtension)
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getAssertedContext_Multiextension()
	 * @model
	 * @generated
	 */
	AssertedByMultiplicityExtension getMultiextension();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.sam.arg.arg.AssertedContext#getMultiextension <em>Multiextension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Multiextension</em>' attribute.
	 * @see org.eclipse.opencert.sam.arg.arg.AssertedByMultiplicityExtension
	 * @see #getMultiextension()
	 * @generated
	 */
	void setMultiextension(AssertedByMultiplicityExtension value);

	/**
	 * Returns the value of the '<em><b>Cardinality</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cardinality</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cardinality</em>' attribute.
	 * @see #setCardinality(String)
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getAssertedContext_Cardinality()
	 * @model default=""
	 * @generated
	 */
	String getCardinality();

	/**
	 * Sets the value of the '{@link org.eclipse.opencert.sam.arg.arg.AssertedContext#getCardinality <em>Cardinality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cardinality</em>' attribute.
	 * @see #getCardinality()
	 * @generated
	 */
	void setCardinality(String value);

	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.sam.arg.arg.ArgumentationElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference list.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getAssertedContext_Source()
	 * @model
	 * @generated
	 */
	EList<ArgumentationElement> getSource();

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.opencert.sam.arg.arg.ArgumentationElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference list.
	 * @see org.eclipse.opencert.sam.arg.arg.ArgPackage#getAssertedContext_Target()
	 * @model
	 * @generated
	 */
	EList<ArgumentationElement> getTarget();

} // AssertedContext
