/*******************************************************************************
 * Copyright (c) 2016 University of York.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Thomas Richardson - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.sam.contract.changeanalysis;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

public class ArgumentationChangeAnalysisHandler extends AbstractHandler {
	
	
	public ArgumentationChangeAnalysisHandler() {
	}

	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		ArgumentationChangeAnalysisDialog changeDialog = new ArgumentationChangeAnalysisDialog(window.getShell());
		changeDialog.open();
		return null;
	}
}
