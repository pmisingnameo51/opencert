/*******************************************************************************
 * Copyright (c) 2016 University of York.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Thomas Richardson - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.sam.contract.wizards;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.opencert.sam.arg.arg.ArgumentationElement;

public class ContextAssociationTableLabelProvider extends LabelProvider implements ITableLabelProvider {
	
	public Image getColumnImage(Object element, int index) {
		return null;
		}
	
	public String getColumnText(Object element, int index) {
	
		ContextAssociation ca = (ContextAssociation) element;
		ArgumentationElement source = ca.getSource();
		ArgumentationElement target = ca.getTarget();
		
		switch (index) {
			case 0 :
				return (String) source.getId();
			case 1 :
				return (String) source.getDescription();
			case 2 :
				return (String) target.getId();
			case 3 :
				return (String) target.getDescription();
			default :
				return "unknown " + index;
		}
	}
}
