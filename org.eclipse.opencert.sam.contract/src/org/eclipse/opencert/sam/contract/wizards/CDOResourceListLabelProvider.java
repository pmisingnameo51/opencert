/*******************************************************************************
 * Copyright (c) 2016 University of York.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Thomas Richardson - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.sam.contract.wizards;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.emf.cdo.eresource.CDOResource;

public class CDOResourceListLabelProvider extends LabelProvider {
	
	public Image getImage(Object element) {
		return null;
		}

	public String getText(Object element) {
		CDOResource resource = (CDOResource) element;
		return resource.getPath();
		}
}
