/*******************************************************************************
 * Copyright (c) 2016 University of York.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Thomas Richardson - initial API and implementation
 *******************************************************************************/

package org.eclipse.opencert.sam.contract.integration.wizards;

import java.util.ArrayList;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.cdo.dawn.preferences.PreferenceConstants;
import org.eclipse.emf.cdo.dawn.ui.wizards.DawnCreateNewDiagramResourceWizardPage;
import org.eclipse.emf.cdo.dawn.ui.wizards.DawnCreateNewResourceWizardPage;
import org.eclipse.emf.cdo.dawn.util.connection.CDOConnectionUtil;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.opencert.sam.arg.arg.ArgFactory;
import org.eclipse.opencert.sam.arg.arg.ArgPackage;
import org.eclipse.opencert.sam.arg.arg.Case;
import org.eclipse.opencert.sam.contract.changeanalysis.AnalysisResultsDialog;
import org.eclipse.opencert.sam.contract.wizards.ContractModelBuilder;
import org.eclipse.opencert.sam.contract.wizards.ContractModelElement;

public class NewIntegrationWizard extends Wizard implements INewWizard {

	private ArgPackage argPackage = ArgPackage.eINSTANCE;
	private ArgFactory argFactory = argPackage.getArgFactory();
	private ISelection selection;
	private CDOView view;
	private ModuleSelectionWizardPage moduleSelectionPage;
	private ModuleSelectionWizardPage contractModuleSelectionPage;
	private DawnCreateNewDiagramResourceWizardPage dawnDiagramModelFilePage;
	private DawnCreateNewResourceWizardPage dawnDomainModelFilePage;
	
	public NewIntegrationWizard() {
		super();
		setNeedsProgressMonitor(true);
		CDOConnectionUtil.instance.init(
				PreferenceConstants.getRepositoryName(),
				PreferenceConstants.getProtocol(),
				PreferenceConstants.getServerName());
		CDOSession session = CDOConnectionUtil.instance.getCurrentSession();
		view = CDOConnectionUtil.instance.openView(session);
	}

	public void addPages() {
		moduleSelectionPage = new ModuleSelectionWizardPage("Select Modules",view, true);
		moduleSelectionPage.setTitle("Select Argument Modules");
		moduleSelectionPage.setDescription("Select argument modules that will constitute the system safety case");
		addPage(moduleSelectionPage);
		contractModuleSelectionPage = new ModuleSelectionWizardPage("Select Contract Modules",view, false);
		contractModuleSelectionPage.setTitle("Select Argumnet Contract Modules");
		contractModuleSelectionPage.setDescription("Select argumnet contract modules that resolve inter-module dependencies");
		addPage(contractModuleSelectionPage);
		
		dawnDiagramModelFilePage = new DawnCreateNewDiagramResourceWizardPage(
				"arg_diagram", false, view);
		dawnDiagramModelFilePage
				.setTitle("New System Integration Arg Diagram");
		dawnDiagramModelFilePage
				.setDescription("Select the location to store the genertaed system integration arg diagram");
		dawnDiagramModelFilePage.setCreateAutomaticResourceName(true);
		addPage(dawnDiagramModelFilePage);

		dawnDomainModelFilePage = new DawnCreateNewResourceWizardPage("arg",
				false, view) {
			@Override
			public void setVisible(boolean visible) {
				if (visible) {
					URI uri = dawnDiagramModelFilePage.getURI();
					String fileName = uri.lastSegment();
					fileName = fileName.substring(0, fileName.length()
							- ".arg_diagram".length()); //$NON-NLS-1$
					fileName += ".arg";
					dawnDomainModelFilePage.setResourceNamePrefix(fileName);
					dawnDomainModelFilePage
							.setResourcePath(dawnDiagramModelFilePage
									.getResourcePath());
				}
				super.setVisible(visible);
			}
		};
		
		dawnDomainModelFilePage
				.setTitle("New System Integration Arg Model");
		dawnDomainModelFilePage
				.setDescription("Select a location to store the generated integration arg model");

		dawnDomainModelFilePage
				.setResourceValidationType(DawnCreateNewResourceWizardPage.VALIDATION_WARN);
		addPage(dawnDomainModelFilePage);
	}


	
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.selection = selection;
	}
	
	public void dispose() {
		view.close();
	}
	
	public boolean canFinish()
	{
		if(getContainer().getCurrentPage() == dawnDomainModelFilePage)
			return true;
		else 
			return false;
	}
	
	public boolean performFinish() {
		ProgressMonitorDialog dialog = new ProgressMonitorDialog(getShell());
		dialog.open();
		IProgressMonitor monitor = dialog.getProgressMonitor();
		monitor.beginTask("Creating System Integration Argumentation Model and Diagram", 20);
	
		try {			
			doFinish(monitor);
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		monitor.worked(1);
		dialog.close();
		return true;
	}
	
	public void doFinish(IProgressMonitor monitor)
	{
		ArrayList<ContractModelElement> systemModules = moduleSelectionPage.getContractModelElements();
		ArrayList<ContractModelElement> contractModules = contractModuleSelectionPage.getContractModelElements();
		ContractModelBuilder cBuilder = new ContractModelBuilder();
		Case assuranceCase = argFactory.createCase();
		ArrayList results  = cBuilder.validateAndIntegrateArgumentModules(systemModules, contractModules, assuranceCase);
		if (results.size() == 0)
		{
			MessageDialog dialog = new MessageDialog(Display.getDefault().getActiveShell(), "Contract Validation", null,
					"Passed Contract Validation: All Undevloped Goals are Resolved by Contract", MessageDialog.INFORMATION, new String[] { "OK",}, 0);
			int result = dialog.open();
			URI diagramResourceURI = dawnDiagramModelFilePage.getURI();
			URI domainModelResourceURI = dawnDomainModelFilePage.getURI();
			cBuilder.createDiagram(domainModelResourceURI, diagramResourceURI, monitor, assuranceCase);
			monitor.worked(1);
		}
		else
		{
			MessageDialog dialog = new MessageDialog(Display.getDefault().getActiveShell(), "Contract Validation", null,
					"Failed Contract Validation", MessageDialog.ERROR, new String[] { "OK",}, 0);
				int result = dialog.open();
				AnalysisResultsDialog resultsDialog = new AnalysisResultsDialog(Display.getDefault().getActiveShell(), results);
				int result2 = resultsDialog.open();
		}
		
	}

}
