/*******************************************************************************
 * Copyright (c) 2018, MDH 
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *   Faiz Ul Muram, Samina Kanwal and Muhammad Atif Javed
 *   Initial API and implementation and/or initial documentation
 *******************************************************************************/
/**
 */
package org.eclipse.opencert.epf2OpenCert.transformation.popup.action;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.epf.library.edit.LibraryEditPlugin;
import org.eclipse.epf.uma.impl.ProcessComponentImpl;
import org.eclipse.epsilon.emc.emf.EmfModel;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.eol.exceptions.models.EolModelLoadingException;
import org.eclipse.epsilon.etl.EtlModule;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.IWorkbenchPage;


public class ProcEvidTransformation {

	private IWorkbenchPage page;
	private String name;

	public ProcEvidTransformation(IWorkbenchPage page){
		this.page=page;
	}

	public void execute(IProgressMonitor monitor){

		EtlModule etlModule = new EtlModule();
		
		//Get the current delivery process
		ISelection sel = page.getSelection();
		TreeSelection tree = (TreeSelection) sel;
		ProcessComponentImpl process = (ProcessComponentImpl) tree.getFirstElement();
		
		name = process.getName();
		

		String pluginPathWS = process.eContainer().eResource().getURI().toFileString();
		File plugin = new File(pluginPathWS); //REQ
		String directoryRootPlugin = plugin.getParent();
		File deliveryProcess = new File(directoryRootPlugin+"/deliveryprocesses/"+ process.getName()+"/model.xmi"); //LIB
		File deliveryProcess2 = new File(directoryRootPlugin+"/deliveryprocesses/"+ process.getName()+"/content.xmi"); //LIB2
		File directoryRoles = new File(directoryRootPlugin+"/roles"); //ROL
		File directoryTools = new File(directoryRootPlugin+"/tools"); //TOOL
		
		String[] listRoles = directoryRoles.list();
		String[] listTools = directoryTools.list();
		
		
		String configPath = null;

		String spluginPath = LibraryEditPlugin.getPlugin().getBundle().getLocation();
		if(spluginPath.contains("plugins")) // Yes, in jar mode.
		{			
			String pluginPath = spluginPath.substring(15, spluginPath.indexOf("plugins"));
			configPath = pluginPath + "configuration";
			
			try {
				String etlPath = configPath + "/" + "epsilon" +"/" + "epf2OpenCert.etl";
				etlModule.parse(new File(etlPath));
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			monitor.worked(2);						
		}

		String modelPath = configPath + "/" + "model" +"/" + "uma.ecore";
		
		//Create the source models
		EmfModel libModel = createEMFSourceModel("LIB", modelPath, deliveryProcess, true, false);
		EmfModel libModel2 = createEMFSourceModel("LIB2", modelPath, deliveryProcess2, true, false);
		
		EmfModel reqModel = createEMFSourceModel("REQ", modelPath, plugin, true, false);
		
		
		List<EmfModel> listModelsRoles = new ArrayList<EmfModel>();
		for (int i=0;i<listRoles.length;i++){
			File role = new File(directoryRoles+"/"+listRoles[i]);
			EmfModel roleModel = createEMFSourceModel(listRoles[i], modelPath, role, true, false);
			roleModel.getAliases().add("ROL");
			listModelsRoles.add(roleModel);	
		}
		List<EmfModel> listModelsTools = new ArrayList<EmfModel>();
		for (int i=0;i<listTools.length;i++){
			File tool = new File(directoryTools+"/"+listTools[i]);
			EmfModel toolModel = createEMFSourceModel(listTools[i], modelPath, tool, true, false);
			toolModel.getAliases().add("TOOL");
			listModelsRoles.add(toolModel);	
		}
		
		//Create the Process project into the workspace
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IProject project = root.getProject("Process");
		String projectLocation = root.getLocation().toString()+"/Process";
		if(project.exists()){
			try {
				project.open(new NullProgressMonitor());
			} catch (CoreException e1) {
				e1.printStackTrace();
			}
		}else{
			try {
				project.create(new NullProgressMonitor());
			} catch (CoreException e1) {
				e1.printStackTrace();
			}
			try {
				project.open(new NullProgressMonitor());
			} catch (CoreException e1) {
				e1.printStackTrace();
			}
		}
		
		//Create the Evidence project into the workspace
		IWorkspaceRoot root1 = ResourcesPlugin.getWorkspace().getRoot();
		IProject project1 = root1.getProject("Evidence");
		String projectLocation1 = root1.getLocation().toString()+"/Evidence";
		if(project1.exists()){
			try {
				project1.open(new NullProgressMonitor());
			} catch (CoreException e1) {
				e1.printStackTrace();
			}
		}else{
			try {
				project1.create(new NullProgressMonitor());
			} catch (CoreException e1) {
				e1.printStackTrace();
			}
			try {
				project1.open(new NullProgressMonitor());
			} catch (CoreException e1) {
				e1.printStackTrace();
			}
		}
		
		monitor.worked(3);
		
		
		String modelPath_ = configPath + "/" + "model" +"/" + "process.ecore";
		//Create the target model
		EmfModel procModel = createEMFTargetModel("PRO","http://process/1.0", modelPath_, projectLocation+"/"+name+".process", false, true);
		//Create the target model
		String modelPath_1 = configPath + "/" + "model" +"/" + "evidence.ecore";
		EmfModel eviModel = createEMFTargetModel("ART","http://evidence/1.0", modelPath_1, projectLocation1+"/"+name+".evidence", false, true);
		//Load the models into ETL module
		etlModule.getContext().getModelRepository().addModel(libModel);
		etlModule.getContext().getModelRepository().addModel(libModel2);
		etlModule.getContext().getModelRepository().addModel(procModel);
		etlModule.getContext().getModelRepository().addModel(eviModel);
		etlModule.getContext().getModelRepository().addModel(reqModel);
		
		for(EmfModel model: listModelsRoles){
			etlModule.getContext().getModelRepository().addModel(model);
		}
		for(EmfModel model: listModelsTools){
			etlModule.getContext().getModelRepository().addModel(model);
		}
		
		//Running the transformation
		try {
			etlModule.execute();
		} catch (EolRuntimeException e) {
			e.printStackTrace();
		}
		
		monitor.worked(4);
		
		libModel.dispose();
		procModel.dispose();
		eviModel.dispose();
		reqModel.dispose();
		libModel2.dispose();
		
		
		for(EmfModel modelRole: listModelsRoles){
			modelRole.dispose();
		}
		
		for(EmfModel modelTool: listModelsTools){
			modelTool.dispose();
		}	
		
		etlModule.getContext().getModelRepository().dispose();	
	}
	
	
	protected EmfModel createEMFSourceModel(String name, String sourceMetaModelFilePath,
			File sourceModelFilePath, Boolean sourceReadOnLoad, Boolean sourceStoreOnDisposal){
		
		EmfModel emfModel= new EmfModel();
		emfModel.setName(name);
		
		if(sourceMetaModelFilePath != null && !sourceMetaModelFilePath.isEmpty()) { 
			if(sourceMetaModelFilePath.contains(",")) {
				String[] metaModelURIs = sourceMetaModelFilePath.split(",");
				List<String> files = new ArrayList<String>(metaModelURIs.length);
				
				for(int i=0;i<metaModelURIs.length; i++)
				{
					files.add(metaModelURIs[i].trim());
				};
				
				for(int i=0; i<metaModelURIs.length; i++){
					files.add(metaModelURIs[i].trim());
				}
				
				emfModel.setMetamodelFiles(files);
			}else {
				emfModel.setMetamodelFile(sourceMetaModelFilePath);
			}
		}
			
		emfModel.setModelFile(sourceModelFilePath.getAbsolutePath());
		emfModel.setReadOnLoad(sourceReadOnLoad);
		emfModel.setStoredOnDisposal(sourceStoreOnDisposal);
		
		// MCP
		emfModel.setCachingEnabled(true);
		emfModel.setExpand(true);
		
		// MCP
		try {
			emfModel.load();
		} catch (EolModelLoadingException e) {
			e.printStackTrace();
		} 
		return emfModel;
	}
	
	
	protected EmfModel createEMFTargetModel(String name, String targetMetaModelURI, String targetMetaModelFilePath,
			String targetModelFilePath, Boolean targetReadOnLoad, Boolean targetStoreOnDisposal){
		
		EmfModel emfModel= new EmfModel();
		emfModel.setName(name);

		if(targetMetaModelURI != null && !targetMetaModelURI.isEmpty()) 
		{ 
			if(targetMetaModelURI.contains(","))
			{
				String[] metaModelURIs = targetMetaModelURI.split(",");
				List<String> uris =new ArrayList<String>(metaModelURIs.length);
				for(int i=0;i<metaModelURIs.length; i++)
				{
					uris.add(metaModelURIs[i].trim());
				};
				emfModel.setMetamodelUris(uris);
			}
			else
			{
				emfModel.setMetamodelUri(targetMetaModelURI);
			}
		}
		
		if(targetMetaModelFilePath != null && !targetMetaModelFilePath.isEmpty()) { 
			if(targetMetaModelFilePath.contains(",")) {
				String[] metaModelURIs = targetMetaModelFilePath.split(",");
				List<String> files = new ArrayList<String>(metaModelURIs.length);
				
				for(int i=0;i<metaModelURIs.length; i++)
				{
					files.add(metaModelURIs[i].trim());
				};
				
				for(int i=0; i<metaModelURIs.length; i++){
					files.add(metaModelURIs[i].trim());
				}
				emfModel.setMetamodelFiles(files);
			}else {
				emfModel.setMetamodelFile(targetMetaModelFilePath);
			}
		}
			
		emfModel.setModelFile(targetModelFilePath);
		emfModel.setReadOnLoad(targetReadOnLoad);
		emfModel.setStoredOnDisposal(targetStoreOnDisposal);
		
		// MCP
		emfModel.setCachingEnabled(true);
		emfModel.setExpand(true);
		
		// MCP
		try {
			emfModel.load(); //254
		} catch (EolModelLoadingException e) {
			e.printStackTrace();
		} 
		return emfModel; 
						
	}
}
